package es.uji.apps.upo.services;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.util.Log4jConfigListener;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.spi.spring.container.servlet.SpringServlet;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;

import es.uji.apps.upo.model.enums.IdiomaPublicacion;
import es.uji.apps.upo.solr.SolrIndexer;
import es.uji.apps.upo.solr.ContenidoSolr;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;

@Ignore
//TODO: Hace fallar otros tests, por ejemplo, si ejecutamos esta suite:
/*
  @Suite.SuiteClasses({
        es.uji.apps.upo.services.BusquedaServiceTest.class,
        es.uji.apps.upo.publicacion.SincronizacionContenidosTest.class,
        es.uji.apps.upo.dao.NodoMapaDAOTest.class })
*/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class BusquedaServiceTest extends JerseyTest
{
    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();
    
    @Autowired
    public SolrIndexer indexer;

    private WebResource resource;

    public BusquedaServiceTest()
    {
        super(new WebAppDescriptor.Builder("es.uji.apps.upo.services")
                .contextParam("contextConfigLocation", "classpath:applicationContext.xml")
                .contextParam("log4jConfigLocation", "src/main/webapp/WEB-INF/log4j.properties")
                .contextParam("webAppRootKey", "uji-upo-base.root")
                .contextListenerClass(Log4jConfigListener.class)
                .contextListenerClass(ContextLoaderListener.class)
                .requestListenerClass(RequestContextListener.class)
                .servletClass(SpringServlet.class).build());

        this.resource = resource();
    }

    @Before
    public void initNewIndexedContent()
    {
        ContenidoSolr contenido = new ContenidoSolr();
        contenido.setId("1");
        contenido.setUrl("perico");
        contenido.setUrlCompleta("/home/perico");
        contenido.setTitulo("Título contenido");
        contenido.setIdioma(IdiomaPublicacion.ES);

        indexer.add(contenido);
    }

    @After
    public void removeIndexedTestDocuments()
    {
        indexer.deleteById("1");
    }

    @Test
    public void searchMethodShouldReturnIndexedValues()
    {
        ClientResponse response = resource.path("busqueda").queryParam("query", "id:1")
                .get(ClientResponse.class);

        List<ContenidoSolr> searchList = response.getEntity(new GenericType<List<ContenidoSolr>>()
        {
        });

        assertEquals(1, searchList.size());
        assertEquals("perico", searchList.get(0).getUrl());
    }

    @Test
    public void shouldBePossibleToSearchByFullPath()
    {
        ClientResponse response = resource.path("busqueda").queryParam("query", "url_completa:/home/perico")
                .get(ClientResponse.class);

        List<ContenidoSolr> searchList = response.getEntity(new GenericType<List<ContenidoSolr>>()
        {
        });

        assertEquals(1, searchList.size());
        assertEquals("/home/perico", searchList.get(0).getUrlCompleta());
    }
}
