package es.uji.apps.upo.migracion;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import es.uji.apps.upo.services.ItemMigracionService;
import junit.framework.Assert;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import es.uji.apps.upo.exceptions.ImportarDocumentoAlReservoriException;
import es.uji.apps.upo.storage.Reservori;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;

@Ignore
public class UrlExtractorTest
{

    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();
    
    private static final String URL_NUEVA_IMAGEN = "http://desymfony.com/images/ponentes/ricardo-borillo.png";
    private static final String URL_NUEVA_DOCS = "/otra/url/con/docs.pdf";
    private static final String ROOT_NODE = "/";
    
    private Reservori reservori = mock(Reservori.class);
    private ItemMigracionService itemMigracionService = mock(ItemMigracionService.class);

    public static byte[] inputStreamToByteArray(InputStream in) throws IOException
    {
        byte[] buffer = new byte[2048];
        int length = 0;

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        while ((length = in.read(buffer)) >= 0)
        {
            baos.write(buffer, 0, length);
        }

        return baos.toByteArray();
    }

    @Test
    public void compruebaQueExistenDosImagenes() throws Exception
    {
        String content = new String(inputStreamToByteArray(new FileInputStream(
                "src/test/resources/urlExtractor-good.html")));
        UrlExtractor urlExtractor = new UrlExtractor(content);

        Assert.assertEquals(2, urlExtractor.extractImages().size());
    }

    @Test
    public void contenidoSinImagenesDebeDevolverCero() throws Exception
    {
        String content = new String(inputStreamToByteArray(new FileInputStream(
                "src/test/resources/urlExtractor-sin-img.html")));
        UrlExtractor urlExtractor = new UrlExtractor(content);

        Assert.assertEquals(0, urlExtractor.extractImages().size());
    }

    @Test
    public void contenidoConDosImagenesDebeDevolverLosDosSrcEsperados()
            throws Exception
    {
        String content = new String(inputStreamToByteArray(new FileInputStream(
                "src/test/resources/urlExtractor-good.html")));
        UrlExtractor urlExtractor = new UrlExtractor(content);
        ArrayList<String> urls = urlExtractor.extractImages();

        Assert.assertEquals(2, urls.size());
        Assert.assertEquals("/kk/imagen.jpg", urls.get(0));
        Assert.assertEquals("/uuu/que/miedo/imagen.jpg", urls.get(1));
    }

    @Test
    public void contenidoConDosDocsDebeDevolverLosDosHrefEsperados() throws Exception
    {
        String content = new String(inputStreamToByteArray(new FileInputStream(
                "src/test/resources/urlExtractor-con-docs.html")));
        UrlExtractor urlExtractor = new UrlExtractor(content);
        ArrayList<String> urls = urlExtractor.extractFiles();

        Assert.assertEquals(3, urls.size());
        Assert.assertEquals("/uuu/que/miedo/doc1.pdf?guest=true", urls.get(0));
        Assert.assertEquals("/kk/doc2.DOC?", urls.get(1));
        Assert.assertEquals("doc3.doc", urls.get(2));
    }

    @Test
    public void contenidoConDosImagenesDebeTenerDosImagenesConvertidas()
            throws Exception
    {
        String content = new String(inputStreamToByteArray(new FileInputStream(
                "src/test/resources/urlExtractor-good.html")));
        UrlExtractor urlExtractor = new UrlExtractor(content);
        urlExtractor.setReservori(reservori);
        urlExtractor.setItemMigracionService(itemMigracionService);

        when(reservori.convierteURL(any())).thenReturn(URL_NUEVA_IMAGEN);

        ArrayList<String> arrayConUrlsViejas = urlExtractor.extractImages();
        ArrayList<String> arrayConUrlsNuevas = urlExtractor
                .convertirURLenRecursoReservori(arrayConUrlsViejas, ROOT_NODE);

        Assert.assertEquals(2, arrayConUrlsNuevas.size());
    }

    @Test
    public void contenidoSinImagenesNoDebeTenerImagenesConvertidas() throws Exception
    {
        String content = new String(inputStreamToByteArray(new FileInputStream(
                "src/test/resources/urlExtractor-sin-img.html")));
        UrlExtractor urlExtractor = new UrlExtractor(content);
        urlExtractor.setReservori(reservori);

        when(reservori.convierteURL(any())).thenReturn(URL_NUEVA_IMAGEN);

        ArrayList<String> arrayConUrlsViejas = urlExtractor.extractImages();
        ArrayList<String> arrayConUrlsNuevas = urlExtractor
                .convertirURLenRecursoReservori(arrayConUrlsViejas, ROOT_NODE);

        Assert.assertEquals(0, arrayConUrlsNuevas.size());

    }

    @Test
    public void contenidoConDosDocsDebeTenerDosDocsConvertidos() throws Exception
    {
        String content = new String(inputStreamToByteArray(new FileInputStream(
                "src/test/resources/urlExtractor-con-docs.html")));
        UrlExtractor urlExtractor = new UrlExtractor(content);
        urlExtractor.setReservori(reservori);
        urlExtractor.setItemMigracionService(itemMigracionService);

        when(reservori.convierteURL(any())).thenReturn(URL_NUEVA_DOCS);

        ArrayList<String> arrayConUrlsViejas = urlExtractor.extractFiles();
        ArrayList<String> arrayConUrlsNuevas = urlExtractor
                .convertirURLenRecursoReservori(arrayConUrlsViejas, ROOT_NODE);

        Assert.assertEquals(3, arrayConUrlsNuevas.size());
    }

    @Test
    public void contenidoSinDocsNoDebeTenerDocsConvertidos() throws Exception
    {
        String content = new String(inputStreamToByteArray(new FileInputStream(
                "src/test/resources/urlExtractor-sin-docs.html")));
        UrlExtractor urlExtractor = new UrlExtractor(content);
        urlExtractor.setReservori(reservori);

        when(reservori.convierteURL(any())).thenReturn(URL_NUEVA_DOCS);

        ArrayList<String> arrayConUrlsViejas = urlExtractor.extractImages();
        ArrayList<String> arrayConUrlsNuevas = urlExtractor
                .convertirURLenRecursoReservori(arrayConUrlsViejas, ROOT_NODE);

        Assert.assertEquals(0, arrayConUrlsNuevas.size());
    }
}
