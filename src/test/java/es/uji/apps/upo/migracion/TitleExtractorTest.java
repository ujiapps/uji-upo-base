package es.uji.apps.upo.migracion;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import junit.framework.Assert;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import es.uji.commons.testing.junit.dao.StaticDAOCleaner;

@Ignore
public class TitleExtractorTest
{
    
    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();
    
    @Test
    public void siExisteUnaEtiquetaH2DebeDevolverSuContenido() throws FileNotFoundException, IOException
    {
        String content = getFileContent("src/test/resources/titleExtractor-con-1-h2.html");        
        
        Assert.assertEquals("OK", TitleExtractor.getTitle(content));
    }
    
    @Test
    public void siExisteMasDeUnaEtiquetaH2DebeDevolverElContenidoDelPrimero() throws FileNotFoundException, IOException
    {
        String content = getFileContent("src/test/resources/titleExtractor-con-varios-h2.html");        
        
        Assert.assertEquals("OK", TitleExtractor.getTitle(content));
    }
    
    @Test
    public void siExisteUnaEtiquetaH2EnMayusculaDebeDevolverSuContenido() throws FileNotFoundException, IOException
    {
        String content = getFileContent("src/test/resources/titleExtractor-con-1-H2-en-mayuscula.html");        
        
        Assert.assertEquals("OK", TitleExtractor.getTitle(content));
    }
    
    @Test
    public void siExisteUnaEtiquetaH2ConAtributosDebeDevolverSuContenido() throws FileNotFoundException, IOException
    {
        String content = getFileContent("src/test/resources/titleExtractor-con-1-h2-con-atributos.html");        
        
        Assert.assertEquals("OK", TitleExtractor.getTitle(content));
    }
    
    @Test
    public void siNoExisteUnaEtiquetaH2DebeDevolverNull() throws FileNotFoundException, IOException
    {
        String content = getFileContent("src/test/resources/titleExtractor-sin-h2.html");        
        
        Assert.assertNull(TitleExtractor.getTitle(content));
    }
    
    public String getFileContent(String file) throws IOException, FileNotFoundException
    {
        return new String(inputStreamToByteArray(new FileInputStream(file)));
    }
    
    public static byte[] inputStreamToByteArray(InputStream in) throws IOException
    {
        byte[] buffer = new byte[2048];
        int length = 0;

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        while ((length = in.read(buffer)) >= 0)
        {
            baos.write(buffer, 0, length);
        }

        return baos.toByteArray();
    }
}
