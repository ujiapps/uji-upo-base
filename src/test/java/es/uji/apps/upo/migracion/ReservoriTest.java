package es.uji.apps.upo.migracion;

import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;

import junit.framework.Assert;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.uji.apps.upo.exceptions.ImportarDocumentoAlReservoriException;
import es.uji.apps.upo.storage.Reservori;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;

import static junit.framework.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
@Ignore
public class ReservoriTest
{
    private static final String ROOT_NODE = "/";
    
    @Autowired
    Reservori reservori;

    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();
    
    private String server;
    
    @Autowired
    public void setConfigValues(@Value("${uji.alfresco.server}") String server)
    {
        this.server = server;
    }

    @Test
    public void SiUnaUrlNoEsAccesibleDebeDevolverLaPropiaUrl()
            throws Exception
    {
        String urlExterna = "http://www.uji.es/tendriaNaricesQueEstoExistiera";

        assertEquals(urlExterna, reservori.convierteURL(new FileSettings(urlExterna, ROOT_NODE)));
    }

    @Test
    public void SiUnaUrlExisteDeberiaDevolverUnaUrlDelRepositorio()
            throws Exception
    {
        String urlCorrecta = "https://e-ujier.uji.es/img/mas.gif";

        String resultado = reservori.convierteURL(new FileSettings(urlCorrecta, ROOT_NODE));

        assertTrue(resultado
                .startsWith(server + "/alfresco/d/d/workspace/SpacesStore/"));

        assertTrue(resultado.endsWith("mas.gif?guest=true"));
    }
}
