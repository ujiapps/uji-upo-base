package es.uji.apps.upo.Translator.Apertium;

import java.net.MalformedURLException;

import junit.framework.Assert;

import org.apache.xmlrpc.XmlRpcException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.uji.apps.upo.exceptions.TraducirTextoException;
import es.uji.apps.upo.translator.TranslatorClient;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class ApertiumTranslatorClientTest
{
    public static final String HTML = "html";
    private String text;
    private String sourceLang;
    private String targetLang;
    private String targetText;

    @Autowired
    private TranslatorClient translatorClient;

    @org.junit.Test
    public void englishToEspanishTranslation() throws TraducirTextoException
    {
        text = "<input type=\"button\">Proof of concept</input>";
        sourceLang = "en";
        targetLang = "es";

        targetText = "<input type=\"button\">Prueba de concepto</input>";

        Assert.assertEquals(targetText,
                translatorClient.translate(text, HTML, sourceLang, targetLang));
    }

    @org.junit.Test
    public void espanishToCatalanranslation() throws TraducirTextoException
    {
        text = "<input type=\"button\">Prueba de concepto</input>";
        sourceLang = "es";
        targetLang = "ca";

        targetText = "<input type=\"button\">Prova de concepte</input>";

        Assert.assertEquals(targetText,
                translatorClient.translate(text, HTML, sourceLang, targetLang));
    }

    @org.junit.Test
    public void espanishtoEnglishTranslation() throws TraducirTextoException
    {
        text = "<input type=\"button\">Prueba de concepto</input>";
        sourceLang = "es";
        targetLang = "en";

        targetText = "<input type=\"button\">Test of concept</input>";

        Assert.assertEquals(targetText,
                translatorClient.translate(text, HTML, sourceLang, targetLang));
    }

    @org.junit.Test
    public void useValencianVersion() throws TraducirTextoException
    {
        text = "color rojo";
        sourceLang = "es";
        targetLang = "ca";

        targetText = "color roig";

        Assert.assertEquals(targetText,
                translatorClient.translate(text, HTML, sourceLang, targetLang));
    }
}
