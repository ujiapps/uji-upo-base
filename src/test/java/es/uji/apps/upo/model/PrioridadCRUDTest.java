package es.uji.apps.upo.model;

import es.uji.apps.upo.dao.NodoMapaDAO;
import es.uji.apps.upo.dao.PrioridadContenidoDAO;
import es.uji.apps.upo.exceptions.*;
import es.uji.apps.upo.services.PersonaService;
import es.uji.apps.upo.services.PrioridadContenidoService;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.text.ParseException;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class PrioridadCRUDTest
{
    private static final String URL = "/una/url/aleatoria/";
    private static final Long PERSONA_ID = 1L;

    private PrioridadContenido prioridadContenido;
    private PrioridadContenidoDAO prioridadContenidoDAO;
    private PrioridadContenidoService prioridadContenidoService;
    private Persona persona;
    private NodoMapa nodoMapa;
    private Contenido contenido;
    private NodoMapaDAO nodoMapaDAO;
    private PersonaService personaService;

    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    @Before
    public void init()
    {
        contenido = mock(Contenido.class);
        nodoMapa = mock(NodoMapa.class);
        nodoMapaDAO = mock(NodoMapaDAO.class);
        persona = mock(Persona.class);
        personaService = mock(PersonaService.class);

        when(persona.getId()).thenReturn(PERSONA_ID);
        when(contenido.getNodoMapa()).thenReturn(nodoMapa);
        when(nodoMapa.getUrlCompleta()).thenReturn(URL);

        prioridadContenidoDAO = mock(PrioridadContenidoDAO.class);

        prioridadContenido = new PrioridadContenido();
        prioridadContenidoService = new PrioridadContenidoService(personaService, prioridadContenidoDAO);
        prioridadContenido.setUpoObjeto(contenido);
    }

    @Test
    public void elAdministradorDeUnNodoPuedeBorrarUnaPrioridad()
            throws BorradoContenidoNoAutorizadoException
    {
        when(personaService.isNodoMapaEditable(nodoMapa.getUrlCompleta(), persona)).thenReturn(true);

        prioridadContenidoService.delete(persona, prioridadContenido);

        verify(prioridadContenidoDAO).delete(PrioridadContenido.class, prioridadContenido.getId());
        verify(personaService).isNodoMapaEditable(anyString(), any(Persona.class));
    }

    @Test(expected = BorradoContenidoNoAutorizadoException.class)
    public void ocurreUnErrorCuandoUnUsuarioNoAdministradorBorraUnContenidoNoAutorizado()
            throws BorradoContenidoNoAutorizadoException
    {
        when(personaService.isNodoMapaEditable(nodoMapa.getUrlCompleta(), persona)).thenReturn(false);

        prioridadContenidoService.delete(persona, prioridadContenido);

        verify(prioridadContenidoDAO, times(0)).delete(prioridadContenido);
    }

    @Test
    public void elAdministradorDeUnNodoPuedeActualizarUnaPrioridad()
            throws           ActualizadoContenidoNoAutorizadoException,
            FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException, UsuarioNoAutenticadoException
    {
        String nodoMapaUrlCompleta = nodoMapa.getUrlCompleta();
        when(personaService.isNodoMapaEditable(nodoMapaUrlCompleta, persona)).thenReturn(true);

        prioridadContenidoService.update(persona, prioridadContenido);

        verify(prioridadContenidoDAO).update(prioridadContenido);
        verify(personaService).isNodoMapaEditable(nodoMapaUrlCompleta, persona);
    }

    @Test(expected = ActualizadoContenidoNoAutorizadoException.class)
    public void ocurreUnErrorCuandoUnUsuarioNoAdministradorActualizaUnContenidoNoAutorizado()
            throws
            ActualizadoContenidoNoAutorizadoException,
            FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException, UsuarioNoAutenticadoException
    {
        when(personaService.isNodoMapaEditable(nodoMapa.getUrlCompleta(), persona)).thenReturn(false);

        prioridadContenidoService.update(persona, prioridadContenido);
        verify(prioridadContenidoDAO, times(0)).update(prioridadContenido);
    }

    @Test
    public void elAdministradorDeUnNodoPuedeInsertarUnaPrioridad()
            throws InsertadoContenidoNoAutorizadoException,
            FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException, UsuarioNoAutenticadoException
    {
        String nodoMapaUrlCompleta = nodoMapa.getUrlCompleta();
        when(personaService.isNodoMapaEditable(nodoMapaUrlCompleta, persona)).thenReturn(true);

        prioridadContenidoService.insert(persona, prioridadContenido);

        verify(prioridadContenidoDAO).insert(prioridadContenido);
        verify(personaService).isNodoMapaEditable(nodoMapaUrlCompleta, persona);
    }

    @Test(expected = InsertadoContenidoNoAutorizadoException.class)
    public void ocurreUnErrorCuandoUnUsuarioNoAdministradorInsertaUnContenidoNoAutorizado()
            throws
            InsertadoContenidoNoAutorizadoException,
            FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException, UsuarioNoAutenticadoException
    {
        when(personaService.isNodoMapaEditable(nodoMapa.getUrlCompleta(), persona)).thenReturn(false);

        prioridadContenidoService.insert(persona, prioridadContenido);
        verify(prioridadContenidoDAO, times(0)).insert(prioridadContenido);
    }
}