package es.uji.apps.upo.model;

import es.uji.apps.upo.model.enums.TipoMime;
import junit.framework.Assert;

import org.junit.Rule;
import org.junit.Test;

import es.uji.commons.testing.junit.dao.StaticDAOCleaner;

public class ContenidoIdiomaRecursoTest
{
    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();
    
    @Test
    public void SiLaURLTieneUnaExtensionConocidaDebeObtenerElTipoMimeAsociado() throws Exception
    {
        Assert.assertEquals(TipoMime.PDF.getTipoMime(), ContenidoIdiomaRecurso.getTipoMime("http://www.uji.es/prueba.pdf"));
        Assert.assertEquals(TipoMime.GIF.getTipoMime(), ContenidoIdiomaRecurso.getTipoMime("http://www.uji.es/prueba.gif"));
        Assert.assertEquals(TipoMime.GIF.getTipoMime(), ContenidoIdiomaRecurso.getTipoMime("/path/prueba.gif"));
        Assert.assertEquals(TipoMime.PNG.getTipoMime(), ContenidoIdiomaRecurso.getTipoMime("http://www.uji.es/path/prueba.png?pepe=pepe&pepe=pepe"));
        Assert.assertEquals(TipoMime.JPG.getTipoMime(), ContenidoIdiomaRecurso.getTipoMime("http://www.uji.es/path/prueba.jpg?pepe=pepe.pepe&pepe=pepe"));
        Assert.assertEquals(TipoMime.DOC.getTipoMime(), ContenidoIdiomaRecurso.getTipoMime("prueba.doc"));
    }
    
    @Test
    public void SiLaURLNoTieneUnaExtensionConocidaDebeDevolverNull() throws Exception
    {
        Assert.assertNull(ContenidoIdiomaRecurso.getTipoMime("http://www.uji.es/prueba.xxx"));
    }    

    @Test
    public void dadaUnaURLDebemosPoderObtenerElNombreDelRecurso() throws Exception
    {
        Assert.assertEquals("prueba.pdf", ContenidoIdiomaRecurso.getNombreRecurso("http://www.uji.es/prueba.pdf"));
        Assert.assertEquals("prueba.gif", ContenidoIdiomaRecurso.getNombreRecurso("/path/prueba.gif"));
        Assert.assertEquals("prueba.png", ContenidoIdiomaRecurso.getNombreRecurso("http://www.uji.es/path/prueba.png?pepe=pepe&pepe=pepe"));
        Assert.assertEquals("prueba.doc", ContenidoIdiomaRecurso.getNombreRecurso("prueba.doc"));
    }
}
