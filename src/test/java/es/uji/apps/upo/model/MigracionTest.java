package es.uji.apps.upo.model;

import java.io.FileNotFoundException;
import java.io.IOException;

import es.uji.apps.upo.dao.MigracionDAO;
import es.uji.apps.upo.services.MigracionService;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import es.uji.commons.testing.junit.dao.StaticDAOCleaner;

public class MigracionTest
{
    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    private MigracionService migracionService;

    @Before
    public void init()
    {
        migracionService = new MigracionService(new MigracionDAO());
    }

    @Test
    public void siLaURLEsNulaLaURLPathHaDeSerNula() throws FileNotFoundException, IOException
    {
        Assert.assertNull(migracionService.getUrlPath(null));
    }
    
    @Test
    public void siLaURLTieneUnSoloNodoLaURLPathHaDeSerIgualADichoNodo() throws FileNotFoundException, IOException
    {
        Assert.assertEquals("pepe", migracionService.getUrlPath("pepe"));
    }    
    
    @Test
    public void siLaURLTieneVariosNodosLaURLPathHaDeSerIgualAlUltimo() throws FileNotFoundException, IOException
    {
        Migracion migracion = new Migracion();
        Assert.assertEquals("pepe3", migracionService.getUrlPath("pepe/pepe2/pepe3"));
    }    
}
