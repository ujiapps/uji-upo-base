package es.uji.apps.upo.model;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.Calendar;

import es.uji.apps.upo.services.PersonaService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import es.uji.apps.upo.exceptions.ActualizadoContenidoNoAutorizadoException;
import es.uji.apps.upo.exceptions.FechaFinPrioridadSuperiorAFechaInicioException;
import es.uji.apps.upo.exceptions.InsertadoContenidoNoAutorizadoException;
import es.uji.apps.upo.exceptions.SeIndicaUnaHoraSinFechaAsociadaException;
import es.uji.apps.upo.exceptions.UsuarioNoAutenticadoException;
import es.uji.apps.upo.dao.PrioridadContenidoDAO;
import es.uji.apps.upo.services.PrioridadContenidoService;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;

public class PrioridadFechasTest
{
    private static final String URL = "/una/url/aleatoria/";

    private PrioridadContenido prioridadContenido;
    private PrioridadContenidoDAO prioridadContenidoDAO;
    private Calendar fechaInicio = Calendar.getInstance();
    private Calendar fechaFin = Calendar.getInstance();
    private Calendar horaInicio = Calendar.getInstance();
    private Calendar horaFin = Calendar.getInstance();
    private Persona persona;
    private NodoMapa nodoMapa;
    private Contenido contenido;
    private PrioridadContenidoService prioridadContenidoService;
    private PersonaService personaService;

    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    @Before
    public void init()
    {
        prioridadContenidoDAO = mock(PrioridadContenidoDAO.class);
        persona = mock(Persona.class);
        personaService = mock(PersonaService.class);

        contenido = mock(Contenido.class);
        nodoMapa = mock(NodoMapa.class);

        when(contenido.getNodoMapa()).thenReturn(nodoMapa);
        when(nodoMapa.getUrlCompleta()).thenReturn(URL);

        prioridadContenido = new PrioridadContenido();
        prioridadContenidoService = new PrioridadContenidoService(personaService, prioridadContenidoDAO);
        prioridadContenido.setUpoObjeto(contenido);

        when(personaService.isNodoMapaEditable(nodoMapa.getUrlCompleta(), persona)).thenReturn(true);
    }

    @Test(expected = FechaFinPrioridadSuperiorAFechaInicioException.class)
    public void ocurreUnErrorAlInsertarUnaPrioridadConUnaFechaFinInferiorOIgualAFechaInicio()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setFechasConFechaFinInferiorOIgualAFechaInicio();

        prioridadContenidoService.insert(persona, prioridadContenido);
    }

    @Test(expected = FechaFinPrioridadSuperiorAFechaInicioException.class)
    public void ocurreUnErrorAlActualizarUnaPrioridadConUnaFechaFinInferiorOIgualAFechaInicio()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            ActualizadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setFechasConFechaFinInferiorOIgualAFechaInicio();

        prioridadContenidoService.update(persona, prioridadContenido);
    }

    @Test
    public void ocurreUnErrorAlInsertarUnaPrioridadConUnaFechaInicioYFinIguales()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setFechasDeInicioYFinIguales();

        prioridadContenidoService.insert(persona, prioridadContenido);

        verify(prioridadContenidoDAO).insert(prioridadContenido);
    }

    @Test
    public void ocurreUnErrorAlActualizarUnaPrioridadConUnaFechaInicioYFinIguales()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            ActualizadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setFechasDeInicioYFinIguales();

        prioridadContenidoService.update(persona, prioridadContenido);
        verify(prioridadContenidoDAO).update(prioridadContenido);
    }

    @Test
    public void alInsertarUnaPrioridadConFechaFinSuperiorAFechaInicioSeActualizaElContenido()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setFechasConFinSuperiorAInicio();

        prioridadContenidoService.insert(persona, prioridadContenido);

        verify(prioridadContenidoDAO).insert(prioridadContenido);
    }

    @Test
    public void alActualizarUnaPrioridadConFechaFinSuperiorAFechaInicioSeActualizaElContenido()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            ActualizadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setFechasConFinSuperiorAInicio();

        prioridadContenidoService.update(persona, prioridadContenido);

        verify(prioridadContenidoDAO).update(prioridadContenido);
    }

    @Test
    public void alInsertarUnaPrioridadConFechasNulasSeActualizaElContenido()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setFechasNulas();

        prioridadContenidoService.insert(persona, prioridadContenido);

        verify(prioridadContenidoDAO).insert(prioridadContenido);
    }

    @Test
    public void alActualizarUnaPrioridadConFechasNulasSeActualizaElContenido()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            ActualizadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setFechasNulas();

        prioridadContenidoService.update(persona, prioridadContenido);

        verify(prioridadContenidoDAO).update(prioridadContenido);
    }

    @Test
    public void alInsertarUnaPrioridadConSoloFechaInicioSeActualizaElContenido()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        fechaInicio.set(2011, 8, 2);
        prioridadContenido.setFechaInicio(fechaInicio.getTime());

        prioridadContenidoService.insert(persona, prioridadContenido);

        verify(prioridadContenidoDAO).insert(prioridadContenido);
    }

    @Test
    public void alActualizarUnaPrioridadConSoloFechaInicioSeActualizaElContenido()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            ActualizadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        fechaInicio.set(2011, 8, 2);
        prioridadContenido.setFechaInicio(fechaInicio.getTime());

        prioridadContenidoService.update(persona, prioridadContenido);

        verify(prioridadContenidoDAO).update(prioridadContenido);
    }

    @Test
    public void alInsertarUnaPrioridadConSoloFechaFinSeActualizaElContenido()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        fechaFin.set(2011, 8, 2);
        prioridadContenido.setFechaFin(fechaFin.getTime());

        prioridadContenidoService.insert(persona, prioridadContenido);

        verify(prioridadContenidoDAO).insert(prioridadContenido);
    }

    @Test
    public void alActualizarUnaPrioridadConSoloFechaFinSeActualizaElContenido()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            ActualizadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        fechaFin.set(2011, 8, 2);
        prioridadContenido.setFechaFin(fechaFin.getTime());

        prioridadContenidoService.update(persona, prioridadContenido);

        verify(prioridadContenidoDAO).update(prioridadContenido);
    }

    @Test(expected = SeIndicaUnaHoraSinFechaAsociadaException.class)
    public void ocurreUnErrorAlInsertarUnaPrioridadConFechaInicioSoloIndicandoLaHora()
            throws SeIndicaUnaHoraSinFechaAsociadaException,
            FechaFinPrioridadSuperiorAFechaInicioException, ParseException,
            InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        horaInicio.set(2011, 1, 1, 9, 0);
        prioridadContenido.setHoraInicio(horaInicio.getTime());
        prioridadContenido.setFechaInicio(null);

        prioridadContenidoService.insert(persona, prioridadContenido);
    }

    @Test(expected = SeIndicaUnaHoraSinFechaAsociadaException.class)
    public void ocurreUnErrorAlActualizarUnaPrioridadConFechaInicioSoloIndicandoLaHora()
            throws SeIndicaUnaHoraSinFechaAsociadaException,
            FechaFinPrioridadSuperiorAFechaInicioException, ParseException,
            ActualizadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        horaInicio.set(2011, 1, 1, 9, 0);
        prioridadContenido.setHoraInicio(horaInicio.getTime());
        prioridadContenido.setFechaInicio(null);

        prioridadContenidoService.update(persona, prioridadContenido);
    }

    @Test(expected = SeIndicaUnaHoraSinFechaAsociadaException.class)
    public void ocurreUnErrorAlInsertarUnaPrioridadConFechaFinSoloIndicandoLaHora()
            throws SeIndicaUnaHoraSinFechaAsociadaException,
            FechaFinPrioridadSuperiorAFechaInicioException, ParseException,
            InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        horaFin.set(2011, 1, 1, 9, 0);
        prioridadContenido.setHoraFin(horaFin.getTime());
        prioridadContenido.setFechaFin(null);

        prioridadContenidoService.insert(persona, prioridadContenido);
    }

    @Test(expected = SeIndicaUnaHoraSinFechaAsociadaException.class)
    public void ocurreUnErrorAlActualizarUnaPrioridadConFechaFinSoloIndicandoLaHora()
            throws SeIndicaUnaHoraSinFechaAsociadaException,
            FechaFinPrioridadSuperiorAFechaInicioException, ParseException,
            ActualizadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        horaFin.set(2011, 1, 1, 9, 0);
        prioridadContenido.setHoraFin(horaFin.getTime());
        prioridadContenido.setFechaFin(null);

        prioridadContenidoService.update(persona, prioridadContenido);
    }

    @Test(expected = FechaFinPrioridadSuperiorAFechaInicioException.class)
    public void ocurreUnErrorAlInsertarUnaPrioridadConUnaHoraFinInferiorOIgualAHoraInicioEnElMismoDia()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setHorasMismoDiaOrdenIncorrecto();

        prioridadContenidoService.insert(persona, prioridadContenido);
    }

    @Test(expected = FechaFinPrioridadSuperiorAFechaInicioException.class)
    public void ocurreUnErrorAlActualizarUnaPrioridadConUnaHoraFinInferiorOIgualAHoraInicioEnElMismoDia()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            ActualizadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setHorasMismoDiaOrdenIncorrecto();

        prioridadContenidoService.update(persona, prioridadContenido);
    }

    @Test
    public void alInsertarUnaPrioridadConHoraFinSuperiorAHoraInicioElMismoDiaSeActualizaElContenido()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setHorasMismoDiaOrdenCorrecto();

        prioridadContenidoService.insert(persona, prioridadContenido);

        verify(prioridadContenidoDAO).insert(prioridadContenido);
    }

    @Test
    public void alActualizarUnaPrioridadConHoraFinSuperiorAHoraInicioElMismoDiaSeActualizaElContenido()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            ActualizadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setHorasMismoDiaOrdenCorrecto();

        prioridadContenidoService.update(persona, prioridadContenido);

        verify(prioridadContenidoDAO).update(prioridadContenido);
    }

    @Test
    public void alInsertarUnaPrioridadConHorasConsecutivasEnDiasDiferentesConsecutivosSeActualizaElContenido()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setHorasConsecutivasEnDiferentesDiasOrdenCorrecto();

        prioridadContenidoService.insert(persona, prioridadContenido);

        verify(prioridadContenidoDAO).insert(prioridadContenido);
    }

    @Test
    public void alActualizarUnaPrioridadConHorasConsecutivasEnDiasDiferentesConsecutivosSeActualizaElContenido()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            ActualizadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setHorasConsecutivasEnDiferentesDiasOrdenCorrecto();

        prioridadContenidoService.update(persona, prioridadContenido);

        verify(prioridadContenidoDAO).update(prioridadContenido);
    }

    @Test
    public void alInsertarUnaPrioridadConHoraFinInferiorOIgualAHoraInicioEnDiasDiferentesCorrectosSeActualizaElContenido()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setHoraFinAnteriorOIgualAHoraIncioDiasDiferentesOrdenCorrecto();

        prioridadContenidoService.insert(persona, prioridadContenido);

        verify(prioridadContenidoDAO).insert(prioridadContenido);
    }

    @Test
    public void alActualizarUnaPrioridadConHoraFinInferiorOIgualAHoraInicioEnDiasDiferentesCorrectosSeActualizaElContenido()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            ActualizadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setHoraFinAnteriorOIgualAHoraIncioDiasDiferentesOrdenCorrecto();

        prioridadContenidoService.update(persona, prioridadContenido);

        verify(prioridadContenidoDAO).update(prioridadContenido);
    }

    @Test(expected = FechaFinPrioridadSuperiorAFechaInicioException.class)
    public void ocurreUnErrorAlInsertarUnaPrioridadConMismaHoraInicioYFinEnMismoDia()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setMismaHoraFinEIncioEnMismoDia();

        prioridadContenidoService.insert(persona, prioridadContenido);
    }

    @Test(expected = FechaFinPrioridadSuperiorAFechaInicioException.class)
    public void ocurreUnErrorAlActualizarUnaPrioridadConMismaHoraInicioYFinEnMismoDia()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            ActualizadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setMismaHoraFinEIncioEnMismoDia();

        prioridadContenidoService.update(persona, prioridadContenido);
    }

    @Test(expected = FechaFinPrioridadSuperiorAFechaInicioException.class)
    public void ocurreUnErrorAlInsertarUnaPrioridadSinHoraInicioYHoraFinIgualA0000ElMismoDia()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setHorafinA0000SinHoraInicioElMismoDia();

        prioridadContenidoService.insert(persona, prioridadContenido);
    }

    @Test(expected = FechaFinPrioridadSuperiorAFechaInicioException.class)
    public void ocurreUnErrorAlActualizarUnaPrioridadSinHoraInicioYHoraFinIgualA0000ElMismoDia()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            ActualizadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setHorafinA0000SinHoraInicioElMismoDia();

        prioridadContenidoService.update(persona, prioridadContenido);
    }

    @Test
    public void alInsertarUnaPrioridadSinHoraInicioYHoraFinSuperiorA0000ElMismoDiaSeActualizaElContenido()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setHoraFinSuperiorA0000SinHoraInicioElMismoDia();

        prioridadContenidoService.insert(persona, prioridadContenido);

        verify(prioridadContenidoDAO).insert(prioridadContenido);
    }

    @Test
    public void alActualizarUnaPrioridadSinHoraInicioYHoraFinSuperiorA0000ElMismoDiaSeActualizaElContenido()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            ActualizadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setHoraFinSuperiorA0000SinHoraInicioElMismoDia();

        prioridadContenidoService.update(persona, prioridadContenido);

        verify(prioridadContenidoDAO).update(prioridadContenido);
    }

    @Test
    public void alInsertarUnaPrioridadSinHorafinYHoraInicioAnteriorA2359ElMismoDiaSeActualizaElContenido()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setHoraInicioAnteriorA2359SinHoraFinElMismoDia();

        prioridadContenidoService.insert(persona, prioridadContenido);

        verify(prioridadContenidoDAO).insert(prioridadContenido);
    }

    @Test
    public void alActualizarUnaPrioridadSinHorafinYHoraInicioAnteriorA2359ElMismoDiaSeActualizaElContenido()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            ActualizadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setHoraInicioAnteriorA2359SinHoraFinElMismoDia();

        prioridadContenidoService.update(persona, prioridadContenido);

        verify(prioridadContenidoDAO).update(prioridadContenido);
    }

    @Test(expected = FechaFinPrioridadSuperiorAFechaInicioException.class)
    public void ocurreUnErrorAlActualizarUnaPrioridadSinFechaFinYConHoraInicioA2359elMismoDia()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            ActualizadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setHoraInicioA2359SinHoraFinElMismoDia();

        prioridadContenidoService.update(persona, prioridadContenido);
    }

    @Test(expected = FechaFinPrioridadSuperiorAFechaInicioException.class)
    public void ocurreUnErrorAlInsertarUnaPrioridadSinFechaFinYConHoraInicioA2359elMismoDia()
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        setHoraInicioA2359SinHoraFinElMismoDia();

        prioridadContenidoService.insert(persona, prioridadContenido);
    }

    private void setFechasNulas()
    {
        prioridadContenido.setFechaInicio(null);
        prioridadContenido.setFechaFin(null);
    }

    private void setFechasConFechaFinInferiorOIgualAFechaInicio()
    {
        fechaInicio.set(2011, 8, 2);
        fechaFin.set(2011, 8, 1);

        prioridadContenido.setFechaInicio(fechaInicio.getTime());
        prioridadContenido.setFechaFin(fechaFin.getTime());
    }

    private void setHorasMismoDiaOrdenIncorrecto()
    {
        fechaInicio.set(2011, 8, 2);
        horaInicio.set(2011, 1, 1, 13, 0);
        fechaFin.set(2011, 8, 2);
        horaFin.set(2011, 1, 1, 12, 0);

        prioridadContenido.setFechaInicio(fechaInicio.getTime());
        prioridadContenido.setHoraInicio(horaInicio.getTime());
        prioridadContenido.setFechaFin(fechaFin.getTime());
        prioridadContenido.setHoraFin(horaFin.getTime());
    }

    private void setHorasMismoDiaOrdenCorrecto()
    {
        fechaInicio.set(2011, 8, 2);
        horaInicio.set(2011, 1, 1, 12, 0);
        fechaFin.set(2011, 8, 2);
        horaFin.set(2011, 1, 1, 12, 05);

        prioridadContenido.setFechaInicio(fechaInicio.getTime());
        prioridadContenido.setHoraInicio(horaInicio.getTime());
        prioridadContenido.setFechaFin(fechaFin.getTime());
        prioridadContenido.setHoraFin(horaFin.getTime());
    }

    private void setHorasConsecutivasEnDiferentesDiasOrdenCorrecto()
    {
        fechaInicio.set(2011, 8, 2);
        horaInicio.set(2011, 1, 1, 12, 0);
        fechaFin.set(2011, 10, 2);
        horaFin.set(2011, 1, 1, 12, 05);

        prioridadContenido.setFechaInicio(fechaInicio.getTime());
        prioridadContenido.setHoraInicio(horaInicio.getTime());
        prioridadContenido.setFechaFin(fechaFin.getTime());
        prioridadContenido.setHoraFin(horaFin.getTime());
    }

    private void setHoraFinAnteriorOIgualAHoraIncioDiasDiferentesOrdenCorrecto()
    {
        fechaInicio.set(2011, 8, 2);
        horaInicio.set(2011, 1, 1, 12, 0);
        fechaFin.set(2011, 10, 2);
        horaFin.set(2011, 1, 1, 11, 05);

        prioridadContenido.setFechaInicio(fechaInicio.getTime());
        prioridadContenido.setHoraInicio(horaInicio.getTime());
        prioridadContenido.setFechaFin(fechaFin.getTime());
        prioridadContenido.setHoraFin(horaFin.getTime());
    }

    private void setMismaHoraFinEIncioEnMismoDia()
    {
        fechaInicio.set(2011, 8, 2);
        horaInicio.set(2011, 1, 1, 12, 00);
        fechaFin.set(2011, 8, 2);
        horaFin.set(2011, 1, 1, 12, 00);

        prioridadContenido.setFechaInicio(fechaInicio.getTime());
        prioridadContenido.setHoraInicio(horaInicio.getTime());
        prioridadContenido.setFechaFin(fechaFin.getTime());
        prioridadContenido.setHoraFin(horaFin.getTime());
    }

    private void setFechasConFinSuperiorAInicio()
    {
        fechaInicio.set(2011, 8, 1);
        fechaFin.set(2011, 8, 2);

        prioridadContenido.setFechaInicio(fechaInicio.getTime());
        prioridadContenido.setFechaFin(fechaFin.getTime());
    }

    private void setFechasDeInicioYFinIguales()
    {
        fechaInicio.set(2011, 8, 2);
        fechaFin.set(2011, 8, 2);

        prioridadContenido.setFechaInicio(fechaInicio.getTime());
        prioridadContenido.setFechaFin(fechaFin.getTime());
    }

    private void setHorafinA0000SinHoraInicioElMismoDia()
    {
        fechaInicio.set(2011, 8, 2);
        fechaFin.set(2011, 8, 2);
        horaFin.set(2011, 1, 1, 00, 00);

        prioridadContenido.setFechaInicio(fechaInicio.getTime());
        prioridadContenido.setFechaFin(fechaFin.getTime());
        prioridadContenido.setHoraFin(horaFin.getTime());
    }

    private void setHoraFinSuperiorA0000SinHoraInicioElMismoDia()
    {
        fechaInicio.set(2011, 8, 2);
        fechaFin.set(2011, 8, 2);
        horaFin.set(2011, 1, 1, 00, 05);

        prioridadContenido.setFechaInicio(fechaInicio.getTime());
        prioridadContenido.setFechaFin(fechaFin.getTime());
        prioridadContenido.setHoraFin(horaFin.getTime());
    }

    private void setHoraInicioAnteriorA2359SinHoraFinElMismoDia()
    {
        fechaInicio.set(2011, 8, 2);
        horaInicio.set(2011, 1, 1, 23, 00);
        fechaFin.set(2011, 8, 2);

        prioridadContenido.setFechaInicio(fechaInicio.getTime());
        prioridadContenido.setHoraInicio(horaInicio.getTime());
        prioridadContenido.setFechaFin(fechaFin.getTime());
    }

    private void setHoraInicioA2359SinHoraFinElMismoDia()
    {
        fechaInicio.set(2011, 8, 2);
        horaInicio.set(2011, 1, 1, 23, 59);
        fechaFin.set(2011, 8, 2);

        prioridadContenido.setFechaInicio(fechaInicio.getTime());
        prioridadContenido.setHoraInicio(horaInicio.getTime());
        prioridadContenido.setFechaFin(fechaFin.getTime());
    }
}