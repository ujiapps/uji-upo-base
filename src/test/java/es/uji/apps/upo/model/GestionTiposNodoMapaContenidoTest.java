package es.uji.apps.upo.model;

import es.uji.apps.upo.dao.ContenidoDAO;
import es.uji.apps.upo.dao.NodoMapaContenidoDAO;
import es.uji.apps.upo.dao.NodoMapaDAO;
import es.uji.apps.upo.exceptions.ContenidoConMismoUrlPathException;
import es.uji.apps.upo.exceptions.ContenidoYaPropuestoException;
import es.uji.apps.upo.exceptions.InsertadoContenidoNoAutorizadoException;
import es.uji.apps.upo.exceptions.NodoMapaContenidoDebeTenerUnContenidoNormalException;
import es.uji.apps.upo.model.enums.TipoReferenciaContenido;
import es.uji.apps.upo.services.ContenidoService;
import es.uji.apps.upo.services.NodoMapaContenidoService;
import es.uji.apps.upo.services.NotificacionService;
import es.uji.apps.upo.services.PersonaService;
import es.uji.apps.upo.services.nodomapa.NodoMapaService;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

public class GestionTiposNodoMapaContenidoTest
{
    private static final long CONTENIDO_ID = 1L;
    private static final long NODO_MAPA_CONTENIDO_ID = 1L;
    private static final long MAPA_ID = 1L;
    private static final long MAPA_ORIGEN_ID = 2L;

    private NodoMapaContenido nodoMapaContenido;
    private NodoMapaContenido nodoMapaContenidoOrigen;

    private NodoMapaDAO nodoMapaDAO;
    private NodoMapaContenidoDAO nodoMapaContenidoDAO;
    private NodoMapa nodoMapa;
    private Contenido contenido;
    private NodoMapaContenidoService nodoMapaContenidoService;
    private NodoMapaService nodoMapaService;
    private ContenidoService contenidoService;

    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    @Before
    public void init()
    {
        nodoMapa = mock(NodoMapa.class);
        contenido = mock(Contenido.class);

        when(contenido.getId()).thenReturn(CONTENIDO_ID);

        initDAOOperations();
        initNodoMapaContenido();

        nodoMapaContenido.setUpoObjeto(contenido);

        nodoMapaContenidoService =
                new NodoMapaContenidoService(mock(PersonaService.class), nodoMapaContenidoDAO, mock(ContenidoDAO.class),
                        mock(NotificacionService.class), mock(NodoMapaDAO.class));

        nodoMapaContenidoService.setContenidoService(contenidoService);

        when(nodoMapaContenidoDAO.getNodoMapaContenidoByContenidoIdAndMapaId(CONTENIDO_ID, MAPA_ORIGEN_ID)).thenReturn(
                nodoMapaContenidoOrigen);
        when(nodoMapaContenidoOrigen.isNormal()).thenReturn(true);
    }

    private void initDAOOperations()
    {
        nodoMapaDAO = mock(NodoMapaDAO.class);
        nodoMapaContenidoDAO = mock(NodoMapaContenidoDAO.class);
        nodoMapaContenidoOrigen = mock(NodoMapaContenido.class);
        nodoMapaService = mock(NodoMapaService.class);
        contenidoService = mock(ContenidoService.class);

        new NodoMapa().setNodoMapaDAO(nodoMapaDAO);

        when(nodoMapaContenidoDAO.getNodoMapaContenidoByContenidoIdAndMapaId(anyLong(), anyLong())).thenReturn(null);
        when(nodoMapaService.getNodoMapa(MAPA_ID, false)).thenReturn(nodoMapa);
        when(contenidoService.getContenido(CONTENIDO_ID)).thenReturn(new Contenido());
    }

    private void initNodoMapaContenido()
    {
        nodoMapaContenido = new NodoMapaContenido();
        nodoMapaContenido.setUpoMapa(nodoMapa);
        nodoMapaContenido.setUpoObjeto(contenido);
    }

    @Test(expected = NodoMapaContenidoDebeTenerUnContenidoNormalException.class)
    public void ocurreUnErrorAlInsertarMasDeUnTipoNormalEnNodoMapaContenidoParaElMismoContenido()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoYaPropuestoException,
            ContenidoConMismoUrlPathException, InsertadoContenidoNoAutorizadoException
    {
        initNodoMapaContenidoDeTipoNormalConIdConValor();

        nodoMapaContenido.setTipo(TipoReferenciaContenido.NORMAL);

        nodoMapaContenidoService.insertContenidoAModerar(nodoMapaContenido, nodoMapaService.getNodoMapa(MAPA_ID, false),
                CONTENIDO_ID, NODO_MAPA_CONTENIDO_ID);
    }

    @Test(expected = NodoMapaContenidoDebeTenerUnContenidoNormalException.class)
    public void ocurreUnErrorAlInsertarTipoLinkSiNoHayTipoNormalEnNodoMapaContenidoParaElMismoContenido()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoYaPropuestoException,
            ContenidoConMismoUrlPathException, InsertadoContenidoNoAutorizadoException
    {
        initNodoMapaContenidoDeTipoNormalConIdSinValor();

        nodoMapaContenido.setTipo(TipoReferenciaContenido.LINK);

        nodoMapaContenidoService.insertContenidoAModerar(nodoMapaContenido, nodoMapaService.getNodoMapa(MAPA_ID, false),
                CONTENIDO_ID, NODO_MAPA_CONTENIDO_ID);
    }

    @Test
    public void seInsertaUnNodoMapaContenidoDeTipoNormalSiNoExisteOtroDeTipoNormalParaElMismoContenido()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoYaPropuestoException,
            ContenidoConMismoUrlPathException, InsertadoContenidoNoAutorizadoException
    {
        initNodoMapaContenidoDeTipoNormalConIdSinValor();

        nodoMapaContenido.setTipo(TipoReferenciaContenido.NORMAL);

        nodoMapaContenidoService.insertContenidoAModerar(nodoMapaContenido, nodoMapaService.getNodoMapa(MAPA_ID, false),
                CONTENIDO_ID, NODO_MAPA_CONTENIDO_ID);

        verify(nodoMapaContenidoDAO).insert(nodoMapaContenido);
    }

    @Test
    public void seInsertaUnNodoMapaContenidoDeTipoLinkSiExisteOtroDeTipoNormalParaElMismoContenido()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoYaPropuestoException,
            ContenidoConMismoUrlPathException, InsertadoContenidoNoAutorizadoException
    {
        initNodoMapaContenidoDeTipoNormalConIdConValor();
        nodoMapaContenido.setTipo(TipoReferenciaContenido.LINK);

        nodoMapaContenidoService.insertContenidoAModerar(nodoMapaContenido, nodoMapaService.getNodoMapa(MAPA_ID, false),
                CONTENIDO_ID, NODO_MAPA_CONTENIDO_ID);

        verify(nodoMapaContenidoDAO).insert(nodoMapaContenido);
    }

    private void initNodoMapaContenidoDeTipoNormalConIdConValor()
    {
        NodoMapaContenido nodoMapaContenido = new NodoMapaContenido();
        nodoMapaContenido.setId(CONTENIDO_ID);

        when(nodoMapaContenidoDAO.getNodoMapaContenidoTipoNormalByContenidoId(anyLong())).thenReturn(nodoMapaContenido);
    }

    private void initNodoMapaContenidoDeTipoNormalConIdSinValor()
    {
        NodoMapaContenido nodoMapaContenido = new NodoMapaContenido();
        nodoMapaContenido.setId(null);

        when(nodoMapaContenidoDAO.getNodoMapaContenidoTipoNormalByContenidoId(anyLong())).thenReturn(nodoMapaContenido);
    }
}