package es.uji.apps.upo.model;

import es.uji.apps.upo.dao.ContenidoDAO;
import es.uji.apps.upo.dao.NodoMapaDAO;
import es.uji.apps.upo.exceptions.BorradoContenidoNoAutorizadoException;
import es.uji.apps.upo.exceptions.InsertadaPlantillaDelMismoTipoException;
import es.uji.apps.upo.exceptions.InsertarPlantillaNoAutorizadoException;
import es.uji.apps.upo.exceptions.UsuarioNoAutenticadoException;
import es.uji.apps.upo.services.*;
import es.uji.apps.upo.services.nodomapa.NodoMapaService;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class NodoMapaContenidoCRUDTest
{
    private static final String URL = "/una/url/aleatoria/";
    private static final Long PLANTILLA_ID = 1L;
    private static final Integer DAFAULT_LEVEL = 1;
    private static final Long NODO_MAPA_ID = 1L;

    private NodoMapa nodoMapa;
    private Persona personaMock;
    private NodoMapaDAO nodoMapaDAOMock;
    private Plantilla plantilla;
    private PersonaService personaServiceMock;
    private NodoMapaService nodoMapaService;

    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    @Before
    public void initData()
    {
        personaMock = mock(Persona.class);
        personaServiceMock = mock(PersonaService.class);
        nodoMapaDAOMock = mock(NodoMapaDAO.class);

        plantilla = new Plantilla();
        plantilla.setId(PLANTILLA_ID);

        nodoMapa = new NodoMapa();
        nodoMapa.setId(NODO_MAPA_ID);
        nodoMapa.setUrlCompleta(URL);
        nodoMapa.setNodoMapaDAO(nodoMapaDAOMock);

        nodoMapaService = new NodoMapaService(personaServiceMock, nodoMapaDAOMock,
                mock(ContenidoDAO.class), mock(NodoMapaPlantillaService.class),
                mock(IdiomaService.class),  mock(NotificacionService.class));
    }

    @Test
    public void elAdministradorDeUnNodoPuedeEliminarPlantillas()
            throws BorradoContenidoNoAutorizadoException
    {
        when(personaServiceMock.isNodoMapaEditable(nodoMapa.getUrlCompleta(), personaMock))
                .thenReturn(true);

        nodoMapaService.deletePlantilla(personaMock, nodoMapa, PLANTILLA_ID);

        verify(personaServiceMock).isNodoMapaEditable(URL, personaMock);
        verify(nodoMapaDAOMock).deletePlantilla(NODO_MAPA_ID, PLANTILLA_ID);
    }

    @Test(expected = BorradoContenidoNoAutorizadoException.class)
    public void exceptionCuandoUnUsuarioNoAdministradorBorraUnaPlantilla()
            throws BorradoContenidoNoAutorizadoException
    {
        when(personaServiceMock.isNodoMapaEditable(nodoMapa.getUrlCompleta(), personaMock))
                .thenReturn(false);

        nodoMapaService.deletePlantilla(personaMock, nodoMapa, PLANTILLA_ID);

        verify(personaServiceMock).isNodoMapaEditable(URL, personaMock);
        verify(nodoMapaDAOMock, times(0)).deletePlantilla(NODO_MAPA_ID, PLANTILLA_ID);
    }

    @Test
    public void elAdministradorDeUnNodoPuedeInsertarPlantillas()
            throws UsuarioNoAutenticadoException, InsertarPlantillaNoAutorizadoException,
            InsertadaPlantillaDelMismoTipoException
    {
        when(personaServiceMock.isNodoMapaEditable(nodoMapa.getUrlCompleta(), personaMock))
                .thenReturn(true);

        nodoMapaService.insertarPlantilla(personaMock, plantilla, nodoMapa, DAFAULT_LEVEL);

        verify(personaServiceMock).isNodoMapaEditable(URL, personaMock);
        verify(nodoMapaDAOMock).insertarPlantilla(NODO_MAPA_ID, PLANTILLA_ID, DAFAULT_LEVEL);
    }

    @Test(expected = InsertarPlantillaNoAutorizadoException.class)
    public void exceptionCuandoUnUsuarioNoAdministradorInsertaUnaPlantilla()
            throws UsuarioNoAutenticadoException, InsertarPlantillaNoAutorizadoException,
            InsertadaPlantillaDelMismoTipoException
    {
        when(personaServiceMock.isNodoMapaEditable(nodoMapa.getUrlCompleta(), personaMock))
                .thenReturn(false);

        nodoMapaService.insertarPlantilla(personaMock, plantilla, nodoMapa, DAFAULT_LEVEL);

        verify(personaServiceMock).isNodoMapaEditable(URL, personaMock);
        verify(nodoMapaDAOMock, times(0)).insertarPlantilla(NODO_MAPA_ID, PLANTILLA_ID,
                DAFAULT_LEVEL);
    }
}
