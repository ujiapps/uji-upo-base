package es.uji.apps.upo.model;

import es.uji.apps.upo.dao.*;
import es.uji.apps.upo.exceptions.ActualizadoContenidoNoAutorizadoException;
import es.uji.apps.upo.exceptions.ContenidoConMismoUrlPathException;
import es.uji.apps.upo.exceptions.InsertadoContenidoNoAutorizadoException;
import es.uji.apps.upo.model.enums.IdiomaPublicacion;
import es.uji.apps.upo.services.*;
import es.uji.apps.upo.solr.ContenidoSolr;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class ContenidoTest {
    private static final String URL = "/una/url/aleatoria/";
    private static final Long PERSONA_ID = 1L;
    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();
    private Contenido contenido;
    private ContenidoDAO contenidoDAO;
    private NodoMapaContenidoDAO nodoMapaContenidoDAO;
    private NodoMapaDAO nodoMapaDAO;
    private Persona persona;
    private NodoMapa nodoMapa;
    private ContenidoService contenidoService;
    private PersonaService personaService;

    @Before
    public void init() {
        nodoMapa = mock(NodoMapa.class);
        persona = mock(Persona.class);
        personaService = mock(PersonaService.class);
        contenidoDAO = mock(ContenidoDAO.class);
        nodoMapaContenidoDAO = mock(NodoMapaContenidoDAO.class);
        nodoMapaDAO = mock(NodoMapaDAO.class);

        contenido = new Contenido();
        contenido.setNodoMapaContenidoDAO(nodoMapaContenidoDAO);

        contenidoService =
                new ContenidoService(contenidoDAO, mock(ContenidoIdiomaDAO.class), mock(ContenidoVigenciaDAO.class),
                        nodoMapaContenidoDAO, mock(TagDAO.class), mock(AutoguardadoDAO.class), personaService,
                        mock(AutoguardadoService.class), mock(IdiomaService.class),
                        mock(ContenidoIdiomaAtributoService.class), mock(ContenidoIdiomaRecursoService.class),
                        mock(PrioridadContenidoDAO.class), mock(AtributoMetadatoService.class));

        contenidoService.setNodoMapaDAO(nodoMapaDAO);

        when(nodoMapa.getUrlCompleta()).thenReturn(URL);
        when(persona.getId()).thenReturn(PERSONA_ID);
        when(nodoMapaContenidoDAO.getNodoMapaTipoNormalByContenidoId(anyLong())).thenReturn(nodoMapa);
    }

    @Test
    public void ElAdministradorDeUnNodoPuedeActualizarUncontenido()
            throws ActualizadoContenidoNoAutorizadoException, ContenidoConMismoUrlPathException {
        when(personaService.isNodoMapaEditable(nodoMapa.getUrlCompleta(), persona)).thenReturn(true);

        contenido.setId(1L);
        contenidoService.update(contenido, persona, nodoMapa);

        verify(contenidoDAO).update(contenido, PERSONA_ID);
        verify(personaService).isNodoMapaEditable(anyString(), any(Persona.class));
    }

    @Test(expected = ActualizadoContenidoNoAutorizadoException.class)
    public void ocurreUnErrorCuandoUnUsuarioNoAdministradorActualizaUnContenidoNoAutorizado()
            throws ActualizadoContenidoNoAutorizadoException, ContenidoConMismoUrlPathException {
        when(personaService.isNodoMapaEditable(nodoMapa.getUrlCompleta(), persona)).thenReturn(false);

        contenidoService.update(contenido, persona, nodoMapa);

        verify(contenidoDAO).update(contenido);
        verify(contenidoDAO, times(0)).update(contenido);
    }

    @Test
    public void ElAdministradorDeUnNodoPuedeInsertarUncontenido()
            throws InsertadoContenidoNoAutorizadoException, ContenidoConMismoUrlPathException {
        when(personaService.isNodoMapaEditable(nodoMapa.getUrlCompleta(), persona)).thenReturn(true);

        contenidoService.insert(contenido, persona, nodoMapa);

        verify(contenidoDAO).insert(contenido, PERSONA_ID);
        verify(personaService).isNodoMapaEditable(anyString(), any(Persona.class));
    }

    @Test(expected = ActualizadoContenidoNoAutorizadoException.class)
    public void ocurreUnErrorCuandoUnUsuarioNoAdministradorInsertaUnContenidoNoAutorizado()
            throws ActualizadoContenidoNoAutorizadoException, ContenidoConMismoUrlPathException {
        when(personaService.isNodoMapaEditable(nodoMapa.getUrlCompleta(), persona)).thenReturn(false);

        contenidoService.update(contenido, persona, nodoMapa);

        verify(contenidoDAO).insert(contenido);
        verify(contenidoDAO, times(0)).insert(contenido);
    }

    @Test
    public void objetosGestionanLasFechasVigencia() {
        Contenido objeto = new Contenido();
        objeto.addFechaVigencia(new Date());

        assertNotNull(objeto.getUpoVigenciasObjetos());
        assertTrue(objeto.getUpoVigenciasObjetos().size() > 0);
    }

    @Test
    public void objetoVigenteSiFechasVigenciaContienenFechaActual() {
        Calendar fechaAntigua1 = Calendar.getInstance();
        fechaAntigua1.set(2011, 0, 1);

        Calendar fechaAntigua2 = Calendar.getInstance();
        fechaAntigua2.set(2011, 1, 1);

        Calendar fechaSuperiorAHoy = Calendar.getInstance();
        fechaSuperiorAHoy.set(fechaAntigua1.get(Calendar.YEAR) + 100, 1, 1);

        Contenido objeto = new Contenido();
        objeto.addFechaVigencia(fechaAntigua1.getTime());
        objeto.addFechaVigencia(fechaAntigua2.getTime());
        objeto.addFechaVigencia(fechaSuperiorAHoy.getTime());

        assertTrue(objeto.isVigente());
    }

    @Test
    public void objetoNOVigenteSiFechasVigenciaNoContienenFechaActual() {
        Calendar fecha1 = Calendar.getInstance();
        fecha1.set(2011, 0, 1);

        Calendar fecha2 = Calendar.getInstance();
        fecha2.set(2011, 1, 1);

        Calendar fecha3 = Calendar.getInstance();
        fecha3.set(2011, 2, 1);

        Contenido objeto = new Contenido();
        objeto.addFechaVigencia(fecha1.getTime());
        objeto.addFechaVigencia(fecha2.getTime());
        objeto.addFechaVigencia(fecha3.getTime());

        assertFalse(objeto.isVigente());
    }

    @Test
    public void contentConverstionToSolrContentShouldPreserveFields()
            throws Exception {
        Set<ContenidoIdioma> contenidosIdioma = new HashSet<ContenidoIdioma>();

        ContenidoIdioma ca = new ContenidoIdioma();
        ca.setTitulo("Prova");
        Idioma idioma = new Idioma();
        idioma.setCodigoISO("CA");
        ca.setUpoIdioma(idioma);
        contenidosIdioma.add(ca);

        Contenido contenido = new Contenido();
        contenido.setId(1L);
        contenido.setUrlPath("infoest");
        contenido.setContenidoIdiomas(Collections.singleton(ca));

        ca.setUpoObjeto(contenido);

        List<ContenidoSolr> solrContentObjects = contenido.toSolrContent(contenido.getUrlCompletaNodoMapa());

        assertEquals(1, solrContentObjects.size());
        assertEquals("Prova", solrContentObjects.get(0).getTitulo());
        assertEquals(IdiomaPublicacion.CA, solrContentObjects.get(0).getIdioma());
    }
}
