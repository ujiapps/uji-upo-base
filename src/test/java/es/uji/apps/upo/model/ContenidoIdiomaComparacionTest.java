package es.uji.apps.upo.model;

import static junit.framework.Assert.assertEquals;

import org.junit.Rule;
import org.junit.Test;

import es.uji.apps.upo.model.enums.TipoFormatoContenido;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;

public class ContenidoIdiomaComparacionTest
{
    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    @Test
    public void laFuncionDeComparacionDeContenidoIdiomaDebeDevolverFalseSiLosContenidosDeTipoStringCambian()
            throws Exception
    {
        ContenidoIdioma contenidoIdioma = new ContenidoIdioma();

        String titulo = "";
        String subtitulo = "";
        String tituloLargo = "";
        String isHtml = "S";
        String resumen = "";
        String enlaceDestino = "";
        String tipoFormato = TipoFormatoContenido.PAGINA.toString();

        boolean resultado = contenidoIdioma.esIgual(titulo, subtitulo, tituloLargo, isHtml, null,
                null, null, null, resumen, enlaceDestino, tipoFormato);
        assertEquals(true, resultado);

        titulo = "cambiado";

        resultado = contenidoIdioma.esIgual(titulo, subtitulo, tituloLargo, isHtml, null, null,
                null, null, resumen, enlaceDestino, tipoFormato);
        assertEquals(false, resultado);

        contenidoIdioma.setTitulo(titulo);
        resultado = contenidoIdioma.esIgual(titulo, subtitulo, tituloLargo, isHtml, null, null,
                null, null, resumen, enlaceDestino, tipoFormato);
        assertEquals(true, resultado);

        subtitulo = "cambiado";

        resultado = contenidoIdioma.esIgual(titulo, subtitulo, tituloLargo, isHtml, null, null,
                null, null, resumen, enlaceDestino, tipoFormato);
        assertEquals(false, resultado);

        contenidoIdioma.setSubtitulo(subtitulo);
        resultado = contenidoIdioma.esIgual(titulo, subtitulo, tituloLargo, isHtml, null, null,
                null, null, resumen, enlaceDestino, tipoFormato);
        assertEquals(true, resultado);

        tituloLargo = "cambiado";

        resultado = contenidoIdioma.esIgual(titulo, subtitulo, tituloLargo, isHtml, null, null,
                null, null, resumen, enlaceDestino, tipoFormato);
        assertEquals(false, resultado);

        contenidoIdioma.setTituloLargo(tituloLargo);
        resultado = contenidoIdioma.esIgual(titulo, subtitulo, tituloLargo, isHtml, null, null,
                null, null, resumen, enlaceDestino, tipoFormato);
        assertEquals(true, resultado);

        resumen = "cambiado";

        resultado = contenidoIdioma.esIgual(titulo, subtitulo, tituloLargo, isHtml, null, null,
                null, null, resumen, enlaceDestino, tipoFormato);
        assertEquals(false, resultado);

        contenidoIdioma.setResumen(resumen);
        resultado = contenidoIdioma.esIgual(titulo, subtitulo, tituloLargo, isHtml, null, null,
                null, null, resumen, enlaceDestino, tipoFormato);
        assertEquals(true, resultado);

        isHtml = "N";

        resultado = contenidoIdioma.esIgual(titulo, subtitulo, tituloLargo, isHtml, null, null,
                null, null, resumen, enlaceDestino, tipoFormato);
        assertEquals(false, resultado);

        contenidoIdioma.setHtml(isHtml);
        resultado = contenidoIdioma.esIgual(titulo, subtitulo, tituloLargo, isHtml, null, null,
                null, null, resumen, enlaceDestino, tipoFormato);
        assertEquals(true, resultado);

        enlaceDestino = "cambiado";

        resultado = contenidoIdioma.esIgual(titulo, subtitulo, tituloLargo, isHtml, null, null,
                null, null, resumen, enlaceDestino, tipoFormato);
        assertEquals(false, resultado);

        contenidoIdioma.setEnlaceDestino(enlaceDestino);
        resultado = contenidoIdioma.esIgual(titulo, subtitulo, tituloLargo, isHtml, null, null,
                null, null, resumen, enlaceDestino, tipoFormato);
        assertEquals(true, resultado);

        tipoFormato = "cambiado";

        resultado = contenidoIdioma.esIgual(titulo, subtitulo, tituloLargo, isHtml, null, null,
                null, null, resumen, enlaceDestino, tipoFormato);
        assertEquals(false, resultado);

        contenidoIdioma.setTipoFormato(tipoFormato);
        resultado = contenidoIdioma.esIgual(titulo, subtitulo, tituloLargo, isHtml, null, null,
                null, null, resumen, enlaceDestino, tipoFormato);
        assertEquals(true, resultado);
    }

    @Test
    public void laFuncionDeComparacionDeContenidoIdiomaDebeDevolverFalseSiElContenidoHtmlCambia()
            throws Exception
    {
        ContenidoIdioma contenidoIdioma = new ContenidoIdioma();

        String isHtml = "S";
        String valorContenido = "";
        String tipoFormato = TipoFormatoContenido.PAGINA.toString();

        contenidoIdioma.setHtml(isHtml);
        contenidoIdioma.setTipoFormato(tipoFormato);

        boolean resultado = contenidoIdioma.esIgual(null, null, null, isHtml, valorContenido, null,
                null, null, null, null, tipoFormato);
        assertEquals(true, resultado);

        valorContenido = "cambiado";

        resultado = contenidoIdioma.esIgual(null, null, null, isHtml, valorContenido, null, null,
                null, null, null, tipoFormato);
        assertEquals(false, resultado);

        contenidoIdioma.setContenido(valorContenido.getBytes());
        resultado = contenidoIdioma.esIgual(null, null, null, isHtml, valorContenido, null, null,
                null, null, null, tipoFormato);
        assertEquals(true, resultado);

        valorContenido = "cambiadoDeNuevo";

        resultado = contenidoIdioma.esIgual(null, null, null, isHtml, valorContenido, null, null,
                null, null, null, tipoFormato);
        assertEquals(false, resultado);

        contenidoIdioma.setContenido(valorContenido.getBytes());
        resultado = contenidoIdioma.esIgual(null, null, null, isHtml, valorContenido, null, null,
                null, null, null, tipoFormato);
        assertEquals(true, resultado);
    }

    @Test
    public void laFuncionDeComparacionDeContenidoIdiomaDebeDevolverFalseSiElFicheroCambia()
            throws Exception
    {
        ContenidoIdioma contenidoIdioma = new ContenidoIdioma();

        String isHtml = "N";
        String tipoMimeFichero = "";
        String nombreFichero = "";
        String tipoFormato = TipoFormatoContenido.BINARIO.toString();

        contenidoIdioma.setHtml(isHtml);
        contenidoIdioma.setTipoFormato(tipoFormato);

        byte[] dataBinary = { new Byte((byte) 1), new Byte((byte) 2) };

        boolean resultado = contenidoIdioma.esIgual(null, null, null, isHtml, null,
                tipoMimeFichero, dataBinary, nombreFichero, null, null, tipoFormato);
        assertEquals(false, resultado);

        contenidoIdioma.setContenido(dataBinary);
        resultado = contenidoIdioma.esIgual(null, null, null, isHtml, null, tipoMimeFichero,
                dataBinary, nombreFichero, null, null, tipoFormato);
        assertEquals(true, resultado);

        tipoMimeFichero = "cambiado";

        resultado = contenidoIdioma.esIgual(null, null, null, isHtml, null, tipoMimeFichero,
                dataBinary, nombreFichero, null, null, tipoFormato);
        assertEquals(false, resultado);

        contenidoIdioma.setMimeType(tipoMimeFichero);
        resultado = contenidoIdioma.esIgual(null, null, null, isHtml, null, tipoMimeFichero,
                dataBinary, nombreFichero, null, null, tipoFormato);
        assertEquals(true, resultado);

        nombreFichero = "cambiado";

        resultado = contenidoIdioma.esIgual(null, null, null, isHtml, null, tipoMimeFichero,
                dataBinary, nombreFichero, null, null, tipoFormato);
        assertEquals(false, resultado);

        contenidoIdioma.setNombreFichero(nombreFichero);
        resultado = contenidoIdioma.esIgual(null, null, null, isHtml, null, tipoMimeFichero,
                dataBinary, nombreFichero, null, null, tipoFormato);
        assertEquals(true, resultado);

        byte[] dataBinaryNuevo = { new Byte((byte) 3), new Byte((byte) 4) };

        resultado = contenidoIdioma.esIgual(null, null, null, isHtml, null, tipoMimeFichero,
                dataBinaryNuevo, nombreFichero, null, null, tipoFormato);
        assertEquals(false, resultado);
    }
}
