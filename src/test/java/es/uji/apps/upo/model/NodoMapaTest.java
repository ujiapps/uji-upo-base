package es.uji.apps.upo.model;

import es.uji.apps.upo.dao.ContenidoDAO;
import es.uji.apps.upo.dao.NodoMapaDAO;
import es.uji.apps.upo.exceptions.*;
import es.uji.apps.upo.services.*;
import es.uji.apps.upo.services.nodomapa.EstructuraFechaService;
import es.uji.apps.upo.services.nodomapa.EstructuraRevistaService;
import es.uji.apps.upo.services.nodomapa.EstructuraVoxUjiService;
import es.uji.apps.upo.services.nodomapa.NodoMapaService;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Collections;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class NodoMapaTest
{
    private Persona persona = mock(Persona.class);
    private NodoMapa nodoMapa = new NodoMapa();
    private NodoMapaPlantilla nodoMapaPlantilla = mock(NodoMapaPlantilla.class);

    private NodoMapaDAO nodoMapaDAO = mock(NodoMapaDAO.class);
    private ContenidoDAO contenidoDAO = mock(ContenidoDAO.class);

    private PersonaService personaService;
    private NodoMapaService nodoMapaService;
    private EstructuraRevistaService estructuraRevistaService;
    private EstructuraVoxUjiService estructuraVoxujiService;
    private EstructuraFechaService estructuraFechaService;
    private NodoMapaPlantillaService nodoMapaPlantillaService = mock(NodoMapaPlantillaService.class);
    private IdiomaService idiomaService = mock(IdiomaService.class);

    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    @Before
    public void init()
    {
        personaService = mock(PersonaService.class);

        new NodoMapa().setNodoMapaDAO(nodoMapaDAO);

        nodoMapa.setNodoMapaDAO(nodoMapaDAO);
        nodoMapa.setUpoMapasPlantillas(Collections.singleton(nodoMapaPlantilla));
        nodoMapaService =
                new NodoMapaService(personaService, nodoMapaDAO, contenidoDAO, nodoMapaPlantillaService, idiomaService,
                        mock(NotificacionService.class));
        estructuraRevistaService =
                new EstructuraRevistaService(nodoMapaService, mock(PlantillaService.class), nodoMapaDAO,
                        personaService);
        estructuraFechaService =
                new EstructuraFechaService(nodoMapaService, mock(PlantillaService.class), nodoMapaPlantillaService,
                        nodoMapaDAO, personaService);
        estructuraVoxujiService =
                new EstructuraVoxUjiService(nodoMapaService, mock(PlantillaService.class), nodoMapaDAO, personaService);
    }

    @Test
    public void ElAdministradorDeUnNodoPuedeInsertarUnNodoMapa()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(true);

        NodoMapa nodoMapaPadre = new NodoMapa();
        nodoMapaPadre.setId(1L);
        nodoMapa.setUpoMapa(nodoMapaPadre);

        nodoMapaService.insert(persona, nodoMapa);

        verify(nodoMapaDAO).insert(nodoMapa);
        verify(personaService).isNodoMapaEditable(anyString(), any(Persona.class));
    }

    @Test(expected = InsertadoContenidoNoAutorizadoException.class)
    public void ocurreUnErrorCuandoUnUsuarioNoAdministradorInsertaUnNodoMapaNoAutorizado()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(false);

        NodoMapa nodoMapaPadre = new NodoMapa();
        nodoMapaPadre.setId(1L);
        nodoMapa.setUpoMapa(nodoMapaPadre);

        nodoMapaService.insert(persona, nodoMapa);

        verify(nodoMapaDAO, times(0)).insert(nodoMapa);
    }

    public void podemosBloquearUnNodoSiTenemosPermisosDeAdmin()
            throws Exception
    {
        when(personaService.isNodoMapaAdmin(anyString(), any(Long.class))).thenReturn(true);

        nodoMapaService.alternaBloqueoNodo(nodoMapa, persona);

        assertFalse(nodoMapa.isBloqueado());
    }

    @Test
    public void nodosPuedenContenerNodosJerarquicamente()
    {
        NodoMapa UpoMapaHijo = new NodoMapa("hijo");

        NodoMapa upoMapaPadre = new NodoMapa("padre");
        upoMapaPadre.addNodoRelacionado(UpoMapaHijo);

        assertNotNull(upoMapaPadre.getUpoMapas());
        assertEquals("hijo", upoMapaPadre.getUpoMapas().iterator().next().getUrlCompleta());
    }

    @Test
    public void valorUrlPorDefectoEnObjetosIgualAIndexHtml()
    {
        Contenido objeto = new Contenido();

        assertEquals("index.html", objeto.getUrlPath());
    }

    @Test
    public void nodosMapaDebenPoderAsociarseAUnaFranquicia()
    {
        Franquicia franquicia = new Franquicia();
        franquicia.setNombre("Servei d'Informàtica");

        NodoMapa nodo = new NodoMapa("noticies");
        nodo.setUpoFranquicia(franquicia);

        assertNotNull(nodo.getUpoFranquicia());
        assertEquals("Servei d'Informàtica", nodo.getUpoFranquicia().getNombre());
    }

    @Test
    public void nodosMapaEsSetDeContenidosSiDefineCriterioYNumItems()
    {
        NodoMapa nodo = new NodoMapa("noticies");
        Assert.assertFalse(nodo.isSetContenidos());

        nodo.setNumItemsSincro(new Long(5));
        Assert.assertFalse(nodo.isSetContenidos());

        Criterio criterio = new Criterio("Filtro popularidad pruebas");

        nodo.setUpoCriterio(criterio);
        assertTrue(nodo.isSetContenidos());
    }

    @Test
    public void siSeCambiaElUrlPathDeUnNodoMapaDebeCambiarSuUrlCompletaYSuUrlPath()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(true);
        when(nodoMapaDAO.update(anyObject())).thenReturn(new NodoMapa());

        NodoMapa nodoA1 = crearNodoA1();

        String nombreNuevo = "nuevo";
        nodoMapaService.actualizarUrlPathyUrlCompletaDeNodoyNodosHijo(nodoA1, nombreNuevo, persona);

        assertEquals(nombreNuevo, nodoA1.getUrlPath());
        assertEquals("/a/nuevo/", nodoA1.getUrlCompleta());
    }

    private NodoMapa crearNodoA1()
    {
        NodoMapa nodoA1 = new NodoMapa("/a/a1/");
        nodoA1.setUrlPath("a1");
        nodoA1.setId(1L);

        NodoMapa nodoMapaPadre = new NodoMapa();
        nodoMapaPadre.setId(1000L);
        nodoA1.setUpoMapa(nodoMapaPadre);

        return nodoA1;
    }

    private NodoMapa crearNodoA()
    {
        NodoMapa nodoA = new NodoMapa("/a/");
        nodoA.setUrlPath("a");
        nodoA.setId(2L);
        return nodoA;
    }

    private NodoMapa crearNodoB()
    {
        NodoMapa nodoA = new NodoMapa("/b/");
        nodoA.setUrlPath("b");
        nodoA.setId(3L);
        return nodoA;
    }

    @Test(expected = ActualizadoContenidoNoAutorizadoException.class)
    public void noSePuedeCambiarLaUrlPathDeUnNodoSiNoEresAdministrador()
            throws Exception
    {
        NodoMapa nodoA1 = crearNodoA1();

        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(false);
        when(nodoMapaDAO.update(nodoA1)).thenReturn(nodoA1);

        String nombreNuevo = "nuevo";
        nodoMapaService.actualizarUrlPathyUrlCompletaDeNodoyNodosHijo(nodoA1, nombreNuevo, persona);
    }

    @Test(expected = NodoNoSePuedeRenombrarException.class)
    public void noSePuedeCambiarLaUrlPathDeUnNodoRaiz()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(true);

        NodoMapa nodoA1 = crearNodoA1();
        nodoA1.setId(0L);

        String nombreNuevo = "nuevo";
        nodoMapaService.actualizarUrlPathyUrlCompletaDeNodoyNodosHijo(nodoA1, nombreNuevo, persona);
    }

    @Test
    public void siSeCambiaElUrlPathDeUnNodoDebeLlamarseAlaFuncioQueCambiaElUrlPathDeSusHijos()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(true);
        when(nodoMapaDAO.update(anyObject())).thenReturn(new NodoMapa());

        NodoMapa nodoA1 = crearNodoA1();
        nodoA1.setNodoMapaDAO(nodoMapaDAO);

        String nombreNuevo = "nuevo";
        nodoMapaService.actualizarUrlPathyUrlCompletaDeNodoyNodosHijo(nodoA1, nombreNuevo, persona);

        verify(nodoMapaDAO).actualizarUrlCompletaNodosHijoPorRenombrado(anyString(), anyString());
    }

    @Test
    public void siUnNodoCambiaDePadreDebeActualizarseSuUrlCompletaYTambienElNodoPadre()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(true);
        when(nodoMapaDAO.update(anyObject())).thenReturn(new NodoMapa());

        NodoMapa nodoA = crearNodoA();
        NodoMapa nodoA1 = crearNodoA1();
        NodoMapa nodoB = crearNodoB();

        nodoA1.setUpoMapa(nodoA);

        Set<NodoMapa> setNodosMapa = nodoA.getUpoMapas();
        setNodosMapa.add(nodoA1);

        nodoA1.setUpoMapas(setNodosMapa);

        nodoMapaService.actualizarNodoPadreyUrlCompletaDeNodoyNodosHijo(nodoA1, nodoB, persona);

        Assert.assertEquals("/b/a1/", nodoA1.getUrlCompleta());

        Assert.assertEquals(3L, 3L);

        Assert.assertEquals(nodoB, nodoA1.getUpoMapa());
    }

    @Test(expected = ActualizadoContenidoNoAutorizadoException.class)
    public void noSePuedeArrastrarUnNodoSiNoEresAdministradorDeAmbosNodos()
            throws Exception
    {
        when(personaService.isNodoMapaEditable("/a/", persona)).thenReturn(true);
        when(personaService.isNodoMapaEditable("/b/", persona)).thenReturn(false);

        NodoMapa nodoA = crearNodoA();
        NodoMapa nodoA1 = crearNodoA1();
        NodoMapa nodoB = crearNodoB();

        nodoA1.setUpoMapa(nodoA);

        Set<NodoMapa> setNodosMapa = nodoA.getUpoMapas();
        setNodosMapa.add(nodoA1);

        nodoA1.setUpoMapas(setNodosMapa);

        nodoMapaService.actualizarNodoPadreyUrlCompletaDeNodoyNodosHijo(nodoA1, nodoB, persona);
    }

    @Test(expected = NodoNoSePuedeArrastrarException.class)
    public void noSePuedeArrastrarUnNodoSiElNodoOrigenOElDestinoEsElNodoRaiz()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(true);

        NodoMapa nodoA = crearNodoA();
        NodoMapa nodoA1 = crearNodoA1();
        NodoMapa nodoB = crearNodoB();

        nodoA1.setId(0L);
        nodoB.setId(0L);

        nodoA1.setUpoMapa(nodoA);

        Set<NodoMapa> setNodosMapa = nodoA.getUpoMapas();
        setNodosMapa.add(nodoA1);

        nodoA1.setUpoMapas(setNodosMapa);

        nodoMapaService.actualizarNodoPadreyUrlCompletaDeNodoyNodosHijo(nodoA1, nodoB, persona);
    }

    @Test(expected = NodoNoSePuedeArrastrarException.class)
    public void noSePuedeArrastrarUnNodoDentroDeElMismo()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(true);

        NodoMapa nodo = crearNodoA1();

        nodoMapaService.actualizarNodoPadreyUrlCompletaDeNodoyNodosHijo(nodo, nodo, persona);
    }

    @Test(expected = NodoNoSePuedeArrastrarException.class)
    public void noSePuedeArrastrarUnNodoDentroDeUnoDeSusHijos()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(true);

        NodoMapa nodoA = crearNodoA();
        NodoMapa nodoA1 = crearNodoA1();

        nodoA1.setUpoMapa(nodoA);

        Set<NodoMapa> setNodosMapa = nodoA.getUpoMapas();
        setNodosMapa.add(nodoA1);

        nodoA1.setUpoMapas(setNodosMapa);

        nodoMapaService.actualizarNodoPadreyUrlCompletaDeNodoyNodosHijo(nodoA, nodoA1, persona);
    }

    @Test
    public void siUnNodoCambiaDePadreDebeLlamarseAlaFuncionQueCambiaElUrlPathDeSusHijos()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(true);
        when(nodoMapaDAO.update(anyObject())).thenReturn(new NodoMapa());

        NodoMapa nodoA = crearNodoA();
        NodoMapa nodoA1 = crearNodoA1();
        NodoMapa nodoB = crearNodoB();

        nodoA1.setNodoMapaDAO(nodoMapaDAO);

        nodoA1.setUpoMapa(nodoA);

        Set<NodoMapa> setNodosMapa = nodoA.getUpoMapas();
        setNodosMapa.add(nodoA1);

        nodoA1.setUpoMapas(setNodosMapa);

        nodoMapaService.actualizarNodoPadreyUrlCompletaDeNodoyNodosHijo(nodoA1, nodoB, persona);

        verify(nodoMapaDAO).actualizarUrlCompletaNodosHijoPorDragAndDrop(anyString(), anyString(), anyString());
    }

    @Test(expected = InsertadoContenidoNoAutorizadoException.class)
    public void ocurreUnErrorAlIntentarCrearUnaEstructuraDeRevistaEnUnNodoSinPermisos()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(false);

        estructuraRevistaService.insertaConEstructuraDeRevista(persona, nodoMapa, "01");
    }

    @Test(expected = InsertarEstructuraDeRevistaEnNodoMapaException.class)
    public void ocurreUnErrorAlIntentarCrearUnaEstructuraDeRevistaSinUnDia()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(true);

        estructuraRevistaService.insertaConEstructuraDeRevista(persona, nodoMapa, null);
    }

    @Test(expected = InsertadoContenidoNoAutorizadoException.class)
    public void ocurreUnErrorAlIntentarCrearUnaEstructuraDeVoxujiEnUnNodoSinPermisos()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(false);

        estructuraVoxujiService.insertaConEstructuraDeVoxuji(persona, nodoMapa, "1");
    }

    @Test(expected = InsertarEstructuraDeVoxujiEnNodoMapaException.class)
    public void ocurreUnErrorAlIntentarCrearUnaEstructuraDeVoxujiSinUnNumero()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(true);

        estructuraVoxujiService.insertaConEstructuraDeVoxuji(persona, nodoMapa, null);
    }

    @Test(expected = InsertadoContenidoNoAutorizadoException.class)
    public void ocurreUnErrorAlIntentarCrearUnaEstructuraDeFechaEnUnNodoSinPermisos()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(false);

        estructuraFechaService.insertaConEstructuraDeFecha(persona, nodoMapa, "01", "2000");
    }

    @Test(expected = InsertarEstructuraDeFechaEnNodoMapaException.class)
    public void ocurreUnErrorAlIntentarCrearUnaEstructuraDeFechaConUnAnyoSuperiorA2100()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(true);

        estructuraFechaService.insertaConEstructuraDeFecha(persona, nodoMapa, "01", "2200");
    }

    @Test(expected = InsertarEstructuraDeFechaEnNodoMapaException.class)
    public void ocurreUnErrorAlIntentarCrearUnaEstructuraDeFechaConUnAnyoInferiorA2000()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(true);

        estructuraFechaService.insertaConEstructuraDeFecha(persona, nodoMapa, "01", "1900");
    }

    @Test(expected = InsertarEstructuraDeFechaEnNodoMapaException.class)
    public void ocurreUnErrorAlIntentarCrearUnaEstructuraDeFechaSinAnyo()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(true);

        estructuraFechaService.insertaConEstructuraDeFecha(persona, nodoMapa, null, null);
    }

    @Test(expected = NumberFormatException.class)
    public void ocurreUnErrorAlIntentarCrearUnaEstructuraDeFechaConUnAnyoQueNoSeaNumero()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(true);

        estructuraFechaService.insertaConEstructuraDeFecha(persona, nodoMapa, "01", "anyo");
    }

    @Test(expected = InsertarEstructuraDeFechaEnNodoMapaException.class)
    public void ocurreUnErrorAlIntentarCrearUnaEstructuraDeFechaConUnMesSuperiorA12()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(true);

        estructuraFechaService.insertaConEstructuraDeFecha(persona, nodoMapa, "13", "2000");
    }

    @Test(expected = InsertarEstructuraDeFechaEnNodoMapaException.class)
    public void ocurreUnErrorAlIntentarCrearUnaEstructuraDeFechaConUnMesInferiorA1()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(true);

        estructuraFechaService.insertaConEstructuraDeFecha(persona, nodoMapa, "00", "2000");
    }

    @Test(expected = NumberFormatException.class)
    public void ocurreUnErrorAlIntentarCrearUnaEstructuraDeFechaConUnMesQueNoSeaNumero()
            throws Exception
    {
        when(personaService.isNodoMapaEditable(anyString(), any(Persona.class))).thenReturn(true);

        estructuraFechaService.insertaConEstructuraDeFecha(persona, nodoMapa, "mes", "2000");
    }
}
