package es.uji.apps.upo.model;

import es.uji.apps.upo.dao.ModeracionLoteDAO;
import es.uji.apps.upo.exceptions.InsertarModeracionLotesNoAutorizadoException;
import es.uji.apps.upo.exceptions.UrlNoValidaException;
import es.uji.apps.upo.migracion.ContentClient;
import es.uji.apps.upo.migracion.XpfRepositoryClient;
import es.uji.apps.upo.services.ModeracionLoteService;
import es.uji.apps.upo.services.PersonaService;
import es.uji.commons.rest.RoleCheckerAspect;
import es.uji.commons.sso.dao.ApaDAO;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import static org.mockito.Mockito.*;

@Ignore
public class ModeracionLoteTest
{
    private static final String URL = "/una/url/aleatoria/";
    public static final long CONNECTED_USER_ID = 1L;

    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    private NodoMapa nodoMapaMock;
    private Persona personaMock;
    private ModeracionLoteDAO moderacionLoteDAOMock;
    private ModeracionLote moderacionLote;
    private ApaDAO apaDAOMock;
    private PersonaService personaService;
    private ModeracionLoteService moderacionLoteService;

    @Before
    public void initData()
    {
        personaMock = mock(Persona.class);
        nodoMapaMock = mock(NodoMapa.class);
        apaDAOMock = mock(ApaDAO.class);
        moderacionLoteDAOMock = mock(ModeracionLoteDAO.class);
        personaService = mock(PersonaService.class);

        new RoleCheckerAspect().setApaDAO(apaDAOMock);

        moderacionLote = new ModeracionLote();
        moderacionLote.setUpoMapa(nodoMapaMock);
        /*moderacionLoteService = new ModeracionLoteService(moderacionLoteDAOMock, mock(XpfRepositoryClient.class),
                mock(ContentClient.class), personaService);*/

        when(nodoMapaMock.getUrlCompleta()).thenReturn(URL);
    }

    @Test
    public void elAdministradorDeUnNodoPuedeSolicitarModeracionLotes()
            throws InsertarModeracionLotesNoAutorizadoException, UrlNoValidaException
    {
        when(personaService.isNodoMapaEditable(URL, personaMock)).thenReturn(true);

        moderacionLoteService.insert(moderacionLote, personaMock, nodoMapaMock, CONNECTED_USER_ID);

        verify(personaService).isNodoMapaEditable(URL, personaMock);
        verify(moderacionLoteDAOMock).insert(moderacionLote);
    }

    @Test(expected = InsertarModeracionLotesNoAutorizadoException.class)
    public void exceptionCuandoUnUsuarioNoAdministradorInsertaUnaModeracionLotes()
            throws InsertarModeracionLotesNoAutorizadoException, UrlNoValidaException
    {
        when(personaService.isNodoMapaEditable(moderacionLote.getUpoMapa().getUrlCompleta(), personaMock)).thenReturn(
                false);

        moderacionLoteService.insert(moderacionLote, personaMock, nodoMapaMock, CONNECTED_USER_ID);
    }
}
