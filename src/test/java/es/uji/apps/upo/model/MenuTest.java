package es.uji.apps.upo.model;

import com.mchange.rmi.NotAuthorizedException;
import es.uji.apps.upo.dao.GrupoDAO;
import es.uji.apps.upo.dao.ItemDAO;
import es.uji.apps.upo.dao.MenuDAO;
import es.uji.apps.upo.dao.MigracionDAO;
import es.uji.apps.upo.exceptions.InsertarModeracionLotesNoAutorizadoException;
import es.uji.apps.upo.services.*;
import es.uji.commons.rest.exceptions.RegistroDuplicadoException;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MenuTest
{
    public static final long PERSONA_ID_ADMIN = 1L;
    public static final long PERSONA_ID_NO_ADMIN = 2L;
    public static final int ORDEN = 1;

    private ItemService itemService;
    private GrupoService grupoService;
    private MenuService menuService;
    private ItemDAO itemDAO;
    private GrupoDAO grupoDAO;
    private MenuDAO menuDAO;
    private PersonaService personaService;

    private Persona personaAdmin;
    private Persona personaNoAdmin;

    private Item itemAdmin;
    private Item itemNoAdmin;

    private Grupo grupoAdmin;
    private Grupo grupoNoAdmin;

    private Menu menuAdmin;
    private Menu menuNoAdmin;

    @Before
    public void init()
    {
        personaService = mock(PersonaService.class);
        itemDAO = mock(ItemDAO.class);
        menuDAO = mock(MenuDAO.class);
        grupoDAO = mock(GrupoDAO.class);

        itemService = new ItemService(itemDAO, personaService);
        grupoService = new GrupoService(grupoDAO, itemDAO, personaService);
        menuService = new MenuService(menuDAO, grupoDAO, personaService);

        personaAdmin = new Persona(PERSONA_ID_ADMIN);
        personaNoAdmin = new Persona(PERSONA_ID_NO_ADMIN);

        when(personaService.isAdmin(PERSONA_ID_ADMIN)).thenReturn(true);
        when(personaService.isAdmin(PERSONA_ID_NO_ADMIN)).thenReturn(false);

        when(itemDAO.get(any(), anyLong())).thenReturn(Arrays.asList(new Item()));
        when(grupoDAO.get(any(), anyLong())).thenReturn(Arrays.asList(new Grupo()));
        when(menuDAO.get(any(), anyLong())).thenReturn(Arrays.asList(new Menu()));

        createItems();
        createGrupos();
        createMenus();
    }

    private void  createItems()
    {
        itemAdmin = new Item();
        itemAdmin.setId(1L);
        itemAdmin.setPersona(personaAdmin);

        itemNoAdmin = new Item();
        itemNoAdmin.setId(2L);
        itemNoAdmin.setPersona(personaNoAdmin);
    }

    private void  createGrupos()
    {
        grupoAdmin = new Grupo();
        grupoAdmin.setId(1L);
        grupoAdmin.setPersona(personaAdmin);

        grupoNoAdmin = new Grupo();
        grupoNoAdmin.setId(2L);
        grupoNoAdmin.setPersona(personaNoAdmin);
    }

    private void  createMenus()
    {
        menuAdmin = new Menu();
        menuAdmin.setId(1L);
        menuAdmin.setPersona(personaAdmin);

        menuNoAdmin = new Menu();
        menuNoAdmin.setId(2L);
        menuNoAdmin.setPersona(personaNoAdmin);
    }

    @Test(expected = UnauthorizedUserException.class)
    public void siNoEresAdministradorNoPuedesBorrarUnItemQueNoSeaTuyo()
            throws UnauthorizedUserException
    {
        itemService.delete(itemAdmin.getId(), PERSONA_ID_NO_ADMIN);
    }

    @Test(expected = UnauthorizedUserException.class)
    public void siNoEresAdministradorNoPuedesActualizarUnItemQueNoSeaTuyo()
            throws UnauthorizedUserException
    {
        itemService.update(itemAdmin, PERSONA_ID_NO_ADMIN);
    }

    @Test
    public void siEresAdminPuedesBorrarYActualizarCualquierItem()
            throws UnauthorizedUserException
    {
        itemService.delete(itemNoAdmin.getId(), PERSONA_ID_ADMIN);
        itemService.update(itemNoAdmin, PERSONA_ID_ADMIN);
    }

    @Test
    public void siNoEresAdminPuedesBorrarYActualizarUnItemPropio()
            throws UnauthorizedUserException
    {
        itemService.delete(itemNoAdmin.getId(), PERSONA_ID_ADMIN);
        itemService.update(itemNoAdmin, PERSONA_ID_ADMIN);
    }

    @Test(expected = UnauthorizedUserException.class)
    public void siNoEresAdministradorNoPuedesBorrarUnGrupoQueNoSeaTuyo()
            throws UnauthorizedUserException
    {
        grupoService.delete(grupoAdmin.getId(), PERSONA_ID_NO_ADMIN);
    }

    @Test(expected = UnauthorizedUserException.class)
    public void siNoEresAdministradorNoPuedesActualizarUnGrupoQueNoSeaTuyo()
            throws UnauthorizedUserException
    {
        grupoService.update(grupoAdmin, PERSONA_ID_NO_ADMIN);
    }

    @Test
    public void siEresAdminPuedesBorrarYActualizarCualquierGrupo()
            throws UnauthorizedUserException
    {
        grupoService.delete(grupoNoAdmin.getId(), PERSONA_ID_ADMIN);
        grupoService.update(grupoNoAdmin, PERSONA_ID_ADMIN);
    }

    @Test
    public void siNoEresAdminPuedesBorrarYActualizarUnGrupoPropio()
            throws UnauthorizedUserException
    {
        grupoService.delete(grupoNoAdmin.getId(), PERSONA_ID_ADMIN);
        grupoService.update(grupoNoAdmin, PERSONA_ID_ADMIN);
    }

    @Test(expected = UnauthorizedUserException.class)
    public void siNoEresAdministradorNoPuedesBorrarUnMenuQueNoSeaTuyo()
            throws UnauthorizedUserException
    {
        menuService.delete(menuAdmin.getId(), PERSONA_ID_NO_ADMIN);
    }

    @Test(expected = UnauthorizedUserException.class)
    public void siNoEresAdministradorNoPuedesActualizarUnMenuQueNoSeaTuyo()
            throws UnauthorizedUserException
    {
        menuService.update(menuAdmin, PERSONA_ID_NO_ADMIN);
    }

    @Test
    public void siEresAdminPuedesBorrarYActualizarCualquierMenu()
            throws UnauthorizedUserException
    {
        menuService.delete(menuNoAdmin.getId(), PERSONA_ID_ADMIN);
        menuService.update(menuNoAdmin, PERSONA_ID_ADMIN);
    }

    @Test
    public void siNoEresAdminPuedesBorrarYActualizarUnMenuPropio()
            throws UnauthorizedUserException
    {
        menuService.delete(menuNoAdmin.getId(), PERSONA_ID_ADMIN);
        menuService.update(menuNoAdmin, PERSONA_ID_ADMIN);
    }

    @Test(expected = UnauthorizedUserException.class)
    public void siNoEresAdminNoPuedesInsertarUnItemEnUnGrupoSiNoEresPropietarioDelGrupo()
            throws UnauthorizedUserException, RegistroDuplicadoException
    {
        grupoService.addItem(grupoNoAdmin.getId(), itemAdmin.getId(), ORDEN, PERSONA_ID_NO_ADMIN);
    }

    @Test(expected = UnauthorizedUserException.class)
    public void siNoEresAdminNoPuedesInsertarUnItemEnUnGrupoSiNoEresPropietarioDelItem()
            throws UnauthorizedUserException, RegistroDuplicadoException
    {
        grupoService.addItem(grupoAdmin.getId(), itemNoAdmin.getId(), ORDEN, PERSONA_ID_NO_ADMIN);
    }

    @Test(expected = UnauthorizedUserException.class)
    public void siNoEresAdminNoPuedesActualizarUnItemEnUnGrupoSiNoEresPropietarioDelGrupo()
            throws UnauthorizedUserException, RegistroDuplicadoException
    {
        grupoService.updateItem(grupoNoAdmin.getId(), itemAdmin.getId(), ORDEN, PERSONA_ID_NO_ADMIN);
    }

    @Test(expected = UnauthorizedUserException.class)
    public void siNoEresAdminNoPuedesActualizarUnItemEnUnGrupoSiNoEresPropietarioDelItem()
            throws UnauthorizedUserException, RegistroDuplicadoException
    {
        grupoService.updateItem(grupoAdmin.getId(), itemNoAdmin.getId(), ORDEN, PERSONA_ID_NO_ADMIN);
    }

    @Test(expected = UnauthorizedUserException.class)
    public void siNoEresAdminNoPuedesBorrarUnItemEnUnGrupoSiNoEresPropietarioDelGrupo()
            throws UnauthorizedUserException, RegistroDuplicadoException
    {
        grupoService.deleteItem(grupoNoAdmin.getId(), itemAdmin.getId(), PERSONA_ID_NO_ADMIN);
    }

    @Test(expected = UnauthorizedUserException.class)
    public void siNoEresAdminNoPuedesBorrarUnItemEnUnGrupoSiNoEresPropietarioDelItem()
            throws UnauthorizedUserException, RegistroDuplicadoException
    {
        grupoService.deleteItem(grupoAdmin.getId(), itemNoAdmin.getId(), PERSONA_ID_NO_ADMIN);
    }

    @Test
    public void siEresAdminPuedesInsertarBorrarYActualizarCualquierItemEnUnGrupo()
            throws UnauthorizedUserException, RegistroDuplicadoException
    {
        grupoService.addItem(grupoNoAdmin.getId(), itemNoAdmin.getId(), ORDEN, PERSONA_ID_ADMIN);
        grupoService.updateItem(grupoNoAdmin.getId(), itemNoAdmin.getId(), ORDEN, PERSONA_ID_ADMIN);
        grupoService.deleteItem(grupoNoAdmin.getId(), itemNoAdmin.getId(), PERSONA_ID_ADMIN);
    }

    @Test(expected = UnauthorizedUserException.class)
    public void siNoEresAdminNoPuedesInsertarUnGrupoEnUnMenuSiNoEresPropietarioDelMenu()
            throws UnauthorizedUserException, RegistroDuplicadoException
    {
        menuService.addGrupo(menuNoAdmin.getId(), grupoAdmin.getId(), ORDEN, PERSONA_ID_NO_ADMIN);
    }

    @Test(expected = UnauthorizedUserException.class)
    public void siNoEresAdminNoPuedesInsertarUnGrupoEnUnMenuSiNoEresPropietarioDelGrupo()
            throws UnauthorizedUserException, RegistroDuplicadoException
    {
        menuService.addGrupo(menuAdmin.getId(), grupoNoAdmin.getId(), ORDEN, PERSONA_ID_NO_ADMIN);
    }

    @Test(expected = UnauthorizedUserException.class)
    public void siNoEresAdminNoPuedesActualizarUnGrupoEnUnMenuSiNoEresPropietarioDelMenu()
            throws UnauthorizedUserException, RegistroDuplicadoException
    {
        menuService.updateGrupo(menuNoAdmin.getId(), grupoAdmin.getId(), ORDEN, PERSONA_ID_NO_ADMIN);
    }

    @Test(expected = UnauthorizedUserException.class)
    public void siNoEresAdminNoPuedesActualizarUnGrupoEnUnMenuSiNoEresPropietarioDelGrupo()
            throws UnauthorizedUserException, RegistroDuplicadoException
    {
        menuService.updateGrupo(menuAdmin.getId(), grupoNoAdmin.getId(), ORDEN, PERSONA_ID_NO_ADMIN);
    }

    @Test(expected = UnauthorizedUserException.class)
    public void siNoEresAdminNoPuedesBorrarUnGrupoEnUnMenuSiNoEresPropietarioDelMenu()
            throws UnauthorizedUserException, RegistroDuplicadoException
    {
        menuService.deleteGrupo(menuNoAdmin.getId(), grupoAdmin.getId(), PERSONA_ID_NO_ADMIN);
    }

    @Test(expected = UnauthorizedUserException.class)
    public void siNoEresAdminNoPuedesBorrarUnGrupoEnUnMenuSiNoEresPropietarioDelGrupo()
            throws UnauthorizedUserException, RegistroDuplicadoException
    {
        menuService.deleteGrupo(menuAdmin.getId(), grupoNoAdmin.getId(), PERSONA_ID_NO_ADMIN);
    }

    @Test
    public void siEresAdminPuedesInsertarBorrarYActualizarCualquierGrupoEnUnMenu()
            throws UnauthorizedUserException, RegistroDuplicadoException
    {
        menuService.addGrupo(menuNoAdmin.getId(), grupoNoAdmin.getId(), ORDEN,  PERSONA_ID_ADMIN);
        menuService.updateGrupo(menuNoAdmin.getId(), grupoNoAdmin.getId(), ORDEN, PERSONA_ID_ADMIN);
        menuService.deleteGrupo(menuNoAdmin.getId(), grupoNoAdmin.getId(), PERSONA_ID_ADMIN);
    }
}
