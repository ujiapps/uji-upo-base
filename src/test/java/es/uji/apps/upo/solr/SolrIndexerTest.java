package es.uji.apps.upo.solr;

import java.util.List;

import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.uji.apps.upo.model.enums.IdiomaPublicacion;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class SolrIndexerTest
{
    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();
    
    @Autowired
    public SolrIndexer indexer;

    @Before
    public void initNewIndexedContent()
    {
        indexer.add(createTestContent("1"));
        indexer.add(createTestContent("2"));

        ContenidoSolr content = createTestContent("3");
        content.setTitulo(content.getTitulo() + "zzz");

        indexer.add(content);
    }

    public ContenidoSolr createTestContent(String id)
    {
        ContenidoSolr contenido = new ContenidoSolr();
        contenido.setId(id);
        contenido.setUrl("perico");
        contenido.setTitulo("Título contenido mojama");
        contenido.setIdioma(IdiomaPublicacion.ES);

        return contenido;
    }

    @Test
    public void documentosWithSameTitleShouldBeRetrieved()
    {
        List<ContenidoSolr> listaResultados = indexer.getList(ContenidoSolr.class,
                "titulo:\"Título contenido mojama\"");

        Assert.assertNotNull(listaResultados);
        Assert.assertEquals(2, listaResultados.size());
    }

    @Test
    public void addedDocumentsShouldBeRetrieved()
    {
        ContenidoSolr resultados = indexer.get(ContenidoSolr.class, "1");

        Assert.assertNotNull(resultados);
        Assert.assertEquals("1", resultados.getId());
    }

    @After
    public void removeIndexedTestDocuments()
    {
        indexer.deleteById("1");
        indexer.deleteById("2");
        indexer.deleteById("3");
    }
}
