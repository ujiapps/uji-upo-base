package es.uji.apps.upo.dao;

import es.uji.apps.upo.builders.*;
import es.uji.apps.upo.exceptions.InsertadaPlantillaDelMismoTipoException;
import es.uji.apps.upo.exceptions.NodoConNombreYaExistenteEnMismoPadreException;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.TipoPlantilla;
import es.uji.apps.upo.services.nodomapa.EstructuraFechaService;
import es.uji.apps.upo.services.nodomapa.NodoMapaService;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class NodoMapaDAOTest
{
    private static final Integer DAFAULT_LEVEL = 1;

    @Autowired
    private NodoMapaDAO nodoMapaDAO;

    @Autowired
    private PersonaDAO personaDAO;

    @Autowired
    private FranquiciaDAO franquiciaDAO;

    @Autowired
    private MenuDAO menuDAO;

    @Autowired
    private PlantillaDAO plantillaDAO;

    @Autowired
    private NodoMapaService nodoMapaService;

    @Autowired
    private EstructuraFechaService estructuraFechaService;

    @PersistenceContext
    private EntityManager entityManager;

    private Persona persona;
    private Franquicia franquicia;
    private NodoMapa nodoMapaRaiz;
    private NodoMapa nodoMapa;
    private NodoMapa nodoMapaHijo;
    private NodoMapa nodoMapaNieto;
    private NodoMapa nodoMapaHermano;
    private NodoMapa nodoMapaRaizOriginal;
    private Menu menu;
    private Menu menuActualizado;
    private Plantilla plantilla;

    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    String raiz = "/" + UUID.randomUUID().toString() + "/";

    @Before
    @Transactional
    public void insertarNodoMapa()
    {
        new NodoMapa().setNodoMapaDAO(nodoMapaDAO);

        menu = new MenuBuilder(menuDAO).withNombre("menu").build();
        menuActualizado = new MenuBuilder(menuDAO).withNombre("menuActualizado").build();

        persona = new PersonaBuilder(personaDAO).getAny();

        franquicia = new FranquiciaBuilder(franquiciaDAO).withNombre("Franquicia").build();

        plantilla = new PlantillaBuilder(plantillaDAO).withNombre("plantilla").withTipo(TipoPlantilla.WEB).build();

        new FranquiciaAccesoBuilder(franquiciaDAO).withFranquicia(franquicia).withPersona(persona).build();

        nodoMapaRaizOriginal = nodoMapaDAO.getNodoMapaById(0L).get(0);

        nodoMapa = new NodoMapaBuilder(nodoMapaDAO)
                .withUrlPath("nodo")
                .withUrlCompleta(raiz + "nodo/")
                .withFranquicia(franquicia)
                .withMenu(menu)
                .withOrden(1L)
                .withParent(nodoMapaRaizOriginal)
                .build();

        nodoMapaHijo = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodo2")
                .withUrlCompleta(raiz + "nodo/nodo2/")
                .withFranquicia(franquicia)
                .withParent(nodoMapa)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        nodoMapaHermano = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nuevoExistente")
                .withUrlCompleta(raiz + "nodo/nuevoExistente/")
                .withFranquicia(franquicia)
                .withParent(nodoMapa)
                .withMenu(menu)
                .withOrden(2L)
                .build();

        nodoMapaNieto = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodo3")
                .withUrlCompleta(raiz + "nodo/nodo2/nodo3/")
                .withFranquicia(franquicia)
                .withParent(nodoMapaHijo)
                .withMenu(menu)
                .withOrden(1L)
                .build();
    }

    @Test
    public void personaEsAdminDeUnaUrlConUnNodo()
    {
        Assert.assertTrue(nodoMapaDAO.isNodoMapaAdmin(raiz + "nodo/", persona.getId()));
    }

    @Test
    public void personaEsAdminDeUnaUrlConMasDeUnNodo()
    {
        Assert.assertFalse(nodoMapaDAO.isNodoMapaAdmin(raiz + "nodoadmin/nodo/", persona.getId()));
    }

    @Test
    public void personaEsAdminDeUnaUrlSiLoEsDeUnNodoSuperior()
    {
        Assert.assertTrue(nodoMapaDAO.isNodoMapaAdmin(raiz + "nodo/nodonoadmin/", persona.getId()));
    }

    @Test
    public void personaNoEsAdminDeUnaUrlConUnNodo()
    {
        Assert.assertFalse(nodoMapaDAO.isNodoMapaAdmin(raiz + "pepe/", persona.getId()));
    }

    @Test
    public void siUnNodoAscendenteEstaBloqueadoEstanTodosBloqueados()
    {
        nodoMapa.setBloqueado(true);

        Assert.assertFalse(nodoMapaDAO.isNodoMapaLocked(nodoMapa.getUrlCompleta()));
        Assert.assertFalse(nodoMapaDAO.isNodoMapaLocked(nodoMapaNieto.getUrlCompleta()));
    }

    @Test
    public void personaEsAdminDeCualquierUrlSiAdministradorDeNodoRaiz()
    {
        nodoMapaRaiz = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("raiz")
                .withParent(nodoMapaRaizOriginal)
                .withUrlCompleta(raiz)
                .withFranquicia(franquicia)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        Assert.assertTrue(nodoMapaDAO.isNodoMapaAdmin(raiz, persona.getId()));
        Assert.assertTrue(nodoMapaDAO.isNodoMapaAdmin(raiz + "loquesea/", persona.getId()));

        deleteNodoMapa(nodoMapaRaiz.getId());
    }

    @Test
    public void lasUrlCompletaDeLosNodosDebenDeSerLasCorrectas()
    {
        NodoMapa nodoMapaTest = nodoMapaDAO.get(NodoMapa.class, nodoMapa.getId()).get(0);
        Assert.assertEquals(raiz + "nodo/", nodoMapaTest.getUrlCompleta());

        nodoMapaTest = nodoMapaDAO.get(NodoMapa.class, nodoMapaNieto.getId()).get(0);
        Assert.assertEquals(raiz + "nodo/nodo2/nodo3/", nodoMapaTest.getUrlCompleta());

        nodoMapaTest = nodoMapaDAO.get(NodoMapa.class, nodoMapaHijo.getId()).get(0);
        Assert.assertEquals(raiz + "nodo/nodo2/", nodoMapaTest.getUrlCompleta());
    }

    @Test
    public void siSeCambiaElUrlPathDeUnNodoMapaDebeCambiarLaUrlCompletaDeLosNodosHijoYLaDelPropioNodo()
            throws Exception
    {
        nodoMapaRaiz = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("raiz")
                .withParent(nodoMapaRaizOriginal)
                .withUrlCompleta(raiz)
                .withFranquicia(franquicia)
                .withMenu(menu)
                .withOrden(1L)
                .build();
        nodoMapa.setUpoMapa(nodoMapaRaiz);

        String nombreNuevo = "nuevoNoExistente";

        nodoMapaService.actualizarUrlPathyUrlCompletaDeNodoyNodosHijo(nodoMapa, nombreNuevo, persona);

        Assert.assertEquals(nombreNuevo, nodoMapa.getUrlPath());
        Assert.assertEquals(raiz + "nuevoNoExistente/", nodoMapa.getUrlCompleta());

        NodoMapa nodoMapaTest = nodoMapaDAO.get(NodoMapa.class, nodoMapa.getId()).get(0);
        Assert.assertEquals(raiz + "nuevoNoExistente/", nodoMapaTest.getUrlCompleta());

        nodoMapaTest = nodoMapaDAO.get(NodoMapa.class, nodoMapaNieto.getId()).get(0);
        Assert.assertEquals(raiz + "nuevoNoExistente/nodo2/nodo3/", nodoMapaTest.getUrlCompleta());

        nodoMapaTest = nodoMapaDAO.get(NodoMapa.class, nodoMapaHijo.getId()).get(0);
        Assert.assertEquals(raiz + "nuevoNoExistente/nodo2/", nodoMapaTest.getUrlCompleta());

        deleteNodoMapa(nodoMapaRaiz.getId());
    }

    @Test(expected = NodoConNombreYaExistenteEnMismoPadreException.class)
    public void NoSePuedeCambiarLaURlPathDeUnNodoSiElPadreDelNodoTieneUnHijoConEsaMismaUrlPath()
            throws Exception
    {
        String nombreNuevo = "nuevoExistente";

        nodoMapaService.actualizarUrlPathyUrlCompletaDeNodoyNodosHijo(nodoMapaHijo, nombreNuevo, persona);
    }

    @Test(expected = NodoConNombreYaExistenteEnMismoPadreException.class)
    public void NoSePuedeInsertarUnNodoSiSuNombreCoincideConElDeUnNodoHermano()
            throws Exception
    {
        nodoMapaHermano = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodo2")
                .withUrlCompleta(raiz + "nodo/nodo2/ParaQueNoCoincida")
                .withFranquicia(franquicia)
                .withParent(nodoMapa)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        nodoMapaService.insert(persona, nodoMapaHermano);
    }

    @Test(expected = Exception.class)
    public void NoSePuedeInsertarUnNodoSiSuUrlCompletaCoincideConOtra()
            throws Exception
    {
        nodoMapaHermano = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("paraQueNoCoincida")
                .withUrlCompleta(raiz + "nodo/nodo2/")
                .withFranquicia(franquicia)
                .withParent(nodoMapa)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        nodoMapaService.insert(persona, nodoMapaHermano);
    }

    @Test(expected = NodoConNombreYaExistenteEnMismoPadreException.class)
    public void NoSePuedeCambiarElPadreDeUnNodoSiTieneUnHijoConElMismoNombreQueELNodo()
            throws Exception
    {
        nodoMapaNieto.setUrlPath("nodo2");

        nodoMapaService.actualizarNodoPadreyUrlCompletaDeNodoyNodosHijo(nodoMapaNieto, nodoMapa, persona);
    }

    @Test
    public void siSeCambiaElPadreDeUnNodoDebeActualizarseSuUrlCompletaYLaDeSusHijos()
            throws Exception
    {
        nodoMapaService.actualizarNodoPadreyUrlCompletaDeNodoyNodosHijo(nodoMapaHijo, nodoMapaHermano, persona);

        NodoMapa nodoMapaTest = nodoMapaDAO.get(NodoMapa.class, nodoMapaNieto.getId()).get(0);
        Assert.assertEquals(raiz + "nodo/nuevoExistente/nodo2/nodo3/", nodoMapaTest.getUrlCompleta());

        nodoMapaTest = nodoMapaDAO.get(NodoMapa.class, nodoMapaHijo.getId()).get(0);
        Assert.assertEquals(raiz + "nodo/nuevoExistente/nodo2/", nodoMapaTest.getUrlCompleta());
    }

    @Test
    public void siSeInsertaUnaEstructuraDeFechasConUnMesEnUnNodoElNumeroDeNodosInsertadorDebeCoincidirConElNumeroDeDias()
            throws Exception
    {
        estructuraFechaService.insertaConEstructuraDeFecha(persona, nodoMapaNieto, "01", "2000");

        List<NodoMapa> result =
                nodoMapaDAO.get(NodoMapa.class, "urlCompleta like '%" + raiz + "nodo/nodo2/nodo3/2000%'");

        Assert.assertEquals(31 + 1 + 1, result.size());
    }

    @Test
    public void siSeInsertaUnaEstructuraDeFechasConUnAnyoEnUnNodoElNumeroDeNodosInsertadorDebeCoincidirConElNumeroDeDias()
            throws Exception
    {
        estructuraFechaService.insertaConEstructuraDeFecha(persona, nodoMapaNieto, "", "2000");

        Calendar calendar = Calendar.getInstance();
        calendar.set(Integer.parseInt("2000"), Integer.parseInt("2") - 1, 1);
        Integer numDias = calendar.getActualMaximum(Calendar.DAY_OF_YEAR);

        List<NodoMapa> result =
                nodoMapaDAO.get(NodoMapa.class, "urlCompleta like '%" + raiz + "nodo/nodo2/nodo3/2000%'");

        Assert.assertEquals(numDias + 12 + 1, result.size());
    }

    @Test
    public void siSeCambiaElMenuDeUnPadreDebeCambiarElMenuDeAquellosHijosQueSeanDeTipoHeredados()
            throws Exception
    {
        NodoMapa nodoMapaBiznieto = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodo2")
                .withUrlCompleta(raiz + "nodo/nodo2/nodo3/nodo4")
                .withFranquicia(franquicia)
                .withParent(nodoMapaNieto)
                .withMenu(menu)
                .withMenuHeredado(false)
                .withOrden(1L)
                .build();

        NodoMapa nodoMapaTataranieto = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodo2")
                .withUrlCompleta(raiz + "nodo/nodo2/nodo3/nodo4/nodo5")
                .withFranquicia(franquicia)
                .withParent(nodoMapaBiznieto)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        NodoMapa nodoMapaCuadrinieto = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodo2")
                .withUrlCompleta(raiz + "nodo/nodo2/nodo3/nodo4/nodo5/nodo6")
                .withFranquicia(franquicia)
                .withParent(nodoMapaTataranieto)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        NodoMapa nodoMapaHijoDeHermano = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodo2")
                .withUrlCompleta(raiz + "nodo/nuevoExistente/nodo2")
                .withFranquicia(franquicia)
                .withParent(nodoMapaHermano)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        NodoMapa nodoMapaOtroHermano = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodo2")
                .withUrlCompleta(raiz + "otroHermano")
                .withFranquicia(franquicia)
                .withParent(nodoMapa)
                .withMenu(menu)
                .withMenuHeredado(false)
                .withOrden(1L)
                .build();

        NodoMapa nodoMapaHijoDeOtroHermano = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodo2")
                .withUrlCompleta(raiz + "otroHermano/nodo2")
                .withFranquicia(franquicia)
                .withParent(nodoMapaOtroHermano)
                .withMenu(menu)
                .withMenuHeredado(false)
                .withOrden(1L)
                .build();

        nodoMapaService.updateMenuANoHeredado(nodoMapa, persona, menuActualizado.getId());

        NodoMapa nodoMapaAux = nodoMapaDAO.get(NodoMapa.class, nodoMapa.getId()).get(0);
        Assert.assertEquals(menuActualizado.getNombre(), nodoMapaAux.getMenu().getNombre());

        nodoMapaAux = nodoMapaDAO.get(NodoMapa.class, nodoMapaHijo.getId()).get(0);
        Assert.assertEquals(menuActualizado.getNombre(), nodoMapaAux.getMenu().getNombre());

        nodoMapaAux = nodoMapaDAO.get(NodoMapa.class, nodoMapaNieto.getId()).get(0);
        Assert.assertEquals(menuActualizado.getNombre(), nodoMapaAux.getMenu().getNombre());

        nodoMapaAux = nodoMapaDAO.get(NodoMapa.class, nodoMapaHermano.getId()).get(0);
        Assert.assertEquals(menuActualizado.getNombre(), nodoMapaAux.getMenu().getNombre());

        nodoMapaAux = nodoMapaDAO.get(NodoMapa.class, nodoMapaHijoDeHermano.getId()).get(0);
        Assert.assertEquals(menuActualizado.getNombre(), nodoMapaAux.getMenu().getNombre());

        nodoMapaAux = nodoMapaDAO.get(NodoMapa.class, nodoMapaTataranieto.getId()).get(0);
        Assert.assertEquals(menu.getNombre(), nodoMapaAux.getMenu().getNombre());

        nodoMapaAux = nodoMapaDAO.get(NodoMapa.class, nodoMapaCuadrinieto.getId()).get(0);
        Assert.assertEquals(menu.getNombre(), nodoMapaAux.getMenu().getNombre());

        nodoMapaAux = nodoMapaDAO.get(NodoMapa.class, nodoMapaOtroHermano.getId()).get(0);
        Assert.assertEquals(menu.getNombre(), nodoMapaAux.getMenu().getNombre());

        nodoMapaAux = nodoMapaDAO.get(NodoMapa.class, nodoMapaHijoDeOtroHermano.getId()).get(0);
        Assert.assertEquals(menu.getNombre(), nodoMapaAux.getMenu().getNombre());

        deleteNodoMapa(nodoMapaBiznieto.getId());
        deleteNodoMapa(nodoMapaTataranieto.getId());
        deleteNodoMapa(nodoMapaCuadrinieto.getId());
        deleteNodoMapa(nodoMapaHijoDeHermano.getId());
        deleteNodoMapa(nodoMapaOtroHermano.getId());
        deleteNodoMapa(nodoMapaHijoDeOtroHermano.getId());
    }

    @Test
    public void siSeCambiaELTipoDeMenuAHeredadoDebePonerseElMismoMenuQueTieneElPadre()
            throws Exception
    {
        nodoMapa.setMenu(menuActualizado);

        nodoMapaService.updateMenuAHeredado(nodoMapaHijo, persona);

        NodoMapa nodoMapaAux = nodoMapaDAO.get(NodoMapa.class, nodoMapaHijo.getId()).get(0);
        Assert.assertEquals(menuActualizado.getNombre(), nodoMapaAux.getMenu().getNombre());

        nodoMapaAux = nodoMapaDAO.get(NodoMapa.class, nodoMapaNieto.getId()).get(0);
        Assert.assertEquals(menuActualizado.getNombre(), nodoMapaAux.getMenu().getNombre());
    }

    @Test
    public void sePuedeActualizarElOrdenDeLosNodos()
    {
        nodoMapaDAO.actualizarOrdenNodos(nodoMapa.getId(), Arrays.asList(nodoMapaHijo, nodoMapaHermano), 1L);

        NodoMapa nodoMapaAux = nodoMapaDAO.get(NodoMapa.class, nodoMapaHijo.getId()).get(0);
        Assert.assertTrue(nodoMapaAux.getOrden().equals(2L));

        nodoMapaAux = nodoMapaDAO.get(NodoMapa.class, nodoMapaHermano.getId()).get(0);
        Assert.assertTrue(nodoMapaAux.getOrden().equals(3L));
    }

    @Test(expected = InsertadaPlantillaDelMismoTipoException.class)
    public void ocurreUnErrorAlIntentarPlantillasDelMismoTipo()
            throws Exception
    {
        nodoMapaService.insertarPlantilla(persona, plantilla, nodoMapa, DAFAULT_LEVEL);
        nodoMapaService.insertarPlantilla(persona, plantilla, nodoMapa, DAFAULT_LEVEL);
    }

    @Transactional
    private void deleteNodoMapa(Long id)
    {
        nodoMapaDAO.delete(NodoMapa.class, id);
    }

    @After
    @Transactional
    public void clean()
    {
        nodoMapaDAO.delete(NodoMapa.class, nodoMapaNieto.getId());
        nodoMapaDAO.delete(NodoMapa.class, nodoMapaHijo.getId());
        nodoMapaDAO.delete(NodoMapa.class, nodoMapaHermano.getId());
        nodoMapaDAO.delete(NodoMapa.class, nodoMapa.getId());

        nodoMapaDAO.delete(Franquicia.class, franquicia.getId());
        nodoMapaDAO.delete(Menu.class, menu.getId());
        nodoMapaDAO.delete(Menu.class, menuActualizado.getId());
        nodoMapaDAO.delete(Plantilla.class, plantilla.getId());
    }
}
