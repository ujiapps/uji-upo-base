package es.uji.apps.upo.dao;

import java.util.List;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.upo.exceptions.NodoMapaContenidoDebeTenerUnContenidoNormalException;
import es.uji.apps.upo.builders.ContenidoBuilder;
import es.uji.apps.upo.builders.FranquiciaBuilder;
import es.uji.apps.upo.builders.MenuBuilder;
import es.uji.apps.upo.builders.NodoMapaBuilder;
import es.uji.apps.upo.builders.NodoMapaContenidoBuilder;
import es.uji.apps.upo.builders.PersonaBuilder;
import es.uji.apps.upo.model.Contenido;
import es.uji.apps.upo.model.enums.EstadoModeracion;
import es.uji.apps.upo.model.Franquicia;
import es.uji.apps.upo.model.Menu;
import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.model.NodoMapaContenido;
import es.uji.apps.upo.model.Persona;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
@Transactional
public class NodoMapaContenidoDAOTest
{
    @Autowired
    private NodoMapaDAO nodoMapaDAO;

    @Autowired
    private ContenidoDAO contenidoDAO;

    @Autowired
    private NodoMapaContenidoDAO nodoMapaContenidoDAO;

    @Autowired
    private FranquiciaDAO franquiciaDAO;

    @Autowired
    private PersonaDAO personaDAO;

    @Autowired
    private MenuDAO menuDAO;

    private Persona personaResponsable;
    private Franquicia franquicia;
    private Contenido contenido;
    private NodoMapa nodoMapa;
    private NodoMapaContenido nodoMapaContenido;
    private Menu menu;

    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    String raiz = "/" + UUID.randomUUID().toString() + "/";

    @Before
    public void insertarNodoMapa()
    {
        new Contenido().setNodoMapaContenidoDAO(nodoMapaContenidoDAO);
        franquicia = new FranquiciaBuilder(franquiciaDAO).withNombre("Franquicia").build();

        menu = new MenuBuilder(menuDAO).withNombre("menu").build();

        nodoMapa = new NodoMapaBuilder(nodoMapaDAO).withParent()
                .withUrlPath("raiz")
                .withUrlCompleta(raiz + "raiz/")
                .withFranquicia(franquicia)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        personaResponsable = new PersonaBuilder(personaDAO).getAny();

        contenido = new ContenidoBuilder(contenidoDAO).withUrlPath("raiz")
                .withPersonaResponsable(personaResponsable)
                .build();
    }

    @Test
    public void nodaMapaExisteYCoincideConElConenidoInsertado()
    {
        NodoMapa nodoMapaAux = nodoMapaDAO.get(NodoMapa.class, nodoMapa.getId()).get(0);

        Assert.assertEquals(nodoMapaAux.getId(), nodoMapa.getId());
    }

    @Test
    public void contenidoExisteYCoincideConElConenidoInsertado()
    {
        Contenido contenidoAux = contenidoDAO.get(Contenido.class, contenido.getId()).get(0);

        Assert.assertEquals(contenidoAux.getId(), contenido.getId());
    }

    @Test
    public void nodoMapaContenidoExisteYCoincideConElConenidoInsertado()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        nodoMapaContenido =
                new NodoMapaContenidoBuilder(nodoMapaContenidoDAO).withEstadoModeracion(EstadoModeracion.PENDIENTE)
                        .withMapa(nodoMapa)
                        .withContenido(contenido)
                        .build();

        NodoMapaContenido nodoMapaContenidoAux =
                nodoMapaContenidoDAO.get(NodoMapaContenido.class, nodoMapaContenido.getId()).get(0);

        Assert.assertEquals(nodoMapaContenidoAux.getId(), nodoMapaContenido.getId());
    }

    @Test
    public void nodoMapaContenidoTieneUnTipoNormalPorElContenidoInsertado()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        nodoMapaContenido =
                new NodoMapaContenidoBuilder(nodoMapaContenidoDAO).withEstadoModeracion(EstadoModeracion.PENDIENTE)
                        .withMapa(nodoMapa)
                        .withContenido(contenido)
                        .build();

        NodoMapaContenido nodoMapaContenidoNormal = nodoMapaContenidoDAO.getNodoMapaContenidoTipoNormalByContenidoId(
                nodoMapaContenido.getUpoObjeto().getId());

        Assert.assertNotNull(nodoMapaContenidoNormal);
    }
}