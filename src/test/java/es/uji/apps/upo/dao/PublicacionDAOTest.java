package es.uji.apps.upo.dao;

import es.uji.apps.upo.builders.*;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.TipoPlantilla;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class PublicacionDAOTest
{
    @Autowired
    private NodoMapaDAO nodoMapaDAO;

    @Autowired
    private PublicacionDAO publicacionDAO;

    @Autowired
    private PersonaDAO personaDAO;

    @Autowired
    private FranquiciaDAO franquiciaDAO;

    @Autowired
    private MenuDAO menuDAO;

    @Autowired
    private PlantillaDAO plantillaDAO;

    private Persona persona;
    private Franquicia franquicia;
    private NodoMapa nodoMapa;
    private NodoMapa nodoMapaHijo;
    private NodoMapa nodoMapaRaizOriginal;
    private Menu menu;
    private Menu menuActualizado;
    private Plantilla plantilla;

    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    String raiz = "/" + UUID.randomUUID().toString() + "/";

    @Before
    @Transactional
    public void insertarNodoMapa()
    {
        new NodoMapa().setNodoMapaDAO(nodoMapaDAO);

        menu = new MenuBuilder(menuDAO).withNombre("menu").build();
        menuActualizado = new MenuBuilder(menuDAO).withNombre("menuActualizado").build();

        persona = new PersonaBuilder(personaDAO).getAny();

        franquicia = new FranquiciaBuilder(franquiciaDAO).withNombre("Franquicia").build();

        plantilla = new PlantillaBuilder(plantillaDAO).withNombre("plantilla").withTipo(TipoPlantilla.WEB).build();

        new FranquiciaAccesoBuilder(franquiciaDAO).withFranquicia(franquicia).withPersona(persona).build();

        nodoMapaRaizOriginal = nodoMapaDAO.getNodoMapaById(0L).get(0);

        nodoMapa = new NodoMapaBuilder(nodoMapaDAO)
                .withUrlPath("nodo")
                .withUrlCompleta(raiz + "nodo/")
                .withFranquicia(franquicia)
                .withMenu(menu)
                .withOrden(1L)
                .withParent(nodoMapaRaizOriginal)
                .build();

        nodoMapaHijo = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodo2")
                .withUrlCompleta(raiz + "nodo/nodo2/")
                .withFranquicia(franquicia)
                .withParent(nodoMapa)
                .withMenu(menu)
                .withOrden(1L)
                .build();
    }

    @Test
    public void alRecuperarUnNodoHeredableDebeDevolverLaUrlMasProxima()
    {
        NodoMapa nodoMapaMegabanner = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("megabanner")
                .withUrlCompleta(raiz + "nodo/nodo2/megabanner/")
                .withFranquicia(franquicia)
                .withParent(nodoMapaHijo)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        String url = publicacionDAO.getUrlNodoHeredado("megabanner", raiz + "nodo/nodo2/nodo3/");

        Assert.assertTrue(url.equals(raiz + "nodo/nodo2/megabanner/"));

        deleteNodoMapa(nodoMapaMegabanner.getId());
    }

    @Transactional
    private void deleteNodoMapa(Long id)
    {
        nodoMapaDAO.delete(NodoMapa.class, id);
    }

    @After
    @Transactional
    public void clean()
    {
        nodoMapaDAO.delete(NodoMapa.class, nodoMapaHijo.getId());
        nodoMapaDAO.delete(NodoMapa.class, nodoMapa.getId());

        nodoMapaDAO.delete(Franquicia.class, franquicia.getId());
        nodoMapaDAO.delete(Menu.class, menu.getId());
        nodoMapaDAO.delete(Menu.class, menuActualizado.getId());
        nodoMapaDAO.delete(Plantilla.class, plantilla.getId());
    }
}