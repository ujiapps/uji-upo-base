package es.uji.apps.upo.dao;

import es.uji.apps.upo.builders.*;
import es.uji.apps.upo.exceptions.*;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.EstadoModeracion;
import es.uji.apps.upo.model.enums.IdiomaPublicacion;
import es.uji.apps.upo.model.enums.TipoReferenciaContenido;
import es.uji.apps.upo.services.ContenidoService;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.io.PipedOutputStream;
import java.util.List;
import java.util.UUID;

@Rollback
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
@Transactional
public class ContenidoDAOTest
{
    @Autowired
    private ContenidoDAO contenidoDAO;

    @Autowired
    private PersonaDAO personaDAO;

    @Autowired
    private ContenidoIdiomaDAO contenidoIdiomaDAO;

    @Autowired
    private IdiomaDAO idiomaDAO;

    @Autowired
    private NodoMapaDAO nodoMapaDAO;

    @Autowired
    private NodoMapaContenidoDAO nodoMapaContenidoDAO;

    @Autowired
    private FranquiciaDAO franquiciaDAO;

    @Autowired
    private ContenidoService contenidoService;

    @Autowired
    private MenuDAO menuDAO;

    private Contenido contenido;
    private Persona persona;
    private Idioma idioma;
    private NodoMapa nodoMapa;
    private Franquicia franquicia;
    private Menu menu;
    private NodoMapaContenido nodoMapaContenido;

    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    String raiz = "/" + UUID.randomUUID().toString() + "/";

    @Before
    public void init()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        new Contenido().setNodoMapaContenidoDAO(nodoMapaContenidoDAO);
        new NodoMapa().setNodoMapaDAO(nodoMapaDAO);

        idioma = new IdiomaBuilder(idiomaDAO).withNombre("Español")
                .withCodigoISO(IdiomaPublicacion.ES.toString())
                .build();

        persona = new PersonaBuilder(personaDAO).getAny();

        contenido = new ContenidoBuilder(contenidoDAO).withPersonaResponsable(persona).withUrlPath("url").build();

        new ContenidoIdiomaBuilder(contenidoIdiomaDAO).withHtml("S")
                .withTitulo("titulo")
                .withIdioma(idioma)
                .withContenido(contenido)
                .build();

        idioma = new IdiomaBuilder(idiomaDAO).withNombre("Ingles")
                .withCodigoISO(IdiomaPublicacion.EN.toString())
                .build();

        new ContenidoIdiomaBuilder(contenidoIdiomaDAO).withHtml("S")
                .withTitulo("titulo")
                .withIdioma(idioma)
                .withContenido(contenido)
                .build();

        franquicia = new FranquiciaBuilder(franquiciaDAO).withNombre("Franquicia").build();

        menu = new MenuBuilder(menuDAO).withNombre("menu").build();

        nodoMapa = new NodoMapaBuilder(nodoMapaDAO).withParent()
                .withUrlPath("raiz")
                .withUrlCompleta(raiz + "raiz/")
                .withFranquicia(franquicia)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        nodoMapaContenido = new NodoMapaContenidoBuilder(nodoMapaContenidoDAO).withTipo(TipoReferenciaContenido.NORMAL)
                .withEstadoModeracion(EstadoModeracion.ACEPTADO)
                .withContenido(contenido)
                .withMapa(nodoMapa)
                .build();
    }

    @Test
    public void testsInsertado()
    {
        new FranquiciaAccesoBuilder(franquiciaDAO).withFranquicia(franquicia).withPersona(persona).build();

        Long contenidoId = contenido.getId();

        List<Contenido> contenido = contenidoDAO.get(Contenido.class, contenidoId);

        Assert.assertEquals(1, contenido.size());

        List<ContenidoIdioma> contenidoIdiomas = contenidoDAO.get(ContenidoIdioma.class, "upoObjeto = " + contenidoId);

        Assert.assertEquals(2, contenidoIdiomas.size());
    }

    @Test
    public void testsBorrado()
            throws BorradoContenidoNoAutorizadoException
    {
        new FranquiciaAccesoBuilder(franquiciaDAO).withFranquicia(franquicia).withPersona(persona).build();

        contenidoService.deleteByPersonaAndNodoMapa(contenido, persona.getId(), nodoMapa.getId());

        Long contenidoId = contenido.getId();

        List<Contenido> contenido = contenidoDAO.get(Contenido.class, contenidoId);

        Assert.assertEquals(0, contenido.size());

        List<ContenidoIdioma> contenidoIdiomas = contenidoDAO.get(ContenidoIdioma.class, "upoObjeto = " + contenidoId);

        Assert.assertEquals(0, contenidoIdiomas.size());
    }

    @Test(expected = BorradoContenidoNoAutorizadoException.class)
    public void testsBorradoNoAutorizado()
            throws BorradoContenidoNoAutorizadoException
    {
        contenidoService.deleteByPersonaAndNodoMapa(contenido, persona.getId(), nodoMapa.getId());
    }

    @Test
    public void testsBorradoPorSerQuienPropone()
            throws BorradoContenidoNoAutorizadoException
    {
        nodoMapaContenido.setPersonaPropuesta(persona);

        contenidoService.deleteByPersonaAndNodoMapa(contenido, persona.getId(), nodoMapa.getId());
    }

    @Test(expected = ContenidoConMismoUrlPathException.class)
    public void NoSeDeberiaInsertarUnContenidoConUnUrlPathExistenteEnElMismoNodo()
            throws InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException,
            ContenidoConMismoUrlPathException, NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        new FranquiciaAccesoBuilder(franquiciaDAO).withFranquicia(franquicia).withPersona(persona).build();

        Contenido contenido =
                new ContenidoBuilder(contenidoDAO).withPersonaResponsable(persona).withUrlPath("url").build();

        new NodoMapaContenidoBuilder(nodoMapaContenidoDAO).withTipo(TipoReferenciaContenido.NORMAL)
                .withEstadoModeracion(EstadoModeracion.ACEPTADO)
                .withContenido(contenido)
                .withMapa(nodoMapa)
                .build();

        contenidoService.insert(contenido, persona, nodoMapa);
    }

    @Test(expected = ContenidoConMismoUrlPathException.class)
    public void NoSeDeberiaActualizarUnContenidoConUnUrlPathExistenteEnElMismoNodo()
            throws ContenidoConMismoUrlPathException, NodoMapaContenidoDebeTenerUnContenidoNormalException,
            ActualizadoContenidoNoAutorizadoException, AccesibilidadException, IdiomaObligatorioException
    {
        new FranquiciaAccesoBuilder(franquiciaDAO).withFranquicia(franquicia).withPersona(persona).build();

        Contenido contenido =
                new ContenidoBuilder(contenidoDAO).withPersonaResponsable(persona).withUrlPath("url2").build();

        new NodoMapaContenidoBuilder(nodoMapaContenidoDAO).withTipo(TipoReferenciaContenido.NORMAL)
                .withEstadoModeracion(EstadoModeracion.ACEPTADO)
                .withContenido(contenido)
                .withMapa(nodoMapa)
                .build();

        contenido.setUrlPath("url");

        contenidoService.update(contenido, persona, nodoMapa);
    }
}