package es.uji.apps.upo.dao;

import es.uji.apps.upo.builders.*;
import es.uji.apps.upo.exceptions.BorradoContenidoNoAutorizadoException;
import es.uji.apps.upo.exceptions.NodoMapaContenidoDebeTenerUnContenidoNormalException;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.EstadoModeracion;
import es.uji.apps.upo.model.enums.TipoReferenciaContenido;
import es.uji.apps.upo.services.nodomapa.NodoMapaService;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
@Transactional
public class DeleteNodoMapaTest
{
    public static final String URL_COMPLETA_PAPELERA = "/paperera/";
    @Autowired
    private NodoMapaDAO nodoMapaDAO;

    @Autowired
    private PersonaDAO personaDAO;

    @Autowired
    private FranquiciaDAO franquiciaDAO;

    @Autowired
    private ContenidoDAO contenidoDAO;

    @Autowired
    private ContenidoLogDAO contenidoLogDAO;

    @Autowired
    private NodoMapaContenidoDAO nodoMapaContenidoDAO;

    @Autowired
    private NodoMapaService nodoMapaService;

    @Autowired
    private MenuDAO menuDAO;

    private Persona persona;
    private Franquicia franquiciaAdmin;
    private Franquicia franquiciaNoAdmin;
    private NodoMapa nodoMapa;
    private NodoMapa nodoMapaRaiz;
    private NodoMapa nodoMapaPapelera;
    private NodoMapa nodoMapaHijoPapelera;
    private NodoMapa nodoMapaNietoPapelera;
    private Contenido contenido1;
    private Contenido contenido2;
    private NodoMapaContenido nodoMapaContenidoEntreNodoMapayContenido1;
    private NodoMapaContenido nodoMapaContenidoEntreNodoMapaHijoyContenido2;
    private NodoMapaContenido nodoMapaContenidoEntreNodoMapayContenido2;
    private Menu menu;

    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    String raizPaperera = URL_COMPLETA_PAPELERA + UUID.randomUUID().toString() + "/";
    String raiz = "/" + UUID.randomUUID().toString() + "/";

    @Before
    public void insertarNodoMapa()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        new Contenido().setNodoMapaContenidoDAO(nodoMapaContenidoDAO);
        new NodoMapa().setNodoMapaDAO(nodoMapaDAO);

        persona = new PersonaBuilder(personaDAO).getAny();

        franquiciaAdmin = new FranquiciaBuilder(franquiciaDAO).withNombre("Franquicia").build();
        franquiciaNoAdmin = new FranquiciaBuilder(franquiciaDAO).withNombre("Franquicia").build();

        menu = new MenuBuilder(menuDAO).withNombre("menu").build();

        new FranquiciaAccesoBuilder(franquiciaDAO).withFranquicia(franquiciaAdmin).withPersona(persona).build();

        nodoMapaRaiz = new NodoMapaBuilder(nodoMapaDAO).withParent()
                .withUrlPath(raiz)
                .withUrlCompleta(raiz)
                .withFranquicia(franquiciaNoAdmin)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        nodoMapaPapelera = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodo")
                .withUrlCompleta(raizPaperera + "nodo/")
                .withFranquicia(franquiciaNoAdmin)
                .withParent(nodoMapaRaiz)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        nodoMapaHijoPapelera = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodo2")
                .withUrlCompleta(raizPaperera + "nodo/nodo2/")
                .withFranquicia(franquiciaAdmin)
                .withParent(nodoMapaPapelera)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        nodoMapaNietoPapelera = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodo3")
                .withUrlCompleta(raizPaperera + "nodo/nodo2/nodo3/")
                .withFranquicia(franquiciaAdmin)
                .withParent(nodoMapaHijoPapelera)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        nodoMapaPapelera.setUpoMapas(Collections.singleton(nodoMapaHijoPapelera));
        nodoMapaHijoPapelera.setUpoMapas(Collections.singleton(nodoMapaNietoPapelera));

        contenido1 = new ContenidoBuilder(contenidoDAO).withUrlPath("c1").build();

        contenido2 = new ContenidoBuilder(contenidoDAO).withUrlPath("c2").build();

        nodoMapaContenidoEntreNodoMapayContenido1 =
                new NodoMapaContenidoBuilder(nodoMapaContenidoDAO).withTipo(TipoReferenciaContenido.NORMAL)
                        .withEstadoModeracion(EstadoModeracion.ACEPTADO)
                        .withContenido(contenido1)
                        .withMapa(nodoMapaHijoPapelera)
                        .build();

        nodoMapaContenidoEntreNodoMapaHijoyContenido2 =
                new NodoMapaContenidoBuilder(nodoMapaContenidoDAO).withTipo(TipoReferenciaContenido.NORMAL)
                        .withEstadoModeracion(EstadoModeracion.ACEPTADO)
                        .withContenido(contenido2)
                        .withMapa(nodoMapaNietoPapelera)
                        .build();

        nodoMapaContenidoEntreNodoMapayContenido2 =
                new NodoMapaContenidoBuilder(nodoMapaContenidoDAO).withTipo(TipoReferenciaContenido.LINK)
                        .withEstadoModeracion(EstadoModeracion.ACEPTADO)
                        .withContenido(contenido2)
                        .withMapa(nodoMapaHijoPapelera)
                        .build();

        nodoMapa = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodo")
                .withUrlCompleta(raiz + "nodo/")
                .withFranquicia(franquiciaAdmin)
                .withMenu(menu)
                .withOrden(1L)
                .withParent(nodoMapaRaiz)
                .build();

        franquiciaAdmin.setUpoMapas(null);
        franquiciaNoAdmin.setUpoMapas(null);
        menu.setNodoMapa(null);
    }

    @Test(expected = BorradoContenidoNoAutorizadoException.class)
    public void NoSePuedeBorrarUnNodoSiNoEresAdministradorDelPadre()
            throws Exception
    {
        Franquicia franquiciaSinPermiso =
                new FranquiciaBuilder(franquiciaDAO).withNombre("FranquiciaSinPermiso").build();

        NodoMapa nodoMapaNoAdministrador = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodoNoAdmin")
                .withParent()
                .withUrlCompleta(raiz + "nodoNoAdmin/")
                .withFranquicia(franquiciaSinPermiso)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        NodoMapa nodoMapaAdministrador = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodoAdmin")
                .withUrlCompleta(raiz + "nodoAdmin/")
                .withFranquicia(franquiciaAdmin)
                .withMenu(menu)
                .withOrden(1L)
                .withParent(nodoMapaNoAdministrador)
                .build();

        nodoMapaService.deleteNodoMapa(nodoMapaAdministrador, persona);
    }

    @Test(expected = BorradoContenidoNoAutorizadoException.class)
    public void NoSePuedeBorrarUnNodoSiElPadreEstaBloqueado()
            throws Exception
    {
        nodoMapaRaiz.setUpoFranquicia(franquiciaAdmin);
        nodoMapaRaiz.setBloqueado(true);

        nodoMapaService.deleteNodoMapa(nodoMapa, persona);
    }

    public void SePuedeBorrarNodoSiLoHasPropuestoPorError()
            throws Exception
    {
        Franquicia franquiciaSinPermiso =
                new FranquiciaBuilder(franquiciaDAO).withNombre("FranquiciaSinPermiso").build();

        NodoMapa nodoMapaNoAdministrador = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodoNoAdmin")
                .withParent()
                .withUrlCompleta(raiz + "nodoNoAdmin/")
                .withFranquicia(franquiciaSinPermiso)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        nodoMapaNoAdministrador.setBloqueado(true);

        NodoMapa nodoMapaNoAdministradorHijo = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodoAdmin")
                .withUrlCompleta(raiz + "nodoAdmin/")
                .withFranquicia(franquiciaNoAdmin)
                .withMenu(menu)
                .withOrden(1L)
                .withParent(nodoMapaNoAdministrador)
                .build();

        nodoMapaNoAdministradorHijo.setBloqueado(true);
        nodoMapaNoAdministradorHijo.setPersonaPropuesta(persona);

        nodoMapaService.deleteNodoMapa(nodoMapaNoAdministradorHijo, persona);
    }

    @Test
    public void elBorraroDeUnNodoDebeGenerarUnNuevoNodoEnPapelera()
            throws Exception
    {
        nodoMapaRaiz.setUpoFranquicia(franquiciaAdmin);
        NodoMapa nodoMapaPaperera = nodoMapaDAO.getNodoMapaByUrlCompleta(URL_COMPLETA_PAPELERA);
        List<NodoMapa> nodosMapaEnPapeleraInicio = nodoMapaDAO.getHijos(nodoMapaPaperera.getId());

        nodoMapaService.deleteNodoMapa(nodoMapa, persona);

        List<NodoMapa> nodosMapaEnPapeleraDespues = nodoMapaDAO.getHijos(nodoMapaPaperera.getId());

        Assert.assertTrue(nodosMapaEnPapeleraInicio.size() == nodosMapaEnPapeleraDespues.size() - 1);
    }

    @Test
    public void elBorraroDeUnNodoDebeTrasladarloALaPapelera()
            throws Exception
    {
        nodoMapaRaiz.setUpoFranquicia(franquiciaAdmin);
        nodoMapaService.deleteNodoMapa(nodoMapa, persona);

        Assert.assertTrue(nodoMapa.getUrlCompleta().startsWith(URL_COMPLETA_PAPELERA));
    }

    @Test
    public void siSeBorraUnNodoDeLaPapeleraDebenDesaparecerTodosLosNodoMapaContenidoDeEsteYDeSusHijos()
            throws Exception
    {
        nodoMapaPapelera.setUpoFranquicia(franquiciaAdmin);
        nodoMapaService.deleteNodoMapa(nodoMapaHijoPapelera, persona);

        List<NodoMapaContenido> listaNodoMapaContenido =
                nodoMapaContenidoDAO.get(NodoMapaContenido.class, nodoMapaContenidoEntreNodoMapayContenido1.getId());

        Assert.assertEquals(0, listaNodoMapaContenido.size());

        listaNodoMapaContenido = nodoMapaContenidoDAO.get(NodoMapaContenido.class,
                nodoMapaContenidoEntreNodoMapaHijoyContenido2.getId());

        Assert.assertEquals(0, listaNodoMapaContenido.size());

        listaNodoMapaContenido =
                nodoMapaContenidoDAO.get(NodoMapaContenido.class, nodoMapaContenidoEntreNodoMapayContenido2.getId());

        Assert.assertEquals(0, listaNodoMapaContenido.size());
    }

    @Test
    public void siSeBorraUnNodoDeLaPapeleraDebenDesaparecerTodosLosContenidoEnlazadosaTravesDeUnNodoMapaContenidoDeTipoNormalDeEsteYDeSusNodosHijos()
            throws Exception
    {
        nodoMapaPapelera.setUpoFranquicia(franquiciaAdmin);
        nodoMapaService.deleteNodoMapa(nodoMapaHijoPapelera, persona);

        List<Contenido> listaContenido = contenidoDAO.get(Contenido.class, contenido1.getId());

        Assert.assertEquals(0, listaContenido.size());

        listaContenido = contenidoDAO.get(Contenido.class, contenido2.getId());

        Assert.assertEquals(0, listaContenido.size());
    }

    @Test
    public void siSeBorranContenidosDebidoAlBorradoDeNodosDeLaPapeleraDebenDesaparecerLosNodoMapaContenidosDeTipoLinkDeOtrosNodos()
            throws Exception
    {
        nodoMapaPapelera.setUpoFranquicia(franquiciaAdmin);
        NodoMapa otroNodoMapa = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodo3")
                .withParent()
                .withUrlCompleta("/otroNodo/")
                .withFranquicia(franquiciaAdmin)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        NodoMapaContenido nodoMapaContenido =
                new NodoMapaContenidoBuilder(nodoMapaContenidoDAO).withTipo(TipoReferenciaContenido.LINK)
                        .withEstadoModeracion(EstadoModeracion.ACEPTADO)
                        .withContenido(contenido1)
                        .withMapa(otroNodoMapa)
                        .build();

        nodoMapaService.deleteNodoMapa(nodoMapaHijoPapelera, persona);

        List<NodoMapaContenido> listaNodoMapaContenido =
                nodoMapaContenidoDAO.get(NodoMapaContenido.class, nodoMapaContenido.getId());

        Assert.assertEquals(0, listaNodoMapaContenido.size());
    }

    @Test
    public void unContenidoNoDebeSerBorradoSiTieneUnEnlaceTipoNormalFueraDelArbolABorrar()
            throws Exception
    {
        nodoMapaPapelera.setUpoFranquicia(franquiciaAdmin);
        NodoMapa otroNodoMapa = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodo3")
                .withParent()
                .withUrlCompleta("/otroNodo/")
                .withFranquicia(franquiciaAdmin)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        Contenido contenido3 = new ContenidoBuilder(contenidoDAO).withUrlPath("c10").build();

        new NodoMapaContenidoBuilder(nodoMapaContenidoDAO).withTipo(TipoReferenciaContenido.LINK)
                .withEstadoModeracion(EstadoModeracion.ACEPTADO)
                .withContenido(contenido3)
                .withMapa(nodoMapaNietoPapelera)
                .build();

        contenido3.setUpoMapasObjetos(null);

        NodoMapaContenido nodoMapaContenidoNormal =
                new NodoMapaContenidoBuilder(nodoMapaContenidoDAO).withTipo(TipoReferenciaContenido.NORMAL)
                        .withEstadoModeracion(EstadoModeracion.ACEPTADO)
                        .withContenido(contenido3)
                        .withMapa(otroNodoMapa)
                        .build();

        nodoMapaService.deleteNodoMapa(nodoMapaHijoPapelera, persona);

        List<NodoMapaContenido> listaNodoMapaContenido =
                nodoMapaContenidoDAO.get(NodoMapaContenido.class, nodoMapaContenidoNormal.getId());

        Assert.assertEquals(1, listaNodoMapaContenido.size());

        List<Contenido> listaContenidos = contenidoDAO.get(Contenido.class, contenido3.getId());

        Assert.assertEquals(1, listaContenidos.size());
    }
}