package es.uji.apps.upo.publicacion.formato.Metadato;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import es.uji.apps.upo.model.Publicacion;
import es.uji.apps.upo.services.rest.publicacion.formato.MetadataExtractor;
import es.uji.apps.upo.services.rest.publicacion.formato.Metadato;

public class MetadatoExtractorTest
{
    MetadataExtractor metadataExtractor = new MetadataExtractor();
    Publicacion publicacion;

    @Test
    public void elNoHayMetadatosDebeDevolverUnaListaVacia()
    {
        publicacion = new Publicacion();

        List<Metadato> metadatos = metadataExtractor.extrae(publicacion);

        Assert.assertTrue(metadatos.isEmpty());
    }

    @Test
    public void elExtractorDeMetadatosDebeTenerEnCuentaElFormato()
    {
        publicacion = new Publicacion();
        publicacion.setMetadata("clave1@@@titulo1@@@valor1@@@1~~~clave2@@@titulo2@@@valor2@@@0");

        List<Metadato> metadatos = metadataExtractor.extrae(publicacion);

        Assert.assertTrue(metadatos.size() == 2);
        Assert.assertTrue(metadatos.get(0).getClave().equals("clave1"));
        Assert.assertTrue(metadatos.get(0).getValor().equals("valor1"));
        Assert.assertTrue(metadatos.get(1).getTitulo().equals("titulo2"));
        Assert.assertTrue(metadatos.get(1).getPublicable().equals(false));
    }
}
