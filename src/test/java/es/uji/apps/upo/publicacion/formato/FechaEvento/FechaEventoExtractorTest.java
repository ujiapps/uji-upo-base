package es.uji.apps.upo.publicacion.formato.FechaEvento;

import es.uji.apps.upo.model.Publicacion;
import es.uji.apps.upo.services.rest.publicacion.formato.FechaEvento;
import es.uji.apps.upo.services.rest.publicacion.formato.FechasEventoExtractor;
import es.uji.apps.upo.utils.DateUtils;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.util.List;

public class FechaEventoExtractorTest
{
    FechasEventoExtractor fechasEventoExtractor = new FechasEventoExtractor();
    Publicacion publicacion;

    @Test
    public void siNoHayFechasEventoDebeDevolverUnaListaVacia()
            throws ParseException
    {
        publicacion = new Publicacion();

        List<FechaEvento> fechasEvento = fechasEventoExtractor.extrae(publicacion);

        Assert.assertTrue(fechasEvento.isEmpty());
    }

    @Test
    public void elExtractorDeFechasEventosDebeTenerEnCuentaElFormato()
            throws ParseException
    {
        publicacion = new Publicacion();
        publicacion.setFechasEvento("1/1/2020@@@@@@~~~1/2/2020@@@14:30@@@15:15~~~1/2/2020@@@14:30@@@~~~1/2/2020@@@@@@15:15");

        List<FechaEvento> fechasEvento = fechasEventoExtractor.extrae(publicacion);

        Assert.assertTrue(fechasEvento.size() == 4);

        Assert.assertTrue(DateUtils.convertDateToStringToPrint(fechasEvento.get(0).getFecha()).equals("1/1/2020"));
        Assert.assertNull(fechasEvento.get(0).getHoraInicio());
        Assert.assertNull(fechasEvento.get(0).getHoraFin());

        Assert.assertTrue(DateUtils.convertDateToStringToPrint(fechasEvento.get(1).getFecha()).equals("1/2/2020"));
        Assert.assertTrue(DateUtils.convertTimeToString(fechasEvento.get(1).getHoraInicio()).equals("14:30:00"));
        Assert.assertTrue(DateUtils.convertTimeToString(fechasEvento.get(1).getHoraFin()).equals("15:15:00"));

        Assert.assertTrue(DateUtils.convertDateToStringToPrint(fechasEvento.get(2).getFecha()).equals("1/2/2020"));
        Assert.assertTrue(DateUtils.convertTimeToString(fechasEvento.get(2).getHoraInicio()).equals("14:30:00"));
        Assert.assertNull(DateUtils.convertTimeToString(fechasEvento.get(2).getHoraFin()));

        Assert.assertTrue(DateUtils.convertDateToStringToPrint(fechasEvento.get(3).getFecha()).equals("1/2/2020"));
        Assert.assertNull(DateUtils.convertTimeToString(fechasEvento.get(3).getHoraInicio()));
        Assert.assertTrue(DateUtils.convertTimeToString(fechasEvento.get(3).getHoraFin()).equals("15:15:00"));
    }
}
