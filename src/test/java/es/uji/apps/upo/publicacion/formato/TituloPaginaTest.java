package es.uji.apps.upo.publicacion.formato;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.upo.services.rest.publicacion.model.PublicacionDetails;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.model.Publicacion;
import es.uji.apps.upo.services.rest.publicacion.formato.FormatoPublicacionHtml;

public class TituloPaginaTest
{
    FormatoPublicacionHtml formatoPublicacionHtml = new FormatoPublicacionHtml();
    PublicacionDetails publicacionDetails;

    @Before
    public void init()
    {
        publicacionDetails = new PublicacionDetails();

        publicacionDetails.setUrl("/");
        publicacionDetails.setContenidos(new ArrayList<Publicacion>());
        publicacionDetails.setIdioma("ca");
    }

    @Test
    public void siElNodoMapaTieneUnTituloEnElIdioma()
    {
        NodoMapa nodoMapa = new NodoMapa();
        nodoMapa.setTituloPublicacionCA("tituloCA");
        nodoMapa.setTituloPublicacionES("tituloES");
        nodoMapa.setTituloPublicacionEN("tituloEN");

        publicacionDetails.setNodoMapa(nodoMapa);

        String titulo = formatoPublicacionHtml.getTitulo(publicacionDetails);

        Assert.assertTrue("tituloCA".equals(titulo));
    }

    @Test
    public void siNoHayTituloEnElNodoDebeRecuperarElTituloDeUnUnicoContenidoHtml()
    {
        NodoMapa nodoMapa = new NodoMapa();
        nodoMapa.setTituloPublicacionCA(null);
        nodoMapa.setTituloPublicacionES("tituloES");
        nodoMapa.setTituloPublicacionEN("tituloEN");

        List<Publicacion> contenidos = new ArrayList<Publicacion>();

        Publicacion publicacion = new Publicacion();
        publicacion.setUrlCompleta("/prova.html");
        publicacion.setIdioma("ca");
        publicacion.setEsHtml("S");
        publicacion.setUrlNodo("/");
        publicacion.setTituloLargo("tituloContenido");

        contenidos.add(publicacion);

        publicacionDetails.setNodoMapa(nodoMapa);
        publicacionDetails.setContenidos(contenidos);

        String titulo = formatoPublicacionHtml.getTitulo(publicacionDetails);

        Assert.assertTrue("tituloContenido".equals(titulo));
    }

    @Test
    public void siHayMasDeUnContenidoNoDevuelveTitulo()
    {
        NodoMapa nodoMapa = new NodoMapa();
        nodoMapa.setTituloPublicacionCA(null);
        nodoMapa.setTituloPublicacionES("tituloES");
        nodoMapa.setTituloPublicacionEN("tituloEN");

        List<Publicacion> contenidos = new ArrayList<Publicacion>();

        Publicacion publicacion = new Publicacion();
        publicacion.setUrlCompleta("/prova.html");
        publicacion.setIdioma("ca");
        publicacion.setEsHtml("S");
        publicacion.setUrlNodo("/");
        publicacion.setTituloLargo("tituloContenido");

        Publicacion publicacion2 = new Publicacion();
        publicacion2.setUrlCompleta("/prova2.html");
        publicacion2.setIdioma("ca");
        publicacion2.setEsHtml("S");
        publicacion2.setUrlNodo("/");
        publicacion2.setTituloLargo("tituloContenido2");

        contenidos.add(publicacion);
        contenidos.add(publicacion2);

        publicacionDetails.setNodoMapa(nodoMapa);
        publicacionDetails.setContenidos(contenidos);

        String titulo = formatoPublicacionHtml.getTitulo(publicacionDetails);

        Assert.assertNull(titulo);
    }
}
