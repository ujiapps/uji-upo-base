package es.uji.apps.upo.publicacion.formato;

import es.uji.apps.upo.model.Publicacion;
import es.uji.apps.upo.services.rest.publicacion.formato.AtributoExtractor;
import es.uji.apps.upo.services.rest.publicacion.formato.FechasEventoExtractor;
import es.uji.apps.upo.services.rest.publicacion.formato.FormatoPublicacionHtml;
import es.uji.apps.upo.services.rest.publicacion.formato.MetadataExtractor;
import es.uji.apps.upo.services.rest.publicacion.formato.filtro.UrlTagsContentReplacer;
import es.uji.apps.upo.services.rest.publicacion.model.RequestDetails;
import es.uji.commons.web.template.model.Recurso;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RecursoTest
{
    private FormatoPublicacionHtml formatoPublicacionHtml;
    private MetadataExtractor metadataExtractor;
    private AtributoExtractor atributoExtractor;
    private FechasEventoExtractor fechasEventoExtractor;
    private UrlTagsContentReplacer urlTagsContentReplacer;
    private Publicacion publicacion;
    private String textoVisible;
    private RequestDetails requestDetails;

    @Before
    public void init()
            throws UnsupportedEncodingException
    {
        formatoPublicacionHtml = new FormatoPublicacionHtml();
        requestDetails = new RequestDetails();

        atributoExtractor = mock(AtributoExtractor.class);
        metadataExtractor = mock(MetadataExtractor.class);
        fechasEventoExtractor = mock(FechasEventoExtractor.class);
        urlTagsContentReplacer = mock(UrlTagsContentReplacer.class);

        formatoPublicacionHtml.setMetadataExtractor(metadataExtractor);
        formatoPublicacionHtml.setAtributoExtractor(atributoExtractor);
        formatoPublicacionHtml.setFechasEventoExtractor(fechasEventoExtractor);
        formatoPublicacionHtml.setUrlTagsContentReplacer(urlTagsContentReplacer);

        textoVisible = "texto vivible";

        publicacion = new Publicacion();
        publicacion.setEsHtml("S");
        publicacion.setVisible("N");
        publicacion.setPublicable("S");
        publicacion.setTextoNoVisible("No visible");
        publicacion.setContenido(textoVisible.getBytes());

        when(urlTagsContentReplacer.replace(textoVisible.getBytes(), requestDetails, "ca")).thenReturn(
                textoVisible.getBytes());
    }

    @Test
    public void siElRecursoEsNoVisibleDebeDevolverElTextoDeNoVisible()
            throws UnsupportedEncodingException, ParseException
    {
        publicacion.setVisible("N");

        Recurso recurso = formatoPublicacionHtml.createRecurso("/", publicacion, requestDetails, "ca");

        Assert.assertTrue("No visible".equals(recurso.getContenido()));
    }

    @Test
    public void siElRecursoEsVisibleDebeDevolverElTextoNormal()
            throws UnsupportedEncodingException, ParseException
    {
        publicacion.setVisible("S");

        Recurso recurso = formatoPublicacionHtml.createRecurso("/", publicacion, requestDetails, "ca");

        Assert.assertTrue(textoVisible.equals(recurso.getContenido()));
    }

    @Test
    public void siElRecursoNoEsPublicableYVisibleDebeDevolverTextoVacio()
            throws UnsupportedEncodingException, ParseException
    {
        publicacion.setVisible("S");
        publicacion.setPublicable("N");

        Recurso recurso = formatoPublicacionHtml.createRecurso("/", publicacion, requestDetails, "ca");

        Assert.assertTrue("".equals(recurso.getContenido()));
    }

    @Test
    public void siElRecursoNoEsPublicableYNoVisibleDebeDevolverTextoDeNoVisible()
            throws UnsupportedEncodingException, ParseException
    {
        publicacion.setVisible("N");
        publicacion.setPublicable("N");

        Recurso recurso = formatoPublicacionHtml.createRecurso("/", publicacion, requestDetails, "ca");

        Assert.assertTrue("No visible".equals(recurso.getContenido()));
    }
}
