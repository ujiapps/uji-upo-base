package es.uji.apps.upo.publicacion.filtro;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.uji.apps.upo.services.rest.publicacion.formato.filtro.UrlGetContent;
import es.uji.apps.upo.services.rest.publicacion.formato.filtro.UrlTagsContentReplacer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class UrlContentReplacerTest
{
    @Autowired
    UrlTagsContentReplacer urlTagsContentReplacer;

    @Test
    public void elExtractorDeTagsDebeDeExtraerTodasLasURLsDeUnContenido() throws IOException
    {
        String contenido = "inicio: {{http://static.uji.es/js/extjs/extjs-4.2.1/license.txt}} :fin";

        byte[] result = urlTagsContentReplacer.replace(contenido.getBytes());

        String resultString = new String(result);

        Assert.assertTrue(resultString.startsWith("inicio: Ext JS 4.2"));
        Assert.assertTrue(resultString.endsWith(":fin"));
        Assert.assertTrue(resultString.length() == 1495);
    }
}
