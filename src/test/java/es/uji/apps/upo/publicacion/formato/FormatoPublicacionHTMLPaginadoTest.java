package es.uji.apps.upo.publicacion.formato;

import com.sun.jersey.core.spi.factory.ResponseImpl;
import com.sun.syndication.io.FeedException;
import es.uji.apps.upo.model.Menu;
import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.model.Publicacion;
import es.uji.apps.upo.services.PublicacionService;
import es.uji.apps.upo.services.rest.publicacion.MenuFactory;
import es.uji.apps.upo.services.rest.publicacion.formato.*;
import es.uji.apps.upo.services.rest.publicacion.formato.filtro.UrlTagsContentReplacer;
import es.uji.apps.upo.services.rest.publicacion.model.RequestParams;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FormatoPublicacionHTMLPaginadoTest extends FormatoPublicacionAbstratTest
{
    FormatoPublicacionHtmlPaginado formatoPublicacionHtmlPaginado;

    MetadataExtractor metadataExtractor;
    AtributoExtractor atributoExtractor;
    FechasEventoExtractor fechasEventoExtractor;
    UrlTagsContentReplacer urlTagsContentReplacer;
    RequestParams requestParams;
    MigasExtractor migasExtractor;

    @Before
    public void init()
    {
        requestParams = new RequestParams(URL_BASE, URL);

        formatoPublicacionHtmlPaginado = new FormatoPublicacionHtmlPaginado();

        publicacionService = mock(PublicacionService.class);
        atributoExtractor = mock(AtributoExtractor.class);
        metadataExtractor = mock(MetadataExtractor.class);
        fechasEventoExtractor = mock(FechasEventoExtractor.class);
        urlTagsContentReplacer = mock(UrlTagsContentReplacer.class);
        menuFactory = mock(MenuFactory.class);
        migasExtractor = mock(MigasExtractor.class);

        formatoPublicacionHtmlPaginado.setPublicacionService(publicacionService);
        formatoPublicacionHtmlPaginado.setMenuFactory(menuFactory);
        formatoPublicacionHtmlPaginado.setAtributoExtractor(atributoExtractor);
        formatoPublicacionHtmlPaginado.setMetadataExtractor(metadataExtractor);
        formatoPublicacionHtmlPaginado.setFechasEventoExtractor(fechasEventoExtractor);
        formatoPublicacionHtmlPaginado.setUrlTagsContentReplacer(urlTagsContentReplacer);
        formatoPublicacionHtmlPaginado.setMigasExtractor(migasExtractor);
    }

    @Test
    public void laRespuestaHaDeSerSiempreHTML()
            throws MalformedURLException, FeedException, ParseException, UnsupportedEncodingException
    {
        List<Publicacion> contenidos = new ArrayList<Publicacion>();

        Publicacion publicacion = new Publicacion();
        publicacion.setUrlCompleta("/prova.html");
        publicacion.setUrlNodo("/");
        contenidos.add(publicacion);

        NodoMapa nodoMapa = new NodoMapa();
        nodoMapa.setId(1L);
        nodoMapa.setMenu(new Menu());

        Map<String, Object> returnValues = new HashMap();
        returnValues.put("numSearchItems", 1);
        returnValues.put("search", contenidos);

        requestParams.getSearchParams().setNumResultados(10);
        requestParams.getSearchParams().setStartSearch(0);
        requestParams.setNodoMapa(nodoMapa);

        when(publicacionService.getContenidosIdiomasByUrlCompletaAndLanguageAndSearchFields(requestParams)).thenReturn(
                returnValues);

        Response response = formatoPublicacionHtmlPaginado.publica(requestParams);

        Assert.assertTrue(((ResponseImpl) response).getStatusType().equals(Response.Status.OK));
        Assert.assertTrue(getMetadata(response, "Content-type").equals("text/html"));
        Assert.assertTrue(((ResponseImpl) response).getEntityType().toString().contains("HTMLTemplate"));
    }
}
