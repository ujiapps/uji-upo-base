package es.uji.apps.upo.publicacion.formato;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.uji.apps.upo.services.rest.publicacion.formato.FormatoPublicacion;
import es.uji.apps.upo.services.rest.publicacion.formato.FormatoPublicacionBinario;
import es.uji.apps.upo.services.rest.publicacion.formato.FormatoPublicacionFactory;
import es.uji.apps.upo.services.rest.publicacion.formato.FormatoPublicacionHtml;
import es.uji.apps.upo.services.rest.publicacion.formato.FormatoPublicacionHtmlPaginado;
import es.uji.apps.upo.services.rest.publicacion.formato.FormatoPublicacionRss;
import es.uji.apps.upo.services.rest.publicacion.formato.FormatoPublicacionVideo;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class FactoryTest
{
    @Autowired
    FormatoPublicacionFactory formatoPublicacionFactory;

    @Test
    public void siUrlContienePuntoDeberiaSerBinario()
    {
        FormatoPublicacion formato = formatoPublicacionFactory.getFormat("html", "http://urlcon.punto", true);

        assertThat(formato, instanceOf(FormatoPublicacionBinario.class));
    }

    @Test
    public void siUrlNoContienePuntoYNoEsFormatoConocidoDeberiaSerBinario()
    {
        Assert.assertTrue(sonIguales(FormatoPublicacionBinario.class.toString(),
                formatoPublicacionFactory.getFormat("otroFormato", "http://urlsinpunto", true).toString()));
        Assert.assertTrue(sonIguales(FormatoPublicacionBinario.class.toString(),
                formatoPublicacionFactory.getFormat("", "http://urlsinpunto", true).toString()));
    }

    @Test
    public void siSeEspecificaVideooRSSDebeSerDelFormatoCorrespondiente()
    {
        Assert.assertTrue(sonIguales(FormatoPublicacionRss.class.toString(),
                formatoPublicacionFactory.getFormat("rss", "http://urlcon.punto", true).toString()));
        Assert.assertTrue(sonIguales(FormatoPublicacionVideo.class.toString(),
                formatoPublicacionFactory.getFormat("video", "http://urlsinpunto", true).toString()));
    }

    @Test
    public void siSeEspecificaHTMLYLaUrlNoLlevaPuntoDebeDeSerHTML()
    {
        Assert.assertTrue(sonIguales(FormatoPublicacionHtml.class.toString(),
                formatoPublicacionFactory.getFormat("html", "http://urlconsinpunto", false)
                        .toString()));
        Assert.assertTrue(sonIguales(FormatoPublicacionHtmlPaginado.class.toString(),
                formatoPublicacionFactory.getFormat("html", "http://urlconsinpunto", true).toString()));
        Assert.assertTrue(sonIguales(FormatoPublicacionHtml.class.toString(),
                formatoPublicacionFactory.getFormat("html", "http://punto.punto/urlconsinpunto", true).toString()));
    }

    private Boolean sonIguales(String clase, String nombreBean)
    {
        clase = clase.substring(clase.indexOf(" ") + 1);
        return nombreBean.contains(clase);
    }
}
