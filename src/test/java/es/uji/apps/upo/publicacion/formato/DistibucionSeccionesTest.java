package es.uji.apps.upo.publicacion.formato;

import es.uji.apps.upo.model.Menu;
import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.model.Publicacion;
import es.uji.apps.upo.services.PublicacionService;
import es.uji.apps.upo.services.rest.publicacion.MenuFactory;
import es.uji.apps.upo.services.rest.publicacion.formato.*;
import es.uji.apps.upo.services.rest.publicacion.formato.filtro.UrlTagsContentReplacer;
import es.uji.apps.upo.services.rest.publicacion.model.PublicacionDetails;
import es.uji.apps.upo.services.rest.publicacion.model.RequestDetails;
import es.uji.commons.web.template.model.Pagina;
import es.uji.commons.web.template.model.Seccion;
import org.junit.Before;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DistibucionSeccionesTest
{
    private FormatoPublicacionHtml formatoPublicacionHtml;
    private FormatoPublicacionHtmlPaginado formatoPublicacionHtmlPaginado;

    private MetadataExtractor metadataExtractor;
    private AtributoExtractor atributoExtractor;
    private FechasEventoExtractor fechasEventoExtractor;
    private UrlTagsContentReplacer urlTagsContentReplacer;
    private PublicacionService publicacionService;
    private MenuFactory menuFactory;
    private MigasExtractor migasExtractor;

    @Before
    public void init()
    {
        atributoExtractor = mock(AtributoExtractor.class);
        metadataExtractor = mock(MetadataExtractor.class);
        fechasEventoExtractor = mock(FechasEventoExtractor.class);
        urlTagsContentReplacer = mock(UrlTagsContentReplacer.class);
        publicacionService = mock(PublicacionService.class);
        menuFactory = mock(MenuFactory.class);
        migasExtractor = mock(MigasExtractor.class);

        when(publicacionService.getNodoMapaByUrlCompleta(anyString())).thenReturn(new NodoMapa());
    }

    @Test
    public void distribucionSeccionesHtml()
            throws ParseException, MalformedURLException, UnsupportedEncodingException
    {
        formatoPublicacionHtml = new FormatoPublicacionHtml();

        formatoPublicacionHtml.setMenuFactory(menuFactory);
        formatoPublicacionHtml.setPublicacionService(publicacionService);
        formatoPublicacionHtml.setMetadataExtractor(metadataExtractor);
        formatoPublicacionHtml.setFechasEventoExtractor(fechasEventoExtractor);
        formatoPublicacionHtml.setUrlTagsContentReplacer(urlTagsContentReplacer);
        formatoPublicacionHtml.setAtributoExtractor(atributoExtractor);
        formatoPublicacionHtml.setMigasExtractor(migasExtractor);

        List<Publicacion> contenidos = getContenidosPublicacionHtml();

        String urlBase = "http://localhost:9005";
        String url = "/voxuji/1/";
        String idioma = "ca";

        Menu menu = new Menu();
        NodoMapa nodoMapa = new NodoMapa();
        nodoMapa.setMenu(menu);

        PublicacionDetails publicacionDetails = new PublicacionDetails();
        publicacionDetails.setUrl(url);
        publicacionDetails.setUrlBase(urlBase);
        publicacionDetails.setContenidos(contenidos);
        publicacionDetails.setNodoMapa(nodoMapa);
        publicacionDetails.setRequestDetails(new RequestDetails());
        publicacionDetails.setIdioma(idioma);

        Pagina pagina = formatoPublicacionHtml.buildPagina(publicacionDetails);

        Seccion principal = pagina.getSecciones().get(0);
        Seccion publicitat = pagina.getSecciones().get(1);

        assertThat(pagina.getSecciones(), hasSize(2));
        assertThat(pagina.getImagenes(), hasSize(1));
        assertThat(pagina.getTextos(), hasSize(2));
        assertThat(principal.getUrl(), is(equalTo("principal")));
        assertThat(publicitat.getUrl(), is(equalTo("publicitat")));

        Seccion ajudesEstudiant = principal.getSecciones().get(0);

        assertThat(principal.getSecciones(), hasSize(1));
        assertThat(ajudesEstudiant.getUrl(), is(equalTo("ajudesestudiantat")));

        Seccion clubDebat = ajudesEstudiant.getSecciones().get(0);

        assertThat(clubDebat.getUrl(), is(equalTo("clubdebat")));
        assertThat(clubDebat.getImagenes(), hasSize(1));
        assertThat(clubDebat.getTextos(), hasSize(1));
        assertThat(publicitat.getSecciones().get(0).getImagenes(), hasSize(2));
        assertThat(publicitat.getSecciones().get(0).getTextos(), hasSize(0));
    }

    @Test
    public void distribucionSeccionesHtmlPaginado()
            throws ParseException, MalformedURLException, UnsupportedEncodingException
    {
        formatoPublicacionHtmlPaginado = new FormatoPublicacionHtmlPaginado();

        formatoPublicacionHtmlPaginado.setMenuFactory(menuFactory);
        formatoPublicacionHtmlPaginado.setPublicacionService(publicacionService);
        formatoPublicacionHtmlPaginado.setMetadataExtractor(metadataExtractor);
        formatoPublicacionHtmlPaginado.setUrlTagsContentReplacer(urlTagsContentReplacer);
        formatoPublicacionHtmlPaginado.setAtributoExtractor(atributoExtractor);
        formatoPublicacionHtmlPaginado.setMigasExtractor(migasExtractor);

        List<Publicacion> contenidos = getContenidosPublicacionHtmlPaginado();

        String urlBase = "http://localhost:9005";
        String url = "/voxuji/1/";
        String idioma = "ca";

        Menu menu = new Menu();
        NodoMapa nodoMapa = new NodoMapa();
        nodoMapa.setMenu(menu);

        PublicacionDetails publicacionDetails = new PublicacionDetails();
        publicacionDetails.setUrl(url);
        publicacionDetails.setUrlBase(urlBase);
        publicacionDetails.setContenidos(contenidos);
        publicacionDetails.setNodoMapa(nodoMapa);
        publicacionDetails.setRequestDetails(new RequestDetails());
        publicacionDetails.setIdioma(idioma);

        Pagina pagina = formatoPublicacionHtmlPaginado.buildPagina(publicacionDetails);

        assertThat(pagina.getSecciones(), hasSize(3));
        assertThat(pagina.getSecciones().get(0).getTextos(), hasSize(1));
        assertThat(pagina.getSecciones().get(1).getTextos(), hasSize(1));
        assertThat(pagina.getSecciones().get(2).getTextos(), hasSize(1));
    }

    private void muestraSecciones(List<Seccion> secciones)
    {
        System.out.println("\n");
        System.out.println("Secciones disponibles:");

        for (Seccion seccion : secciones)
        {
            System.out.println(seccion.getUrl());
        }
    }

    private List<Publicacion> getContenidosPublicacionHtml()
    {
        List<Publicacion> contenidos = new ArrayList<Publicacion>();

        Map<String, String> originales = new LinkedHashMap();

        originales.put("/voxuji/1/portada.jpg", "S");
        originales.put("/voxuji/1/portada2.jpg", "N");
        originales.put("/voxuji/1/texto1.html", "N");
        originales.put("/voxuji/1/texto2.html", "S");
        originales.put("/voxuji/1/principal/ajudesestudiantat/clubdebat/foto.jpg", "S");
        originales.put("/voxuji/1/principal/ajudesestudiantat/clubdebat/texto.html", "S");
        originales.put("/voxuji/1/publicitat/zona1/1.jpg", "S");
        originales.put("/voxuji/1/publicitat/zona1/1p.jpg", "S");
        originales.put("/voxuji/1/publicitat/zona1/texto.html", "N");

        for (Map.Entry<String, String> original : originales.entrySet())
        {
            String url = original.getKey();

            Publicacion publicacion = new Publicacion();
            publicacion.setTitulo(url);
            publicacion.setContenidoId(new Random().nextLong());
            publicacion.setUrlCompleta(url);
            publicacion.setUrlCompletaOriginal(url);
            publicacion.setUrlNodo("/voxuji/1/");
            publicacion.setPublicable("S");

            if (url.endsWith(".jpg"))
            {
                publicacion.setEsHtml("N");
                publicacion.setVisible(original.getValue());
                publicacion.setMimeType("image/jpg");
            }
            else
            {
                publicacion.setEsHtml("S");
                publicacion.setVisible(original.getValue());
                publicacion.setMimeType("text/html");
            }

            publicacion.setIdioma("ca");
            contenidos.add(publicacion);
        }

        return contenidos;
    }

    private List<Publicacion> getContenidosPublicacionHtmlPaginado()
    {
        List<Publicacion> contenidos = new ArrayList<Publicacion>();

        List<String> originales = new ArrayList();

        originales.add("/voxuji/1/portada.html");
        originales.add("/voxuji/1/principal/ajudesestudiantat/clubdebat/foto.html");
        originales.add("/voxuji/1/publicitat/zona1/texto.html");

        for (String original : originales)
        {
            String url = original;

            Publicacion publicacion = new Publicacion();
            publicacion.setTitulo(url);
            publicacion.setContenidoId(new Random().nextLong());
            publicacion.setUrlCompleta(url);
            publicacion.setUrlCompletaOriginal(url);
            publicacion.setUrlNodo(url.substring(0, url.lastIndexOf("/") + 1));

            if (url.endsWith(".jpg"))
            {
                publicacion.setEsHtml("N");
                publicacion.setMimeType("image/jpg");
            }
            else
            {
                publicacion.setEsHtml("S");
                publicacion.setMimeType("text/html");
            }

            publicacion.setIdioma("ca");
            contenidos.add(publicacion);
        }

        return contenidos;
    }
}
