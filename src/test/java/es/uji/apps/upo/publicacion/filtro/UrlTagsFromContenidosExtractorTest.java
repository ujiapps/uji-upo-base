package es.uji.apps.upo.publicacion.filtro;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.uji.apps.upo.services.rest.publicacion.formato.filtro.UrlTagsExtractor;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class UrlTagsFromContenidosExtractorTest
{
    @Autowired
    UrlTagsExtractor urlTagsExtractor;

    @Test
    public void elExtractorDeTagsDebeDeExtraerTodasLasURLsDeUnContenido()
    {
        String contenido = "esto es un contenido cualquiera con un tag {{http://www.google.com}} y otro a {{www.meneame.es/pepito?pepe=pito&pito=pepe}}";

        List<String> urls = urlTagsExtractor.extract(contenido);

        Assert.assertEquals("http://www.google.com", urls.get(0));
        Assert.assertEquals("www.meneame.es/pepito?pepe=pito&pito=pepe", urls.get(1));
    }
}
