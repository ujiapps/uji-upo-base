package es.uji.apps.upo.publicacion.formato;

import com.sun.jersey.core.spi.factory.ResponseImpl;
import com.sun.syndication.io.FeedException;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.TipoPlantilla;
import es.uji.apps.upo.services.PublicacionService;
import es.uji.apps.upo.services.rest.publicacion.MenuFactory;
import es.uji.apps.upo.services.rest.publicacion.formato.FormatoPublicacionHtml;
import es.uji.apps.upo.services.rest.publicacion.formato.MigasExtractor;
import es.uji.apps.upo.services.rest.publicacion.model.RequestParams;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FormatoPublicacionHTMLTest extends FormatoPublicacionAbstratTest
{
    FormatoPublicacionHtml formatoPublicacionHtml;

    private RequestParams requestParams;
    private MenuFactory menuFactory;
    private MigasExtractor migasExtractor;

    @Before
    public void init()
    {
        requestParams = new RequestParams(URL_BASE, URL);

        formatoPublicacionHtml = new FormatoPublicacionHtml();

        publicacionService = mock(PublicacionService.class);
        menuFactory = mock(MenuFactory.class);
        migasExtractor = mock(MigasExtractor.class);

        formatoPublicacionHtml.setPublicacionService(publicacionService);
        formatoPublicacionHtml.setMenuFactory(menuFactory);
        formatoPublicacionHtml.setMigasExtractor(migasExtractor);
    }

    @Test
    public void siNoSeRecuperanContenidosLaRespuestaHaDeSerNotFound()
            throws MalformedURLException, FeedException, ParseException, UnsupportedEncodingException
    {
        when(publicacionService.getContenidosIdiomasByUrlNodoAndLanguage(requestParams.getUrl(),
                requestParams.getConfigPublicacion().getIdioma())).thenReturn(null);

        Response response = formatoPublicacionHtml.publica(requestParams);

        Assert.assertTrue(((ResponseImpl) response).getStatusType().equals(Response.Status.NOT_FOUND));
    }

    @Test
    public void siNoSeRecuperaElNodoMapaLaRespuestaHaDeSerNotFound()
            throws MalformedURLException, FeedException, ParseException, UnsupportedEncodingException
    {
        List<Publicacion> contenidos = new ArrayList<Publicacion>();
        contenidos.add(new Publicacion());

        when(publicacionService.getContenidosIdiomasByUrlNodoAndLanguage(requestParams.getUrl(),
                requestParams.getConfigPublicacion().getIdioma())).thenReturn(contenidos);
        when(publicacionService.getNodoMapaByUrlCompleta(requestParams.getUrl())).thenReturn(null);

        Response response = formatoPublicacionHtml.publica(requestParams);

        Assert.assertTrue(((ResponseImpl) response).getStatusType().equals(Response.Status.NOT_FOUND));
    }

    @Test
    public void siSeRecuperanContenidosYNodoLaRespuestaHaDeSerCorrecta()
            throws MalformedURLException, FeedException, ParseException, UnsupportedEncodingException
    {
        buildCommonDataStructure();

        Response response = formatoPublicacionHtml.publica(requestParams);

        Assert.assertTrue(((ResponseImpl) response).getStatusType().equals(Response.Status.OK));
        Assert.assertTrue(getMetadata(response, "Content-type").equals("text/html"));
        Assert.assertNotNull(((ResponseImpl) response).getEntityType());
    }

    @Test
    public void siSeRecuperaLaPlantillaPDFSeDebeDevolverUnPDF()
            throws MalformedURLException, FeedException, ParseException, UnsupportedEncodingException
    {
        buildCommonDataStructure();

        Plantilla plantilla = new Plantilla();
        plantilla.setTipo(TipoPlantilla.PDF);
        plantilla.setFichero("nombrePlantilla");
        NodoMapaPlantilla nodoMapaPlantilla = new NodoMapaPlantilla();
        nodoMapaPlantilla.setUpoPlantilla(plantilla);

        List<NodoMapaPlantilla> nodosMapaPlantillas = new ArrayList<>();
        nodosMapaPlantillas.add(nodoMapaPlantilla);

        when(publicacionService.getNodoMapaPlantillasByNodoMapaId(anyLong())).thenReturn(nodosMapaPlantillas);
        when(publicacionService.getNodoMapaPlantillaByNodoMapaIdAndType(1L, TipoPlantilla.PDF)).thenReturn(
                nodoMapaPlantilla);

        Response response = formatoPublicacionHtml.publica(requestParams);

        Assert.assertTrue(((ResponseImpl) response).getStatusType().equals(Response.Status.OK));
        Assert.assertTrue(getMetadata(response, "Content-type").equals("application/pdf"));
        Assert.assertTrue(((ResponseImpl) response).getEntityType().toString().contains("PDFTemplate"));
    }

    @Test
    public void siSeRecuperaUnaPlantillaHTMLSeDebeDevolverEsaPlantilla()
            throws MalformedURLException, FeedException, ParseException, UnsupportedEncodingException
    {
        buildCommonDataStructure();

        Plantilla plantilla = new Plantilla();
        plantilla.setTipo(TipoPlantilla.PDF);
        plantilla.setFichero("nombrePlantillaPDF");

        Plantilla plantilla2 = new Plantilla();
        plantilla2.setTipo(TipoPlantilla.WEB);
        plantilla2.setFichero("nombrePlantillaManual");

        NodoMapaPlantilla nodoMapaPlantilla = new NodoMapaPlantilla();
        nodoMapaPlantilla.setUpoPlantilla(plantilla);

        List<NodoMapaPlantilla> nodosMapaPlantillas = new ArrayList<>();
        nodosMapaPlantillas.add(nodoMapaPlantilla);

        when(publicacionService.getNodoMapaPlantillasByNodoMapaId(anyLong())).thenReturn(nodosMapaPlantillas);
        when(publicacionService.getNodoMapaPlantillaByNodoMapaIdAndType(1L, TipoPlantilla.PDF)).thenReturn(
                nodoMapaPlantilla);
        when(publicacionService.getPlantillaByFileName("nombrePlantillaManual")).thenReturn(plantilla2);

        requestParams.getConfigPublicacion().setPlantilla("nombrePlantillaManual");

        Response response = formatoPublicacionHtml.publica(requestParams);

        Assert.assertTrue(((ResponseImpl) response).getStatusType().equals(Response.Status.OK));
        Assert.assertTrue(getMetadata(response, "Content-type").equals("text/html"));
        Assert.assertTrue(((ResponseImpl) response).getEntityType().toString().contains("HTMLTemplate"));
    }

    @Test
    public void siSeEspecificaUnaPlantillaDebeAplicarEsaPlantilla()
            throws MalformedURLException, FeedException, ParseException, UnsupportedEncodingException
    {
        buildCommonDataStructure();

        Plantilla plantilla = new Plantilla();
        plantilla.setTipo(TipoPlantilla.PDF);
        plantilla.setFichero("nombrePlantillaPDF");

        Plantilla plantilla2 = new Plantilla();
        plantilla2.setTipo(TipoPlantilla.WEB);
        plantilla2.setFichero("nombrePlantillaHTML");

        NodoMapaPlantilla nodoMapaPlantilla = new NodoMapaPlantilla();
        nodoMapaPlantilla.setUpoPlantilla(plantilla);

        NodoMapaPlantilla nodoMapaPlantilla2 = new NodoMapaPlantilla();
        nodoMapaPlantilla2.setUpoPlantilla(plantilla2);

        List<NodoMapaPlantilla> nodosMapaPlantillas = new ArrayList<>();
        nodosMapaPlantillas.add(nodoMapaPlantilla);
        nodosMapaPlantillas.add(nodoMapaPlantilla2);

        when(publicacionService.getNodoMapaPlantillasByNodoMapaId(anyLong())).thenReturn(nodosMapaPlantillas);
        when(publicacionService.getNodoMapaPlantillaByNodoMapaIdAndType(1L, TipoPlantilla.PDF)).thenReturn(
                nodoMapaPlantilla);
        when(publicacionService.getNodoMapaPlantillaByNodoMapaIdAndType(1L, TipoPlantilla.WEB)).thenReturn(
                nodoMapaPlantilla2);

        Response response = formatoPublicacionHtml.publica(requestParams);

        Assert.assertTrue(((ResponseImpl) response).getStatusType().equals(Response.Status.OK));
        Assert.assertTrue(getMetadata(response, "Content-type").equals("text/html"));
        Assert.assertTrue(((ResponseImpl) response).getEntityType().toString().contains("HTMLTemplate"));
    }

    @Test
    public void siNoSeEspecificaPlantillaDebeDevolverUnaPlantillaPorDefecto()
            throws MalformedURLException, FeedException, ParseException, UnsupportedEncodingException
    {
        buildCommonDataStructure();

        Response response = formatoPublicacionHtml.publica(requestParams);

        Assert.assertTrue(((ResponseImpl) response).getStatusType().equals(Response.Status.OK));
        Assert.assertTrue(getMetadata(response, "Content-type").equals("text/html"));
        Assert.assertTrue(((ResponseImpl) response).getEntityType().toString().contains("HTMLTemplate"));
    }


    private void buildCommonDataStructure()
    {
        List<Publicacion> contenidos = new ArrayList<Publicacion>();

        Publicacion publicacion = new Publicacion();
        publicacion.setUrlCompleta("/prova.html");
        contenidos.add(publicacion);

        NodoMapa nodoMapa = new NodoMapa();
        nodoMapa.setId(1L);
        nodoMapa.setMenu(new Menu());

        when(publicacionService.getContenidosIdiomasByUrlNodoAndLanguage(requestParams.getUrl(),
                requestParams.getConfigPublicacion().getIdioma())).thenReturn(contenidos);

        requestParams.setNodoMapa(nodoMapa);
    }
}
