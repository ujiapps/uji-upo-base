package es.uji.apps.upo.publicacion.formato;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.model.Publicacion;
import es.uji.apps.upo.services.rest.publicacion.formato.ContenidoRSSAdapter;
import es.uji.apps.upo.services.rest.publicacion.formato.MetadataExtractor;
import es.uji.apps.upo.services.rest.publicacion.formato.filtro.UrlTagsContentReplacer;
import es.uji.apps.upo.services.rest.publicacion.formato.rss.ContenidoRSS;
import es.uji.apps.upo.services.rest.publicacion.formato.rss.FeedRSS;

public class ContenidoRSSAdapterTest
{
    private ContenidoRSSAdapter contenidoRSSAdapter = new ContenidoRSSAdapter("http://server/");

    private UrlTagsContentReplacer urlTagsContentReplacer;
    private MetadataExtractor metadataExtractor;

    private byte[] someBytes = "algo".getBytes();

    @Before
    public void init() throws UnsupportedEncodingException
    {
        urlTagsContentReplacer = mock(UrlTagsContentReplacer.class);
        metadataExtractor = mock(MetadataExtractor.class);

        when(urlTagsContentReplacer.replace(someBytes)).thenReturn(someBytes);

        contenidoRSSAdapter.setUrlTagsContentReplacer(urlTagsContentReplacer);
        contenidoRSSAdapter.setMetadataExtractor(metadataExtractor);
    }

    @Test
    public void distribucionSeccionesHtml() throws UnsupportedEncodingException
    {
        List<Publicacion> contenidos = getContenidosPublicacionHtml();

        FeedRSS contenidosRSS = contenidoRSSAdapter.adapta(contenidos);

        assertThat(contenidosRSS.size(), equalTo(4));

        ContenidoRSS contenidoRSS = contenidosRSS.get("/voxuji/1/");
        assertThat(contenidoRSS.getTitulo(), containsString("texto2.html"));
        assertThat(contenidoRSS.getEnclosures(), hasSize(1));

        contenidoRSS = contenidosRSS.get("/voxuji/1/principal/ajudesestudiantat/clubdebat/");
        assertThat(contenidoRSS.getTitulo(), containsString("texto.html"));
        assertThat(contenidoRSS.getEnclosures(), hasSize(1));

        contenidoRSS = contenidosRSS.get("/voxuji/1/publicitat/zona1/");
        assertThat(contenidoRSS.getTitulo(), containsString("texto.html"));
        assertThat(contenidoRSS.getEnclosures(), hasSize(2));

        contenidoRSS = contenidosRSS.get("/voxuji/1/publicitat/zona2/");
        assertThat(contenidoRSS.getTitulo(), containsString("2.jpg"));
        assertThat(contenidoRSS.getEnclosures(), hasSize(2));
        assertThat(contenidoRSS.getContenido(), is(nullValue()));
    }

    private List<Publicacion> getContenidosPublicacionHtml()
    {
        List<Publicacion> contenidos = new ArrayList<Publicacion>();

        Map<String, String> originales = new LinkedHashMap();

        originales.put("/voxuji/1/portada.jpg", "S");
        originales.put("/voxuji/1/texto1.html", "N");
        originales.put("/voxuji/1/texto2.html", "S");
        originales.put("/voxuji/1/portada2.jpg", "N");
        originales.put("/voxuji/1/principal/ajudesestudiantat/clubdebat/texto.html", "S");
        originales.put("/voxuji/1/principal/ajudesestudiantat/clubdebat/foto.jpg", "S");
        originales.put("/voxuji/1/publicitat/zona1/1p.jpg", "S");
        originales.put("/voxuji/1/publicitat/zona1/texto.html", "N");
        originales.put("/voxuji/1/publicitat/zona1/1.jpg", "S");
        originales.put("/voxuji/1/publicitat/zona2/1.jpg", "S");
        originales.put("/voxuji/1/publicitat/zona2/2.jpg", "S");

        for (Map.Entry<String, String> original : originales.entrySet())
        {
            String url = original.getKey();

            Publicacion publicacion = new Publicacion();
            publicacion.setTitulo(url);
            publicacion.setContenidoId(new Random().nextLong());
            publicacion.setUrlCompleta(url);
            publicacion.setUrlCompletaOriginal(url);
            publicacion.setUrlNodo(original.getKey().substring(0,
                    original.getKey().lastIndexOf("/") + 1));
            publicacion.setContenido(someBytes);

            if (url.endsWith(".jpg"))
            {
                publicacion.setEsHtml("N");
                publicacion.setVisible(original.getValue());
                publicacion.setMimeType("image/jpg");
            }
            else
            {
                publicacion.setEsHtml("S");
                publicacion.setVisible(original.getValue());
                publicacion.setMimeType("text/html");
            }

            publicacion.setIdioma("ca");
            contenidos.add(publicacion);
        }

        return contenidos;
    }
}
