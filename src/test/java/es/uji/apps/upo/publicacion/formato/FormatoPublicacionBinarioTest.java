package es.uji.apps.upo.publicacion.formato;

import com.sun.jersey.core.spi.factory.ResponseImpl;
import com.sun.xml.ws.api.message.ExceptionHasMessage;
import es.uji.apps.upo.model.PublicacionBinarios;
import es.uji.apps.upo.services.PublicacionService;
import es.uji.apps.upo.services.rest.publicacion.formato.FormatoPublicacionBinario;
import es.uji.apps.upo.services.rest.publicacion.model.RequestParams;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FormatoPublicacionBinarioTest extends FormatoPublicacionAbstratTest
{
    FormatoPublicacionBinario formatoPublicacionBinario;
    RequestParams requestParams;

    @Before
    public void init()
    {
        requestParams = new RequestParams(URL_BASE, URL);

        formatoPublicacionBinario = new FormatoPublicacionBinario();

        publicacionService = mock(PublicacionService.class);

        formatoPublicacionBinario.setPublicacionService(publicacionService);
    }

    @Test
    public void siNoSeRecuperanContenidosLaRespuestaHaDeSerNotFound()
            throws Exception
    {
        when(publicacionService.getBinarioByUrlCompletaAndLanguage(requestParams.getUrl(),
                requestParams.getConfigPublicacion().getIdioma())).thenReturn(null);

        Response response = formatoPublicacionBinario.publica(requestParams);

        assertTrue(((ResponseImpl) response).getStatusType().equals(Response.Status.NOT_FOUND));
    }

    @Test
    public void porDefectoElContentDispositionDebeDeSerAtachment()
            throws Exception
    {
        PublicacionBinarios contenido = new PublicacionBinarios();
        contenido.setMimeType("application/octet-stream");

        when(publicacionService.getBinarioByUrlCompletaAndLanguage(requestParams.getUrl(),
                requestParams.getConfigPublicacion().getIdioma())).thenReturn(contenido);

        Response response = formatoPublicacionBinario.publica(requestParams);

        assertTrue(getMetadata(response, "Content-disposition").startsWith("attachment"));

    }

    @Test
    public void siSeRecuperaUnaImagenElContentDispositionDebeDeSerInline()
            throws Exception
    {
        PublicacionBinarios contenido = new PublicacionBinarios();
        contenido.setMimeType("image/png");

        when(publicacionService.getBinarioByUrlCompletaAndLanguage(requestParams.getUrl(),
                requestParams.getConfigPublicacion().getIdioma())).thenReturn(contenido);

        Response response = formatoPublicacionBinario.publica(requestParams);

        assertTrue(getMetadata(response, "Content-disposition").startsWith("inline"));
    }

    @Test
    public void siSeRecuperaUnaPDFElContentDispositionDebeDeSerInline()
            throws Exception
    {
        PublicacionBinarios contenido = new PublicacionBinarios();
        contenido.setMimeType("application/pdf");

        when(publicacionService.getBinarioByUrlCompletaAndLanguage(requestParams.getUrl(),
                requestParams.getConfigPublicacion().getIdioma())).thenReturn(contenido);

        Response response = formatoPublicacionBinario.publica(requestParams);

        String contentDisposition = getMetadata(response, "Content-disposition");
        assertThat(contentDisposition, startsWith("inline"));
    }

    @Test
    public void debeDeIncluirseElMimeType()
            throws Exception
    {
        PublicacionBinarios contenido = new PublicacionBinarios();
        contenido.setMimeType("application/pdf");

        when(publicacionService.getBinarioByUrlCompletaAndLanguage(requestParams.getUrl(),
                requestParams.getConfigPublicacion().getIdioma())).thenReturn(contenido);

        Response response = formatoPublicacionBinario.publica(requestParams);

        assertTrue(getMetadata(response, "Content-type").equals("application/pdf"));
    }

    @Test
    public void debeDeIncluirseLaUrlCompletaComoNombreDelFichero()
            throws Exception
    {
        PublicacionBinarios contenido = new PublicacionBinarios();
        contenido.setUrlNodo("urlNodo/");
        contenido.setUrlPath("x.jpg");

        when(publicacionService.getBinarioByUrlCompletaAndLanguage(requestParams.getUrl(),
                requestParams.getConfigPublicacion().getIdioma())).thenReturn(contenido);

        Response response = formatoPublicacionBinario.publica(requestParams);

        assertTrue(getMetadata(response, "Content-Disposition").contains("filename=\"urlNodo/x.jpg\""));
    }

    @Test
    public void debeDeIncluirseElContenido()
            throws Exception
    {
        byte[] entity = "prueba de contenido".getBytes();
        PublicacionBinarios contenido = new PublicacionBinarios();
        contenido.setContenido(entity);

        when(publicacionService.getBinarioByUrlCompletaAndLanguage(requestParams.getUrl(),
                requestParams.getConfigPublicacion().getIdioma())).thenReturn(contenido);

        Response response = formatoPublicacionBinario.publica(requestParams);

        assertTrue(Arrays.equals((byte[]) response.getEntity(), entity));
        assertTrue(((ResponseImpl) response).getStatusType().equals(Response.Status.OK));
    }
}
