package es.uji.apps.upo.publicacion.formato.Atributo;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import es.uji.apps.upo.model.Publicacion;
import es.uji.apps.upo.services.rest.publicacion.formato.Atributo;
import es.uji.apps.upo.services.rest.publicacion.formato.AtributoExtractor;

public class AtributoExtractorTest
{
    AtributoExtractor atributoExtractor = new AtributoExtractor();
    Publicacion publicacion;

    @Test
    public void elNoHayAtributosDebeDevolverUnaListaVacia()
    {
        publicacion = new Publicacion();

        List<Atributo> atributos = atributoExtractor.extrae(publicacion);

        Assert.assertTrue(atributos.isEmpty());
    }

    @Test
    public void elExtractorDeAtributosDebeTenerEnCuentaElFormato()
    {
        publicacion = new Publicacion();
        publicacion.setAtributos("clave1@@@valor1~~~clave2@@@valor2");

        List<Atributo> atributos = atributoExtractor.extrae(publicacion);

        Assert.assertTrue(atributos.size() == 2);
        Assert.assertTrue(atributos.get(0).getClave().equals("clave1"));
        Assert.assertTrue(atributos.get(1).getValor().equals("valor2"));
    }
}
