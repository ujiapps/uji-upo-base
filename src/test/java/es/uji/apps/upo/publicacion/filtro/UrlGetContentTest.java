package es.uji.apps.upo.publicacion.filtro;

import es.uji.apps.upo.services.rest.publicacion.formato.filtro.UrlGetContent;
import es.uji.apps.upo.services.rest.publicacion.model.RequestDetails;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class UrlGetContentTest
{
    @Autowired
    UrlGetContent urlGetContent;

    @Test
    public void elExtractorDeTagsDebeDeExtraerTodasLasURLsDeUnContenido()
            throws IOException
    {
        String url = "http://static.uji.es/js/extjs/extjs-4.2.1/license.txt";

        urlGetContent.setRequestParams(url, new RequestDetails());

        String result = urlGetContent.get();

        Assert.assertTrue(result.startsWith("Ext JS 4.2"));
    }
}
