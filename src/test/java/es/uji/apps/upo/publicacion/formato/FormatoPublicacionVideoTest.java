package es.uji.apps.upo.publicacion.formato;

import com.sun.jersey.core.spi.factory.ResponseImpl;
import es.uji.apps.upo.model.PublicacionBinarios;
import es.uji.apps.upo.services.PublicacionService;
import es.uji.apps.upo.services.rest.publicacion.formato.FormatoPublicacionVideo;
import es.uji.apps.upo.services.rest.publicacion.model.RequestParams;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.net.MalformedURLException;
import java.text.ParseException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FormatoPublicacionVideoTest extends FormatoPublicacionAbstratTest
{
    FormatoPublicacionVideo formatoPublicacionVideo;

    RequestParams requestParams;

    @Before
    public void init()
    {
        requestParams = new RequestParams(URL_BASE, URL);

        formatoPublicacionVideo = new FormatoPublicacionVideo();

        publicacionService = mock(PublicacionService.class);

        formatoPublicacionVideo.setPublicacionService(publicacionService);
    }

    @Test
    public void siNoSeRecuperanContenidosLaRespuestaHaDeSerNotFound()
            throws Exception
    {
        when(publicacionService.getBinarioByUrlCompletaAndLanguage(requestParams.getUrl(),
                requestParams.getConfigPublicacion().getIdioma())).thenReturn(null);

        Response response = formatoPublicacionVideo.publica(requestParams);

        Assert.assertTrue(((ResponseImpl) response).getStatusType().equals(Response.Status.NOT_FOUND));
    }

    @Test
    public void siElRangeNoEsNuloDebeDebeDevolverseUnaRespuestaAdaptada()
            throws Exception
    {
        byte[] entity = "prueba de contenido".getBytes();
        PublicacionBinarios contenido = new PublicacionBinarios();
        contenido.setMimeType("video/mpeg4");
        contenido.setContenido(entity);

        when(publicacionService.getBinarioByUrlCompletaAndLanguage(requestParams.getUrl(),
                requestParams.getConfigPublicacion().getIdioma())).thenReturn(contenido);

        requestParams.getSearchParams().setRange("r=0-4");

        Response response = formatoPublicacionVideo.publica(requestParams);

        Assert.assertTrue(getMetadata(response, "Content-type").equals("video/mpeg4"));
        Assert.assertTrue(getMetadata(response, "Accept-Ranges").equals("bytes"));
        Assert.assertTrue(getMetadata(response, "Content-Length").equals(((Integer) 5).toString()));
        Assert.assertNotNull(getMetadata(response, "Content-Range"));
        Assert.assertTrue(((ResponseImpl) response).getStatus() == 206);
    }

    @Test
    public void siElRangeEsNuloDebeDebeDevolverseUnaRespuestaStandard()
            throws Exception
    {
        byte[] entity = "prueba de contenido".getBytes();
        PublicacionBinarios contenido = new PublicacionBinarios();
        contenido.setMimeType("video/mpeg4");
        contenido.setContenido(entity);

        when(publicacionService.getBinarioByUrlCompletaAndLanguage(requestParams.getUrl(),
                requestParams.getConfigPublicacion().getIdioma())).thenReturn(contenido);

        Response response = formatoPublicacionVideo.publica(requestParams);

        Assert.assertTrue(getMetadata(response, "Content-type").equals("video/mpeg4"));
        Assert.assertTrue(getMetadata(response, "Content-Length").equals(((Integer) entity.length).toString()));
        Assert.assertTrue(((ResponseImpl) response).getStatusType().equals(Response.Status.OK));
    }
}
