package es.uji.apps.upo.publicacion.formato;

import com.sun.jersey.core.spi.factory.ResponseImpl;
import com.sun.syndication.io.FeedException;
import es.uji.apps.upo.model.Publicacion;
import es.uji.apps.upo.services.PublicacionService;
import es.uji.apps.upo.services.rest.publicacion.formato.ContenidoRSSAdapter;
import es.uji.apps.upo.services.rest.publicacion.formato.FormatoPublicacionRss;
import es.uji.apps.upo.services.rest.publicacion.formato.rss.FeedRSS;
import es.uji.apps.upo.services.rest.publicacion.model.RequestParams;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FormatoPublicacionRSSTest extends FormatoPublicacionAbstratTest
{
    FormatoPublicacionRss formatoPublicacionRss;

    ContenidoRSSAdapter contenidoRSSAdapter;

    RequestParams requestParams;

    @Before
    public void init()
            throws UnsupportedEncodingException
    {
        requestParams = new RequestParams(URL_BASE, URL);

        formatoPublicacionRss = new FormatoPublicacionRss();

        publicacionService = mock(PublicacionService.class);
        contenidoRSSAdapter = mock(ContenidoRSSAdapter.class);

        when(contenidoRSSAdapter.adapta(anyList())).thenReturn(new FeedRSS());

        formatoPublicacionRss.setPublicacionService(publicacionService);
        formatoPublicacionRss.setContenidoRSSAdapter(contenidoRSSAdapter);
    }

    @Test
    public void siNoSeRecuperanContenidosLaRespuestaHaDeSerNotFound()
            throws MalformedURLException, FeedException, ParseException, UnsupportedEncodingException
    {
        when(publicacionService.getContenidosIdiomasByUrlNodoAndLanguage(requestParams.getUrl(),
                requestParams.getConfigPublicacion().getIdioma())).thenReturn(null);

        Response response = formatoPublicacionRss.publica(requestParams);

        Assert.assertTrue(((ResponseImpl) response).getStatusType().equals(Response.Status.NOT_FOUND));
    }

    @Test
    public void siSeRecuperanContenidosLaRespuestaHaDeSerCorrecta()
            throws MalformedURLException, FeedException, ParseException, UnsupportedEncodingException
    {
        List<Publicacion> contenidos = new ArrayList<Publicacion>();
        contenidos.add(new Publicacion());

        when(publicacionService.getContenidosIdiomasByUrlNodoAndLanguage(requestParams.getUrl(),
                requestParams.getConfigPublicacion().getIdioma())).thenReturn(contenidos);

        Response response = formatoPublicacionRss.publica(requestParams);

        Assert.assertTrue(((ResponseImpl) response).getStatusType().equals(Response.Status.OK));
        Assert.assertTrue(getMetadata(response, "Content-type").equals("text/xml"));
        Assert.assertTrue(((ResponseImpl) response).getEntityType().toString().contains("xerces"));
    }
}
