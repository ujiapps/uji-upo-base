package es.uji.apps.upo.publicacion.formato.miga;

import es.uji.apps.upo.model.Publicacion;
import es.uji.apps.upo.services.rest.publicacion.formato.MigasExtractor;
import es.uji.commons.web.template.model.Miga;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class MigaExtractorTest
{
    MigasExtractor migasExtractor = new MigasExtractor();

    @Test
    public void elNoHayMigasDebeDevolverUnaListaVacia()
    {
        List<Miga> migas = migasExtractor.extrae(null);

        Assert.assertTrue(migas.isEmpty());
    }

    @Test
    public void elExtractorDeMigasDebeTenerEnCuentaElFormato()
    {
        List<Miga> migas = migasExtractor.extrae("titulo1@@@url1~~~titulo2@@@url2");

        Assert.assertTrue(migas.size() == 2);
        Assert.assertTrue(migas.get(0).getTitulo().equals("titulo1"));
        Assert.assertTrue(migas.get(0).getUrl().equals("url1"));
        Assert.assertTrue(migas.get(1).getTitulo().equals("titulo2"));
        Assert.assertTrue(migas.get(1).getUrl().equals("url2"));
    }
}
