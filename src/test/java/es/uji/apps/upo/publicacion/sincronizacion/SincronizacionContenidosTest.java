package es.uji.apps.upo.publicacion.sincronizacion;

import es.uji.apps.upo.builders.*;
import es.uji.apps.upo.dao.*;
import es.uji.apps.upo.exceptions.BorradoContenidoNoAutorizadoException;
import es.uji.apps.upo.exceptions.NodoMapaContenidoDebeTenerUnContenidoNormalException;
import es.uji.apps.upo.exceptions.UsuarioNoAutenticadoException;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.EstadoModeracion;
import es.uji.apps.upo.model.enums.IdiomaPublicacion;
import es.uji.apps.upo.model.enums.TipoPrioridadContenido;
import es.uji.apps.upo.model.enums.TipoReferenciaContenido;
import es.uji.apps.upo.services.nodomapa.NodoMapaService;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Types;
import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class SincronizacionContenidosTest
{
    private es.uji.apps.upo.model.Menu menu;
    private Franquicia franquicia;
    private Persona persona;
    private Idioma idiomaCA, idiomaES;
    private NodoMapa nodoOrigen;
    private NodoMapa nodoDestino;
    private NodoMapa nodo1, nodo2, nodo3, nodo1_1, nodo1_2;
    private Contenido contenido1, contenido2, contenido3, contenido1_1, contenido1_2;
    private SincronizadorContenidos sincronizadorContenidos;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private MenuDAO menuDAO;

    @Autowired
    private FranquiciaDAO franquiciaDAO;

    @Autowired
    private NodoMapaDAO nodoMapaDAO;

    @Autowired
    private ContenidoDAO contenidoDAO;

    @Autowired
    private PersonaDAO personaDAO;

    @Autowired
    private NodoMapaContenidoDAO nodoMapaContenidoDAO;

    @Autowired
    private NodoMapaService nodoMapaService;

    @Autowired
    private ContenidoIdiomaDAO contenidoIdiomaDAO;

    @Autowired
    private IdiomaDAO idiomaDAO;

    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    String raiz = "/" + UUID.randomUUID().toString() + "/";

    @org.junit.Before
    public void init()
    {
        menu = new MenuBuilder(menuDAO).withNombre("menu").build();

        franquicia = new FranquiciaBuilder(franquiciaDAO).withNombre("Franquicia").build();

        persona = new PersonaBuilder(personaDAO).getAny();

        crearEstructuraDeNodos();

        nodoDestino.setNodoMapaDAO(nodoMapaDAO);

        idiomaCA = new IdiomaBuilder(idiomaDAO).withNombre("Català")
                .withCodigoISO(IdiomaPublicacion.CA.toString())
                .build();
        idiomaES = new IdiomaBuilder(idiomaDAO).withNombre("Espanol")
                .withCodigoISO(IdiomaPublicacion.ES.toString())
                .build();

        sincronizadorContenidos = new SincronizadorContenidos(dataSource);
    }

    private void crearEstructuraDeNodos()
    {
        nodoOrigen = buildNodoMapa("nodoOrigen", raiz + "nodoOrigen/");
        nodoDestino = buildNodoMapa("nodoDestino", raiz + "nodoDestino/");

        nodo1 = buildNodoMapa("nodo1", raiz + "nodoOrigen/nodo1/");
        nodo1_1 = buildNodoMapa("nodo1_1", raiz + "nodoOrigen/nodo1/nodo1_1/");
        nodo1_2 = buildNodoMapa("nodo1_2", raiz + "nodoOrigen/nodo1/nodo1_2/");

        nodo2 = buildNodoMapa("nodo2", raiz + "nodoOrigen/nodo2/");
        nodo3 = buildNodoMapa("nodo3", raiz + "nodoOrigen/nodo3/");
    }

    @Transactional
    private NodoMapa buildNodoMapa(String urlPath, String urlCompleta)
    {
        return new NodoMapaBuilder(nodoMapaDAO).withParent()
                .withUrlPath(urlPath)
                .withUrlCompleta(urlCompleta)
                .withFranquicia(franquicia)
                .withMenu(menu)
                .withOrden(1L)
                .build();
    }

    @org.junit.Test
    public void nodoDestinoDebeEstarVacioSiOrigenVacio()
    {
        sincronizadorContenidos.sincroniza(5L);

        Assert.assertFalse(nodoMapaService.hasNodosMapaHijo(nodoDestino));
    }

    @org.junit.Test
    public void nodoDestinoDebeTenerUnHijoSiOrigenTieneUnSoloContenido()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        contenido1 = crearContenidoParaNodo(nodo1);

        sincronizadorContenidos.sincroniza(5L);

        Assert.assertEquals(1, nodoMapaService.getHijos(nodoDestino.getId()).size());
    }

    @org.junit.Test
    public void puedeHaberUnNodoConMasDeUnContenido()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        contenido1 = crearContenidoParaNodo(nodo1);
        contenido2 = crearContenidoParaNodo(nodo1);

        sincronizadorContenidos.sincroniza(5L);

        Assert.assertEquals(1, nodoMapaService.getHijos(nodoDestino.getId()).size());
    }

    @org.junit.Test
    public void nodoDestinoDebeTenerElMaximoDeItemsSiHaySuficientesNodos()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        contenido1 = crearContenidoParaNodo(nodo1);
        contenido2 = crearContenidoParaNodo(nodo2);
        contenido3 = crearContenidoParaNodo(nodo3);

        sincronizadorContenidos.sincroniza(2L);

        Assert.assertEquals(2, nodoMapaService.getHijos(nodoDestino.getId()).size());
    }

    @org.junit.Test
    public void nodoOrigenPuedeTenerVariosNiveles()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        contenido1 = crearContenidoParaNodo(nodo1);
        contenido2 = crearContenidoParaNodo(nodo2);
        contenido3 = crearContenidoParaNodo(nodo3);
        contenido1_1 = crearContenidoParaNodo(nodo1_1);
        contenido1_2 = crearContenidoParaNodo(nodo1_2);

        sincronizadorContenidos.sincroniza(5L);

        Assert.assertEquals(5, nodoMapaService.getHijos(nodoDestino.getId()).size());
    }

    @org.junit.Test
    public void seDebenRecuperarSoloLosNodosConCiertoNivelDeProfundidadSiSeEspecifica()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        contenido1 = crearContenidoParaNodo(nodo1);
        contenido2 = crearContenidoParaNodo(nodo2);
        contenido3 = crearContenidoParaNodo(nodo3);
        contenido1_1 = crearContenidoParaNodo(nodo1_1);
        contenido1_2 = crearContenidoParaNodo(nodo1_2);

        sincronizadorContenidos.sincroniza(5L, 1L);

        Assert.assertEquals(3, nodoMapaService.getHijos(nodoDestino.getId()).size());
    }

    @org.junit.Test
    public void seSincronizanLosEnlacesTipoLink()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        contenido1 = crearContenidoParaNodo(nodo1);

        new NodoMapaContenidoBuilder(nodoMapaContenidoDAO).withTipo(TipoReferenciaContenido.LINK)
                .withEstadoModeracion(EstadoModeracion.ACEPTADO)
                .withContenido(contenido1)
                .withMapa(nodo2)
                .withOrden(1L)
                .build();

        sincronizadorContenidos.sincroniza(5L);

        Assert.assertEquals(2, nodoMapaService.getHijos(nodoDestino.getId()).size());
    }

    @org.junit.Test
    public void losNodosNoVigentesNoDebenSincronizarse()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        contenido1 = crearContenidoParaNodo(nodo1);
        contenido2 = crearContenidoParaNodo(nodo2);
        contenido3 = crearContenidoParaNodo(nodo3);

        Date fecha = fechaActualMasDias(1);
        addVigenciaAContenido(fecha, contenido1);

        fecha = fechaActualMasDias(3);
        addVigenciaAContenido(fecha, contenido3);

        fecha = fechaActualMasDias(-1);
        addVigenciaAContenido(fecha, contenido2);

        fecha = fechaActualMasDias(4);
        addVigenciaAContenido(fecha, contenido2);

        sincronizadorContenidos.sincroniza(5L);

        Assert.assertEquals(2, nodoMapaService.getHijos(nodoDestino.getId()).size());
    }

    @org.junit.Test
    public void losNodosNoVigentesNoDebenSincronizarseSiAjustamosAFechasDeVigencia()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        contenido1 = crearContenidoParaNodo(nodo1);
        contenido2 = crearContenidoParaNodo(nodo2);
        contenido3 = crearContenidoParaNodo(nodo3);

        contenido1.setAjustarAFechasVigencia(true);
        contenido2.setAjustarAFechasVigencia(true);
        contenido3.setAjustarAFechasVigencia(true);

        Date fecha = fechaActualMasDias(1);
        addVigenciaAContenido(fecha, contenido1);

        fecha = fechaActualMasDias(3);
        addVigenciaAContenido(fecha, contenido3);

        fecha = fechaActualMasDias(-1);
        addVigenciaAContenido(fecha, contenido2);

        fecha = fechaActualMasDias(4);
        addVigenciaAContenido(fecha, contenido2);

        sincronizadorContenidos.sincroniza(5L);

        Assert.assertEquals(0, nodoMapaService.getHijos(nodoDestino.getId()).size());
    }

    @org.junit.Test
    public void losNodosSinIdiomaEspañolYCatalanNoDebenSincronizarse()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        contenido1 = new ContenidoBuilder(contenidoDAO).withPersonaResponsable(persona).withUrlPath("url").build();

        new NodoMapaContenidoBuilder(nodoMapaContenidoDAO).withTipo(TipoReferenciaContenido.NORMAL)
                .withEstadoModeracion(EstadoModeracion.ACEPTADO)
                .withContenido(contenido1)
                .withMapa(nodo1)
                .withOrden(1L)
                .build();

        sincronizadorContenidos.sincroniza(5L);

        Assert.assertEquals(0, nodoMapaService.getHijos(nodoDestino.getId()).size());
    }

    @org.junit.Test
    public void seTieneQueConsiderarElOrdenDeLosContenidosParaDescartar()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        contenido1 = crearContenidoConOrdenParaNodo(10L, nodo1);
        contenido2 = crearContenidoConOrdenParaNodo(2L, nodo2);
        contenido3 = crearContenidoConOrdenParaNodo(1L, nodo3);
        contenido1_1 = crearContenidoConOrdenParaNodo(2L, nodo1_1);

        sincronizadorContenidos.sincroniza(3L);

        Assert.assertFalse(nodoEstaSincronizado(nodo1, nodoMapaService.getHijos(nodoDestino.getId())));
    }

    @org.junit.Test
    public void seTieneQueConsiderarElOrdenDeLasVigenciasParaDescartar()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        contenido1 = crearContenidoParaNodo(nodo1);
        contenido2 = crearContenidoParaNodo(nodo2);
        contenido3 = crearContenidoParaNodo(nodo3);

        Date fecha = fechaActualMasDias(1);
        addVigenciaAContenido(fecha, contenido1);

        fecha = fechaActualMasDias(2);
        addVigenciaAContenido(fecha, contenido2);

        fecha = fechaActualMasDias(3);
        addVigenciaAContenido(fecha, contenido3);

        sincronizadorContenidos.sincroniza(2L);

        Assert.assertEquals(2, nodoMapaService.getHijos(nodoDestino.getId()).size());
        Assert.assertFalse(nodoEstaSincronizado(nodo3, nodoMapaService.getHijos(nodoDestino.getId())));
    }

    @org.junit.Test
    public void seTieneQueConsiderarElOrdenDeLasPrioridadesParaDescartar()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        contenido1 = crearContenidoParaNodo(nodo1);
        contenido2 = crearContenidoParaNodo(nodo2);
        contenido3 = crearContenidoParaNodo(nodo3);

        addPrioridadAContenido(TipoPrioridadContenido.URGENTE, contenido3);
        addPrioridadAContenido(TipoPrioridadContenido.ALTA, contenido2);
        addPrioridadAContenido(TipoPrioridadContenido.NORMAL, contenido1);

        sincronizadorContenidos.sincroniza(2L);

        Assert.assertEquals(2, nodoMapaService.getHijos(nodoDestino.getId()).size());
        Assert.assertFalse(nodoEstaSincronizado(nodo1, nodoMapaService.getHijos(nodoDestino.getId())));
    }

    @org.junit.Test
    public void conLaMismaPrioridadYLaMismaVigenciaCuentaElOrden()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        contenido1 = crearContenidoConOrdenParaNodo(2L, nodo1);
        contenido2 = crearContenidoConOrdenParaNodo(1L, nodo2);

        Date fecha = fechaActualMasDias(1);
        addVigenciaAContenido(fecha, contenido1);
        addVigenciaAContenido(fecha, contenido2);

        addPrioridadAContenido(TipoPrioridadContenido.ALTA, contenido2);
        addPrioridadAContenido(TipoPrioridadContenido.ALTA, contenido1);

        sincronizadorContenidos.sincroniza(1L);

        Assert.assertEquals(1, nodoMapaService.getHijos(nodoDestino.getId()).size());
        Assert.assertFalse(nodoEstaSincronizado(nodo1, nodoMapaService.getHijos(nodoDestino.getId())));
    }

    @org.junit.Test
    public void conLaMismaPrioridadYCuentaLaVigenciaSinConsiderarElOrden()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        contenido1 = crearContenidoConOrdenParaNodo(1L, nodo1);
        contenido2 = crearContenidoConOrdenParaNodo(2L, nodo2);
        contenido3 = crearContenidoConOrdenParaNodo(3L, nodo3);

        Date fecha = fechaActualMasDias(1);
        addVigenciaAContenido(fecha, contenido1);

        fecha = fechaActualMasDias(1);
        addVigenciaAContenido(fecha, contenido2);

        fecha = fechaActualMasDias(3);
        addVigenciaAContenido(fecha, contenido3);

        addPrioridadAContenido(TipoPrioridadContenido.BAJA, contenido1);
        addPrioridadAContenido(TipoPrioridadContenido.BAJA, contenido2);
        addPrioridadAContenido(TipoPrioridadContenido.BAJA, contenido3);

        sincronizadorContenidos.sincroniza(2L);

        Assert.assertEquals(2, nodoMapaService.getHijos(nodoDestino.getId()).size());
        Assert.assertFalse(nodoEstaSincronizado(nodo3, nodoMapaService.getHijos(nodoDestino.getId())));
    }

    @org.junit.Test
    public void laPrioridadEsElFactorMasImportante()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        contenido1 = crearContenidoConOrdenParaNodo(1L, nodo1);
        contenido2 = crearContenidoConOrdenParaNodo(2L, nodo2);
        contenido3 = crearContenidoConOrdenParaNodo(3L, nodo3);
        contenido1_1 = crearContenidoConOrdenParaNodo(10L, nodo3);

        Date fecha = fechaActualMasDias(1);
        addVigenciaAContenido(fecha, contenido1);

        fecha = fechaActualMasDias(1);
        addVigenciaAContenido(fecha, contenido2);

        fecha = fechaActualMasDias(3);
        addVigenciaAContenido(fecha, contenido3);

        fecha = fechaActualMasDias(1);
        addVigenciaAContenido(fecha, contenido1_1);

        addPrioridadAContenido(TipoPrioridadContenido.BAJA, contenido1);
        addPrioridadAContenido(TipoPrioridadContenido.ALTA, contenido2);
        addPrioridadAContenido(TipoPrioridadContenido.URGENTE, contenido3);
        addPrioridadAContenido(TipoPrioridadContenido.BAJA, contenido1_1);

        sincronizadorContenidos.sincroniza(2L);

        Assert.assertEquals(2, nodoMapaService.getHijos(nodoDestino.getId()).size());
        Assert.assertFalse(nodoEstaSincronizado(nodo1, nodoMapaService.getHijos(nodoDestino.getId())));
    }

    @org.junit.Test
    public void laFechaDeCreacionEsElFactorMenosImportante()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        contenido1 = crearContenidoParaNodo(nodo1);
        contenido2 = crearContenidoParaNodo(nodo2);

        contenido1.setFechaCreacion(fechaActualMasDias(1));

        sincronizadorContenidos.sincroniza(1L);

        Assert.assertEquals(1, nodoMapaService.getHijos(nodoDestino.getId()).size());
        Assert.assertFalse(nodoEstaSincronizado(nodo1, nodoMapaService.getHijos(nodoDestino.getId())));
    }

    @Transactional
    private void addPrioridadAContenido(TipoPrioridadContenido tipoPrioridad, Contenido contenido)
    {
        PrioridadContenido prioridad = new PrioridadContenido();
        prioridad.setPrioridad(tipoPrioridad);
        prioridad.setUpoObjeto(contenido);

        contenido.setUpoObjetosPrioridades(Collections.singleton(prioridad));

        contenidoDAO.update(contenido);
    }

    private void addVigenciaAContenido(Date fecha, Contenido contenido)
    {
        addVigenciaAContenido(fecha, null, null, contenido);
    }

    @Transactional
    private void addVigenciaAContenido(Date fecha, Date horaInicio, Date horaFin, Contenido contenido)
    {
        VigenciaContenido vigencia = new VigenciaContenido();
        vigencia.setContenido(contenido);
        vigencia.setFecha(fecha);

        if (horaInicio != null)
        {
            vigencia.setHoraInicio(horaInicio);
        }

        if (horaFin != null)
        {
            vigencia.setHoraFin(horaFin);
        }

        contenido.setUpoVigenciasObjetos(Collections.singleton(vigencia));

        contenidoDAO.update(contenido);
    }

    private Date fechaActualMasDias(Integer numDias)
    {
        Calendar cal = Calendar.getInstance();
        Date fecha = new Date();
        cal.setTime(fecha);
        cal.add(Calendar.DATE, numDias);

        return cal.getTime();
    }

    private Boolean nodoEstaSincronizado(NodoMapa nodoMapa, List<NodoMapa> listaNodosSincronizados)
    {
        for (NodoMapa nodoMapaSincronizado : listaNodosSincronizados)
        {
            if (nodoMapaSincronizado.getUrlPath().endsWith(nodoMapa.getUrlPath()))
            {
                return true;
            }
        }

        return false;
    }

    private Contenido crearContenidoParaNodo(NodoMapa nodoMapa)
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        return crearContenidoConOrdenParaNodo(1L, nodoMapa);
    }

    private Contenido crearContenidoConOrdenParaNodo(Long orden, NodoMapa nodoMapa)
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        Contenido contenido =
                new ContenidoBuilder(contenidoDAO).withPersonaResponsable(persona).withUrlPath("url").build();

        new NodoMapaContenidoBuilder(nodoMapaContenidoDAO).withTipo(TipoReferenciaContenido.NORMAL)
                .withEstadoModeracion(EstadoModeracion.ACEPTADO)
                .withContenido(contenido)
                .withMapa(nodoMapa)
                .withOrden(orden)
                .build();

        new ContenidoIdiomaBuilder(contenidoIdiomaDAO).withHtml("S")
                .withTitulo("titulo")
                .withIdioma(idiomaES)
                .withContenido(contenido)
                .build();

        new ContenidoIdiomaBuilder(contenidoIdiomaDAO).withHtml("S")
                .withTitulo("titulo")
                .withIdioma(idiomaCA)
                .withContenido(contenido)
                .build();

        return contenido;
    }

    @org.junit.After
    @Transactional
    public void end()
            throws BorradoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        nodoMapaDAO.delete(NodoMapa.class, nodo1_1.getId());
        nodoMapaDAO.delete(NodoMapa.class, nodo1_2.getId());
        nodoMapaDAO.delete(NodoMapa.class, nodo1.getId());
        nodoMapaDAO.delete(NodoMapa.class, nodo2.getId());
        nodoMapaDAO.delete(NodoMapa.class, nodo3.getId());

        nodoMapaDAO.delete(NodoMapa.class, nodoOrigen.getId());
        nodoMapaDAO.delete(NodoMapa.class, nodoDestino.getId());

        deleteContenido(contenido1);
        deleteContenido(contenido2);
        deleteContenido(contenido3);
        deleteContenido(contenido1_1);
        deleteContenido(contenido1_2);

        idiomaDAO.delete(Idioma.class, idiomaCA.getId());
        idiomaDAO.delete(Idioma.class, idiomaES.getId());
    }

    private void deleteContenido(Contenido contenido)
    {
        try
        {
            contenidoDAO.delete(Contenido.class, contenido.getId());
        } catch (Exception e)
        {
        }
    }

    private class SincronizadorContenidos extends StoredProcedure
    {
        private String urlNodoOrigen = raiz + "nodoOrigen/";
        private String urlNodoDestino = raiz + "nodoDestino/";
        private String SQL = "sync_contenidos.sync_directory_from_settings";
        private String PORIGEN = "p_origen";
        private String PDESTINO = "p_destino";
        private String PMAXCONTENIDOS = "p_max_contenidos";
        private String PNUMNIVELES = "p_num_niveles";

        public SincronizadorContenidos(DataSource dataSource1)
        {
            setDataSource(dataSource);
            setSql(SQL);
            declareParameter(new SqlParameter(PORIGEN, Types.VARCHAR));
            declareParameter(new SqlParameter(PDESTINO, Types.VARCHAR));
            declareParameter(new SqlParameter(PMAXCONTENIDOS, Types.BIGINT));
            declareParameter(new SqlParameter(PNUMNIVELES, Types.BIGINT));
            compile();
        }

        public void sincroniza(Long numItems)
        {
            sincroniza(numItems, 10L);
        }

        @Transactional
        public void sincroniza(Long numItems, Long numNiveles)
        {
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(PORIGEN, urlNodoOrigen);
            inParams.put(PDESTINO, urlNodoDestino);
            inParams.put(PMAXCONTENIDOS, numItems);
            inParams.put(PNUMNIVELES, numNiveles);
            execute(inParams);
        }
    }
}

