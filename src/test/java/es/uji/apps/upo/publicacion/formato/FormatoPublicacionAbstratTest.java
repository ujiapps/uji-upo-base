package es.uji.apps.upo.publicacion.formato;

import com.sun.jersey.core.spi.factory.ResponseImpl;
import es.uji.apps.upo.services.PublicacionService;
import es.uji.apps.upo.services.rest.publicacion.MenuFactory;

import javax.ws.rs.core.Response;

public class FormatoPublicacionAbstratTest
{
    public static final String URL_BASE = "http://localhost";
    public static final String URL = "/";

    PublicacionService publicacionService;
    MenuFactory menuFactory;

    protected String getMetadata(Response response, String key)
    {
        return ((ResponseImpl) response).getMetadata().get(key).get(0).toString();
    }
}
