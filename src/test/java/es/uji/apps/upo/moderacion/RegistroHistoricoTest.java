package es.uji.apps.upo.moderacion;

import es.uji.apps.upo.builders.*;
import es.uji.apps.upo.dao.*;
import es.uji.apps.upo.exceptions.ContenidoConMismoUrlPathException;
import es.uji.apps.upo.exceptions.ContenidoYaPropuestoException;
import es.uji.apps.upo.exceptions.InsertadoContenidoNoAutorizadoException;
import es.uji.apps.upo.exceptions.NodoMapaContenidoDebeTenerUnContenidoNormalException;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.EstadoModeracion;
import es.uji.apps.upo.model.enums.TipoReferenciaContenido;
import es.uji.apps.upo.services.*;
import es.uji.apps.upo.services.nodomapa.NodoMapaService;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Rollback
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class RegistroHistoricoTest
{
    @Autowired
    private NodoMapaDAO nodoMapaDAO;

    @Autowired
    private ContenidoDAO contenidoDAO;

    @Autowired
    private NodoMapaContenidoDAO nodoMapaContenidoDAO;

    @Autowired
    private ContenidoService contenidoService;

    @Autowired
    private FranquiciaDAO franquiciaDAO;

    @Autowired
    private PersonaDAO personaDAO;

    @Autowired
    private MenuDAO menuDAO;

    @Autowired
    private ModeracionDAO moderacionDAO;

    @Autowired
    private ModeracionHistoricoService moderacionHistoricoService;

    private NodoMapaContenidoService nodoMapaContenidoService;

    private ModeracionService moderacionService;

    private Persona personaResponsable;
    private Franquicia franquicia;
    private Contenido contenido;
    private NodoMapa nodoMapa;
    private NodoMapa nodoMapaHijo;
    private NodoMapaContenido nodoMapaContenido;
    private NodoMapaContenido nodoMapaContenidoPropuesto;
    private Menu menu;
    private ModeracionDAO moderacionDAOMock;
    private PersonaService personaServiceMock;

    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    String raiz = "/" + UUID.randomUUID().toString() + "/";

    @Before
    @Transactional
    public void insertarNodoMapa()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        new Contenido().setNodoMapaContenidoDAO(nodoMapaContenidoDAO);
        franquicia = new FranquiciaBuilder(franquiciaDAO).withNombre("Franquicia").build();

        menu = new MenuBuilder(menuDAO).withNombre("menu").build();

        nodoMapa = new NodoMapaBuilder(nodoMapaDAO).withParent()
                .withUrlPath("raiz")
                .withUrlCompleta(raiz + "raiz/")
                .withFranquicia(franquicia)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        nodoMapaHijo = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodo2")
                .withUrlCompleta(raiz + "nodo/nodo2/")
                .withFranquicia(franquicia)
                .withParent(nodoMapa)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        personaResponsable = new PersonaBuilder(personaDAO).getAny();

        contenido = new ContenidoBuilder(contenidoDAO).withUrlPath("raiz")
                .withPersonaResponsable(personaResponsable)
                .build();

        nodoMapaContenido =
                new NodoMapaContenidoBuilder(nodoMapaContenidoDAO).withEstadoModeracion(EstadoModeracion.PENDIENTE)
                        .withMapa(nodoMapa)
                        .withContenido(contenido)
                        .build();

        personaServiceMock = mock(PersonaService.class);

        nodoMapaContenidoService =
                new NodoMapaContenidoService(personaServiceMock, nodoMapaContenidoDAO, mock(ContenidoDAO.class),
                        mock(NotificacionService.class), mock(NodoMapaDAO.class));

        nodoMapaContenidoService.setContenidoService(contenidoService);

        moderacionDAOMock = mock(ModeracionDAO.class);

        moderacionService = new ModeracionService(nodoMapaContenidoDAO, moderacionDAOMock, nodoMapaContenidoService,
                mock(NotificacionService.class), moderacionHistoricoService, mock(NodoMapaService.class));
    }

    @Test
    public void siSeAceptaDebeDeModificarseEnElHistorico()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoConMismoUrlPathException,
            ContenidoYaPropuestoException, InsertadoContenidoNoAutorizadoException
    {
        InsertaContenidoAModerar();

        mockComprobacionDeEstadoPendiente(nodoMapaContenidoPropuesto);

        moderacionService.marcaContenidoComoAceptado(nodoMapaContenidoPropuesto.getId(),
                new PersonaBuilder(personaDAO).getAny().getId());

        ModeracionHistorico moderacionHistorico =
                moderacionDAO.obtenerElementoHistoricoModeracion(nodoMapaContenidoPropuesto.getId());

        Assert.assertEquals(moderacionHistorico.getEstadoModeracion(), EstadoModeracion.ACEPTADO.toString());
    }

    @Test
    public void siSeRechazaDebeDeModificarseEnElHistorico()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoConMismoUrlPathException,
            ContenidoYaPropuestoException, InsertadoContenidoNoAutorizadoException
    {
        InsertaContenidoAModerar();

        mockComprobacionDeEstadoPendiente(nodoMapaContenidoPropuesto);

        String textoRechazo = "xxx";

        moderacionService.marcaContenidoComoRechazado(nodoMapaContenidoPropuesto.getId(), textoRechazo,
                new PersonaBuilder(personaDAO).getAny().getId());

        ModeracionHistorico moderacionHistorico =
                moderacionDAO.obtenerElementoHistoricoModeracion(nodoMapaContenidoPropuesto.getId());

        Assert.assertEquals(moderacionHistorico.getEstadoModeracion(), EstadoModeracion.RECHAZADO.toString());
        Assert.assertEquals(moderacionHistorico.getTextoRechazo(), textoRechazo);
    }

    @Test
    public void siSeRechazaSeDebePoderVolverProponer()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoConMismoUrlPathException,
            ContenidoYaPropuestoException, InsertadoContenidoNoAutorizadoException
    {
        InsertaContenidoAModerar();

        mockComprobacionDeEstadoPendiente(nodoMapaContenidoPropuesto);

        String textoRechazo = "xxx";

        moderacionService.marcaContenidoComoRechazado(nodoMapaContenidoPropuesto.getId(), textoRechazo,
                new PersonaBuilder(personaDAO).getAny().getId());

        nodoMapaContenidoService.insertContenidoAModerar(nodoMapaContenidoPropuesto, nodoMapaHijo, contenido.getId(),
                nodoMapaContenido.getId());

        moderacionService.marcaContenidoComoAceptado(nodoMapaContenidoPropuesto.getId(),
                new PersonaBuilder(personaDAO).getAny().getId());

        ModeracionHistorico moderacionHistorico =
                moderacionDAO.obtenerElementoHistoricoModeracion(nodoMapaContenidoPropuesto.getId());

        Assert.assertEquals(moderacionHistorico.getEstadoModeracion(), EstadoModeracion.ACEPTADO.toString());
        Assert.assertNull(moderacionHistorico.getTextoRechazo());
    }

    @Test(expected = ContenidoYaPropuestoException.class)
    public void noSePuedeVolverAProponerUnContenidoAceptado()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoConMismoUrlPathException,
            ContenidoYaPropuestoException, InsertadoContenidoNoAutorizadoException
    {
        InsertaContenidoAModerar();

        mockComprobacionDeEstadoPendiente(nodoMapaContenidoPropuesto);

        moderacionService.marcaContenidoComoAceptado(nodoMapaContenidoPropuesto.getId(),
                new PersonaBuilder(personaDAO).getAny().getId());

        nodoMapaContenidoService.insertContenidoAModerar(nodoMapaContenidoPropuesto, nodoMapaHijo, contenido.getId(),
                nodoMapaContenido.getId());
    }

    @Test(expected = ContenidoYaPropuestoException.class)
    public void noSePuedeVolverAProponerUnContenidoPendiente()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoConMismoUrlPathException,
            ContenidoYaPropuestoException, InsertadoContenidoNoAutorizadoException
    {
        InsertaContenidoAModerar();

        nodoMapaContenidoService.insertContenidoAModerar(nodoMapaContenidoPropuesto, nodoMapaHijo, contenido.getId(),
                nodoMapaContenido.getId());
    }

    @Test
    public void unContenidoExternoDebeDePoderModerarse()
    {
        contenidoService.ajustaAPropuestaExterna(contenido);

        mockComprobacionDeEstadoPendiente(nodoMapaContenido);

        moderacionService.marcaContenidoComoAceptado(nodoMapaContenido.getId(),
                new PersonaBuilder(personaDAO).getAny().getId());

        ModeracionHistorico moderacionHistorico =
                moderacionDAO.obtenerElementoHistoricoModeracion(nodoMapaContenido.getId());

        NodoMapaContenido nodoMapaContenidoAux =
                nodoMapaContenidoDAO.get(NodoMapaContenido.class, nodoMapaContenido.getId()).get(0);

        Assert.assertEquals(moderacionHistorico.getEstadoModeracion(), EstadoModeracion.ACEPTADO.toString());
        Assert.assertEquals(nodoMapaContenidoAux.getEstadoModeracion(), EstadoModeracion.ACEPTADO);
    }

    private void mockComprobacionDeEstadoPendiente(NodoMapaContenido nodoMapaContenido)
    {
        Moderacion moderacion = new Moderacion();
        moderacion.setMapaObjetoId(nodoMapaContenido.getId());

        when(moderacionDAOMock.getElementoPendientesModeracionById(new PersonaBuilder(personaDAO).getAny().getId(),
                nodoMapaContenido.getId())).thenReturn(moderacion);
    }

    private void InsertaContenidoAModerar()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoYaPropuestoException,
            ContenidoConMismoUrlPathException, InsertadoContenidoNoAutorizadoException
    {
        nodoMapaContenidoPropuesto = new NodoMapaContenido();

        nodoMapaContenidoPropuesto.setTipo(TipoReferenciaContenido.LINK);
        nodoMapaContenidoPropuesto.setUpoMapa(nodoMapaHijo);
        nodoMapaContenidoPropuesto.setUpoObjeto(contenido);

        nodoMapaContenidoService.insertContenidoAModerar(nodoMapaContenidoPropuesto, nodoMapaHijo, contenido.getId(),
                nodoMapaContenido.getId());
    }

    @After
    @Transactional
    public void clean()
    {
        nodoMapaDAO.delete(NodoMapa.class, nodoMapa.getId());
        contenidoDAO.delete(Contenido.class, contenido.getId());

        if (nodoMapaContenido != null) contenidoDAO.delete(ModeracionHistorico.class, nodoMapaContenido.getId());

        if (nodoMapaContenidoPropuesto != null)
            contenidoDAO.delete(ModeracionHistorico.class, nodoMapaContenidoPropuesto.getId());
    }
}
