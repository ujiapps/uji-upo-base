package es.uji.apps.upo.moderacion;

import es.uji.apps.upo.dao.ContenidoDAO;
import es.uji.apps.upo.dao.NodoMapaContenidoDAO;
import es.uji.apps.upo.dao.NodoMapaDAO;
import es.uji.apps.upo.dao.PersonaDAO;
import es.uji.apps.upo.exceptions.*;
import es.uji.apps.upo.model.Contenido;
import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.model.NodoMapaContenido;
import es.uji.apps.upo.model.Persona;
import es.uji.apps.upo.model.enums.EstadoModeracion;
import es.uji.apps.upo.model.enums.TipoReferenciaContenido;
import es.uji.apps.upo.services.ContenidoService;
import es.uji.apps.upo.services.NodoMapaContenidoService;
import es.uji.apps.upo.services.NotificacionService;
import es.uji.apps.upo.services.PersonaService;
import es.uji.apps.upo.services.nodomapa.NodoMapaService;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

@Rollback
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class EstadoModeracionTest
{
    private static final long CONTENIDO_ID = 1L;
    private static final long NODO_MAPA_CONTENIDO_ID = 1L;
    private static final long MAPA_ID = 1L;
    private static final long MAPA_ORIGEN_ID = 2L;

    @Autowired
    private PersonaDAO personaDAO;

    private static final String URL = "/soy/administrador/";

    private NodoMapaContenido nodoMapaContenido;
    private NodoMapaContenido nodoMapaContenidoOrigen;
    private NodoMapaContenido nodoMapaContenidoRechazado;

    private NodoMapaContenidoDAO nodoMapaContenidoDAO;
    private NodoMapa nodoMapa;
    private Persona persona;
    private Contenido contenido;
    private PersonaService personaService;
    private NodoMapaContenidoService nodoMapaContenidoService;
    private NodoMapaService nodoMapaService;
    private ContenidoService contenidoService;

    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    @Before
    public void init()
    {
        nodoMapa = mock(NodoMapa.class);
        persona = mock(Persona.class);
        personaService = mock(PersonaService.class);
        contenido = mock(Contenido.class);
        nodoMapaService = mock(NodoMapaService.class);
        contenidoService = mock(ContenidoService.class);

        initDAOOperations();

        nodoMapaContenidoService =
                new NodoMapaContenidoService(personaService, nodoMapaContenidoDAO, mock(ContenidoDAO.class),
                        mock(NotificacionService.class), mock(NodoMapaDAO.class));

        nodoMapaContenidoService.setContenidoService(contenidoService);

        buildNodoMapaContenido();
        buildNodoMapaContenidoRechazado();

        when(nodoMapa.getUrlCompleta()).thenReturn(URL);
        when(contenido.getId()).thenReturn(CONTENIDO_ID);

        initValuesToAvoidExceptionInInsert();
    }

    private void initValuesToAvoidExceptionInInsert()
    {
        initNodoMapaContenidoDeTipoNormalConIdConValor();
        nodoMapaContenido.setTipo(TipoReferenciaContenido.LINK);
    }

    private void initDAOOperations()
    {
        nodoMapaContenidoDAO = mock(NodoMapaContenidoDAO.class);
        nodoMapaContenidoOrigen = mock(NodoMapaContenido.class);

        when(nodoMapaContenidoDAO.getNodoMapaContenidoByContenidoIdAndMapaId(anyLong(), anyLong())).thenReturn(null);
        when(nodoMapaService.getNodoMapa(MAPA_ID, false)).thenReturn(nodoMapa);
        when(contenidoService.getContenido(CONTENIDO_ID)).thenReturn(new Contenido());
        when(personaService.getPersona(anyLong())).thenReturn(persona);
    }

    private void buildNodoMapaContenidoRechazado()
    {
        nodoMapaContenidoRechazado = new NodoMapaContenido();
        nodoMapaContenidoRechazado.setPersonaPropuesta(persona);
        nodoMapaContenidoRechazado.setUpoMapa(nodoMapa);
        nodoMapaContenidoRechazado.setEstadoModeracion(EstadoModeracion.RECHAZADO);
    }

    private void buildNodoMapaContenido()
    {
        nodoMapaContenido = new NodoMapaContenido();
        nodoMapaContenido.setPersonaPropuesta(persona);
        nodoMapaContenido.setUpoMapa(nodoMapa);
        nodoMapaContenido.setUpoObjeto(contenido);
    }

    @Test
    public void estadoModeracionDebeSerAceptadoSiSoyAdministrador()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoYaPropuestoException,
            ContenidoConMismoUrlPathException, InsertadoContenidoNoAutorizadoException
    {
        when(personaService.isNodoMapaEditable(URL, persona)).thenReturn(true);
        when(nodoMapaContenidoDAO.getNodoMapaContenidoByContenidoIdAndMapaId(CONTENIDO_ID, MAPA_ORIGEN_ID)).thenReturn(
                nodoMapaContenidoOrigen);
        when(nodoMapaContenidoOrigen.isNormal()).thenReturn(true);

        nodoMapaContenidoService.insertContenidoAModerar(nodoMapaContenido, nodoMapaService.getNodoMapa(MAPA_ID, false),
                CONTENIDO_ID, NODO_MAPA_CONTENIDO_ID);

        verifyNodoMapaCollaborations();
        assertEquals(EstadoModeracion.ACEPTADO, nodoMapaContenido.getEstadoModeracion());
    }

    @Test
    public void estadoModeracionDebeSerPendienteSiNoSoyAdministrador()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoYaPropuestoException,
            ContenidoConMismoUrlPathException, InsertadoContenidoNoAutorizadoException
    {
        when(personaService.isNodoMapaEditable(URL, persona)).thenReturn(false);
        when(nodoMapaContenidoDAO.getNodoMapaContenidoByContenidoIdAndMapaId(CONTENIDO_ID, MAPA_ORIGEN_ID)).thenReturn(
                nodoMapaContenidoOrigen);
        when(nodoMapaContenidoOrigen.isNormal()).thenReturn(true);

        nodoMapaContenidoService.insertContenidoAModerar(nodoMapaContenido, nodoMapaService.getNodoMapa(MAPA_ID, false),
                CONTENIDO_ID, NODO_MAPA_CONTENIDO_ID);

        verifyNodoMapaCollaborations();
        assertEquals(EstadoModeracion.PENDIENTE, nodoMapaContenido.getEstadoModeracion());
    }

    @Test
    public void estadoModeracionDebeSerAceptadoCuandoHabiaSidoRechazadoYSoyAdministrador()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoYaPropuestoException,
            ContenidoConMismoUrlPathException, InsertadoContenidoNoAutorizadoException
    {
        setMocksToReturnContenidoRechazadoWhenPersonaIsNodoAdmin();

        nodoMapaContenidoService.insertContenidoAModerar(nodoMapaContenido, nodoMapaService.getNodoMapa(MAPA_ID, false),
                CONTENIDO_ID, NODO_MAPA_CONTENIDO_ID);

        verify(personaService, atLeast(1)).isNodoMapaEditable(URL, persona);
        verify(nodoMapaContenidoDAO).update(nodoMapaContenidoRechazado);

        assertEquals(EstadoModeracion.ACEPTADO, nodoMapaContenidoRechazado.getEstadoModeracion());
    }

    private void setMocksToReturnContenidoRechazadoWhenPersonaIsNodoAdmin()
    {
        when(nodoMapaContenidoDAO.getNodoMapaContenidoByContenidoIdAndMapaId(anyLong(), anyLong())).thenReturn(
                nodoMapaContenidoRechazado);
        when(personaService.isNodoMapaEditable(URL, persona)).thenReturn(true);
    }

    @Test
    public void estadoModeracionDebeSerPendienteCuandoHabiaSidoRechazadoYNoSoyAdministrador()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoYaPropuestoException,
            ContenidoConMismoUrlPathException, InsertadoContenidoNoAutorizadoException
    {
        setMocksToReturnContenidoRechazadoWhenPersonaIsNotNodoAdmin();

        nodoMapaContenidoService.insertContenidoAModerar(nodoMapaContenido, nodoMapaService.getNodoMapa(MAPA_ID, false),
                CONTENIDO_ID, NODO_MAPA_CONTENIDO_ID);

        verify(personaService, atLeast(1)).isNodoMapaEditable(URL, persona);
        verify(nodoMapaContenidoDAO).update(nodoMapaContenidoRechazado);

        assertEquals(EstadoModeracion.PENDIENTE, nodoMapaContenido.getEstadoModeracion());
    }

    @Test
    public void elCampoOrdenDebeSerElMismo()
            throws ContenidoYaPropuestoException, NodoMapaContenidoDebeTenerUnContenidoNormalException,
            ContenidoConMismoUrlPathException, InsertadoContenidoNoAutorizadoException
    {
        Long ordenValue = 100L;
        NodoMapaContenido nodoMapaContenidoOriginal = new NodoMapaContenido();
        nodoMapaContenidoOriginal.setOrden(ordenValue);

        when(nodoMapaContenidoDAO.getNodoMapaContenidoAndFechasEvento(1L)).thenReturn(nodoMapaContenidoOriginal);

        nodoMapaContenidoService.insertContenidoAModerar(nodoMapaContenido, nodoMapaService.getNodoMapa(MAPA_ID, false),
                CONTENIDO_ID, NODO_MAPA_CONTENIDO_ID);

        assertTrue(nodoMapaContenido.getOrden().equals(ordenValue));
    }

    private void setMocksToReturnContenidoRechazadoWhenPersonaIsNotNodoAdmin()
    {
        when(nodoMapaContenidoDAO.getNodoMapaContenidoByContenidoIdAndMapaId(anyLong(), anyLong())).thenReturn(
                nodoMapaContenidoRechazado);
        when(personaService.isNodoMapaEditable(URL, persona)).thenReturn(false);
    }

    private void verifyNodoMapaCollaborations()
    {
        verify(personaService).isNodoMapaEditable(URL, persona);
        verify(nodoMapaContenidoDAO).insert(nodoMapaContenido);
    }

    private void initNodoMapaContenidoDeTipoNormalConIdConValor()
    {
        NodoMapaContenido nodoMapaContenido = new NodoMapaContenido();
        nodoMapaContenido.setId(CONTENIDO_ID);

        when(nodoMapaContenidoDAO.getNodoMapaContenidoTipoNormalByContenidoId(anyLong())).thenReturn(nodoMapaContenido);
    }
}