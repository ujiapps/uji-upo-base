package es.uji.apps.upo.moderacion;

import es.uji.apps.upo.builders.*;
import es.uji.apps.upo.dao.*;
import es.uji.apps.upo.exceptions.*;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.EstadoModeracion;
import es.uji.apps.upo.model.enums.TipoFranquiciaAcceso;
import es.uji.apps.upo.model.enums.TipoReferenciaContenido;
import es.uji.apps.upo.services.*;
import es.uji.apps.upo.services.nodomapa.NodoMapaService;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@Rollback
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class ModeracionTest
{
    @Autowired
    private NodoMapaDAO nodoMapaDAO;

    @Autowired
    private NodoMapaService nodoMapaService;

    @Autowired
    private ContenidoDAO contenidoDAO;

    @Autowired
    private NodoMapaContenidoDAO nodoMapaContenidoDAO;

    @Autowired
    private ContenidoService contenidoService;

    @Autowired
    private FranquiciaDAO franquiciaDAO;

    @Autowired
    private PersonaDAO personaDAO;

    @Autowired
    private MenuDAO menuDAO;

    @Autowired
    private ModeracionDAO moderacionDAO;

    private NodoMapaContenidoService nodoMapaContenidoService;

    private ModeracionService moderacionService;

    private Persona personaResponsable;
    private Franquicia franquicia;
    private Contenido contenido;
    private Contenido contenidoHijo;
    private NodoMapa nodoMapa;
    private NodoMapa nodoMapaHijo;
    private NodoMapa nodoMapaNieto;
    private NodoMapaContenido nodoMapaContenido;
    private NodoMapaContenido nodoMapaContenidoHijo;
    private NodoMapaContenido nodoMapaContenidoPropuesto;
    private NodoMapaContenido nodoMapaContenidoPropuestoHijo;
    private Menu menu;
    private PersonaService personaServiceMock;
    private NotificacionService notificacionServiceMock;

    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    String raiz = "/" + UUID.randomUUID().toString() + "/";

    @Before
    @Transactional
    public void insertarNodoMapa()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        new Contenido().setNodoMapaContenidoDAO(nodoMapaContenidoDAO);
        franquicia = new FranquiciaBuilder(franquiciaDAO).withNombre("Franquicia").build();

        personaResponsable = new PersonaBuilder(personaDAO).getAny();
        personaResponsable.setUpoFranquiciasAccesos(new HashSet<>());

        new FranquiciaAccesoBuilder(franquiciaDAO).withFranquicia(franquicia)
                .withPersona(personaResponsable)
                .withTipo(TipoFranquiciaAcceso.ADMINISTRADOR)
                .build();

        menu = new MenuBuilder(menuDAO).withNombre("menu").build();

        nodoMapa = new NodoMapaBuilder(nodoMapaDAO).withParent()
                .withUrlPath("nodo")
                .withUrlCompleta(raiz + "nodo/")
                .withFranquicia(franquicia)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        nodoMapaHijo = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodo2")
                .withUrlCompleta(raiz + "nodo/nodo2/")
                .withFranquicia(franquicia)
                .withParent(nodoMapa)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        nodoMapaNieto = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodo3")
                .withUrlCompleta(raiz + "nodo/nodo2/nodo3")
                .withFranquicia(franquicia)
                .withParent(nodoMapaHijo)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        contenido = new ContenidoBuilder(contenidoDAO).withUrlPath("raiz")
                .withPersonaResponsable(personaResponsable)
                .build();

        contenidoHijo = new ContenidoBuilder(contenidoDAO).withUrlPath("raiz2")
                .withPersonaResponsable(personaResponsable)
                .build();

        nodoMapaContenido =
                new NodoMapaContenidoBuilder(nodoMapaContenidoDAO).withEstadoModeracion(EstadoModeracion.PENDIENTE)
                        .withMapa(nodoMapa)
                        .withContenido(contenido)
                        .build();

        nodoMapaContenidoHijo =
                new NodoMapaContenidoBuilder(nodoMapaContenidoDAO).withEstadoModeracion(EstadoModeracion.PENDIENTE)
                        .withMapa(nodoMapa)
                        .withContenido(contenidoHijo)
                        .build();

        personaServiceMock = mock(PersonaService.class);

        nodoMapaContenidoService = new NodoMapaContenidoService(personaServiceMock, nodoMapaContenidoDAO, contenidoDAO,
                mock(NotificacionService.class), nodoMapaDAO);

        nodoMapaContenidoService.setContenidoService(contenidoService);

        notificacionServiceMock = mock(NotificacionService.class);

        moderacionService = new ModeracionService(nodoMapaContenidoDAO, moderacionDAO, nodoMapaContenidoService,
                notificacionServiceMock, mock(ModeracionHistoricoService.class), mock(NodoMapaService.class));
    }

    @Test
    public void siAceptamosUnNodoDebeDeDesmarcarTodosLosContenidosDescendientesDePendientes()
            throws ContenidoYaPropuestoException, ContenidoConMismoUrlPathException,
            NodoMapaContenidoDebeTenerUnContenidoNormalException, InsertadoContenidoNoAutorizadoException
    {
        InsertaContenidoAModerar();

        moderacionService.marcaContenidoNodosComoAceptado(nodoMapaHijo, personaResponsable);

        assertTrue(moderacionDAO.getElementosDescendientesPendientesModeracionByUrlCompleta(personaResponsable.getId(),
                nodoMapaHijo.getUrlCompleta()).size() == 0);
    }

    @Test
    public void siAceptamosUnNodoDebeDeMarcarTodosLosContenidosDescendientesComoAceptados()
            throws ContenidoYaPropuestoException, ContenidoConMismoUrlPathException,
            NodoMapaContenidoDebeTenerUnContenidoNormalException, InsertadoContenidoNoAutorizadoException
    {
        InsertaContenidoAModerar();

        moderacionService.marcaContenidoNodosComoAceptado(nodoMapaHijo, personaResponsable);

        NodoMapaContenido nodoMapaContenidoRecuperado =
                nodoMapaContenidoService.getNodoMapaContenido(nodoMapaContenidoPropuesto.getId());
        NodoMapaContenido nodoMapaContenidoRecuperadoHijo =
                nodoMapaContenidoService.getNodoMapaContenido(nodoMapaContenidoPropuestoHijo.getId());

        assertTrue(nodoMapaContenidoRecuperado.getEstadoModeracion().equals(EstadoModeracion.ACEPTADO));
        assertTrue(nodoMapaContenidoRecuperadoHijo.getEstadoModeracion().equals(EstadoModeracion.ACEPTADO));
    }

    @Test
    public void siRechazamosUnNodoDebeDeMarcarTodosLosContenidosDescendientesComoRechazados()
            throws ContenidoYaPropuestoException, ContenidoConMismoUrlPathException,
            NodoMapaContenidoDebeTenerUnContenidoNormalException, BorradoContenidoNoAutorizadoException,
            NodoConNombreYaExistenteEnMismoPadreException, UsuarioNoAutenticadoException,
            InsertadoContenidoNoAutorizadoException
    {
        InsertaContenidoAModerar();

        moderacionService.marcaContenidoNodosComoRechazado(nodoMapaHijo, "prova", personaResponsable);

        NodoMapaContenido nodoMapaContenidoRecuperado =
                nodoMapaContenidoService.getNodoMapaContenido(nodoMapaContenidoPropuesto.getId());
        NodoMapaContenido nodoMapaContenidoRecuperadoHijo =
                nodoMapaContenidoService.getNodoMapaContenido(nodoMapaContenidoPropuestoHijo.getId());

        assertTrue(nodoMapaContenidoRecuperado.getEstadoModeracion().equals(EstadoModeracion.RECHAZADO));
        assertTrue(nodoMapaContenidoRecuperadoHijo.getEstadoModeracion().equals(EstadoModeracion.RECHAZADO));
    }

    @Test
    public void siRechazamosUnNodoDebeDeDesmarcarTodosLosContenidosDescendientesDePendientes()
            throws ContenidoYaPropuestoException, ContenidoConMismoUrlPathException,
            NodoMapaContenidoDebeTenerUnContenidoNormalException, BorradoContenidoNoAutorizadoException,
            NodoConNombreYaExistenteEnMismoPadreException, UsuarioNoAutenticadoException,
            InsertadoContenidoNoAutorizadoException
    {
        InsertaContenidoAModerar();

        moderacionService.marcaContenidoNodosComoRechazado(nodoMapaHijo, "prova", personaResponsable);

        assertTrue(moderacionDAO.getElementosDescendientesPendientesModeracionByUrlCompleta(personaResponsable.getId(),
                nodoMapaHijo.getUrlCompleta()).size() == 0);
    }

    @Test
    public void siAceptamosUnNodoDebeDesBloquearLosNodosDescendientes()
            throws ContenidoYaPropuestoException, ContenidoConMismoUrlPathException,
            NodoMapaContenidoDebeTenerUnContenidoNormalException, InsertadoContenidoNoAutorizadoException
    {
        InsertaContenidoAModerar();

        moderacionService.marcaContenidoNodosComoAceptado(nodoMapaHijo, personaResponsable);

        NodoMapa nodoMapaRecuperadoHijo = nodoMapaDAO.getNodoMapaByUrlCompleta(nodoMapaHijo.getUrlCompleta());
        NodoMapa nodoMapaRecuperadoNieto = nodoMapaDAO.getNodoMapaByUrlCompleta(nodoMapaNieto.getUrlCompleta());

        assertFalse(nodoMapaRecuperadoHijo.isBloqueado());
        assertFalse(nodoMapaRecuperadoNieto.isBloqueado());
    }

    @Test
    public void siRechazamosUnNodoDebeEliminarLosNodosDescendientes()
            throws ContenidoYaPropuestoException, ContenidoConMismoUrlPathException,
            NodoMapaContenidoDebeTenerUnContenidoNormalException, BorradoContenidoNoAutorizadoException,
            NodoConNombreYaExistenteEnMismoPadreException, UsuarioNoAutenticadoException,
            InsertadoContenidoNoAutorizadoException
    {
        InsertaContenidoAModerar();

        moderacionService.marcaContenidoNodosComoRechazado(nodoMapaHijo, "prova", personaResponsable);

        NodoMapa nodoMapaRecuperadoHijo = nodoMapaDAO.getNodoMapaByUrlCompleta(nodoMapaHijo.getUrlCompleta());
        NodoMapa nodoMapaRecuperadoNieto = nodoMapaDAO.getNodoMapaByUrlCompleta(nodoMapaNieto.getUrlCompleta());

        assertFalse(nodoMapaRecuperadoHijo.getUrlCompleta().startsWith("/paperera/"));
        assertFalse(nodoMapaRecuperadoNieto.getUrlCompleta().startsWith("/paperera/"));
    }

    @Test
    public void siAceptamosDebeDeEnviarseUnCorreo()
            throws ContenidoYaPropuestoException, ContenidoConMismoUrlPathException,
            NodoMapaContenidoDebeTenerUnContenidoNormalException, InsertadoContenidoNoAutorizadoException
    {
        InsertaContenidoAModerar();

        moderacionService.marcaContenidoNodosComoAceptado(nodoMapaHijo, personaResponsable);

        verify(notificacionServiceMock).enviaCorreoResultadoModeracion(any(String.class), any(String.class),
                any(String.class), anyList(), any(String.class));
    }

    @Test
    public void siRechazamosDebeDeEnviarseUnCorreo()
            throws ContenidoYaPropuestoException, ContenidoConMismoUrlPathException,
            NodoMapaContenidoDebeTenerUnContenidoNormalException, BorradoContenidoNoAutorizadoException,
            NodoConNombreYaExistenteEnMismoPadreException, UsuarioNoAutenticadoException,
            InsertadoContenidoNoAutorizadoException
    {
        InsertaContenidoAModerar();

        moderacionService.marcaContenidoNodosComoRechazado(nodoMapaHijo, "prova", personaResponsable);

        verify(notificacionServiceMock).enviaCorreoResultadoModeracion(any(String.class), any(String.class),
                any(String.class), anyList(), any(String.class));
    }

    @Test(expected = InsertadoContenidoNoAutorizadoException.class)
    public void siProponemosEnUnNodoBloqueadoDebeDeDarError()
            throws ContenidoYaPropuestoException, InsertadoContenidoNoAutorizadoException,
            NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoConMismoUrlPathException
    {
        nodoMapaContenidoPropuesto = new NodoMapaContenido();

        nodoMapaContenidoPropuesto.setTipo(TipoReferenciaContenido.LINK);
        nodoMapaContenidoPropuesto.setUpoMapa(nodoMapa);
        nodoMapaContenidoPropuesto.setUpoObjeto(contenido);

        nodoMapa.setBloqueado(true);
        nodoMapaDAO.update(nodoMapa);

        nodoMapaContenidoService.insertContenidoAModerar(nodoMapaContenidoPropuesto, nodoMapaHijo, contenido.getId(),
                nodoMapaContenido.getId());
    }

    @Test(expected = NodoNoSePuedeProponerException.class)
    public void siProponemosUnNodoAOtroBloqueadoDebeDeDarError()
            throws ContenidoYaPropuestoException, NodoMapaContenidoDebeTenerUnContenidoNormalException,
            ContenidoConMismoUrlPathException, NodoConNombreYaExistenteEnMismoPadreException,
            CrearNuevoContenidoException, TipoContenidoNoAdecuadoException, ActualizadoContenidoNoAutorizadoException,
            NodoNoSePuedeProponerException
    {
        NodoMapa nodoMapaHermano = new NodoMapaBuilder(nodoMapaDAO).withUrlPath("nodoX")
                .withUrlCompleta(raiz + "nodo/nodoX/")
                .withFranquicia(franquicia)
                .withParent(nodoMapa)
                .withMenu(menu)
                .withOrden(1L)
                .build();

        nodoMapaHermano.setBloqueado(true);
        nodoMapaDAO.update(nodoMapaHermano);

        nodoMapaService.crearNodoMapaHijoYEnlacesAContenidos(nodoMapaHermano, nodoMapaNieto, personaResponsable, true, false);
    }

    private void InsertaContenidoAModerar()
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoYaPropuestoException,
            ContenidoConMismoUrlPathException, InsertadoContenidoNoAutorizadoException
    {
        Persona personaPropuesta = new Persona();
        personaPropuesta.setId(5121L);

        nodoMapaContenidoPropuesto = new NodoMapaContenido();

        nodoMapaContenidoPropuesto.setTipo(TipoReferenciaContenido.LINK);
        nodoMapaContenidoPropuesto.setUpoMapa(nodoMapa);
        nodoMapaContenidoPropuesto.setUpoObjeto(contenido);
        nodoMapaContenidoPropuesto.setPersonaPropuesta(personaPropuesta);

        nodoMapaContenidoService.insertContenidoAModerar(nodoMapaContenidoPropuesto, nodoMapaHijo, contenido.getId(),
                nodoMapaContenido.getId());

        nodoMapaContenidoPropuestoHijo = new NodoMapaContenido();

        nodoMapaContenidoPropuestoHijo.setTipo(TipoReferenciaContenido.LINK);
        nodoMapaContenidoPropuestoHijo.setUpoMapa(nodoMapaHijo);
        nodoMapaContenidoPropuestoHijo.setUpoObjeto(contenido);
        nodoMapaContenidoPropuestoHijo.setPersonaPropuesta(personaPropuesta);

        nodoMapaContenidoService.insertContenidoAModerar(nodoMapaContenidoPropuestoHijo, nodoMapaNieto,
                contenido.getId(), nodoMapaContenidoHijo.getId());
    }

    @After
    @Transactional
    public void clean()
    {
        nodoMapaDAO.delete(NodoMapa.class, nodoMapa.getId());
        contenidoDAO.delete(Contenido.class, contenido.getId());
    }
}
