package es.uji.apps.upo.youtube;

import es.uji.apps.upo.exceptions.TraducirTextoException;
import es.uji.apps.upo.translator.TranslatorClient;
import es.uji.apps.upo.utils.YoutubeUrlFormatter;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class YoutubeUrlFormatterTest
{
    private YoutubeUrlFormatter youtubeUrlFormatter;
    private String urlCorrect = "https://www.youtube.com/embed/dLRjiiAawGg";

    @Before
    public void init()
    {
        youtubeUrlFormatter = new YoutubeUrlFormatter();
    }

    @org.junit.Test
    public void urlFormatterFromLongUrl()
    {
        String urlFormatted = youtubeUrlFormatter.format("https://www.youtube.com/watch?v=dLRjiiAawGg");

        assertThat(urlFormatted, is(equalTo(urlCorrect)));

        urlFormatted = youtubeUrlFormatter.format("http://www.youtube.com/watch?v=dLRjiiAawGg");

        assertThat(urlFormatted, is(equalTo(urlCorrect)));
    }

    @org.junit.Test
    public void urlFormatterFromShortrl()
    {
        String urlFormatted = youtubeUrlFormatter.format("https://youtu.be/dLRjiiAawGg");

        assertThat(urlFormatted, is(equalTo(urlCorrect)));

        urlFormatted = youtubeUrlFormatter.format("http://youtu.be/dLRjiiAawGg");

        assertThat(urlFormatted, is(equalTo(urlCorrect)));
    }

    @org.junit.Test
    public void urlFormatterFromEmbedUrl()
    {
        String urlFormatted = youtubeUrlFormatter.format(urlCorrect);

        assertThat(urlFormatted, is(equalTo(urlCorrect)));
    }

    @org.junit.Test
    public void urlFormatterFromId()
    {
        String urlFormatted = youtubeUrlFormatter.format("dLRjiiAawGg");

        assertThat(urlFormatted, is(equalTo(urlCorrect)));
    }

    @org.junit.Test
    public void urlFormatterFromProblematicUrl()
    {
        String urlFormatted = youtubeUrlFormatter.format("https://www.youtube.com/embed/vDBZpgCnIQ0");

        assertThat(urlFormatted, is(equalTo("https://www.youtube.com/embed/vDBZpgCnIQ0")));
    }
}
