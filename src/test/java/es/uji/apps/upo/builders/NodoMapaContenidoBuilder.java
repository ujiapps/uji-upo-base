package es.uji.apps.upo.builders;

import java.util.HashSet;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.upo.exceptions.NodoMapaContenidoDebeTenerUnContenidoNormalException;
import es.uji.apps.upo.dao.NodoMapaContenidoDAO;
import es.uji.apps.upo.model.Contenido;
import es.uji.apps.upo.model.enums.EstadoModeracion;
import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.model.NodoMapaContenido;
import es.uji.apps.upo.model.enums.TipoReferenciaContenido;

public class NodoMapaContenidoBuilder
{
    private NodoMapaContenidoDAO nodoMapaContenidoDAO;
    private NodoMapaContenido nodoMapaContenido;

    public NodoMapaContenidoBuilder(NodoMapaContenidoDAO nodoMapaContenidoDAO)
    {
        nodoMapaContenido = new NodoMapaContenido();
        this.nodoMapaContenidoDAO = nodoMapaContenidoDAO;
    }

    public NodoMapaContenidoBuilder withEstadoModeracion(EstadoModeracion estadoModeracion)
    {
        nodoMapaContenido.setEstadoModeracion(estadoModeracion);
        return this;
    }

    public NodoMapaContenidoBuilder withMapa(NodoMapa nodoMapa)
    {
        Set<NodoMapaContenido> nodoMapaContenidos;

        if (nodoMapa.getUpoMapasObjetos() != null)
        {
            nodoMapaContenidos = nodoMapa.getUpoMapasObjetos();
        }
        else
        {
            nodoMapaContenidos = new HashSet<NodoMapaContenido>();
        }

        nodoMapaContenidos.add(nodoMapaContenido);
        nodoMapa.setUpoMapasObjetos(nodoMapaContenidos);

        nodoMapaContenido.setUpoMapa(nodoMapa);
        return this;
    }

    public NodoMapaContenidoBuilder withContenido(Contenido contenido)
    {
        Set<NodoMapaContenido> nodoMapaContenidos;

        if (contenido.getUpoMapasObjetos() != null)
        {
            nodoMapaContenidos = contenido.getUpoMapasObjetos();
        }
        else
        {
            nodoMapaContenidos = new HashSet<NodoMapaContenido>();
        }

        nodoMapaContenidos.add(nodoMapaContenido);
        contenido.setUpoMapasObjetos(nodoMapaContenidos);

        nodoMapaContenido.setUpoObjeto(contenido);
        return this;
    }

    public NodoMapaContenidoBuilder withTipo(TipoReferenciaContenido tipo)
    {
        nodoMapaContenido.setTipo(tipo);
        return this;
    }

    public NodoMapaContenidoBuilder withOrden(Long orden)
    {
        nodoMapaContenido.setOrden(orden);
        return this;
    }

    @Transactional
    public NodoMapaContenido build() throws NodoMapaContenidoDebeTenerUnContenidoNormalException
    {
        return nodoMapaContenidoDAO.insert(nodoMapaContenido);
    }
}