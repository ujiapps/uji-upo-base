package es.uji.apps.upo.builders;

import java.util.HashSet;
import java.util.Set;

import es.uji.apps.upo.model.enums.TipoFranquiciaAcceso;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.upo.dao.FranquiciaDAO;
import es.uji.apps.upo.model.Franquicia;
import es.uji.apps.upo.model.FranquiciaAcceso;
import es.uji.apps.upo.model.Persona;

public class FranquiciaAccesoBuilder
{
    private final FranquiciaDAO franquiciaDAO;
    private FranquiciaAcceso franquiciaAcceso;

    public FranquiciaAccesoBuilder(FranquiciaDAO franquiciaDAO)
    {
        franquiciaAcceso = new FranquiciaAcceso();
        this.franquiciaDAO = franquiciaDAO;
    }

    public FranquiciaAccesoBuilder withPersona(Persona persona)
    {
        Set<FranquiciaAcceso> franquiciasAccesos;

        if (persona.getUpoFranquiciasAccesos() != null)
        {
            franquiciasAccesos = persona.getUpoFranquiciasAccesos();
        }
        else
        {
            franquiciasAccesos = new HashSet<FranquiciaAcceso>();
        }

        franquiciaAcceso.setUpoExtPersona(persona);
        franquiciasAccesos.add(franquiciaAcceso);
        persona.setUpoFranquiciasAccesos(franquiciasAccesos);

        franquiciaAcceso.setUpoExtPersona(persona);
        return this;
    }

    public FranquiciaAccesoBuilder withFranquicia(Franquicia franquicia)
    {
        Set<FranquiciaAcceso> franquiciasAccesos;

        if (franquicia.getUpoFranquiciasAccesos() != null)
        {
            franquiciasAccesos = franquicia.getUpoFranquiciasAccesos();
        }
        else
        {
            franquiciasAccesos = new HashSet<FranquiciaAcceso>();
        }

        franquiciasAccesos.add(franquiciaAcceso);
        franquicia.setUpoFranquiciasAccesos(franquiciasAccesos);

        franquiciaAcceso.setUpoFranquicia(franquicia);
        return this;
    }

    public FranquiciaAccesoBuilder withTipo(TipoFranquiciaAcceso tipo)
    {
        franquiciaAcceso.setTipo(tipo);

        return this;
    }

    @Transactional
    public FranquiciaAcceso build()
    {
        return franquiciaDAO.insert(franquiciaAcceso);
    }
}
