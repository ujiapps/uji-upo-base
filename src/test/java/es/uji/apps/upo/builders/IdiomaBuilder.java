package es.uji.apps.upo.builders;

import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.upo.dao.IdiomaDAO;
import es.uji.apps.upo.model.Idioma;

public class IdiomaBuilder
{
    private IdiomaDAO idiomaDAO;
    private Idioma idioma;

    public IdiomaBuilder(IdiomaDAO idiomaDAO)
    {
        idioma = new Idioma();
        this.idiomaDAO = idiomaDAO;
    }

    public IdiomaBuilder withNombre(String nombre)
    {
        idioma.setNombre(nombre);
        return this;
    }
    
    public IdiomaBuilder withCodigoISO(String codigoISO)
    {
        idioma.setCodigoISO(codigoISO);
        return this;
    }

    @Transactional
    public Idioma build()
    {
        return idiomaDAO.insert(idioma);
    }
}