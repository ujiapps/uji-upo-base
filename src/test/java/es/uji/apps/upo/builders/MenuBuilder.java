package es.uji.apps.upo.builders;

import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.upo.dao.MenuDAO;
import es.uji.apps.upo.model.Menu;

public class MenuBuilder
{
    private MenuDAO menuDAO;
    private Menu menu;

    public MenuBuilder(MenuDAO menuDAO)
    {
        menu = new Menu();
        this.menuDAO = menuDAO;
    }

    public MenuBuilder withNombre(String nombre)
    {
        menu.setNombre(nombre);
        return this;
    }

    @Transactional
    public Menu build()
    {
        return menuDAO.insert(menu);
    }
}