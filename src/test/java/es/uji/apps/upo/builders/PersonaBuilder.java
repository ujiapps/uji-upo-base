package es.uji.apps.upo.builders;

import es.uji.apps.upo.dao.PersonaDAO;
import es.uji.apps.upo.model.Persona;

import java.util.HashSet;

public class PersonaBuilder
{
    private PersonaDAO personaDAO;
    private Persona persona;

    public PersonaBuilder(PersonaDAO personaDAO)
    {
        persona = new Persona();
        persona.setUpoFranquiciasAccesos(new HashSet<>());

        this.personaDAO = personaDAO;
    }

    public Persona getAny()
    {
        return personaDAO.getPersona(40000L);
    }

    /*@Transactional
    public Persona build()
    {
        return personaDAO.insert(persona);
    }*/
}
