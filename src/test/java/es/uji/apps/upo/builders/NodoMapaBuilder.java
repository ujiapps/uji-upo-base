package es.uji.apps.upo.builders;

import java.util.HashSet;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.upo.dao.NodoMapaDAO;
import es.uji.apps.upo.model.Franquicia;
import es.uji.apps.upo.model.Menu;
import es.uji.apps.upo.model.NodoMapa;

public class NodoMapaBuilder
{
    private NodoMapaDAO nodoMapaDAO;
    private NodoMapa nodoMapa;

    public NodoMapaBuilder()
    {
    }

    public NodoMapaBuilder(NodoMapaDAO nodoMapaDAO)
    {
        nodoMapa = new NodoMapa();
        nodoMapa.setUpoMapas(new HashSet<>());
        this.nodoMapaDAO = nodoMapaDAO;
    }

    public NodoMapaBuilder withUrlPath(String urlPath)
    {
        nodoMapa.setUrlPath(urlPath);
        return this;
    }

    public NodoMapaBuilder withUrlCompleta(String urlCompleta)
    {
        nodoMapa.setUrlCompleta(urlCompleta);
        return this;
    }

    public NodoMapaBuilder withFranquicia(Franquicia franquicia)
    {
        Set<NodoMapa> nodosMapa;

        if (franquicia.getUpoMapas() != null)
        {
            nodosMapa = franquicia.getUpoMapas();

        }
        else
        {
            nodosMapa = new HashSet<NodoMapa>();
        }

        nodosMapa.add(nodoMapa);
        franquicia.setUpoMapas(nodosMapa);

        nodoMapa.setUpoFranquicia(franquicia);
        return this;
    }

    public NodoMapaBuilder withParent()
    {
        NodoMapa nodoMapaRaiz = nodoMapaDAO.getNodoMapaById(0L).get(0);
        nodoMapaRaiz.setUpoMapas(null);

        return withParent(nodoMapaRaiz);
    }

    public NodoMapaBuilder withParent(NodoMapa nodoMapaPadre)
    {
        Set<NodoMapa> nodosMapa;

        if (nodoMapaPadre.getUpoMapas() != null)
        {
            nodosMapa = nodoMapaPadre.getUpoMapas();
        }
        else
        {
            nodosMapa = new HashSet<NodoMapa>();
        }

        nodosMapa.add(nodoMapa);
        nodoMapaPadre.setUpoMapas(nodosMapa);

        nodoMapa.setUpoMapa(nodoMapaPadre);
        return this;
    }

    public NodoMapaBuilder withMenu(Menu menu)
    {
        Set<NodoMapa> nodosMapa;

        if (menu.getNodoMapa() != null)
        {
            nodosMapa = menu.getNodoMapa();

        }
        else
        {
            nodosMapa = new HashSet<NodoMapa>();
        }

        nodosMapa.add(nodoMapa);
        menu.setNodoMapa(nodosMapa);

        nodoMapa.setMenu(menu);
        return this;
    }

    public NodoMapaBuilder withMenuHeredado(Boolean menuHeredado)
    {
        nodoMapa.setMenuHeredado(menuHeredado);
        return this;
    }

    public NodoMapaBuilder withOrden(Long orden)
    {
        nodoMapa.setOrden(orden);
        return this;
    }

    public NodoMapaBuilder withNumItems(Long numItems)
    {
        nodoMapa.setNumItemsSincro(numItems);
        return this;
    }

    public NodoMapaBuilder withSincroDesdeMapa(String sincroDesdeMapa)
    {
        nodoMapa.setSincroDesdeNodoMapa(sincroDesdeMapa);
        return this;
    }

    @Transactional
    public NodoMapa build()
    {
        return nodoMapaDAO.insert(nodoMapa);
    }
}