package es.uji.apps.upo.builders;

import java.util.HashSet;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.upo.dao.ContenidoIdiomaDAO;
import es.uji.apps.upo.model.Contenido;
import es.uji.apps.upo.model.ContenidoIdioma;
import es.uji.apps.upo.model.Idioma;

public class ContenidoIdiomaBuilder
{
    private ContenidoIdiomaDAO contenidoIdiomaDAO;
    private ContenidoIdioma contenidoIdioma;

    public ContenidoIdiomaBuilder(ContenidoIdiomaDAO contenidoIdiomaDAO)
    {
        contenidoIdioma = new ContenidoIdioma();
        this.contenidoIdiomaDAO = contenidoIdiomaDAO;
    }

    public ContenidoIdiomaBuilder withHtml(String html)
    {
        contenidoIdioma.setHtml(html);
        return this;
    }

    public ContenidoIdiomaBuilder withTipoFormato(String tipoFormato)
    {
        contenidoIdioma.setTipoFormato(tipoFormato);
        return this;
    }

    public ContenidoIdiomaBuilder withTitulo(String titulo)
    {
        contenidoIdioma.setTitulo(titulo);
        return this;
    }

    public ContenidoIdiomaBuilder withIdioma(Idioma idioma)
    {
        Set<ContenidoIdioma> contenidoIdiomas;

        if (idioma.getUpoObjetosIdiomas() != null)
        {
            contenidoIdiomas = idioma.getUpoObjetosIdiomas();

        }
        else
        {
            contenidoIdiomas = new HashSet<ContenidoIdioma>();
        }

        contenidoIdiomas.add(contenidoIdioma);
        idioma.setUpoObjetosIdiomas(contenidoIdiomas);

        contenidoIdioma.setUpoIdioma(idioma);
        return this;
    }

    public ContenidoIdiomaBuilder withContenido(Contenido contenido)
    {
        Set<ContenidoIdioma> contenidoIdiomas;

        if (contenido.getContenidoIdiomas() != null)
        {
            contenidoIdiomas = contenido.getContenidoIdiomas();

        }
        else
        {
            contenidoIdiomas = new HashSet<ContenidoIdioma>();
        }

        contenidoIdiomas.add(contenidoIdioma);
        contenido.setContenidoIdiomas(contenidoIdiomas);

        contenidoIdioma.setUpoObjeto(contenido);
        return this;
    }

    @Transactional
    public ContenidoIdioma build()
    {
        return contenidoIdiomaDAO.insert(contenidoIdioma);
    }
}