package es.uji.apps.upo.builders;

import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.upo.dao.FranquiciaDAO;
import es.uji.apps.upo.model.Franquicia;

public class FranquiciaBuilder
{
    private final FranquiciaDAO franquiciaDAO;
    private Franquicia franquicia;

    public FranquiciaBuilder(FranquiciaDAO franquiciaDAO)
    {
        franquicia = new Franquicia();
        this.franquiciaDAO = franquiciaDAO;
    }

    public FranquiciaBuilder withNombre(String string)
    {
        franquicia.setNombre("Franquicia");
        return this;
    }

    @Transactional
    public Franquicia build()
    {
        return franquiciaDAO.insert(franquicia);
    }
}
