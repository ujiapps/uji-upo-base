package es.uji.apps.upo.builders;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.upo.dao.ContenidoDAO;
import es.uji.apps.upo.model.Contenido;
import es.uji.apps.upo.model.Persona;

public class ContenidoBuilder
{
    private ContenidoDAO contenidoDAO;
    private Contenido contenido;

    public ContenidoBuilder(ContenidoDAO contenidoDAO)
    {
        contenido = new Contenido();
        this.contenidoDAO = contenidoDAO;
    }

    public ContenidoBuilder withUrlPath(String urlPath)
    {
        contenido.setUrlPath(urlPath);
        return this;
    }

    public ContenidoBuilder withFechaCreacion(Date fechaCreacion)
    {
        contenido.setFechaCreacion(fechaCreacion);
        return this;
    }

    public ContenidoBuilder withPersonaResponsable(Persona persona)
    {
        Set<Contenido> contenidos = new HashSet<Contenido>();

        contenidos.add(contenido);
        persona.setUpoObjetos1(contenidos);

        contenido.setUpoExtPersona1(persona);
        return this;
    }

    @Transactional
    public Contenido build()
    {
        return contenidoDAO.insert(contenido);
    }
}