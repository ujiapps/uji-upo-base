package es.uji.apps.upo.builders;

import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.upo.dao.PlantillaDAO;
import es.uji.apps.upo.model.Plantilla;
import es.uji.apps.upo.model.enums.TipoPlantilla;

public class PlantillaBuilder
{
    private final PlantillaDAO plantillaDAO;
    private Plantilla plantilla;

    public PlantillaBuilder(PlantillaDAO plantillaDAO)
    {
        plantilla = new Plantilla();
        this.plantillaDAO = plantillaDAO;
    }

    public PlantillaBuilder withNombre(String string)
    {
        plantilla.setNombre("Plantilla");
        return this;
    }

    public PlantillaBuilder withTipo(TipoPlantilla tipoPlantilla)
    {
        plantilla.setTipo(tipoPlantilla);
        return this;
    }

    public PlantillaBuilder withFichero(String fichero)
    {
        plantilla.setFichero(fichero);
        return this;
    }

    @Transactional
    public Plantilla build()
    {
        return plantillaDAO.insert(plantilla);
    }
}
