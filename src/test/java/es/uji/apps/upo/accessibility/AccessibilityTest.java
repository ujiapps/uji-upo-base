package es.uji.apps.upo.accessibility;

import java.io.FileInputStream;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.uji.commons.rest.StreamUtils;
import es.uji.commons.testing.junit.dao.StaticDAOCleaner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class AccessibilityTest
{
    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    private String heraURL;
    private AccesibleTemplate accesibleTemplate;
    private AccesibilityChecker checker;

    @Autowired
    public void setConfigValues(@Value("${uji.hera.url}") String heraURL, AccesibleTemplate accesibleTemplate)
    {
        this.heraURL = heraURL;
        this.accesibleTemplate = accesibleTemplate;
    }

    @Before
    public void init()
    {
        checker = new HeraAccesibilityChecker(heraURL, accesibleTemplate);
    }

    @Test
    public void shouldValidateSimpleContentWithoutEnvlopedPage()
    {
        Assert.assertTrue(checker.checkValidAAA("<h1>Contenido individual</h1>"
                .getBytes()));
    }

    @Test
    public void htmlPageAndImageWithoutAlternativeTextIsNotAccessibleLevelA() throws Exception
    {
        byte[] data = StreamUtils.inputStreamToByteArray(new FileInputStream(
                "src/test/resources/bad.html"));
        checker.checkValidAAA(data);

        Assert.assertEquals(1, checker.getNumberOfLevelAErrors());
        Assert.assertEquals(2, checker.getNumberOfLevelAAErrors());
        Assert.assertEquals(1, checker.getNumberOfLevelAAAErrors());
    }

    @Test
    public void elServicioDeValidacionDeHTML5DebeDeEstarAccesible() throws Exception
    {
        byte[] data = StreamUtils.inputStreamToByteArray(new FileInputStream(
                "src/test/resources/html5.html"));
        checker.checkValidAAA(data);

        Assert.assertEquals(1, checker.getNumberOfLevelAAErrors());
    }

    @Test
    public void correctHtmlIsAccesible() throws Exception
    {
        byte[] data = StreamUtils.inputStreamToByteArray(new FileInputStream(
                "src/test/resources/good.html"));
        checker.checkValidAAA(data);

        Assert.assertEquals(0, checker.getNumberOfLevelAErrors());
        Assert.assertEquals(0, checker.getNumberOfLevelAAErrors());
        Assert.assertEquals(0, checker.getNumberOfLevelAAAErrors());
    }
}
