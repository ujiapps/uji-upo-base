package es.uji.apps.upo.storage.alfresco;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import junit.framework.Assert;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
@Ignore
public class ThumbnailsAlfrescoTest
{
    private static final String ROOT_NODE = "/";
    private static final String EXAMPLE_FILE_NAME = "image_icon.png";
    private static final String EXAMPLE_FILE_MIMETYPE = "image/png";
    private static final int EXAMPLE_FILE_SIZE = 899;

    private AlfrescoStorageClient alfrescoClient;
    private AlfrescoThumbailService thumbnailsService;

    @Autowired
    private AlfrescoCredentials credentials;

    @Before
    public void init()
    {
        AlfrescoTicketService ticketService = new AlfrescoTicketService(credentials);

        thumbnailsService = new AlfrescoThumbailService(credentials, ticketService);
        alfrescoClient = new AlfrescoStorageClient(credentials, thumbnailsService,
                credentials.getWorkingPath());
    }

    @Test
    @Ignore
    public void losFicherosDeberianTenerRenditionsSiSeCreanATravesDelAPIREStDeAlfresco()
            throws Exception
    {
        addDocumentToAlfresco();

        String id = getIdItemInserted();
        CmisObject byId = alfrescoClient.getById(id);

        Assert.assertFalse(byId.getRenditions().isEmpty());
    }

    private String getIdItemInserted()
    {
        ItemIterable<QueryResult> results = alfrescoClient.searchByNameAndMimeType(
                EXAMPLE_FILE_NAME, ROOT_NODE, EXAMPLE_FILE_MIMETYPE, 0, 10);

        QueryResult item = results.iterator().next();

        String id = (String) item.getPropertyById("alfcmis:nodeRef").getFirstValue();
        return id;
    }

    private void addDocumentToAlfresco() throws FileNotFoundException
    {
        FileInputStream content = new FileInputStream("src/test/resources/imagen_icon.png");

        try
        {
            alfrescoClient.add(EXAMPLE_FILE_NAME, EXAMPLE_FILE_MIMETYPE, EXAMPLE_FILE_SIZE,
                    content, ROOT_NODE);
        }
        catch (Exception e)
        {
        }
    }

    @After
    public void deleteSampleFiles() throws InterruptedException
    {
        try
        {
            alfrescoClient.removeByName(ROOT_NODE, EXAMPLE_FILE_NAME);
        }
        catch (Exception e)
        {
        }
    }
}
