package es.uji.apps.upo.storage.alfresco.opensearch;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.abdera.Abdera;
import org.apache.abdera.model.Document;
import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.apache.abdera.parser.Parser;

import com.sun.jersey.core.util.Base64;
import org.junit.Ignore;

public class AlfrescoOpenSearchService
{
    private static final String SEARCH_URL = "/alfresco/service/api/search/keyword";

    private String openSearchURL;
    private final String username;
    private final String password;

    public AlfrescoOpenSearchService(String host, String username, String password)
    {
        this.username = username;
        this.password = password;

        openSearchURL = MessageFormat.format(
                "{0}/alfresco/service/api/search/keyword?l=es&format=atom", host, 
                SEARCH_URL);
    }

    public List<Resultado> search(String query, int start, int limit) throws IOException
    {
        URL url = buildQueryURL(query, start, limit);
        URLConnection urlConnection = url.openConnection();
        urlConnection.setRequestProperty("Authorization", getAuthHTTPHeader());

        Parser parser = Abdera.getInstance().getParser();
        Document<Feed> document = parser.parse(urlConnection.getInputStream());

        List<Resultado> resultados = new ArrayList<Resultado>();

        for (Entry entry : document.getRoot().getEntries())
        {
            resultados.add(new Resultado(entry));
        }

        return resultados;
    }

    private URL buildQueryURL(String query, int start, int limit) throws MalformedURLException,
            UnsupportedEncodingException
    {
        return new URL(MessageFormat.format("{0}&q={1}&p={2}&c={3}", openSearchURL,
                URLEncoder.encode(query, "UTF-8"), start, limit));
    }
    
    private String getAuthHTTPHeader()
    {
        String authString = username + ":" + password;
        return "Basic " + new String(Base64.encode(authString.getBytes()));
    }    
} 
