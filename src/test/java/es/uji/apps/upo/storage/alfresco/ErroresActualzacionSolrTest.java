package es.uji.apps.upo.storage.alfresco;

import static org.mockito.Mockito.mock;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.commons.exceptions.CmisRuntimeException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.uji.apps.upo.exceptions.StorageException;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class ErroresActualzacionSolrTest
{
    private static final String ROOT_NODE = "/";
    private static final String EXAMPLE_FILE_NAME = "imagen_icon.png";
    private static final int TIEMPO_ESPERA_ACTUALIZACION_INDICE_SOLR = 1500;

    private AlfrescoStorageClient alfrescoClient;
    private AlfrescoThumbailService thumbnailsService;

    @Autowired
    private AlfrescoCredentials credentials;

    @Before
    public void init()
    {
        thumbnailsService = mock(AlfrescoThumbailService.class);

        alfrescoClient = new AlfrescoStorageClient(credentials, thumbnailsService,
                credentials.getWorkingPath());
    }

    @Test(expected = CmisRuntimeException.class)
    public void busquedaEnSolrTrasBorradoNoFunciona() throws Exception
    {
        addFicheroEjemplo();
        esperaHastaQueFicheroActualizadoEnSolr();
        borraFichero();

        ItemIterable<QueryResult> result = alfrescoClient.searchByNameAndMimeType(
                EXAMPLE_FILE_NAME, ROOT_NODE, "image", 0, 10);
        Assert.assertEquals(0, result.getPageNumItems());
    }

    @Test(expected = CmisRuntimeException.class)
    public void busquedaEnSolrTrasInsercionNoFunciona() throws Exception
    {
        addFicheroEjemplo();

        ItemIterable<QueryResult> result = alfrescoClient.searchByNameAndMimeType(
                EXAMPLE_FILE_NAME, ROOT_NODE, "image", 0, 10);
        Assert.assertEquals(1, result.getPageNumItems());
    }

    private void borraFichero()
    {
        alfrescoClient.removeByName(ROOT_NODE, EXAMPLE_FILE_NAME);
    }

    private void addFicheroEjemplo() throws StorageException, FileNotFoundException
    {
        FileInputStream content = new FileInputStream("src/test/resources/" + EXAMPLE_FILE_NAME);
        alfrescoClient.add(EXAMPLE_FILE_NAME, "image/png", 922, content, ROOT_NODE);
    }

    private void esperaHastaQueFicheroActualizadoEnSolr() throws InterruptedException
    {
        ItemIterable<QueryResult> result = null;

        while (true)
        {
            result = alfrescoClient.searchByNameAndMimeType(EXAMPLE_FILE_NAME, ROOT_NODE, "image",
                    0, 10);

            if (result.getPageNumItems() > 0)
            {
                break;
            }

            Thread.sleep(TIEMPO_ESPERA_ACTUALIZACION_INDICE_SOLR);
        }
    }

    @After
    public void borraFicheroEjemplo() throws InterruptedException
    {
        Thread.sleep(TIEMPO_ESPERA_ACTUALIZACION_INDICE_SOLR);

        try
        {
            borraFichero();
        }
        catch (Exception e)
        {
        }
    }
}
