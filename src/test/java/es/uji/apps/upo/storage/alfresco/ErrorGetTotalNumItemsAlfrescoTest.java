package es.uji.apps.upo.storage.alfresco;

import static org.mockito.Mockito.mock;

import java.io.ByteArrayInputStream;

import junit.framework.Assert;

import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.uji.commons.testing.junit.dao.StaticDAOCleaner;

@Ignore // En la versión 4.2.d parece que ya funciona
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class ErrorGetTotalNumItemsAlfrescoTest
{
    private static final String ROOT_NODE = "/";
    private static final String EXAMPLE_FILE_NAME = "@@@fichero@@@1.txt";
    private static final String EXAMPLE_FILE_MIMETYPE = "text/plain";
    private static final String EXAMPLE_FILE_CONTENT = "Contenido del @@@fichero@@@";

    private AlfrescoStorageClient alfrescoClient;
    private AlfrescoThumbailService thumbnailsService;

    @Autowired
    private AlfrescoCredentials credentials;

    @Rule
    public StaticDAOCleaner staticDAOCleaner = new StaticDAOCleaner();

    @Before
    public void init()
    {
        thumbnailsService = mock(AlfrescoThumbailService.class);

        alfrescoClient = new AlfrescoStorageClient(credentials, thumbnailsService,
                credentials.getWorkingPath());
    }

    @Test
    public void numeroDeFicherosEncontradosDeberiaSerIncorrectoPorErrorEnVersionDeAlfresco()
            throws Exception
    {
        addDocument(EXAMPLE_FILE_NAME, EXAMPLE_FILE_MIMETYPE, EXAMPLE_FILE_CONTENT);

        ItemIterable<QueryResult> results = alfrescoClient.searchByNameAndMimeType("@@@fichero@@@",
                ROOT_NODE, EXAMPLE_FILE_MIMETYPE, 0, 10);

        Assert.assertEquals(-1, results.getTotalNumItems());
    }

    private void addDocument(String fileName, String mimeType, String fileContent)
    {
        try
        {
            alfrescoClient.add(fileName, mimeType, fileContent.length(), new ByteArrayInputStream(
                    fileContent.getBytes()), ROOT_NODE);
        }
        catch (Exception e)
        {
        }
    }

    @After
    public void deleteSampleFiles() throws InterruptedException
    {
        try
        {
            alfrescoClient.removeByName(ROOT_NODE, EXAMPLE_FILE_NAME);
        }
        catch (Exception e)
        {
        }
    }
}
