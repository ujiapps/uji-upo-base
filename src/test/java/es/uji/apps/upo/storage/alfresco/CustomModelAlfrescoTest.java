package es.uji.apps.upo.storage.alfresco;

import static org.mockito.Mockito.mock;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import junit.framework.Assert;

import org.apache.chemistry.opencmis.client.api.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
@Ignore
public class CustomModelAlfrescoTest
{
    public static final String ROOT_NODE = "/";
    private static final String EXAMPLE_FILE_NAME = "image_icon.png";
    private static final String EXAMPLE_FILE_MIMETYPE = "image/png";
    private static final int EXAMPLE_FILE_SIZE = 899;

    private AlfrescoStorageClient alfrescoClient;
    private AlfrescoThumbailService thumbnailsService;

    @Autowired
    private AlfrescoCredentials credentials;

    @Before
    public void init()
    {
        thumbnailsService = mock(AlfrescoThumbailService.class);

        alfrescoClient = new AlfrescoStorageClient(credentials, thumbnailsService,
                credentials.getWorkingPath());
    }

    @Test
    public void elDocumentoDeberiaTenerLasPropiedadesDefinidasEnElModelo() throws Exception
    {
        Document documento = addDocumentToAlfresco();

        Assert.assertEquals(ROOT_NODE, documento.getProperty("uji:urlNode").getFirstValue());
    }

    private Document addDocumentToAlfresco() throws FileNotFoundException
    {
        FileInputStream content = new FileInputStream("src/test/resources/imagen_icon.png");
        Document documentoInsertado = null;

        try
        {
            documentoInsertado = alfrescoClient.add(EXAMPLE_FILE_NAME, EXAMPLE_FILE_MIMETYPE,
                    EXAMPLE_FILE_SIZE, content, ROOT_NODE);
        }
        catch (Exception e)
        {
        }

        return documentoInsertado;
    }

    @After
    public void deleteSampleFiles() throws InterruptedException
    {
        try
        {
            alfrescoClient.removeByName(ROOT_NODE, EXAMPLE_FILE_NAME);
        }
        catch (Exception e)
        {
        }
    }
}
