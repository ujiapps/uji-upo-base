package es.uji.apps.upo.storage;

import es.uji.apps.upo.storage.metadata.ImageMetadataExtractor;
import org.junit.Test;

import java.io.FileInputStream;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

public class ImageMetadataExtractorTest
{
    private ImageMetadataExtractor imageMetadataExtractor;

    @Test
    public void losFicherosDeberianTenerRenditionsSiSeCreanATravesDelAPIREStDeAlfresco()
            throws Exception
    {
        imageMetadataExtractor = new ImageMetadataExtractor();
        FileInputStream content = new FileInputStream("src/test/resources/imagen_grande.jpg");

        List<RecursoMetadato> metadatos = imageMetadataExtractor.extract(content);

        assertThat(metadatos, hasSize(12));
        assertThat(containsStringInValue(metadatos, "1024"), is(equalTo(true)));
    }

    private Boolean containsStringInValue(List<RecursoMetadato> metadatos, String value)
    {
        for (RecursoMetadato metadato : metadatos)
        {
            if (metadato.getValue().contains(value))
            {
                return true;
            }
        }

        return false;
    }
}
