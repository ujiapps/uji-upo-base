package es.uji.apps.upo.storage.alfresco.opensearch;

import org.apache.abdera.model.Entry;

/*
   <entry>
     <title type="text">bin_asc_agendacs_2006_febrer.pdf</title>
     <link rel="alternate" href="http://documents.uji.es:8080/alfresco/service/api/node/content/workspace/SpacesStore/760642a0-c626-4c3c-9ab2-bf3bb2d80dbf/bin_asc_agendacs_2006_febrer.pdf" />
      <icon>http://documents.uji.es:8080/alfresco/images/filetypes/pdf.gif</icon>       
      <id>urn:uuid:760642a0-c626-4c3c-9ab2-bf3bb2d80dbf</id>
      <alf:noderef>workspace://SpacesStore/760642a0-c626-4c3c-9ab2-bf3bb2d80dbf</alf:noderef>
      <updated>2012-03-29T13:27:29.518+02:00</updated>
      <summary type="text" />
      <author> 
        <name>portal</name>
      </author>
      <relevance:score>0.055</relevance:score>
    </entry> 
 */

public class Resultado
{
    private String id;
    private String titulo;
    private String url;
    private String nodoAlfresco;

    public Resultado(Entry entry)
    {
        id = entry.getId().getUserInfo();
        titulo = entry.getTitle();
        url = entry.getLinks().get(0).getHref().toString();
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getNodoAlfresco()
    {
        return nodoAlfresco;
    }

    public void setNodoAlfresco(String nodoAlfresco)
    {
        this.nodoAlfresco = nodoAlfresco;
    }

    @Override
    public String toString()
    {
        StringBuffer value = new StringBuffer();
        value.append("Id     :").append(id).append("\n").
              append("Titulo :").append(titulo).append("\n").
              append("URL    :").append(url).append("\n").
              append("Nodo   :").append(nodoAlfresco).append("\n");
        
        return value.toString();
    }
}
