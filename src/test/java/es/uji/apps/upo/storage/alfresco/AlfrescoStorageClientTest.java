package es.uji.apps.upo.storage.alfresco;

import static org.mockito.Mockito.mock;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.util.UUID;

import org.alfresco.cmis.client.AlfrescoDocument;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.uji.apps.upo.exceptions.StorageException;
import es.uji.apps.upo.storage.Reservori;
import es.uji.commons.rest.StreamUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
@Ignore
public class AlfrescoStorageClientTest
{
    public static final String ROOT_NODE = "/test/";

    private static final String EXAMPLE_FILE_NAME = "@@@fichero@@@xxxx1.txt";
    private static final String EXAMPLE_FILE_NAME2 = "@@@fichero@@@xxxx2.txt";
    private static final String EXAMPLE_FILE_NAME3 = "@@@fichero@@@xxxx3.txt";
    private static final String EXAMPLE_FILE_NAME4 = "@@@fichero@@@xxxx4.txt";

    private static final String EXAMPLE_FILE_MIMETYPE = "text/plain";

    private AlfrescoStorageClient alfrescoClient;
    private AlfrescoThumbailService thumbnailsService;

    @Autowired
    private AlfrescoCredentials credentials;

    @Before
    public void init()
    {
        thumbnailsService = mock(AlfrescoThumbailService.class);

        alfrescoClient = new AlfrescoStorageClient(credentials, thumbnailsService,
                credentials.getWorkingPath());
    }

    @Test
    public void addedDocumentIsStored() throws Exception
    {
        addDocument(EXAMPLE_FILE_NAME, EXAMPLE_FILE_MIMETYPE);

        CmisObject object = alfrescoClient.getByName(ROOT_NODE, EXAMPLE_FILE_NAME);
        Assert.assertNotNull(object);
    }

    @Test(expected = CmisObjectNotFoundException.class)
    public void retrieveANonExistentObjectMustFail() throws Exception
    {
        alfrescoClient.getByName(ROOT_NODE, UUID.randomUUID().toString());
    }

    @Test(expected = CmisObjectNotFoundException.class)
    public void removeANonExistentObjectMustFail() throws Exception
    {
        alfrescoClient.removeByName(ROOT_NODE, UUID.randomUUID().toString());
    }

    @Test
    public void objectsShouldBeRetrievedByNameAndMimeType() throws Exception
    {
        addDocument(EXAMPLE_FILE_NAME, EXAMPLE_FILE_MIMETYPE);

        ItemIterable<QueryResult> results = alfrescoClient.searchByNameAndMimeType(
                EXAMPLE_FILE_NAME, ROOT_NODE, EXAMPLE_FILE_MIMETYPE, 0, 10);
        Assert.assertEquals(1, results.getPageNumItems());
    }

    @Test
    public void allObjectsShouldBeRetrievedByGeneralSearchAndMimeType() throws Exception
    {
        addDocument(EXAMPLE_FILE_NAME, EXAMPLE_FILE_MIMETYPE);
        addDocument(EXAMPLE_FILE_NAME2, EXAMPLE_FILE_MIMETYPE);

        ItemIterable<QueryResult> results = alfrescoClient.searchByNameAndMimeType("*", ROOT_NODE,
                EXAMPLE_FILE_MIMETYPE, 0, 10);
        Assert.assertTrue(2 <= results.getPageNumItems());
    }

    @Test
    public void allObjectsShouldBeRetrievedByGeneralSearchAndNotMimeType() throws Exception
    {
        String incorrectMimeType = "image";

        addDocument(EXAMPLE_FILE_NAME, EXAMPLE_FILE_MIMETYPE);

        ItemIterable<QueryResult> results = alfrescoClient.searchByNameAndNotMimeType("*",
                ROOT_NODE, incorrectMimeType, 0, 10);
        Assert.assertTrue(1 <= results.getPageNumItems());
    }

    @Test
    public void objectsShouldBeRetrievedByNotCaseSensitiveNameAndMimeType() throws Exception
    {
        addDocument(EXAMPLE_FILE_NAME, EXAMPLE_FILE_MIMETYPE);

        ItemIterable<QueryResult> results = alfrescoClient.searchByNameAndMimeType(
                EXAMPLE_FILE_NAME.toUpperCase(), ROOT_NODE, EXAMPLE_FILE_MIMETYPE, 0, 10);
        Assert.assertTrue(1 <= results.getPageNumItems());
    }

    @Test
    public void objectsShouldBeRetrievedByContentAndNotMimeType() throws Exception
    {
        String wordFileContent = "@@@fichero@@@";
        String incorrectMimeType = "image";

        addDocument(EXAMPLE_FILE_NAME, EXAMPLE_FILE_MIMETYPE);

        ItemIterable<QueryResult> results = alfrescoClient.searchByContentAndNotMimeType(
                wordFileContent, ROOT_NODE, incorrectMimeType, 0, 10);
        Assert.assertTrue(results.getPageNumItems() > 0);
    }

    @Test
    public void objectsShouldBeNotRetrievedIfItIsDeleted() throws Exception
    {
        addDocument(EXAMPLE_FILE_NAME, EXAMPLE_FILE_MIMETYPE);
        removeDocument(ROOT_NODE, EXAMPLE_FILE_NAME);

        ItemIterable<QueryResult> results = alfrescoClient.searchByNameAndMimeType(
                EXAMPLE_FILE_NAME, ROOT_NODE, EXAMPLE_FILE_MIMETYPE, 0, 10);
        Assert.assertEquals(0, results.getPageNumItems());
    }

    @Test
    public void objectsShouldBeRetrievedInPagedMode() throws Exception
    {
        addDocument(EXAMPLE_FILE_NAME, EXAMPLE_FILE_MIMETYPE);
        addDocument(EXAMPLE_FILE_NAME2, EXAMPLE_FILE_MIMETYPE);
        addDocument(EXAMPLE_FILE_NAME3, EXAMPLE_FILE_MIMETYPE);
        addDocument(EXAMPLE_FILE_NAME4, EXAMPLE_FILE_MIMETYPE);

        ItemIterable<QueryResult> results = alfrescoClient.searchByNameAndMimeType("@@@fichero@@@",
                ROOT_NODE, EXAMPLE_FILE_MIMETYPE, 2, 2);

        Assert.assertEquals(2, results.getPageNumItems());
    }

    @Test
    public void numeroDeFicherosPorPaginaDeberiaCoincidirConElMaximoSiNoHayPaginacion()
    {
        addDocument(EXAMPLE_FILE_NAME, EXAMPLE_FILE_MIMETYPE);
        addDocument(EXAMPLE_FILE_NAME2, EXAMPLE_FILE_MIMETYPE);
        addDocument(EXAMPLE_FILE_NAME3, EXAMPLE_FILE_MIMETYPE);
        addDocument(EXAMPLE_FILE_NAME4, EXAMPLE_FILE_MIMETYPE);

        Long numResults = alfrescoClient.getTotalNumItemsByNameAndMimeType("@@@fichero@@@",
                ROOT_NODE, EXAMPLE_FILE_MIMETYPE);

        Assert.assertTrue(4L == numResults);
    }

    @Test
    public void seDebePoderSustituirElContenidoDeUnFichero() throws IOException
    {
        addDocument(EXAMPLE_FILE_NAME, EXAMPLE_FILE_MIMETYPE);
        String nuevoContenido = "Nuevo contenido";
        String nuevoNombre = "pepe.pdf";
        String nuevoContentType = "application/pdf";

        replaceDocument(ROOT_NODE, EXAMPLE_FILE_NAME, nuevoNombre, nuevoContentType, nuevoContenido);

        Document document = (Document) alfrescoClient.getByName(ROOT_NODE, nuevoNombre);

        Assert.assertEquals(nuevoContenido, getContentAsString(document.getContentStream()));
        Assert.assertEquals(nuevoNombre, document.getPropertyValue("uji:name"));
        Assert.assertEquals(nuevoContentType, document.getContentStream().getMimeType());

        deleteFile(ROOT_NODE, nuevoNombre);
    }

    @Test
    public void seDebePoderSustituirElContenidoDeUnFicheroBinario() throws IOException,
            StorageException
    {
        String nombreImagenOriginal = "imagen_grande.jpg";
        String nombreImagenSustituta = "imagen_icon.png";

        byte[] file = StreamUtils.inputStreamToByteArray(new FileInputStream(
                "src/test/resources/" + nombreImagenOriginal));

        alfrescoClient.add(nombreImagenOriginal, "imagen/jpeg", file.length,
                new ByteArrayInputStream(file), ROOT_NODE);

        Document document = (Document) alfrescoClient.getByName(ROOT_NODE, nombreImagenOriginal);

        byte[] file2 = StreamUtils.inputStreamToByteArray(new FileInputStream(
                "src/test/resources/" + nombreImagenSustituta));

        alfrescoClient.replace(document, nombreImagenSustituta, "image/png", file2.length,
                new ByteArrayInputStream(file2), ROOT_NODE);

        document = (Document) alfrescoClient.getByName(ROOT_NODE, nombreImagenSustituta);

        Assert.assertEquals(file2.length, StreamUtils.inputStreamToByteArray(document.getContentStream().getStream()).length);

        deleteFile(ROOT_NODE, nombreImagenSustituta);
    }

    private static String getContentAsString(ContentStream stream) throws IOException
    {
        return new String(StreamUtils.inputStreamToByteArray(stream.getStream()));
    }

    private void addDocument(String fileName, String mimeType)
    {
        String fileContent = "Contenido del @@@fichero@@@";

        addDocument(fileName, mimeType, fileContent);
    }

    private void addDocument(String fileName, String mimeType, String fileContent)
    {
        try
        {
            alfrescoClient.add(fileName, mimeType, fileContent.length(), new ByteArrayInputStream(
                    fileContent.getBytes()), ROOT_NODE);
        }
        catch (Exception e)
        {
        }
    }

    private void replaceDocument(String urlNode, String fileName, String newFileName,
            String newContentType, String fileContent)
    {
        try
        {
            Document document = (Document) alfrescoClient.getByName(urlNode, fileName);

            alfrescoClient.replace(document, newFileName, newContentType, fileContent.length(),
                    new ByteArrayInputStream(fileContent.getBytes()), urlNode);
        }
        catch (Exception e)
        {
        }
    }

    private void removeDocument(String urlNode, String fileName)
    {
        alfrescoClient.removeByName(urlNode, fileName);
    }

    private void deleteFile(String urlNode, String fileName)
    {
        try
        {
            alfrescoClient.removeByName(urlNode, fileName);
        }
        catch (Exception e)
        {
        }
    }

    @After
    public void deleteSampleFiles() throws InterruptedException
    {
        deleteFile(ROOT_NODE, EXAMPLE_FILE_NAME);
        deleteFile(ROOT_NODE, EXAMPLE_FILE_NAME2);
        deleteFile(ROOT_NODE, EXAMPLE_FILE_NAME3);
        deleteFile(ROOT_NODE, EXAMPLE_FILE_NAME4);
    }
}
