package es.uji.apps.upo.storage.alfresco.opensearch;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import org.apache.abdera.parser.ParseException;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
@Ignore
public class OpenSearchServiceTest
{
    private String server;
    private String userName;
    private String password;

    @Autowired
    public void setConfigValues(@Value("${uji.alfresco.server}") String server,
            @Value("${uji.alfresco.username}") String userName,
            @Value("${uji.alfresco.password}") String password)
    {
        this.server = server;
        this.userName = userName;
        this.password = password;
    }

    @Test
    @Ignore
    public void test() throws ParseException, MalformedURLException, IOException
    {
        AlfrescoOpenSearchService search = new AlfrescoOpenSearchService(server, userName, password);

        List<Resultado> resultados = search.search("jpg", 1, 10);

        for (Resultado resultado : resultados)
        {
            System.out.println(resultado);
        }

    }
}