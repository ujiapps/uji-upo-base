package es.uji.apps.upo.search;

import es.uji.apps.upo.services.rest.publicacion.formato.search.*;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class FiltroTest
{
    @Test
    public void filtroNumerico() throws Exception
    {
        Filtro filtro = new FiltroNumerico("valor", 3);

        String sql = filtro.generate();

        assertThat(sql, is("(valor = 3)"));
    }

    @Test
    public void filtroMetadato() throws Exception
    {
        Filtro filtro = new FiltroMetadato("key", "País");

        String sql = filtro.generate();

        assertThat(sql, is("(clave = 'key' and valor like '%pais%')"));
    }

    @Test
    public void filtroClob() throws Exception
    {
        Filtro filtro = new FiltroClob("key", "País");

        String sql = filtro.generate();

        assertThat(sql, is("(dbms_lob.instr(key, utl_raw.cast_to_raw('País')) > 0)"));
    }

    @Test
    public void filtroFechasDefinidas() throws Exception
    {
        LocalDate fechaInicio = LocalDate.of(2015, Month.JANUARY, 1);
        LocalDate fechaFin = LocalDate.of(2015, Month.DECEMBER, 31);

        Filtro filtro = new FiltroFechasDefinidas("clave", fechaInicio, fechaFin);

        String sql = filtro.generate();

        assertThat(sql, is("(clave between to_date('01/01/2015', 'dd/mm/rrrr') and to_date('31/12/2015', 'dd/mm/rrrr'))"));
    }

    @Test
    public void filtroFechaMayorOIgualQue() throws Exception
    {
        LocalDate fechaInicio = LocalDate.of(2015, Month.JANUARY, 1);

        Filtro filtro = new FiltroFechaMayorOIgual("clave", fechaInicio);

        String sql = filtro.generate();

        assertThat(sql, is("(trunc(to_date('01/01/2015', 'dd/mm/rrrr')) <= trunc(clave))"));
    }

    @Test
    public void filtroFechaMenorOIgualQue() throws Exception
    {
        LocalDate fechaFin = LocalDate.of(2015, Month.DECEMBER, 31);

        Filtro filtro = new FiltroFechaMenorOIgual("clave", fechaFin);

        String sql = filtro.generate();

        assertThat(sql, is("(trunc(to_date('31/12/2015', 'dd/mm/rrrr')) >= trunc(clave))"));
    }

    @Test
    public void filtroIgualCadena() throws Exception
    {
        Filtro filtro = new FiltroIgualCadena("valor", "País");

        String sql = filtro.generate();

        assertThat(sql, is("(valor = 'País')"));
    }

    @Test
    public void filtroLikeCadena() throws Exception
    {
        Filtro filtro = new FiltroLikeCadena("valor", "País");

        String sql = filtro.generate();

        assertThat(sql, is("(valor like '%pais%')"));
    }

    @Test
    public void filtroInCadena() throws Exception
    {
        Filtro filtro = new FiltroInCadena("valor", "a1, a2,a3");

        String sql = filtro.generate();

        assertThat(sql, is("(valor in ('a1','a2','a3'))"));
    }

}
