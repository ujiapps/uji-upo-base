package es.uji.apps.upo.search;

import es.uji.apps.upo.services.rest.publicacion.formato.search.*;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ExpresionTest
{
    @Test
    public void orExpresions()
            throws Exception
    {
        Filtro filtroNumerico = new FiltroNumerico("valor", 3);
        Filtro filtroMetadato = new FiltroMetadato("key", "valor");

        Expression expression = new OrExpression();
        expression.add(filtroNumerico);
        expression.add(filtroMetadato);

        String sql = expression.generate();

        assertThat(sql, is("((valor = 3) or (clave = 'key' and valor like '%valor%'))"));
    }

    @Test
    public void andExpresionsOnlyFilters()
            throws Exception
    {
        Filtro filtroNumerico = new FiltroNumerico("valor", 3);
        Filtro filtroMetadato = new FiltroMetadato("key", "valor");

        Expression expression = new AndExpression();
        expression.add(filtroNumerico);
        expression.add(filtroMetadato);

        String sql = expression.generate();

        assertThat(sql, is("((valor = 3) and (clave = 'key' and valor like '%valor%'))"));
    }

    @Test
    public void andExpresionsCanContainOrExpressions()
            throws Exception
    {
        Filtro filtroNumerico = new FiltroNumerico("valor", 3);
        Filtro filtroMetadato = new FiltroMetadato("key", "valor");

        Expression orExpression = new OrExpression();
        orExpression.add(filtroNumerico);
        orExpression.add(filtroMetadato);

        Expression andExpression = new AndExpression();
        andExpression.add(orExpression);
        andExpression.add(orExpression);

        String sql = andExpression.generate();

        assertThat(sql, is("(((valor = 3) or (clave = 'key' and valor like '%valor%')) and ((valor = 3) or (clave = 'key' and valor like '%valor%')))"));
    }

}
