package es.uji.apps.upo.search;

import es.uji.apps.upo.services.rest.publicacion.formato.search.*;
import org.junit.Ignore;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class RevistaSearchTest
{
    @Test
    public void selectBasica()
            throws Exception
    {
        SearchBuilder search = new RevistaSearchBuilder();

        String sql = search.generate();

        assertThat(reemplazaEspacios(sql), is(reemplazaEspacios("select MAPA_URL_COMPLETA " +
                " from UPO_VW_REVISTA " +
                " where 1=1 " +
                " order by primera_fecha_vigencia desc")));
    }

    private String reemplazaEspacios(String s)
    {
        return s.replaceAll("\\s+", " ");
    }

    @Test
    public void selectConFechasYExpresion()
            throws Exception
    {
        SearchBuilder search = new RevistaSearchBuilder();

        Filtro filtroFechaMayor = new FiltroFechaMayorOIgual("primera_fecha_vigencia", LocalDate.of(2015, Month.JANUARY, 1));
        Filtro filtroFechaMenor = new FiltroFechaMenorOIgual("primera_fecha_vigencia", LocalDate.of(2015, Month.DECEMBER, 31));

        search.addExpresionCuerpo(filtroFechaMayor);
        search.addExpresionCuerpo(filtroFechaMenor);

        Filtro filtroLike1 = new FiltroLikeCadena("tags", "investigacio");
        Filtro filtroLike2 = new FiltroLikeCadena("tags", "cultura");

        Expression orExpression = new OrExpression();
        orExpression.add(filtroLike1);
        orExpression.add(filtroLike2);

        search.addExpresionCuerpo(orExpression);

        String sql = search.generate();

        assertThat(reemplazaEspacios(sql), is(reemplazaEspacios("select MAPA_URL_COMPLETA " +
                " from UPO_VW_REVISTA " +
                "where 1=1 " +
                " and ((trunc(to_date('01/01/2015', 'dd/mm/rrrr')) <= trunc(primera_fecha_vigencia))" +
                " and (trunc(to_date('31/12/2015', 'dd/mm/rrrr')) >= trunc(primera_fecha_vigencia))" +
                " and ((tags like '%investigacio%') or (tags like '%cultura%')))" +
                " order by primera_fecha_vigencia desc")));
    }
}
