package es.uji.apps.upo.search;

import es.uji.apps.upo.services.rest.publicacion.formato.search.*;
import es.uji.apps.upo.services.rest.publicacion.model.ConfigPublicacion;
import es.uji.apps.upo.services.rest.publicacion.model.RequestParams;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class NormalSearchTest
{
    private RequestParams requestParams;

    @Before
    public void init()
    {
        requestParams = new RequestParams("urlBase", "urlCompleta");

        ConfigPublicacion configPublicacion = new ConfigPublicacion();
        configPublicacion.setIdioma("ca");

        requestParams.setConfigPublicacion(configPublicacion);
    }

    @Test
    public void selectBasica()
            throws Exception
    {

        SearchBuilder search = new NormalSearchBuilder(requestParams);

        String sql = search.generate();

        assertThat(reemplazaEspacios(sql), is(reemplazaEspacios(
                "select MAPA_URL_COMPLETA " + " from UPO_VW_BUSQUEDA " + "where MAPA_URL_COMPLETA in (  select distinct mapa_url_completa " + "                                from ( select mapa_url_completa from  UPO_VW_BUSQUEDA_SIN_FILTRAR " + "                                        where (MAPA_URL_COMPLETA like 'urlCompleta%') " + "                                              and lower(IDIOMA_CODIGO_ISO)= 'ca' " + "                                              and (TITULO_LARGO is not null) " + "                                     ) " + "                           ) " + " and lower(IDIOMA_CODIGO_ISO)= 'ca' " + " order by prioridad asc, DISTANCIA_SYSDATE asc, orden asc, decode(es_html, 'S', fecha_creacion, to_date('1/1/1900')) desc, orden_mapa asc")));
    }

    @Test
    public void selectBasicaConMetadatos()
            throws Exception
    {
        SearchBuilder search = new NormalSearchBuilder(requestParams);
        Filtro filtroMetadatos = new FiltroMetadato("urltramitacion", "http");
        Filtro filtroMetadatos2 = new FiltroMetadato("materias", "otras");

        search.addExpresionMetadatos(filtroMetadatos);
        search.addExpresionMetadatos(filtroMetadatos2);

        String sql = search.generate();

        assertThat(reemplazaEspacios(sql), is(reemplazaEspacios(
                "select MAPA_URL_COMPLETA " + " from UPO_VW_BUSQUEDA " + "where MAPA_URL_COMPLETA in (  select distinct mapa_url_completa " + "                                from ( select mapa_url_completa from  UPO_VW_BUSQUEDA_SIN_FILTRAR " + "                                        where (MAPA_URL_COMPLETA like 'urlCompleta%') " + "                                              and lower(IDIOMA_CODIGO_ISO)= 'ca' " + "                                              and (TITULO_LARGO is not null) " + "                                     ) " + "                           ) " + " and lower(IDIOMA_CODIGO_ISO)= 'ca' " + " and exists (select * from upo_vw_metadatos " + "              where idioma_codigo_iso = idioma " + "                and objeto_id = contenido_id " + "                and (clave = 'urltramitacion' " + "                and valor like '%http%')" + "             )" + " and exists (select * from upo_vw_metadatos " + "              where idioma_codigo_iso = idioma " + "                and objeto_id = contenido_id " + "                and (clave = 'materias' " + "                and valor like '%otras%')" + "             )" + " order by prioridad asc, DISTANCIA_SYSDATE asc, orden asc, decode(es_html, 'S', fecha_creacion, to_date('1/1/1900')) desc, orden_mapa asc")));
    }

    @Test
    public void selectConFechasDeInicioYFin()
            throws Exception
    {
        SearchBuilder search = new NormalSearchBuilder(requestParams);

        Filtro filtroFechaMayor = new FiltroFechaMayorOIgual("fecha_vigencia", LocalDate.of(2015, Month.JANUARY, 1));
        Filtro filtroFechaMenor = new FiltroFechaMenorOIgual("fecha_vigencia", LocalDate.of(2015, Month.DECEMBER, 31));

        search.addExpresionCabecera(filtroFechaMayor);
        search.addExpresionCabecera(filtroFechaMenor);

        String sql = search.generate();

        assertThat(reemplazaEspacios(sql), is(reemplazaEspacios(
                "select MAPA_URL_COMPLETA " + " from UPO_VW_BUSQUEDA " + "where MAPA_URL_COMPLETA in (  select distinct mapa_url_completa " + "                                from ( select mapa_url_completa from  UPO_VW_BUSQUEDA_SIN_FILTRAR " + "                                        where (MAPA_URL_COMPLETA like 'urlCompleta%') " + "                                              and lower(IDIOMA_CODIGO_ISO)= 'ca' " + "                                              and (TITULO_LARGO is not null) " + "                                              and ((trunc(to_date('01/01/2015', 'dd/mm/rrrr')) <= trunc(fecha_vigencia)) " + "                                              and (trunc(to_date('31/12/2015', 'dd/mm/rrrr')) >= trunc(fecha_vigencia))) " + "                                     ) " + "                           ) " + " and lower(IDIOMA_CODIGO_ISO)= 'ca' " + " order by prioridad asc, DISTANCIA_SYSDATE asc, orden asc, decode(es_html, 'S', fecha_creacion, to_date('1/1/1900')) desc, orden_mapa asc")));
    }

    private String reemplazaEspacios(String s)
    {
        return s.replaceAll("\\s+", " ");
    }

    @Test
    public void selectConFechasYExpresion()
            throws Exception
    {
        SearchBuilder search = new NormalSearchBuilder(requestParams);

        Filtro filtroFechaMayor = new FiltroFechaMayorOIgual("fecha_vigencia", LocalDate.of(2015, Month.JANUARY, 1));
        Filtro filtroFechaMenor = new FiltroFechaMenorOIgual("fecha_vigencia", LocalDate.of(2015, Month.DECEMBER, 31));

        search.addExpresionCabecera(filtroFechaMayor);
        search.addExpresionCabecera(filtroFechaMenor);

        Filtro filtroNumerico = new FiltroNumerico("valor", 3);
        Filtro filtroMetadato = new FiltroMetadato("key", "valor");

        Expression orExpression = new OrExpression();
        orExpression.add(filtroNumerico);
        orExpression.add(filtroMetadato);

        search.addExpresionCuerpo(orExpression);
        search.addExpresionCuerpo(orExpression);

        String sql = search.generate();

        assertThat(reemplazaEspacios(sql), is(reemplazaEspacios(
                "select MAPA_URL_COMPLETA " + " from UPO_VW_BUSQUEDA " + "where MAPA_URL_COMPLETA in (  select distinct mapa_url_completa " + "                                from ( select mapa_url_completa from  UPO_VW_BUSQUEDA_SIN_FILTRAR " + "                                        where (MAPA_URL_COMPLETA like 'urlCompleta%') " + "                                              and lower(IDIOMA_CODIGO_ISO)= 'ca' " + "                                              and (TITULO_LARGO is not null) " + "                                              and ((trunc(to_date('01/01/2015', 'dd/mm/rrrr')) <= trunc(fecha_vigencia)) " + "                                              and (trunc(to_date('31/12/2015', 'dd/mm/rrrr')) >= trunc(fecha_vigencia))) " + "                                     ) " + "                           ) " + " and lower(IDIOMA_CODIGO_ISO)= 'ca' " + " and (((valor = 3) or (clave = 'key' and valor like '%valor%')) and ((valor = 3) or (clave = 'key' and valor like '%valor%')))" + " order by prioridad asc, DISTANCIA_SYSDATE asc, orden asc, decode(es_html, 'S', fecha_creacion, to_date('1/1/1900')) desc, orden_mapa asc")));
    }
}
