package es.uji.apps.upo.search;

import es.uji.apps.upo.exceptions.EstructuraDeLosParametrosException;
import es.uji.apps.upo.services.rest.publicacion.formato.search.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class SearchConverterTest
{
    private List<CondicionFiltro> condiciones;
    private CondicionFiltro condicion;
    private CondicionFiltro condicion2;
    private List<Expression> expressions;
    private SearchConverter searchConverter;
    private CondicionFactory factory;

    @Before
    public void init()
            throws EstructuraDeLosParametrosException
    {
        condiciones = new ArrayList<>();
        condicion = new CondicionFiltro();
        condicion2 = new CondicionFiltro();

        factory = new CondicionFactory();

        searchConverter = new SearchConverter(factory);
    }

    @Test
    public void convertNumericExpression()
            throws Exception
    {
        condicion.setNombre("nombre");
        condicion.setValor("11");
        condicion.setTipo(TipoCondicion.IGUAL_NUMERO);

        condiciones.add(condicion);

        expressions = searchConverter.convert(condiciones);

        assertThat(1, is(equalTo(expressions.size())));
        assertThat("(nombre = 11)", is(equalTo(expressions.get(0).generate())));
    }

    @Test
    public void convertCLOBExpression()
            throws Exception
    {
        condicion.setNombre("nombre");
        condicion.setValor("valor");
        condicion.setTipo(TipoCondicion.CLOB);

        condiciones.add(condicion);

        expressions = searchConverter.convert(condiciones);

        assertThat(1, is(equalTo(expressions.size())));
        assertThat("(dbms_lob.instr(nombre, utl_raw.cast_to_raw('valor')) > 0)",
                is(equalTo(expressions.get(0).generate())));
    }

    @Test
    public void convertMetadatoExpression()
            throws Exception
    {
        condicion.setNombre("nombre");
        condicion.setValor("valor");
        condicion.setTipo(TipoCondicion.METADATO);

        condiciones.add(condicion);

        expressions = searchConverter.convert(condiciones);

        assertThat(1, is(equalTo(expressions.size())));
        assertThat("(clave = 'nombre' and valor like '%valor%')", is(equalTo(expressions.get(0).generate())));
    }

    @Test
    public void convertStringExpressions()
            throws Exception
    {
        condicion.setNombre("nombre");
        condicion.setValor("valor");
        condicion.setTipo(TipoCondicion.IGUAL_CADENA);

        condicion2.setNombre("nombre");
        condicion2.setValor("valor");
        condicion2.setTipo(TipoCondicion.LIKE);

        condiciones.add(condicion);
        condiciones.add(condicion2);

        expressions = searchConverter.convert(condiciones);

        assertThat(2, is(equalTo(expressions.size())));
        assertThat("(nombre = 'valor')", is(equalTo(expressions.get(0).generate())));
        assertThat("(nombre like '%valor%')", is(equalTo(expressions.get(1).generate())));
    }

    @Test
    public void convertDateExpressions()
            throws Exception
    {
        condicion.setNombre("nombre");
        condicion.setValor("01/01/2015");
        condicion.setTipo(TipoCondicion.FECHA_MAYOR_IGUAL);

        condicion2.setNombre("nombre");
        condicion2.setValor("31/12/2015");
        condicion2.setTipo(TipoCondicion.FECHA_MENOR_IGUAL);

        condiciones.add(condicion);
        condiciones.add(condicion2);

        expressions = searchConverter.convert(condiciones);

        assertThat(2, is(equalTo(expressions.size())));
        assertThat("(trunc(to_date('01/01/2015', 'dd/mm/rrrr')) <= trunc(nombre))",
                is(equalTo(expressions.get(0).generate())));
        assertThat("(trunc(to_date('31/12/2015', 'dd/mm/rrrr')) >= trunc(nombre))",
                is(equalTo(expressions.get(1).generate())));
    }

    @Test
    public void convertGroupedExpressions()
            throws Exception
    {
        condicion.setNombre("nombre");
        condicion.setTipo(TipoCondicion.IGUAL_NUMERO);
        condicion.setValor("1");
        condicion.setAgrupacion("A");
        condiciones.add(condicion);

        condicion2.setNombre("nombre2");
        condicion2.setTipo(TipoCondicion.IGUAL_CADENA);
        condicion2.setValor("A");
        condicion2.setAgrupacion("A");
        condiciones.add(condicion2);

        CondicionFiltro condicion3 = new CondicionFiltro();
        condicion3.setNombre("nombre3");
        condicion3.setTipo(TipoCondicion.IGUAL_CADENA);
        condicion3.setValor("B");
        condicion3.setAgrupacion("B");
        condiciones.add(condicion3);

        expressions = searchConverter.convert(condiciones);

        assertThat(2, is(equalTo(expressions.size())));
        assertThat("((nombre = 1) or (nombre2 = 'A'))", is(equalTo(expressions.get(0).generate())));
        assertThat("((nombre3 = 'B'))", is(equalTo(expressions.get(1).generate())));
    }

    @Test
    public void convertGroupedAndNotGroupedExpressions()
            throws Exception
    {
        condicion.setNombre("nombre");
        condicion.setTipo(TipoCondicion.IGUAL_NUMERO);
        condicion.setValor("1");
        condicion.setAgrupacion("A");
        condiciones.add(condicion);

        condicion2.setNombre("nombre2");
        condicion2.setTipo(TipoCondicion.IGUAL_CADENA);
        condicion2.setValor("A");
        condicion2.setAgrupacion("A");
        condiciones.add(condicion2);

        CondicionFiltro condicion3 = new CondicionFiltro();
        condicion3.setNombre("nombre3");
        condicion3.setTipo(TipoCondicion.IGUAL_CADENA);
        condicion3.setValor("B");
        condiciones.add(condicion3);

        expressions = searchConverter.convert(condiciones);

        assertThat(2, is(equalTo(expressions.size())));
        assertThat("(nombre3 = 'B')", is(equalTo(expressions.get(0).generate())));
        assertThat("((nombre = 1) or (nombre2 = 'A'))", is(equalTo(expressions.get(1).generate())));
    }
}
