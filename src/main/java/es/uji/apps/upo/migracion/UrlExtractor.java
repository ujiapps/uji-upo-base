package es.uji.apps.upo.migracion;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import es.uji.apps.upo.exceptions.StorageException;
import es.uji.apps.upo.model.ItemMigracion;
import es.uji.apps.upo.model.enums.TipoItemMigracion;
import es.uji.apps.upo.services.ItemMigracionService;
import es.uji.commons.rest.StreamUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.upo.exceptions.ImportarDocumentoAlReservoriException;
import es.uji.apps.upo.storage.Reservori;

//@Component
public class UrlExtractor
{
    private static final String HREF_ATTRIBUTE = "href";
    private static final String SRC_ATTRIBUTE = "src";
    private static final String PATRON_ENLACES_TIPO_IMAGEN = "img[src]";
    private static final String PATRON_ENLACES_TIPO_FICHERO = "a[href~=\\.(doc|DOC|pdf|PDF)\\??]";
    public static final int TAMAÑO_PREFIJO_HTTP = 17;
    public static final int TAMAÑO_PREFIJO_HTTPS = 18;

    private static Reservori reservori;
    private static ItemMigracionService itemMigracionService;
    private Document document;

    @Autowired
    public void setReservori(Reservori reservori)
    {
        UrlExtractor.reservori = reservori;
    }

    @Autowired
    public void setItemMigracionService(ItemMigracionService itemMigracionService)
    {
        UrlExtractor.itemMigracionService = itemMigracionService;
    }

    public UrlExtractor()
    {
    }

    public UrlExtractor(String html)
    {
        this.document = Jsoup.parse(html);
    }

    public void setHtml(String html)
    {
        this.document = Jsoup.parse(html);
    }

    public ArrayList<String> extractAll()
    {
        ArrayList<String> allItems = new ArrayList<String>();
        allItems.addAll(extractImages());
        allItems.addAll(extractFiles());

        return allItems;
    }

    public ArrayList<String> extractImages()
    {
        return extract(PATRON_ENLACES_TIPO_IMAGEN, SRC_ATTRIBUTE);
    }

    public ArrayList<String> extractFiles()
    {
        return extract(PATRON_ENLACES_TIPO_FICHERO, HREF_ATTRIBUTE);
    }

    private ArrayList<String> extract(String pattern, String attribute)
    {
        Elements elementos = document.select(pattern);

        return convertElementsToArrayList(elementos, attribute);
    }

    private ArrayList<String> convertElementsToArrayList(Elements listado, String attribute)
    {
        ArrayList<String> arrayList = new ArrayList<String>();

        for (int i = 0; i < listado.size(); i++)
        {
            String attributeValue = listado.get(i).attr(attribute);
            if (!arrayList.contains(attributeValue))
            {
                arrayList.add(listado.get(i).attr(attribute));
            }
        }

        return arrayList;
    }

    public ArrayList<String> convertirURLenRecursoReservori(ArrayList<String> listadoUrl, String urlNode)
            throws IOException, ImportarDocumentoAlReservoriException
    {
        ArrayList<String> listadoUrlsNuevas = new ArrayList<String>();

        for (String url : listadoUrl)
        {
            listadoUrlsNuevas.add(convertirURLenRecursoReservori(url, urlNode));
        }
        return listadoUrlsNuevas;
    }

    public String convertirURLenRecursoReservori(String url, String urlNode)
            throws ImportarDocumentoAlReservoriException
    {
        if (url == null)
        {
            return null;
        }

        if (!esUrlUJI(url))
        {
            return url;
        }

        String urlSinPrefijo = url;
        String urlConPrefijo = "http://www.uji.es" + url;

        if (url.startsWith("http://"))
        {
            urlConPrefijo = url;
            urlSinPrefijo = url.substring(TAMAÑO_PREFIJO_HTTP);
        }

        if (url.startsWith("https://"))
        {
            urlConPrefijo = url;
            urlSinPrefijo = url.substring(TAMAÑO_PREFIJO_HTTPS);
        }

        FileSettings fileSettings;
        try
        {
            fileSettings = new FileSettings(urlConPrefijo, urlNode);
        } catch (Exception e) {
            throw new ImportarDocumentoAlReservoriException(
                    "Errada al importar un document al reservori, amb url: " + urlConPrefijo);
        }

        ItemMigracion item = itemMigracionService.getItems(urlSinPrefijo);

        if (item == null || esTexto(item))
        {
            return reservori.convierteURL(fileSettings);
        }

        return reservori.convierteContenido(fileSettings, new ByteArrayInputStream(item.getContenido()),
                item.getMimeType(), item.getContenido().length);
    }

    private boolean esUrlUJI(String url)
    {
        return url.startsWith("http://www.uji.es") || url.startsWith("https://www.uji.es") || url.startsWith("/");
    }

    private Boolean esTexto(ItemMigracion itemMigracion)
    {
        return TipoItemMigracion.TEXTO.toString().equalsIgnoreCase(itemMigracion.getTipo());
    }
}
