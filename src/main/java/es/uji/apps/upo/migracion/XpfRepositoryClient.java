package es.uji.apps.upo.migracion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

import es.uji.commons.rest.UIEntity;

//@Component
public class XpfRepositoryClient extends UpoClient
{
    private String authToken;
    private String urlConexionCliente;

    @Autowired
    public XpfRepositoryClient(@Value("${uji.deploy.authToken}") String authToken,
                               @Value("${uji.webapp.upo.url}") String urlConexionCliente)
    {
        this.authToken = authToken;
        this.urlConexionCliente = urlConexionCliente;
    }

    public List<UIEntity> getContenidosByURL(String url, String urlNode)
    {
        if (resource == null)
        {
            super.init(urlConexionCliente);
        }

        ClientResponse response = resource.path("migracion/url").queryParam("url", url)
                .queryParam("urlNode", urlNode).header("X-UJI-AuthToken", authToken)
                .get(ClientResponse.class);

        List<UIEntity> entityList = response.getEntity(new GenericType<List<UIEntity>>()
        {
        });

        return entityList;
    }
}