package es.uji.apps.upo.migracion;

import es.uji.apps.upo.utils.Hasher;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;

public class FileSettings
{
    private String url, urlNode, fileNameCompleto, fileName, hash;
    private URL urlParsed;

    public FileSettings(String url, String urlNode)
            throws MalformedURLException, NoSuchAlgorithmException
    {
        this.url = url;
        this.urlNode = urlNode;

        this.urlParsed = new URL(url);

        this.fileNameCompleto = urlParsed.getFile();
        this.fileName = urlParsed.getFile().substring(urlParsed.getFile().lastIndexOf("/") + 1);
        this.hash = Hasher.hash(fileNameCompleto.toString()) + "_";
    }

    public String getUrl()
    {
        return url;
    }

    public String getUrlNode()
    {
        return urlNode;
    }

    public String getFileNameCompleto()
    {
        return fileNameCompleto;
    }

    public String getFileName()
    {
        return fileName;
    }

    public String getHash()
    {
        return hash;
    }

    public URL getUrlParsed()
    {
        return urlParsed;
    }
}
