package es.uji.apps.upo.migracion;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.multipart.FormDataMultiPart;

import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.content.AccesibilityCleaner;

//@Component
public class ContentClient extends UpoClient
{
    public static Logger log = Logger.getLogger(ContentClient.class);

    private AccesibilityCleaner contentCleaner;
    private String authToken;
    private String urlConexionCliente;

    public ContentClient(@Value("${uji.deploy.authToken}") String authToken,
                         @Value("${uji.webapp.upo.url}") String urlConexionCliente)
    {
        contentCleaner = new AccesibilityCleaner();

        this.authToken = authToken;
        this.urlConexionCliente = urlConexionCliente;
    }

    public void registrarContenido(Long nodMapaId, UIEntity entity)
    {
        FormDataMultiPart multipart = convertInputParamsIntoMultiPart(nodMapaId, entity);

        if (resource == null)
        {
            super.init(urlConexionCliente);
        }

        ClientResponse response = resource.path("contenido").type(MediaType.MULTIPART_FORM_DATA)
                .header("X-UJI-AuthToken", authToken).post(ClientResponse.class, multipart);

        List<UIEntity> entityList = response.getEntity(new GenericType<List<UIEntity>>()
        {
        });

        if (response.getStatus() == 200)
        {
            UIEntity insertedEntity = entityList.get(0);
            log.debug("Registrado nuevo contenido para " + insertedEntity.get("urlPath"));
        }
        else
        {
            log.debug("No se ha podido migrar " + entity.get("urlPath"));
        }
    }

    private FormDataMultiPart convertInputParamsIntoMultiPart(Long nodMapaId, UIEntity entity)
    {
        FormDataMultiPart multipart = new FormDataMultiPart();
        addParametroToFormMultiPartData(multipart, "nodoMapaId", String.valueOf(nodMapaId));
        addParametroToFormMultiPartData(multipart, "urlPath", entity.get("urlPath"));
        addParametroToFormMultiPartData(multipart, "urlOriginal", entity.get("urlOriginal"));
        addParametroToFormMultiPartData(multipart, "tituloCA", entity.get("tituloCA"));
        addParametroToFormMultiPartData(multipart, "tituloES", entity.get("tituloES"));
        addParametroToFormMultiPartData(multipart, "tituloEN", entity.get("tituloUN"));
        addParametroToFormMultiPartData(multipart, "htmlCA", "S");
        addParametroToFormMultiPartData(multipart, "htmlES", "S");
        addParametroToFormMultiPartData(multipart, "htmlEN", "S");
        addContenidoToFormMultiPartData(multipart, "contenidoCA", entity.get("contenidoCA"));
        addContenidoToFormMultiPartData(multipart, "contenidoES", entity.get("contenidoES"));
        addContenidoToFormMultiPartData(multipart, "contenidoEN", entity.get("contenidoEN"));

        return multipart;
    }

    private void addParametroToFormMultiPartData(FormDataMultiPart multipart, String name,
            String value)
    {
        multipart.field(name, (value != null) ? value : "");
    }

    private void addContenidoToFormMultiPartData(FormDataMultiPart multipart, String name,
            String value)
    {
        String contenidoFinal = (value != null) ? value : "";

        try
        {
            contenidoFinal = contentCleaner.clean(contenidoFinal);
        }
        catch (Exception e)
        {
            log.error("No se ha podido limpiar el contenido: " + contenidoFinal, e);
        }

        multipart.field(name, contenidoFinal, MediaType.APPLICATION_OCTET_STREAM_TYPE);
    }
}
