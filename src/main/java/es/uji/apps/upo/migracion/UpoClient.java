package es.uji.apps.upo.migracion;

import org.springframework.stereotype.Component;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import es.uji.commons.rest.xml.UIEntityListMessageBodyReader;
import es.uji.commons.rest.xml.UIEntityMessageBodyReader;
import es.uji.commons.rest.xml.UIEntityMessageBodyWriter;

public class UpoClient
{
    protected WebResource resource;
    
    public UpoClient()
    {        
    }
    
    public void init(String urlConexionCliente)
    {
        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getClasses().add(UIEntityMessageBodyReader.class);
        clientConfig.getClasses().add(UIEntityListMessageBodyReader.class);
        clientConfig.getClasses().add(UIEntityMessageBodyWriter.class);

        Client client = Client.create(clientConfig);

        if (!urlConexionCliente.endsWith("/"))
        {
            urlConexionCliente += "/";
        }
        
        resource = client.resource(urlConexionCliente + "upo/rest/");
    }
}
