package es.uji.apps.upo.migracion;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

//@Component
public class TitleExtractor
{

    public TitleExtractor()
    {
    }

    public static String getTitle(String content)
    {
        String html = null;
        Document document = Jsoup.parse(content);

        try
        {
            Elements elementos = document.select("h2");
            html = elementos.first().text();
        }
        catch (Exception e)
        {
        }

        return html;
    }
}
