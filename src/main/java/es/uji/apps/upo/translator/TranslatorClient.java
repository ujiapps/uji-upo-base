package es.uji.apps.upo.translator;

import es.uji.apps.upo.exceptions.TraducirTextoException;

public interface TranslatorClient
{
    String translate(String text, String format, String sourceLang, String targetLang)
            throws TraducirTextoException;
}
