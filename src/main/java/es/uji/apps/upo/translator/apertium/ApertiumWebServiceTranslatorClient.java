package es.uji.apps.upo.translator.apertium;

import java.util.Arrays;
import java.util.Vector;

import org.apache.xmlrpc.XmlRpcClient;

import es.uji.apps.upo.exceptions.TraducirTextoException;
import es.uji.apps.upo.translator.TranslatorClient;

public class ApertiumWebServiceTranslatorClient implements TranslatorClient
{
    public static final String APERTIUM_API_KEY = "OGsRFS29UZRU26s809G+XUpo3G4";

    @Override
    public String translate(String text, String format, String sourceLang, String targetLang)
            throws TraducirTextoException
    {
        sourceLang = addValencianSufix(sourceLang);
        targetLang = addValencianSufix(targetLang);

        try
        {
            XmlRpcClient client = new XmlRpcClient("http://api.apertium.org/xmlrpc");

            Vector params = new Vector(Arrays.asList(new Object[] { text, format, sourceLang,
                    targetLang, APERTIUM_API_KEY }));

            return (String) client.execute("service.translate", params);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new TraducirTextoException();
        }
    }

    private String addValencianSufix(String sourceLang)
    {
        if ("ca".equals(sourceLang))
        {
            sourceLang += "_valencia";
        }

        return sourceLang;
    }
}
