package es.uji.apps.upo.translator.apertium;

import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;

import es.uji.apps.upo.exceptions.TraducirTextoException;
import es.uji.apps.upo.translator.TranslatorClient;

@Component
public class ApertiumWebFormTranslatorClient implements TranslatorClient
{
    @Override
    public String translate(String text, String format, String sourceLang, String targetLang)
            throws TraducirTextoException
    {
        String direccion = sourceLang + "-" + targetLang;

        direccion = (direccion.equals("es-ca")) ? "spa-cat_valencia_uni" : direccion;
        direccion = (direccion.equals("ca-es")) ? "cat-spa" : direccion;

        direccion = (direccion.equals("es-en")) ? "spa-eng" : direccion;
        direccion = (direccion.equals("en-es")) ? "eng-spa" : direccion;

        try
        {
            Client client = Client.create();
            WebResource resource = client.resource("https://apertium.ua.es/traddocs.php");

            FormDataMultiPart formDataMultiPart = new FormDataMultiPart();

            formDataMultiPart.field("MAX_FILE_SIZE1", "10000000");
            formDataMultiPart.field("MAX_FILE_SIZE2", "10000000");
            formDataMultiPart.field("tipo", format);
            formDataMultiPart.field("direccion", direccion);
            formDataMultiPart.field("marcar", "0");

            FormDataContentDisposition dispo = FormDataContentDisposition.name("userfile1")
                    .fileName("test.html").size(text.getBytes().length).build();

            formDataMultiPart.bodyPart(new FormDataBodyPart(dispo, text));

            return resource
                    .type(MediaType.MULTIPART_FORM_DATA)
                    .accept(MediaType.TEXT_HTML)
                    .header("Referer", "https://apertium.ua.es/docs.php")
                    .header("User-Agent",
                            "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36")
                    .post(String.class, formDataMultiPart);
        }
        catch (Exception e)
        {
            throw new TraducirTextoException();
        }
    }
}
