package es.uji.apps.upo.services;

import java.util.Comparator;
import java.util.List;

import es.uji.commons.sso.exceptions.UnauthorizedUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.upo.dao.GrupoDAO;
import es.uji.apps.upo.dao.ItemDAO;
import es.uji.apps.upo.dao.MenuDAO;
import es.uji.apps.upo.model.Grupo;
import es.uji.apps.upo.model.Item;
import es.uji.apps.upo.model.Menu;
import es.uji.apps.upo.model.MenuGrupo;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.exceptions.RegistroDuplicadoException;

@Service
public class MenuService
{
    private MenuDAO menuDAO;
    private GrupoDAO grupoDAO;
    private PersonaService personaService;

    @Autowired
    public MenuService(MenuDAO menuDAO, GrupoDAO grupoDAO, PersonaService personaService)
    {
        this.menuDAO = menuDAO;
        this.grupoDAO = grupoDAO;
        this.personaService = personaService;
    }

    public List<Menu> getMenusByUserId(Long connectedUserId)
    {
        if (personaService.isAdmin(connectedUserId))
        {
            return menuDAO.getAll();
        }

        return menuDAO.getByPersonaId(connectedUserId);
    }

    public List<Menu> getMenus()
    {
        return menuDAO.getAll();
    }

    public Menu getMenu(Long menuId)
    {
        List<Menu> listaMenus = menuDAO.get(Menu.class, menuId);

        Menu result = null;

        if (listaMenus != null && listaMenus.size() > 0)
        {
            result = listaMenus.get(0);
        }

        return result;
    }

    public Menu insert(Menu menu)
    {
        return menuDAO.insert(menu);
    }

    public Menu update(Menu menu, Long connectedUserId)
            throws UnauthorizedUserException
    {
        if (personaAutorizada(connectedUserId, getMenu(menu.getId())))
        {
            return menuDAO.update(menu);
        }

        throw new UnauthorizedUserException();
    }

    public void delete(Long id, Long connectedUserId)
            throws UnauthorizedUserException
    {
        if (personaAutorizada(connectedUserId, getMenu(id)))
        {
            menuDAO.delete(Menu.class, id);
            return;
        }

        throw new UnauthorizedUserException();
    }

    private Boolean personaAutorizada(Long connectedUserId, Menu menu)
    {
        return personaService.isAdmin(connectedUserId) || (menu.getPersona() != null && connectedUserId.equals(
                menu.getPersona().getId()));
    }

    private Boolean personaAutorizada(Long connectedUserId, Menu menu, Grupo grupo)
    {
        return personaAutorizada(connectedUserId, menu) && personaService.isAdmin(
                connectedUserId) || (grupo.getPersona() != null && connectedUserId.equals(grupo.getPersona().getId()));
    }


    public List<MenuGrupo> getGrupos(Long menuId, Long connectedUserId)
    {
        return menuDAO.getByMenu(menuId);
    }

    public void addGrupo(Long menuId, Long grupoId, Integer orden, Long connectedUserId)
            throws RegistroDuplicadoException, UnauthorizedUserException
    {
        if (personaAutorizada(connectedUserId, getMenu(menuId), grupoDAO.get(Grupo.class, grupoId).get(0)))
        {
            try
            {
                menuDAO.addGrupo(menuId, grupoId, orden);
                return;
            } catch (Exception e)
            {
                throw new RegistroDuplicadoException();
            }
        }

        throw new UnauthorizedUserException();
    }

    public void updateGrupo(long menuId, long grupoId, int orden, Long connectedUserId)
            throws UnauthorizedUserException
    {
        if (personaAutorizada(connectedUserId, getMenu(menuId), grupoDAO.get(Grupo.class, grupoId).get(0)))
        {
            menuDAO.updateGrupo(menuId, grupoId, orden);
            return;
        }

        throw new UnauthorizedUserException();
    }

    public void deleteGrupo(Long menuId, Long grupoId, Long connectedUserId)
            throws UnauthorizedUserException
    {

        if (personaAutorizada(connectedUserId, getMenu(menuId), grupoDAO.get(Grupo.class, grupoId).get(0)))
        {
            menuDAO.deleteGrupo(menuId, grupoId);
            return;
        }

        throw new UnauthorizedUserException();
    }
}