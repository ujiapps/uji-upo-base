package es.uji.apps.upo.services.rest.metadatos;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.upo.model.metadatos.AtributoMetadato;
import es.uji.apps.upo.model.metadatos.EsquemaMetadato;
import es.uji.apps.upo.model.metadatos.ValorMetadato;
import es.uji.apps.upo.services.AtributoMetadatoService;
import es.uji.apps.upo.services.EsquemaMetadatoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("tipo-metadato")
public class TipoResource extends CoreBaseService
{
    @InjectParam
    EsquemaMetadatoService esquemaMetadatoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTipos()
    {
        return UIEntity.toUI(esquemaMetadatoService.getTiposMetadatos());
    }
}
