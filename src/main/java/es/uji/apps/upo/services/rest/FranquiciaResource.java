package es.uji.apps.upo.services.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.upo.model.Franquicia;
import es.uji.apps.upo.model.FranquiciaAcceso;
import es.uji.apps.upo.model.OrigenInformacion;
import es.uji.apps.upo.model.enums.TipoFranquiciaAcceso;
import es.uji.apps.upo.services.FranquiciaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroDuplicadoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Path("franquicia")
public class FranquiciaResource extends CoreBaseService
{
    @InjectParam
    FranquiciaService franquiciaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getFranquicias()
    {
        return UIEntity.toUI(franquiciaService.getFranquicias());
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> insert(UIEntity franquicia) throws UnauthorizedUserException
    {
        Long personaId = AccessManager.getConnectedUserId(request);

        if (franquicia == null || franquicia.get("nombre") == null)
        {
            return new ArrayList<UIEntity>();
        }

        Franquicia newFranquicia = franquicia.toModel(Franquicia.class);

        String origenInformacionId = franquicia.get("origenInformacionId");

        if (origenInformacionId != null && !origenInformacionId.isEmpty())
        {
            OrigenInformacion origen = new OrigenInformacion();
            origen.setId(ParamUtils.parseLong(origenInformacionId));

            newFranquicia.setOrigenInformacion(origen);
        }

        return Collections.singletonList(UIEntity.toUI(franquiciaService.insert(newFranquicia,
                personaId)));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> update(@PathParam("id") String id, UIEntity uiEntity)
            throws UnauthorizedUserException

    {
        Franquicia franquicia = uiEntity.toModel(Franquicia.class);
        Long personaId = AccessManager.getConnectedUserId(request);

        String origenInformacionId = uiEntity.get("origenInformacionId");

        if (origenInformacionId != null && !origenInformacionId.isEmpty())
        {
            OrigenInformacion origen = new OrigenInformacion();
            origen.setId(ParamUtils.parseLong(origenInformacionId));

            franquicia.setOrigenInformacion(origen);
        }
        else
        {
            franquicia.setOrigenInformacion(null);
        }

        franquiciaService.update(franquicia, personaId);

        return Collections.singletonList(uiEntity);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") String id) throws RegistroConHijosException,
            UnauthorizedUserException
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        franquiciaService.delete(Long.parseLong(id), personaId);
    }

    @GET
    @Path("{id}/administrador")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAdministradores(@PathParam("id") String franquiciaId)
    {
        List<UIEntity> listaAdministradores = new ArrayList<UIEntity>();

        List<FranquiciaAcceso> administradores = franquiciaService.getAdministradores(Long
                .parseLong(franquiciaId));

        for (FranquiciaAcceso franquiciaAcceso : administradores)
        {
            UIEntity uiEntity = new UIEntity();
            uiEntity.put("franquiciaId", franquiciaAcceso.getUpoFranquicia().getId());
            uiEntity.put("id", franquiciaAcceso.getUpoExtPersona().getId());

            if (franquiciaAcceso.getTipo() != null)
            {
                uiEntity.put("tipo", franquiciaAcceso.getTipo().toString());
            }

            listaAdministradores.add(uiEntity);
        }

        return listaAdministradores;
    }

    @POST
    @Path("{id}/administrador")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> insertAdministrador(@PathParam("id") String franquiciaId,
            UIEntity uiEntity) throws RegistroDuplicadoException, UnauthorizedUserException
    {
        String personaId = uiEntity.get("id");
        String tipoPermiso = uiEntity.get("tipo");

        if (personaId == null || franquiciaId == null || tipoPermiso == null)
        {
            return new ArrayList<UIEntity>();
        }

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        franquiciaService.addAdministrador(Long.parseLong(franquiciaId), Long.parseLong(personaId),
                TipoFranquiciaAcceso.valueOf(tipoPermiso.toUpperCase()), connectedUserId);

        return Collections.singletonList(uiEntity);
    }

    @PUT
    @Path("{id}/administrador/{perId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> updateAdministrador(@PathParam("id") String franquiciaId,
            @PathParam("perId") String personaId, UIEntity uiEntity)
    {
        String tipoPermiso = uiEntity.get("tipo");

        if (personaId == null || franquiciaId == null || tipoPermiso == null)
        {
            return new ArrayList<UIEntity>();
        }

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        franquiciaService.updateAdministrador(Long.parseLong(franquiciaId),
                Long.parseLong(personaId), TipoFranquiciaAcceso.valueOf(tipoPermiso.toUpperCase()),
                connectedUserId);

        return Collections.singletonList(uiEntity);
    }

    @DELETE
    @Path("{id}/administrador/{administradorId}")
    public void deleteAdministrador(@PathParam("id") String id,
            @PathParam("administradorId") String administradorId) throws UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        franquiciaService.deleteAdministrador(Long.parseLong(id), Long.parseLong(administradorId),
                connectedUserId);
    }

    @POST
    @Path("{franquiciaDestinoId}/franquicia/{franquiciaOrigenId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage insertDesdeOtraFranquicia(
            @PathParam("franquiciaDestinoId") String franquiciaDestinoId,
            @PathParam("franquiciaOrigenId") String franquiciaOrigenId)
    {
        ParamUtils.checkNotNull(franquiciaDestinoId, franquiciaOrigenId);

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        franquiciaService.addAdministradoresFromOtherFranquicia(
                ParamUtils.parseLong(franquiciaDestinoId),
                ParamUtils.parseLong(franquiciaOrigenId), connectedUserId);

        return new ResponseMessage(true);
    }
}