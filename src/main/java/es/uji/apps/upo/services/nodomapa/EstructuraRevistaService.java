package es.uji.apps.upo.services.nodomapa;

import es.uji.apps.upo.dao.NodoMapaDAO;
import es.uji.apps.upo.exceptions.InsertadoContenidoNoAutorizadoException;
import es.uji.apps.upo.exceptions.InsertarEstructuraDeRevistaEnNodoMapaException;
import es.uji.apps.upo.exceptions.NodoConNombreYaExistenteEnMismoPadreException;
import es.uji.apps.upo.exceptions.UsuarioNoAutenticadoException;
import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.model.NodoMapaPlantilla;
import es.uji.apps.upo.model.Persona;
import es.uji.apps.upo.services.*;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EstructuraRevistaService extends AuthenticatedService
{
    NodoMapaService nodoMapaService;
    PlantillaService plantillaService;
    NodoMapaDAO nodoMapaDAO;
    private PersonaService personaService;

    @Autowired
    public EstructuraRevistaService(NodoMapaService nodoMapaService, PlantillaService plantillaService,
            NodoMapaDAO nodoMapaDAO, PersonaService personaService)
    {
        super(personaService);

        this.nodoMapaService = nodoMapaService;
        this.plantillaService = plantillaService;
        this.nodoMapaDAO = nodoMapaDAO;
        this.personaService = personaService;
    }

    public void insertaConEstructuraDeRevista(Persona persona, NodoMapa nodoMapa, String dia)
            throws UsuarioNoAutenticadoException, NodoConNombreYaExistenteEnMismoPadreException,
            InsertadoContenidoNoAutorizadoException, InsertarEstructuraDeRevistaEnNodoMapaException
    {
        ParamUtils.checkNotNull(persona);

        if (!personaAdminEnNodo(nodoMapa, persona))
        {
            throw new InsertadoContenidoNoAutorizadoException();
        }

        if (dia != null && !dia.isEmpty() && Integer.parseInt(dia) >= 1 && Integer.parseInt(dia) <= 31)
        {
            String diaFormateado = (dia.length() == 2) ? dia : '0' + dia;

            nodoMapaService.comprobarQueNoHayaNodosHijoConElMimoNombre(nodoMapa, diaFormateado);
            insertaConEstructuraDeRevista(nodoMapa, diaFormateado);
        }
        else
        {
            throw new InsertarEstructuraDeRevistaEnNodoMapaException(
                    "El dia no puede estar vacío y debe estar comprendido entre 1 y 31");
        }
    }

    @Transactional
    private void insertaConEstructuraDeRevista(NodoMapa parentNodoMapa, String dia)
    {
        NodoMapaPlantilla nodoMapaPlantilla = new NodoMapaPlantilla();
        nodoMapaPlantilla.setNivel(3);

        String fecha = dia + "/" + parentNodoMapa.getUrlPath() + "/" + parentNodoMapa.getUpoMapa().getUrlPath();

        nodoMapaPlantilla.setUpoPlantilla(plantillaService.getByFileName("2021/page-indice-revista"));

        NodoMapa nodoMapaParentDia = buildNewNodoMapaForEstructuraRevista(dia, parentNodoMapa, nodoMapaPlantilla,
                "Revista d'actualitat " + fecha, "Revista de actualidad " + fecha,
                "News magazine " + parentNodoMapa.getUrlPath() + "/" + dia + "/" + parentNodoMapa.getUpoMapa()
                        .getUrlPath());

        nodoMapaPlantilla.setUpoPlantilla(plantillaService.getByFileName("2021/page-pdf"));

        buildNewNodoMapaForEstructuraRevista("noticiesuji", nodoMapaParentDia, nodoMapaPlantilla,
                "Notícies de la Universitat Jaume I", "Noticias de la Universitat Jaume I", "Universitat Jaume I news");

        buildNewNodoMapaForEstructuraRevista("politicauniversitaria", nodoMapaParentDia, nodoMapaPlantilla,
                "Política universitària", "Política universitaria", "University policy");

        buildNewNodoMapaForEstructuraRevista("noticiesaltresuniversitats", nodoMapaParentDia, nodoMapaPlantilla,
                "Notícies d'altres universitats", "Noticias de otras universidades", "News from other universities");

        buildNewNodoMapaForEstructuraRevista("altresnoticies", nodoMapaParentDia, nodoMapaPlantilla,
                "Altres notícies d'interés", "Otras noticies de interés", "Other news of interest");
    }

    public NodoMapa buildNewNodoMapaForEstructuraRevista(String urlPath, NodoMapa parentNodoMapa,
            NodoMapaPlantilla nodoMapaPlantillaOriginal, String tituloPublicacionCA, String tituloPublicacionES,
            String tituloPublicacionEN)
    {
        NodoMapa newNodoMapa = new NodoMapa();
        newNodoMapa.setNodoMapaDAO(nodoMapaDAO);

        newNodoMapa.setUrlPath(urlPath);
        newNodoMapa.setUrlCompleta(parentNodoMapa.getUrlCompleta() + urlPath + '/');

        newNodoMapa.setCommonInheritedfields(parentNodoMapa, newNodoMapa, nodoMapaPlantillaOriginal);

        newNodoMapa.setTituloPublicacionCA(tituloPublicacionCA);
        newNodoMapa.setTituloPublicacionES(tituloPublicacionES);
        newNodoMapa.setTituloPublicacionEN(tituloPublicacionEN);

        return nodoMapaDAO.insert(newNodoMapa);
    }
}
