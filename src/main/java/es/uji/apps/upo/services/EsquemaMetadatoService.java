package es.uji.apps.upo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.upo.model.metadatos.EsquemaMetadato;
import es.uji.apps.upo.model.metadatos.TipoMetadato;
import es.uji.apps.upo.services.rest.metadatos.TipoResource;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.rest.Role;

@Service
public class EsquemaMetadatoService
{
    private BaseDAO baseDAO;

    @Autowired
    public EsquemaMetadatoService(BaseDAO baseDAO)
    {
        this.baseDAO = baseDAO;
    }

    public List<EsquemaMetadato> getAll()
    {
        return baseDAO.get(EsquemaMetadato.class);
    }

    @Transactional
    @Role("ADMIN")
    public EsquemaMetadato insert(EsquemaMetadato esquemaMetadato, Long connectedUserId)
    {
        return baseDAO.insert(esquemaMetadato);
    }

    @Transactional
    @Role("ADMIN")
    public EsquemaMetadato update(EsquemaMetadato esquemaMetadato, Long connectedUserId)
    {
        return baseDAO.update(esquemaMetadato);
    }

    @Transactional
    @Role("ADMIN")
    public void delete(Long id, Long connectedUserId)
    {
        baseDAO.delete(EsquemaMetadato.class, id);
    }

    public List<TipoMetadato> getTiposMetadatos()
    {
        return baseDAO.get(TipoMetadato.class);
    }

    public TipoMetadato getTipoMetadatos(Long tipoId)
    {
        return baseDAO.get(TipoMetadato.class, tipoId).get(0);
    }

    public EsquemaMetadato getEsquemaMetadato(Long esquemaId)
    {
        return baseDAO.get(EsquemaMetadato.class, esquemaId).get(0);
    }
}
