package es.uji.apps.upo.services.rest.publicacion.model;

import es.uji.apps.upo.model.NodoMapa;

public class RequestParams
{
    private String url;
    private String urlBase;
    private ConfigPublicacion configPublicacion;
    private RequestDetails requestDetails;
    private SearchParams searchParams;
    private NodoMapa nodoMapa;

    public RequestParams(String urlBase, String url)
    {
        this.urlBase = urlBase;
        this.url = url;
        this.configPublicacion = new ConfigPublicacion();
        this.requestDetails = new RequestDetails();
        this.searchParams = new SearchParams();
    }

    public String getUrl()
    {
        return url;
    }

    public String getUrlBase()
    {
        return urlBase;
    }

    public ConfigPublicacion getConfigPublicacion()
    {
        return configPublicacion;
    }

    public void setConfigPublicacion(ConfigPublicacion configPublicacion)
    {
        this.configPublicacion = configPublicacion;
    }

    public RequestDetails getRequestDetails()
    {
        return requestDetails;
    }

    public void setRequestDetails(RequestDetails requestDetails)
    {
        this.requestDetails = requestDetails;
    }

    public SearchParams getSearchParams()
    {
        return searchParams;
    }

    public void setSearchParams(SearchParams searchParams)
    {
        this.searchParams = searchParams;
    }

    public NodoMapa getNodoMapa()
    {
        return nodoMapa;
    }

    public void setNodoMapa(NodoMapa nodoMapa)
    {
        this.nodoMapa = nodoMapa;
    }
}
