package es.uji.apps.upo.services.rest.publicacion;

import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;

@Component
public class UrlNormalizer
{
    public String normalize(String url) throws MalformedURLException
    {
        if (url != null && !url.trim().endsWith("/") && !url.contains("."))
        {
            return url + "/";
        }

        return url;
    }

    public String getUrlBase(HttpServletRequest clientRequest, String sufixCookieName) throws MalformedURLException
    {
        String forwardedHeader = clientRequest.getHeader("X_FORWARDED_PROTOCOL");
        String protocol = (forwardedHeader != null) ? forwardedHeader : "http";
        String port = (clientRequest.getLocalPort() != 80) ? ":"+clientRequest.getLocalPort() : "";
        String queryString = (clientRequest.getQueryString() == null) ? "" : clientRequest.getQueryString();

        String url = normalize(clientRequest.getRequestURL().toString());
        URL normalizedUrl = new URL(url);

        return MessageFormat.format("{0}://{1}.uji.es{2}{3}?{4}", protocol, sufixCookieName,
                port, normalizedUrl.getPath(), queryString);
    }

    public String getUrlBase(HttpServletRequest clientRequest, String sufixCookieName, String path) throws MalformedURLException
    {
        String forwardedHeader = clientRequest.getHeader("X_FORWARDED_PROTOCOL");
        String protocol = (forwardedHeader != null) ? forwardedHeader : "http";

        return MessageFormat.format("{0}://{1}.uji.es{2}", protocol, sufixCookieName, path);
    }
}

