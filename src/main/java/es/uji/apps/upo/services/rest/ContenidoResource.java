package es.uji.apps.upo.services.rest;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import es.uji.apps.upo.exceptions.*;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.IdiomaPublicacion;
import es.uji.apps.upo.model.enums.TipoFormatoContenido;
import es.uji.apps.upo.model.enums.TipoReferenciaContenido;
import es.uji.apps.upo.model.metadatos.ContenidoMetadato;
import es.uji.apps.upo.services.*;
import es.uji.apps.upo.services.nodomapa.NodoMapaService;
import es.uji.apps.upo.services.rest.metadatos.ContenidoMetadatosResource;
import es.uji.commons.rest.*;
import es.uji.commons.rest.exceptions.ParametrosObligatoriosException;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.sso.AccessManager;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

@Path("contenido")
public class ContenidoResource extends CoreBaseService
{
    @Context
    private ServletContext servletContext;
    @InjectParam
    private ContenidoIdiomaService contenidoIdiomaService;
    @InjectParam
    private ContenidoIdiomaRecursoService contenidoIdiomaRecursoService;
    @InjectParam
    private ContenidoIdiomaAtributoService contenidoIdiomaAtributoService;
    @InjectParam
    private IdiomaService idiomaService;
    @InjectParam
    private ContenidoService contenidoService;
    @InjectParam
    private NodoMapaService nodoMapaService;
    @InjectParam
    private NodoMapaContenidoService nodoMapaContenidoService;
    @InjectParam
    private ContenidoMetadatoService contenidoMetadatoService;

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getContenido(@PathParam("id") Long objetoId,
            @QueryParam("nodoMapaContenidoId") String nodoMapaContenidoId)
    {
        Contenido contenido = contenidoService.getContenido(objetoId);

        UIEntity uiEntity = UIEntity.toUI(contenido);

        addAtributosNoPrimitivosToUI(contenido, ParamUtils.parseLong(nodoMapaContenidoId), uiEntity);

        uiEntity.put("urlCompletaNodoMapa", contenido.getUrlCompletaNodoMapa());

        return Collections.singletonList(uiEntity);
    }

    @GET
    @Path("{id}/raw/")
    public Response getContenidoRaw(@PathParam("id") Long objetoId, @QueryParam("idioma") String idioma)
            throws URISyntaxException
    {
        Contenido contenido = contenidoService.getContenido(objetoId);
        ContenidoIdioma contenidoIdioma = contenido.getContenidoIdioma(idioma);

        String idDescarga = contenidoIdioma.getIdDescarga();

        if (idDescarga != null)
        {
            return Response.temporaryRedirect(new URI(contenidoService.getDocumentsServer() + idDescarga)).build();
        }

        return Response.status(200)
                .header("Content-Type", contenidoIdioma.getMimeType())
                .header("Content-Disposition", "inline; filename=\"" + contenidoIdioma.getNombreFichero() + "\"")
                .entity(contenidoIdioma.getContenido())
                .build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getContenidos(@QueryParam("mapaId") Long mapaId)
    {
        List<Contenido> listaContenidos = new ArrayList<Contenido>();

        if ((mapaId == null || mapaId.equals("")))
        {
            throw new IllegalArgumentException();
        }
        else
        {
            listaContenidos = contenidoService.getContenidosByMapaId(mapaId);
        }

        List<UIEntity> uiListaObjetos = new ArrayList<UIEntity>();

        NodoMapaContenido nodoMapaContenido = null;

        for (Contenido contenido : listaContenidos)
        {
            UIEntity uiEntity = UIEntity.toUI(contenido);
            addContenidosIdiomasToUI(contenido, uiEntity);

            uiListaObjetos.add(uiEntity);

            nodoMapaContenido =
                    nodoMapaContenidoService.getNodoMapaContenidoByContenidoIdAndMapaId(contenido.getId(), mapaId);

            uiEntity.put("estadoModeracion", nodoMapaContenido.getEstadoModeracion());
            uiEntity.put("tipoContenido", nodoMapaContenido.getTipo());
            uiEntity.put("nodoMapaContenidoId", nodoMapaContenido.getId());
            uiEntity.put("nodoMapaId", nodoMapaContenido.getUpoMapa().getId());

            if (nodoMapaContenido.getPersonaPropuesta() != null)
            {
                uiEntity.put("personaPropuestaId", nodoMapaContenido.getPersonaPropuesta().getId());
            }

            setNodoMapaDataOfNodoMapaContenidoNormalIfThisTypeLink(nodoMapaContenido, contenido, uiEntity);
        }

        return uiListaObjetos;
    }

    private void setNodoMapaDataOfNodoMapaContenidoNormalIfThisTypeLink(NodoMapaContenido nodoMapaContenido,
            Contenido contenido, UIEntity uiEntity)
    {
        if (nodoMapaContenido.getTipo().equals(TipoReferenciaContenido.LINK))
        {
            NodoMapa nodoMapaDelNodoMapaContenidoNormal =
                    nodoMapaService.getNodoMapaOfNodoMapaContenidoNormalByContenidoId(contenido.getId());

            uiEntity.put("urlNodoMapaContenidoNormal", nodoMapaDelNodoMapaContenidoNormal.getUrlCompleta());
        }
    }

    private void addContenidosIdiomasToUI(Contenido contenido, UIEntity uiEntity)
    {
        for (ContenidoIdioma contenidoIdioma : contenido.getContenidoIdiomas())
        {
            String idiomaISO = contenidoIdioma.getUpoIdioma().getCodigoISO().toUpperCase();
            uiEntity.put("titulo" + idiomaISO, contenidoIdioma.getTitulo());
            uiEntity.put("mimeType" + idiomaISO, contenidoIdioma.getMimeType());
            uiEntity.put("html" + idiomaISO, contenidoIdioma.getHtml());
            uiEntity.put("longitud" + idiomaISO, contenidoIdioma.getLongitud());
            uiEntity.put("tipoFormato" + idiomaISO, contenidoIdioma.getTipoFormato());

            addContenidoIdiomaRecursosToUI(uiEntity, contenidoIdioma, idiomaISO);
        }
    }

    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") String contenidoId, UIEntity entity)
            throws BorradoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        Long personaId = AccessManager.getConnectedUserId(request);

        Contenido contenido = contenidoService.getContenido(Long.parseLong(contenidoId));
        contenidoService.deleteByPersonaAndNodoMapa(contenido, personaId, Long.parseLong(entity.get("nodoMapaId")));
    }

    @POST
    @Path("texto-accesible")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResponseMessage getContenidoEsAccesible(@FormParam("contenidoES") String contenidoES,
            @FormParam("contenidoEN") String contenidoEN, @FormParam("contenidoCA") String contenidoCA)
    {
        Contenido contenido = new Contenido();

        if (contenido.isAccesible(contenidoES) && contenido.isAccesible(contenidoEN) && contenido.isAccesible(
                contenidoCA))
        {
            return new ResponseMessage(true);
        }

        return new ResponseMessage(false);
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> insertObjeto(FormDataMultiPart params)
            throws IOException, ParametrosObligatoriosException, ActualizadoContenidoException,
            InsertadoContenidoException, LongitudMaximaExcedidaEnMetadatosException
    {
        Long personaId = AccessManager.getConnectedUserId(request);

        String nombreFicheroCA = null;
        String nombreFicheroES = null;
        String nombreFicheroEN = null;

        String id = getValueOf(params, "id");
        String nodoMapaId = getValueOf(params, "nodoMapaId");
        String urlPath = getValueOf(params, "urlPath");
        String origenInformacion = getValueOf(params, "origenInformacion");
        String urlOriginal = getValueOf(params, "urlOriginal");
        String latitud = getValueOf(params, "latitud");
        String longitud = getValueOf(params, "longitud");
        String orden = getValueOf(params, "orden");
        String lugar = getValueOf(params, "lugar");
        String otrosAutores = getValueOf(params, "otrosAutores");
        String vigencia = getValueOf(params, "vigencia");
        String ajustarAFechasVigencia = getValueOf(params, "ajustarAFechasVigencia");
        String fechasEvento = getValueOf(params, "fechasEvento");
        String prioridad = getValueOf(params, "prioridad");
        String tags = getValueOf(params, "tags");
        String fechaCreacion = getValueOf(params, "fechaCreacion");
        String visible = getValueOf(params, "visible");
        String textoNoVisible = getValueOf(params, "textoNoVisible");

        visible = (visible == null) ? "S" : visible;
        Boolean ajustarAFechasVigenciaBoolean = ajustarAFechasVigencia == null ? false : true;

        String tituloCA = escapaCaracteres(getValueOf(params, "tituloCA"));
        String subtituloCA = escapaCaracteres(getValueOf(params, "subtituloCA"));
        String tituloLargoCA = escapaCaracteres(getValueOf(params, "tituloLargoCA"));
        String resumenCA = escapaCaracteres(getValueOf(params, "resumenCA"));
        String tipoFormatoCA = getValueOf(params, "tipoFormatoCA");
        String contenidoCA = getValueOf(params, "contenidoCA");
        String enlaceDestinoCA = getValueOf(params, "enlaceDestinoCA");
        String repetirFicheroCA = getValueOf(params, "repetirFicheroCA");
        byte[] dataBinaryCA = getByteArrayOf(params, "dataBinaryCA");
        FormDataContentDisposition dataBinaryCADetail = getFormDataContentDispositionOf(params, "dataBinaryCA");

        String tituloES = escapaCaracteres(getValueOf(params, "tituloES"));
        String subtituloES = escapaCaracteres(getValueOf(params, "subtituloES"));
        String tituloLargoES = escapaCaracteres(getValueOf(params, "tituloLargoES"));
        String resumenES = escapaCaracteres(getValueOf(params, "resumenES"));
        String tipoFormatoES = getValueOf(params, "tipoFormatoES");
        String contenidoES = getValueOf(params, "contenidoES");
        String enlaceDestinoES = getValueOf(params, "enlaceDestinoES");
        String repetirFicheroES = getValueOf(params, "repetirFicheroES");
        byte[] dataBinaryES = getByteArrayOf(params, "dataBinaryES");
        FormDataContentDisposition dataBinaryESDetail = getFormDataContentDispositionOf(params, "dataBinaryES");

        String tituloEN = escapaCaracteres(getValueOf(params, "tituloEN"));
        String subtituloEN = escapaCaracteres(getValueOf(params, "subtituloEN"));
        String tituloLargoEN = escapaCaracteres(getValueOf(params, "tituloLargoEN"));
        String resumenEN = escapaCaracteres(getValueOf(params, "resumenEN"));
        String tipoFormatoEN = getValueOf(params, "tipoFormatoEN");
        String contenidoEN = getValueOf(params, "contenidoEN");
        String enlaceDestinoEN = getValueOf(params, "enlaceDestinoEN");
        String repetirFicheroEN = getValueOf(params, "repetirFicheroEN");
        byte[] dataBinaryEN = getByteArrayOf(params, "dataBinaryEN");
        FormDataContentDisposition dataBinaryENDetail = getFormDataContentDispositionOf(params, "dataBinaryEN");

        Map<String, String> atributosCA = getAtributos(params, IdiomaPublicacion.CA.toString());
        Map<String, String> atributosES = getAtributos(params, IdiomaPublicacion.ES.toString());
        Map<String, String> atributosEN = getAtributos(params, IdiomaPublicacion.EN.toString());

        Boolean repetirFicheroBooleanCA = repetirFicheroCA == null ? false : true;
        Boolean repetirFicheroBooleanES = repetirFicheroES == null ? false : true;
        Boolean repetirFicheroBooleanEN = repetirFicheroEN == null ? false : true;

        if (dataBinaryCADetail != null)
        {
            nombreFicheroCA = dataBinaryCADetail.getFileName();
        }

        if (dataBinaryESDetail != null)
        {
            nombreFicheroES = dataBinaryESDetail.getFileName();
        }

        if (dataBinaryENDetail != null)
        {
            nombreFicheroEN = dataBinaryENDetail.getFileName();
        }

        String tipoMimeFicheroCA = servletContext.getMimeType(nombreFicheroCA);
        String tipoMimeFicheroES = servletContext.getMimeType(nombreFicheroES);
        String tipoMimeFicheroEN = servletContext.getMimeType(nombreFicheroEN);

        enlaceDestinoCA = (enlaceDestinoCA != null) ? enlaceDestinoCA.trim() : null;
        enlaceDestinoES = (enlaceDestinoES != null) ? enlaceDestinoES.trim() : null;
        enlaceDestinoEN = (enlaceDestinoEN != null) ? enlaceDestinoEN.trim() : null;

        Contenido contenido;

        tipoFormatoCA =
                (tipoFormatoCA == null || tipoFormatoCA.isEmpty()) ? TipoFormatoContenido.PAGINA.toString() : tipoFormatoCA;
        tipoFormatoES =
                (tipoFormatoES == null || tipoFormatoES.isEmpty()) ? TipoFormatoContenido.PAGINA.toString() : tipoFormatoES;
        tipoFormatoEN =
                (tipoFormatoEN == null || tipoFormatoEN.isEmpty()) ? TipoFormatoContenido.PAGINA.toString() : tipoFormatoEN;

        String htmlCA = (TipoFormatoContenido.PAGINA.toString().equals(tipoFormatoCA)) ? "S" : "N";
        String htmlES = (TipoFormatoContenido.PAGINA.toString().equals(tipoFormatoES)) ? "S" : "N";
        String htmlEN = (TipoFormatoContenido.PAGINA.toString().equals(tipoFormatoEN)) ? "S" : "N";

        if (id != null && !id.isEmpty() && !id.equals("-1"))
        {
            NodoMapaContenido nodoMapaContenido =
                    nodoMapaContenidoService.getNodoMapaContenidoByContenidoIdAndMapaId(Long.parseLong(id),
                            Long.parseLong(nodoMapaId));

            if (checkRequiredParameters(urlPath) && nodoMapaContenido.isNormal())
            {
                throw new ParametrosObligatoriosException("urlPath");
            }

            contenido = contenidoService.updateContenido(id, nodoMapaId, origenInformacion, urlPath, personaId, orden,
                    latitud, longitud, lugar, otrosAutores, tituloCA, subtituloCA, tituloLargoCA, resumenCA, htmlCA,
                    contenidoCA, dataBinaryCA, enlaceDestinoCA, tituloES, subtituloES, tituloLargoES, resumenES, htmlES,
                    contenidoES, dataBinaryES, enlaceDestinoES, tituloEN, subtituloEN, tituloLargoEN, resumenEN, htmlEN,
                    contenidoEN, dataBinaryEN, enlaceDestinoEN, vigencia, ajustarAFechasVigenciaBoolean, fechasEvento,
                    prioridad, tags, nombreFicheroCA, nombreFicheroES, nombreFicheroEN, tipoMimeFicheroCA,
                    tipoMimeFicheroES, tipoMimeFicheroEN, repetirFicheroBooleanCA, repetirFicheroBooleanES,
                    repetirFicheroBooleanEN, fechaCreacion, tipoFormatoCA, tipoFormatoES, tipoFormatoEN, atributosCA,
                    atributosES, atributosEN, visible, textoNoVisible, buildSetMetadatos(params));
        }
        else
        {
            if (checkRequiredParameters(urlPath))
            {
                throw new ParametrosObligatoriosException("urlPath");
            }

            contenido = contenidoService.insertContenido(nodoMapaId, origenInformacion, urlPath, urlOriginal, personaId,
                    orden, latitud, longitud, lugar, otrosAutores, tituloCA, subtituloCA, tituloLargoCA, resumenCA,
                    htmlCA, contenidoCA, dataBinaryCA, enlaceDestinoCA, tituloES, subtituloES, tituloLargoES, resumenES,
                    htmlES, contenidoES, dataBinaryES, enlaceDestinoES, tituloEN, subtituloEN, tituloLargoEN, resumenEN,
                    htmlEN, contenidoEN, dataBinaryEN, enlaceDestinoEN, vigencia, ajustarAFechasVigenciaBoolean,
                    fechasEvento, prioridad, tags, nombreFicheroCA, nombreFicheroES, nombreFicheroEN, tipoMimeFicheroCA,
                    tipoMimeFicheroES, tipoMimeFicheroEN, repetirFicheroBooleanCA, repetirFicheroBooleanES,
                    repetirFicheroBooleanEN, fechaCreacion, tipoFormatoCA, tipoFormatoES, tipoFormatoEN, atributosCA,
                    atributosES, atributosEN, visible, textoNoVisible, buildSetMetadatos(params));
        }

        UIEntity uiEntity = UIEntity.toUI(contenido);

        addAtributosNoPrimitivosToUI(contenido, uiEntity);
        return Collections.singletonList(uiEntity);
    }

    private Set<ContenidoMetadato> buildSetMetadatos(FormDataMultiPart params)
            throws LongitudMaximaExcedidaEnMetadatosException
    {
        Map<String, List<String>> metadatos = getMetadatos(params);
        Set<ContenidoMetadato> contenidoMetadatos = new HashSet<ContenidoMetadato>();

        for (Map.Entry<String, List<String>> metadato : metadatos.entrySet())
        {
            if (metadato.getKey().endsWith("CA"))
            {
                contenidoMetadatos.add(contenidoMetadatoService.buildMetadatos(metadatos, null, metadato));
            }
        }

        return contenidoMetadatos;
    }

    private String escapaCaracteres(String value)
    {
        if (value == null)
        {
            return null;
        }

        return value.replaceAll("&(?!amp;)", "&amp;");
    }

    private String getValueOf(FormDataMultiPart params, String paramName)
    {
        FormDataBodyPart param = params.getField(paramName);

        if (param != null)
        {
            return param.getValue();
        }

        return null;
    }

    private byte[] getByteArrayOf(FormDataMultiPart params, String paramName)
            throws IOException
    {
        FormDataBodyPart param = params.getField(paramName);

        if (param != null)
        {
            return StreamUtils.inputStreamToByteArray(param.getValueAs(InputStream.class));
        }

        return null;
    }

    private FormDataContentDisposition getFormDataContentDispositionOf(FormDataMultiPart params, String paramName)
    {
        FormDataBodyPart param = params.getField(paramName);

        if (param != null)
        {
            return param.getFormDataContentDisposition();
        }

        return null;
    }

    private Map<String, List<String>> getMetadatos(FormDataMultiPart params)
    {
        Map<String, List<FormDataBodyPart>> metadatos = new HashMap<>();

        for (Map.Entry<String, List<FormDataBodyPart>> paramMap : params.getFields().entrySet())
        {
            String key = paramMap.getKey();

            if (key.startsWith("$"))
            {
                metadatos.put(paramMap.getKey().substring(1), paramMap.getValue());
            }
        }

        return listFormDataBodyPartToListString(metadatos);
    }

    private Map<String, List<String>> listFormDataBodyPartToListString(Map<String, List<FormDataBodyPart>> bodyParts)
    {
        Map<String, List<String>> mapStrings = new HashMap<>();

        for (Map.Entry<String, List<FormDataBodyPart>> paramMap : bodyParts.entrySet())
        {
            mapStrings.put(paramMap.getKey(), formDataBodyPartToString(paramMap.getValue()));
        }

        return mapStrings;
    }

    private List<String> formDataBodyPartToString(List<FormDataBodyPart> bodyParts)
    {
        List<String> strings = new ArrayList<>();

        for (FormDataBodyPart bodyPart : bodyParts)
        {
            strings.add(bodyPart.getValue());
        }

        return strings;
    }

    private Map<String, String> getAtributos(FormDataMultiPart params, String idioma)
    {
        Map<String, String> atributos = new HashMap<>();
        Map<String, List<FormDataBodyPart>> paramsMap = params.getFields();

        for (Map.Entry<String, List<FormDataBodyPart>> paramMap : paramsMap.entrySet())
        {
            String key = paramMap.getKey();

            if (key.startsWith("_") && key.endsWith(idioma))
            {
                atributos.put(paramMap.getKey(), getValueOf(params, paramMap.getKey()));
            }
        }

        return atributos;
    }

    private boolean checkRequiredParameters(String urlPath)
    {
        return urlPath == null || urlPath.isEmpty();
    }

    @PUT
    @Path("{id}/visibilidad/")
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> updateVisibilidad(@PathParam("id") String contenidoId, @FormParam("visible") String visible,
            @FormParam("textoNoVisible") String textoNoVisible)
            throws IdiomaObligatorioException, AccesibilidadException, ActualizadoContenidoNoAutorizadoException,
            UsuarioNoAutenticadoException, ContenidoConMismoUrlPathException
    {
        if (contenidoId == null || visible == null)
        {
            return new ArrayList<UIEntity>();
        }

        Contenido contenido = contenidoService.getContenido(Long.parseLong(contenidoId));
        contenido.setVisible(visible);
        contenido.setTextoNoVisible(textoNoVisible);

        Persona persona = new Persona();
        persona.setId(AccessManager.getConnectedUserId(request));

        contenidoService.update(contenido, persona, contenido.getNodoMapa());

        return Collections.singletonList(UIEntity.toUI(contenido));
    }

    @PUT
    @Path("visibilidad/")
    @Produces(MediaType.TEXT_XML)
    public void updateVisibilidadMultiple(@FormParam("contenidoId") List<String> contenidoIdList,
            @FormParam("visible") String visible, @FormParam("textoNoVisible") String textoNoVisible,
            @FormParam("mapaId") String mapaId)
            throws IdiomaObligatorioException, AccesibilidadException, ActualizadoContenidoNoAutorizadoException,
            UsuarioNoAutenticadoException, ContenidoConMismoUrlPathException
    {
        if (contenidoIdList == null || visible == null)
        {
            return;
        }

        Persona persona = new Persona();
        persona.setId(AccessManager.getConnectedUserId(request));

        contenidoService.updateVisibility(persona, contenidoIdList, visible, textoNoVisible, mapaId);
    }

    @DELETE
    @Path("{id}/adjunto/{idioma}")
    public void deleteAdjuntoIdioma(@PathParam("id") String contenidoId, @PathParam("idioma") String idioma)
            throws UsuarioNoAutenticadoException, ActualizadoContenidoNoAutorizadoException
    {
        Persona persona = getPersona();

        Contenido contenido = contenidoService.getContenido(Long.parseLong(contenidoId));

        ContenidoIdioma contenidoIdioma = contenido.getContenidoIdioma(idioma);
        contenidoIdiomaService.borraAdjunto(persona, contenidoIdioma);
    }

    private void addAtributosNoPrimitivosToUI(Contenido contenido, UIEntity uiEntity)
    {
        addAtributosNoPrimitivosToUI(contenido, null, uiEntity);
    }

    private void addAtributosNoPrimitivosToUI(Contenido contenido, Long nodoMapaContenidoId, UIEntity uiEntity)
    {
        Boolean contenidoSustituidoPorAutoguardado = false;
        Long personaId = AccessManager.getConnectedUserId(request);

        if (contenido.getUpoExtPersona1() != null)
        {
            uiEntity.put("personaResponsable", contenido.getUpoExtPersona1().getNombreCompleto());
        }

        if (contenido.getOrigenInformacion() != null)
        {
            uiEntity.put("origenInformacion", contenido.getOrigenInformacion().getId());
        }

        for (Idioma idioma : idiomaService.getIdiomas())
        {
            String idiomaCodigoISO = idioma.getCodigoISO();
            ContenidoIdioma contenidoIdioma = contenido.getContenidoIdioma(idiomaCodigoISO);

            Boolean hayContenidoAutoguardado =
                    setMultilangFieldsAngCheckContenidoSustituidoPorAutoguardado(uiEntity, contenidoIdioma,
                            idiomaCodigoISO, contenido.getId());

            contenidoSustituidoPorAutoguardado = contenidoSustituidoPorAutoguardado || hayContenidoAutoguardado;

            addContenidoIdiomaRecursosToUI(uiEntity, contenidoIdioma, idiomaCodigoISO);
            addContenidoIdiomaAtributosToUI(uiEntity, contenidoIdioma);
        }

        uiEntity.put("contenidoSustituidoPorAutoguardado", contenidoSustituidoPorAutoguardado);

        List<ContenidoMetadato> contenidoMetadatos =
                contenidoMetadatoService.getMetadatosByContenido(contenido.getId());

        uiEntity.put("contenidoMetadatos", UIEntity.toUI(contenidoMetadatos));
        uiEntity.put("esquemaId", contenidoMetadatoService.getEsquemaIdByMetadatos(contenidoMetadatos));

        List<String> personasEditando =
                contenidoService.getOtrasPersonasEditandoContenido(contenido.getId(), personaId);
        convertPersonasEditandoToString(personasEditando, uiEntity);

        convertVigenciasSetToString(contenido, uiEntity);
        convertTagsSetToString(contenido, uiEntity);

        if (nodoMapaContenidoId != null)
        {
            NodoMapaContenido nodoMapaContenido = nodoMapaContenidoService.getNodoMapaContenidoAndFechasEventos(nodoMapaContenidoId);

            uiEntity.put("orden", (nodoMapaContenido.getOrden() == null) ? "" : nodoMapaContenido.getOrden());
            convertFechasEventoSetToString(nodoMapaContenido, uiEntity);
        }

        uiEntity.put("validarIdiomas", contenidoService.validarIdiomas(contenido, contenido.getNodoMapa()) ? "S" : "N");
    }

    private void convertPersonasEditandoToString(List<String> personasEditando, UIEntity uiEntity)
    {
        String personas = StringUtils.join(personasEditando, " ; ");
        uiEntity.put("otrasPersonasEditandoContenido", personas);
    }

    private void convertVigenciasSetToString(Contenido contenido, UIEntity uiEntity)
    {
        if (contenido.getUpoVigenciasObjetos() != null)
        {
            List<VigenciaContenido> vigenciaContenidoAux =
                    new ArrayList<VigenciaContenido>(contenido.getUpoVigenciasObjetos());
            Collections.sort(vigenciaContenidoAux);

            String vigencia = StringUtils.join(vigenciaContenidoAux.toArray(), ",");

            uiEntity.put("vigencia", vigencia);
        }
    }

    private void convertFechasEventoSetToString(NodoMapaContenido nodoMapaContenido, UIEntity uiEntity)
    {
        if (nodoMapaContenido.getFechasEvento() != null)
        {
            List<FechaEvento> fechasEventoAux = new ArrayList<>(nodoMapaContenido.getFechasEvento());

            Collections.sort(fechasEventoAux);

            String fechasEvento = StringUtils.join(fechasEventoAux.toArray(), ",");

            uiEntity.put("fechasEvento", fechasEvento);
        }
    }

    public void convertTagsSetToString(Contenido contenido, UIEntity uiEntity)
    {
        String tags = StringUtils.join(contenido.getUpoContenidosTags().toArray(), ",");
        uiEntity.put("tags", tags);
    }

    private void addContenidoIdiomaRecursosToUI(UIEntity uiEntity, ContenidoIdioma contenidoIdioma, String idioma)
    {
        if (contenidoIdioma != null)
        {
            List<ContenidoIdiomaRecurso> contenidoIdiomaRecursos =
                    contenidoIdiomaRecursoService.getContenidoIdiomaRecursosByContenidoIdioma(contenidoIdioma);

            for (ContenidoIdiomaRecurso recurso : contenidoIdiomaRecursos)
            {
                UIEntity entity = new UIEntity();

                entity.put("mimeType", recurso.getContentType());
                entity.put("nombre", ContenidoIdiomaRecurso.getNombreRecurso(recurso.getUrl()));
                entity.put("url", recurso.getUrl());
                entity.put("idioma", idioma.toUpperCase());

                uiEntity.put("contenidoIdiomaRecursos" + idioma.toUpperCase(), entity);
            }
        }
    }

    private void addContenidoIdiomaAtributosToUI(UIEntity uiEntity, ContenidoIdioma contenidoIdioma)
    {
        if (contenidoIdioma != null)
        {
            List<ContenidoIdiomaAtributo> contenidoIdiomaAtributos =
                    contenidoIdiomaAtributoService.getContenidoIdiomaAtributosByContenidoIdioma(contenidoIdioma);

            for (ContenidoIdiomaAtributo atributo : contenidoIdiomaAtributos)
            {
                uiEntity.put(atributo.getClave(), atributo.getValor());
            }
        }
    }

    private Boolean setMultilangFieldsAngCheckContenidoSustituidoPorAutoguardado(UIEntity uiEntity,
            ContenidoIdioma contenidoIdioma, String idioma, Long contenidoId)
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        Boolean contenidoSustituidoPorAutoguardado = false;

        if (contenidoIdioma != null)
        {
            uiEntity.put("titulo" + idioma.toUpperCase(), contenidoIdioma.getTitulo());
            uiEntity.put("subtitulo" + idioma.toUpperCase(), contenidoIdioma.getSubtitulo());
            uiEntity.put("tituloLargo" + idioma.toUpperCase(), contenidoIdioma.getTituloLargo());
            uiEntity.put("enlaceDestino" + idioma.toUpperCase(), contenidoIdioma.getEnlaceDestino());
            uiEntity.put("resumen" + idioma.toUpperCase(), contenidoIdioma.getResumen());
            uiEntity.put("mimeType" + idioma.toUpperCase(), contenidoIdioma.getMimeType());
            uiEntity.put("nombreFichero" + idioma.toUpperCase(), contenidoIdioma.getNombreFichero());
            uiEntity.put("html" + idioma.toUpperCase(), contenidoIdioma.getHtml());
            uiEntity.put("tipoFormato" + idioma.toUpperCase(), contenidoIdioma.getTipoFormato());
            uiEntity.put("idDescarga" + idioma.toUpperCase(), contenidoIdioma.getIdDescarga());

            if ("S".equals(contenidoIdioma.getHtml()))
            {
                Map<String, Object> valoresDeLaEditora =
                        contenidoIdiomaService.getContenidoComprobadoConAutoguardados(personaId, contenidoIdioma);

                if (valoresDeLaEditora != null && !valoresDeLaEditora.isEmpty())
                {
                    String contenidoEditora = (String) valoresDeLaEditora.get("contenido");

                    uiEntity.put("contenido" + idioma.toUpperCase(), contenidoEditora);

                    if ((Boolean) valoresDeLaEditora.get("contenidoProvenienteDeAutoguardado"))
                    {
                        contenidoSustituidoPorAutoguardado = true;
                    }
                }
            }
        }
        else
        {
            Contenido contenido = contenidoService.getContenido(contenidoId);
            String contenidoEditora = contenidoService.getAutoguardado(contenido, personaId, idioma);

            if (contenidoEditora != null && !contenidoEditora.isEmpty())
            {
                uiEntity.put("contenido" + idioma.toUpperCase(), contenidoEditora);
                contenidoSustituidoPorAutoguardado = true;
            }
        }

        return contenidoSustituidoPorAutoguardado;
    }

    private Persona getPersona()
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona();
        persona.setId(personaId);
        return persona;
    }

    @Path("{id}/metadato")
    public ContenidoMetadatosResource getPlatformMetadatos(@InjectParam ContenidoMetadatosResource metadatosResource)
    {
        return metadatosResource;
    }

    @Path("{id}/prioridad")
    public PrioridadContenidoResource getPlatformPrioridad(@InjectParam PrioridadContenidoResource prioridadResource)
    {
        return prioridadResource;
    }
}
