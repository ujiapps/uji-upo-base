package es.uji.apps.upo.services.rest.publicacion.formato.search;

import org.apache.commons.lang3.StringUtils;

public class OrExpression extends Expression
{
    @Override
    public String generate()
    {
        return "(" + StringUtils.join(getExpressions(), " or ") + ")";
    }
}
