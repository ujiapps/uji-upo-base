package es.uji.apps.upo.services.rest.propuestaexterna;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.exceptions.InsertadoContenidoException;
import es.uji.apps.upo.exceptions.NodoConNombreYaExistenteEnMismoPadreException;
import es.uji.apps.upo.model.Contenido;
import es.uji.apps.upo.model.Franquicia;
import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.model.Persona;
import es.uji.apps.upo.model.enums.TipoFormatoContenido;
import es.uji.apps.upo.model.metadatos.ContenidoMetadato;
import es.uji.apps.upo.services.ContenidoService;
import es.uji.apps.upo.services.FranquiciaService;
import es.uji.apps.upo.services.nodomapa.NodoMapaService;
import es.uji.apps.upo.utils.DateUtils;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;
import es.uji.commons.web.template.model.Pagina;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;

@Path("propuesta-externa")
public class PropuestaExternaResource extends CoreBaseService
{
    public static Logger log = LoggerFactory.getLogger(PropuestaExternaResource.class);

    public static final long FRANQUICIA_SCP = 1486723L;
    private final String URL_BASE = "/upo/rest/propuesta-externa/";
    private final String NODO_DESTINO = "/test/paco/propostes/";

    @InjectParam
    private NodoMapaService nodoMapaService;

    @InjectParam
    private FranquiciaService franquiciaService;

    @InjectParam
    private ContenidoService contenidoService;

    @GET
    public Response eujier()
            throws ParseException
    {
        Template template = buildTemplateBase();

        return Response.ok(template).type(MediaType.TEXT_HTML).build();
    }

    private Template buildTemplateBase()
            throws ParseException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Pagina pagina = new Pagina("", "propuestas", "ca", "Propostes externes");

        Template template = new HTMLTemplate("upo/propuestas/page-propuestas", new Locale("ca"), "upo");
        template.put("perId", connectedUserId);
        template.put("pagina", pagina);
        template.put("plantilla", "seccion-all-sections");
        template.put("urlBase", URL_BASE);

        return template;
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insert(MultivaluedMap<String, String> params)
            throws NodoConNombreYaExistenteEnMismoPadreException, InsertadoContenidoException, ParseException
    {
        Long personaId = AccessManager.getConnectedUserId(request);

        String tituloCA = params.getFirst("tituloCA");
        String tituloES = params.getFirst("tituloES");
        String subtituloCA = params.getFirst("subtituloCA");
        String subtituloES = params.getFirst("subtituloES");

        NodoMapa nodoMapa = creaNodoMapa();

        Contenido contenido =
                contenidoService.insertContenido(nodoMapa.getId().toString(), null, "index.html", "", personaId, "", "",
                        "", "", "", tituloCA, subtituloCA, tituloCA, "", "S", "", null, "", tituloES, subtituloES,
                        tituloES, "", "S", "", null, "", "", "", "", "", "S", "", null, "", "", false, "", "", "", "",
                        "", "", "", "", "", false, false, false,
                        DateUtils.convertDateToStringToPrint(new Date()) + " 00:00:00",
                        TipoFormatoContenido.PAGINA.toString(), TipoFormatoContenido.PAGINA.toString(),
                        TipoFormatoContenido.PAGINA.toString(), new HashMap<String, String>(),
                        new HashMap<String, String>(), new HashMap<String, String>(), "S", "",
                        new HashSet<ContenidoMetadato>());

        contenidoService.ajustaAPropuestaExterna(contenido);

        Template template = buildTemplateBase();

        template.put("guardado", true);

        return Response.ok(template).type(MediaType.TEXT_HTML).build();
    }

    private NodoMapa creaNodoMapa()
            throws NodoConNombreYaExistenteEnMismoPadreException
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        String personaUser = AccessManager.getConnectedUser(request).getName();

        NodoMapa parentNodoMapa = nodoMapaService.getNodoMapaByUrlCompleta(NODO_DESTINO);

        NodoMapa newNodoMapa = new NodoMapa();

        newNodoMapa.setMenuHeredado(true);
        newNodoMapa.setMenu(parentNodoMapa.getMenu());

        String urlPath = personaUser + '-' + newNodoMapa.normalizeUrlPath(DateUtils.getTimeStamp());

        newNodoMapa.setUrlPath(urlPath);
        newNodoMapa.setUrlCompleta(parentNodoMapa.getUrlCompleta() + urlPath + "/");
        newNodoMapa.setUpoMapa(parentNodoMapa);

        Franquicia franquicia = franquiciaService.getFranquicia(FRANQUICIA_SCP);
        newNodoMapa.setUpoFranquicia(franquicia);

        Persona persona = new Persona();
        persona.setId(personaId);

        nodoMapaService.insertForced(persona, newNodoMapa);

        return newNodoMapa;
    }
}