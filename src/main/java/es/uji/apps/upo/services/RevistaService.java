package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.FicheroDAO;
import es.uji.apps.upo.dao.RevistaDAO;
import es.uji.apps.upo.model.Fichero;
import es.uji.apps.upo.model.FicheroRevista;
import es.uji.apps.upo.model.Revista;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class RevistaService
{
    private RevistaDAO revistaDAO;
    private FicheroDAO ficheroDAO;
    private String server;

    @Autowired
    public RevistaService(RevistaDAO revistaDAO, FicheroDAO ficheroDAO, @Value("${uji.webapp.host}") String server)
    {
        this.revistaDAO = revistaDAO;
        this.ficheroDAO = ficheroDAO;
        this.server = server;
    }

    public List<Revista> doSearch(String titulo, String resumen, String resumenOP, String palabrasClave,
            String palabrasClaveOP, String seccion, String medio, Date fechaInicio, Date fechaFin)
    {
        return revistaDAO.doSearch(titulo, resumen, resumenOP, palabrasClave, palabrasClaveOP, seccion, medio,
                fechaInicio, fechaFin);
    }

    public String getServer()
    {
        return server;
    }

    public void setServer(String server)
    {
        this.server = server;
    }

    public Fichero getFichero(Long id)
    {
        List<Fichero> ficheros = ficheroDAO.get(Fichero.class, id);

        if (ficheros.size() > 0)
        {
            return ficheros.get(0);
        }

        return null;
    }

    public FicheroRevista getRevista(Long id)
    {
        List<FicheroRevista> ficheros = ficheroDAO.get(FicheroRevista.class, id);

        if (ficheros.size() > 0)
        {
            return ficheros.get(0);
        }

        return null;
    }
}
