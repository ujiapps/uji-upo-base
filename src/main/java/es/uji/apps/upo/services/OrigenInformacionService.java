package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.OrigenInformacionDAO;
import es.uji.apps.upo.model.OrigenInformacion;
import es.uji.commons.rest.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OrigenInformacionService
{
    private OrigenInformacionDAO origenInformacionDAO;

    @Autowired
    public OrigenInformacionService(OrigenInformacionDAO origenInformacionDAO)
    {
        this.origenInformacionDAO = origenInformacionDAO;
    }

    public List<OrigenInformacion> getOrigenesInformacion()
    {
        return origenInformacionDAO.getOrigenesInformacion();
    }

    public List<OrigenInformacion> getOrigenesInformacionUsadosEnCatalogoDeProcedimientos()
    {
        return origenInformacionDAO.origenesInformacionUsadosEnCatalogoDeProcedimientos();
    }

    @Role("ADMIN")
    @Transactional
    public OrigenInformacion insert(OrigenInformacion origenInformacion, Long connectedUserId)
    {
        return origenInformacionDAO.insert(origenInformacion);
    }

    @Role("ADMIN")
    @Transactional
    public OrigenInformacion update(OrigenInformacion origenInformacion, Long connectedUserId)
    {
        return origenInformacionDAO.update(origenInformacion);
    }

    @Role("ADMIN")
    @Transactional
    public void delete(long origenInformacionId, Long connectedUserId)
    {
        origenInformacionDAO.delete(OrigenInformacion.class, origenInformacionId);
    }
}
