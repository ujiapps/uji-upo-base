package es.uji.apps.upo.services.rest.publicacion.formato.search;

import es.uji.apps.upo.exceptions.EstructuraDeLosParametrosException;

public class CondicionFiltro
{
    public static final String SEPARADOR_VALORES = ":";
    private String nombre;
    private TipoCondicion tipo;
    private String agrupacion;
    private String valor;
    private String claveOriginal;

    public CondicionFiltro()
    {
    }

    public CondicionFiltro(String key, String value, String claveOriginal)
            throws EstructuraDeLosParametrosException
    {
        if (key == null || value == null)
        {
            return;
        }

        String[] parts = key.split(SEPARADOR_VALORES);

        if (parts.length < 3 || parts.length > 5)
        {
            throw new EstructuraDeLosParametrosException();
        }

        this.nombre = parts[1];
        this.valor = value;
        this.tipo = TipoCondicion.valueOf(parts[2]);
        this.claveOriginal = claveOriginal;

        if (parts.length >= 4)
        {
            this.agrupacion = parts[3];
        }
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public TipoCondicion getTipo()
    {
        return tipo;
    }

    public void setTipo(TipoCondicion tipo)
    {
        this.tipo = tipo;
    }

    public String getAgrupacion()
    {
        return agrupacion;
    }

    public void setAgrupacion(String agrupacion)
    {
        this.agrupacion = agrupacion;
    }

    public void setValor(String valor)
    {
        this.valor = valor;
    }

    public String getValor()
    {
        return valor;
    }

    public static void main(String[] args)
    {
        System.out.println("--".split("-").length);
    }

    public String getClaveOriginal()
    {
        return claveOriginal;
    }

    public void setClaveOriginal(String claveOriginal)
    {
        this.claveOriginal = claveOriginal;
    }
}
