package es.uji.apps.upo.services.rest.publicacion.formato;

public class Metadato
{
    private String clave;
    private String titulo;
    private String valor;
    private Boolean publicable;

    public Metadato(String clave, String titulo, String valor, Boolean publicable)
    {
        this.valor = valor;
        this.titulo = titulo;
        this.clave = clave;
        this.publicable = publicable;
    }

    public String getClave()
    {
        return clave;
    }

    public void setClave(String clave)
    {
        this.clave = clave;
    }

    public String getValor()
    {
        return valor;
    }

    public void setValor(String valor)
    {
        this.valor = valor;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public Boolean getPublicable()
    {
        return publicable;
    }

    public void isPublicable(Boolean publicable)
    {
        this.publicable = publicable;
    }
}
