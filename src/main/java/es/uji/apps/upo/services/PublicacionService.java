package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.PublicacionDAO;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.TipoPlantilla;
import es.uji.apps.upo.services.rest.publicacion.formato.search.Expression;
import es.uji.apps.upo.services.rest.publicacion.formato.search.SearchBuilder;
import es.uji.apps.upo.services.rest.publicacion.formato.search.SearchConverter;
import es.uji.apps.upo.services.rest.publicacion.formato.search.SearchFactory;
import es.uji.apps.upo.services.rest.publicacion.model.RequestParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PublicacionService
{
    private PublicacionDAO publicacionDAO;

    @Autowired
    private SearchConverter searchConverter;

    @Autowired
    private SearchFactory searchFactory;

    @Autowired
    public PublicacionService(PublicacionDAO publicacionDAO)
    {
        this.publicacionDAO = publicacionDAO;
    }

    public String getUrlNodoHeredado(String nombreNodo, String urlCompletaNodoActual)
    {
        return publicacionDAO.getUrlNodoHeredado(nombreNodo, urlCompletaNodoActual);
    }

    public NodoMapa getNodoMapaByUrlCompleta(String urlCompleta)
    {
        return publicacionDAO.getNodoMapaByUrlCompleta(urlCompleta);
    }

    public List<NodoMapaPlantilla> getNodoMapaPlantillasByNodoMapaId(Long nodoMapaId)
    {
        return publicacionDAO.getNodoMapaPlantillasByNodoMapaId(nodoMapaId);
    }

    public Plantilla getPlantillaByFileName(String fileName)
    {
        return publicacionDAO.getPlantillaByFileName(fileName);
    }

    public NodoMapaPlantilla getNodoMapaPlantillaByNodoMapaIdAndType(Long nodoMapaId, TipoPlantilla tipoPlantilla)
    {
        return publicacionDAO.getNodoMapaPlantillaByNodoMapaIdAndType(nodoMapaId, tipoPlantilla);
    }

    public Publicacion getContenidoIdiomaByContenidoIdAndLanguage(Long contenidoId, String language)
    {
        return publicacionDAO.getContenidoIdiomaByContenidoIdAndLanguage(contenidoId, language);
    }

    public List<Publicacion> getContenidosIdiomasByUrlNodoAndLanguage(String urlNodo, String language)
    {
        return publicacionDAO.getContenidosIdiomasByUrlNodoAndLanguage(urlNodo, language);
    }

    public PublicacionBinarios getBinarioByUrlCompletaAndLanguage(String urlCompleta, String language)
    {
        return publicacionDAO.getBinarioByUrlCompletaAndLanguage(urlCompleta, language);
    }

    public Map<String, Object> getContenidosIdiomasByUrlCompletaAndLanguageAndSearchFields(RequestParams requestParams)
    {
        SearchBuilder search = searchFactory.get(requestParams);

        for (Expression expression : searchConverter.convert(requestParams.getSearchParams().getFiltrosCabecera()))
        {
            search.addExpresionCabecera(expression);
        }

        for (Expression expression : searchConverter.convert(requestParams.getSearchParams().getFiltrosCuerpo()))
        {
            search.addExpresionCuerpo(expression);
        }

        for (Expression expression : searchConverter.convert(requestParams.getSearchParams().getFiltrosMetadatos()))
        {
            search.addExpresionMetadatos(expression);
        }

        return publicacionDAO.getContenidosIdiomasByUrlCompletaAndLanguageAndSearchFields(requestParams,
                search.generate());
    }

    public List<Grupo> getGruposByMenuId(Long menuId)
    {
        return publicacionDAO.getGruposByMenuId(menuId);
    }

    public List<Item> getItemsByGrupoId(Long grupoId)
    {
        return publicacionDAO.getItemsByGrupoId(grupoId);
    }

    public String getMigas(Long mapaId, String idiomaISO)
    {
        return publicacionDAO.getMigas(mapaId, idiomaISO);
    }
}

