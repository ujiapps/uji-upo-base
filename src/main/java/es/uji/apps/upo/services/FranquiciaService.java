package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.FranquiciaDAO;
import es.uji.apps.upo.model.Franquicia;
import es.uji.apps.upo.model.FranquiciaAcceso;
import es.uji.apps.upo.model.enums.TipoFranquiciaAcceso;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroDuplicadoException;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class FranquiciaService
{
    private FranquiciaDAO franquiciaDAO;
    private FranquiciaAccesoService franquiciaAccesoService;

    @Autowired
    public FranquiciaService(FranquiciaDAO franquiciaDAO, FranquiciaAccesoService franquiciaAccesoService)
    {
        this.franquiciaDAO = franquiciaDAO;
        this.franquiciaAccesoService = franquiciaAccesoService;
    }

    public List<Franquicia> getFranquicias()
    {
        return franquiciaDAO.getFranquiciaByOrder(Franquicia.class);
    }

    public Franquicia getFranquicia(Long franquiciaId)
    {
        return franquiciaDAO.getFranquiciaById(franquiciaId);
    }

    @Role("ADMIN")
    @Transactional
    public Franquicia insert(Franquicia franquicia, Long connectedUserId)
            throws UnauthorizedUserException
    {
        return franquiciaDAO.insert(franquicia);
    }

    @Role("ADMIN")
    @Transactional
    public void update(Franquicia franquicia, Long connectedUserId)
            throws UnauthorizedUserException
    {
        franquiciaDAO.update(franquicia);
    }

    @Role("ADMIN")
    @Transactional
    public void delete(Long id, Long connectedUserId) throws RegistroConHijosException,
            UnauthorizedUserException
    {
        try
        {
            franquiciaDAO.delete(Franquicia.class, id);
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException("");
        }
    }

    public List<FranquiciaAcceso> getAdministradores(Long franquiciaId)
    {
        return franquiciaDAO.getAdministradores(franquiciaId);
    }

    @Role("ADMIN")
    public void addAdministrador(Long franquiciaId, Long personaId,
                                 TipoFranquiciaAcceso tipoPermiso, Long connectedUserId)
            throws RegistroDuplicadoException, UnauthorizedUserException
    {
        try
        {
            franquiciaDAO.addAdministrador(franquiciaId, personaId, tipoPermiso);
        }
        catch (Exception e)
        {
            throw new RegistroDuplicadoException();
        }
    }

    @Role("ADMIN")
    public void deleteAdministrador(Long franquiciaId, Long personaId, Long connectedUserId)
            throws UnauthorizedUserException
    {
        franquiciaDAO.deleteAdministrador(franquiciaId, personaId);
    }

    @Role("ADMIN")
    @Transactional
    public void updateAdministrador(Long franquiciaId, Long personaId,
                                    TipoFranquiciaAcceso tipoPermiso, Long connectedUserId)
    {
        FranquiciaAcceso franquiciaAcceso =
                franquiciaAccesoService.getFranquiciaAccesoByFranquiciaIdAndPersonaId(franquiciaId, personaId);
        franquiciaAcceso.setTipo(tipoPermiso);

        franquiciaDAO.update(franquiciaAcceso);
    }

    @Role("ADMIN")
    @Transactional
    public void addAdministradoresFromOtherFranquicia(Long franquiciaDestinoId,
                                                      Long franquiciaOrigenId, Long connectedUserId)
    {
        franquiciaDAO.addAdministradoresFromOtherFranquicia(franquiciaDestinoId, franquiciaOrigenId);
    }
}
