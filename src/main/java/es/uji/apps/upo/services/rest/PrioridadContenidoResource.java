package es.uji.apps.upo.services.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.upo.exceptions.ActualizadoContenidoNoAutorizadoException;
import es.uji.apps.upo.exceptions.BorradoContenidoNoAutorizadoException;
import es.uji.apps.upo.exceptions.FechaFinPrioridadSuperiorAFechaInicioException;
import es.uji.apps.upo.exceptions.InsertadoContenidoNoAutorizadoException;
import es.uji.apps.upo.exceptions.SeIndicaUnaHoraSinFechaAsociadaException;
import es.uji.apps.upo.exceptions.UsuarioNoAutenticadoException;
import es.uji.apps.upo.model.Contenido;
import es.uji.apps.upo.model.Persona;
import es.uji.apps.upo.model.PrioridadContenido;
import es.uji.apps.upo.model.enums.TipoPrioridadContenido;
import es.uji.apps.upo.services.PrioridadContenidoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

public class PrioridadContenidoResource extends CoreBaseService
{
    private SimpleDateFormat formatter;

    @InjectParam
    PrioridadContenidoService prioridadContenidoService;

    public PrioridadContenidoResource()
    {
        formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    }

    @PathParam("id")
    String contenidoId;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPrioridades()
    {
        List<UIEntity> uiListaPrioridades = new ArrayList<UIEntity>();

        List<PrioridadContenido> prioridades = prioridadContenidoService
                .getPrioridadesByContenido(Long.parseLong(contenidoId));

        for (PrioridadContenido prioridadContenido : prioridades)
        {
            UIEntity uiEntity = UIEntity.toUI(prioridadContenido);
            addAtributosNoPrimitivosToUI(prioridadContenido, Long.parseLong(contenidoId), uiEntity);
            uiListaPrioridades.add(uiEntity);
        }

        return uiListaPrioridades;
    }

    @POST
    @Produces(MediaType.TEXT_XML)
    @Consumes(MediaType.TEXT_XML)
    public List<UIEntity> insertPrioridad(UIEntity uiEntity)
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        ArrayList<UIEntity> result = new ArrayList<UIEntity>();

        String prioridad = uiEntity.get("prioridad");

        if (contenidoId == null || prioridad == null)
        {
            return result;
        }

        Persona persona = getPersona();

        Contenido contenido = new Contenido();
        contenido.setId(Long.parseLong(uiEntity.get("contenidoId")));

        PrioridadContenido prioridadContenido = new PrioridadContenido();
        prioridadContenido.setPrioridad(TipoPrioridadContenido.valueOf(prioridad));
        prioridadContenido.setUpoObjeto(contenido);
        prioridadContenido.setFechaInicio(getDate(uiEntity.get("fechaInicio")));
        prioridadContenido.setFechaFin(getDate(uiEntity.get("fechaFin")));
        prioridadContenido.setHoraInicio(getDateTime(uiEntity.get("horaInicio")));
        prioridadContenido.setHoraFin(getDateTime(uiEntity.get("horaFin")));

        prioridadContenidoService.insert(persona, prioridadContenido);

        UIEntity uiEntityRetorno = UIEntity.toUI(prioridadContenido);
        addAtributosNoPrimitivosToUI(prioridadContenido, contenido.getId(), uiEntityRetorno);

        return Collections.singletonList(uiEntityRetorno);
    }

    @PUT
    @Path("{prioridadId}")
    @Produces(MediaType.TEXT_XML)
    @Consumes(MediaType.TEXT_XML)
    public List<UIEntity> updatePrioridad(@PathParam("prioridadId") String prioridadId,
            UIEntity uiEntity) throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            ActualizadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        ArrayList<UIEntity> result = new ArrayList<UIEntity>();

        String prioridad = uiEntity.get("prioridad");

        if (contenidoId == null || prioridad == null)
        {
            return result;
        }

        Persona persona = getPersona();

        Contenido contenido = new Contenido();
        contenido.setId(Long.parseLong(uiEntity.get("contenidoId")));

        PrioridadContenido prioridadContenido = prioridadContenidoService
                .getPrioridadByIdAndContenido(Long.parseLong(contenidoId),
                        Long.parseLong(prioridadId));
        prioridadContenido.setPrioridad(TipoPrioridadContenido.valueOf(prioridad));
        prioridadContenido.setUpoObjeto(contenido);
        prioridadContenido.setFechaInicio(getDate(uiEntity.get("fechaInicio")));
        prioridadContenido.setFechaFin(getDate(uiEntity.get("fechaFin")));
        prioridadContenido.setHoraInicio(getDateTime(uiEntity.get("horaInicio")));
        prioridadContenido.setHoraFin(getDateTime(uiEntity.get("horaFin")));

        prioridadContenidoService.update(persona, prioridadContenido);

        UIEntity uiEntityRetorno = UIEntity.toUI(prioridadContenido);
        addAtributosNoPrimitivosToUI(prioridadContenido, contenido.getId(), uiEntityRetorno);

        return Collections.singletonList(uiEntityRetorno);
    }

    @DELETE
    @Path("{prioridadId}")
    public void deletePrioridad(@PathParam("prioridadId") String prioridadId)
            throws BorradoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        PrioridadContenido prioridadContenido = prioridadContenidoService
                .getPrioridadByIdAndContenido(Long.parseLong(contenidoId),
                        Long.parseLong(prioridadId));

        Persona persona = null;
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        if (connectedUserId != null)
        {
            persona = new Persona();
            persona.setId(connectedUserId);
        }

        prioridadContenidoService.delete(persona, prioridadContenido);
    }

    private void addAtributosNoPrimitivosToUI(PrioridadContenido prioridadContenido, Long contenidoId,
            UIEntity uiEntity)
    {
        uiEntity.put("contenidoId", contenidoId);
        uiEntity.put("fechaInicio", prioridadContenido.getFechaInicio());
        uiEntity.put("fechaFin", prioridadContenido.getFechaFin());
        uiEntity.put("horaInicio", prioridadContenido.getHoraInicio());
        uiEntity.put("horaFin", prioridadContenido.getHoraFin());
        uiEntity.put("prioridad", prioridadContenido.getPrioridad());
    }

    private Persona getPersona()
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona();
        persona.setId(personaId);
        return persona;
    }

    public Date getDate(String value)
    {
        try
        {
            return formatter.parse(value);
        }
        catch (Exception e)
        {
            return null;
        }
    }

    private Date getDateTime(String time)
    {
        if (time != null && !time.isEmpty())
        {
            return getDate("01/01/2011 " + time + ":00");
        }
        else
        {
            return null;
        }
    }
}
