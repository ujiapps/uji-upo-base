package es.uji.apps.upo.services.rest.publicacion.formato.search;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SearchConverter
{
    private List<CondicionFiltro> condiciones;
    private CondicionFactory factory;

    @Autowired
    public SearchConverter(CondicionFactory searchFactory)
    {
        this.factory = searchFactory;
    }

    public List<Expression> convert(List<CondicionFiltro> condiciones)
    {
        this.condiciones = condiciones;

        List<Expression> expressions = new ArrayList<>();

        setCondicionesSinGrupo(expressions);
        setCondicionesConGrupo(expressions, getGrupos(condiciones));

        return expressions;
    }

    private void setCondicionesConGrupo(List<Expression> expressions, List<String> grupos)
    {
        for (String grupo : grupos)
        {
            OrExpression expression = new OrExpression();

            for (CondicionFiltro condicion : condiciones)
            {
                if (grupo.equals(condicion.getAgrupacion()))
                {
                    Filtro filtro = factory.get(condicion);

                    if (filtro != null)
                    {
                        expression.add(filtro);
                    }
                }
            }

            expressions.add(expression);
        }
    }

    private void setCondicionesSinGrupo(List<Expression> expressions)
    {
        for (CondicionFiltro condicion : condiciones)
        {
            if (!condicionSinGrupo(condicion))
            {
                continue;
            }

            Filtro filtro = factory.get(condicion);

            if (filtro == null)
            {
                continue;
            }

            expressions.add(filtro);
        }
    }

    private List<String> getGrupos(List<CondicionFiltro> condiciones)
    {
        List<String> groups = new ArrayList<>();

        for (CondicionFiltro condicion : condiciones)
        {
            if (condicionSinGrupo(condicion))
            {
                continue;
            }

            if (groups.contains(condicion.getAgrupacion()))
            {
                continue;
            }

            groups.add(condicion.getAgrupacion());
        }

        return groups;
    }

    private boolean condicionSinGrupo(CondicionFiltro condicion)
    {
        return condicion.getAgrupacion() == null || condicion.getAgrupacion().isEmpty();
    }
}
