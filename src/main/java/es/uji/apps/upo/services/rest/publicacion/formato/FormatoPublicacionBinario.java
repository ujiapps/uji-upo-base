package es.uji.apps.upo.services.rest.publicacion.formato;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.model.PublicacionBinarios;
import es.uji.apps.upo.services.rest.publicacion.UrlNormalizer;
import es.uji.apps.upo.services.rest.publicacion.model.RequestParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;

@Component
public class FormatoPublicacionBinario extends AbstractPublicacionHtml implements FormatoPublicacion
{
    private String server;

    @Autowired
    public void setServer(@Value("${uji.documentos.server}") String server)
    {
        this.server = server;
    }

    @Override
    public Response publica(RequestParams requestParams)
            throws MalformedURLException, ParseException, UnsupportedEncodingException, URISyntaxException
    {
        PublicacionBinarios contenido = publicacionService.getBinarioByUrlCompletaAndLanguage(requestParams.getUrl(),
                requestParams.getConfigPublicacion().getIdioma());

        if (contenido == null)
        {
            return new PaginaNoEncontrada().publica(requestParams);
        }

        String idDescarga = contenido.getIdDescarga();

        if (idDescarga != null)
        {
            String queryString = "";

            if (requestParams.getRequestDetails().getQueryString() != null)
            {
                queryString = "?" + requestParams.getRequestDetails().getQueryString();
            }

            return Response.temporaryRedirect(new URI(server + idDescarga + queryString)).build();
        }

        String contentDisposition = chooseContentDispositionByContentType(contenido);

        return Response.ok(contenido.getContenido())
                .type(contenido.getMimeType())
                .header("Content-Disposition",
                        contentDisposition + "; filename=\"" + contenido.getUrlNodo() + contenido.getUrlPath() + "\"")
                .build();
    }

    private String chooseContentDispositionByContentType(PublicacionBinarios contenido)
    {
        String contentDisposition = "attachment";

        if (contenido.getMimeType() != null && (contenido.getMimeType().startsWith("image/") || contenido.getMimeType()
                .equals("application/pdf")))
        {
            contentDisposition = "inline";
        }
        return contentDisposition;
    }
}
