package es.uji.apps.upo.services.rest.publicacion.formato;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class FormatoPublicacionFactory
{
    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private FormatoPublicacionUtils formatoPublicacionUtils;

    public FormatoPublicacion getFormat(String formato, String url, Boolean search)
    {
        if (formatoPublicacionUtils.necesitaAplicarPlantilla(url))
        {
            if ("html".equals(formato) && !search)
            {
                return applicationContext.getBean(FormatoPublicacionHtml.class);
            }

            if ("html".equals(formato) && search)
            {
                return applicationContext.getBean(FormatoPublicacionHtmlPaginado.class);
            }
        }

        if ("rss".equals(formato))
        {
            return applicationContext.getBean(FormatoPublicacionRss.class);
        }

        if ("video".equals(formato))
        {
            return applicationContext.getBean(FormatoPublicacionVideo.class);
        }

        return applicationContext.getBean(FormatoPublicacionBinario.class);
    }
}
