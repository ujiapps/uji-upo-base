package es.uji.apps.upo.services.rest.publicacion.formato;

import es.uji.apps.upo.model.Publicacion;
import es.uji.apps.upo.utils.DateUtils;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class FechasEventoExtractor
{
    public List<FechaEvento> extrae(Publicacion contenido)
            throws ParseException
    {
        List<FechaEvento> fechasEvento = new ArrayList<>();

        String fechasString = contenido.getFechasEvento();

        if (fechasString == null)
        {
            return fechasEvento;
        }

        String[] fieldList = fechasString.split("~~~");

        for (String field : fieldList)
        {
            String[] record = field.split("@@@");

            Date fecha = DateUtils.buildDateTimeFromString(record[0], null);
            Date horaInicio = null, horaFin = null;

            if (record.length > 1 && record[1] != null && !record[1].isEmpty())
            {
                horaInicio = DateUtils.buildDateTimeFromString(record[0], record[1]);
            }

            if (record.length > 2 && record[2] != null && !record[2].isEmpty())
            {
                horaFin = DateUtils.buildDateTimeFromString(record[0], record[2]);
            }

            fechasEvento.add(new FechaEvento(fecha, horaInicio, horaFin));
        }

        return fechasEvento;
    }
}
