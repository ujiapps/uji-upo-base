package es.uji.apps.upo.services;

import es.uji.apps.upo.exceptions.GeneralUPOException;
import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.model.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthenticatedService
{
    private PersonaService personaService;

    @Autowired
    public AuthenticatedService(PersonaService personaService)
    {
        this.personaService = personaService;
    }

    protected boolean personaAdminEnNodo(NodoMapa nodoMapa, Persona persona)
    {
        return (nodoMapa != null && personaService.isNodoMapaEditable(nodoMapa.getUrlCompleta(), persona));
    }

    public boolean personaAdminEnNodo(String url, Long connectedUserId) throws GeneralUPOException
    {
        return personaService.isNodoMapaEditable(url, connectedUserId);
    }
}
