package es.uji.apps.upo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.upo.dao.MigracionDAO;
import es.uji.apps.upo.model.Migracion;

@Service
public class MigracionService
{
    private MigracionDAO migracionDAO;

    @Autowired
    public MigracionService(MigracionDAO migracionDAO)
    {
        this.migracionDAO = migracionDAO;
    }

    public List<Migracion> getURLsByPaginationAndURL(Integer start, Integer limit, String query)
    {
        return migracionDAO.getURLsByPaginationAndURL(start, limit, query);
    }

    public int getCountURl(String query)
    {
        return migracionDAO.getCountURl(query);
    }

    public String getUrlPath(String url)
    {
        if (url != null)
        {
            String[] urlPathList = url.split("\\/");

            if (isNotEmpty(urlPathList))
            {
                return urlPathList[getLastPosition(urlPathList)];
            }
        }

        return null;
    }

    private int getLastPosition(String[] urlPathList)
    {
        return urlPathList.length - 1;
    }

    private boolean isNotEmpty(String[] urlPathList)
    {
        return urlPathList.length > 0;
    }
}
