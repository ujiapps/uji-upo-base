package es.uji.apps.upo.services.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import es.uji.apps.upo.model.Persona;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.upo.model.Grupo;
import es.uji.apps.upo.model.GrupoItem;
import es.uji.apps.upo.model.Item;
import es.uji.apps.upo.services.GrupoService;
import es.uji.apps.upo.services.ItemService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroDuplicadoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Service
@Path("grupo")
public class GrupoResource extends CoreBaseService
{
    @InjectParam
    private GrupoService grupoService;

    @InjectParam
    private ItemService itemService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getGrupos(@QueryParam("menuId") Long menuId)
    {
        Long userId = AccessManager.getConnectedUserId(request);

        if (menuId == null)
        {
            return UIEntity.toUI(grupoService.getGrupos(userId));
        }

        return UIEntity.toUI(grupoService.getGruposByMenuNoAsignados(menuId, userId));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> insert(UIEntity grupo)
    {
        Long userId = AccessManager.getConnectedUserId(request);

        if (!checkMandatoryFields(grupo)) return new ArrayList<UIEntity>();

        Grupo newGrupo = grupo.toModel(Grupo.class);
        newGrupo.setPersona(new Persona(userId));

        return Collections.singletonList(UIEntity.toUI(grupoService.insert(newGrupo)));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> update(@PathParam("id") String id, UIEntity grupo)
            throws UnauthorizedUserException
    {
        if (!checkMandatoryFields(grupo)) return new ArrayList<UIEntity>();

        Long userId = AccessManager.getConnectedUserId(request);
        Grupo newGrupo = grupo.toModel(Grupo.class);
        newGrupo.setPersona(new Persona(userId));

        grupoService.update(newGrupo, userId);

        return Collections.singletonList(grupo);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") String id)
            throws UnauthorizedUserException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        grupoService.delete(Long.parseLong(id), userId);
    }

    @GET
    @Path("{id}/item")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getItems(@PathParam("id") Long grupoId)
    {
        ArrayList<UIEntity> listaItemsAsociados = new ArrayList<UIEntity>();

        List<GrupoItem> items = grupoService.getItems(grupoId);

        for (GrupoItem grupoItem : items)
        {
            UIEntity uiEntity = new UIEntity();
            uiEntity.put("grupoId", grupoId);
            uiEntity.put("id", grupoItem.getItem().getId());
            uiEntity.put("url", grupoItem.getItem().getUrl());
            uiEntity.put("orden", grupoItem.getOrden());
            listaItemsAsociados.add(uiEntity);
        }

        return listaItemsAsociados;
    }

    @POST
    @Path("{id}/item")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> insertItem(@PathParam("id") Long grupoId, UIEntity uiEntity)
            throws RegistroDuplicadoException, UnauthorizedUserException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        String itemId = uiEntity.get("id");
        String orden = uiEntity.get("orden");

        if (grupoId == null || itemId == null || orden == null)
        {
            return new ArrayList<UIEntity>();
        }

        Item item = itemService.getItem(Long.parseLong(itemId));
        grupoService.addItem(grupoId, Long.parseLong(itemId), Integer.parseInt(orden), userId);

        uiEntity.put("url", item.getUrl());

        return Collections.singletonList(uiEntity);
    }

    @PUT
    @Path("{id}/item/{itemId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> updateItem(@PathParam("id") Long grupoId, @PathParam("itemId") Long itemId, UIEntity uiEntity)
            throws RegistroDuplicadoException, UnauthorizedUserException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        String orden = uiEntity.get("orden");

        if (grupoId == null || itemId == null || orden == null)
        {
            return new ArrayList<UIEntity>();
        }

        Item item = itemService.getItem(itemId);
        grupoService.updateItem(grupoId, itemId, Integer.parseInt(orden), userId);

        uiEntity.put("url", item.getUrl());

        return Collections.singletonList(uiEntity);
    }

    @DELETE
    @Path("{id}/item/{itemId}")
    public void deleteItem(@PathParam("id") Long grupoId, @PathParam("itemId") Long itemId)
            throws UnauthorizedUserException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        grupoService.deleteItem(grupoId, itemId, userId);
    }

    private boolean checkMandatoryFields(UIEntity item)
    {
        if (item == null || item.get("nombreCA") == null || item.get("nombreES") == null || item.get(
                "nombreEN") == null)
        {
            return false;
        }
        return true;
    }
}
