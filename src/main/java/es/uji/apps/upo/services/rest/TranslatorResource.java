package es.uji.apps.upo.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.exceptions.TraducirTextoException;
import es.uji.apps.upo.translator.TranslatorClient;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Service
@Path("translate")
public class TranslatorResource
{
    @InjectParam
    TranslatorClient translatorClient;

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public String getHtmlTranslation(@FormParam("text") String text, @FormParam("sourceLang") String sourceLang,
            @FormParam("targetLang") String targetLang, @FormParam("format") @DefaultValue("html") String format)
            throws TraducirTextoException
    {
        return translatorClient.translate(text, format, sourceLang, targetLang);
    }
}
