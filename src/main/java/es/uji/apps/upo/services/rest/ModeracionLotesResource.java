package es.uji.apps.upo.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.exceptions.InsertarModeracionLotesNoAutorizadoException;
import es.uji.apps.upo.exceptions.UrlNoValidaException;
import es.uji.apps.upo.exceptions.UsuarioNoAutenticadoException;
import es.uji.apps.upo.model.enums.EstadoModeracion;
import es.uji.apps.upo.model.ModeracionLote;
import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.model.Persona;
import es.uji.apps.upo.services.ModeracionLoteService;
import es.uji.apps.upo.services.nodomapa.NodoMapaService;
import es.uji.apps.upo.services.PersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Path("migracionlotes")
public class ModeracionLotesResource extends CoreBaseService
{
    @InjectParam
    private ModeracionLoteService moderacionLoteService;

    @InjectParam
    private PersonaService personaService;

    @InjectParam
    private NodoMapaService nodoMapaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getElementosPendientesModeracionLotes()
    {
        ArrayList<UIEntity> resultList = new ArrayList<UIEntity>();

        for (ModeracionLote moderacionLote : moderacionLoteService.getElementosPendientesModeracion())
        {
            UIEntity result = UIEntity.toUI(moderacionLote);

            result.put("estado", moderacionLote.getEstadoModeracion());

            resultList.add(result);
        }
        return resultList;
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity marcaLoteComoAprobadoRechazado(UIEntity entity)
            throws UnauthorizedUserException
    {
        ModeracionLote moderacionLote = entity.toModel(ModeracionLote.class);

        EstadoModeracion estadoModeracion = EstadoModeracion.valueOf(entity.get("estado"));
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        if (EstadoModeracion.ACEPTADO.equals(estadoModeracion))
        {
            moderacionLoteService.marcaLoteComoAceptado(moderacionLote.getId(), connectedUserId);
        }

        if (EstadoModeracion.RECHAZADO.equals(estadoModeracion))
        {
            moderacionLoteService.marcaLoteComoRechazado(moderacionLote.getId(), entity.get("motivo"),
                    connectedUserId);
        }

        return entity;
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage insertarPeticionMigracion(@FormParam("mapaId") String mapaId,
            @FormParam("url") String url) throws InsertarModeracionLotesNoAutorizadoException,
            UsuarioNoAutenticadoException, UrlNoValidaException, UnauthorizedUserException
    {
        Long personaId = AccessManager.getConnectedUserId(request);

        if (mapaId == null || url == null)
        {
            return null;
        }

        Persona persona = new Persona();
        persona.setId(personaId);

        Persona personaSolicitante = personaService.getPersona(personaId);

        ModeracionLote moderacionLote = new ModeracionLote();
        NodoMapa nodoMapa = nodoMapaService.getNodoMapa(Long.valueOf(mapaId), false);

        moderacionLote.setUrl(url);
        moderacionLote.setUpoMapa(nodoMapa);
        moderacionLote.setUpoExtPersonaSolicitud(personaSolicitante);

        moderacionLoteService.insert(moderacionLote, persona, nodoMapa, personaId);

        return new ResponseMessage(true);
    }
}