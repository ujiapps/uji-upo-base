package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.AtributoMetadatoDAO;
import es.uji.apps.upo.model.metadatos.AtributoMetadato;
import es.uji.apps.upo.model.metadatos.ValorMetadato;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.rest.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class AtributoMetadatoService
{
    private BaseDAO baseDAO;
    private AtributoMetadatoDAO atributoMetadatoDAO;

    @Autowired
    public AtributoMetadatoService(BaseDAO baseDAO, AtributoMetadatoDAO atributoMetadatoDAO)
    {
        this.baseDAO = baseDAO;
        this.atributoMetadatoDAO = atributoMetadatoDAO;
    }

    public List<AtributoMetadato> getByEsquemaId(Long esquemaId)
    {
        return atributoMetadatoDAO.getByEsquemaId(esquemaId);
    }

    @Role("ADMIN")
    public AtributoMetadato insertOrUpdateAtributoMetadato(AtributoMetadato atributoMetadato,
            HashSet<ValorMetadato> valoresMetadatos, Long connectedUserId)
    {
        AtributoMetadato atributoMetadatoReturn = new AtributoMetadato();

        if (atributoMetadato.getId() != null)
        {
            atributoMetadatoReturn = atributoMetadatoDAO.update(atributoMetadato);

            for (ValorMetadato valorMetadato : valoresMetadatos)
            {
                if (valorMetadato.getId() != null)
                {
                    atributoMetadatoDAO.update(valorMetadato);
                }

                else
                {
                    atributoMetadatoDAO.insert(valorMetadato);
                }
            }

            return atributoMetadatoReturn;
        }

        if (valoresMetadatos != null && !valoresMetadatos.isEmpty())
        {
            atributoMetadatoDAO.insert(atributoMetadato);
            atributoMetadato.setValoresMetadatos(valoresMetadatos);
            return atributoMetadatoDAO.update(atributoMetadato);
        }

        return atributoMetadatoDAO.insert(atributoMetadato);
    }

    @Transactional
    @Role("ADMIN")
    public void deleteAtributoMetadato(Long atributoId, Long connectedUserId)
    {
        atributoMetadatoDAO.delete(AtributoMetadato.class, atributoId);
    }

    public AtributoMetadato getById(Long atributoId)
    {
        return atributoMetadatoDAO.getById(atributoId);
    }

    @Role("ADMIN")
    @Transactional
    public void deleteAtributosValoresByAtributoIdAndValoresMetadatos(Long id, List<Long> idValoresMetadatos, Long connectedUserId)
    {
        atributoMetadatoDAO.deleteAtributosValoresByAtributoIdAndValoresMetadatos(id, idValoresMetadatos);
    }
}
