package es.uji.apps.upo.services.rest.publicacion.formato;

public class Atributo
{
    private String clave;
    private String valor;

    public Atributo(String clave, String valor)
    {
        this.valor = valor;
        this.clave = clave;
    }

    public String getClave()
    {
        return clave;
    }

    public void setClave(String clave)
    {
        this.clave = clave;
    }

    public String getValor()
    {
        return valor;
    }

    public void setValor(String valor)
    {
        this.valor = valor;
    }
}
