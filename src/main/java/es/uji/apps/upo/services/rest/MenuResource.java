package es.uji.apps.upo.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.model.Menu;
import es.uji.apps.upo.model.MenuGrupo;
import es.uji.apps.upo.model.Persona;
import es.uji.apps.upo.services.MenuService;
import es.uji.apps.upo.services.PersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroDuplicadoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@Path("menu")
public class MenuResource extends CoreBaseService
{
    @InjectParam
    private MenuService menuService;

    @InjectParam
    private PersonaService personaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getMenus()
    {
        Long userId = AccessManager.getConnectedUserId(request);

        return UIEntity.toUI(menuService.getMenusByUserId(userId));
    }

    @GET
    @Path("todos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getMenusTodos()
    {
        return UIEntity.toUI(menuService.getMenus());
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> insert(UIEntity menu)
    {
        Long userId = AccessManager.getConnectedUserId(request);
        if (menu == null || menu.get("nombre") == null)
        {
            return new ArrayList<UIEntity>();
        }

        Menu menuDB = menu.toModel(Menu.class);

        if (!personaService.isAdmin(userId))
        {
            menuDB.setPersona(new Persona(userId));
        }

        return Collections.singletonList(UIEntity.toUI(menuService.insert(menuDB)));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> update(@PathParam("id") String id, UIEntity menu)
            throws UnauthorizedUserException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        Menu newMenu = menu.toModel(Menu.class);

        if (!personaService.isAdmin(userId))
        {
            newMenu.setPersona(new Persona(userId));
        }

        menuService.update(newMenu, userId);

        return Collections.singletonList(menu);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") String id)
            throws UnauthorizedUserException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        menuService.delete(Long.parseLong(id), userId);
    }

    @GET
    @Path("{id}/grupo")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getGrupos(@PathParam("id") Long menuId)
            throws UnauthorizedUserException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        ArrayList<UIEntity> listaGruposAsociados = new ArrayList<UIEntity>();

        List<MenuGrupo> grupos = menuService.getGrupos(menuId, userId);

        for (MenuGrupo menuGrupo : grupos)
        {
            UIEntity uiEntity = new UIEntity();
            uiEntity.put("menuId", menuId);
            uiEntity.put("id", menuGrupo.getGrupo().getId());
            uiEntity.put("orden", menuGrupo.getOrden());
            listaGruposAsociados.add(uiEntity);
        }

        return listaGruposAsociados;
    }

    @POST
    @Path("{id}/grupo")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> insertGrupo(@PathParam("id") Long menuId, UIEntity uiEntity)
            throws RegistroDuplicadoException, UnauthorizedUserException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        String grupoId = uiEntity.get("id");
        String orden = uiEntity.get("orden");

        if (grupoId == null || menuId == null || orden == null)
        {
            return new ArrayList<UIEntity>();
        }

        menuService.addGrupo(menuId, Long.parseLong(grupoId), Integer.parseInt(orden), userId);

        return Collections.singletonList(uiEntity);
    }

    @PUT
    @Path("{id}/grupo/{grupoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> updateGrupo(@PathParam("id") String menuId, @PathParam("grupoId") String grupoId,
            UIEntity uiEntity)
            throws RegistroDuplicadoException, UnauthorizedUserException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        String orden = uiEntity.get("orden");

        if (grupoId == null || menuId == null || orden == null)
        {
            return new ArrayList<UIEntity>();
        }

        menuService.updateGrupo(Long.parseLong(menuId), Long.parseLong(grupoId), Integer.parseInt(orden), userId);

        return Collections.singletonList(uiEntity);
    }

    @DELETE
    @Path("{id}/grupo/{grupoId}")
    public void deleteGrupo(@PathParam("id") Long menuId, @PathParam("grupoId") Long grupoId)
            throws UnauthorizedUserException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        menuService.deleteGrupo(menuId, grupoId, userId);
    }
}
