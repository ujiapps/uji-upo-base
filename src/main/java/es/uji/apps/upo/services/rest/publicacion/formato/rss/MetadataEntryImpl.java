package es.uji.apps.upo.services.rest.publicacion.formato.rss;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Namespace;

import com.sun.syndication.feed.synd.SyndEnclosure;

public class MetadataEntryImpl implements MetadataEntry, Cloneable
{
    private Map<String, ValorAtributo> fields = new HashMap<String, ValorAtributo>();

    @Override
    public String getUri()
    {
        return URI;
    }

    @Override
    @SuppressWarnings("rawtypes")
    public Class getInterface()
    {
        return getClass();
    }

    @Override
    public Object clone()
    {
        MetadataEntryImpl info = new MetadataEntryImpl();
        info.copyFrom(this);

        return info;
    }

    @Override
    public Map<String, ValorAtributo> getFields()
    {
        return fields;
    }

    @Override
    public void setFields(Map<String, ValorAtributo> fields)
    {
        this.fields = fields;
    }

    @Override
    public void addField(String name, ValorAtributo valorAtributo)
    {
        this.fields.put(name, valorAtributo);
    }

    @Override
    public void copyFrom(Object o)
    {
        MetadataEntry module = (MetadataEntry) o;
        setFields(module.getFields());
    }

    public Namespace getNamespace(String namespace)
    {
        return Namespace.getNamespace(namespace, MetadataEntryImpl.URI);
    }
}