package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.AutoguardadoDAO;
import es.uji.apps.upo.model.Autoguardado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AutoguardadoService
{
    private AutoguardadoDAO autoguardadoDAO;

    @Autowired
    public AutoguardadoService(AutoguardadoDAO autoguardadoDAO)
    {
        this.autoguardadoDAO = autoguardadoDAO;
    }

    @Transactional
    public void insert(Autoguardado autoguardado)
    {
        autoguardadoDAO.insert(autoguardado);
    }

    public void delete(Long mapaId, Long contenidoId, Long personaId)
    {
        autoguardadoDAO.deleteByParameters(mapaId, contenidoId, personaId);
    }

    public Autoguardado getAutoguardadoMasActualByParameters(Long mapaId, Long contenidoId, Long personaId, String codigoIdiomaISO)
    {
        return autoguardadoDAO.getByParameters(mapaId, contenidoId, personaId, codigoIdiomaISO);
    }
}
