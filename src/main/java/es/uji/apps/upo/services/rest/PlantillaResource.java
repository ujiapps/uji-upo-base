package es.uji.apps.upo.services.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.upo.model.Plantilla;
import es.uji.apps.upo.model.enums.TipoPlantilla;
import es.uji.apps.upo.services.PlantillaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Path("plantilla")
public class PlantillaResource extends CoreBaseService
{
    @InjectParam
    PlantillaService plantillaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPlantillas(@QueryParam("tipo") String tipoPlantilla)
    {
        ArrayList<UIEntity> resultList = new ArrayList<UIEntity>();
        List<Plantilla> listaPlantillas = null;

        if (tipoPlantilla == null || tipoPlantilla.isEmpty())
        {
            listaPlantillas = plantillaService.getPlantillas();
        }
        else
        {
            listaPlantillas = plantillaService.getPlantillasByTipo(tipoPlantilla);
        }

        listaPlantillas.forEach(p -> resultList.add(buildPlantillaUI(p)));

        return resultList;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("vigente")
    public List<UIEntity> getPlantillasVigentes(@QueryParam("tipo") String tipoPlantilla)
    {
        List<UIEntity> resultList = new ArrayList<UIEntity>();

        resultList = plantillaService.getPlantillasVigentes().stream().map(this::buildPlantillaUI).collect(Collectors.toList());

        return resultList;
    }

    private UIEntity buildPlantillaUI(Plantilla plantilla)
    {
        UIEntity uiEntity = UIEntity.toUI(plantilla);
        uiEntity.put("tipo", plantilla.getTipo());
        uiEntity.put("obsoleta", plantilla.getObsoleta());

        return uiEntity;
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") String id)
    {
        Long userId = AccessManager.getConnectedUserId(request);
        plantillaService.delete(Long.parseLong(id), userId);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> update(@PathParam("id") String id, UIEntity uiEntity)
    {
        Long userId = AccessManager.getConnectedUserId(request);
        Plantilla plantilla = uiEntity.toModel(Plantilla.class);

        plantilla.setTipo(TipoPlantilla.valueOf(uiEntity.get("tipo")));
        plantillaService.update(plantilla, userId);

        return Collections.singletonList(uiEntity);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> insert(UIEntity uiEntity)
    {
        Plantilla plantilla = uiEntity.toModel(Plantilla.class);
        Long userId = AccessManager.getConnectedUserId(request);

        if (plantilla == null || plantilla.getNombre() == null || plantilla.getNombre().isEmpty())
        {
            return new ArrayList<UIEntity>();
        }

        String tipoPlantilla = uiEntity.get("tipo");
        plantilla.setTipo(TipoPlantilla.valueOf(tipoPlantilla));

        UIEntity entityReturn = UIEntity.toUI(plantillaService.insert(plantilla, userId));
        entityReturn.put("tipo", plantilla.getTipo().toString());

        return Collections.singletonList(entityReturn);
    }
}