package es.uji.apps.upo.services.rest.publicacion.model;

import java.util.HashMap;
import java.util.Map;

public class RequestDetails
{
    private String queryString;
    private String cookies;
    private Map<String, String> formParams;
    private Map<String, String> queryParams;

    public RequestDetails()
    {
        this.cookies = "";
        this.queryString = "";
        this.formParams = new HashMap<>();
        this.queryParams = new HashMap<>();
    }

    public String getQueryString()
    {
        return queryString;
    }

    public void setQueryString(String queryString)
    {
        this.queryString = queryString;
    }

    public String getCookies()
    {
        return cookies;
    }

    public void setCookies(String cookies)
    {
        this.cookies = cookies;
    }

    public Map<String, String> getFormParams()
    {
        return formParams;
    }

    public void setFormParams(Map<String, String> formParams)
    {
        this.formParams = formParams;
    }

    public Boolean isPostRequest()
    {
        return this.formParams == null || this.formParams.size() == 0;
    }

    public Map<String, String> getQueryParams()
    {
        return queryParams;
    }

    public void setQueryParams(Map<String, String> queryParams)
    {
        this.queryParams = queryParams;
    }
}

