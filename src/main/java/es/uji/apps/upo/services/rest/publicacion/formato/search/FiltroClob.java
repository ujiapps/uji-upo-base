package es.uji.apps.upo.services.rest.publicacion.formato.search;

public class FiltroClob extends Filtro<String>
{
    public FiltroClob(String clave, String valor)
    {
        super(clave, Util.replaceForbiddenChars(valor));
    }

    @Override
    public String generate()
    {
        return "(dbms_lob.instr(" + clave + ", utl_raw.cast_to_raw('" + valor + "')) > 0)";
    }
}
