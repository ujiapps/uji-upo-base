package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.FranquiciaDAO;
import es.uji.apps.upo.model.ClientConfiguration;
import es.uji.commons.sso.dao.ApaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ConfigurationService
{
    private ApaDAO apaDAO;
    private FranquiciaDAO franquiciaDAO;
    private ClientConfiguration clientConfiguration;
    private String host;
    private String hostHttp;
    private String heraEditoraUrl;

    @Autowired
    public ConfigurationService(ApaDAO apaDAO, FranquiciaDAO franquiciaDAO, ClientConfiguration clientConfiguration,
            @Value("${uji.webapp.host}") String host, @Value("${uji.webapp.host.http}") String hostHttp,
            @Value("${uji.hera.editora.url}") String heraEditoraUrl)
    {
        this.apaDAO = apaDAO;
        this.franquiciaDAO = franquiciaDAO;
        this.clientConfiguration = clientConfiguration;
        this.host = host;
        this.hostHttp = hostHttp;
        this.heraEditoraUrl = heraEditoraUrl;
    }

    public Boolean isAdmin(Long userId)
    {
        return apaDAO.hasPerfil("UPO", "ADMIN", userId);
    }

    public Boolean isDocumentalista(Long userId)
    {
        return apaDAO.hasPerfil("UPO", "DOCUMENTALISTA", userId);
    }

    public Boolean hasFranquicia(Long userId)
    {
        return franquiciaDAO.hasFranquicia(userId);
    }

    public String getHost()
    {
        return this.host;
    }

    public String getHostHttp()
    {
        return this.hostHttp;
    }

    public String getHeraEditoraUrl()
    {
        return this.heraEditoraUrl;
    }
}
