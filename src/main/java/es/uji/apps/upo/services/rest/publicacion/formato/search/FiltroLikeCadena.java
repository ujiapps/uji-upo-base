package es.uji.apps.upo.services.rest.publicacion.formato.search;

import es.uji.commons.rest.StringUtils;

public class FiltroLikeCadena extends Filtro<String>
{
    public FiltroLikeCadena(String clave, String valor)
    {
        super(clave, Util.replaceForbiddenChars(StringUtils.limpiaAcentos(valor.toLowerCase())));
    }

    @Override
    public String generate()
    {
        return "(" + clave + " like '%" + valor + "%')";
    }
}