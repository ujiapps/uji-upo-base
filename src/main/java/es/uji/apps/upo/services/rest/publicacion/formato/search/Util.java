package es.uji.apps.upo.services.rest.publicacion.formato.search;

public class Util
{
    public static String replaceForbiddenChars(String value)
    {
        return value.replace("'", "''");
    }
}
