package es.uji.apps.upo.services.rest.publicacion.formato.rss;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.jdom.Element;
import org.jdom.Namespace;

import com.sun.syndication.feed.module.Module;
import com.sun.syndication.io.ModuleGenerator;

public class MetadataModuleGenerator implements ModuleGenerator
{
    public void generate(Module module, Element element)
    {
        MetadataEntry modulePortalMetadata = (MetadataEntry) module;

        for (Map.Entry<String, ValorAtributo> field : modulePortalMetadata.getFields().entrySet())
        {
            element.addContent(generateSimpleElement(field.getKey(), field.getValue()));
        }
    }

    public java.util.Set<Namespace> getNamespaces()
    {
        Set<Namespace> namespaces = new HashSet<Namespace>();

        namespaces.add(MetadataEntry.namespace_general);
        namespaces.add(MetadataEntry.namespace_metadato);

        return namespaces;
    }

    public String getNamespaceUri()
    {
        return MetadataEntry.URI;
    }

    protected Element generateSimpleElement(String name, ValorAtributo values)
    {
        Element element = new Element(name, new MetadataEntryImpl().getNamespace(values
                .getNamespace()));
        element.addContent(values.getValor());

        return element;
    }
}
