package es.uji.apps.upo.services.rest.publicacion.formato;

import es.uji.apps.upo.model.ContenidoIdioma;
import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.model.NodoMapaPlantilla;
import es.uji.apps.upo.model.Publicacion;
import es.uji.apps.upo.model.enums.TipoPlantilla;
import es.uji.apps.upo.services.ContenidoIdiomaService;
import es.uji.apps.upo.services.ContenidoService;
import es.uji.apps.upo.services.PublicacionService;
import es.uji.apps.upo.services.rest.publicacion.MenuFactory;
import es.uji.apps.upo.services.rest.publicacion.formato.filtro.UrlTagsContentReplacer;
import es.uji.apps.upo.services.rest.publicacion.model.PublicacionDetails;
import es.uji.apps.upo.services.rest.publicacion.model.RequestDetails;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.PDFTemplate;
import es.uji.commons.web.template.Template;
import es.uji.commons.web.template.model.Atributo;
import es.uji.commons.web.template.model.Metadato;
import es.uji.commons.web.template.model.FechaEvento;
import es.uji.commons.web.template.model.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public abstract class AbstractPublicacionHtml
{
    public static final String PLANTILLA_DEFECTO = "2021/page-base";

    protected PublicacionService publicacionService;
    protected MetadataExtractor metadataExtractor;
    protected MigasExtractor migasExtractor;
    protected AtributoExtractor atributoExtractor;
    protected FechasEventoExtractor fechasEventoExtractor;
    protected UrlTagsContentReplacer urlTagsContentReplacer;
    protected MenuFactory menuFactory;

    protected String server;
    private ContenidoIdioma contenidoIdioma;
    private ContenidoIdiomaService contenidoIdiomaService;
    private ContenidoService contenidoService;

    @Autowired
    public void setMetadataExtractor(MetadataExtractor metadataExtractor)
    {
        this.metadataExtractor = metadataExtractor;
    }

    @Autowired
    public void setMigasExtractor(MigasExtractor migasExtractor)
    {
        this.migasExtractor = migasExtractor;
    }

    @Autowired
    public void setAtributoExtractor(AtributoExtractor atributoExtractor)
    {
        this.atributoExtractor = atributoExtractor;
    }

    @Autowired
    public void setFechasEventoExtractor(FechasEventoExtractor fechasEventoExtractor)
    {
        this.fechasEventoExtractor = fechasEventoExtractor;
    }

    @Autowired
    public void setUrlTagsContentReplacer(UrlTagsContentReplacer urlTagsContentReplacer)
    {
        this.urlTagsContentReplacer = urlTagsContentReplacer;
    }

    @Autowired
    public void setPublicacionService(PublicacionService publicacionService)
    {
        this.publicacionService = publicacionService;
    }

    @Autowired
    public void setMenuFactory(MenuFactory menuFactory)
    {
        this.menuFactory = menuFactory;
    }

    public String getServer()
    {
        return server;
    }

    @Autowired
    public void setServer(@Value("${uji.webapp.host}") String server)
    {
        this.server = server;
    }

    protected Template buildHTMLTemplate(Pagina pagina, String nombrePlantilla, PublicacionDetails publicacionDetails)
            throws MalformedURLException, ParseException, UnsupportedEncodingException
    {
        NodoMapaPlantilla nodoMapaPlantilla =
                publicacionService.getNodoMapaPlantillaByNodoMapaIdAndType(publicacionDetails.getNodoMapa().getId(),
                        TipoPlantilla.WEB);

        String templateName = PLANTILLA_DEFECTO;

        if (nodoMapaPlantilla != null)
        {
            templateName = nodoMapaPlantilla.getUpoPlantilla().getFichero();
        }

        if (nombrePlantilla != null)
        {
            templateName = nombrePlantilla;
        }

        Template template =
                new HTMLTemplate("upo/" + templateName, new Locale(publicacionDetails.getIdioma().toLowerCase()),
                        "upo");
        template.put("pagina", pagina);
        template.put("urlBase", publicacionDetails.getUrl());
        template.put("queryString", publicacionDetails.getRequestDetails().getQueryString());
        template.put("server", server);

        return template;
    }

    protected Template buildPDFTemplate(NodoMapa nodoMapa, String idioma, Pagina pagina)
            throws MalformedURLException, ParseException
    {
        NodoMapaPlantilla nodoMapaPlantilla =
                publicacionService.getNodoMapaPlantillaByNodoMapaIdAndType(nodoMapa.getId(), TipoPlantilla.PDF);

        String templateName = nodoMapaPlantilla.getUpoPlantilla().getFichero();

        Template template = new PDFTemplate("upo/" + templateName, new Locale(idioma.toLowerCase()), "upo");

        template.put("pagina", pagina);
        template.put("server", server);

        return template;
    }

    public Recurso createRecurso(String urlBase, Publicacion contenido, String idioma)
            throws UnsupportedEncodingException, ParseException
    {
        return createRecurso(urlBase, contenido, new RequestDetails(), idioma);
    }

    public Recurso createRecurso(String urlBase, Publicacion contenido, RequestDetails requestDetails, String idioma)
            throws ParseException, UnsupportedEncodingException
    {
        Recurso recurso = new Recurso(urlBase, contenido.getContenido());

        if (!esTexto(contenido))
        {
            recurso.setUrlDescarga(contenido.getUrlCompleta());
        }

        recurso.setId(contenido.getContenidoIdiomaId());
        recurso.setContenidoId(contenido.getContenidoId());
        recurso.setMimeType(contenido.getMimeType());
        recurso.setUrlNodo(contenido.getUrlContenido());
        recurso.setUrlNodoCompleta(contenido.getUrlNodoOriginal());
        recurso.setUrlPathNodo(contenido.getUrlPathNodo());
        recurso.setUrlDestino(contenido.getUrlDestino());
        recurso.setUrlCompleta(contenido.getUrlCompletaOriginal());
        recurso.setEsHtml(contenido.getEsHtml());
        recurso.setTituloNodo(contenido.getTituloNodo());
        recurso.setTipoFormato(contenido.getTipoFormato());
        recurso.setFechaModificacion(contenido.getFechaModificacion());
        recurso.setFechaCreacion(contenido.getFechaCreacion());
        recurso.setProximaHoraVigencia(getHora(contenido.getProximaFechaVigencia()));
        recurso.setProximaFechaVigencia(getFecha(contenido.getProximaFechaVigencia()));
        recurso.setPrimeraHoraVigencia(getHora(contenido.getPrimeraFechaVigencia()));
        recurso.setPrimeraFechaVigencia(getFecha(contenido.getPrimeraFechaVigencia()));

        if (sePuedeMostrar(contenido))
        {
            recurso.setTitulo(contenido.getTitulo());
            recurso.setTituloLargo(contenido.getTituloLargo());
            recurso.setSubTitulo(contenido.getSubtitulo());
            recurso.setAutor(contenido.getAutor());
            recurso.setOrigenNombre(contenido.getOrigenNombre());
            recurso.setOrigenUrl(contenido.getOrigenUrl());
            recurso.setLugar(contenido.getLugar());

            anyadeMetadatos(recurso, contenido);
            anyadeAtributos(recurso, contenido);
            anyadeTags(recurso, contenido);
            anyadeFechasEvento(recurso, contenido);

            if (contenido.getResumen() != null)
            {
                recurso.setResumen(contenido.getResumen());
            }
        }

        updateContenido(recurso, contenido, requestDetails, idioma);

        return recurso;
    }

    private void updateContenido(Recurso recurso, Publicacion contenido, RequestDetails requestDetails, String idioma)
            throws UnsupportedEncodingException
    {
        if (!esTexto(contenido))
        {
            return;
        }

        String textoNoVisible = contenido.getTextoNoVisible();

        if (textoNoVisible == null)
        {
            textoNoVisible = "";
        }

        if (!esPublicable(contenido))
        {
            recurso.setContenido("");
        }

        if (!esVisible(contenido))
        {
            recurso.setContenido(textoNoVisible);
        }

        if (sePuedeMostrar(contenido) && contenido.getContenido() != null)
        {
            recurso.setContenido(urlTagsContentReplacer.replace(contenido.getContenido(), requestDetails, idioma));
        }
    }

    private void anyadeMetadatos(Recurso recurso, Publicacion contenido)
    {
        List<es.uji.apps.upo.services.rest.publicacion.formato.Metadato> metadatos =
                metadataExtractor.extrae(contenido);

        for (es.uji.apps.upo.services.rest.publicacion.formato.Metadato metadato : metadatos)
        {
            recurso.addMetadato(new Metadato(metadato.getClave(), metadato.getTitulo(), metadato.getValor(),
                    metadato.getPublicable()));
        }
    }

    private void anyadeAtributos(Recurso recurso, Publicacion contenido)
    {
        List<es.uji.apps.upo.services.rest.publicacion.formato.Atributo> atributos =
                atributoExtractor.extrae(contenido);

        for (es.uji.apps.upo.services.rest.publicacion.formato.Atributo atributo : atributos)
        {
            recurso.addAtributo(new Atributo(atributo.getClave(), atributo.getValor()));
        }
    }

    private void anyadeTags(Recurso recurso, Publicacion contenido)
    {
        String tags = contenido.getTags();

        if (tags != null && !tags.isEmpty())
        {
            recurso.setTags(Arrays.asList(StringUtils.split(tags, ",")));
        }
    }

    private void anyadeFechasEvento(Recurso recurso, Publicacion contenido)
            throws ParseException
    {
        List<es.uji.apps.upo.services.rest.publicacion.formato.FechaEvento> fechasEvento =
                fechasEventoExtractor.extrae(contenido);

        for (es.uji.apps.upo.services.rest.publicacion.formato.FechaEvento fechaEvento : fechasEvento)
        {
            recurso.addFechaEvento(
                    new FechaEvento(fechaEvento.getFecha(), fechaEvento.getHoraInicio(), fechaEvento.getHoraFin()));
        }
    }

    protected boolean esTexto(Publicacion contenido)
    {
        return ("S".equals(contenido.getEsHtml()));
    }

    protected boolean esVisible(Publicacion contenido)
    {
        return ("S".equals(contenido.getVisible()));
    }

    protected boolean esPublicable(Publicacion contenido)
    {
        return ("S".equals(contenido.getPublicable()));
    }

    protected boolean sePuedeMostrar(Publicacion contenido)
    {
        return esVisible(contenido) && esPublicable(contenido);
    }

    private String getFecha(Date date)
            throws ParseException
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        try
        {
            return formatter.format(date);
        }
        catch (Exception e)
        {
            return null;
        }
    }

    private String getHora(Date date)
            throws ParseException
    {
        SimpleDateFormat formatter = new SimpleDateFormat("H:mm");

        try
        {
            return formatter.format(date);
        }
        catch (Exception e)
        {
            return null;
        }
    }

    public Date getDate(String value)
    {
        try
        {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

            return formatter.parse(value);
        }
        catch (Exception e)
        {
            return null;
        }
    }

    public String getTitulo(PublicacionDetails publicacionDetails)
    {
        String title;

        String tituloNodoMapa =
                getTituloByIdiomaFromNodoMapa(publicacionDetails.getNodoMapa(), publicacionDetails.getIdioma());

        if (ParamUtils.isNotNull(tituloNodoMapa))
        {
            title = tituloNodoMapa;
        }
        else
        {
            title = getTituloContenido(publicacionDetails);
        }

        return title;
    }

    protected String getTituloByIdiomaFromNodoMapa(NodoMapa nodoMapa, String idioma)
    {
        String tituloNodoMapa = "";

        if (idioma.toLowerCase().equals("ca"))
        {
            tituloNodoMapa = nodoMapa.getTituloPublicacionCA();
        }

        if (idioma.toLowerCase().equals("es"))
        {
            tituloNodoMapa = nodoMapa.getTituloPublicacionES();
        }

        if (idioma.toLowerCase().equals("en"))
        {
            tituloNodoMapa = nodoMapa.getTituloPublicacionEN();
        }

        return tituloNodoMapa;
    }

    protected String getTituloContenido(PublicacionDetails publicacionDetails)
    {
        List<Publicacion> contenidosHtml = getContenidosHtmlOfThisUrlByIdioma(publicacionDetails);

        if (contenidosHtml.size() == 1)
        {
            String tituloContenido = contenidosHtml.get(0).getTituloLargo();

            if (ParamUtils.isNotNull(tituloContenido))
            {
                return tituloContenido;
            }
        }

        return null;
    }

    protected List<Publicacion> getContenidosHtmlOfThisUrlByIdioma(PublicacionDetails publicacionDetails)
    {
        List<Publicacion> contenidosHtml = new ArrayList();

        for (Publicacion contenido : publicacionDetails.getContenidos())
        {
            if ("S".equals(contenido.getEsHtml()) && publicacionDetails.getIdioma()
                    .toLowerCase()
                    .equals(contenido.getIdioma().toLowerCase()) && contenido.getUrlNodo()
                    .equals(publicacionDetails.getUrl()))
            {
                contenidosHtml.add(contenido);
            }
        }

        return contenidosHtml;
    }

    protected Pagina buildPagina(PublicacionDetails publicacionDetails)
            throws ParseException, MalformedURLException, UnsupportedEncodingException
    {
        NodoMapa nodoMapa = publicacionDetails.getNodoMapa();
        Menu menu = menuFactory.buildMenuByNodoMapaMenu(nodoMapa.getMenu(), publicacionDetails.getIdioma());
        List<Miga> migas =
                migasExtractor.extrae(publicacionService.getMigas(nodoMapa.getId(), publicacionDetails.getIdioma()));

        Pagina pagina =
                new Pagina(publicacionDetails.getUrlBase(), publicacionDetails.getUrl(), publicacionDetails.getIdioma(),
                        getTitulo(publicacionDetails));

        pagina.setSubTitulo("");
        pagina.setMenu(menu);

        pagina.setMostrarReloj(nodoMapa.isMostrarReloj());
        pagina.setMostrarMenuPerfilesPlegado(nodoMapa.isMostrarMenuPerfilesPlegado());
        pagina.setMostrarRecursosRelacionados(nodoMapa.isMostrarRecursosRelacionados());
        pagina.setMostrarTags(nodoMapa.isMostrarTags());
        pagina.setMostrarFechaModificacion(nodoMapa.isMostrarFechaModificacion());
        pagina.setMostrarFuente(nodoMapa.isMostrarFuente());
        pagina.setMostrarRedesSociales(nodoMapa.isMostrarRedesSociales());
        pagina.setMostrarInformacionProporcionada(nodoMapa.isMostrarInformacionProporcionada());
        pagina.setMostrarBarraGris(nodoMapa.isMostrarBarraGris());
        pagina.setMigas(migas);
        pagina.setMostrarFechasEvento(nodoMapa.isMostrarFechasEvento());

        return pagina;
    }

    protected void addSeccionHeredada(String nombreSeccion, Seccion seccion, PublicacionDetails publicacionDetails)
            throws ParseException, UnsupportedEncodingException
    {
        if (!seccion.existeNodo(seccion.getSeccionesSinFiltrar(), nombreSeccion))
        {
            String urlSeccion = publicacionService.getUrlNodoHeredado(nombreSeccion, publicacionDetails.getUrl());

            if (urlSeccion == null || urlSeccion.isEmpty())
            {
                return;
            }

            List<Publicacion> contenidos = publicacionService.getContenidosIdiomasByUrlNodoAndLanguage(urlSeccion,
                    publicacionDetails.getIdioma());

            if (contenidos != null && !contenidos.isEmpty())
            {
                Seccion nuevaSeccion = new Seccion(publicacionDetails.getUrlBase(), nombreSeccion,
                        publicacionDetails.getUrl() + nombreSeccion, "", "", true);

                for (Publicacion contenido : contenidos)
                {
                    if (debeAnyadirseRecurso(contenido, true))
                    {
                        nuevaSeccion.add(createRecurso(publicacionDetails.getUrlBase(), contenido,
                                publicacionDetails.getRequestDetails(), publicacionDetails.getIdioma()));
                    }
                }

                seccion.addSeccion(nuevaSeccion);
            }
        }
    }

    protected Seccion buildEstructura(PublicacionDetails publicacionDetails, Seccion seccion)
            throws ParseException, UnsupportedEncodingException
    {
        List<Publicacion> contenidos = publicacionDetails.getContenidos();

        if (contenidos != null && !contenidos.isEmpty())
        {
            for (Publicacion contenido : contenidos)
            {
                String urlLocal = contenido.getUrlCompleta().substring(publicacionDetails.getUrl().length());
                String urlCompleta = publicacionDetails.getUrl();
                Seccion seccionActual = seccion;

                boolean esPrimerNivel = !urlLocal.contains("/");
                int index = 0;

                for (String urlPath : urlLocal.split("/"))
                {
                    if (esUrlPathDeSeccion(urlLocal, index))
                    {
                        urlCompleta += urlPath + "/";
                        NodoMapa nodoMapaSeccion = publicacionService.getNodoMapaByUrlCompleta(urlCompleta);
                        Seccion nuevaSeccion = new Seccion(publicacionDetails.getUrlBase(), urlPath, urlCompleta,
                                nodoMapaSeccion.getEnlaceDestino(),
                                getTituloByIdiomaFromNodoMapa(nodoMapaSeccion, publicacionDetails.getIdioma()),
                                nodoMapaSeccion.isContenidoRelacionado());
                        seccionActual = seccionActual.addSeccion(nuevaSeccion);
                    }

                    if (esUrlPathDeContenido(urlLocal, index))
                    {
                        if (debeAnyadirseRecurso(contenido, esPrimerNivel))
                        {
                            if (esPrimerNivel && contenido.getIdioma().equalsIgnoreCase("en") && (contenido.getContenido() == null || contenido.getContenido().length == 0)) {
                                sustituyeContenidoEnIngles(publicacionDetails, contenido);
                            }

                            seccionActual.add(createRecurso(publicacionDetails.getUrlBase(), contenido,
                                    publicacionDetails.getRequestDetails(), publicacionDetails.getIdioma()));
                        }
                    }

                    index++;
                }
            }
        }

        return seccion;
    }

    private void sustituyeContenidoEnIngles(PublicacionDetails publicacionDetails, Publicacion contenido) {
        Publicacion contenidoIdioma = publicacionService.getContenidoIdiomaByContenidoIdAndLanguage(contenido.getContenidoId(), "ES");

        if (contenidoIdioma != null && contenidoIdioma.getContenido() != null && contenidoIdioma.getContenido().length > 0)
        {
            contenido.setTituloLargo("Not found in English");
            contenido.setContenido(("<p class='default-content'>" +
                    "Sorry, this information is only available in " +
                    "<a href='/upo/rest/publicacion/idioma/ca?urlRedirect=" + server + publicacionDetails.getUrl() + "&amp;" + publicacionDetails.getRequestDetails().getQueryString() + "'>Valencian</a> " +
                    "and " +
                    "<a href='/upo/rest/publicacion/idioma/es?urlRedirect=" + server + publicacionDetails.getUrl() + "&amp;" + publicacionDetails.getRequestDetails().getQueryString() + "'>Spanish</a>.</p>").getBytes());
        }
    }

    private boolean debeAnyadirseRecurso(Publicacion contenido, Boolean esPrimerNivel)
    {
        if (!esTexto(contenido) && (!sePuedeMostrar(contenido)))
        {
            return false;
        }

        if (esTexto(contenido) && !sePuedeMostrar(contenido) && !esPrimerNivel)
        {
            return false;
        }

        return true;
    }

    private boolean esUrlPathDeContenido(String url, int index)
    {
        return index == url.split("/").length - 1;
    }

    private boolean esUrlPathDeSeccion(String url, int index)
    {
        return index < url.split("/").length - 1;
    }

    @Autowired
    public void setContenidoIdioma(ContenidoIdioma contenidoIdioma) {
        this.contenidoIdioma = contenidoIdioma;
    }

    @Autowired
    public void setContenidoIdiomaService(ContenidoIdiomaService contenidoIdiomaService) {
        this.contenidoIdiomaService = contenidoIdiomaService;
    }

    @Autowired
    public void setContenidoService(ContenidoService contenidoService) {
        this.contenidoService = contenidoService;
    }
}
