package es.uji.apps.upo.services.rest.publicacion.formato;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;

import com.sun.syndication.feed.synd.SyndEnclosure;
import com.sun.syndication.feed.synd.SyndEnclosureImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndFeedImpl;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedOutput;

import es.uji.apps.upo.services.rest.publicacion.formato.rss.ContenidoRSS;
import es.uji.apps.upo.services.rest.publicacion.formato.rss.Enclosure;
import es.uji.apps.upo.services.rest.publicacion.formato.rss.FeedRSS;
import es.uji.apps.upo.services.rest.publicacion.formato.rss.MetadataEntry;
import es.uji.apps.upo.services.rest.publicacion.formato.rss.MetadataEntryImpl;
import es.uji.apps.upo.services.rest.publicacion.formato.rss.ValorAtributo;

public class FeedPublicacion
{
    private SyndFeed feed;

    public FeedPublicacion()
    {
        feed = new SyndFeedImpl();

        feed.getModules().add(new MetadataEntryImpl());

        feed.setFeedType("rss_2.0");
        feed.setTitle("Universitat Jaume I UPO");
        feed.setLink("http://www.uji.es/upo/");
        feed.setLanguage("es-es");
        feed.setCopyright("(c) 2013 Universitat Jaume I");
        feed.setDescription("Canal RSS Universitat Jaume I UPO");
    }

    public void addEntries(FeedRSS contenidosRSS)
    {
        for (Map.Entry<String, ContenidoRSS> contenidoRSS : contenidosRSS.entrySet())
        {
            addEntry(contenidoRSS.getValue());
        }
    }

    public void addEntry(ContenidoRSS contenidoRSS)
    {
        SyndEntry entry = new SyndEntryImpl();

        addEntryFields(contenidoRSS, entry);
        addMetadataFields(contenidoRSS, entry);
        addGeneralFields(contenidoRSS, entry);
        addEnclosures(contenidoRSS, entry);

        feed.getEntries().add(entry);
    }

    private void addGeneralFields(ContenidoRSS contenidoRSS, SyndEntry entry)
    {
        MetadataEntry generalEntry = new MetadataEntryImpl();

        Map<String, ValorAtributo> fields = new HashMap<String, ValorAtributo>();

        fields.put("contenidoId", getValorAtributoGeneral(String.valueOf(contenidoRSS.getContenidoId())));
        fields.put("urlCompleta", getValorAtributoGeneral(contenidoRSS.getUrlCompleta()));
        fields.put("autor", getValorAtributoGeneral(contenidoRSS.getAutor()));
        fields.put("origen", getValorAtributoGeneral(contenidoRSS.getOrigen()));
        fields.put("lugar", getValorAtributoGeneral(contenidoRSS.getLugar()));
        fields.put("idioma", getValorAtributoGeneral(contenidoRSS.getIdioma()));
        fields.put("tituloLargo", getValorAtributoGeneral(contenidoRSS.getTituloLargo()));
        fields.put("titulo", getValorAtributoGeneral(contenidoRSS.getTitulo()));
        fields.put("subtitulo", getValorAtributoGeneral(contenidoRSS.getSubtitulo()));
        fields.put("resumen", getValorAtributoGeneral(contenidoRSS.getResumen()));
        fields.put("contenido", getValorAtributoGeneral(contenidoRSS.getContenido()));

        generalEntry.setFields(fields);

        entry.getModules().add(generalEntry);
    }

    private void addMetadataFields(ContenidoRSS contenidoRSS, SyndEntry entry)
    {
        MetadataEntry metadatosEntry = new MetadataEntryImpl();

        for (Metadato metadato : contenidoRSS.getMetadatos())
        {
            metadatosEntry.addField(metadato.getClave(),
                    getValorAtributoMetadato(metadato.getValor()));
        }

        entry.getModules().add(metadatosEntry);
    }

    private void addEnclosures(ContenidoRSS contenidoRSS, SyndEntry entry)
    {
        if (contenidoRSS.getEnclosures() == null)
        {
            return;
        }

        List<SyndEnclosure> enclosureList = new ArrayList<SyndEnclosure>();

        for (Enclosure enclosure : contenidoRSS.getEnclosures())
        {
            SyndEnclosure syndEnclosure = new SyndEnclosureImpl();
            syndEnclosure.setType(enclosure.getType());
            syndEnclosure.setUrl(enclosure.getUrl());

            enclosureList.add(syndEnclosure);
        }

        entry.setEnclosures(enclosureList);
    }

    private void addEntryFields(ContenidoRSS contenidoRSS, SyndEntry entry)
    {
        entry.setTitle(contenidoRSS.getTituloLargo());
        entry.setPublishedDate(contenidoRSS.getFechaModificacion());
        entry.setLink(contenidoRSS.getUrlCompleta());
        entry.setAuthor(contenidoRSS.getAutor());
    }

    public Document getXML() throws FeedException
    {
        SyndFeedOutput output = new SyndFeedOutput();
        return output.outputW3CDom(feed);
    }

    private ValorAtributo getValorAtributoGeneral(String valor)
    {
        return new ValorAtributo(valor, MetadataEntry.PREFIX_UPO_GENERAL);
    }

    private ValorAtributo getValorAtributoMetadato(String valor)
    {
        return new ValorAtributo(valor, MetadataEntry.PREFIX_UPO_METADATO);
    }
}
