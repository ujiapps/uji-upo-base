package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.IdiomaDAO;
import es.uji.apps.upo.model.Idioma;
import es.uji.commons.rest.Role;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class IdiomaService
{
    private IdiomaDAO idiomaDAO;

    @Autowired
    public IdiomaService(IdiomaDAO idiomaDAO)
    {
        this.idiomaDAO = idiomaDAO;
    }

    public List<Idioma> getIdiomas()
    {
        return idiomaDAO.get(Idioma.class);
    }

    public Idioma getIdiomaByCodigoISO(String codigoISO)
    {
        return idiomaDAO.getIdiomaByCodigoISO(codigoISO);
    }

    @Role("ADMIN")
    @Transactional
    public void delete(Long id, Long connectedUserId) throws UnauthorizedUserException
    {
        idiomaDAO.delete(Idioma.class, id);
    }

    @Role("ADMIN")
    @Transactional
    public Idioma insert(Idioma idioma, Long connectedUserId) throws UnauthorizedUserException
    {
        return idiomaDAO.insert(idioma);
    }

    @Role("ADMIN")
    @Transactional
    public Idioma update(Idioma idioma, Long connectedUserId) throws UnauthorizedUserException
    {
        return idiomaDAO.update(idioma);
    }
}