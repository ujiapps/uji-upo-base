package es.uji.apps.upo.services.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.upo.model.Criterio;
import es.uji.apps.upo.services.CriterioService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("criterio")
public class CriterioResource extends CoreBaseService
{
    @InjectParam
    CriterioService criterioService;

    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<UIEntity> getCriterios()
    {
        return UIEntity.toUI(criterioService.getCriterios());
    }

    @GET
    @Path("{criterioId}")
    @Produces(MediaType.APPLICATION_XML)
    public List<UIEntity> getCriterioById(@PathParam("criterioId") String criterioId)
    {
        Criterio criterio = criterioService.getCriterioById(Long.parseLong(criterioId));
        UIEntity uiEntity = UIEntity.toUI(criterio);

        return Collections.singletonList(uiEntity);
    }

    @PUT
    @Path("{criterioId}")
    @Consumes(MediaType.TEXT_XML)
    public List<UIEntity> update(@PathParam("criterioId") String criterioId, UIEntity uiEntity)
    {
        Criterio criterio = uiEntity.toModel(Criterio.class);
        criterioService.update(criterio);

        return Collections.singletonList(uiEntity);
    }

    @DELETE
    @Path("{criterioId}")
    public void delete(@PathParam("criterioId") String criterioId)
    {
        criterioService.delete(Long.parseLong(criterioId));
    }

    @POST
    @Consumes(MediaType.TEXT_XML)
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> insert(UIEntity uiEntity)
    {
        Criterio criterio = uiEntity.toModel(Criterio.class);

        if (criterio == null || criterio.getNombre() == null)
        {
            return new ArrayList<UIEntity>();
        }

        criterio.setProximidadDiasEvento("N");
        criterio.setPopularidad("N");
        criterio.setPrioridad("N");

        return Collections.singletonList(UIEntity.toUI(criterioService.insert(criterio)));
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_XML)
    public UIEntity updateCriterio(MultivaluedMap<String, String> params)
    {
        String criterioId = params.getFirst("criterioId");

        Criterio criterio = criterioService.getCriterioById(Long.parseLong(criterioId));

        criterio.setProximidadDiasEvento(getParam(params, "proximidadDiasEvento", "N"));
        criterio.setNumeroDias(getDecimalParam(params, "numeroDias"));
        criterio.setPesoDiasEvento(getDecimalParam(params, "pesoDiasEnvento"));
        criterio.setPopularidad(getParam(params, "popularidad", "N"));
        criterio.setPesoPopularidad(getDecimalParam(params, "pesoPopularidad"));
        criterio.setDiasVentanaPopularidad(getDecimalParam(params, "diasVentanaPopularidad"));
        criterio.setPrioridad(getParam(params, "prioridad", "N"));
        criterio.setPesoPrioridad(getDecimalParam(params, "pesoPrioridad"));
        criterio.setDiasUrgente(getDecimalParam(params, "diasUrgente"));
        criterio.setDiasAlta(getDecimalParam(params, "diasAlta"));

        criterioService.update(criterio);

        return UIEntity.toUI(criterio);
    }
}
