package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.ContenidoIdiomaRecursoDAO;
import es.uji.apps.upo.model.ContenidoIdioma;
import es.uji.apps.upo.model.ContenidoIdiomaRecurso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContenidoIdiomaRecursoService
{
    private ContenidoIdiomaRecursoDAO contenidoIdiomaRecursoDAO;

    @Autowired
    public ContenidoIdiomaRecursoService(ContenidoIdiomaRecursoDAO contenidoIdiomaRecursoDAO)
    {
        this.contenidoIdiomaRecursoDAO = contenidoIdiomaRecursoDAO;
    }

    @Transactional
    public void deleteByContenido(Long contenidoId)
    {
        contenidoIdiomaRecursoDAO.deleteByContenido(contenidoId);
    }

    public List<ContenidoIdiomaRecurso> getContenidoIdiomaRecursosByContenidoIdioma(
            ContenidoIdioma contenidoIdioma)
    {
        return contenidoIdiomaRecursoDAO.getContenidoIdiomaRecursosUrlByContenidoIdioma(contenidoIdioma);
    }

}
