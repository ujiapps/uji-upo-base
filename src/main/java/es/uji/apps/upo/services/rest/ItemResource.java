package es.uji.apps.upo.services.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import es.uji.apps.upo.model.Persona;
import es.uji.apps.upo.services.PersonaService;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.upo.model.Item;
import es.uji.apps.upo.services.ItemService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Service
@Path("item")
public class ItemResource extends CoreBaseService
{
    @InjectParam
    ItemService itemService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getItems(@QueryParam("grupoId") Long grupoId)
    {
        Long userId = AccessManager.getConnectedUserId(request);

        if (grupoId == null)
        {
            return UIEntity.toUI(itemService.getItems(userId));
        }

        return UIEntity.toUI(itemService.getItemsByGrupoNoAsignados(grupoId, userId));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> insert(UIEntity item)
    {
        if (!checkMandatoryFields(item)) return new ArrayList<UIEntity>();

        Item newItem = item.toModel(Item.class);
        Long userId = AccessManager.getConnectedUserId(request);
        newItem.setPersona(new Persona(userId));

        return Collections.singletonList(UIEntity.toUI(itemService.insert(newItem)));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> update(@PathParam("id") String id, UIEntity item)
            throws UnauthorizedUserException
    {
        if (!checkMandatoryFields(item)) return new ArrayList<UIEntity>();

        Long userId = AccessManager.getConnectedUserId(request);
        Item newItem = item.toModel(Item.class);
        newItem.setPersona(new Persona(userId));
        itemService.update(newItem, userId);

        return Collections.singletonList(item);
    }

    private boolean checkMandatoryFields(UIEntity item)
    {
        if (item == null || item.get("nombreCA") == null || item.get("nombreES") == null || item.get(
                "nombreEN") == null || item.get("url") == null)
        {
            return false;
        }
        return true;
    }

    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") String id)
            throws UnauthorizedUserException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        itemService.delete(Long.parseLong(id), userId);
    }
}