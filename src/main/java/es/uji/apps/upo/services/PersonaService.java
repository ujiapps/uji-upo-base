package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.NodoMapaDAO;
import es.uji.apps.upo.dao.PersonaDAO;
import es.uji.apps.upo.model.Persona;
import es.uji.apps.upo.model.PersonaTodas;
import es.uji.apps.upo.model.UsuarioExterno;
import es.uji.commons.rest.Role;
import es.uji.commons.sso.dao.ApaDAO;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PersonaService
{
    private PersonaDAO personaDAO;
    private NodoMapaDAO nodoMapaDAO;
    private ApaDAO apaDAO;
    private List<UsuarioExterno> usuariosExternos;

    @Autowired
    public PersonaService(PersonaDAO personaDAO, NodoMapaDAO nodoMapaDAO, ApaDAO apaDAO)
    {
        this.personaDAO = personaDAO;
        this.nodoMapaDAO = nodoMapaDAO;
        this.apaDAO = apaDAO;
    }

    public Persona getPersona(long personaId)
    {
        return personaDAO.getPersona(personaId);
    }

    public List<Persona> getPersonasByFranquiciaIdNoAsignados(Long franquiciaId)
    {
        return personaDAO.getPersonasByFranquiciaIdNoAsignados(franquiciaId);
    }

    public List<Persona> getPersonasAdministradoresByFranquiciaId(Long franquiciaId)
    {
        return personaDAO.getPersonasAdministradorasByFranquiciaId(franquiciaId);
    }

    public List<Persona> getPersonas()
    {
        return personaDAO.getPersonas();
    }

    public Boolean isNodoMapaEditable(String urlCompleta, Persona persona)
    {
        return isNodoMapaEditable(urlCompleta, persona.getId());
    }

    public Boolean isNodoMapaEditable(String urlCompleta, Long perId)
    {
        return nodoMapaDAO.isNodoMapaAdmin(urlCompleta, perId) && !nodoMapaDAO.isNodoMapaLocked(urlCompleta);
    }

    public Boolean isNodoMapaAdmin(String urlCompleta, Long perId)
    {
        return nodoMapaDAO.isNodoMapaAdmin(urlCompleta, perId);
    }

    public Boolean isNodoMapaLocked(String urlCompleta)
    {
        return nodoMapaDAO.isNodoMapaLocked(urlCompleta);
    }

    public Boolean isAdmin(Long personaId)
    {
        return apaDAO.hasPerfil("UPO", Persona.ROLE_ADMIN, personaId);
    }

    @Role("ADMIN")
    public List<PersonaTodas> getUsuariosExternos(Long connectedUserId)
    {
        return personaDAO.getUsuariosExternos();
    }

    @Role("ADMIN")
    @Transactional
    public void deleteUsuarioExterno(Long personaId, Long connectedUserId)
    {
        personaDAO.delete(UsuarioExterno.class, personaId);
    }

    @Role("ADMIN")
    public PersonaTodas getPersonaTodasById(Long personaId, Long connectedUserId)
    {
        return personaDAO.get(PersonaTodas.class, personaId).get(0);
    }

    @Role("ADMIN")
    @Transactional
    public UsuarioExterno insertUsuarioExterno(UsuarioExterno usuarioExterno, Long connectedUserId)
    {
        return personaDAO.insert(usuarioExterno);
    }
}
