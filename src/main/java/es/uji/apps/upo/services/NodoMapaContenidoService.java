package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.ContenidoDAO;
import es.uji.apps.upo.dao.NodoMapaContenidoDAO;
import es.uji.apps.upo.dao.NodoMapaDAO;
import es.uji.apps.upo.exceptions.*;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.EstadoModeracion;
import es.uji.apps.upo.model.enums.TipoReferenciaContenido;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class NodoMapaContenidoService extends AuthenticatedService
{
    private NodoMapaContenidoDAO nodoMapaContenidoDAO;
    private ContenidoDAO contenidoDAO;
    private ContenidoService contenidoService;
    private PersonaService personaService;
    private NotificacionService notificacionService;
    private NodoMapaDAO nodoMapaDAO;

    @Autowired
    public NodoMapaContenidoService(PersonaService personaService, NodoMapaContenidoDAO nodoMapaContenidoDAO,
            ContenidoDAO contenidoDAO, NotificacionService notificacionService, NodoMapaDAO nodoMapaDAO)
    {
        super(personaService);

        this.personaService = personaService;
        this.nodoMapaContenidoDAO = nodoMapaContenidoDAO;
        this.contenidoDAO = contenidoDAO;
        this.notificacionService = notificacionService;
        this.nodoMapaDAO = nodoMapaDAO;
    }

    @Autowired
    public void setContenidoService(ContenidoService contenidoService)
    {
        this.contenidoService = contenidoService;
    }

    private void setDefaultValues(Long personaId, NodoMapa nodoMapa, Contenido contenido)
    {
        if (nodoMapa.getUpoFranquicia() != null)
        {
            contenido.setOtrosAutores(nodoMapa.getUpoFranquicia().getOtrosAutores());
            contenido.setOrigenInformacion(nodoMapa.getUpoFranquicia().getOrigenInformacion());
        }

        contenido.setUpoExtPersona1(personaService.getPersona(personaId));
    }

    private void verificarAdminNodo(Persona persona, NodoMapa nodoMapa)
            throws ActualizadoContenidoNoAutorizadoException
    {
        ParamUtils.checkNotNull(persona);

        if (!personaAdminEnNodo(nodoMapa, persona))
        {
            throw new ActualizadoContenidoNoAutorizadoException();
        }
    }

    @Transactional
    public void insertarContenido(Long personaId, Contenido contenido)
    {
        contenido = contenidoDAO.update(contenido);
        contenidoDAO.registraInsert(contenido, personaId);
    }

    private Contenido verificarYCrearContenido(NodoMapa nodoMapa, NodoMapaContenido nodoMapaContenido, Persona persona)
            throws ActualizadoContenidoNoAutorizadoException, CrearNuevoContenidoException
    {
        verificarAdminNodo(persona, nodoMapa);

        Long contenidoId = nodoMapaContenido.getUpoObjeto().getId();

        Contenido contenido = contenidoService.getContenido(contenidoId, true);
        contenidoService.crearNuevoContenido(contenido);

        setDefaultValues(persona.getId(), nodoMapa, contenido);

        contenido.setUpoMapasObjetos(Collections.singleton(nodoMapaContenido));

        nodoMapaContenido.setUpoObjeto(contenido);

        return contenido;
    }

    public void cambiaTipoLinkToNormal(Long nodoMapaContenidoId, Persona persona)
            throws ActualizadoContenidoNoAutorizadoException, TipoContenidoNoAdecuadoException,
            CrearNuevoContenidoException
    {
        NodoMapaContenido nodoMapaContenido = getNodoMapaContenido(nodoMapaContenidoId);

        if (!TipoReferenciaContenido.LINK.equals(nodoMapaContenido.getTipo()))
        {
            throw new TipoContenidoNoAdecuadoException();
        }

        nodoMapaContenido.setTipo(TipoReferenciaContenido.NORMAL);
        nodoMapaContenido.setEstadoModeracion(EstadoModeracion.ACEPTADO);
        Contenido contenido = verificarYCrearContenido(nodoMapaContenido.getUpoMapa(), nodoMapaContenido, persona);

        insertarContenido(persona.getId(), contenido);
    }

    public void duplicarContenido(long nodoMapaContenidoId, Persona persona)
            throws ActualizadoContenidoNoAutorizadoException, TipoContenidoNoAdecuadoException,
            CrearNuevoContenidoException
    {
        NodoMapaContenido nodoMapaContenido = getNodoMapaContenidoAndFechasEventos(nodoMapaContenidoId);

        if (!TipoReferenciaContenido.NORMAL.equals(nodoMapaContenido.getTipo()))
        {
            throw new TipoContenidoNoAdecuadoException();
        }

        Contenido contenido = verificarYCrearContenido(nodoMapaContenido.getUpoMapa(), nodoMapaContenido, persona);

        nodoMapaContenido.setId(null);
        contenido.setUrlPath("new_" + contenido.getUrlPath());

        nodoMapaContenido.getFechasEvento().forEach(f -> {
            f.setId(null);
        });

        insertarContenido(persona.getId(), contenido);
    }

    public void createNewContenido(NodoMapaContenido nodoMapaContenido, NodoMapa nodoMapa, Persona persona)
            throws ActualizadoContenidoNoAutorizadoException, TipoContenidoNoAdecuadoException,
            CrearNuevoContenidoException
    {
        if (!TipoReferenciaContenido.NORMAL.equals(nodoMapaContenido.getTipo()))
        {
            throw new TipoContenidoNoAdecuadoException();
        }

        Contenido contenido = verificarYCrearContenido(nodoMapa, nodoMapaContenido, persona);
        NodoMapaContenido nodoMapaContenidoConFechasEvento =
                nodoMapaContenidoDAO.getNodoMapaContenidoAndFechasEvento(nodoMapaContenido.getId());

        nodoMapaContenido.setId(null);
        nodoMapaContenido.setUpoMapa(nodoMapa);
        copiaFechasEvento(nodoMapaContenido, nodoMapaContenidoConFechasEvento);

        insertarContenido(persona.getId(), contenido);
    }

    public NodoMapaContenido getNodoMapaContenidoByContenidoIdAndMapaId(Long contenidoId, Long mapaId)
    {
        return nodoMapaContenidoDAO.getNodoMapaContenidoByContenidoIdAndMapaId(contenidoId, mapaId);
    }

    public NodoMapaContenido getNodoMapaContenidoByContenidoIdAndMapaId(NodoMapaContenido nodoMapaContenido)
    {
        return getNodoMapaContenidoByContenidoIdAndMapaId(nodoMapaContenido.getUpoObjeto().getId(),
                nodoMapaContenido.getUpoMapa().getId());
    }

    public List<NodoMapaContenido> getNodoMapasContenidoByMapaId(Long mapaId)
    {
        return nodoMapaContenidoDAO.getNodoMapaContenidoByMapaId(mapaId);
    }

    public NodoMapaContenido getNodoMapaContenido(Long nodoMapaContenidoId)
    {
        List<NodoMapaContenido> list = nodoMapaContenidoDAO.get(NodoMapaContenido.class, nodoMapaContenidoId);

        if (list.size() > 0)
        {
            return list.get(0);
        }

        return new NodoMapaContenido();
    }

    public NodoMapaContenido getNodoMapaContenidoAndFechasEventos(Long nodoMapaContenidoId)
    {
        NodoMapaContenido nodoMapaContenido =
                nodoMapaContenidoDAO.getNodoMapaContenidoAndFechasEvento(nodoMapaContenidoId);

        if (nodoMapaContenido == null)
        {
            return new NodoMapaContenido();
        }

        return nodoMapaContenido;
    }

    @Transactional
    public void update(NodoMapaContenido nodoMapaContenido, Persona persona, NodoMapa nodoMapa)
            throws ActualizadoContenidoNoAutorizadoException
    {
        ParamUtils.checkNotNull(persona);

        if (!personaAdminEnNodo(nodoMapa, persona))
        {
            throw new ActualizadoContenidoNoAutorizadoException();
        }

        nodoMapaContenidoDAO.update(nodoMapaContenido);
    }

    public NodoMapaContenido insertContenidoAModerar(NodoMapaContenido nodoMapaContenido, NodoMapa nodoMapa,
            Long contenidoId, Long nodoMapaContenidoId)
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoYaPropuestoException,
            ContenidoConMismoUrlPathException, InsertadoContenidoNoAutorizadoException
    {
        if (nodoMapaDAO.isNodoMapaLocked(nodoMapa.getUrlCompleta()))
        {
            throw new InsertadoContenidoNoAutorizadoException();
        }

        return insertContenidoAModerar(nodoMapaContenido, nodoMapa, contenidoId, nodoMapaContenidoId, true);
    }

    public NodoMapaContenido insertContenidoAModerar(NodoMapaContenido nodoMapaContenido, NodoMapa nodoMapa,
            Long contenidoId, Long nodoMapaContenidoId, boolean enviaCorreo)
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoYaPropuestoException,
            ContenidoConMismoUrlPathException
    {
        NodoMapaContenido result = null;

        NodoMapaContenido currentNodoMapaContenido = getNodoMapaContenidoAndFechasEventos(nodoMapaContenidoId);

        nodoMapaContenido.setUpoMapa(nodoMapa);
        nodoMapaContenido.setOrden(currentNodoMapaContenido.getOrden());

        copiaFechasEvento(nodoMapaContenido, currentNodoMapaContenido);

        Contenido contenido = new Contenido();
        contenido.setId(contenidoId);
        nodoMapaContenido.setUpoObjeto(contenido);

        NodoMapaContenido nodoMapaContenidoInsertado = getNodoMapaContenidoByContenidoIdAndMapaId(nodoMapaContenido);

        if (nodoMapaContenidoInsertado == null)
        {
            result = insertCuandoContenidoNoExiste(nodoMapaContenido, enviaCorreo);
        }
        else
        {
            result = updateCuandoContenidoExiste(nodoMapaContenidoInsertado, enviaCorreo);
        }

        return result;
    }

    private void copiaFechasEvento(NodoMapaContenido nodoMapaContenido, NodoMapaContenido currentNodoMapaContenido)
    {
        Set<FechaEvento> fechasEvento = new HashSet<>();

        currentNodoMapaContenido.getFechasEvento().stream().forEach(f -> {
            f.setNodoMapaContenido(nodoMapaContenido);
            f.setId(null);
            fechasEvento.add(f);
        });

        nodoMapaContenido.setFechasEvento(fechasEvento);
    }

    public NodoMapaContenido updateCuandoContenidoExiste(NodoMapaContenido nodoMapaContenidoInsertado,
            boolean enviaCorreo)
            throws ContenidoYaPropuestoException
    {
        if (EstadoModeracion.RECHAZADO.equals(nodoMapaContenidoInsertado.getEstadoModeracion()))
        {
            boolean isAdminEnNodo = personaAdminEnNodo(nodoMapaContenidoInsertado.getUpoMapa(),
                    nodoMapaContenidoInsertado.getPersonaPropuesta());

            nodoMapaContenidoInsertado.setEstadoModeracion(
                    isAdminEnNodo ? EstadoModeracion.ACEPTADO : EstadoModeracion.PENDIENTE);

            if (!isAdminEnNodo && nodoMapaContenidoInsertado.getPersonaPropuesta() != null && enviaCorreo)
            {
                notificacionService.enviarCorreoAAdministradores(nodoMapaContenidoInsertado.getUpoMapa(),
                        nodoMapaContenidoInsertado.getPersonaPropuesta().getId());
            }
        }
        else
        {
            throw new ContenidoYaPropuestoException();
        }

        return nodoMapaContenidoDAO.update(nodoMapaContenidoInsertado);
    }

    public NodoMapaContenido insertCuandoContenidoNoExiste(NodoMapaContenido nodoMapaContenido, boolean enviaCorreo)
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoConMismoUrlPathException
    {
        NodoMapaContenido nodoMapaContenidoNormal = nodoMapaContenidoDAO.getNodoMapaContenidoTipoNormalByContenidoId(
                nodoMapaContenido.getUpoObjeto().getId());

        if (nodoMapaContenido.isNormal() && nodoMapaContenidoNormal.getId() != null)
        {
            throw new NodoMapaContenidoDebeTenerUnContenidoNormalException();
        }

        if (!nodoMapaContenido.isNormal() && nodoMapaContenidoNormal.getId() == null)
        {
            throw new NodoMapaContenidoDebeTenerUnContenidoNormalException();
        }

        Contenido contenidoOriginal = contenidoService.getContenido(nodoMapaContenido.getUpoObjeto().getId());

        contenidoService.verificarUrlPathNoRepetidoEnOtrosContenidos(nodoMapaContenido.getUpoMapa(),
                contenidoOriginal.getUrlPath());

        if (personaAdminEnNodo(nodoMapaContenido.getUpoMapa(), nodoMapaContenido.getPersonaPropuesta()))
        {
            nodoMapaContenido.setEstadoModeracion(EstadoModeracion.ACEPTADO);
        }
        else if (nodoMapaContenido.getPersonaPropuesta() != null && enviaCorreo)
        {
            notificacionService.enviarCorreoAAdministradores(nodoMapaContenido.getUpoMapa(),
                    nodoMapaContenido.getPersonaPropuesta().getId());
        }

        return nodoMapaContenidoDAO.insert(nodoMapaContenido);
    }
}
