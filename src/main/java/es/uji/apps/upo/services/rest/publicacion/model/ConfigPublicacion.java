package es.uji.apps.upo.services.rest.publicacion.model;

import es.uji.apps.upo.model.enums.IdiomaPublicacion;

public class ConfigPublicacion
{
    private String formato;
    private String plantilla;
    private String idioma;

    public ConfigPublicacion()
    {
        this.idioma = IdiomaPublicacion.CA.toString();
    }

    public String getFormato()
    {
        return formato;
    }

    public void setFormato(String formato)
    {
        this.formato = formato;
    }

    public String getPlantilla()
    {
        return plantilla;
    }

    public void setPlantilla(String plantilla)
    {
        this.plantilla = plantilla;
    }

    public String getIdioma()
    {
        return idioma;
    }

    public void setIdioma(String idioma)
    {
        this.idioma = idioma;
    }
}



