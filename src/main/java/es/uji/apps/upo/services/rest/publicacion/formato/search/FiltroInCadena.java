package es.uji.apps.upo.services.rest.publicacion.formato.search;

import es.uji.commons.rest.StringUtils;

import java.util.StringTokenizer;

public class FiltroInCadena extends Filtro<String>
{
    private StringTokenizer st;

    public FiltroInCadena(String clave, String valor)
    {
        super(clave, Util.replaceForbiddenChars(StringUtils.limpiaAcentos(valor.toLowerCase())));
    }

    @Override
    public String generate()
    {
        st = new StringTokenizer(valor, ",");
        String sql = "(" + clave + " in (";

        while (st.hasMoreTokens())
        {
            sql += "'" + st.nextToken().trim() + "'";

            if (st.hasMoreTokens())
            {
                sql += ",";
            }
        }

        sql += "))";

        return sql;
    }
}