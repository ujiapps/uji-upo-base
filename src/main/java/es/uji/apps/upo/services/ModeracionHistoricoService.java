package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.ModeracionDAO;
import es.uji.apps.upo.model.ModeracionHistorico;
import es.uji.apps.upo.model.ModeracionMapaObjeto;
import es.uji.apps.upo.model.NodoMapaContenido;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ModeracionHistoricoService
{
    private ModeracionDAO moderacionDAO;

    @Autowired
    public ModeracionHistoricoService(ModeracionDAO moderacionDAO)
    {
        this.moderacionDAO = moderacionDAO;
    }

    @Transactional
    public ModeracionHistorico registraHistoricoModeracion(NodoMapaContenido nodoMapaContenido)
    {
        ModeracionHistorico historico = moderacionDAO.obtenerElementoHistoricoModeracion(nodoMapaContenido.getId());
        ModeracionMapaObjeto mapaObjeto = moderacionDAO.obtenerModeracionMapaObjeto(nodoMapaContenido.getId());

        if (historico == null)
        {
            return moderacionDAO.insert(actualizaHistorico(historico, mapaObjeto));
        }

        return moderacionDAO.update(actualizaHistorico(historico, mapaObjeto));
    }

    private ModeracionHistorico actualizaHistorico(ModeracionHistorico historico, ModeracionMapaObjeto mapaObjeto)
    {
        if (historico == null)
        {
            historico = new ModeracionHistorico();
        }

        historico.setId(mapaObjeto.getId());
        historico.setMapaObjetoId(mapaObjeto.getMapaObjetoId());
        historico.setObjetoId(mapaObjeto.getObjetoId());
        historico.setMapaId(mapaObjeto.getMapaId());
        historico.setEstadoModeracion(mapaObjeto.getEstadoModeracion());
        historico.setPerIdModeracion(mapaObjeto.getPerIdModeracion());
        historico.setPerIdPropuesta(mapaObjeto.getPerIdPropuesta());
        historico.setPropuestaExterna(mapaObjeto.getPropuestaExterna());
        historico.setTextoRechazo(mapaObjeto.getTextoRechazo());
        historico.setObjetoUrlPath(mapaObjeto.getObjetoUrlPath());
        historico.setMapaUrlCompleta(mapaObjeto.getMapaUrlCompleta());
        historico.setUrlCompleta(mapaObjeto.getUrlCompleta());
        historico.setObjetoTitulo(mapaObjeto.getObjetoTitulo());

        return historico;
    }
}
