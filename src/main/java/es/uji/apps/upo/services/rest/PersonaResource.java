package es.uji.apps.upo.services.rest;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.upo.model.PersonaTodas;
import es.uji.apps.upo.model.UsuarioExterno;
import es.uji.apps.upo.services.PersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("persona")
public class PersonaResource extends CoreBaseService
{
    @InjectParam
    private PersonaService personaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPersonas(@QueryParam("franquiciaId") Long franquiciaId)
    {
        if (franquiciaId == null)
        {
            return UIEntity.toUI(personaService.getPersonas());
        }

        return UIEntity.toUI(personaService.getPersonasByFranquiciaIdNoAsignados(franquiciaId));
    }

    @GET
    @Path("/usuario-externo")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getUsuariosExternos()
    {
        return UIEntity.toUI(personaService.getUsuariosExternos(AccessManager.getConnectedUserId(request)));
    }

    @DELETE
    @Path("/usuario-externo/{personaId}")
    public void deleteUsuarioExterno(@PathParam("personaId") Long personaId)
    {
        Long userId = AccessManager.getConnectedUserId(request);
        personaService.deleteUsuarioExterno(personaId, userId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/usuario-externo")
    public UIEntity addPerfil(UIEntity entity)
    {
        Long userId = AccessManager.getConnectedUserId(request);
        String personaId = entity.get("personaId");

        ParamUtils.checkNotNull(personaId);

        PersonaTodas persona = personaService.getPersonaTodasById(ParamUtils.parseLong(personaId), userId);

        UsuarioExterno usuarioExterno = new UsuarioExterno();
        usuarioExterno.setPersonaTodas(persona);

        personaService.insertUsuarioExterno(usuarioExterno, userId);

        return UIEntity.toUI(persona);
    }
}
