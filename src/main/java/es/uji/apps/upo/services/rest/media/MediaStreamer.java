package es.uji.apps.upo.services.rest.media;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

public class MediaStreamer implements StreamingOutput
{
    private final int HTTP_CHUNK_SIZE = 1024 * 1024;

    private int from;
    private int to;

    private byte[] partialContent;
    private int totalLength;

    public MediaStreamer(String range, byte[] content)
    {
        this.totalLength = content.length;

        String[] ranges = range.split("=")[1].split("-");

        from = Integer.parseInt(ranges[0]);
        to = HTTP_CHUNK_SIZE + from;

        if (to >= content.length)
        {
            to = (int) (content.length - 1);
        }

        if (ranges.length == 2)
        {
            to = Integer.parseInt(ranges[1]);
        }

        this.partialContent = Arrays.copyOfRange(content, from, to + 1);
    }

    @Override
    public void write(OutputStream outputStream) throws IOException, WebApplicationException
    {
        outputStream.write(partialContent);
    }

    public String getResponseRange()
    {
        return String.format("bytes %d-%d/%d", from, to, totalLength);
    }

    public int getLenth()
    {
        return partialContent.length;
    }
}