package es.uji.apps.upo.services.rest.publicacion.formato.search;

public class RevistaSearchBuilder extends SearchBuilder
{
    public RevistaSearchBuilder()
    {
        super(null, null);
    }

    public String generate()
    {
        return cabecera() + generaCondicionesCuerpo() + pie();
    }

    private String generaCondicionesCuerpo()
    {
        if (expresionesCuerpo.size() == 0)
        {
            return "";
        }

        AndExpression andExpression = new AndExpression();
        andExpression.expressions = expresionesCuerpo;

        return " and " + andExpression.generate();
    }

    private String cabecera()
    {
        return "select MAPA_URL_COMPLETA from UPO_VW_REVISTA where 1=1 ";
    }

    private String pie()
    {
        return " order by primera_fecha_vigencia desc";
    }
}
