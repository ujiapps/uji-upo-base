package es.uji.apps.upo.services.rest.publicacion;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.accessibility.AccesibilityChecker;
import es.uji.apps.upo.model.Autoguardado;
import es.uji.apps.upo.model.Publicacion;
import es.uji.apps.upo.services.AutoguardadoService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

public class PublicacionAccesibilidadService extends PublicacionBaseService
{
    @InjectParam
    private AccesibilityChecker accesibilityChecker;

    @InjectParam
    private AutoguardadoService autoguardadoService;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String publicaContenidoComoPaginaParaAccesibilidad(@Context HttpServletRequest request,
            @Context HttpServletResponse response, @QueryParam("mapaId") String mapaId,
            @QueryParam("contenidoId") String contenidoId, @QueryParam("idioma") String codigoIdiomaISO,
            @QueryParam("personaId") String personaId)
            throws IOException
    {
        Long contenidoIdParsed = ((contenidoId != null && !contenidoId.isEmpty()) ? Long.parseLong(contenidoId) : null);

        Autoguardado contenidoAutoguardado =
                autoguardadoService.getAutoguardadoMasActualByParameters(Long.parseLong(mapaId), contenidoIdParsed,
                        Long.parseLong(personaId), codigoIdiomaISO);

        String contenido = "";

        if (contenidoAutoguardado != null)
        {
            contenido = contenidoAutoguardado.getContenidoEditora();
        }
        else
        {
            contenido = getContenidoByIdAndIdioma(contenidoId, codigoIdiomaISO);
        }

        return accesibilityChecker.buildAccesiblePageFromContent(contenido);
    }

    private String getContenidoByIdAndIdioma(String contenidoId, String codigoIdiomaISO)
    {
        String contenido = "";

        Publicacion contenidoIdioma =
                publicacionService.getContenidoIdiomaByContenidoIdAndLanguage(Long.parseLong(contenidoId),
                        codigoIdiomaISO);

        if (contenidoIdioma != null)
        {
            contenido = new String(contenidoIdioma.getContenido());
        }

        return contenido;
    }
}
