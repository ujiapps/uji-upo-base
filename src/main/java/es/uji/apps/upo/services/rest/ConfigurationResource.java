package es.uji.apps.upo.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.exceptions.GeneralUPOException;
import es.uji.apps.upo.model.ClientConfiguration;
import es.uji.apps.upo.model.Permiso;
import es.uji.apps.upo.services.AuthenticatedService;
import es.uji.apps.upo.services.ConfigurationService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.User;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("configuration")
public class ConfigurationResource extends CoreBaseService
{
    @InjectParam
    private ConfigurationService configurationService;

    @GET
    public UIEntity get()
    {
        User connectedUser = AccessManager.getConnectedUser(request);
        Long personaId = connectedUser.getId();

        UIEntity uiEntity = new UIEntity();

        uiEntity.put("userAuthenticatedId", personaId);

        uiEntity.put("applicationAdmin", configurationService.isAdmin(personaId));
        uiEntity.put("documentalista", configurationService.isDocumentalista(personaId));
        uiEntity.put("hasFranquicia", configurationService.hasFranquicia(personaId));

        uiEntity.put("host", configurationService.getHost());
        uiEntity.put("hostHttp", configurationService.getHostHttp());
        uiEntity.put("heraEditoraUrl", configurationService.getHeraEditoraUrl());

        return uiEntity;
    }
}