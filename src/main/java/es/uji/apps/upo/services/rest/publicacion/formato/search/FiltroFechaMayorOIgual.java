package es.uji.apps.upo.services.rest.publicacion.formato.search;

import java.time.LocalDate;

public class FiltroFechaMayorOIgual extends Filtro
{
    private LocalDate fechaInicio;

    public FiltroFechaMayorOIgual(String clave, LocalDate fechaInicio)
    {
        super(clave, null);

        this.fechaInicio = fechaInicio;
    }

    @Override
    public String generate()
    {
        String fechaInicioFormateada = getFormattedDate(fechaInicio);

        return "(trunc(to_date('" + fechaInicioFormateada + "', 'dd/mm/rrrr')) <= trunc(" + clave + "))";
    }
}
