package es.uji.apps.upo.services.rest.publicacion.formato.rss;

import java.util.Date;
import java.util.List;

import es.uji.apps.upo.services.rest.publicacion.formato.Metadato;

public class ContenidoRSS
{
    private Long contenidoId;
    private String titulo;
    private String tituloLargo;
    private Date fechaModificacion;
    private String urlCompleta;
    private String autor;
    private String urlBase;
    private String origen;
    private String lugar;
    private String subtitulo;
    private String idioma;
    private String resumen;
    private String contenido;
    private List<Metadato> metadatos;
    private List<Enclosure> enclosures;
    private String esHtml;

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getTituloLargo()
    {
        return tituloLargo;
    }

    public void setTituloLargo(String tituloLargo)
    {
        this.tituloLargo = tituloLargo;
    }

    public Date getFechaModificacion()
    {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion)
    {
        this.fechaModificacion = fechaModificacion;
    }

    public String getAutor()
    {
        return autor;
    }

    public void setAutor(String autor)
    {
        this.autor = autor;
    }

    public String getUrlCompleta()
    {
        return urlCompleta;
    }

    public void setUrlCompleta(String urlCompleta)
    {
        this.urlCompleta = urlCompleta;
    }

    public String getUrlBase()
    {
        return urlBase;
    }

    public void setUrlBase(String urlBase)
    {
        this.urlBase = urlBase;
    }

    public String getOrigen()
    {
        return origen;
    }

    public void setOrigen(String origen)
    {
        this.origen = origen;
    }

    public String getLugar()
    {
        return lugar;
    }

    public void setLugar(String lugar)
    {
        this.lugar = lugar;
    }

    public String getIdioma()
    {
        return idioma;
    }

    public void setIdioma(String idioma)
    {
        this.idioma = idioma;
    }

    public String getSubtitulo()
    {
        return subtitulo;
    }

    public void setSubtitulo(String subtitulo)
    {
        this.subtitulo = subtitulo;
    }

    public String getResumen()
    {
        return resumen;
    }

    public void setResumen(String resumen)
    {
        this.resumen = resumen;
    }

    public String getContenido()
    {
        return contenido;
    }

    public void setContenido(String contenido)
    {
        this.contenido = contenido;
    }

    public List<Metadato> getMetadatos()
    {
        return metadatos;
    }

    public void setMetadatos(List<Metadato> metadatos)
    {
        this.metadatos = metadatos;
    }

    public List<Enclosure> getEnclosures()
    {
        return enclosures;
    }

    public void setEnclosures(List<Enclosure> enclosures)
    {
        this.enclosures = enclosures;
    }

    public Long getContenidoId()
    {
        return contenidoId;
    }

    public void setContenidoId(Long contenidoId)
    {
        this.contenidoId = contenidoId;
    }

    public String getEsHtml()
    {
        return esHtml;
    }

    public void setEsHtml(String esHtml)
    {
        this.esHtml = esHtml;
    }
}
