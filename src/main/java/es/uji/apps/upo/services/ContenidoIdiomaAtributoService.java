package es.uji.apps.upo.services;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.upo.dao.ContenidoIdiomaAtributoDAO;
import es.uji.apps.upo.model.ContenidoIdioma;
import es.uji.apps.upo.model.ContenidoIdiomaAtributo;

@Service
public class ContenidoIdiomaAtributoService
{
    private ContenidoIdiomaAtributoDAO contenidoIdiomaAtributoDAO;

    @Autowired
    public ContenidoIdiomaAtributoService(ContenidoIdiomaAtributoDAO contenidoIdiomaAtributoDAO)
    {
        this.contenidoIdiomaAtributoDAO = contenidoIdiomaAtributoDAO;
    }

    @Transactional
    public void deleteByContenido(Long contenidoId)
    {
        contenidoIdiomaAtributoDAO.deleteByContenido(contenidoId);
    }

    public List<ContenidoIdiomaAtributo> getContenidoIdiomaAtributosByContenidoIdioma(
            ContenidoIdioma contenidoIdioma)
    {
        return contenidoIdiomaAtributoDAO
                .getContenidoIdiomaAtributosUrlByContenidoIdioma(contenidoIdioma);
    }
}
