package es.uji.apps.upo.services.nodomapa;

import es.uji.apps.upo.dao.ContenidoDAO;
import es.uji.apps.upo.dao.NodoMapaDAO;
import es.uji.apps.upo.exceptions.*;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.*;
import es.uji.apps.upo.services.*;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

@Service
public class EstructuraFechaService extends AuthenticatedService
{
    NodoMapaService nodoMapaService;
    PlantillaService plantillaService;
    NodoMapaPlantillaService nodoMapaPlantillaService;
    NodoMapaDAO nodoMapaDAO;
    private PersonaService personaService;

    @Autowired
    public EstructuraFechaService(NodoMapaService nodoMapaService, PlantillaService plantillaService,
            NodoMapaPlantillaService nodoMapaPlantillaService, NodoMapaDAO nodoMapaDAO, PersonaService personaService)
    {
        super(personaService);

        this.nodoMapaService = nodoMapaService;
        this.plantillaService = plantillaService;
        this.nodoMapaPlantillaService = nodoMapaPlantillaService;
        this.nodoMapaDAO = nodoMapaDAO;
        this.personaService = personaService;
    }


    public void insertaConEstructuraDeFecha(Persona persona, NodoMapa nodoMapa, String mes, String anyo)
            throws UsuarioNoAutenticadoException, NodoConNombreYaExistenteEnMismoPadreException,
            InsertadoContenidoNoAutorizadoException, InsertarEstructuraDeFechaEnNodoMapaException
    {
        ParamUtils.checkNotNull(persona);

        if (!personaAdminEnNodo(nodoMapa, persona))
        {
            throw new InsertadoContenidoNoAutorizadoException();
        }

        if (anyo != null && !anyo.isEmpty() && Integer.parseInt(anyo) >= 2000 && Integer.parseInt(anyo) <= 2100)
        {
            if (mes != null && !mes.isEmpty() && (Integer.parseInt(mes) < 1 || Integer.parseInt(mes) > 12))
            {
                throw new InsertarEstructuraDeFechaEnNodoMapaException(
                        "Si se especifica un mes debe estar entre 1 y 12");
            }

            nodoMapaService.comprobarQueNoHayaNodosHijoConElMimoNombre(nodoMapa, anyo);
            insertaConEstructuraDeAnyoYMes(nodoMapa, anyo, mes);
        }
        else
        {
            throw new InsertarEstructuraDeFechaEnNodoMapaException(
                    "El año no puede estar vacío y comprendido entre 2000 y 2100");
        }
    }

    @Transactional
    private void insertaConEstructuraDeAnyoYMes(NodoMapa parentNodoMapa, String anyo, String mes)
    {
        NodoMapa newNodoMapa = new NodoMapa();
        newNodoMapa.setNodoMapaDAO(nodoMapaDAO);

        newNodoMapa.setUrlPath(anyo);
        newNodoMapa.setUrlCompleta(parentNodoMapa.getUrlCompleta() + anyo + '/');

        List<NodoMapaPlantilla> nodoMapaPlantillasOriginal =
                nodoMapaPlantillaService.getNodoMapaPlantillaByNodoMapaId(parentNodoMapa.getId());

        NodoMapaPlantilla nodoMapaPlantillaOriginal =
                nodoMapaPlantillasOriginal.size() > 0 ? nodoMapaPlantillasOriginal.get(0) : null;

        newNodoMapa.setCommonInheritedfields(parentNodoMapa, newNodoMapa, nodoMapaPlantillaOriginal);

        nodoMapaDAO.insert(newNodoMapa);
        nodoMapaDAO.flush();

        if (mes != null && !mes.isEmpty())
        {
            insertaConEstrucutraDeMes(newNodoMapa, mes, anyo, nodoMapaPlantillaOriginal);
        }
        else
        {
            for (Integer it = 1; it <= 12; it++)
            {
                insertaConEstrucutraDeMes(newNodoMapa, it.toString(), anyo, nodoMapaPlantillaOriginal);
            }
        }
    }

    @Transactional
    private void insertaConEstrucutraDeMes(NodoMapa parentNodoMapa, String mes, String anyo,
            NodoMapaPlantilla nodoMapaPlantillaOriginal)
    {
        NodoMapa newNodoMapa = new NodoMapa();
        newNodoMapa.setNodoMapaDAO(nodoMapaDAO);

        String mesFormateado = (mes.length() == 2) ? mes : '0' + mes;

        newNodoMapa.setUrlPath(mesFormateado);
        newNodoMapa.setUrlCompleta(parentNodoMapa.getUrlCompleta() + mesFormateado + '/');

        newNodoMapa.setCommonInheritedfields(parentNodoMapa, newNodoMapa, nodoMapaPlantillaOriginal);

        nodoMapaDAO.insert(newNodoMapa);
        nodoMapaDAO.flush();

        Long plantillaId = null;
        Integer plantillaNivel = null;

        if (nodoMapaPlantillaOriginal != null)
        {
            plantillaId = nodoMapaPlantillaOriginal.getUpoPlantilla().getId();
            plantillaNivel = nodoMapaPlantillaOriginal.getNivel();
        }

        nodoMapaDAO.insertarDiasDelMesComoNodoMapas(newNodoMapa, mes, anyo, plantillaId, plantillaNivel);
    }
}
