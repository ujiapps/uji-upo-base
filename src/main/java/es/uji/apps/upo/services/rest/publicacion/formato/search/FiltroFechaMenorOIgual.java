package es.uji.apps.upo.services.rest.publicacion.formato.search;

import java.time.LocalDate;

public class FiltroFechaMenorOIgual extends Filtro
{
    public FiltroFechaMenorOIgual(String clave, LocalDate fechaFin)
    {
        super(clave, null);

        this.fechaFin = fechaFin;
    }

    @Override
    public String generate()
    {
        String fechaFinFormateada = getFormattedDate(fechaFin);

        return "(trunc(to_date('" + fechaFinFormateada + "', 'dd/mm/rrrr')) >= trunc(" + clave + "))";
    }
}
