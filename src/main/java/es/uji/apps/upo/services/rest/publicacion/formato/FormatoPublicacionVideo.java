package es.uji.apps.upo.services.rest.publicacion.formato;

import es.uji.apps.upo.model.PublicacionBinarios;
import es.uji.apps.upo.services.rest.media.MediaStreamer;
import es.uji.apps.upo.services.rest.publicacion.model.RequestParams;
import org.springframework.stereotype.Component;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.text.ParseException;

@Component
public class FormatoPublicacionVideo extends AbstractPublicacionHtml implements FormatoPublicacion
{
    @Override
    public Response publica(RequestParams requestParams)
            throws MalformedURLException, ParseException, UnsupportedEncodingException
    {
        PublicacionBinarios contenido = publicacionService.getBinarioByUrlCompletaAndLanguage(requestParams.getUrl(),
                requestParams.getConfigPublicacion().getIdioma());

        if (contenido == null)
        {
            return new PaginaNoEncontrada().publica(requestParams);
        }

        return streamContent(contenido.getMimeType(), contenido.getContenido(),
                requestParams.getSearchParams().getRange());
    }

    private Response streamContent(final String mimeType, final byte[] contenido, String range)
    {
        if (range == null)
        {
            return Response.ok(new StreamingOutput()
            {
                @Override
                public void write(OutputStream outputStream)
                        throws IOException, WebApplicationException
                {
                    outputStream.write(contenido);
                }
            })
                    .status(200)
                    .header(HttpHeaders.CONTENT_TYPE, mimeType)
                    .header(HttpHeaders.CONTENT_LENGTH, contenido.length)
                    .build();
        }

        MediaStreamer streamer = new MediaStreamer(range, contenido);

        return Response.ok(streamer)
                .status(206)
                .header("Accept-Ranges", "bytes")
                .header("Content-Range", streamer.getResponseRange())
                .header(HttpHeaders.CONTENT_TYPE, mimeType)
                .header(HttpHeaders.CONTENT_LENGTH, streamer.getLenth())
                .build();
    }
}