package es.uji.apps.upo.services.rest.publicacion.formato;

import com.sun.syndication.io.FeedException;
import es.uji.apps.upo.services.rest.publicacion.formato.search.CondicionFiltro;
import es.uji.apps.upo.services.rest.publicacion.model.RequestParams;

import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

public interface FormatoPublicacion
{
    Response publica(RequestParams requestParams)
            throws MalformedURLException, ParseException, FeedException, UnsupportedEncodingException,
            URISyntaxException;
}
