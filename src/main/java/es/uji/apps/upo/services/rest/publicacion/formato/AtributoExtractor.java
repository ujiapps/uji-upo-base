package es.uji.apps.upo.services.rest.publicacion.formato;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import es.uji.apps.upo.model.Publicacion;

@Component
public class AtributoExtractor
{
    public List<Atributo> extrae(Publicacion contenido)
    {
        List<Atributo> atributos = new ArrayList<Atributo>();

        String atributo = contenido.getAtributos();

        if (atributo == null)
        {
            return atributos;
        }

        String[] fieldList = atributo.split("~~~");

        for (String field : fieldList)
        {
            String[] record = field.split("@@@");

            String key = record[0];
            String value = record[1];

            atributos.add(new Atributo(key, value));
        }

        return atributos;
    }
}
