package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.ContenidoLogDAO;
import es.uji.apps.upo.model.ContenidoLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ContenidoLogService
{
    private final ContenidoLogDAO contenidoLogDAO;

    @Autowired
    public ContenidoLogService(ContenidoLogDAO contenidoLogDAO)
    {
        this.contenidoLogDAO = contenidoLogDAO;
    }

    @Transactional
    public void insertar(ContenidoLog contenidoLog)
    {
        contenidoLogDAO.insert(contenidoLog);
    }
}
