package es.uji.apps.upo.services.rest.publicacion.formato;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import es.uji.apps.upo.model.Publicacion;

@Component
public class MetadataExtractor
{
    public List<Metadato> extrae(Publicacion contenido)
    {
        List<Metadato> metadatos = new ArrayList<Metadato>();
        String metadata = contenido.getMetadata();

        if (metadata == null)
        {
            return metadatos;
        }

        String[] fieldList = metadata.split("~~~");

        for (String field : fieldList)
        {
            String[] record = field.split("@@@");

            String key = record[0];
            String titulo = record[1];
            String value = record[2];
            String publicable = record[3];

            metadatos.add(new Metadato(key, titulo, value, "0".equals(publicable) ? false : true));
        }

        return metadatos;
    }
}
