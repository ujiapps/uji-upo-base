package es.uji.apps.upo.services.rest.publicacion.formato;

import java.util.Date;

public class FechaEvento
{
    private Date fecha;
    private Date horaInicio;
    private Date horaFin;

    public FechaEvento(Date fecha, Date horaInicio, Date horaFin)
    {
        this.fecha = fecha;
        this.horaInicio = horaInicio;
        this.horaFin = horaFin;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Date getHoraInicio()
    {
        return horaInicio;
    }

    public void setHoraInicio(Date horaInicio)
    {
        this.horaInicio = horaInicio;
    }

    public Date getHoraFin()
    {
        return horaFin;
    }

    public void setHoraFin(Date horaFin)
    {
        this.horaFin = horaFin;
    }
}
