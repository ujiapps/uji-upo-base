package es.uji.apps.upo.services.rest.metadatos;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.upo.exceptions.*;
import es.uji.apps.upo.model.Persona;
import es.uji.apps.upo.model.metadatos.ContenidoMetadato;
import es.uji.apps.upo.services.ContenidoMetadatoService;
import es.uji.apps.upo.services.ContenidoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

public class ContenidoMetadatosResource extends CoreBaseService
{
    @PathParam("id")
    private String contenidoId;

    @InjectParam
    ContenidoMetadatoService contenidoMetadatoService;

    @InjectParam
    ContenidoService contenidoService;

    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<UIEntity> getMetadatos()
    {
        ParamUtils.checkNotNull(contenidoId);

        List<ContenidoMetadato> metadatos =
                contenidoMetadatoService.getMetadatosByContenido(ParamUtils.parseLong(contenidoId));
        Long esquemaId = contenidoMetadatoService.getEsquemaIdByMetadatos(metadatos);

        List<UIEntity> entidades = UIEntity.toUI(metadatos);

        if (esquemaId != null)
        {
            UIEntity entidad = new UIEntity();

            entidad.put("esquemaId", esquemaId);

            entidades.add(entidad);
        }

        return entidades;
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_XML)
    public ResponseMessage insertMetadato(MultivaluedMap<String, String> params)
            throws IdiomaObligatorioException, UsuarioNoAutenticadoException, ActualizadoContenidoNoAutorizadoException,
            AccesibilidadException, ContenidoConMismoUrlPathException, LongitudMaximaExcedidaEnMetadatosException
    {
        contenidoMetadatoService.insert(params, ParamUtils.parseLong(contenidoId), getPersona());

        return new ResponseMessage(true);
    }

    @DELETE
    public void deleteEsquemaMetadatos()
            throws IdiomaObligatorioException, UsuarioNoAutenticadoException, ActualizadoContenidoNoAutorizadoException,
            AccesibilidadException, ContenidoConMismoUrlPathException
    {
        contenidoMetadatoService.delete(ParamUtils.parseLong(contenidoId), getPersona());
    }

    @PUT
    @Path("{esquemaId}")
    public void updateEsquemaMetadatos(@PathParam("esquemaId") Long esquemaId)
            throws IdiomaObligatorioException, UsuarioNoAutenticadoException, ActualizadoContenidoNoAutorizadoException,
            AccesibilidadException, ContenidoConMismoUrlPathException
    {
        contenidoMetadatoService.update(esquemaId, ParamUtils.parseLong(contenidoId), getPersona());
    }

    private Persona getPersona()
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona();
        persona.setId(personaId);
        return persona;
    }
}
