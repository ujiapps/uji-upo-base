package es.uji.apps.upo.services.rest.publicacion.formato.search;

import java.time.LocalDate;

public abstract class Filtro<T> extends Expression
{
    protected String clave;
    protected T valor;
    protected LocalDate fechaInicio;
    protected LocalDate fechaFin;

    public Filtro(String clave, T valor)
    {
        this.clave = clave;
        this.valor = valor;
    }

    public LocalDate getFechaInicio()
    {
        return fechaInicio;
    }

    public void setFechaInicio(LocalDate fechaInicio)
    {
        this.fechaInicio = fechaInicio;
    }

    public LocalDate getFechaFin()
    {
        return fechaFin;
    }

    public void setFechaFin(LocalDate fechaFin)
    {
        this.fechaFin = fechaFin;
    }
}
