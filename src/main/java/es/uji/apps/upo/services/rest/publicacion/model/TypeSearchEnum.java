package es.uji.apps.upo.services.rest.publicacion.model;

public enum TypeSearchEnum
{
    NORMAL, REVISTA
}
