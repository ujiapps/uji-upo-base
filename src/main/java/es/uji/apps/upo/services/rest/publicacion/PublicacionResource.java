package es.uji.apps.upo.services.rest.publicacion;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.exceptions.EstructuraDeLosParametrosException;
import es.uji.apps.upo.exceptions.RedireccionNoPermitidaException;
import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.services.rest.publicacion.formato.FormatoPublicacion;
import es.uji.apps.upo.services.rest.publicacion.formato.FormatoPublicacionFactory;
import es.uji.apps.upo.services.rest.publicacion.formato.FormatoPublicacionUtils;
import es.uji.apps.upo.services.rest.publicacion.formato.search.CondicionFiltro;
import es.uji.apps.upo.services.rest.publicacion.model.RequestParams;
import es.uji.apps.upo.services.rest.publicacion.model.TypeSearchEnum;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

@Path("publicacion")
public class PublicacionResource extends PublicacionBaseService
{
    private static final int DURACION_UN_ANYO = 31536000;
    public static final String METADATO_PREFIX = "$M";
    public static final String FIELD_PREFIX = "$F";
    public static final String CABECERA_PREFIX = "$C";
    public static final String SEPARADOR_CAMPOS_AGRUPADOS = "@";

    @Context
    protected HttpServletResponse response;

    @InjectParam
    private FormatoPublicacionFactory formatoPublicacionFactory;

    @InjectParam
    private UrlNormalizer urlNormalizer;

    @InjectParam
    private FormatoPublicacionUtils formatoPublicacionUtils;

    @Path("accesibilidad")
    public PublicacionAccesibilidadService getPublicacionAccesibilidadService(
            @InjectParam PublicacionAccesibilidadService publicacionAccesibilidadService)
    {
        return publicacionAccesibilidadService;
    }

    @GET
    @Path("idioma")
    public Response getIdioma( @CookieParam("uji-lang") @DefaultValue("ca") String idioma)
    {
        return Response.ok(idioma).build();
    }

    @GET
    @Path("idioma/{codigoISO}")
    public Response cambioIdioma(@PathParam("codigoISO") String codigoISO, @QueryParam("urlRedirect") String url,
            @Context UriInfo uriInfo)
            throws URISyntaxException, RedireccionNoPermitidaException
    {
        if (!(new URI(url).getHost().toLowerCase().endsWith("uji.es")) && !(new URI(url).getHost().toLowerCase().equals("localhost")))
        {
            throw new RedireccionNoPermitidaException();
        }

        Cookie cookie = new Cookie("uji-lang", codigoISO.toLowerCase());
        cookie.setPath("/");
        cookie.setDomain(".uji.es");
        cookie.setMaxAge(DURACION_UN_ANYO);

        response.addCookie(cookie);

        return Response.temporaryRedirect(new URI(url + "?" + uriInfo.getRequestUri().getQuery())).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response postToPublicacion(@QueryParam("url") String url,
            @CookieParam("uji-lang") @DefaultValue("ca") String idioma, @QueryParam("idioma") String idiomaForzado,
            @HeaderParam("Range") String range, @QueryParam("startSearch") Integer startSearch,
            @QueryParam("pageSearch") Integer pageSearch, @QueryParam("numResultados") Integer numResultados,
            @QueryParam("orderSearch") String orderSearch, @QueryParam("typeSearch") String typeSearch,
            @QueryParam("formato") @DefaultValue("html") String formato, @QueryParam("p") String plantilla,
            @Context UriInfo uriInfo, MultivaluedMap<String, String> formParams, @Context Request request)
            throws Exception
    {
        return publica(url, idioma, idiomaForzado, range, startSearch, pageSearch, numResultados, orderSearch,
                typeSearch, formato, plantilla, uriInfo, prepareParameters(formParams));
    }

    @GET
    public Response publicacion(@QueryParam("url") String url,
            @CookieParam("uji-lang") @DefaultValue("ca") String idioma, @QueryParam("idioma") String idiomaForzado,
            @HeaderParam("Range") String range, @QueryParam("startSearch") Integer startSearch,
            @QueryParam("pageSearch") Integer pageSearch, @QueryParam("numResultados") Integer numResultados,
            @QueryParam("orderSearch") String orderSearch, @QueryParam("typeSearch") String typeSearch,
            @QueryParam("formato") @DefaultValue("html") String formato, @QueryParam("p") String plantilla,
            @Context UriInfo uriInfo)
            throws Exception
    {
        return publica(url, idioma, idiomaForzado, range, startSearch, pageSearch, numResultados, orderSearch,
                typeSearch, formato, plantilla, uriInfo, new HashMap());
    }

    private Response publica(String url, String idioma, String idiomaForzado, String range, Integer startSearch,
            Integer pageSearch, Integer numResultados, String orderSearch, String typeSearch, String formato,
            String plantilla, UriInfo uriInfo, Map<String, String> formParams)
            throws Exception
    {
        String idiomaPublicacion = (idiomaForzado != null) ? idiomaForzado : idioma;
        String urlNormalizada = urlNormalizer.normalize(url);
        Boolean nodoPaginado = false;
        RequestParams requestParams = new RequestParams(getUrlBase(request), urlNormalizada);
        MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();

        if (formatoPublicacionUtils.necesitaAplicarPlantilla(urlNormalizada))
        {
            NodoMapa nodoMapa = publicacionService.getNodoMapaByUrlCompleta(urlNormalizada);
            requestParams.setNodoMapa(nodoMapa);

            if (nodoMapa != null) nodoPaginado = nodoMapa.isPaginado();
        }

        requestParams.getConfigPublicacion().setFormato(formato);
        requestParams.getConfigPublicacion().setPlantilla(plantilla);
        requestParams.getConfigPublicacion().setIdioma(idiomaPublicacion);

        requestParams.getRequestDetails().setQueryString(uriInfo.getRequestUri().getRawQuery());
        requestParams.getRequestDetails().setCookies(getCookies(request));
        requestParams.getRequestDetails().setFormParams(formParams);
        requestParams.getRequestDetails().setQueryParams(prepareParameters(queryParams));

        if (nodoPaginado && pageSearch == null) pageSearch = 1;

        requestParams.getSearchParams().setNumResultados(numResultados);
        requestParams.getSearchParams().setStartSearch(startSearch);
        requestParams.getSearchParams().setPageSearch(pageSearch);
        requestParams.getSearchParams().setRange(range);
        requestParams.getSearchParams().setOrden(orderSearch);
        requestParams.getSearchParams().setFiltrosCabecera(getQueryParams(queryParams, CABECERA_PREFIX));
        requestParams.getSearchParams().setFiltrosCuerpo(getQueryParams(queryParams, FIELD_PREFIX));
        requestParams.getSearchParams().setFiltrosMetadatos(getQueryParams(queryParams, METADATO_PREFIX));

        if (typeSearch != null)
        {
            requestParams.getSearchParams().setTypeSearch(TypeSearchEnum.valueOf(typeSearch.toUpperCase()));
        }

        FormatoPublicacion formatoPublicacion = formatoPublicacionFactory.getFormat(formato, urlNormalizada,
                necesitaPaginar(startSearch, pageSearch, nodoPaginado));

        return formatoPublicacion.publica(requestParams);
    }

    private Map<String, String> prepareParameters(MultivaluedMap<String, String> queryParameters)
            throws UnsupportedEncodingException
    {
        Map<String, String> parameters = new HashMap();

        Iterator<String> it = queryParameters.keySet().iterator();

        while (it.hasNext())
        {
            String theKey = (String) it.next();
            parameters.put(theKey, queryParameters.getFirst(theKey));
            //parameters.put(theKey, new String(queryParameters.getFirst(theKey).getBytes("ISO-8859-1"), "UTF-8"));
        }

        return parameters;
    }

    private boolean necesitaPaginar(Integer startSearch, Integer pageSearch, Boolean nodoPaginado)
    {
        return startSearch != null || pageSearch != null || nodoPaginado;
    }

    private List<CondicionFiltro> getQueryParams(MultivaluedMap<String, String> queryParams, String prefijo)
            throws EstructuraDeLosParametrosException
    {
        List<CondicionFiltro> condiciones = new ArrayList<>();

        Iterator it = queryParams.keySet().iterator();

        while (it.hasNext())
        {
            String key = (String) it.next();

            if (key.startsWith(prefijo))
            {
                addCondiciones(queryParams, condiciones, key);
            }
        }

        return condiciones;
    }

    private void addCondiciones(MultivaluedMap<String, String> queryParams, List<CondicionFiltro> condiciones,
            String key)
            throws EstructuraDeLosParametrosException
    {
        if (key.contains(SEPARADOR_CAMPOS_AGRUPADOS))
        {
            String[] grupos = key.split(SEPARADOR_CAMPOS_AGRUPADOS);

            for (String grupo : grupos)
            {
                addCondicion(grupo, queryParams.getFirst(key), key, condiciones);
            }
        }
        else
        {
            addCondicion(key, queryParams.getFirst(key), key, condiciones);
        }
    }

    private void addCondicion(String name, String value, String key, List<CondicionFiltro> condiciones)
            throws EstructuraDeLosParametrosException
    {
        if (value != null && !value.isEmpty())
        {
            condiciones.add(new CondicionFiltro(name, value, key));
        }
    }
}