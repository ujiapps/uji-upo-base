package es.uji.apps.upo.services.rest;

import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.upo.model.MigracionUrlsPer;
import es.uji.apps.upo.services.MigracionUrlPerService;
import es.uji.apps.upo.services.PersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Path("permisos-migracion")
public class MigracionUrlPerResource extends CoreBaseService
{
    @InjectParam
    private MigracionUrlPerService migracionUrlPerService;
    @InjectParam
    private PersonaService personaService;

    @GET
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getPermisos()
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(migracionUrlPerService.getPermisos(personaId));
    }

    @POST
    @Consumes(MediaType.TEXT_XML)
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> insert(UIEntity item) throws UnauthorizedUserException
    {
        if (!ParamUtils.isNotNull(item.get("personaId"), item.get("urlBase")))
        {
            return Collections.EMPTY_LIST;
        }

        Long userId = AccessManager.getConnectedUserId(request);

        MigracionUrlsPer migracionUrlsPer = new MigracionUrlsPer();

        migracionUrlsPer.setPersona(personaService.getPersona(ParamUtils.parseLong(item
                .get("personaId"))));
        migracionUrlsPer.setUrlBase(item.get("urlBase"));

        return Collections.singletonList(UIEntity.toUI(migracionUrlPerService.insertPermisos(
                migracionUrlsPer, userId)));
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.TEXT_XML)
    @Consumes(MediaType.TEXT_XML)
    public List<UIEntity> update(@PathParam("id") String id, UIEntity item)
    {
        ParamUtils.checkNotNull(item.get("personaId"), item.get("urlBase"));

        Long userId = AccessManager.getConnectedUserId(request);

        MigracionUrlsPer migracionUrlsPer = migracionUrlPerService.getById(
                ParamUtils.parseLong(id), userId);

        migracionUrlsPer.setPersona(personaService.getPersona(ParamUtils.parseLong(item
                .get("personaId"))));
        migracionUrlsPer.setUrlBase(item.get("urlBase"));

        return Collections.singletonList(UIEntity.toUI(migracionUrlPerService.updatePermisos(
                migracionUrlsPer, userId)));
    }

    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") String id)
    {
        ParamUtils.checkNotNull(id);

        Long userId = AccessManager.getConnectedUserId(request);

        migracionUrlPerService.deletePermisos(ParamUtils.parseLong(id), userId);
    }
}
