package es.uji.apps.upo.services;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import es.uji.apps.upo.dao.ItemDAO;
import es.uji.apps.upo.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.upo.dao.GrupoDAO;
import es.uji.apps.upo.model.Grupo;
import es.uji.apps.upo.model.GrupoItem;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.exceptions.RegistroDuplicadoException;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Service
public class GrupoService
{
    private GrupoDAO grupoDAO;
    private ItemDAO itemDAO;
    private PersonaService personaService;

    @Autowired
    public GrupoService(GrupoDAO grupoDAO, ItemDAO itemDAO, PersonaService personaService)
    {
        this.grupoDAO = grupoDAO;
        this.itemDAO = itemDAO;
        this.personaService = personaService;
    }

    public List<Grupo> getGrupos(Long connectedUserId)
    {
        if (personaService.isAdmin(connectedUserId))
        {
            return grupoDAO.get(Grupo.class);
        }

        return grupoDAO.getByPersonaId(connectedUserId);
    }

    public List<Grupo> getGruposByMenuNoAsignados(Long menuId, Long connectedUserId)
    {
        if (personaService.isAdmin(connectedUserId))
        {
            return grupoDAO.getGruposByMenuNoAsignados(menuId);
        }

        return grupoDAO.getGruposByMenuNoAsignadosByPersonaId(menuId, connectedUserId);
    }

    public Grupo insert(Grupo grupo)
    {
        return grupoDAO.insert(grupo);
    }

    public Grupo update(Grupo grupo, Long connectedUserId)
            throws UnauthorizedUserException
    {
        if (personaAutorizada(connectedUserId, grupoDAO.get(Grupo.class, grupo.getId()).get(0)))
        {
            return grupoDAO.update(grupo);
        }

        throw new UnauthorizedUserException();
    }

    public void delete(Long id, Long connectedUserId)
            throws UnauthorizedUserException
    {
        if (personaAutorizada(connectedUserId, grupoDAO.get(Grupo.class, id).get(0)))
        {
            grupoDAO.delete(Grupo.class, id);
            return;
        }

        throw new UnauthorizedUserException();
    }

    private Boolean personaAutorizada(Long connectedUserId, Grupo grupo)
    {
        return personaService.isAdmin(connectedUserId) || (grupo.getPersona() != null && connectedUserId.equals(
                grupo.getPersona().getId()));
    }

    private Boolean personaAutorizada(Long connectedUserId, Grupo grupo, Item item)
    {
        return personaAutorizada(connectedUserId, grupo) && personaService.isAdmin(
                connectedUserId) || (item.getPersona() != null && connectedUserId.equals(item.getPersona().getId()));
    }

    public List<GrupoItem> getItems(Long grupoId)
    {
        return grupoDAO.getItemsByGrupoId(grupoId);
    }

    public void addItem(Long grupoId, Long itemId, Integer orden, Long connectedUserId)
            throws RegistroDuplicadoException, UnauthorizedUserException
    {
        if (personaAutorizada(connectedUserId, grupoDAO.get(Grupo.class, grupoId).get(0),
                itemDAO.get(Item.class, itemId).get(0)))
        {
            try
            {
                grupoDAO.addItem(grupoId, itemId, orden);
                return;
            } catch (Exception e)
            {
                throw new RegistroDuplicadoException();
            }
        }

        throw new UnauthorizedUserException();
    }

    public void updateItem(long grupoId, long itemId, Integer orden, Long connectedUserId)
            throws UnauthorizedUserException
    {
        if (personaAutorizada(connectedUserId, grupoDAO.get(Grupo.class, grupoId).get(0),
                itemDAO.get(Item.class, itemId).get(0)))
        {
            grupoDAO.updateItem(grupoId, itemId, orden);
            return;
        }

        throw new UnauthorizedUserException();
    }

    public void deleteItem(Long grupoId, Long itemId, Long connectedUserId)
            throws UnauthorizedUserException
    {
        if (personaAutorizada(connectedUserId, grupoDAO.get(Grupo.class, grupoId).get(0),
                itemDAO.get(Item.class, itemId).get(0)))
        {
            grupoDAO.deleteItem(grupoId, itemId);
            return;
        }

        throw new UnauthorizedUserException();
    }
}
