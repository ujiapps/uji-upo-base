package es.uji.apps.upo.services.rest.publicacion.formato.search;

public class FiltroNumerico extends Filtro<Integer>
{
    public FiltroNumerico(String clave, Integer valor)
    {
        super(clave, valor);
    }

    @Override
    public String generate()
    {
        return "(" + clave + " = " + valor + ")";
    }
}