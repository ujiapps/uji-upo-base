package es.uji.apps.upo.services.rest.publicacion.formato.search;

import es.uji.apps.upo.services.rest.publicacion.model.RequestParams;
import org.springframework.stereotype.Component;

@Component
public class SearchFactory
{
    public static SearchBuilder get(RequestParams requestParams)
    {
        switch (requestParams.getSearchParams().getTypeSearch())
        {
            case NORMAL:
                return new NormalSearchBuilder(requestParams);

            case REVISTA:
                return new RevistaSearchBuilder();
        }

        return null;
    }
}
