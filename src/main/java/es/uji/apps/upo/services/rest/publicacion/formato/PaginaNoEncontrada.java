package es.uji.apps.upo.services.rest.publicacion.formato;

import es.uji.apps.upo.services.rest.publicacion.model.RequestParams;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;
import es.uji.commons.web.template.model.Menu;
import es.uji.commons.web.template.model.Pagina;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.Locale;

@Component
public class PaginaNoEncontrada
{
    public Response publica(RequestParams requestParams)
            throws MalformedURLException, ParseException, UnsupportedEncodingException
    {
        Pagina pagina = new Pagina(requestParams.getUrlBase(), requestParams.getUrl(),
                requestParams.getConfigPublicacion().getIdioma(), "NOT FOUND");

        pagina.setMenu(new Menu());

        Template template = new HTMLTemplate("upo/2021/page-not-found",
                new Locale(requestParams.getConfigPublicacion().getIdioma().toLowerCase()), "upo");

        template.put("pagina", pagina);
        template.put("urlBase", requestParams.getUrl());

        return Response.ok(template).status(Response.Status.NOT_FOUND).type(MediaType.TEXT_HTML).build();
    }
}
