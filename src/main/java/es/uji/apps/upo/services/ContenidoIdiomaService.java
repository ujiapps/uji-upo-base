package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.ContenidoDAO;
import es.uji.apps.upo.dao.ContenidoIdiomaDAO;
import es.uji.apps.upo.exceptions.ActualizadoContenidoNoAutorizadoException;
import es.uji.apps.upo.exceptions.ImportarDocumentoException;
import es.uji.apps.upo.exceptions.UsuarioNoAutenticadoException;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.TipoAccionCrearContenidoDesdeRecurso;
import es.uji.apps.upo.model.enums.TipoFormatoContenido;
import es.uji.apps.upo.storage.RecursoReservori;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StreamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

@Service
public class ContenidoIdiomaService extends AuthenticatedService
{
    private static final String UPDATE_RECORD = "U";

    private ContenidoDAO contenidoDAO;
    private ContenidoIdiomaDAO contenidoIdiomaDAO;
    private AutoguardadoService autoguardadoService;
    private ContenidoService contenidoService;
    private String host;

    @Autowired
    public ContenidoIdiomaService(PersonaService personaService, ContenidoIdiomaDAO contenidoIdiomaDAO,
            ContenidoDAO contenidoDAO, AutoguardadoService autoguardadoService, ContenidoService contenidoService,
            @Value("${uji.webapp.host}") String host)
    {
        super(personaService);

        this.contenidoDAO = contenidoDAO;
        this.contenidoIdiomaDAO = contenidoIdiomaDAO;
        this.autoguardadoService = autoguardadoService;
        this.contenidoService = contenidoService;
        this.host = host;
    }

    @Transactional
    public void borraAdjunto(Persona persona, ContenidoIdioma contenidoIdioma)
            throws UsuarioNoAutenticadoException, ActualizadoContenidoNoAutorizadoException
    {
        ParamUtils.checkNotNull(persona, contenidoIdioma.getUpoObjeto());

        Contenido contenido = contenidoIdioma.getUpoObjeto();

        if (!personaAdminEnNodo(contenido.getNodoMapa(), persona))
        {
            throw new ActualizadoContenidoNoAutorizadoException();
        }

        contenidoIdioma.setContenido(null);
        contenidoIdioma.setMimeType(MediaType.APPLICATION_OCTET_STREAM);
        contenidoIdioma.setNombreFichero(null);
        contenidoIdioma.setFechaModificacion(new Date());
        contenidoIdioma.setLongitud(0L);
        contenidoIdioma.setIdDescarga(null);

        contenidoIdiomaDAO.update(contenidoIdioma);
        contenidoDAO.registrarContenidoIdiomaLog(UPDATE_RECORD, persona.getId(), contenidoIdioma);
    }

    //TODO: Todo mezclado
    public Map<String, Object> getContenidoComprobadoConAutoguardados(Long personaId, ContenidoIdioma contenidoIdioma)
    {
        Contenido contenido = contenidoService.getContenido(contenidoIdioma.getUpoObjeto().getId());
        Long mapaId = contenido.getNodoMapa().getId();

        Map<String, Object> returnValues = new HashMap<String, Object>();

        Autoguardado autoguardado =
                autoguardadoService.getAutoguardadoMasActualByParameters(mapaId, contenido.getId(), personaId,
                        contenidoIdioma.getUpoIdioma().getCodigoISO());

        if (autoguardado != null && autoguardado.getFecha().after(contenidoIdioma.getFechaModificacion()))
        {
            returnValues.put("contenido", autoguardado.getContenidoEditora());
            returnValues.put("contenidoProvenienteDeAutoguardado", true);

            return returnValues;
        }

        if (contenidoIdioma.getContenido() != null && contenidoIdioma.getContenido().length > 0)
        {
            returnValues.put("contenido", new String(contenidoIdioma.getContenido()));
            returnValues.put("contenidoProvenienteDeAutoguardado", false);

            return returnValues;
        }

        return null;
    }

    public ResultadoBusquedaBinarios getImagesBySearchAndUrlCompleta(ParametrosBusquedaBinarios parametros)
    {
        List<BinarioBusqueda> binarios = contenidoIdiomaDAO.getBinariosBySearchAndUrlCompleta(parametros, true);
        Long numResultados = contenidoIdiomaDAO.getNumBinariosBySearchAndUrlCompleta(parametros, true);

        return buildRespuestaBinariosBusqueda(binarios, numResultados);
    }

    public ResultadoBusquedaBinarios getBinariosBySearchAndUrlCompleta(ParametrosBusquedaBinarios parametros)
    {
        List<BinarioBusqueda> binarios = contenidoIdiomaDAO.getBinariosBySearchAndUrlCompleta(parametros, false);
        Long numResultados = contenidoIdiomaDAO.getNumBinariosBySearchAndUrlCompleta(parametros, false);

        return buildRespuestaBinariosBusqueda(binarios, numResultados);
    }

    private ResultadoBusquedaBinarios buildRespuestaBinariosBusqueda(List<BinarioBusqueda> binarios, Long numResultados)
    {
        ResultadoBusquedaBinarios resultadoBusqueda = new ResultadoBusquedaBinarios();
        List<RecursoReservori> resultados = new ArrayList<>();

        for (BinarioBusqueda resultado : binarios)
        {
            inserta(resultado, resultados);
        }

        resultadoBusqueda.setNumResultados(numResultados);
        resultadoBusqueda.setBinarios(resultados);

        return resultadoBusqueda;
    }

    private void inserta(BinarioBusqueda resultado, List<RecursoReservori> resultados)
    {
        RecursoReservori recurso = new RecursoReservori();

        String urlDescarga =
                this.host + "/upo/rest/contenido/" + resultado.getObjetoId() + "/raw?idioma=" + resultado.getIdiomaCodigoIso();

        recurso.setId(resultado.getContenidoIdiomaId().toString());
        recurso.setNombre(resultado.getFicheroNombre());
        recurso.setTypeMime(resultado.getFicheroMimeType());
        recurso.setUrl(urlDescarga);
        recurso.setUrlThumbnail(urlDescarga);
        recurso.setUrlRedirect(urlDescarga);

        if (resultado.getDescargaId() != null)
        {
            String urlRedirect = "https://ujiapps.uji.es/ade/rest/storage/" + resultado.getDescargaId();

            recurso.setUrlRedirect(urlRedirect);
            recurso.setUrlThumbnail(urlRedirect + "?t=SCALE,h=100");
        }

        resultados.add(recurso);
    }

    public InputStream getStreamById(Long id)
    {
        return contenidoIdiomaDAO.getStreamById(id);
    }


    public String preparaContenidosIdioma(String idiomaCodigoISO, Set<ContenidoIdioma> contenidosIdiomas, String url,
            TipoAccionCrearContenidoDesdeRecurso tipo)
            throws IOException, ImportarDocumentoException
    {
        String fileName = null;

        for (ContenidoIdioma contenidoIdioma : contenidosIdiomas)
        {
            contenidoIdioma.setContenidoIdiomaRecursos(null);
            contenidoIdioma.setFechaModificacion(new Date());

            if (!idiomaCodigoISO.equals(contenidoIdioma.getUpoIdioma().getCodigoISO().toUpperCase()))
            {
                setContenidoIdiomaBinarioAVacio(contenidoIdioma);
            }
            else
            {
                fileName = setContenidoIdiomaWithRecursoImportado(url, contenidoIdioma, tipo);
            }
        }

        return fileName;
    }

    private String setContenidoIdiomaWithRecursoImportado(String url, ContenidoIdioma contenidoIdioma,
            TipoAccionCrearContenidoDesdeRecurso tipo)
            throws IOException, ImportarDocumentoException
    {
        contenidoIdioma.setHtml("N");
        contenidoIdioma.setTipoFormato(TipoFormatoContenido.BINARIO.toString());

        if (url.startsWith("//"))
        {
            url = "http:" + url;
        }

        if (url.startsWith("/"))
        {
            url = host + url;
        }

        URL urlParsed = new URL(url);
        URLConnection urlConnection = urlParsed.openConnection();
        HttpURLConnection httpConnection = (HttpURLConnection) urlConnection;

        if (httpConnection.getResponseCode() != 200)
        {
            throw new ImportarDocumentoException("Errada al importar un document amb url: " + url);
        }

        String fileName = urlParsed.getFile().substring(urlParsed.getFile().lastIndexOf("/") + 1);
        fileName = fileName.replace("?" + urlParsed.getQuery(), "");

        if (tipo.equals(TipoAccionCrearContenidoDesdeRecurso.COPIAR))
        {
            InputStream urlContent = urlParsed.openStream();
            String contentTypeRecurso = urlConnection.getContentType();

            byte[] dataBinary = StreamUtils.inputStreamToByteArray(urlContent);
            contenidoIdioma.setContenido(dataBinary);
            contenidoIdioma.setMimeType(contentTypeRecurso.split(";")[0]);
            contenidoIdioma.setNombreFichero(fileName);
            contenidoIdioma.setLongitud(Long.valueOf(dataBinary.length));

            httpConnection.disconnect();
        }
        else
        {
            contenidoIdioma.setEnlaceDestino(url);
            contenidoIdioma.setMimeType(MediaType.APPLICATION_OCTET_STREAM);
            contenidoIdioma.setContenido(null);
            contenidoIdioma.setLongitud(0L);
        }

        return fileName;
    }

    private void setContenidoIdiomaBinarioAVacio(ContenidoIdioma contenidoIdioma)
    {
        contenidoIdioma.setTitulo(null);
        contenidoIdioma.setTituloLargo(null);
        contenidoIdioma.setContenido(null);
        contenidoIdioma.setResumen(null);
        contenidoIdioma.setMimeType(MediaType.APPLICATION_OCTET_STREAM);
        contenidoIdioma.setHtml("N");
        contenidoIdioma.setTipoFormato(TipoFormatoContenido.BINARIO.toString());
        contenidoIdioma.setNombreFichero(null);
        contenidoIdioma.setSubtitulo(null);
    }

}