package es.uji.apps.upo.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.model.Idioma;
import es.uji.apps.upo.services.IdiomaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Path("idioma")
public class IdiomaResource extends CoreBaseService
{
    @InjectParam
    private IdiomaService idiomaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getIdioma()
    {
        return UIEntity.toUI(idiomaService.getIdiomas());
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> insert(UIEntity idioma) throws UnauthorizedUserException
    {
        if (idioma == null || idioma.get("nombre") == null)
        {
            return new ArrayList<UIEntity>();
        }

        Idioma idiomaDB = idioma.toModel(Idioma.class);
        Long personaId = AccessManager.getConnectedUserId(request);

        return Collections.singletonList(UIEntity.toUI(idiomaService.insert(idiomaDB, personaId)));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> update(@PathParam("id") String id, UIEntity idioma)
            throws UnauthorizedUserException
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        Idioma idiomaModel = idioma.toModel(Idioma.class);

        idiomaService.update(idiomaModel, personaId);

        return Collections.singletonList(idioma);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long id) throws UnauthorizedUserException
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        idiomaService.delete(id, personaId);
    }
}