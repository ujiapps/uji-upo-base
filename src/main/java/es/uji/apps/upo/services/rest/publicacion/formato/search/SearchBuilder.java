package es.uji.apps.upo.services.rest.publicacion.formato.search;

import java.util.ArrayList;
import java.util.List;

public abstract class SearchBuilder
{
    protected String urlCompleta;
    protected String idioma;
    protected List<Expression> expresionesCabecera;
    protected List<Expression> expresionesCuerpo;
    protected List<Expression> expresionesMetadatos;

    public SearchBuilder(String urlCompleta, String idioma)
    {
        this.urlCompleta = urlCompleta;
        this.idioma = idioma;

        this.expresionesCabecera = new ArrayList<>();
        this.expresionesCuerpo = new ArrayList<>();
        this.expresionesMetadatos = new ArrayList<>();
    }

    public void addExpresionCabecera(Expression expresion)
    {
        this.expresionesCabecera.add(expresion);
    }

    public void addExpresionCuerpo(Expression expresion)
    {
        this.expresionesCuerpo.add(expresion);
    }

    public void addExpresionMetadatos(Expression expresion)
    {
        this.expresionesMetadatos.add(expresion);
    }

    public abstract String generate();
}
