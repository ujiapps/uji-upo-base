package es.uji.apps.upo.services;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.upo.dao.ItemDAO;
import es.uji.apps.upo.model.Item;
import es.uji.commons.rest.Role;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Service
public class ItemService
{
    private ItemDAO itemDAO;
    private PersonaService personaService;

    @Autowired
    public ItemService(ItemDAO itemDAO, PersonaService personaService)
    {
        this.itemDAO = itemDAO;
        this.personaService = personaService;
    }

    public Item insert(Item item)
    {
        return itemDAO.insert(item);
    }

    public Item update(Item item, Long connectedUserId)
            throws UnauthorizedUserException
    {
        if (personaAutorizada(connectedUserId, getItem(item.getId())))
        {
            return itemDAO.update(item);
        }

        throw new UnauthorizedUserException();
    }

    public void delete(Long id, Long connectedUserId)
            throws UnauthorizedUserException
    {
        if (personaAutorizada(connectedUserId, getItem(id)))
        {
            itemDAO.delete(Item.class, id);
            return;
        }

        throw new UnauthorizedUserException();
    }

    private Boolean personaAutorizada(Long connectedUserId, Item item)
    {
        return personaService.isAdmin(connectedUserId) || (item.getPersona() != null && connectedUserId.equals(
                item.getPersona().getId()));
    }

    public Item getItem(Long id)
    {
        List<Item> listaItems = itemDAO.get(Item.class, id);

        if (listaItems.size() > 0)
        {
            return listaItems.get(0);
        }

        return new Item();
    }

    public List<Item> getItems(Long connectedUserId)
    {
        if (personaService.isAdmin(connectedUserId))
        {
            return itemDAO.get(Item.class);
        }

        return itemDAO.getByPersonaId(connectedUserId);
    }

    public List<Item> getItemsByGrupoId(Long grupoId)
    {
        List<Item> items = itemDAO.getItemsByGrupoId(grupoId);

        Set setItems = new LinkedHashSet(items);
        items.clear();
        items.addAll(setItems);

        return items;
    }

    public List<Item> getItemsByGrupoNoAsignados(Long grupoId, Long connectedUserId)
    {
        if (personaService.isAdmin(connectedUserId))
        {
            return itemDAO.getItemsByGrupoNoAsignados(grupoId);
        }

        return itemDAO.getItemsByGrupoNoAsignadosByPersonaId(grupoId, connectedUserId);
    }
}
