package es.uji.apps.upo.services.rest.publicacion.formato;

import es.uji.commons.web.template.model.Miga;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MigasExtractor
{
    public List<Miga> extrae(String migasCodificado)
    {
        List<Miga> migas = new ArrayList<>();

        if (migasCodificado == null)
        {
            return migas;
        }

        String[] fieldList = migasCodificado.split("~~~");

        for (String field : fieldList)
        {
            String[] record = field.split("@@@");

            String titulo = record[0];
            String url = record[1];

            migas.add(new Miga(titulo, url));
        }

        return migas;
    }
}
