package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.MigracionUrlsPerDAO;
import es.uji.apps.upo.model.MigracionUrlsPer;
import es.uji.commons.rest.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MigracionUrlPerService
{
    private MigracionUrlsPerDAO migracionUrlsPerDAO;

    @Autowired
    public MigracionUrlPerService(MigracionUrlsPerDAO migracionUrlsPerDAO)
    {
        this.migracionUrlsPerDAO = migracionUrlsPerDAO;
    }

    @Role("ADMIN")
    public List<MigracionUrlsPer> getPermisos(Long connectedUserId)
    {
        return migracionUrlsPerDAO.get(MigracionUrlsPer.class);
    }

    @Role("ADMIN")
    @Transactional
    public MigracionUrlsPer insertPermisos(MigracionUrlsPer migracionUrlsPer,
                                           Long connectedUserId)
    {
        return migracionUrlsPerDAO.insert(migracionUrlsPer);
    }

    @Role("ADMIN")
    @Transactional
    public void deletePermisos(Long id, Long connectedUserId)
    {
        migracionUrlsPerDAO.delete(MigracionUrlsPer.class, id);
    }

    @Role("ADMIN")
    public MigracionUrlsPer getById(Long id, Long connectedUserId)
    {
        return migracionUrlsPerDAO.get(MigracionUrlsPer.class, id).get(0);
    }

    @Role("ADMIN")
    @Transactional
    public MigracionUrlsPer updatePermisos(MigracionUrlsPer migracionUrlsPer,
                                           Long connectedUserId)
    {
        return migracionUrlsPerDAO.update(migracionUrlsPer);
    }
}
