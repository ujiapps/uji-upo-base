package es.uji.apps.upo.services.rest.publicacion.formato;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class FormatoPublicacionUtils
{
    public boolean necesitaAplicarPlantilla(String url)
    {
        String urlPath = url;

        if (url.contains("/"))
        {
            urlPath = url.substring(url.lastIndexOf("/"));
        }

        return !urlPath.contains(".");
    }
}
