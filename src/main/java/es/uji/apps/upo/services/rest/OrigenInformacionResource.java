package es.uji.apps.upo.services.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.upo.model.OrigenInformacion;
import es.uji.apps.upo.services.OrigenInformacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Path("origen")
public class OrigenInformacionResource extends CoreBaseService
{
    @InjectParam
    OrigenInformacionService origenInformacionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getOrigenesInformacion(@CookieParam("uji-lang") @DefaultValue("ca") String idioma)
    {
        return UIEntity.toUI(origenInformacionService.getOrigenesInformacion());
    }

    @GET
    @Path("catalogo")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getOrigenesInformacionParaCatalogo(@CookieParam("uji-lang") @DefaultValue("ca") String idioma)
    {
        List<OrigenInformacion> origenes =
                origenInformacionService.getOrigenesInformacionUsadosEnCatalogoDeProcedimientos();

        for (OrigenInformacion origen : origenes)
        {
            if ("es".equalsIgnoreCase(idioma) && origen.getNombreEs() != null)
            {
                origen.setNombre(origen.getNombreEs());
            }

            if ("en".equalsIgnoreCase(idioma) && origen.getNombreEn() != null)
            {
                origen.setNombre(origen.getNombreEn());
            }
        }

        return UIEntity.toUI(origenes);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> insert(UIEntity uiEntity)
            throws UnauthorizedUserException
    {
        Long userId = AccessManager.getConnectedUserId(request);

        if (uiEntity.get("nombre") == null) return new ArrayList<UIEntity>();

        OrigenInformacion origenInformacion = uiEntity.toModel(OrigenInformacion.class);

        return Collections.singletonList(UIEntity.toUI(origenInformacionService.insert(origenInformacion, userId)));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> update(UIEntity uiEntity)
            throws UnauthorizedUserException
    {
        Long userId = AccessManager.getConnectedUserId(request);

        if (uiEntity.get("nombre") == null) return new ArrayList<UIEntity>();

        OrigenInformacion origenInformacion = uiEntity.toModel(OrigenInformacion.class);
        origenInformacion = origenInformacionService.update(origenInformacion, userId);

        return Collections.singletonList(UIEntity.toUI(origenInformacion));
    }

    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") String id)
            throws UnauthorizedUserException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        origenInformacionService.delete(Long.parseLong(id), userId);
    }
}