package es.uji.apps.upo.services.rest.publicacion.model;

import es.uji.apps.upo.services.rest.publicacion.formato.search.CondicionFiltro;

import java.util.ArrayList;
import java.util.List;

public class SearchParams
{
    private List<CondicionFiltro> filtrosMetadatos;
    private List<CondicionFiltro> filtrosCabecera;
    private List<CondicionFiltro> filtrosCuerpo;
    private String orden;
    private String range;
    private Integer startSearch;
    private Integer pageSearch;
    private Integer numResultados;
    private TypeSearchEnum typeSearch;

    public SearchParams()
    {
        this.filtrosCabecera = new ArrayList<>();
        this.filtrosMetadatos = new ArrayList<>();
        this.filtrosCuerpo = new ArrayList<>();
        this.typeSearch = TypeSearchEnum.NORMAL;
    }

    public List<CondicionFiltro> getFiltrosMetadatos()
    {
        return filtrosMetadatos;
    }

    public void setFiltrosMetadatos(List<CondicionFiltro> filtrosMetadatos)
    {
        this.filtrosMetadatos = filtrosMetadatos;
    }

    public List<CondicionFiltro> getFiltrosCabecera()
    {
        return filtrosCabecera;
    }

    public void setFiltrosCabecera(List<CondicionFiltro> filtrosCabecera)
    {
        this.filtrosCabecera = filtrosCabecera;
    }

    public List<CondicionFiltro> getFiltrosCuerpo()
    {
        return filtrosCuerpo;
    }

    public void setFiltrosCuerpo(List<CondicionFiltro> filtrosCuerpo)
    {
        this.filtrosCuerpo = filtrosCuerpo;
    }

    public String getRange()
    {
        return range;
    }

    public void setRange(String range)
    {
        this.range = range;
    }

    public Integer getStartSearch()
    {
        return startSearch;
    }

    public void setStartSearch(Integer startSearch)
    {
        this.startSearch = startSearch;
    }

    public Integer getNumResultados()
    {
        return numResultados;
    }

    public void setNumResultados(Integer numResultados)
    {
        this.numResultados = numResultados;
    }

    public String getOrden()
    {
        return orden;
    }

    public void setOrden(String orden)
    {
        this.orden = orden;
    }

    public TypeSearchEnum getTypeSearch()
    {
        return typeSearch;
    }

    public void setTypeSearch(TypeSearchEnum typeSearch)
    {
        if (typeSearch != null)
        {
            this.typeSearch = typeSearch;
        }
    }

    public Integer getPageSearch()
    {
        return pageSearch;
    }

    public void setPageSearch(Integer pageSearch)
    {
        this.pageSearch = pageSearch;
    }
}

