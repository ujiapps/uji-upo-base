package es.uji.apps.upo.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.model.Fichero;
import es.uji.apps.upo.model.FicheroRevista;
import es.uji.apps.upo.model.Revista;
import es.uji.apps.upo.services.PlantillaService;
import es.uji.apps.upo.services.RevistaService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.web.template.PDFTemplate;
import es.uji.commons.web.template.Template;
import es.uji.commons.web.template.model.Pagina;
import es.uji.commons.web.template.model.Recurso;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Service
@Path("revista")
public class RevistaResource
{
    @InjectParam
    private RevistaService revistaService;

    @InjectParam
    private PlantillaService plantillaService;

    private DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> searchRevista(@FormParam("titulo") String titulo, @FormParam("resumen") String resumen,
            @FormParam("resumenOP") String resumenOP, @FormParam("palabrasClave") String palabrasClave,
            @FormParam("palabrasClaveOP") String palabrasClaveOP, @FormParam("seccion") String seccion,
            @FormParam("medio") String medio, @FormParam("fechaInicio") String fechaInicio,
            @FormParam("fechaFin") String fechaFin)
    {
        List<UIEntity> entities = new ArrayList<>();

        for (Revista result : revistaService.doSearch(titulo, resumen, resumenOP, palabrasClave, palabrasClaveOP,
                seccion, medio, stringToDate(fechaInicio), stringToDate(fechaFin)))
        {
            UIEntity entity = new UIEntity();

            entity.put("titulo", result.getTituloLargo());
            entity.put("medio", result.getOtrosAutores());
            entity.put("resumen", result.getResumen());
            entity.put("url", revistaService.getServer() + result.getUrlCompleta());
            entity.put("fecha", df.format(result.getPrimeraFechaVigencia()));

            entities.add(entity);
        }

        return entities;
    }

    @GET
    @Path("{id}/binary")
    public Response getBinary(@PathParam("id") Long id)
    {
        ParamUtils.checkNotNull(id);

        Fichero fichero = revistaService.getFichero(id);

        if (fichero == null)
        {
            return null;
        }

        return Response.ok(fichero.getContenido())
                .type(fichero.getMimeType())
                .header("Content-Disposition", "inline; filename=\"" + fichero.getNombre() + "\"")
                .build();
    }

    @GET
    @Path("{id}")
    public Response getRevistaPDF(@PathParam("id") Long id, @QueryParam("revistaId") Long revistaId)
            throws ParseException
    {
        ParamUtils.checkNotNull(id, revistaId);

        Fichero fichero = revistaService.getFichero(id);

        if (fichero == null)
        {
            return null;
        }

        if (fichero.getMimeType().equalsIgnoreCase("application/pdf"))
        {
            return Response.ok(fichero.getContenido())
                    .type(fichero.getMimeType())
                    .header("Content-Disposition", "inline; filename=\"" + fichero.getNombre() + "\"")
                    .build();
        }

        FicheroRevista revista = revistaService.getRevista(revistaId);
        Template template = new PDFTemplate("upo/" + "page-pdf", new Locale("ca"), "upo");
        Pagina pagina = new Pagina("", "", "ca", fichero.getNombre());

        Recurso recurso = new Recurso("", fichero.getContenido());
        recurso.setAutor(revista.getAutor());
        recurso.setPrimeraFechaVigencia(dateToString(revista.getFecha()));
        recurso.setEsHtml("N");
        recurso.setMimeType(fichero.getMimeType());
        recurso.setTitulo(revista.getTitulo());
        recurso.setUrlCompleta("/upo/rest/revista/" + id + "/binary");

        pagina.add(recurso);

        template.put("pagina", pagina);
        template.put("server", revistaService.getServer());

        return Response.ok(template).type("application/pdf").build();
    }

    private String dateToString(Date date)
    {
        if (date == null)
        {
            return null;
        }

        return df.format(date);
    }

    private Date stringToDate(String date)
    {
        if (date == null || date.isEmpty())
        {
            return null;
        }

        try
        {
            return df.parse(date);
        } catch (ParseException e)
        {
            return null;
        }
    }
}
