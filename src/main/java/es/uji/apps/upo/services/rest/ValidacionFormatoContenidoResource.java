package es.uji.apps.upo.services.rest;

import java.io.UnsupportedEncodingException;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.content.ContentCleaner;
import es.uji.commons.rest.content.PlainTextHTMLCleaner;

@Path("formatocontenido")
public class ValidacionFormatoContenidoResource extends CoreBaseService
{
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_PLAIN)
    public String conversionHTMLATextoPlano(@FormParam("content") String content)
            throws UnsupportedEncodingException
    {
        ContentCleaner contentCleaner = new PlainTextHTMLCleaner();
        return contentCleaner.clean(content);
    }
}
