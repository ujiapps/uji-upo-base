package es.uji.apps.upo.services.rest.publicacion.formato;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import es.uji.apps.upo.model.Publicacion;
import es.uji.apps.upo.services.rest.publicacion.formato.filtro.UrlTagsContentReplacer;
import es.uji.apps.upo.services.rest.publicacion.formato.rss.ContenidoRSS;
import es.uji.apps.upo.services.rest.publicacion.formato.rss.Enclosure;
import es.uji.apps.upo.services.rest.publicacion.formato.rss.FeedRSS;

@Component
public class ContenidoRSSAdapter
{
    private UrlTagsContentReplacer urlTagsContentReplacer;
    private MetadataExtractor metadataExtractor;
    private String server;

    @Autowired
    public void setUrlTagsContentReplacer(UrlTagsContentReplacer urlTagsContentReplacer)
    {
        this.urlTagsContentReplacer = urlTagsContentReplacer;
    }

    @Autowired
    public void setMetadataExtractor(MetadataExtractor metadataExtractor)
    {
        this.metadataExtractor = metadataExtractor;
    }

    @Autowired
    public ContenidoRSSAdapter(@Value("${uji.webapp.host}") String server)
    {
        this.server = server;
    }

    public FeedRSS adapta(List<Publicacion> contenidos) throws UnsupportedEncodingException
    {
        FeedRSS contenidosRSS = new FeedRSS();

        for (Publicacion contenido : contenidos)
        {
            addContenidosHTMLyMetadatos(contenidosRSS, contenido, getMetadatosByIdioma(contenido));
        }

        for (Publicacion contenido : contenidos)
        {
            addEnclosures(contenidosRSS, contenido);
        }

        return contenidosRSS;
    }

    private void addEnclosures(FeedRSS contenidosRSS, Publicacion contenido)
    {
        ContenidoRSS contenidoRSS = contenidosRSS.get(contenido.getUrlNodo());

        if (contenidoRSS == null || "S".equals(contenido.getEsHtml())
                || "N".equals(contenido.getVisible()) || "N".equals(contenido.getPublicable()))
        {
            return;
        }

        Enclosure enclosure = new Enclosure();

        enclosure.setUrl(server + contenido.getUrlCompleta());
        enclosure.setType(contenido.getMimeType());

        List<Enclosure> enclosuresContenido = contenidoRSS.getEnclosures();

        if (enclosuresContenido == null)
        {
            enclosuresContenido = new ArrayList<Enclosure>();
        }

        enclosuresContenido.add(enclosure);

        contenidoRSS.setEnclosures(enclosuresContenido);
    }

    private void addContenidosHTMLyMetadatos(FeedRSS contenidosRSS, Publicacion contenido,
            List<Metadato> metadatos) throws UnsupportedEncodingException
    {
        ContenidoRSS contenidoRSS = new ContenidoRSS();

        contenidoRSS.setEsHtml(contenido.getEsHtml());
        contenidoRSS.setContenidoId(contenido.getContenidoId());
        contenidoRSS.setTitulo(contenido.getTitulo());
        contenidoRSS.setTituloLargo(contenido.getTituloLargo());
        contenidoRSS.setFechaModificacion(contenido.getFechaModificacion());
        contenidoRSS.setUrlCompleta(server + contenido.getUrlCompleta());
        contenidoRSS.setUrlBase(server + contenido.getUrlNodo());
        contenidoRSS.setOrigen(contenido.getOrigenNombre());
        contenidoRSS.setLugar(contenido.getLugar());
        contenidoRSS.setSubtitulo(contenido.getSubtitulo());
        contenidoRSS.setIdioma(contenido.getIdioma());
        contenidoRSS.setResumen(contenido.getResumen());

        contenidoRSS.setContenido(getContenido(contenido));

        contenidoRSS.setMetadatos(metadatos);

        contenidosRSS.put(contenido.getUrlNodo(), contenidoRSS);
    }

    private String getContenido(Publicacion contenido) throws UnsupportedEncodingException
    {
        if ("N".equals(contenido.getEsHtml()))
        {
            return null;
        }

        String textoNoVisible = contenido.getTextoNoVisible();

        if (textoNoVisible == null)
        {
            textoNoVisible = "";
        }

        if ("N".equals(contenido.getVisible()) && contenido.getTextoNoVisible() != null)
        {
            return textoNoVisible;
        }

        if ("N".equals(contenido.getPublicable()))
        {
            return null;
        }

        if ("S".equals(contenido.getVisible()) && contenido.getContenido() != null)
        {
            return new String(urlTagsContentReplacer.replace(contenido.getContenido()));
        }

        return null;
    }

    protected List<Metadato> getMetadatosByIdioma(Publicacion contenido)
    {
        return metadataExtractor.extrae(contenido);
    }
}
