package es.uji.apps.upo.services;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import es.uji.apps.upo.exceptions.*;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.upo.dao.ContenidoMetadatoDAO;
import es.uji.apps.upo.model.Contenido;
import es.uji.apps.upo.model.Persona;
import es.uji.apps.upo.model.metadatos.AtributoMetadato;
import es.uji.apps.upo.model.metadatos.ContenidoMetadato;
import es.uji.apps.upo.model.metadatos.TipoMetadato;

@Service
public class ContenidoMetadatoService extends AuthenticatedService
{
    private ContenidoMetadatoDAO contenidoMetadatoDAO;
    private ContenidoService contenidoService;
    private AtributoMetadatoService atributoMetadatoService;

    @Autowired
    public ContenidoMetadatoService(PersonaService personaService, ContenidoMetadatoDAO contenidoMetadatoDAO,
            ContenidoService contenidoService, AtributoMetadatoService atributoMetadatoService)
    {
        super(personaService);

        this.contenidoMetadatoDAO = contenidoMetadatoDAO;
        this.contenidoService = contenidoService;
        this.atributoMetadatoService = atributoMetadatoService;
    }

    public Long getEsquemaIdByMetadatos(List<ContenidoMetadato> metadatos)
    {
        Long esquemaId = null;

        for (ContenidoMetadato metadato : metadatos)
        {
            if (metadato.getAtributoMetadato() != null && metadato.getAtributoMetadato().getEsquemaMetadato() != null)
            {
                esquemaId = metadato.getAtributoMetadato().getEsquemaMetadato().getId();
            }
        }

        return esquemaId;
    }

    public List<ContenidoMetadato> getMetadatosByContenido(long contenidoId)
    {
        return contenidoMetadatoDAO.getMetadatosByContenidoId(contenidoId);
    }

    @Transactional
    public void update(Long esquemaId, Long contenidoId, Persona persona)
            throws IdiomaObligatorioException, UsuarioNoAutenticadoException, ActualizadoContenidoNoAutorizadoException,
            AccesibilidadException, ContenidoConMismoUrlPathException
    {
        Contenido contenido = contenidoService.getContenido(contenidoId);
        contenidoService.deleteMetadatosByContenidoId(contenidoId);

        Set<ContenidoMetadato> metadatos = new HashSet<ContenidoMetadato>();

        for (AtributoMetadato atributoMetadato : atributoMetadatoService.getByEsquemaId(esquemaId))
        {
            ContenidoMetadato contenidoMetadato = new ContenidoMetadato();

            contenidoMetadato.setUpoObjeto(contenido);
            contenidoMetadato.setAtributoMetadato(atributoMetadato);

            contenidoMetadato.setNombreClaveCA(atributoMetadato.getNombreClaveCA());
            contenidoMetadato.setNombreClaveES(atributoMetadato.getNombreClaveES());
            contenidoMetadato.setNombreClaveEN(atributoMetadato.getNombreClaveEN());

            metadatos.add(contenidoMetadato);
        }

        contenido.setUpoContenidosMetadatos(metadatos);

        contenidoService.update(contenido, persona, contenido.getNodoMapa());
    }

    @Transactional
    public void delete(Long contenidoId, Persona persona)
            throws IdiomaObligatorioException, UsuarioNoAutenticadoException, ActualizadoContenidoNoAutorizadoException,
            AccesibilidadException, ContenidoConMismoUrlPathException
    {
        Contenido contenido = contenidoService.getContenido(contenidoId);
        contenidoService.deleteMetadatosByContenidoId(contenidoId);

        contenidoService.update(contenido, persona, contenido.getNodoMapa());
    }

    @Transactional
    public void insert(Map<String, List<String>> params, Long contenidoId, Persona persona)
            throws IdiomaObligatorioException, UsuarioNoAutenticadoException, ActualizadoContenidoNoAutorizadoException,
            AccesibilidadException, ContenidoConMismoUrlPathException, LongitudMaximaExcedidaEnMetadatosException
    {
        Contenido contenido = contenidoService.getContenido(contenidoId);
        contenidoService.deleteMetadatosByContenidoId(contenidoId);

        Set<ContenidoMetadato> metadatos = new HashSet<ContenidoMetadato>();

        for (Map.Entry<String, List<String>> param : params.entrySet())
        {
            if (param.getKey().endsWith("CA"))
            {
                metadatos.add(buildMetadatos(params, contenido, param));
            }
        }

        contenido.setUpoContenidosMetadatos(metadatos);

        contenidoService.update(contenido, persona, contenido.getNodoMapa());
    }

    public ContenidoMetadato buildMetadatos(Map<String, List<String>> params, Contenido contenido,
            Map.Entry<String, List<String>> param)
            throws LongitudMaximaExcedidaEnMetadatosException
    {
        String key = param.getKey().substring(0, param.getKey().length() - 2);
        String tipo = param.getValue().get(1);
        Long id = Long.parseLong(getSubstring(tipo));
        String valorCA, valorES, valorEN;

        ContenidoMetadato contenidoMetadato = new ContenidoMetadato();

        setTipoMetadato(tipo, id, contenidoMetadato);
        setAtributoMetadato(tipo, id, contenidoMetadato);

        contenidoMetadato.setClave(key);

        valorCA = compruebaLongitudMaxima(param.getValue().get(0), key + " CA");
        valorES = compruebaLongitudMaxima(getValor(key + "ES", params), key + " ES");
        valorEN = compruebaLongitudMaxima(getValor(key + "EN", params), key + " EN");

        contenidoMetadato.setValorCA(valorCA);
        contenidoMetadato.setValorES(valorES);
        contenidoMetadato.setValorEN(valorEN);

        if (tipo.startsWith("tipo"))
        {
            contenidoMetadato.setNombreClaveCA(getSubstring(param.getValue().get(2)));
            contenidoMetadato.setNombreClaveES(getNombreClave(key + "ES", params));
            contenidoMetadato.setNombreClaveEN(getNombreClave(key + "EN", params));
        }

        contenidoMetadato.setUpoObjeto(contenido);

        return contenidoMetadato;
    }

    private String compruebaLongitudMaxima(String valor, String clave)
            throws LongitudMaximaExcedidaEnMetadatosException
    {
        if (valor.length() >= 4000)
        {
            throw new LongitudMaximaExcedidaEnMetadatosException("Longitud màxima excedida en metadada " + clave);
        }

        return valor;
    }

    private String getValor(String key, Map<String, List<String>> params)
    {
        if (params.get(key) != null)
        {
            return params.get(key).get(0);
        }

        return null;
    }

    private String getNombreClave(String key, Map<String, List<String>> params)
    {
        if (params.get(key) != null)
        {
            return getSubstring(params.get(key).get(2));
        }

        return null;
    }

    private String getSubstring(String string)
    {
        return string.substring(string.lastIndexOf(":") + 1);
    }

    private void setAtributoMetadato(String tipo, Long id, ContenidoMetadato contenidoMetadato)
    {
        if (tipo.startsWith("atributo"))
        {
            AtributoMetadato atributoMetadato = new AtributoMetadato();
            atributoMetadato.setId(id);
            atributoMetadato.setContenidosMetadatos(Collections.singleton(contenidoMetadato));

            contenidoMetadato.setAtributoMetadato(atributoMetadato);
        }
    }

    private void setTipoMetadato(String tipo, Long id, ContenidoMetadato contenidoMetadato)
    {
        if (tipo.startsWith("tipo"))
        {
            TipoMetadato tipoMetadato = new TipoMetadato();
            tipoMetadato.setId(id);
            tipoMetadato.setContenidosMetadatos(Collections.singleton(contenidoMetadato));

            contenidoMetadato.setTipoMetadato(tipoMetadato);
        }
    }
}
