package es.uji.apps.upo.services.rest.publicacion.formato.search;

public enum TipoCondicion
{
    CLOB, FECHA_MAYOR_IGUAL, FECHA_MENOR_IGUAL, FECHA_BETWEEN, LIKE, IGUAL_CADENA, IGUAL_NUMERO, METADATO, IN
}
