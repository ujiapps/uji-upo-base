package es.uji.apps.upo.services.rest.publicacion.formato.filtro;

import es.uji.apps.upo.services.rest.publicacion.model.RequestDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;

@Component
public class UrlTagsContentReplacer
{
    public static Logger log = LoggerFactory.getLogger(UrlTagsContentReplacer.class);

    private RequestDetails requestDetails;
    private byte[] contenido;
    private String idioma;

    @Autowired
    private UrlGetContent urlGetContent;

    @Autowired
    private UrlTagsExtractor urlTagsExtractor;

    public byte[] replace()
            throws UnsupportedEncodingException
    {
        String contenidoString = new String(contenido, "UTF-8");
        List<String> urls = urlTagsExtractor.extract(contenidoString);

        for (String url : urls)
        {
            try
            {
                String definitiveUrl = normalizeQueryString(escapaCaracteres(url), requestDetails.getQueryString());

                String content = "";

                urlGetContent.setRequestParams(definitiveUrl, requestDetails);

                if (requestDetails.isPostRequest())
                {
                    content = urlGetContent.get();
                }
                else
                {
                    content = urlGetContent.post();
                }

                contenidoString = contenidoString.replace("{{" + url + "}}", content);
            } catch (Exception e)
            {
                log.error("Error replacing content of: " + url, e);

                String message = "";

                if (idioma == null || idioma.isEmpty() || idioma.toLowerCase().equals("ca")) {
                    message = "<strong>No s'han pogut recuperar les dades. Torneu a provar passats uns minuts.</strong>";
                }

                if (idioma.toLowerCase().equals("es")) {
                    message = "<strong>No se han podido recuperar los datos. Volved a probar pasados unos minutos.</strong>";
                }

                if (idioma.toLowerCase().equals("en")) {
                    message = "<strong>The data could not be recovered. Please try again after a few minutes.</strong>";
                }

                message += "<br/><br/>";

                message += "<!--";
                message += url + "<br/><br/>";
                message += e.toString();
                message += "</br>";
                message += e.getMessage();
                message += "-->";

                return message.getBytes();
            }
        }

        return contenidoString.getBytes();
    }

    private String escapaCaracteres(String value)
    {
        if (value == null)
        {
            return null;
        }

        return value.replaceAll("&amp;", "&");
    }

    private String normalizeQueryString(String url, String queryString)
    {
        if (queryString == null)
        {
            return url;
        }

        if (url.contains("?"))
        {
            queryString = "&" + queryString;
        }

        if (!url.contains("?"))
        {
            queryString = "?" + queryString;
        }

        return url + queryString;
    }

    public byte[] replace(byte[] contenido, RequestDetails requestDetails, String idioma)
            throws UnsupportedEncodingException
    {
        this.contenido = contenido;
        this.requestDetails = requestDetails;
        this.idioma = idioma;

        return replace();
    }

    public byte[] replace(byte[] contenido)
            throws UnsupportedEncodingException
    {
        this.contenido = contenido;
        this.requestDetails = new RequestDetails();

        return replace();
    }
}
