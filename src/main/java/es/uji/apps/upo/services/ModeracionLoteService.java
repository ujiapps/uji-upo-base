package es.uji.apps.upo.services;

import es.uji.apps.upo.exceptions.InsertarModeracionLotesNoAutorizadoException;
import es.uji.apps.upo.exceptions.UrlNoValidaException;
import es.uji.apps.upo.dao.ModeracionLoteDAO;
import es.uji.apps.upo.migracion.ContentClient;
import es.uji.apps.upo.migracion.XpfRepositoryClient;
import es.uji.apps.upo.model.ModeracionLote;
import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.model.Persona;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.UIEntity;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

//@Service
public class ModeracionLoteService
{
    public static Logger log = Logger.getLogger(ModeracionLoteService.class);
    private ModeracionLoteDAO moderacionLoteDAO;
    private XpfRepositoryClient xpfRepositoryClient;
    private ContentClient contentClient;
    private PersonaService personaService;

    //@Autowired
    /*public ModeracionLoteService(ModeracionLoteDAO moderacionLoteDAO, XpfRepositoryClient xpfRepositoryClient,
                                 ContentClient contentClient, PersonaService personaService)
    {
        this.moderacionLoteDAO = moderacionLoteDAO;
        this.xpfRepositoryClient = xpfRepositoryClient;
        this.contentClient = contentClient;
        this.personaService = personaService;
    }*/

    @Role("ADMIN")
    public void marcaLoteComoAceptado(Long moderacionLoteId, Long connectedUserId)
    {
        moderacionLoteDAO.marcaLoteComoAceptado(moderacionLoteId, connectedUserId);
    }

    @Role("ADMIN")
    public void marcaLoteComoRechazado(Long moderacionLoteId, String motivoRechazo,
                                       Long connectedUserId)
    {
        moderacionLoteDAO.marcaLoteComoRechazado(moderacionLoteId, motivoRechazo, connectedUserId);
    }

    public List<ModeracionLote> getElementosPendientesModeracion()
    {
        return moderacionLoteDAO.getElementosPendientesModeracion();
    }

    public List<ModeracionLote> getElementosPendientesProcesamiento()
    {
        return moderacionLoteDAO.getElementosPendientesProcesamiento();
    }

    @Transactional
    public void insert(ModeracionLote moderacionLote, Persona persona, NodoMapa nodoMapa, Long connectedUserId)
            throws InsertarModeracionLotesNoAutorizadoException, UrlNoValidaException
    {
        String urlCompleta = nodoMapa.getUrlCompleta();

        if (!personaService.isNodoMapaEditable(urlCompleta, persona))
        {
            throw new InsertarModeracionLotesNoAutorizadoException();
        }

        moderacionLoteDAO.insert(moderacionLote);
    }

    public void migrar(ModeracionLote moderacionLote, NodoMapa nodoMapa)
    {
        log.info("Migrando contenido con ID " + moderacionLote.getId() + " al nodoMapa " + nodoMapa.getId());

        for (UIEntity entity : xpfRepositoryClient.getContenidosByURL(moderacionLote.getUrl(),
                nodoMapa.getUrlCompleta()))
        {
            contentClient.registrarContenido(nodoMapa.getId(), entity);
        }

        marcarLoteComoMigrado(moderacionLote);
    }

    @Transactional
    private void marcarLoteComoMigrado(ModeracionLote moderacionLote)
    {
        moderacionLote.migrar();

        moderacionLoteDAO.update(this);
    }
}
