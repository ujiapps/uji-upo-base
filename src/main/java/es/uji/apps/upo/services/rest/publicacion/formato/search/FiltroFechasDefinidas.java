package es.uji.apps.upo.services.rest.publicacion.formato.search;

import java.time.LocalDate;

public class FiltroFechasDefinidas extends Filtro
{
    public FiltroFechasDefinidas(String clave, LocalDate fechaInicio, LocalDate fechaFin)
    {
        super(clave, null);

        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }

    @Override
    public String generate()
    {
        return "(" + clave + " between to_date('" + getFormattedDate(
                fechaInicio) + "', 'dd/mm/rrrr') and to_date('" + getFormattedDate(fechaFin) + "', 'dd/mm/rrrr'))";
    }
}
