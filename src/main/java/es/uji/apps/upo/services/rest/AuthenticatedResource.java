package es.uji.apps.upo.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.exceptions.GeneralUPOException;
import es.uji.apps.upo.services.AuthenticatedService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.User;

import javax.servlet.http.Cookie;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("authenticated")
public class AuthenticatedResource extends CoreBaseService
{
    @InjectParam
    private AuthenticatedService authenticatedService;

    @GET
    public UIEntity get(@QueryParam("url") String url)
            throws GeneralUPOException
    {
        User connectedUser = AccessManager.getConnectedUser(request);
        UIEntity uiEntity = new UIEntity();
        Long userId = null;

        if (connectedUser != null)
        {
            userId = connectedUser.getId();
        }

        uiEntity.put("autenticado", checkAuthenticated(userId));
        uiEntity.put("edicion", checkPermisos(userId, url));

        return uiEntity;
    }

    private Boolean checkPermisos(Long connectedUserId, String url)
            throws GeneralUPOException
    {
        if (connectedUserId == null || url == null) return false;

        return authenticatedService.personaAdminEnNodo(url, connectedUserId);
    }

    private Boolean checkAuthenticated(Long connectedUserId)
    {
        return connectedUserId != null && connectedUserId > 0;
    }
}