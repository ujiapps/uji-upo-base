package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.PlantillaDAO;
import es.uji.apps.upo.model.NodoMapaPlantilla;
import es.uji.apps.upo.model.Plantilla;
import es.uji.commons.rest.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PlantillaService
{
    private PlantillaDAO plantillaDAO;

    @Autowired
    public PlantillaService(PlantillaDAO plantillaDAO)
    {
        this.plantillaDAO = plantillaDAO;
    }

    public List<Plantilla> getPlantillas()
    {
        return plantillaDAO.get(Plantilla.class);
    }

    public List<Plantilla> getPlantillasVigentes()
    {
        return plantillaDAO.getPlantillasVigentes();
    }

    @Role("ADMIN")
    @Transactional
    public Plantilla insert(Plantilla plantilla, Long connectedUserId)
    {
        return plantillaDAO.insert(plantilla);
    }

    @Role("ADMIN")
    @Transactional
    public void delete(long id, Long connectedUserId)
    {
        plantillaDAO.delete(Plantilla.class, id);
    }

    @Role("ADMIN")
    @Transactional
    public void update(Plantilla plantilla, Long connectedUserId)
    {
        plantillaDAO.update(plantilla);
    }

    public Plantilla getPlantillaById(long plantillaId)
    {
        List<Plantilla> listaPlantillas = plantillaDAO.get(Plantilla.class, plantillaId);

        if (listaPlantillas != null && !listaPlantillas.isEmpty())
        {
            return listaPlantillas.get(0);
        }

        return null;
    }

    public List<Plantilla> getPlantillasByTipo(String tipoPlantilla)
    {
        return plantillaDAO.getPlantillaByTipo(tipoPlantilla);
    }

    public Integer getNivelByMapaId(Long mapaId, Plantilla plantilla)
    {
        NodoMapaPlantilla nodoMapaPlantilla = getNodoMapaPlantillaByMapaId(mapaId, plantilla);

        if (nodoMapaPlantilla != null)
        {
            return nodoMapaPlantilla.getNivel();
        }

        return Plantilla.DEFAULT_LEVEL;
    }

    @Transactional
    public void setNivelByMapaId(Long mapaId, int nivel, Plantilla plantilla)
    {
        NodoMapaPlantilla nodoMapaPlantilla = getNodoMapaPlantillaByMapaId(mapaId, plantilla);
        nodoMapaPlantilla.setNivel(nivel);

        plantillaDAO.update(nodoMapaPlantilla);
    }

    private NodoMapaPlantilla getNodoMapaPlantillaByMapaId(Long mapaId, Plantilla plantilla)
    {
        return plantillaDAO.getNodoMapaPlantillaByMapaId(plantilla.getId(), mapaId);
    }

    public Plantilla getByFileName(String fileName)
    {
        return plantillaDAO.getByFileName(fileName);
    }
}
