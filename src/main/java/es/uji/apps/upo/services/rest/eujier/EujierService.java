package es.uji.apps.upo.services.rest.eujier;

import es.uji.apps.upo.dao.eujier.EujierDAO;
import es.uji.apps.upo.model.eujier.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class EujierService
{
    @Autowired
    EujierDAO eujierDAO;

    public List<EujierItem> setAplicacionesToItems(List<EujierItem> items, String idioma)
    {
        for (EujierItem item : items)
        {
            item.setAplicaciones(eujierDAO.getAplicacionesByItemId(item.getId(), idioma));
        }

        return items;
    }

    public EujierAplicacion getAplicacion(String idioma, Long aplicacionId, Long regionId)
    {
        return eujierDAO.getAplicacion(idioma, aplicacionId, regionId);
    }

    public List<EujierGrupo> getGruposByAplicacion(String idioma, Long connectedUserId, Long aplicacionId,
            Long regionId)
    {
        return eujierDAO.getGruposByAplicacion(idioma, connectedUserId, aplicacionId, regionId);
    }

    public String getGrupoName(String idioma, Long aplicacionId, Long regionId, Long grupoId)
    {
        return eujierDAO.getGrupoName(idioma, aplicacionId, regionId, grupoId);
    }

    public List<EujierItem> getItemsByGrupo(String idioma, Long connectedUserId, Long aplicacionId, Long regionId,
            Long grupoId)
    {
        return setAplicacionesToItems(
                eujierDAO.getItemsByGrupo(idioma, connectedUserId, aplicacionId, regionId, grupoId), idioma);
    }

    public EujierResultadosBusqueda getItemsBySearch(String idioma, Long connectedUserId, String s, Integer pagina)
    {
        EujierResultadosBusqueda results = eujierDAO.getItemsBySearch(idioma, connectedUserId, s, pagina);

        results.setItems(setAplicacionesToItems(results.getItems(), idioma));

        return results;
    }

    public Map<String, List<EujierGrupo>> getGruposBySearch(String idioma, Long connectedUserId, String search)
    {
        Map<String, List<EujierGrupo>> result = new LinkedHashMap<>();

        for (EujierAplicacion aplicacion : eujierDAO.getAplicacionesBySearch(idioma, search))
        {
            EujierGrupo grupo = new EujierGrupo();

            grupo.setAplicacionNombre(aplicacion.getNombre());
            grupo.setAplicacionId(aplicacion.getAplicacionId());
            grupo.setRegionId(aplicacion.getRegionId());
            grupo.setRegionNombre(aplicacion.getRegionNombre());

            addGrupoToMap(result, aplicacion.getRegionNombre(), grupo);
        }

        for (EujierGrupo grupo : eujierDAO.getGruposBySearch(idioma, connectedUserId, search))
        {
            addGrupoToMap(result, grupo.getRegionNombre(), grupo);
        }

        return result;
    }

    private void addGrupoToMap(Map<String, List<EujierGrupo>> result, String key, EujierGrupo grupo)
    {
        if (!result.containsKey(key))
        {
            result.put(key, new ArrayList<EujierGrupo>());
        }

        result.get(key).add(grupo);
    }

    public List<EujierRegion> getRegiones(String idioma)
    {
        return eujierDAO.getRegiones(idioma);
    }

    public List<EujierAplicacion> getAplicaciones(String idioma)
    {
        return eujierDAO.getAplicaciones(idioma);
    }

    public List<EujierAviso> getAvisos(String idioma, Long connectedUserId)
    {
        return eujierDAO.getAvisos(idioma, connectedUserId);
    }

    public void delFromFavoritos(Long itemId, Long connectedUserId)
    {
        eujierDAO.delFromFavoritos(itemId, connectedUserId);
    }

    public void addToFavoritos(Long itemId, Long connectedUserId)
    {
        eujierDAO.addToFavoritos(itemId, connectedUserId);
    }

    public void markAviso(Long avisoId, Long connectedUserId)
    {
        eujierDAO.markAviso(avisoId, connectedUserId);
    }
}
