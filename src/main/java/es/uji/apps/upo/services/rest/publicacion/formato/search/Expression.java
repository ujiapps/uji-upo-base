package es.uji.apps.upo.services.rest.publicacion.formato.search;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public abstract class Expression
{
    protected List<Expression> expressions;

    public Expression()
    {
        expressions = new ArrayList<>();
    }

    public abstract String generate();

    public void add(Expression expression)
    {
        expressions.add(expression);
    }

    protected String getFormattedDate(LocalDate date)
    {
        if (date == null)
        {
            return "";
        }

        return date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

    protected List<String> getExpressions()
    {
        List<String> results = new ArrayList<>();

        for (Expression expression : expressions)
        {
            results.add(expression.generate());
        }

        return results;
    }
}
