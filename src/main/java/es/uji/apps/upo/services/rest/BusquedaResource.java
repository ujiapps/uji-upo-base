package es.uji.apps.upo.services.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.upo.solr.ContenidoSolr;
import es.uji.apps.upo.solr.SolrIndexer;
import es.uji.commons.rest.CoreBaseService;

@Path("busqueda")
public class BusquedaResource extends CoreBaseService
{
    @InjectParam
    private SolrIndexer indexer;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<ContenidoSolr> search(@QueryParam("query") String query)
    {
        return indexer.getList(ContenidoSolr.class, query);
    }
}
