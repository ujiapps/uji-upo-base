package es.uji.apps.upo.services.rest.publicacion.formato.search;

import es.uji.commons.rest.StringUtils;

public class FiltroMetadato extends Filtro<String>
{
    public FiltroMetadato(String clave, String valor)
    {
        super(clave, Util.replaceForbiddenChars(StringUtils.limpiaAcentos(valor.toLowerCase())));
    }

    @Override
    public String generate()
    {
        if ("*".equals(clave))
        {
            return "(valor like '%" + valor + "%')";
        }

        return "(clave = '" + clave + "' and valor like '%" + valor + "%')";
    }
}
