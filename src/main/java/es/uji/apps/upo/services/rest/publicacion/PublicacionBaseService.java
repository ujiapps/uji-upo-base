package es.uji.apps.upo.services.rest.publicacion;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.services.PublicacionService;
import es.uji.commons.rest.CoreBaseService;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;

public class PublicacionBaseService extends CoreBaseService
{
    @InjectParam
    protected PublicacionService publicacionService;

    protected String getUrlBase(HttpServletRequest clientRequest) throws MalformedURLException
    {
        String urlReference = clientRequest.getRequestURL().toString();

        URL result = new URL(urlReference);
        int port = result.getPort();

        if (port <= 0)
        {
            port = 80;
        }

        return MessageFormat.format("{0}://{1}:{2,number,#}", result.getProtocol(),
                result.getHost(), port);
    }

    protected String getCookies(HttpServletRequest clientRequest) throws MalformedURLException
    {
        return clientRequest.getHeader("Cookie");
    }
}