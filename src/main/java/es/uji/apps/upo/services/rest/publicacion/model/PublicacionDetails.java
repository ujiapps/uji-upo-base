package es.uji.apps.upo.services.rest.publicacion.model;

import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.model.Publicacion;
import es.uji.apps.upo.model.enums.IdiomaPublicacion;

import java.util.List;

public class PublicacionDetails
{
    private String url;
    private String urlBase;
    private String idioma;
    private List<Publicacion> contenidos;
    private NodoMapa nodoMapa;
    private RequestDetails requestDetails;
    private String titulo;

    public PublicacionDetails()
    {
        this.idioma = IdiomaPublicacion.CA.toString();
    }

    public String getIdioma()
    {
        return idioma;
    }

    public void setIdioma(String idioma)
    {
        this.idioma = idioma;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getUrlBase()
    {
        return urlBase;
    }

    public void setUrlBase(String urlBase)
    {
        this.urlBase = urlBase;
    }

    public NodoMapa getNodoMapa()
    {
        return nodoMapa;
    }

    public void setNodoMapa(NodoMapa nodoMapa)
    {
        this.nodoMapa = nodoMapa;
    }

    public RequestDetails getRequestDetails()
    {
        return requestDetails;
    }

    public void setRequestDetails(RequestDetails requestDetails)
    {
        this.requestDetails = requestDetails;
    }

    public List<Publicacion> getContenidos()
    {
        return contenidos;
    }

    public void setContenidos(List<Publicacion> contenidos)
    {
        this.contenidos = contenidos;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }
}



