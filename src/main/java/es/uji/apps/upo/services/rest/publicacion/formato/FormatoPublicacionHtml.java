package es.uji.apps.upo.services.rest.publicacion.formato;

import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.model.NodoMapaPlantilla;
import es.uji.apps.upo.model.Plantilla;
import es.uji.apps.upo.model.Publicacion;
import es.uji.apps.upo.model.enums.TipoPlantilla;
import es.uji.apps.upo.services.rest.publicacion.formato.search.CondicionFiltro;
import es.uji.apps.upo.services.rest.publicacion.model.PublicacionDetails;
import es.uji.apps.upo.services.rest.publicacion.model.RequestDetails;
import es.uji.apps.upo.services.rest.publicacion.model.RequestParams;
import es.uji.apps.upo.utils.DateUtils;
import es.uji.commons.web.template.Template;
import es.uji.commons.web.template.model.*;
import es.uji.commons.web.template.model.Metadato;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.*;

@Component
public class FormatoPublicacionHtml extends AbstractPublicacionHtml implements FormatoPublicacion
{
    @Override
    public Response publica(RequestParams requestParams)
            throws MalformedURLException, ParseException, UnsupportedEncodingException
    {
        NodoMapa nodoMapa = requestParams.getNodoMapa();

        if (nodoMapa == null)
        {
            return new PaginaNoEncontrada().publica(requestParams);
        }

        List<Publicacion> contenidos =
                publicacionService.getContenidosIdiomasByUrlNodoAndLanguage(requestParams.getUrl(),
                        requestParams.getConfigPublicacion().getIdioma());
        Plantilla plantilla = null;

        if (contenidos == null || contenidos.isEmpty() || nodoMapa == null)
        {
            return new PaginaNoEncontrada().publica(requestParams);
        }

        if (requestParams.getConfigPublicacion().getPlantilla() != null)
        {
            plantilla = publicacionService.getPlantillaByFileName(requestParams.getConfigPublicacion().getPlantilla());

            if (plantilla == null)
            {
                requestParams.getConfigPublicacion().setPlantilla(null);
            }
        }

        List<NodoMapaPlantilla> nodoMapaPlantilla =
                publicacionService.getNodoMapaPlantillasByNodoMapaId(nodoMapa.getId());

        PublicacionDetails publicacionDetails = new PublicacionDetails();
        publicacionDetails.setUrl(requestParams.getUrl());
        publicacionDetails.setUrlBase(requestParams.getUrlBase());
        publicacionDetails.setContenidos(contenidos);
        publicacionDetails.setNodoMapa(nodoMapa);
        publicacionDetails.setRequestDetails(requestParams.getRequestDetails());
        publicacionDetails.setIdioma(requestParams.getConfigPublicacion().getIdioma());

        Pagina pagina = buildPagina(publicacionDetails);

        addParametros(pagina, publicacionDetails.getRequestDetails().getQueryParams());
        addParametros(pagina, publicacionDetails.getRequestDetails().getFormParams());

        if (plantilla != null && plantilla.getTipo().equals(TipoPlantilla.PDF))
        {
            return publicaConPlantillaPDF(nodoMapa, requestParams.getConfigPublicacion().getIdioma(), pagina);
        }

        if (plantilla == null && nodoMapaPlantilla.size() == 1 && nodoMapaPlantilla.get(0)
                .getUpoPlantilla()
                .getTipo()
                .equals(TipoPlantilla.PDF))
        {
            return publicaConPlantillaPDF(nodoMapa, requestParams.getConfigPublicacion().getIdioma(), pagina);
        }

        return publicaConPlantillaHTML(pagina, requestParams.getConfigPublicacion().getPlantilla(), publicacionDetails);
    }

    private void addParametros(Pagina pagina, Map<String, String> filtros)
    {
        filtros.entrySet().stream().forEach(e -> pagina.addParametro(e.getKey(), e.getValue()));
    }

    private Response publicaConPlantillaHTML(Pagina pagina, String nombrePlantilla,
            PublicacionDetails publicacionDetails)
            throws ParseException, UnsupportedEncodingException, MalformedURLException
    {
        Template template = buildHTMLTemplate(pagina, nombrePlantilla, publicacionDetails);

        return Response.ok(template).type(MediaType.TEXT_HTML).build();
    }

    private Response publicaConPlantillaPDF(NodoMapa nodoMapa, String idioma, Pagina pagina)
            throws ParseException, MalformedURLException, UnsupportedEncodingException
    {
        Template template = buildPDFTemplate(nodoMapa, idioma, pagina);

        return Response.ok(template).type("application/pdf").build();
    }

    @Override
    public Pagina buildPagina(PublicacionDetails publicacionDetails)
            throws ParseException, MalformedURLException, UnsupportedEncodingException
    {
        Pagina pagina = super.buildPagina(publicacionDetails);

        pagina = (Pagina) buildEstructura(publicacionDetails, pagina);

        addFechaModificacionDocumentos(pagina);

        return pagina;
    }

    private void addFechaModificacionDocumentos(Pagina pagina)
    {
        for (Recurso texto : pagina.getTextos())
        {
            if (!"dataset opendata".equals(texto.getMetadato("esquema"))) continue;

            Date ultimaFechaModificacionDocumentos = getUltimaFechaModificacionDocumentos(pagina);

            if (ultimaFechaModificacionDocumentos == null) continue;

            texto.setFechaUltimaActualizacionDocumentos(ultimaFechaModificacionDocumentos);
        }
    }

    private Date getUltimaFechaModificacionDocumentos(Pagina pagina)
    {
        List<Date> fechasModificacion = new ArrayList<>();

        Seccion seccionDocumentos = null;

        for (Seccion seccion : pagina.getSecciones())
        {
            if (seccion.getUrl().equals("documents"))
            {
                seccionDocumentos = seccion;
            }
        }

        if (seccionDocumentos == null) return null;

        for (Recurso pdf : seccionDocumentos.getDocumentos())
        {
            fechasModificacion.add(pdf.getFechaModificacion());
        }

        if (fechasModificacion.size() == 0) return null;

        return Collections.max(fechasModificacion);
    }
}
