package es.uji.apps.upo.services.rest.eujier;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.dao.eujier.EujierDAO;
import es.uji.apps.upo.model.eujier.EujierAplicacion;
import es.uji.apps.upo.model.eujier.EujierResultadosBusqueda;
import es.uji.apps.upo.model.eujier.EujierSeccionConfiguracion;
import es.uji.apps.upo.model.eujier.enums.TipoVista;
import es.uji.apps.upo.services.GrupoService;
import es.uji.apps.upo.services.ItemService;
import es.uji.apps.upo.services.MenuService;
import es.uji.apps.upo.services.rest.publicacion.MenuFactory;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;
import es.uji.commons.web.template.model.Menu;
import es.uji.commons.web.template.model.Pagina;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.util.Locale;
import java.util.Map;


@Path("iglu")
public class EujierResource extends CoreBaseService
{
    public static final long IGLU_MENU_ID = 112543463L;
    public static final int CHARS_MINIMOS_BUSQUEDA = 3;
    public static Logger log = LoggerFactory.getLogger(EujierResource.class);
    private final String URL_BASE = "/upo/rest/iglu/";
    @InjectParam
    private MenuFactory menuFactory;

    @InjectParam
    private EujierSeccionConfiguracionAdapter adapter;

    @InjectParam
    private MenuService menuService;

    @InjectParam
    private GrupoService grupoService;

    @InjectParam
    private ItemService itemService;

    @InjectParam
    private EujierService eujierService;

    @GET
    public Response eujier(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @Context UriInfo uriInfo)
            throws ParseException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Map<String, EujierSeccionConfiguracion> stringEujierSeccionConfiguracionMap =
                adapter.adaptaSecciones(idioma, uriInfo.getQueryParameters(), connectedUserId);

        Template template = buildTemplateBase(idioma);
        template.put("plantilla", "seccion-all-sections");
        template.put("secciones", stringEujierSeccionConfiguracionMap);
        template.put("urlBase", URL_BASE);
        template.put("queryString", "");

        return Response.ok(template).type(MediaType.TEXT_HTML).build();
    }

    @GET
    @Path("section")
    public Response section(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                            @QueryParam("codigoSeccion") String codigoSecccion, @QueryParam("vista") String vista)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Template template = new HTMLTemplate("upo/eujier/2021/seccion-main", new Locale(idioma.toLowerCase()), "upo");
        template.put("seccion", adapter.adaptaSeccion(idioma, codigoSecccion, connectedUserId, vista));
        template.put("key", codigoSecccion);

        return Response.ok(template).type(MediaType.TEXT_HTML).build();
    }

    @GET
    @Path("group")
    public Response group(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                          @QueryParam("aplicacionId") Long aplicacionId, @QueryParam("regionId") Long regionId)
            throws ParseException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Template template = buildTemplateBase(idioma);
        EujierAplicacion aplicacion = eujierService.getAplicacion(idioma, aplicacionId, regionId);

        template.put("nombreAplicacion", aplicacion.getNombre());
        template.put("colorAplicacion", aplicacion.getColorClass());
        template.put("aplicacionId", aplicacionId);
        template.put("regionId", regionId);
        template.put("plantilla", "seccion-group");
        template.put("grupos", eujierService.getGruposByAplicacion(idioma, connectedUserId, aplicacionId, regionId));
        template.put("urlBase", URL_BASE + "group/");
        template.put("queryString", "aplicacionId=" + aplicacionId + "&regionId=" + regionId);

        return Response.ok(template).type(MediaType.TEXT_HTML).build();
    }

    @GET
    @Path("item")
    public Response item(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                         @QueryParam("aplicacionId") Long aplicacionId, @QueryParam("regionId") Long regionId,
                         @QueryParam("grupoId") Long grupoId, @QueryParam("vista") String vista)
            throws ParseException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Template template = buildTemplateBase(idioma);
        EujierAplicacion aplicacion = eujierService.getAplicacion(idioma, aplicacionId, regionId);

        String vistaAux = (vista == null) ? TipoVista.LISTA.toString() : vista;

        template.put("nombreAplicacion", aplicacion.getNombre());
        template.put("colorAplicacion", aplicacion.getColorClass());
        template.put("nombreGrupo", eujierService.getGrupoName(idioma, aplicacionId, regionId, grupoId));
        template.put("aplicacionId", aplicacionId);
        template.put("regionId", regionId);
        template.put("grupoId", grupoId);
        template.put("plantilla", "seccion-item");
        template.put("vista", vistaAux);
        template.put("items", eujierService.getItemsByGrupo(idioma, connectedUserId, aplicacionId, regionId, grupoId));
        template.put("urlBase", URL_BASE + "item/");
        template.put("queryString",
                "vista=" + vistaAux + "&aplicacionId=" + aplicacionId + "&regionId=" + regionId + "&grupoId=" + grupoId);

        return Response.ok(template).type(MediaType.TEXT_HTML).build();
    }

    @GET
    @Path("search")
    public Response search(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                           @QueryParam("pageSearch") Integer pageSearch, @QueryParam("texto") String texto, @Context UriInfo uriInfo)
            throws ParseException, UnsupportedEncodingException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(pageSearch, texto);

        String search = URLDecoder.decode(StringUtils.removeStopWords(texto, idioma));
        EujierResultadosBusqueda resultsItems = eujierService.getItemsBySearch(idioma, connectedUserId, search, pageSearch);
        resultsItems.setPagina(pageSearch);

        Template template = buildTemplateBase(idioma, resultsItems);

        template.put("plantilla", "seccion-search");

        template.put("items", resultsItems.getItems());
        template.put("texto", texto);
        template.put("vista", TipoVista.CUADRICULA.toString());
        template.put("urlBase", URL_BASE + "search/");
        template.put("queryString", uriInfo.getRequestUri().getRawQuery());

        if (search.trim().length() >= CHARS_MINIMOS_BUSQUEDA)
        {
            template.put("gruposBusqueda", eujierService.getGruposBySearch(idioma, connectedUserId, search));
        }

        return Response.ok(template).type(MediaType.TEXT_HTML).build();
    }

    private Template buildTemplateBase(String idioma)
            throws ParseException
    {
        return buildTemplateBase(idioma, null);
    }

    private Template buildTemplateBase(String idioma, EujierResultadosBusqueda resultadosBusqueda)
            throws ParseException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Pagina pagina = new Pagina("", "eujier/group", idioma, "IGLU");

        Menu menu = menuFactory.buildMenuByNodoMapaMenu(menuService.getMenu(IGLU_MENU_ID), idioma);
        pagina.setMenu(menu);
        pagina.setMostrarReloj(true);
        pagina.setMostrarRedesSociales(true);

        if (resultadosBusqueda != null)
        {
            pagina.setHasSearch(true);
            pagina.setValuesForNextAndPreviousPage((resultadosBusqueda.getPagina() - 1) * EujierDAO.RESULTADOS_POR_PAGINA, EujierDAO.RESULTADOS_POR_PAGINA,
                    resultadosBusqueda.getNumItems().intValue(), resultadosBusqueda.getPagina());
        }

        Template template = new HTMLTemplate("upo/eujier/2021/page-eujier", new Locale(idioma.toLowerCase()), "upo");
        template.put("perId", connectedUserId);
        template.put("pagina", pagina);
        template.put("regiones", eujierService.getRegiones(idioma));
        template.put("aplicaciones", eujierService.getAplicaciones(idioma));
        template.put("avisos", eujierService.getAvisos(idioma, AccessManager.getConnectedUserId(request)));

        return template;
    }

    @PUT
    @Path("favourite")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResponseMessage doFavouritesAction(@FormParam("action") String action, @FormParam("id") Long itemId)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ParamUtils.checkNotNull(action, itemId, connectedUserId);

        if ("del".equalsIgnoreCase(action))
        {
            eujierService.delFromFavoritos(itemId, connectedUserId);
        }

        if ("add".equalsIgnoreCase(action))
        {
            eujierService.addToFavoritos(itemId, connectedUserId);
        }

        return new ResponseMessage(true);
    }

    @PUT
    @Path("notice")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResponseMessage doNoticesAction(@FormParam("id") Long avisoId)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ParamUtils.checkNotNull(avisoId, connectedUserId);

        eujierService.markAviso(avisoId, connectedUserId);

        return new ResponseMessage(true);
    }
}