package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.PrioridadContenidoDAO;
import es.uji.apps.upo.exceptions.*;
import es.uji.apps.upo.model.Persona;
import es.uji.apps.upo.model.PrioridadContenido;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.List;

@Service
public class PrioridadContenidoService extends AuthenticatedService
{
    private PrioridadContenidoDAO prioridadContenidoDAO;

    @Autowired
    public PrioridadContenidoService(PersonaService personaService, PrioridadContenidoDAO prioridadContenidoDAO)
    {
        super(personaService);

        this.prioridadContenidoDAO = prioridadContenidoDAO;
    }

    public List<PrioridadContenido> getPrioridadesByContenido(Long contenidoId)
    {
        return prioridadContenidoDAO.getPrioridadesByContenido(contenidoId);
    }

    public PrioridadContenido getPrioridadByIdAndContenido(Long contenidoId, Long prioridadId)
    {
        return prioridadContenidoDAO.getPrioridadByIdAndContenido(contenidoId, prioridadId);
    }

    @Transactional
    public void delete(Persona persona, PrioridadContenido prioridadContenido)
            throws BorradoContenidoNoAutorizadoException
    {
        ParamUtils.checkNotNull(persona);

        if (!personaAdminEnNodo(prioridadContenido.getNodoMapa(), persona))
        {
            throw new BorradoContenidoNoAutorizadoException();
        }

        prioridadContenidoDAO.delete(PrioridadContenido.class, prioridadContenido.getId());
    }

    @Transactional
    public PrioridadContenido insert(Persona persona, PrioridadContenido prioridadContenido)
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            InsertadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        ParamUtils.checkNotNull(persona);

        if (!personaAdminEnNodo(prioridadContenido.getNodoMapa(), persona))
        {
            throw new InsertadoContenidoNoAutorizadoException();
        }

        prioridadContenido.checkFechasCorrectas();
        return prioridadContenidoDAO.insert(prioridadContenido);
    }

    @Transactional
    public PrioridadContenido update(Persona persona, PrioridadContenido prioridadContenido)
            throws FechaFinPrioridadSuperiorAFechaInicioException,
            SeIndicaUnaHoraSinFechaAsociadaException, ParseException,
            ActualizadoContenidoNoAutorizadoException, UsuarioNoAutenticadoException
    {
        ParamUtils.checkNotNull(persona);

        if (!personaAdminEnNodo(prioridadContenido.getNodoMapa(), persona))
        {
            throw new ActualizadoContenidoNoAutorizadoException();
        }

        prioridadContenido.checkFechasCorrectas();
        return prioridadContenidoDAO.update(prioridadContenido);
    }
}
