package es.uji.apps.upo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.upo.dao.CriterioDAO;
import es.uji.apps.upo.model.Criterio;

@Service
public class CriterioService
{
    private CriterioDAO criterioDAO;

    @Autowired
    public CriterioService(CriterioDAO criterioDAO)
    {
        this.criterioDAO = criterioDAO;
    }

    @Transactional
    public Criterio update(Criterio criterio)
    {
        return criterioDAO.update(criterio);
    }

    @Transactional
    public void delete(long criterioId)
    {
        criterioDAO.delete(Criterio.class, criterioId);
    }

    @Transactional
    public Criterio insert(Criterio criterio)
    {
        return criterioDAO.insert(criterio);
    }

    public Criterio getCriterioById(Long criterioId)
    {
        return criterioDAO.get(Criterio.class, criterioId).get(0);
    }

    public List<Criterio> getCriterios()
    {
        return criterioDAO.getCriterios();
    }
}
