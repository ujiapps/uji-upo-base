package es.uji.apps.upo.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.exceptions.ActualizadoContenidoNoAutorizadoException;
import es.uji.apps.upo.exceptions.CrearNuevoContenidoException;
import es.uji.apps.upo.exceptions.TipoContenidoNoAdecuadoException;
import es.uji.apps.upo.exceptions.UsuarioNoAutenticadoException;
import es.uji.apps.upo.model.Persona;
import es.uji.apps.upo.services.NodoMapaContenidoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("nodomapa-contenido")
public class NodoMapaContenidoResource extends CoreBaseService
{
    @InjectParam
    private NodoMapaContenidoService nodoMapaContenidoService;

    @PUT
    @Path("{id}/actualizar-tipo")
    public void convertirContenidoLinkANormal(@PathParam("id") Long nodoMapaContenidoId)
            throws UsuarioNoAutenticadoException, ActualizadoContenidoNoAutorizadoException, NumberFormatException,
            TipoContenidoNoAdecuadoException, CrearNuevoContenidoException
    {
        Persona persona = new Persona();
        persona.setId(AccessManager.getConnectedUserId(request));

        nodoMapaContenidoService.cambiaTipoLinkToNormal(nodoMapaContenidoId, persona);
    }

    @PUT
    @Path("{id}/duplicar")
    public void duplicarContenidoNormal(@PathParam("id") Long nodoMapaContenidoId)
            throws ActualizadoContenidoNoAutorizadoException, TipoContenidoNoAdecuadoException,
            UsuarioNoAutenticadoException, CrearNuevoContenidoException
    {
        Persona persona = new Persona();
        persona.setId(AccessManager.getConnectedUserId(request));

        nodoMapaContenidoService.duplicarContenido(nodoMapaContenidoId, persona);
    }
}
