package es.uji.apps.upo.services.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.Role;
import es.uji.commons.sso.AccessManager;

@Path("time")
public class TimeResource extends CoreBaseService
{
    @GET
    public String get()
    {
        return new Long(System.currentTimeMillis()).toString();
    }
}
