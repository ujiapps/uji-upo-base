package es.uji.apps.upo.services.rest;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.exceptions.BuscadoFicherosEnElReservoriException;
import es.uji.apps.upo.model.ParametrosBusquedaBinarios;
import es.uji.apps.upo.model.ResultadoBusquedaBinarios;
import es.uji.apps.upo.services.ContenidoIdiomaService;
import es.uji.apps.upo.storage.RecursoReservori;
import es.uji.apps.upo.storage.metadata.ImageMetadataExtractor;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

@Path("reservori")
public class ReservoriResource extends CoreBaseService
{
    @Context
    private ServletContext servletContext;

    @InjectParam
    private ContenidoIdiomaService contenidoIdiomaService;

    @InjectParam
    private ImageMetadataExtractor imageMetadataExtractor;

    @GET
    @Path("recursos/imagenes")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getImagenes(@QueryParam("query") String query, @QueryParam("urlNode") String urlNode,
            @QueryParam("urlNodeSearch") String urlNodeSearch, @QueryParam("start") String start,
            @QueryParam("limit") String limit, @QueryParam("repositorio") String repositorio,
            @QueryParam("idioma") String idioma)
            throws BuscadoFicherosEnElReservoriException
    {
        Integer startNumber = 0, limitNumber = 18;

        if (query == null || query.isEmpty())
        {
            return new UIEntity();
        }

        if (start != null && !start.isEmpty())
        {
            startNumber = Integer.parseInt(start);
        }

        if (limit != null && !limit.isEmpty())
        {
            limitNumber = Integer.parseInt(limit);
        }

        if (urlNode == null || urlNode.isEmpty())
        {
            urlNode = "/";
        }

        if (urlNodeSearch == null || urlNodeSearch.isEmpty())
        {
            urlNodeSearch = "/";
        }

        UIEntity entity = null;

        if (repositorio.equalsIgnoreCase("ALFRESCO"))
        {
            entity = prepararRespuestaReservori(
                    RecursoReservori.getListImagesURL(query, urlNodeSearch, startNumber, limitNumber));
        }

        if (repositorio.equalsIgnoreCase("DIRECTORIO"))
        {
            //TODO: hay que incorporar cosas de los ifs anteriores a esta clase, al quitar el repo de Alfresco
            ParametrosBusquedaBinarios parametros = new ParametrosBusquedaBinarios();

            parametros.setQuery(query);
            parametros.setUrlNode(urlNode);
            parametros.setStart(startNumber);
            parametros.setLimit(limitNumber);
            parametros.setIdioma(idioma);

            entity = prepararRespuestaReservori(contenidoIdiomaService.getImagesBySearchAndUrlCompleta(parametros));
        }

        return entity;
    }

    @GET
    @Path("recursos/documentos")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getDocumentos(@QueryParam("query") String query, @QueryParam("urlNode") String urlNode,
            @QueryParam("urlNodeSearch") String urlNodeSearch, @QueryParam("start") String start,
            @QueryParam("limit") String limit, @QueryParam("repositorio") String repositorio,
            @QueryParam("idioma") String idioma)
            throws BuscadoFicherosEnElReservoriException, NumberFormatException
    {
        Integer startNumber = 0, limitNumber = 25;

        if (query == null || query.isEmpty())
        {
            return new UIEntity();
        }

        if (start != null && !start.isEmpty())
        {
            startNumber = Integer.parseInt(start);
        }

        if (limit != null && !limit.isEmpty())
        {
            limitNumber = Integer.parseInt(limit);
        }

        if (urlNode == null || urlNode.isEmpty())
        {
            urlNode = "/";
        }

        if (urlNodeSearch == null || urlNodeSearch.isEmpty())
        {
            urlNodeSearch = "/";
        }

        UIEntity entity = null;

        if (repositorio.equalsIgnoreCase("ALFRESCO"))
        {
            entity = prepararRespuestaReservori(
                    RecursoReservori.getListDocumentsURL(query, urlNodeSearch, startNumber, limitNumber));
        }

        if (repositorio.equalsIgnoreCase("DIRECTORIO"))
        {
            //TODO: hay que incorporar cosas de los ifs anteriores a esta clase, al quitar el repo de Alfresco
            ParametrosBusquedaBinarios parametros = new ParametrosBusquedaBinarios();

            parametros.setQuery(query);
            parametros.setUrlNode(urlNode);
            parametros.setStart(startNumber);
            parametros.setLimit(limitNumber);
            parametros.setIdioma(idioma);

            entity = prepararRespuestaReservori(contenidoIdiomaService.getBinariosBySearchAndUrlCompleta(parametros));
        }

        return entity;
    }

    private UIEntity prepararRespuestaReservori(Map<String, Object> documentosReservori)
    {
        Long numDocumentos = (Long) documentosReservori.get("numDocumentos");
        List<RecursoReservori> listaRecursos = (List<RecursoReservori>) documentosReservori.get("listaDocumentos");

        UIEntity result = new UIEntity("pagination");

        result.put("numDocumentos", numDocumentos);
        result.put("resultados", UIEntity.toUI(listaRecursos));

        return result;
    }

    private UIEntity prepararRespuestaReservori(ResultadoBusquedaBinarios documentosReservori)
    {
        UIEntity result = new UIEntity("pagination");

        result.put("numDocumentos", documentosReservori.getNumResultados());

        result.put("resultados", UIEntity.toUI(documentosReservori.getBinarios()));
        return result;
    }

    @PUT
    @Path("recursos/imagenes/metadatos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getImagenesMetadatos(@FormParam("id") String id, @FormParam("url") String urlDescarga)
    {
        InputStream stream;

        //Esto es para Alfresco, a borrar cuando se quite
        if (id.contains("workspace://"))
        {
            stream = RecursoReservori.getFileById(id).getStream();
        }
        else
        {
            stream = contenidoIdiomaService.getStreamById(ParamUtils.parseLong(id));
        }

        if (stream == null)
        {
            Client client = Client.create();

            WebResource getFile = client.resource(urlDescarga);

            ClientResponse response = getFile.get(ClientResponse.class);

            stream = response.getEntityInputStream();
        }

        return UIEntity.toUI(imageMetadataExtractor.extract(stream));
    }
}
