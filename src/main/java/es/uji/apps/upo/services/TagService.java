package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.TagDAO;
import es.uji.apps.upo.exceptions.TagExisteException;
import es.uji.apps.upo.model.Tag;
import es.uji.commons.rest.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TagService {
    private TagDAO tagDAO;

    @Autowired
    public TagService(TagDAO contenidoTagDAO) {
        this.tagDAO = contenidoTagDAO;
    }

    @Role({"ADMIN", "DOCUMENTALISTA"})
    public Tag insert(Tag tag, Long connectedUserId) throws TagExisteException {
        if (tagDAO.existsTag(tag.getTagname())) {
            throw new TagExisteException();
        }

        return tagDAO.insert(tag);
    }

    public List<Tag> getAllTags() {
        return tagDAO.getAllTags();
    }

    public List<String> getTagsAsChannels() {
        return tagDAO.getTagsAsChannels();
    }

    @Role({"ADMIN", "DOCUMENTALISTA"})
    public void deleteById(Long id, Long connectedUserId) {
        tagDAO.delete(Tag.class, id);
    }

    @Role({"ADMIN", "DOCUMENTALISTA"})
    @Transactional
    public void update(Long id, String newTagName, Long connectedUserId) {
        Tag tag = tagDAO.getById(id);

        tagDAO.updateAllValuesOnObjetoTag(tag.getTagname(), newTagName);

        if (tagDAO.existsTag(newTagName)) {
            tagDAO.delete(tag);

            return;
        }

        tag.setTagname(newTagName);
        tagDAO.update(tag);
    }
}
