package es.uji.apps.upo.services.rest.publicacion.formato.filtro;

import es.uji.apps.upo.services.rest.publicacion.model.RequestDetails;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Component
public class UrlGetContent
{
    public static Logger log = LoggerFactory.getLogger(UrlGetContent.class);

    private String url;
    private RequestDetails requestDetails;

    public UrlGetContent()
    {
    }

    public String get()
            throws IOException
    {
        URL url = new URL(this.url);
        String cookies = requestDetails.getCookies();

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setConnectTimeout(15000);
        connection.setReadTimeout(15000);

        connection.setRequestMethod("GET");

        if (cookies != null && !cookies.isEmpty())
        {
            connection.setRequestProperty("Cookie", cookies);
        }

        connection.connect();

        return getInputString(connection);
    }

    public String post()
            throws IOException
    {
        URL url = new URL(this.url);
        String cookies = requestDetails.getCookies();

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestMethod("POST");

        if (cookies != null && !cookies.isEmpty())
        {
            connection.setRequestProperty("Cookie", cookies);
        }

        connection.setDoInput(true);
        connection.setDoOutput(true);

        OutputStream os = connection.getOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
        writer.write(getPostDataString(requestDetails.getFormParams()));

        writer.flush();
        writer.close();
        os.close();

        return getInputString(connection);
    }

    private String getInputString(HttpURLConnection connection)
            throws IOException
    {
        InputStream in = connection.getInputStream();

        String encoding = connection.getContentEncoding();
        encoding = encoding == null ? "UTF-8" : encoding;

        return IOUtils.toString(in, encoding);
    }

    private String getPostDataString(Map<String, String> params)
            throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet())
        {
            if (first) first = false;
            else result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public void setRequestParams(String url, RequestDetails requestDetails)
    {
        this.url = url;
        this.requestDetails = requestDetails;
    }
}