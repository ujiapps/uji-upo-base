package es.uji.apps.upo.services.rest.publicacion.formato.search;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class CondicionFactory
{
    public Filtro get(CondicionFiltro condicion)
    {
        switch (condicion.getTipo())
        {
            case IGUAL_NUMERO:
                return new FiltroNumerico(condicion.getNombre(), Integer.parseInt(condicion.getValor()));

            case IGUAL_CADENA:
                return new FiltroIgualCadena(condicion.getNombre(), condicion.getValor());

            case LIKE:
                return new FiltroLikeCadena(condicion.getNombre(), condicion.getValor());

            case IN:
                return new FiltroInCadena(condicion.getNombre(), condicion.getValor());

            case CLOB:
                return new FiltroClob(condicion.getNombre(), condicion.getValor());

            case METADATO:
                return new FiltroMetadato(condicion.getNombre(), condicion.getValor());

            case FECHA_MAYOR_IGUAL:
                return new FiltroFechaMayorOIgual(condicion.getNombre(), toLocalDate(condicion.getValor()));

            case FECHA_MENOR_IGUAL:
                return new FiltroFechaMenorOIgual(condicion.getNombre(), toLocalDate(condicion.getValor()));
        }

        return null;
    }

    private LocalDate toLocalDate(String date)
    {
        if (date == null || date.isEmpty())
        {
            return null;
        }

        return LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }
}
