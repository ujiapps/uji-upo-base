package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.NodoMapaPlantillaDAO;
import es.uji.apps.upo.model.NodoMapaPlantilla;
import es.uji.apps.upo.model.enums.TipoPlantilla;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NodoMapaPlantillaService
{
    private NodoMapaPlantillaDAO nodoMapaPlantillaDAO;

    @Autowired
    public void setNodoMapaPlantillaDAO(NodoMapaPlantillaDAO nodoMapaPlantillaDAO)
    {
        this.nodoMapaPlantillaDAO = nodoMapaPlantillaDAO;
    }

    public NodoMapaPlantilla getNodoMapaPlantillaByNodoMapaIdAndType(Long nodoMapaId, TipoPlantilla tipoPlantilla)
    {
        List<NodoMapaPlantilla> nodoMapaPlantillas = nodoMapaPlantillaDAO
                .getNodoMapaPlantillaByNodoMapaIdAndType(nodoMapaId, tipoPlantilla);

        if (nodoMapaPlantillas.size() != 0)
        {
            return nodoMapaPlantillas.get(0);
        }

        return null;
    }

    public List<NodoMapaPlantilla> getNodoMapaPlantillaByNodoMapaId(Long nodoMapaId)
    {
        return nodoMapaPlantillaDAO.getNodoMapaPlantillasByNodoMapaId(nodoMapaId);
    }
}
