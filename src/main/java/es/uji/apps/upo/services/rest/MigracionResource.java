package es.uji.apps.upo.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.exceptions.ImportarDocumentoAlReservoriException;
import es.uji.apps.upo.exceptions.InsertadoContenidoException;
import es.uji.apps.upo.migracion.TitleExtractor;
import es.uji.apps.upo.migracion.UrlExtractor;
import es.uji.apps.upo.model.ItemMigracion;
import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.model.enums.TipoFormatoContenido;
import es.uji.apps.upo.model.enums.TipoItemMigracion;
import es.uji.apps.upo.model.metadatos.ContenidoMetadato;
import es.uji.apps.upo.services.ContenidoService;
import es.uji.apps.upo.services.ItemMigracionService;
import es.uji.apps.upo.services.MigracionService;
import es.uji.apps.upo.services.PersonaService;
import es.uji.apps.upo.services.nodomapa.NodoMapaService;
import es.uji.apps.upo.utils.DateUtils;
import es.uji.apps.upo.utils.UrlUtils;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import org.apache.commons.lang3.StringEscapeUtils;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.*;

@Path("migracion")
public class MigracionResource extends CoreBaseService
{
    private static final String ESPANOL = "ES";
    private static final String CATALAN = "CA";
    private static final String INGLES = "UK";

    @InjectParam
    private MigracionService migracionService;

    @InjectParam
    private ItemMigracionService itemMigracionService;

    @InjectParam
    private PersonaService personaService;

    @InjectParam
    private NodoMapaService nodoMapaService;

    @InjectParam
    private ContenidoService contenidoService;

    @GET
    @Path("url/binario")
    public void addElementoBinarioByUrl(@QueryParam("url") String url, @QueryParam("urlNode") String urlNode)
            throws InsertadoContenidoException
    {
        Long userId = AccessManager.getConnectedUserId(request);

        NodoMapa nodoMapa = nodoMapaService.getNodoMapaByUrlCompleta(urlNode);

        ItemMigracion item = itemMigracionService.getItems(url);

        if (item == null || esTexto(item))
        {
            return;
        }

        String urlPath = normalizeUrlPath(UrlUtils.getUrlPath(url, false));
        Long origenInformacion = null;
        String otrosAutores = null;

        if (nodoMapa.getUpoFranquicia() != null && nodoMapa.getUpoFranquicia().getOrigenInformacion() != null)
        {
            origenInformacion = nodoMapa.getUpoFranquicia().getOrigenInformacion().getId();
        }

        if (nodoMapa.getUpoFranquicia() != null)
        {
            otrosAutores = nodoMapa.getUpoFranquicia().getOtrosAutores();
        }

        String titulo = getTitle(item, url);
        String resumen = StringEscapeUtils.unescapeHtml4(item.getResumen());
        String tags = StringEscapeUtils.unescapeHtml4(item.getTags());
        String mimeType = item.getMimeType();
        byte[] contenido = item.getContenido();

        contenidoService.insertContenido(nodoMapa.getId().toString(), origenInformacion.toString(), urlPath, url,
                userId, "", "", "", "", otrosAutores, titulo, "", titulo, resumen, "N", "", contenido, "", titulo, "",
                titulo, resumen, "N", "", contenido, "", titulo, "", titulo, resumen, "N", "", contenido, "", "", false,
                "", "", tags, urlPath, urlPath, urlPath, mimeType, mimeType, mimeType, false, false, false,
                DateUtils.convertDateToStringToPrint(new Date()) + " 00:00:00", TipoFormatoContenido.BINARIO.toString(),
                TipoFormatoContenido.BINARIO.toString(), TipoFormatoContenido.BINARIO.toString(),
                new HashMap<String, String>(), new HashMap<String, String>(), new HashMap<String, String>(), "S", "",
                new HashSet<ContenidoMetadato>());
    }

    @GET
    @Path("url/texto")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getElementoTextoByUrl(@QueryParam("url") String url, @QueryParam("urlNode") String urlNode)
            throws IOException, ImportarDocumentoAlReservoriException
    {
        Long userId = AccessManager.getConnectedUserId(request);

        UIEntity result = new UIEntity("Contenido");

        NodoMapa nodoMapa = nodoMapaService.getNodoMapaByUrlCompleta(urlNode);

        ItemMigracion item = itemMigracionService.getItems(url);

        if (item == null || esBinario(item))
        {
            return null;
        }

        addContenidoIdiomaToEntity(result, CATALAN, url, urlNode);
        addContenidoIdiomaToEntity(result, ESPANOL, url, urlNode);
        addContenidoIdiomaToEntity(result, INGLES, url, urlNode);

        result.put("tags", StringEscapeUtils.unescapeHtml4(item.getTags()));
        result.put("urlOriginal", url);
        result.put("urlPath", normalizeUrlPath(UrlUtils.getUrlPath(url)));

        if (nodoMapa.getUpoFranquicia() != null)
        {
            result.put("otrosAutores", nodoMapa.getUpoFranquicia().getOtrosAutores());
        }

        if (nodoMapa.getUpoFranquicia() != null && nodoMapa.getUpoFranquicia().getOrigenInformacion() != null)
        {
            result.put("origenInformacion", nodoMapa.getUpoFranquicia().getOrigenInformacion().getId());
        }

        result.put("personaResponsable", personaService.getPersona(userId).getNombreCompleto());
        result.put("fechaCreacion", new Date());

        return Collections.singletonList(result);
    }

    private String normalizeUrlPath(String urlPath)
    {
        return urlPath.replaceAll("[^A-Za-z0-9_.\\-]+", "");
    }

    private void addContenidoIdiomaToEntity(UIEntity result, String idioma, String url, String urlNode)
            throws IOException, ImportarDocumentoAlReservoriException
    {
        ItemMigracion itemMigracion = itemMigracionService.getItem(idioma, url);

        if (itemMigracion == null || esBinario(itemMigracion))
        {
            return;
        }

        String titulo = getTitle(itemMigracion, url);
        result.put("titulo" + idioma, titulo);
        result.put("tituloLargo" + idioma, titulo);
        result.put("resumen" + idioma, StringEscapeUtils.unescapeHtml4(itemMigracion.getResumen()));
        result.put("tipoFormato" + idioma, TipoFormatoContenido.PAGINA);
        result.put("contenido" + idioma,
                actualizarContenidoConUrlReservori(new String(itemMigracion.getContenido()), urlNode));
    }

    private Boolean esBinario(ItemMigracion itemMigracion)
    {
        return TipoItemMigracion.BINARIO.toString().equalsIgnoreCase(itemMigracion.getTipo());
    }

    private Boolean esTexto(ItemMigracion itemMigracion)
    {
        return TipoItemMigracion.TEXTO.toString().equalsIgnoreCase(itemMigracion.getTipo());
    }

    private String getTitle(ItemMigracion item, String url)
    {
        if (item.getTitulo() != null)
        {
            return StringEscapeUtils.unescapeHtml4(item.getTitulo());
        }

        String titulo = null;

        if (!esBinario(item))
        {
            titulo = TitleExtractor.getTitle(new String(item.getContenido()));
        }

        if (titulo != null)
        {
            return titulo;
        }

        return url;
    }

    private String actualizarContenidoConUrlReservori(String html, String urlNode)
            throws IOException, ImportarDocumentoAlReservoriException
    {
        UrlExtractor urlExtractor = new UrlExtractor(html);
        String htmlConUrlsConvertidas = html;

        if (html != null && !html.isEmpty())
        {
            ArrayList<String> urlsNoConvertidas = urlExtractor.extractAll();

            for (String urlNoConvertida : urlsNoConvertidas)
            {
                htmlConUrlsConvertidas = htmlConUrlsConvertidas.replaceAll(urlNoConvertida,
                        urlExtractor.convertirURLenRecursoReservori(urlNoConvertida, urlNode));
            }
        }

        return htmlConUrlsConvertidas;
    }

    @GET
    @Path("listaurl")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getListURL(@QueryParam("start") String start, @QueryParam("limit") String limit,
            @QueryParam("query") String query)
    {
        Integer startNumber = 0, limitNumber = 25;

        if (start != null)
        {
            startNumber = Integer.parseInt(start);
        }

        if (limit != null)
        {
            limitNumber = Integer.parseInt(limit);
        }

        List<UIEntity> listEntities =
                UIEntity.toUI(migracionService.getURLsByPaginationAndURL(startNumber, limitNumber, query));

        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setSuccess(true);

        responseMessage.setData(listEntities);
        responseMessage.setTotalCount(migracionService.getCountURl(query));

        return responseMessage;
    }
}

