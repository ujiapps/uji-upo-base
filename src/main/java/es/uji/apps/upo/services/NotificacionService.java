package es.uji.apps.upo.services;

import es.uji.apps.upo.model.*;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.MailMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class NotificacionService
{
    private PersonaService personaService;

    @Autowired
    public NotificacionService(PersonaService personaService)
    {
        this.personaService = personaService;
    }

    @Transactional
    public void enviaCorreoResultadoModeracion(String nodoMapaUrlCompleta, String estado, String textoRechazo, List<String> emails,
            String tipo)
    {
        MailMessage email = new MailMessage("UPO");

        email.setTitle("[e-ujier@portal] " + tipo + " " + estado);
        email.setSender("noreply@uji.es");
        email.setContent(getCuerpoEmail(nodoMapaUrlCompleta, estado, textoRechazo));
        email.setContentType(MediaType.TEXT_PLAIN);

        if (emails == null || emails.isEmpty()) return;

        emails.stream().distinct().forEach(mail -> {
            email.addToRecipient(mail);
        });

        try
        {
            MessagingClient emailClient = new MessagingClient();
            emailClient.send(email);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private String getCuerpoEmail(String urlCompleta, String estado, String textoRechazo)
    {
        String message =
                "Aquest correu s'ha generat automàticament per a informar-vos que un contingut propossat per vosaltres ha estat: " + estado + "\n\n";

        message += "Adreça: " + urlCompleta + "\n";

        if (textoRechazo != null)
        {
            message += "Motiu: " + textoRechazo + "\n";
        }

        message += new Date() + "\n";

        return message;
    }

    public void enviarCorreoAAdministradores(NodoMapa nodoMapa, Long personaPropuestaId)
    {
        Franquicia franquicia = nodoMapa.getUpoFranquicia();
        List<Persona> administradoresDeLaFranquicia = new ArrayList<Persona>();

        if (franquicia != null)
        {
            administradoresDeLaFranquicia = personaService.getPersonasAdministradoresByFranquiciaId(franquicia.getId());
        }

        for (Persona destinatario : administradoresDeLaFranquicia)
        {
            if (destinatario.getEmail() != null)
            {
                enviaCorreo(nodoMapa.getUrlCompleta(), personaPropuestaId, destinatario);
            }
        }
    }

    @Transactional
    public void enviaCorreo(String nodoMapaUrlCompleta, Long personaPropuestaId, Persona destinatario)
    {
        MailMessage email = new MailMessage("UPO");
        email.setTitle("[e-ujier@portal] Contingut pendent de moderar");
        email.setSender("noreply@uji.es");
        email.setContent(getCuerpoEmailNotificacionAlAdministrador(nodoMapaUrlCompleta, personaPropuestaId));
        email.setContentType(MediaType.TEXT_PLAIN);

        email.addToRecipient(destinatario.getEmail().trim());

        try
        {
            MessagingClient emailClient = new MessagingClient();
            emailClient.send(email);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private String getCuerpoEmailNotificacionAlAdministrador(String nodoMapaUrlCompleta, Long personaPropuestaId)
    {
        String message =
                "Aquest correu s'ha generat automàticament per a informar-vos que teniu un contingut pendent de Moderar: \n\n";

        message += "Adreça: " + nodoMapaUrlCompleta + "\n";

        if (personaPropuestaId != null)
        {
            message += "Proposat per: " + personaService.getPersona(personaPropuestaId).getNombreCompleto() + "\n";
        }

        message += new Date() + "\n";

        return message;
    }
}
