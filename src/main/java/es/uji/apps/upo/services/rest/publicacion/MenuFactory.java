package es.uji.apps.upo.services.rest.publicacion;

import es.uji.apps.upo.model.Grupo;
import es.uji.apps.upo.model.Item;
import es.uji.apps.upo.services.PublicacionService;
import es.uji.commons.web.template.model.GrupoMenu;
import es.uji.commons.web.template.model.ItemMenu;
import es.uji.commons.web.template.model.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MenuFactory
{
    private PublicacionService publicacionService;

    @Autowired
    public MenuFactory(PublicacionService publicacionService)
    {
        this.publicacionService = publicacionService;
    }

    public Menu buildMenuByNodoMapaMenu(es.uji.apps.upo.model.Menu nodoMapaMenu,
            String idioma)
    {
        List<Grupo> nodoMapaGrupos = publicacionService.getGruposByMenuId(nodoMapaMenu.getId());
        Menu menu = new Menu();

        for (Grupo nodoMapaGrupo : nodoMapaGrupos)
        {
            GrupoMenu grupo = new GrupoMenu(getNombreGrupo(nodoMapaGrupo, idioma));

            addItemsToGrupo(nodoMapaMenu, nodoMapaGrupo, grupo, idioma);

            menu.addGrupo(grupo);
        }

        return menu;
    }

    private String getNombreGrupo(Grupo nodoMapaGrupo, String idioma)
    {
        if (idioma.toLowerCase().equals("es"))
        {
            return nodoMapaGrupo.getNombreES();
        }

        if (idioma.toLowerCase().equals("en"))
        {
            return nodoMapaGrupo.getNombreEN();
        }

        return nodoMapaGrupo.getNombreCA();
    }

    public Menu buildMenuPrevisualizacion()
    {
        Menu menu = new Menu();

        GrupoMenu grupo = new GrupoMenu("Comunicació");
        grupo.addItem(new ItemMenu("Noticies", "http://www.uji.es/"));
        grupo.addItem(new ItemMenu("Investigació", "http://www.uji.es/"));
        menu.addGrupo(grupo);

        return menu;
    }

    private void addItemsToGrupo(es.uji.apps.upo.model.Menu nodoMapaMenu,
            Grupo nodoMapaGrupo, GrupoMenu grupo, String idioma)
    {
        List<Item> nodoMapaItems = publicacionService.getItemsByGrupoId(nodoMapaGrupo.getId());

        for (Item nodoMapaItem : nodoMapaItems)
        {
            grupo.addItem(new ItemMenu(getNombreItem(nodoMapaItem, idioma), nodoMapaItem.getUrl()));
        }
    }

    private String getNombreItem(Item nodoMapaItem, String idioma)
    {
        if (idioma.toLowerCase().equals("es"))
        {
            return nodoMapaItem.getNombreES();
        }

        if (idioma.toLowerCase().equals("en"))
        {
            return nodoMapaItem.getNombreEN();
        }

        return nodoMapaItem.getNombreCA();
    }
}
