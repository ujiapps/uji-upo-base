package es.uji.apps.upo.services;

import com.google.common.collect.Lists;
import es.uji.apps.upo.dao.ModeracionDAO;
import es.uji.apps.upo.dao.NodoMapaContenidoDAO;
import es.uji.apps.upo.exceptions.BorradoContenidoNoAutorizadoException;
import es.uji.apps.upo.exceptions.NodoConNombreYaExistenteEnMismoPadreException;
import es.uji.apps.upo.exceptions.UsuarioNoAutenticadoException;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.services.nodomapa.NodoMapaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ModeracionService
{
    public static final String NODO = "Node";
    public static final String CONTENIDO = "Contingut";
    public static final String RECHAZADO = "REBUTJAT";
    public static final String ACEPTADO = "ACCEPTAT";

    private NodoMapaContenidoDAO nodoMapaContenidoDAO;
    private ModeracionDAO moderacionDAO;
    private NodoMapaContenidoService nodoMapaContenidoService;
    private NotificacionService notificacionService;
    private ModeracionHistoricoService moderacionHistoricoService;
    private NodoMapaService nodoMapaService;

    @Autowired
    public ModeracionService(NodoMapaContenidoDAO nodoMapaContenidoDAO, ModeracionDAO moderacionDAO,
            NodoMapaContenidoService nodoMapaContenidoService, NotificacionService notificacionService,
            ModeracionHistoricoService moderacionHistoricoService, NodoMapaService nodoMapaService)
    {
        this.nodoMapaContenidoDAO = nodoMapaContenidoDAO;
        this.moderacionDAO = moderacionDAO;
        this.notificacionService = notificacionService;
        this.nodoMapaContenidoService = nodoMapaContenidoService;
        this.moderacionHistoricoService = moderacionHistoricoService;
        this.nodoMapaService = nodoMapaService;
    }

    public List<Moderacion> getNodosPendientesModeracion(Long personaId)
    {
        return moderacionDAO.getNodosPendientesModeracion(personaId);
    }

    public List<Moderacion> getElementosPendientesModeracionByMapaId(Long personaId, Long mapaId)
    {
        return moderacionDAO.getElementosPendientesModeracionByMapaId(personaId, mapaId);
    }

    public ModeracionHistoricoRespuesta getElementosHistoricoModeracion(Long personaId, Long start, Long limit)
    {
        ModeracionHistoricoRespuesta respuesta = new ModeracionHistoricoRespuesta();

        respuesta.setResultados(moderacionDAO.getElementosHistoricoModeracion(personaId, start, limit));
        respuesta.setNumResultados(moderacionDAO.getNumElementosHistoricoModeracion(personaId));

        return respuesta;
    }

    public void marcaContenidoComoAceptado(Long nodoMapaContenidoId, Long connectedUserId)
    {
        if (!estaContenidoPendienteDeModeracion(nodoMapaContenidoId, connectedUserId))
        {
            return;
        }

        NodoMapaContenido nodoMapaContenido = aceptaContenido(nodoMapaContenidoId, connectedUserId);

        enviaCorreoContenido(nodoMapaContenido, ACEPTADO);
    }

    public void marcaContenidoNodosComoAceptado(NodoMapa nodoMapa, Persona persona)
    {
        List<String> emails = new ArrayList();
        List<Moderacion> contenidosPendientes =
                moderacionDAO.getElementosDescendientesPendientesModeracionByUrlCompleta(persona.getId(), nodoMapa.getUrlCompleta());

        contenidosPendientes.forEach(c -> {
            NodoMapaContenido nodoMapaContenido = aceptaContenido(c.getMapaObjetoId(), persona.getId());

            addEmail(emails, nodoMapaContenido);
        });

        nodoMapaService.desbloquearNodoMapaYDescendientes(nodoMapa.getUrlCompleta());

        enviaCorreoNodo(nodoMapa, ACEPTADO, null, emails);
    }

    private NodoMapaContenido aceptaContenido(Long nodoMapaContenidoId, Long connectedUserId)
    {
        NodoMapaContenido nodoMapaContenido = nodoMapaContenidoService.getNodoMapaContenido(nodoMapaContenidoId);

        nodoMapaContenido.acepta(connectedUserId);

        nodoMapaContenido = nodoMapaContenidoDAO.update(nodoMapaContenido);

        moderacionHistoricoService.registraHistoricoModeracion(nodoMapaContenido);

        return nodoMapaContenido;
    }

    public void marcaContenidoComoRechazado(Long nodoMapaContenidoId, String motivoRechazo, Long connectedUserId)
    {
        if (!estaContenidoPendienteDeModeracion(nodoMapaContenidoId, connectedUserId))
        {
            return;
        }

        NodoMapaContenido nodoMapaContenido = rechazaContenido(nodoMapaContenidoId, motivoRechazo, connectedUserId);

        enviaCorreoContenido(nodoMapaContenido, RECHAZADO);
    }

    public void marcaContenidoNodosComoRechazado(NodoMapa nodoMapa, String motivoRechazo, Persona persona)
            throws BorradoContenidoNoAutorizadoException, NodoConNombreYaExistenteEnMismoPadreException,
            UsuarioNoAutenticadoException
    {
        List<String> emails = new ArrayList();
        List<Moderacion> contenidosPendientes =
                moderacionDAO.getElementosDescendientesPendientesModeracionByUrlCompleta(persona.getId(), nodoMapa.getUrlCompleta());

        contenidosPendientes.forEach(c -> {
            NodoMapaContenido nodoMapaContenido = rechazaContenido(c.getMapaObjetoId(), motivoRechazo, persona.getId());

            addEmail(emails, nodoMapaContenido);
        });

        nodoMapaService.arrastrarNodoAPapelera(nodoMapa, persona);

        enviaCorreoNodo(nodoMapa, RECHAZADO, motivoRechazo, emails);
    }

    private NodoMapaContenido rechazaContenido(Long nodoMapaContenidoId, String motivoRechazo, Long connectedUserId)
    {
        NodoMapaContenido nodoMapaContenido = nodoMapaContenidoService.getNodoMapaContenido(nodoMapaContenidoId);

        nodoMapaContenido.rechaza(connectedUserId, motivoRechazo);

        nodoMapaContenido = nodoMapaContenidoDAO.update(nodoMapaContenido);

        moderacionHistoricoService.registraHistoricoModeracion(nodoMapaContenido);

        return nodoMapaContenido;
    }

    private void addEmail(List<String> emails, NodoMapaContenido nodoMapaContenido)
    {
        if (nodoMapaContenido.getPersonaPropuesta() != null)
        {
            emails.add(nodoMapaContenido.getPersonaPropuesta().getEmail().trim());
        }
    }

    public void enviaCorreoContenido(NodoMapaContenido nodoMapaContenido, String estado)
    {
        if (nodoMapaContenido.getPersonaPropuesta() == null) return;

        notificacionService.enviaCorreoResultadoModeracion(nodoMapaContenido.getUpoMapa().getUrlCompleta(), estado,
                nodoMapaContenido.getTextoRechazo(),
                Lists.newArrayList(nodoMapaContenido.getPersonaPropuesta().getEmail().trim()), CONTENIDO);
    }

    public void enviaCorreoNodo(NodoMapa nodoMapa, String estado, String textoRechazo, List<String> emails)
    {
        notificacionService.enviaCorreoResultadoModeracion(nodoMapa.getUrlCompleta(), estado, textoRechazo, emails,
                NODO);
    }

    private boolean estaContenidoPendienteDeModeracion(Long nodoMapaContenidoId, Long connectedUserId)
    {
        Moderacion contenido = moderacionDAO.getElementoPendientesModeracionById(connectedUserId, nodoMapaContenidoId);

        if (contenido != null && contenido.getMapaObjetoId().equals(nodoMapaContenidoId))
        {
            return true;
        }

        return false;
    }
}
