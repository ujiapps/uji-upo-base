package es.uji.apps.upo.services.nodomapa;

import es.uji.apps.upo.dao.NodoMapaDAO;
import es.uji.apps.upo.exceptions.*;
import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.model.NodoMapaPlantilla;
import es.uji.apps.upo.model.Persona;
import es.uji.apps.upo.services.AuthenticatedService;
import es.uji.apps.upo.services.PersonaService;
import es.uji.apps.upo.services.PlantillaService;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EstructuraVoxUjiService extends AuthenticatedService
{
    NodoMapaService nodoMapaService;
    PlantillaService plantillaService;
    NodoMapaDAO nodoMapaDAO;
    private PersonaService personaService;

    @Autowired
    public EstructuraVoxUjiService(NodoMapaService nodoMapaService, PlantillaService plantillaService,
            NodoMapaDAO nodoMapaDAO, PersonaService personaService)
    {
        super(personaService);

        this.nodoMapaService = nodoMapaService;
        this.plantillaService = plantillaService;
        this.nodoMapaDAO = nodoMapaDAO;
        this.personaService = personaService;
    }

    public void insertaConEstructuraDeVoxuji(Persona persona, NodoMapa nodoMapa, String numero)
            throws UsuarioNoAutenticadoException, NodoConNombreYaExistenteEnMismoPadreException,
            InsertadoContenidoNoAutorizadoException, InsertarEstructuraDeVoxujiEnNodoMapaException
    {
        ParamUtils.checkNotNull(persona);

        if (!personaAdminEnNodo(nodoMapa, persona))
        {
            throw new InsertadoContenidoNoAutorizadoException();
        }

        if (numero != null && !numero.isEmpty())
        {
            nodoMapaService.comprobarQueNoHayaNodosHijoConElMimoNombre(nodoMapa, numero);
            insertaConEstructuraDeVoxuji(nodoMapa, numero);
        }
        else
        {
            throw new InsertarEstructuraDeVoxujiEnNodoMapaException("El número no puede estar vacío");
        }
    }

    @Transactional
    public void insertaConEstructuraDeVoxuji(NodoMapa parentNodoMapa, String numero)
    {
        NodoMapaPlantilla nodoMapaPlantillaNewsletter = new NodoMapaPlantilla();
        nodoMapaPlantillaNewsletter.setNivel(3);

        nodoMapaPlantillaNewsletter.setUpoPlantilla(plantillaService.getByFileName("2021/page-newsletter"));

        NodoMapa nodoMapaParent =
                buildNewNodoMapaForEstructuraVoxuji(numero, parentNodoMapa, nodoMapaPlantillaNewsletter);

        NodoMapaPlantilla nodoMapaPlantillaNewsletterPortada = new NodoMapaPlantilla();
        nodoMapaPlantillaNewsletterPortada.setUpoPlantilla(plantillaService.getByFileName("2021/page-newsletter-portada"));

        NodoMapaPlantilla nodoMapaPlantillaNewsletterDetalleFotonoticia = new NodoMapaPlantilla();
        nodoMapaPlantillaNewsletterDetalleFotonoticia.setUpoPlantilla(
                plantillaService.getByFileName("2021/page-fotonoticia"));

        NodoMapaPlantilla nodoMapaPlantillaNewsletterNoticia = new NodoMapaPlantilla();
        nodoMapaPlantillaNewsletterNoticia.setUpoPlantilla(
                plantillaService.getByFileName("2021/page-base"));

        NodoMapaPlantilla nodoMapaPlantillaNewsletterEspaiObert = new NodoMapaPlantilla();
        nodoMapaPlantillaNewsletterEspaiObert.setUpoPlantilla(plantillaService.getByFileName("2021/page-base"));

        buildNewNodoMapaForEstructuraVoxuji("principal", nodoMapaParent, nodoMapaPlantillaNewsletterPortada);
        buildNewNodoMapaForEstructuraVoxuji("destacades", nodoMapaParent, nodoMapaPlantillaNewsletterPortada);
        buildNewNodoMapaForEstructuraVoxuji("fotonoticies", nodoMapaParent,
                nodoMapaPlantillaNewsletterDetalleFotonoticia);
        buildNewNodoMapaForEstructuraVoxuji("altres", nodoMapaParent, nodoMapaPlantillaNewsletterNoticia);
        buildNewNodoMapaForEstructuraVoxuji("publicitat", nodoMapaParent, nodoMapaPlantillaNewsletterPortada);
        buildNewNodoMapaForEstructuraVoxuji("espaiobert", nodoMapaParent, nodoMapaPlantillaNewsletterEspaiObert);
    }

    public NodoMapa buildNewNodoMapaForEstructuraVoxuji(String urlPath, NodoMapa parentNodoMapa,
            NodoMapaPlantilla nodoMapaPlantillaOriginal)
    {
        NodoMapa newNodoMapa = new NodoMapa();
        newNodoMapa.setNodoMapaDAO(nodoMapaDAO);

        newNodoMapa.setUrlPath(urlPath);
        newNodoMapa.setUrlCompleta(parentNodoMapa.getUrlCompleta() + urlPath + '/');

        newNodoMapa.setCommonInheritedfields(parentNodoMapa, newNodoMapa, nodoMapaPlantillaOriginal);

        return nodoMapaDAO.insert(newNodoMapa);
    }
}
