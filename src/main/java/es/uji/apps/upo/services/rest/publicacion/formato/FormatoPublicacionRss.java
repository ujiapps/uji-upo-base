package es.uji.apps.upo.services.rest.publicacion.formato;

import com.sun.syndication.io.FeedException;
import es.uji.apps.upo.model.Publicacion;
import es.uji.apps.upo.services.rest.publicacion.model.RequestParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.List;

@Component
public class FormatoPublicacionRss extends AbstractPublicacionHtml implements FormatoPublicacion
{
    protected ContenidoRSSAdapter contenidoRSSAdapter;

    @Autowired
    public void setContenidoRSSAdapter(ContenidoRSSAdapter contenidoRSSAdapter)
    {
        this.contenidoRSSAdapter = contenidoRSSAdapter;
    }

    @Override
    public Response publica(RequestParams requestParams)
            throws MalformedURLException, ParseException, FeedException, UnsupportedEncodingException
    {
        List<Publicacion> contenidos =
                publicacionService.getContenidosIdiomasByUrlNodoAndLanguage(requestParams.getUrl(),
                        requestParams.getConfigPublicacion().getIdioma());

        if (contenidos == null || contenidos.isEmpty())
        {
            return new PaginaNoEncontrada().publica(requestParams);
        }

        FeedPublicacion feed = new FeedPublicacion();

        feed.addEntries(contenidoRSSAdapter.adapta(contenidos));

        return Response.ok(feed.getXML()).type("text/xml").build();
    }
}
