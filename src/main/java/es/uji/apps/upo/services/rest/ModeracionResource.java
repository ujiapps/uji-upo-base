package es.uji.apps.upo.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.exceptions.BorradoContenidoNoAutorizadoException;
import es.uji.apps.upo.exceptions.NodoConNombreYaExistenteEnMismoPadreException;
import es.uji.apps.upo.exceptions.UsuarioNoAutenticadoException;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.EstadoModeracion;
import es.uji.apps.upo.services.ContenidoService;
import es.uji.apps.upo.services.ModeracionService;
import es.uji.apps.upo.services.nodomapa.NodoMapaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("moderacion")
public class ModeracionResource extends CoreBaseService
{
    @InjectParam
    private ModeracionService moderacionService;

    @InjectParam
    private ContenidoService contenidoService;

    @InjectParam
    private NodoMapaService nodoMapaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getNodosPendientesModeracion()
    {
        List<UIEntity> resultList = new ArrayList<UIEntity>();
        Long personaId = AccessManager.getConnectedUserId(request);

        for (Moderacion contenido : moderacionService.getNodosPendientesModeracion(personaId))
        {
            UIEntity result = UIEntity.toUI(contenido);

            resultList.add(result);
        }

        return resultList;
    }

    @GET
    @Path("{mapaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getElementosPendientesModeracionByMapId(@PathParam("mapaId") Long mapaId)
    {
        List<UIEntity> resultList = new ArrayList<UIEntity>();
        Long personaId = AccessManager.getConnectedUserId(request);

        for (Moderacion contenido : moderacionService.getElementosPendientesModeracionByMapaId(personaId, mapaId))
        {
            UIEntity result = UIEntity.toUI(contenido);

            resultList.add(result);
        }

        return resultList;
    }


    @GET
    @Path("historico")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getElementosHistoricoModeracion(@QueryParam("start") Long start, @QueryParam("limit") Long limit)
    {
        Long personaId = AccessManager.getConnectedUserId(request);

        ModeracionHistoricoRespuesta resultados =
                moderacionService.getElementosHistoricoModeracion(personaId, start, limit);

        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setSuccess(true);

        responseMessage.setData(UIEntity.toUI(resultados.getResultados()));
        responseMessage.setTotalCount(resultados.getNumResultados().intValue());

        return responseMessage;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void marcaContenidoComoAprobadoRechazado(UIEntity entity)
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        UIEntity data = entity.getRelations().get("data").get(0);
        EstadoModeracion estadoModeracion = EstadoModeracion.valueOf(data.get("estadoModeracion"));
        String textoRechazo = data.get("textoRechazo");

        for (String nodoMapaObjetoId : data.getArray("nodoMapaObjetos"))
        {
            Long nodoMapaContenidoId = ParamUtils.parseLong(nodoMapaObjetoId);

            if (EstadoModeracion.ACEPTADO.equals(estadoModeracion))
            {
                moderacionService.marcaContenidoComoAceptado(nodoMapaContenidoId, personaId);
            }

            if (EstadoModeracion.RECHAZADO.equals(estadoModeracion))
            {
                moderacionService.marcaContenidoComoRechazado(nodoMapaContenidoId, textoRechazo, personaId);
            }
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("nodos")
    public void marcaContenidoNodosComoAprobadoRechazado(UIEntity entity)
            throws BorradoContenidoNoAutorizadoException, NodoConNombreYaExistenteEnMismoPadreException,
            UsuarioNoAutenticadoException
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona();
        persona.setId(personaId);

        UIEntity data = entity.getRelations().get("data").get(0);
        EstadoModeracion estadoModeracion = EstadoModeracion.valueOf(data.get("estadoModeracion"));
        String textoRechazo = data.get("textoRechazo");

        for (String nodoMapaId : data.getArray("nodoMapas"))
        {
            NodoMapa nodoMapa = nodoMapaService.getNodoMapa(ParamUtils.parseLong(nodoMapaId), false);

            if (nodoMapa == null) return;

            if (EstadoModeracion.ACEPTADO.equals(estadoModeracion))
            {
                moderacionService.marcaContenidoNodosComoAceptado(nodoMapa, persona);
            }

            if (EstadoModeracion.RECHAZADO.equals(estadoModeracion))
            {
                moderacionService.marcaContenidoNodosComoRechazado(nodoMapa, textoRechazo, persona);
            }
        }
    }
}