package es.uji.apps.upo.services.rest.publicacion.formato.filtro;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

@Component
public class UrlTagsExtractor
{
    private String contenido;
    private List<String> allMatches = new ArrayList<>();
    private Matcher matcher;
    private Pattern pattern = Pattern.compile("\\{\\{[^\\}]*\\}\\}");

    public UrlTagsExtractor()
    {
    }

    public UrlTagsExtractor(String contenido)
    {
        super();
        this.contenido = contenido;
    }

    public List<String> extract()
    {
        allMatches = new ArrayList<>();
        matcher = pattern.matcher(contenido);

        while (matcher.find())
        {
            allMatches.add(getUrl(matcher.group()));
        }

        return allMatches;
    }

    public List<String> extract(String contenido)
    {
        this.contenido = contenido;

        return extract();
    }

    private String getUrl(String matcher)
    {
        return matcher.substring(2, matcher.length() - 2);
    }
}
