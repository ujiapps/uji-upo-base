package es.uji.apps.upo.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.model.Autoguardado;
import es.uji.apps.upo.model.Contenido;
import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.services.AutoguardadoService;
import es.uji.apps.upo.services.ContenidoService;
import es.uji.apps.upo.services.nodomapa.NodoMapaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("autoguardado")
public class AutoguardadoResource extends CoreBaseService
{
    @InjectParam
    private AutoguardadoService autoguardadoService;

    @InjectParam
    private NodoMapaService nodoMapaService;

    @InjectParam
    private ContenidoService contenidoService;

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void insert(@FormParam("mapaId") String mapaId,
                       @FormParam("contenido") String contenidoEditora,
                       @FormParam("idioma") String idioma,
                       @FormParam("contenidoId") String contenidoId)
    {
        Autoguardado autoguardado = new Autoguardado();
        NodoMapa nodoMapa = nodoMapaService.getNodoMapaByMapaId(Long.parseLong(mapaId)).get(0);
        Long personaId = AccessManager.getConnectedUserId(request);

        autoguardado.setContenidoEditora(contenidoEditora);
        autoguardado.setIdiomaCodigoIso(idioma);
        autoguardado.setNodoMapa(nodoMapa);
        autoguardado.setPerId(personaId);

        if (contenidoId != null && !contenidoId.isEmpty())
        {
            Contenido contenido = contenidoService.getContenido(Long.parseLong(contenidoId));

            autoguardado.setContenido(contenido);
        }

        autoguardadoService.insert(autoguardado);
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void delete(@FormParam("mapaId") String mapaId,
                       @FormParam("contenidoId") String contenidoId)
    {
        Long personaId = AccessManager.getConnectedUserId(request);

        Long contenidoIdParsed = null;

        if (contenidoId != null && !contenidoId.isEmpty())
        {
            contenidoIdParsed = Long.parseLong(contenidoId);
        }

        autoguardadoService.delete(Long.parseLong(mapaId), contenidoIdParsed, personaId);
    }

    @GET
    public UIEntity get(@QueryParam("mapaId") Long mapaId)
    {
        Long personaId = AccessManager.getConnectedUserId(request);

        Autoguardado autoguardadoCA = autoguardadoService.getAutoguardadoMasActualByParameters(mapaId, null, personaId, "CA");
        Autoguardado autoguardadoES = autoguardadoService.getAutoguardadoMasActualByParameters(mapaId, null, personaId, "ES");
        Autoguardado autoguardadoEN = autoguardadoService.getAutoguardadoMasActualByParameters(mapaId, null, personaId, "EN");

        UIEntity entity = new UIEntity();

        if (autoguardadoCA != null)
        {
            entity.put("autoguardadoCA", autoguardadoCA.getContenidoEditora());
        }

        if (autoguardadoES != null)
        {
            entity.put("autoguardadoES", autoguardadoES.getContenidoEditora());
        }

        if (autoguardadoEN != null)
        {
            entity.put("autoguardadoEN", autoguardadoEN.getContenidoEditora());
        }

        return entity;
    }

}
