package es.uji.apps.upo.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.exceptions.TagExisteException;
import es.uji.apps.upo.model.Tag;
import es.uji.apps.upo.services.TagService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("tags")
public class TagResource extends CoreBaseService {

    @InjectParam
    private TagService tagService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAllTags() {
        return UIEntity.toUI(tagService.getAllTags());
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateValue(@PathParam("id") Long id, UIEntity entity) {
        Long userId = AccessManager.getConnectedUserId(request);
        String tagName = entity.get("tagname");

        ParamUtils.checkNotNull(tagName);

        tagService.update(id, tagName, userId);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertValue(UIEntity entity) throws TagExisteException {
        Long userId = AccessManager.getConnectedUserId(request);
        String tagName = entity.get("tagname");

        ParamUtils.checkNotNull(tagName);

        Tag tag = new Tag(tagName);

        return UIEntity.toUI(tagService.insert(tag, userId));
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteValue(@PathParam("id") Long id) {
        Long userId = AccessManager.getConnectedUserId(request);

        tagService.deleteById(id, userId);
    }

    @GET
    @Path("channels")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTagsAsChannels() {
        List<UIEntity> entities = new ArrayList<>();

        for (String tag : tagService.getTagsAsChannels()) {
            UIEntity entity = new UIEntity();

            entity.put("tag", tag);

            entities.add(entity);
        }

        return entities;
    }
}
