package es.uji.apps.upo.services.rest.metadatos;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.upo.model.metadatos.AtributoMetadato;
import es.uji.apps.upo.model.metadatos.EsquemaMetadato;
import es.uji.apps.upo.model.metadatos.TipoMetadato;
import es.uji.apps.upo.model.metadatos.ValorMetadato;
import es.uji.apps.upo.services.AtributoMetadatoService;
import es.uji.apps.upo.services.EsquemaMetadatoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;
import java.util.*;
import java.util.stream.Collectors;

@Path("esquema-metadato")
public class EsquemaResource extends CoreBaseService
{
    public static final long ID_LISTA_DESPLEGABLE = 4L;
    public static final long ID_NO_EXISTENTE = -1L;
    @InjectParam
    EsquemaMetadatoService esquemaMetadatoService;

    @InjectParam
    AtributoMetadatoService atributoMetadatoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEsquemas()
    {
        return UIEntity.toUI(esquemaMetadatoService.getAll());
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addEsquema(UIEntity entity)
    {
        Long userId = AccessManager.getConnectedUserId(request);

        if (entity.get("nombre") == null)
        {
            return new UIEntity();
        }

        return UIEntity.toUI(esquemaMetadatoService.insert(entity.toModel(EsquemaMetadato.class), userId));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateEsquema(@PathParam("id") Long id, UIEntity entity)
    {
        Long userId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(entity.get("id"), entity.get("nombre"));

        return UIEntity.toUI(esquemaMetadatoService.update(entity.toModel(EsquemaMetadato.class), userId));
    }

    @DELETE
    @Path("{id}")
    public void deleteEsquema(@PathParam("id") Long id)
    {
        Long userId = AccessManager.getConnectedUserId(request);

        esquemaMetadatoService.delete(id, userId);
    }

    @POST
    @Path("{esquemaId}/atributo")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage addOrUpdateAtributo(@FormParam("id") Long id, @FormParam("clave") String clave,
            @FormParam("nombreClaveCA") String nombreClaveCA, @FormParam("nombreClaveES") String nombreClaveES,
            @FormParam("nombreClaveEN") String nombreClaveEN, @PathParam("esquemaId") Long esquemaId,
            @FormParam("tipoMetadatoId") Long tipoId, @FormParam("valorDefectoCA") String valorDefectoCA,
            @FormParam("valorDefectoES") String valorDefectoES, @FormParam("valorDefectoEN") String valorDefectoEN,
            @FormParam("permiteBlanco") Integer permiteBlanco, @FormParam("publicable") Integer publicable,
            @FormParam("orden") Integer orden, @FormParam("valores") String valores)
            throws JAXBException
    {
        Long userId = AccessManager.getConnectedUserId(request);

        publicable = (publicable == null ? 0 : 1);
        permiteBlanco = (permiteBlanco == null ? 0 : 1);

        ParamUtils.checkNotNull(clave, nombreClaveCA, nombreClaveEN, nombreClaveES, esquemaId, tipoId, permiteBlanco,
                publicable, orden);

        AtributoMetadato atributoMetadato = new AtributoMetadato();
        HashSet<ValorMetadato> valoresMetadatos = getValoresMetadatos(valores, atributoMetadato);
        List<Long> idValoresMetadatos = new ArrayList<>();

        for (ValorMetadato valorMetadato : valoresMetadatos)
        {
            idValoresMetadatos.add(valorMetadato.getId() != null ? valorMetadato.getId() : ID_NO_EXISTENTE);
        }

        if (id != null)
        {
            atributoMetadatoService.deleteAtributosValoresByAtributoIdAndValoresMetadatos(id, idValoresMetadatos,
                    userId);
            atributoMetadato = atributoMetadatoService.getById(id);
        }

        TipoMetadato tipoMetadato = esquemaMetadatoService.getTipoMetadatos(tipoId);
        EsquemaMetadato esquemaMetadato = esquemaMetadatoService.getEsquemaMetadato(esquemaId);

        atributoMetadato.setTipoMetadato(tipoMetadato);
        atributoMetadato.setEsquemaMetadato(esquemaMetadato);
        atributoMetadato.setClave(clave);
        atributoMetadato.setNombreClaveCA(nombreClaveCA);
        atributoMetadato.setNombreClaveES(nombreClaveES);
        atributoMetadato.setNombreClaveEN(nombreClaveEN);
        atributoMetadato.setPermiteBlanco(permiteBlanco);
        atributoMetadato.setPublicable(publicable);
        atributoMetadato.setOrden(orden);

        if (tipoMetadato.getId() == ID_LISTA_DESPLEGABLE)
        {
            atributoMetadato.setValorDefectoCA(null);
            atributoMetadato.setValorDefectoEN(null);
            atributoMetadato.setValorDefectoES(null);
        }
        else
        {
            atributoMetadato.setValorDefectoCA(valorDefectoCA);
            atributoMetadato.setValorDefectoEN(valorDefectoEN);
            atributoMetadato.setValorDefectoES(valorDefectoES);
        }

        atributoMetadatoService.insertOrUpdateAtributoMetadato(atributoMetadato,
                getValoresMetadatos(valores, atributoMetadato), userId);

        return new ResponseMessage(true);
    }

    private HashSet<ValorMetadato> getValoresMetadatos(String valores, AtributoMetadato atributoMetadato)
    {
        HashSet<ValorMetadato> valoresMetadatos = new HashSet<ValorMetadato>();

        String[] valoresArray = valores.split("\\|\\|");
        for (String valor : valoresArray)
        {
            if (valor == null || valor.isEmpty())
            {
                break;
            }

            String[] subvaloresArray = valor.split("\\|");

            ValorMetadato valorMetadato = new ValorMetadato();

            for (String subvalor : subvaloresArray)
            {
                String key = subvalor.substring(0, subvalor.indexOf(':'));
                String value = subvalor.substring(subvalor.indexOf(':') + 1);

                if ("ID".equals(key) && value != null && !value.isEmpty())
                {
                    valorMetadato.setId(new Long(value));
                }

                if ("CA".equals(key))
                {
                    valorMetadato.setValorCA(value);
                }

                if ("ES".equals(key))
                {
                    valorMetadato.setValorES(value);
                }

                if ("EN".equals(key))
                {
                    valorMetadato.setValorEN(value);
                }

                if ("ORDEN".equals(key))
                {
                    valorMetadato.setOrden(Integer.parseInt(value));
                }
            }

            valorMetadato.setAtributoMetadato(atributoMetadato);
            valoresMetadatos.add(valorMetadato);
        }

        return valoresMetadatos;
    }

    @DELETE
    @Path("{esquemaId}/atributo/{atributoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteAtributo(@PathParam("atributoId") Long atributoId)
    {

        atributoMetadatoService.deleteAtributoMetadato(atributoId, AccessManager.getConnectedUserId(request));
    }

    @GET
    @Path("{esquemaId}/atributo")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAtributos(@PathParam("esquemaId") Long esquemaId)
    {
        List<AtributoMetadato> atributos = atributoMetadatoService.getByEsquemaId(esquemaId);
        List<UIEntity> entidades = new ArrayList<UIEntity>();

        for (AtributoMetadato atributo : atributos)
        {
            UIEntity entidad = UIEntity.toUI(atributo);

            if (atributo.getValoresMetadatos().size() > 0)
            {
                setValoresMetadatosToUiEntity(ordenaValores(atributo.getValoresMetadatos()), entidad);
            }

            entidades.add(entidad);
        }

        return entidades;
    }

    @GET
    @Path("{esquemaId}/atributo/{atributoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getAtributos(@PathParam("esquemaId") Long esquemaId, @PathParam("atributoId") Long atributoId)
    {
        AtributoMetadato atributoMetadato = atributoMetadatoService.getById(atributoId);

        UIEntity entity = UIEntity.toUI(atributoMetadato);

        setValoresMetadatosToUiEntity(ordenaValores(atributoMetadato.getValoresMetadatos()), entity);

        return entity;
    }

    private void setValoresMetadatosToUiEntity(List<ValorMetadato> metadatos, UIEntity entidad)
    {
        for (ValorMetadato valor : metadatos)
        {
            UIEntity entity = new UIEntity();

            entity.put("id", valor.getId());
            entity.put("valorCA", valor.getValorCA());
            entity.put("valorES", valor.getValorES());
            entity.put("valorEN", valor.getValorEN());
            entity.put("orden", valor.getOrden());

            entidad.put("valores", entity);
        }
    }

    private List<ValorMetadato> ordenaValores(Set<ValorMetadato> valores)
    {
        return valores.stream().sorted(Comparator.comparingLong(ValorMetadato::getOrden)).collect(Collectors.toList());
    }
}
