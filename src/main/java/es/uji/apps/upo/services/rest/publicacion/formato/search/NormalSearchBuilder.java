package es.uji.apps.upo.services.rest.publicacion.formato.search;

import es.uji.apps.upo.services.rest.publicacion.model.RequestParams;

public class NormalSearchBuilder extends SearchBuilder
{
    private String orderSearch;

    public NormalSearchBuilder(RequestParams requestParams)
    {
        super(requestParams.getUrl(),
                requestParams.getConfigPublicacion().getIdioma());

        this.orderSearch = requestParams.getSearchParams().getOrden();
    }

    public String generate()
    {
        return generaCabecera() + generaCondicionesCuerpo() + generaCondicionesMetadatos() + pie();
    }

    private String generaCondicionesCuerpo()
    {
        if (expresionesCuerpo.size() == 0)
        {
            return "";
        }

        AndExpression andExpression = new AndExpression();
        andExpression.expressions = expresionesCuerpo;

        return " and " + andExpression.generate();
    }

    private String generaCondicionesMetadatos()
    {
        if (expresionesMetadatos.size() == 0)
        {
            return "";
        }

        String sql = "";

        for (Expression expression : expresionesMetadatos)
        {
            sql += cabeceraMetadatos() + " and " + expression.generate() + pieMetadatos();
        }

        return sql;
    }

    private String generaCabecera()
    {
        if (expresionesCabecera.size() == 0)
        {
            return cabecera() + subCabecera();
        }

        AndExpression andExpression = new AndExpression();
        andExpression.expressions = expresionesCabecera;

        return cabecera() + " and " + andExpression.generate() + subCabecera();
    }

    private String cabecera()
    {
        return "select MAPA_URL_COMPLETA " +
                " from UPO_VW_BUSQUEDA " +
                "where MAPA_URL_COMPLETA in (  select distinct mapa_url_completa " +
                "                                from ( select mapa_url_completa from  UPO_VW_BUSQUEDA_SIN_FILTRAR " +
                "                                        where (MAPA_URL_COMPLETA like '" + urlCompleta + "%') " +
                "                                              and lower(IDIOMA_CODIGO_ISO)= '" + idioma + "' " +
                "                                              and (TITULO_LARGO is not null) ";
    }

    private String subCabecera()
    {
        return "                                     ) " +
                "                           ) " +
                " and lower(IDIOMA_CODIGO_ISO)= '" + idioma + "' ";
    }

    private String cabeceraMetadatos()
    {
        return " and exists (select * from upo_vw_metadatos\n" +
                "              where idioma_codigo_iso = idioma \n" +
                "                and objeto_id = contenido_id";
    }

    private String pieMetadatos()
    {
        return " )";
    }

    private String pie()
    {
        if (this.orderSearch != null && !this.orderSearch.isEmpty()) return " order by " + this.orderSearch;

        return " order by prioridad asc, DISTANCIA_SYSDATE asc, orden asc, decode(es_html, 'S', fecha_creacion, to_date('1/1/1900')) desc, orden_mapa asc";
    }
}
