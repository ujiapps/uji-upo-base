package es.uji.apps.upo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.upo.dao.ItemMigracionDAO;
import es.uji.apps.upo.model.ItemMigracion;

import java.util.List;

@Service
public class ItemMigracionService
{
    private static final String CATALAN = "CA";
    private static final String ESPANOL = "ES";
    private static final String INGLES = "UK";

    private ItemMigracionDAO itemMigracionDAO;

    @Autowired
    public ItemMigracionService(ItemMigracionDAO itemMigracionDAO)
    {
        this.itemMigracionDAO = itemMigracionDAO;
    }

    public ItemMigracion getItems(String url)
    {
        return itemMigracionDAO.getContent(url);
    }

    public ItemMigracion getItem(String idioma, String url)
    {
        if (url == null)
        {
            return null;
        }

        if (idioma == CATALAN)
        {
            return itemMigracionDAO.getContent(convertURlToCA(url));
        }

        if (idioma == ESPANOL)
        {
            return itemMigracionDAO.getContent(convertURlToES(url));
        }

        if (idioma == INGLES)
        {
            return itemMigracionDAO.getContent(convertURlToUK(url));
        }

        return null;
    }

    private String convertURlToCA(String url)
    {
        if (hasUrlPrefix(url))
        {
            return setUrlStartWith(url, CATALAN);
        }

        return url;
    }

    private String convertURlToES(String url)
    {
        return setUrlStartWith(url, ESPANOL);
    }

    private String convertURlToUK(String url)
    {
        return setUrlStartWith(url, INGLES);
    }

    private boolean hasUrlPrefix(String url)
    {
        return (urlStartWith(url, CATALAN) || urlStartWith(url, ESPANOL) || urlStartWith(url, INGLES));
    }

    private boolean urlStartWith(String url, String idioma)
    {
        String idiomaBetweenSlashes = "/" + idioma + "/";

        return url.startsWith(idiomaBetweenSlashes);
    }

    private String setUrlStartWith(String url, String idioma)
    {
        String urlSeted = url;

        if (urlStartWith(url, CATALAN) || urlStartWith(url, ESPANOL) || urlStartWith(url, INGLES))
        {
            urlSeted = urlSeted.substring(4);
        }

        if (urlSeted.startsWith("/"))
        {
            return "/" + idioma + urlSeted;
        }

        return "/" + idioma + "/" + urlSeted;
    }
}
