package es.uji.apps.upo.services.rest.publicacion.formato;

import com.sun.syndication.io.FeedException;
import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.model.Publicacion;
import es.uji.apps.upo.services.rest.publicacion.formato.search.CondicionFiltro;
import es.uji.apps.upo.services.rest.publicacion.model.PublicacionDetails;
import es.uji.apps.upo.services.rest.publicacion.model.RequestParams;
import es.uji.commons.web.template.Template;
import es.uji.commons.web.template.model.Pagina;
import es.uji.commons.web.template.model.Recurso;
import es.uji.commons.web.template.model.Seccion;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class FormatoPublicacionHtmlPaginado extends AbstractPublicacionHtml implements FormatoPublicacion
{
    private static final int NUMERO_RESULTADOS = 10;

    @Override
    public Response publica(RequestParams requestParams)
            throws MalformedURLException, ParseException, FeedException, UnsupportedEncodingException
    {
        NodoMapa nodoMapa = requestParams.getNodoMapa();

        if (nodoMapa == null)
        {
            return new PaginaNoEncontrada().publica(requestParams);
        }

        if (requestParams.getSearchParams().getNumResultados() == null)
        {
            requestParams.getSearchParams().setNumResultados(NUMERO_RESULTADOS);
        }

        if (requestParams.getSearchParams().getPageSearch() != null)
        {
            requestParams.getSearchParams()
                    .setStartSearch(
                            (requestParams.getSearchParams().getPageSearch() - 1) * requestParams.getSearchParams()
                                    .getNumResultados());
        }

        Map<String, Object> returnValues =
                publicacionService.getContenidosIdiomasByUrlCompletaAndLanguageAndSearchFields(requestParams);

        Integer numSearchItems = (Integer) returnValues.get("numSearchItems");
        List<Publicacion> contenidos = (List<Publicacion>) returnValues.get("search");

        PublicacionDetails publicacionDetails = new PublicacionDetails();
        publicacionDetails.setUrl(requestParams.getUrl());
        publicacionDetails.setUrlBase(requestParams.getUrlBase());
        publicacionDetails.setContenidos(contenidos);
        publicacionDetails.setNodoMapa(nodoMapa);
        publicacionDetails.setRequestDetails(requestParams.getRequestDetails());
        publicacionDetails.setIdioma(requestParams.getConfigPublicacion().getIdioma());

        Pagina pagina = buildPagina(publicacionDetails);

        addContenidosNodo(pagina, publicacionDetails);
        updateTitulo(pagina, publicacionDetails);

        addParametros(pagina, requestParams.getSearchParams().getFiltrosCabecera());
        addParametros(pagina, requestParams.getSearchParams().getFiltrosCuerpo());
        addParametros(pagina, requestParams.getSearchParams().getFiltrosMetadatos());

        pagina.setHasSearch(true);
        pagina.setValuesForNextAndPreviousPage(requestParams.getSearchParams().getStartSearch(),
                requestParams.getSearchParams().getNumResultados(), numSearchItems,
                requestParams.getSearchParams().getPageSearch());

        Template template =
                buildHTMLTemplate(pagina, requestParams.getConfigPublicacion().getPlantilla(), publicacionDetails);

        return Response.ok(template).type(MediaType.TEXT_HTML).build();
    }

    private void addContenidosNodo(Pagina pagina, PublicacionDetails publicacionDetails)
            throws UnsupportedEncodingException, ParseException
    {
        List<Publicacion> contenidosMapa =
                publicacionService.getContenidosIdiomasByUrlNodoAndLanguage(publicacionDetails.getUrl(),
                        publicacionDetails.getIdioma());

        for (Publicacion contenido : contenidosMapa)
        {
            if (contenido.getUrlNodo().equals(publicacionDetails.getUrl()))
            {
                pagina.add(createRecurso(publicacionDetails.getUrlBase(), contenido,
                        publicacionDetails.getRequestDetails(), publicacionDetails.getIdioma()));
            }
        }
    }

    private void updateTitulo(Pagina pagina, PublicacionDetails publicacionDetails)
    {
        if (pagina.getTitulo() == null || pagina.getTitulo().isEmpty())
        {
            Optional<Recurso> recurso = pagina.getRecursos().stream().filter(r -> "S".equals(r.getEsHtml())).findAny();

            if (recurso.isPresent()) pagina.setTitulo(recurso.get().getTituloLargo());
        }
    }

    private void addParametros(Pagina pagina, List<CondicionFiltro> filtros)
    {
        for (CondicionFiltro condicionFiltro : filtros)
        {
            pagina.addParametro(condicionFiltro.getClaveOriginal(), condicionFiltro.getValor());
        }
    }

    @Override
    public Pagina buildPagina(PublicacionDetails publicacionDetails)
            throws ParseException, MalformedURLException, UnsupportedEncodingException
    {
        Pagina pagina = super.buildPagina(publicacionDetails);

        List<Publicacion> contenidos = publicacionDetails.getContenidos();

        if (contenidos != null && !contenidos.isEmpty())
        {
            Seccion seccionActual = pagina;

            for (Publicacion contenido : contenidos)
            {
                Seccion nuevaSeccion =
                        new Seccion(publicacionDetails.getUrlBase(), contenido.getUrlNodo(), contenido.getUrlCompleta(),
                                null, null, true);
                nuevaSeccion = seccionActual.addSeccion(nuevaSeccion);

                Recurso recurso = createRecurso(publicacionDetails.getUrlBase(), contenido, publicacionDetails.getIdioma());

                nuevaSeccion.add(recurso);
            }
        }

        return pagina;
    }
}
