package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.FranquiciaAccesoDAO;
import es.uji.apps.upo.model.FranquiciaAcceso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FranquiciaAccesoService
{
    private FranquiciaAccesoDAO franquiciaAccesoDAO;

    @Autowired
    public FranquiciaAccesoService(FranquiciaAccesoDAO franquiciaAccesoDAO)
    {
        this.franquiciaAccesoDAO = franquiciaAccesoDAO;
    }

    public FranquiciaAcceso getFranquiciaAccesoByFranquiciaIdAndPersonaId(
            Long franquiciaId,Long personaId)
    {
        return franquiciaAccesoDAO.getFranquiciaAccesoByFranquiciaIdAndPersonaId(franquiciaId,
                personaId);
    }
}
