package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.ContenidoIdiomaLogDAO;
import es.uji.apps.upo.model.ContenidoIdiomaLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ContenidoIdiomaLogService
{
    private final ContenidoIdiomaLogDAO contenidoIdiomaLogDAO;

    @Autowired
    public ContenidoIdiomaLogService(ContenidoIdiomaLogDAO contenidoIdiomaLogDAO)
    {
        this.contenidoIdiomaLogDAO = contenidoIdiomaLogDAO;
    }

    @Transactional
    public void insert(ContenidoIdiomaLog contenidoIdiomaLog)
    {
        contenidoIdiomaLogDAO.insert(contenidoIdiomaLog);
    }
}
