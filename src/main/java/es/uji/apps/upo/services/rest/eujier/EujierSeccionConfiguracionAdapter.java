package es.uji.apps.upo.services.rest.eujier;

import es.uji.apps.upo.dao.eujier.EujierDAO;
import es.uji.apps.upo.model.eujier.EujierItem;
import es.uji.apps.upo.model.eujier.EujierSeccion;
import es.uji.apps.upo.model.eujier.EujierSeccionConfiguracion;
import es.uji.apps.upo.model.eujier.enums.GrupoPerfil;
import es.uji.apps.upo.services.rest.eujier.EujierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.ws.rs.core.MultivaluedMap;
import java.util.*;

@Component
public class EujierSeccionConfiguracionAdapter
{
    private EujierDAO eujierDAO;
    private EujierService itemService;

    @Autowired
    public EujierSeccionConfiguracionAdapter(EujierDAO eujierDAO, EujierService itemService)
    {
        this.eujierDAO = eujierDAO;
        this.itemService = itemService;
    }

    public Map<String, EujierSeccionConfiguracion> adaptaSecciones(String idioma, MultivaluedMap<String, String> params,
            Long connectedUserId)
    {
        Map<String, EujierSeccionConfiguracion> secciones;

        Long grupoPerfilId = 0L;
        secciones = new LinkedHashMap<>();
        List<EujierSeccion> seccionesConfiguracion = eujierDAO.getSecciones(idioma, connectedUserId);

        if (seccionesConfiguracion.size() > 0)
        {
            grupoPerfilId = seccionesConfiguracion.get(0).getGrupoPerfilId();
        }

        for (EujierSeccion seccionConfiguracion : seccionesConfiguracion)
        {
            EujierSeccionConfiguracion eujierSeccionConfig = buildSeccionConfiguracion(seccionConfiguracion, idioma,
                    params.getFirst(seccionConfiguracion.getCodigo()), connectedUserId, grupoPerfilId);

            secciones.put(seccionConfiguracion.getCodigo(), eujierSeccionConfig);
        }

        return secciones;
    }

    public EujierSeccionConfiguracion adaptaSeccion(String idioma, String codigoSeccion, Long connectedUserId,
            String vista)
    {
        EujierSeccion seccion = eujierDAO.getSeccionByCodigo(idioma, connectedUserId, codigoSeccion);

        return buildSeccionConfiguracion(seccion, idioma, vista, connectedUserId, seccion.getGrupoPerfilId());
    }

    private EujierSeccionConfiguracion buildSeccionConfiguracion(EujierSeccion configuracion, String idioma,
            String vista, Long connectedUserId, Long grupoPerfilId)
    {
        EujierSeccionConfiguracion seccionConfiguracion = new EujierSeccionConfiguracion();

        seccionConfiguracion.setConfiguracion(configuracion);
        seccionConfiguracion.toggleVista(vista, configuracion);
        seccionConfiguracion.setItems(getItems(configuracion.getCodigo(), idioma, connectedUserId, grupoPerfilId,
                configuracion.getNumItems()));

        return seccionConfiguracion;
    }

    private List<EujierItem> getItems(String codigo, String idioma, Long connectedUserId, Long grupoPerfilId,
            Long numItems)
    {
        List<EujierItem> items = new ArrayList<>();

        if (GrupoPerfil.FAVORITOS.toString().equalsIgnoreCase(codigo))
        {
            items = eujierDAO.getFavoritos(idioma, connectedUserId, numItems);
        }

        if (GrupoPerfil.DESTACADOS.toString().equalsIgnoreCase(codigo))
        {
            items = eujierDAO.getDestacados(idioma, connectedUserId, grupoPerfilId, numItems);
        }

        if (GrupoPerfil.NUEVOS.toString().equalsIgnoreCase(codigo))
        {
            items = eujierDAO.getNuevos(idioma, connectedUserId, numItems);
        }

        if (GrupoPerfil.USOPROPIO.toString().equalsIgnoreCase(codigo))
        {
            items = eujierDAO.getMasUsados(idioma, connectedUserId, numItems);
        }

        if (GrupoPerfil.USOPERFIL.toString().equalsIgnoreCase(codigo))
        {
            items = eujierDAO.getMasUsadosPerfil(idioma, connectedUserId, grupoPerfilId, numItems);
        }

        return itemService.setAplicacionesToItems(items, idioma);
    }
}
