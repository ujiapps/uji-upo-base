package es.uji.apps.upo.services.nodomapa;

import es.uji.apps.upo.dao.ContenidoDAO;
import es.uji.apps.upo.dao.NodoMapaDAO;
import es.uji.apps.upo.exceptions.*;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.*;
import es.uji.apps.upo.services.*;
import es.uji.apps.upo.utils.DateUtils;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class NodoMapaService extends AuthenticatedService
{
    public static final long ID_NODO_RAIZ = -1L;
    public static final String URL_COMPLETA_PAPELERA = "/paperera/";
    private NodoMapaDAO nodoMapaDAO;
    private ContenidoDAO contenidoDAO;
    private NodoMapaPlantillaService nodoMapaPlantillaService;
    private IdiomaService idiomaService;
    private ContenidoService contenidoService;
    private PersonaService personaService;
    private NodoMapaContenidoService nodoMapaContenidoService;
    private ContenidoIdiomaService contenidoIdiomaService;
    private NotificacionService notificacionService;
    private MenuService menuService;

    @Autowired
    public NodoMapaService(PersonaService personaService, NodoMapaDAO nodoMapaDAO, ContenidoDAO contenidoDAO, NodoMapaPlantillaService nodoMapaPlantillaService, IdiomaService idiomaService, NotificacionService notificacionService)
    {
        super(personaService);

        this.personaService = personaService;
        this.nodoMapaDAO = nodoMapaDAO;
        this.contenidoDAO = contenidoDAO;
        this.nodoMapaPlantillaService = nodoMapaPlantillaService;
        this.idiomaService = idiomaService;
        this.notificacionService = notificacionService;
    }

    @Autowired
    public void setMenuService(MenuService menuService)
    {
        this.menuService = menuService;
    }

    @Autowired
    public void setContenidoService(ContenidoService contenidoService)
    {
        this.contenidoService = contenidoService;
    }

    @Autowired
    public void setNodoMapaContenidoService(NodoMapaContenidoService nodoMapaContenidoService)
    {
        this.nodoMapaContenidoService = nodoMapaContenidoService;
    }

    @Autowired
    public void setContenidoIdiomaService(ContenidoIdiomaService contenidoIdiomaService)
    {
        this.contenidoIdiomaService = contenidoIdiomaService;
    }

    public void setPersonaService(PersonaService personaService)
    {
        this.personaService = personaService;
    }

    public void crearContenidoDesdeFichero(Persona persona, String fileName, String tipoMime, byte[] file, Long mapaId) throws InsertadoContenidoNoAutorizadoException, ContenidoConMismoUrlPathException
    {
        NodoMapa nodoMapa = getNodoMapa(mapaId, true);

        ParamUtils.checkNotNull(persona);

        if (!personaAdminEnNodo(nodoMapa, persona))
        {
            throw new InsertadoContenidoNoAutorizadoException();
        }

        Contenido contenido = new Contenido(fileName);

        compruebaQueNoHayaContenidosRepetidos(nodoMapa, fileName);

        NodoMapaContenido nodoMapaContenido = new NodoMapaContenido();
        nodoMapaContenido.setEstadoModeracion(EstadoModeracion.ACEPTADO);
        nodoMapaContenido.setUpoMapa(nodoMapa);
        nodoMapaContenido.setUpoObjeto(contenido);

        nodoMapa.addNodoMapaContenidoRelacionado(nodoMapaContenido);
        contenido.setUpoMapasObjetos(Collections.singleton(nodoMapaContenido));

        for (Idioma idioma : idiomaService.getIdiomas())
        {
            crearContenidoIdiomaParaFichero(fileName, tipoMime, file, contenido, idioma);
        }

        setDefaultValuesForContenido(nodoMapa, persona, contenido);
        contenido.setPublicable("S");

        contenidoDAO.insert(contenido, persona.getId());
    }

    private void crearContenidoIdiomaParaFichero(String fileName, String tipoMime, byte[] file, Contenido contenido, Idioma idioma)
    {
        ContenidoIdioma contenidoIdioma = new ContenidoIdioma(idioma);
        contenidoIdioma.setHtml("N");
        contenidoIdioma.setTipoFormato(TipoFormatoContenido.BINARIO.toString());

        contenidoIdioma.setUpoObjeto(contenido);

        Set<ContenidoIdioma> contenidoIdiomas = contenido.getContenidoIdiomas();
        contenidoIdiomas.add(contenidoIdioma);
        contenido.setContenidoIdiomas(contenidoIdiomas);

        contenidoIdioma.setContenido(file);
        contenidoIdioma.setMimeType(tipoMime);
        contenidoIdioma.setNombreFichero(fileName);
        contenidoIdioma.setLongitud(Long.valueOf(file.length));
    }

    public List<Plantilla> getPlantillasByMapaId(Long mapaId)
    {
        return nodoMapaDAO.getPlantillasByMapaId(mapaId);
    }

    public List<NodoMapa> getHijos(Long nodoId)
    {
        return nodoMapaDAO.getHijos(nodoId);
    }

    public List<NodoMapa> getDescendientes(String urlBase)
    {
        return nodoMapaDAO.getDescendientes(urlBase);
    }

    public NodoMapa getNodoMapa(Long nodoMapaId, boolean fetchAll)
    {
        List<NodoMapa> listaMapas;

        if (fetchAll)
        {
            listaMapas = nodoMapaDAO.getNodoMapaAndRelations(nodoMapaId);
        } else
        {
            listaMapas = nodoMapaDAO.get(NodoMapa.class, nodoMapaId);
        }

        if (listaMapas != null && !listaMapas.isEmpty())
        {
            return listaMapas.get(0);
        }
        return null;
    }

    public List<NodoMapa> getNodoMapaByMapaId(long mapaId)
    {
        return nodoMapaDAO.getNodoMapaById(mapaId);
    }

    public NodoMapa getNodoMapaByUrlCompleta(String urlCompleta)
    {
        return nodoMapaDAO.getNodoMapaByUrlCompleta(urlCompleta);
    }

    public List<Idioma> getIdiomasObligatorios(Long nodoMapaId)
    {
        return nodoMapaDAO.getIdiomasObligatorios(nodoMapaId);
    }

    public void comprobarQueNoHayaNodosHijoConElMimoNombre(NodoMapa nodoMapaPadre, String comparador) throws NodoConNombreYaExistenteEnMismoPadreException
    {
        if (nodoMapaPadre != null)
        {
            List<NodoMapa> nodosHijo = getHijos(nodoMapaPadre.getId());

            for (NodoMapa nodoHijo : nodosHijo)
            {
                if (nodoHijo.getUrlPath().equals(comparador))
                {
                    throw new NodoConNombreYaExistenteEnMismoPadreException();
                }
            }
        }
    }

    public void insertarPlantilla(Persona persona, Plantilla plantilla, NodoMapa nodoMapa, Integer nivel) throws InsertarPlantillaNoAutorizadoException, InsertadaPlantillaDelMismoTipoException
    {
        ParamUtils.checkNotNull(persona);

        if (!personaAdminEnNodo(nodoMapa, persona))
        {
            throw new InsertarPlantillaNoAutorizadoException();
        }

        verificarQueNoExistaUnaPlantillaDelMismoTipo(nodoMapa.getId(), plantilla);
        nodoMapaDAO.insertarPlantilla(nodoMapa.getId(), plantilla.getId(), nivel);
    }

    private void verificarQueNoExistaUnaPlantillaDelMismoTipo(Long nodoMapaId, Plantilla plantilla) throws InsertadaPlantillaDelMismoTipoException
    {
        List<NodoMapaPlantilla> nodoMapaPlantillas = nodoMapaPlantillaService.getNodoMapaPlantillaByNodoMapaId(nodoMapaId);

        for (NodoMapaPlantilla nodoMapaPlantilla : nodoMapaPlantillas)
        {
            if (nodoMapaPlantilla.getUpoPlantilla().getTipo().equals(plantilla.getTipo()))
            {
                throw new InsertadaPlantillaDelMismoTipoException();
            }
        }
    }

    public void deletePlantilla(Persona persona, NodoMapa nodoMapa, Long plantillaId) throws BorradoContenidoNoAutorizadoException
    {
        ParamUtils.checkNotNull(persona);

        if (!personaAdminEnNodo(nodoMapa, persona))
        {
            throw new BorradoContenidoNoAutorizadoException();
        }

        nodoMapaDAO.deletePlantilla(nodoMapa.getId(), plantillaId);
    }

    public void insertOrUpdate(Persona persona, NodoMapa nodoMapa) throws InsertadoContenidoNoAutorizadoException, NodoConNombreYaExistenteEnMismoPadreException, ActualizadoContenidoNoAutorizadoException
    {
        if (nodoMapa.getId() != null)
        {
            update(persona, nodoMapa);
        } else
        {
            insert(persona, nodoMapa);
        }
    }

    @Transactional
    public Long update(Persona persona, NodoMapa nodoMapa) throws ActualizadoContenidoNoAutorizadoException, NodoConNombreYaExistenteEnMismoPadreException
    {
        ParamUtils.checkNotNull(persona);

        comprobarQueNoHayaNodosHijoConElMimoNombre(nodoMapa.getUpoMapa(), nodoMapa);

        if (!personaAdminEnNodo(nodoMapa, persona))
        {
            throw new ActualizadoContenidoNoAutorizadoException();
        }

        return nodoMapaDAO.update(nodoMapa).getId();
    }

    private void esAdministradorDeLosDosNodos(NodoMapa nodoMapaOrigen, Persona persona) throws ActualizadoContenidoNoAutorizadoException
    {
        ParamUtils.checkNotNull(persona);

        if (!personaAdminEnNodo(nodoMapaOrigen, persona))
        {
            throw new ActualizadoContenidoNoAutorizadoException();
        }
    }

    private void comprobarQueNoHayaNodosHijoConElMimoNombre(NodoMapa nodoMapaPadre, NodoMapa nodoMapa) throws NodoConNombreYaExistenteEnMismoPadreException
    {
        if (nodoMapa != null)
        {
            Long idNodoPadre = ID_NODO_RAIZ;

            if (nodoMapaPadre != null)
            {
                idNodoPadre = nodoMapaPadre.getId();
            }

            for (NodoMapa nodoHijo : getHijos(idNodoPadre))
            {
                if (nodoHijo.getUrlPath().equals(nodoMapa.getUrlPath()) && !nodoHijo.getId().equals(nodoMapa.getId()))
                {
                    throw new NodoConNombreYaExistenteEnMismoPadreException();
                }
            }
        }
    }

    @Transactional
    public Long updateForced(Persona persona, NodoMapa nodoMapa) throws NodoConNombreYaExistenteEnMismoPadreException
    {
        ParamUtils.checkNotNull(persona);

        comprobarQueNoHayaNodosHijoConElMimoNombre(nodoMapa.getUpoMapa(), nodoMapa);

        return nodoMapaDAO.update(nodoMapa).getId();
    }

    @Transactional
    public void insert(Persona persona, NodoMapa nodoMapa) throws InsertadoContenidoNoAutorizadoException, NodoConNombreYaExistenteEnMismoPadreException
    {
        ParamUtils.checkNotNull(persona);

        comprobarQueNoHayaNodosHijoConElMimoNombre(nodoMapa.getUpoMapa(), nodoMapa);

        if (!personaAdminEnNodo(nodoMapa, persona))
        {
            throw new InsertadoContenidoNoAutorizadoException();
        }

        nodoMapa.setNodoMapaDAO(nodoMapaDAO);
        nodoMapaDAO.insert(nodoMapa);
    }

    @Transactional
    public void insertForced(Persona persona, NodoMapa nodoMapa) throws NodoConNombreYaExistenteEnMismoPadreException
    {
        ParamUtils.checkNotNull(persona);

        comprobarQueNoHayaNodosHijoConElMimoNombre(nodoMapa.getUpoMapa(), nodoMapa);

        nodoMapa.setNodoMapaDAO(nodoMapaDAO);
        nodoMapaDAO.insert(nodoMapa);
    }

    @Transactional
    public void actualizarUrlPathyUrlCompletaDeNodoyNodosHijo(NodoMapa nodoMapa, String nombreNodo, Persona persona) throws NodoNoSePuedeRenombrarException, ActualizadoContenidoNoAutorizadoException, NodoConNombreYaExistenteEnMismoPadreException
    {
        String urlBase = nodoMapa.getUrlCompleta();
        String urlNueva = nodoMapa.getNuevaUrlCompleta(urlBase, nombreNodo);

        if (nodoMapa.getId() <= 0)
        {
            throw new NodoNoSePuedeRenombrarException();
        }

        if (!personaAdminEnNodo(nodoMapa.getUpoMapa(), persona))
        {
            throw new ActualizadoContenidoNoAutorizadoException();
        }

        nodoMapa.setUrlCompleta(urlNueva);
        nodoMapa.setUrlPath(nombreNodo);

        updateForced(persona, nodoMapa);

        nodoMapaDAO.actualizarUrlCompletaNodosHijoPorRenombrado(urlBase, urlNueva);
    }

    public void actualizarNodoPadreyUrlCompletaDeNodoyNodosHijo(NodoMapa nodoMapa, NodoMapa nodoMapaDestino, Persona persona) throws NodoNoSePuedeArrastrarException, ActualizadoContenidoNoAutorizadoException, NodoConNombreYaExistenteEnMismoPadreException
    {
        String urlBase = nodoMapa.getUrlCompleta();
        String nuevaUrlBase = nodoMapaDestino.getUrlCompleta();
        String urlBaseSinNodo = nodoMapa.getUrlBaseSinNodo();

        if (nodoMapa.getId() <= 0 || nodoMapaDestino.getId() <= 0)
        {
            throw new NodoNoSePuedeArrastrarException();
        }

        if (nodoMapaDestino.getUrlCompleta().startsWith(nodoMapa.getUrlCompleta()))
        {
            throw new NodoNoSePuedeArrastrarException();
        }

        esAdministradorDeLosDosNodos(nodoMapaDestino, persona);

        nodoMapa.setUpoMapa(nodoMapaDestino);
        nodoMapa.setUrlCompleta(nuevaUrlBase + urlBase.substring(urlBaseSinNodo.length()));

        update(persona, nodoMapa);

        nodoMapaDAO.actualizarUrlCompletaNodosHijoPorDragAndDrop(urlBase, urlBaseSinNodo, nuevaUrlBase);
    }

    public void duplicarEstructura(NodoMapa nodoMapaOrigen, NodoMapa nodoMapaDestino, String nombreNodo, Boolean duplicarContenidos, Persona persona) throws NodoNoSePuedeDuplicarException, NodoConNombreYaExistenteEnMismoPadreException, ContenidoYaPropuestoException, NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoConMismoUrlPathException, TipoContenidoNoAdecuadoException, ActualizadoContenidoNoAutorizadoException, CrearNuevoContenidoException
    {
        if (nodoMapaOrigen.getId() <= 0 || !personaAdminEnNodo(nodoMapaDestino, persona))
        {
            throw new NodoNoSePuedeDuplicarException();
        }

        String urlBaseOrigen = nodoMapaOrigen.getUrlBaseSinNodo();

        List<NodoMapa> nodosMapaACrear = getDescendientes(nodoMapaOrigen.getUrlCompleta());
        nodosMapaACrear.get(0).setUrlPath(nombreNodo);

        int index = 0;
        for (NodoMapa nodoMapaACrear : nodosMapaACrear)
        {
            createNewNodoMapaAndNodoMapaContenido(nodoMapaDestino, nodoMapaACrear, urlBaseOrigen, false, duplicarContenidos, nombreNodo, persona, false, index);

            index++;
        }
    }

    public void crearNodoMapaHijoYEnlacesAContenidos(NodoMapa nodoMapaDestino, NodoMapa nodoMapaOrigen, Persona persona, Boolean incluirHijos, Boolean esPropuestaPorTag)
            throws NodoNoSePuedeProponerException, NodoConNombreYaExistenteEnMismoPadreException, NodoMapaContenidoDebeTenerUnContenidoNormalException,
                   ContenidoYaPropuestoException, ContenidoConMismoUrlPathException, TipoContenidoNoAdecuadoException, ActualizadoContenidoNoAutorizadoException, CrearNuevoContenidoException
    {
        if (nodoMapaDestino.getId() <= 0 || nodoMapaOrigen.getId() <= 0)
        {
            throw new NodoNoSePuedeProponerException();
        }

        if (nodoMapaDAO.isNodoMapaLocked(nodoMapaDestino.getUrlCompleta()))
        {
            throw new NodoNoSePuedeProponerException();
        }

        String urlBaseOrigen = nodoMapaOrigen.getUrlBaseSinNodo();
        List<NodoMapa> nodosMapaACrear = Arrays.asList(nodoMapaOrigen);

        if (incluirHijos)
        {
            nodosMapaACrear = getDescendientes(nodoMapaOrigen.getUrlCompleta());
        }

        String nombreNodo = nodosMapaACrear.get(0).getUrlPath();
        Boolean adminEnNodo = personaAdminEnNodo(nodoMapaDestino, persona);

        int index = 0;
        for (NodoMapa nodoMapaACrear : nodosMapaACrear)
        {
            if (!adminEnNodo)
            {
                nodoMapaACrear.setBloqueado(true);
            }

            nodoMapaACrear.setPersonaPropuesta(persona);

            createNewNodoMapaAndNodoMapaContenido(nodoMapaDestino, nodoMapaACrear, urlBaseOrigen, true, false, nombreNodo, persona, esPropuestaPorTag, index);

            index++;
        }

        if (!adminEnNodo)
        {
            notificacionService.enviarCorreoAAdministradores(nodoMapaDestino, persona.getId());
        }
    }

    private void createNewNodoMapaAndNodoMapaContenido(NodoMapa nodoMapa, NodoMapa nodoMapaOrigen, String urlBaseOrigen, Boolean createPropuestas, Boolean createObjetos, String nombreNodo, Persona persona, Boolean esPropuestaPorTag, int index)
            throws NodoConNombreYaExistenteEnMismoPadreException, NodoMapaContenidoDebeTenerUnContenidoNormalException,
            ContenidoYaPropuestoException, ContenidoConMismoUrlPathException, TipoContenidoNoAdecuadoException, ActualizadoContenidoNoAutorizadoException,
            CrearNuevoContenidoException
    {
        Long nodoMapaOrigenId = nodoMapaOrigen.getId();

        setValuesForNewNodoMapa(nodoMapa, nodoMapaOrigen, urlBaseOrigen, nombreNodo);

        if (index == 0)
        {
            nodoMapaOrigen = asignaUltimaPosicionANodo(nodoMapaOrigen, nodoMapa);
            nodoMapaOrigen.setPropuestaMapaId(esPropuestaPorTag ? nodoMapaOrigenId : null);
        }

        Long newId = updateForced(persona, nodoMapaOrigen);
        List<NodoMapaContenido> nodosMapaContenido = null;

        if (createObjetos || createPropuestas)
        {
            nodosMapaContenido = getNodosMapaContenidoByNodoMapaId(nodoMapaOrigenId);
        }

        if (createPropuestas)
        {
            createNewPropuestasForNewNodoMapa(persona, nodosMapaContenido, newId);
        }

        if (createObjetos)
        {
            createNewContenidosForNewNodoMapa(persona, nodosMapaContenido, newId);
        }
    }

    private void setValuesForNewNodoMapa(NodoMapa nodoMapa, NodoMapa nodoMapaOrigen, String urlBaseOrigen, String nombreNodo)
    {
        nodoMapaOrigen.setId(null);
        nodoMapaOrigen.setAutoguardado(null);
        nodoMapaOrigen.setUpoMapasObjetos(null);
        nodoMapaOrigen.setUpoModeracionLotes(null);
        nodoMapaOrigen.setUpoFranquicia(nodoMapa.getUpoFranquicia());
        nodoMapaOrigen.setUpoCriterio(null);
        nodoMapaOrigen.setMenu(nodoMapa.getMenu());
        nodoMapaOrigen.setMenuHeredado(true);

        String urlBase = nodoMapaOrigen.getUrlCompleta().substring(urlBaseOrigen.length());
        String urlAConservar = urlBase.substring(urlBase.indexOf("/"));

        Integer ultimaPosDeBarra = urlAConservar.lastIndexOf("/", urlAConservar.length() - 2);

        String urlAConservarBase = "";

        if (ultimaPosDeBarra != -1)
        {
            urlAConservarBase = urlAConservar.substring(0, ultimaPosDeBarra + 1);
        }

        String urlPadre = nodoMapa.getUrlCompleta();

        if (urlAConservar.length() > 1)
        {
            urlPadre = nodoMapa.getUrlCompleta() + nombreNodo + urlAConservarBase;
        }

        nodoMapaOrigen.setUpoMapa(getNodoMapaByUrlCompleta(urlPadre));

        nodoMapaOrigen.setUrlCompleta(nodoMapa.getUrlCompleta() + nombreNodo + urlAConservar);

        for (NodoMapaPlantilla nodoMapaPlantilla : nodoMapaOrigen.getUpoMapasPlantillas())
        {
            nodoMapaPlantilla.setId(null);
            nodoMapaPlantilla.setUpoMapa(nodoMapaOrigen);
        }

        for (NodoMapaExclusionIdioma nodoMapaExclusionIdioma : nodoMapaOrigen.getUpoMapasIdiomasExclusiones())
        {
            nodoMapaExclusionIdioma.setId(null);
            nodoMapaExclusionIdioma.setUpoMapa(nodoMapaOrigen);
        }
    }

    private List<NodoMapaContenido> getNodosMapaContenidoByNodoMapaId(Long nodoMapaId)
    {
        return nodoMapaContenidoService.getNodoMapasContenidoByMapaId(nodoMapaId);
    }

    private void compruebaQueNoHayaContenidosRepetidos(NodoMapa nodoMapa, String fileName) throws ContenidoConMismoUrlPathException
    {
        List<Contenido> contenidos = contenidoService.getContenidosByMapaId(nodoMapa.getId());

        for (Contenido contenido : contenidos)
        {
            if (contenido.getUrlPath().equals(fileName) && !contenido.getId().equals(nodoMapa.getId()))
            {
                throw new ContenidoConMismoUrlPathException();
            }
        }
    }

    public boolean hasNodosMapaHijo(NodoMapa nodoMapa)
    {
        return nodoMapaDAO.hasNodosMapaHijos(nodoMapa);
    }

    @Transactional
    public void reordenarNodos(NodoMapa nodoMapa, Long nodoMapaOrigenId, Long nodoMapaJuntoId, String point, Persona persona) throws ReordenadoDeNodosNoAutorizadoException
    {
        ParamUtils.checkNotNull(persona);

        if (!personaAdminEnNodo(nodoMapa, persona))
        {
            throw new ReordenadoDeNodosNoAutorizadoException();
        }

        List<NodoMapa> nodosMapaHijo = getHijos(nodoMapa.getId());
        NodoMapa nodoMapaOrigen = getNodoMapa(nodoMapaOrigenId, false);

        Long orden = getOrdenParaActualizar(nodosMapaHijo, nodoMapaJuntoId, point);
        List<NodoMapa> nodosMapaAActualizar = getNodosMapaParaActualizarOrden(nodosMapaHijo, nodoMapaJuntoId, point, nodoMapaOrigenId);

        if (!nodosMapaAActualizar.isEmpty())
        {
            nodoMapaDAO.actualizarOrdenNodos(nodoMapa.getId(), nodosMapaAActualizar, orden);
        }

        nodoMapaOrigen.setOrden(orden);
        nodoMapaDAO.update(nodoMapaOrigen);
    }

    private List<NodoMapa> getNodosMapaParaActualizarOrden(List<NodoMapa> nodosMapaHijo, Long nodoMapaJuntoId, String point, Long nodoMapaOrigenId)
    {
        List<NodoMapa> nodosMapaAActualizar = new ArrayList<NodoMapa>();
        Boolean startUpdate = false;

        for (NodoMapa nodoMapaHijo : nodosMapaHijo)
        {
            if (nodoMapaHijo.getId().equals(nodoMapaOrigenId))
            {
                continue;
            }

            if (startUpdate)
            {
                nodosMapaAActualizar.add(nodoMapaHijo);
            }

            if (nodoMapaHijo.getId().equals(nodoMapaJuntoId))
            {
                if (point.toUpperCase().equals(TipoPointsArrastrarNodoArbol.BEFORE.toString()))
                {
                    nodosMapaAActualizar.add(nodoMapaHijo);
                }

                startUpdate = true;
            }
        }

        return nodosMapaAActualizar;
    }

    private Long getOrdenParaActualizar(List<NodoMapa> nodosMapaHijo, Long nodoMapaJuntoId, String point)
    {
        NodoMapa nodoMapaAnterior = null;

        for (NodoMapa nodoMapaHijo : nodosMapaHijo)
        {
            if (nodoMapaHijo.getId().equals(nodoMapaJuntoId))
            {
                if (point.toUpperCase().equals(TipoPointsArrastrarNodoArbol.BEFORE.toString()))
                {
                    if (nodoMapaAnterior == null)
                    {
                        return 1L;
                    }

                    return nodoMapaAnterior.getOrden() + 1;
                }

                return nodoMapaHijo.getOrden() + 1;
            }

            nodoMapaAnterior = nodoMapaHijo;
        }

        return 1L;
    }

    public NodoMapa asignaUltimaPosicionANodo(NodoMapa nodoMapa, NodoMapa nodoMapaDestino)
    {
        List<NodoMapa> hijos = getHijos(nodoMapaDestino.getId());

        Long ordenMaximo = hijos.stream().mapToLong(n -> n.getOrden()).max().orElse(0L);

        nodoMapa.setOrden(ordenMaximo + 1L);

        return nodoMapa;
    }

    public NodoMapa getNodoMapaOfNodoMapaContenidoNormalByContenidoId(Long contenidoId)
    {
        return nodoMapaDAO.getNodoMapaOfNodoMapaContenidoNormalByContenidoId(contenidoId);
    }

    public void deleteNodoMapa(NodoMapa nodoMapa, Persona persona) throws UsuarioNoAutenticadoException, BorradoContenidoNoAutorizadoException, NodoConNombreYaExistenteEnMismoPadreException
    {
        ParamUtils.checkNotNull(persona);

        if (!tienePermisosDeBorrado(nodoMapa, persona))
        {
            throw new BorradoContenidoNoAutorizadoException();
        }

        if (nodoMapa.getUrlCompleta().startsWith(URL_COMPLETA_PAPELERA))
        {
            deleteContenidosYNodosMapa(nodoMapa, persona);
            return;
        }

        arrastrarNodoAPapelera(nodoMapa, persona);
    }

    private boolean tienePermisosDeBorrado(NodoMapa nodoMapa, Persona persona)
    {
        return personaAdminEnNodo(nodoMapa.getUpoMapa(), persona) || (nodoMapa.getPersonaPropuesta() != null && persona.getId()
                .equals(nodoMapa.getPersonaPropuesta().getId()));
    }

    public void arrastrarNodoAPapelera(NodoMapa nodoMapa, Persona persona) throws NodoConNombreYaExistenteEnMismoPadreException, UsuarioNoAutenticadoException
    {
        if (nodoMapa.getUrlCompleta().startsWith(URL_COMPLETA_PAPELERA)) return;

        NodoMapa nodoPapelera = getNodoMapaByUrlCompleta(URL_COMPLETA_PAPELERA);
        NodoMapa nodoMapaBase = new NodoMapa();

        String nombre = nodoMapa.getUrlPath() + nodoMapa.normalizeUrlPath(DateUtils.getTimeStamp());
        String urlCompleta = URL_COMPLETA_PAPELERA + nombre + '/';
        String urlBase = nodoMapa.getUrlCompleta();
        String urlBaseSinNodo = nodoMapa.getUrlBaseSinNodo();

        nodoMapaBase.setUrlPath(nombre);
        nodoMapaBase.setUrlCompleta(urlCompleta);
        nodoMapaBase.setUpoMapa(nodoPapelera);
        nodoMapaBase.setUpoFranquicia(nodoMapa.getUpoFranquicia());
        nodoMapaBase.setMenu(nodoMapa.getMenu());

        insertForced(persona, nodoMapaBase);

        nodoMapa.setUpoMapa(nodoMapaBase);
        nodoMapa.setUrlCompleta(urlCompleta + urlBase.substring(urlBaseSinNodo.length()));

        updateForced(persona, nodoMapa);

        nodoMapaDAO.actualizarUrlCompletaNodosHijoPorDragAndDrop(urlBase, urlBaseSinNodo, urlCompleta);
    }

    private void deleteContenidosYNodosMapa(NodoMapa nodoMapa, Persona persona) throws BorradoContenidoNoAutorizadoException
    {
        List<Contenido> contenidos = contenidoDAO.getContenidosByUrlCompleta(nodoMapa.getUrlCompleta());

        for (Contenido contenido : contenidos)
        {
            contenidoService.deleteByPersonaAndNodoMapa(contenido, persona.getId(), contenido.getNodoMapa().getId());
        }

        nodoMapaDAO.deleteByNodoMapaId(nodoMapa.getId());
    }

    public void setVisibilidad(String visible, String texto, String extenderAHijos, NodoMapa nodoMapa, Persona persona) throws ActualizadoContenidoNoAutorizadoException
    {
        ParamUtils.checkNotNull(persona);

        if (!personaAdminEnNodo(nodoMapa, persona))
        {
            throw new ActualizadoContenidoNoAutorizadoException();
        }

        if (extenderAHijos.equals("S"))
        {
            contenidoDAO.setVisibilidadByUrlCompleta(nodoMapa.getUrlCompleta(), visible, texto);
        } else
        {
            contenidoDAO.setVisibilidadByNodoMapa(nodoMapa.getId(), visible, texto);
        }
    }

    public void updateMenuAHeredado(NodoMapa nodoMapa, Persona persona) throws ActualizadoContenidoNoAutorizadoException, NodoConNombreYaExistenteEnMismoPadreException
    {
        if (!haCambiadoAlgoDelMenu(nodoMapa, true, null)) return;

        Menu menuPadre = nodoMapa.getUpoMapa().getMenu();

        updateMenu(nodoMapa, persona, menuPadre, true);
    }

    public void updateMenuANoHeredado(NodoMapa nodoMapa, Persona persona, Long menuId) throws ActualizadoContenidoNoAutorizadoException, NodoConNombreYaExistenteEnMismoPadreException
    {
        if (!haCambiadoAlgoDelMenu(nodoMapa, false, menuId)) return;

        Menu menu = menuService.getMenu(menuId);

        updateMenu(nodoMapa, persona, menu, false);
    }

    private boolean haCambiadoAlgoDelMenu(NodoMapa nodoMapa, boolean menuHeredado, Long menuId)
    {
        if (!nodoMapa.isMenuHeredado().equals(menuHeredado)) return true;

        if (nodoMapa.getMenu() != null && !nodoMapa.getMenu().getId().equals(menuId)) return true;

        return false;
    }

    public void updateMenu(NodoMapa nodoMapa, Persona persona, Menu menu, boolean menuHeredado) throws ActualizadoContenidoNoAutorizadoException, NodoConNombreYaExistenteEnMismoPadreException
    {
        ParamUtils.checkNotNull(persona);

        if (!personaAdminEnNodo(nodoMapa, persona))
        {
            throw new ActualizadoContenidoNoAutorizadoException();
        }

        nodoMapa.setMenu(menu);
        nodoMapa.setMenuHeredado(menuHeredado);
        update(persona, nodoMapa);

        nodoMapaDAO.updateMenuEnCascada(nodoMapa, menu);
    }

    public void crearContenidoDesdeRecurso(NodoMapa nodoMapa, Persona persona, String url, Long contenidoId, String idiomaCodigoISO, TipoAccionCrearContenidoDesdeRecurso tipo) throws InsertadoContenidoNoAutorizadoException, IOException, ImportarDocumentoException, ContenidoConMismoUrlPathException, CrearNuevoContenidoException
    {
        ParamUtils.checkNotNull(persona);

        if (!personaAdminEnNodo(nodoMapa, persona))
        {
            throw new InsertadoContenidoNoAutorizadoException();
        }

        Contenido contenido = contenidoService.getContenido(contenidoId, true);
        Contenido contenidoNuevo = contenidoService.crearNuevoContenido(contenido);

        Set<ContenidoIdioma> contenidosIdiomas = contenidoNuevo.getContenidoIdiomas();

        String fileName = contenidoIdiomaService.preparaContenidosIdioma(idiomaCodigoISO, contenidosIdiomas, url, tipo);

        contenidoNuevo.setUrlPath(fileName);

        compruebaQueNoHayaContenidosRepetidos(nodoMapa, fileName);

        NodoMapaContenido nodoMapaContenido = new NodoMapaContenido();
        nodoMapaContenido.setEstadoModeracion(EstadoModeracion.ACEPTADO);
        nodoMapaContenido.setUpoMapa(nodoMapa);
        nodoMapaContenido.setUpoObjeto(contenidoNuevo);

        contenidoNuevo.setUpoMapasObjetos(Collections.singleton(nodoMapaContenido));

        nodoMapa.addNodoMapaContenidoRelacionado(nodoMapaContenido);

        setDefaultValuesForContenido(nodoMapa, persona, contenidoNuevo);

        contenidoDAO.insert(contenidoNuevo, persona.getId());
    }

    public void setDefaultValuesForContenido(NodoMapa nodoMapa, Persona persona, Contenido contenidoNuevo)
    {
        if (nodoMapa.getUpoFranquicia() != null)
        {
            contenidoNuevo.setOtrosAutores(nodoMapa.getUpoFranquicia().getOtrosAutores());
            contenidoNuevo.setOrigenInformacion(nodoMapa.getUpoFranquicia().getOrigenInformacion());
        }

        contenidoNuevo.setUpoExtPersona1(personaService.getPersona(persona.getId()));
    }

    public void createNewPropuestasForNewNodoMapa(Persona persona, List<NodoMapaContenido> nodosMapaContenido, Long newId) throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoYaPropuestoException, ContenidoConMismoUrlPathException
    {
        for (NodoMapaContenido nodoMapaContenido : nodosMapaContenido)
        {
            if (!EstadoModeracion.RECHAZADO.equals(nodoMapaContenido.getEstadoModeracion()))
            {
                NodoMapaContenido newNodoMapaContenido = new NodoMapaContenido();

                newNodoMapaContenido.setPersonaPropuesta(persona);
                newNodoMapaContenido.setTipo(TipoReferenciaContenido.LINK);

                nodoMapaContenidoService.insertContenidoAModerar(newNodoMapaContenido, getNodoMapa(newId, false), nodoMapaContenido.getUpoObjeto()
                        .getId(), nodoMapaContenido.getId(), false);
            }
        }
    }

    public void createNewContenidosForNewNodoMapa(Persona persona, List<NodoMapaContenido> nodosMapaContenido, Long newId) throws NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoYaPropuestoException, ContenidoConMismoUrlPathException, ActualizadoContenidoNoAutorizadoException, TipoContenidoNoAdecuadoException, CrearNuevoContenidoException
    {
        for (NodoMapaContenido nodoMapaContenido : nodosMapaContenido)
        {
            if (EstadoModeracion.RECHAZADO.equals(nodoMapaContenido.getEstadoModeracion())) continue;

            if (TipoReferenciaContenido.LINK.equals(nodoMapaContenido.getTipo()))
            {
                NodoMapaContenido newNodoMapaContenido = new NodoMapaContenido();
                newNodoMapaContenido.setPersonaPropuesta(persona);
                newNodoMapaContenido.setTipo(TipoReferenciaContenido.LINK);

                nodoMapaContenidoService.insertContenidoAModerar(newNodoMapaContenido, getNodoMapa(newId, false), nodoMapaContenido.getUpoObjeto()
                        .getId(), nodoMapaContenido.getId(), false);
            }

            if (TipoReferenciaContenido.NORMAL.equals(nodoMapaContenido.getTipo()))
            {
                nodoMapaContenido.setPersonaPropuesta(persona);
                nodoMapaContenidoService.createNewContenido(nodoMapaContenido, getNodoMapa(newId, true), persona);
            }
        }
    }

    public void alternaBloqueoNodo(NodoMapa nodoMapa, Persona persona) throws NodoConNombreYaExistenteEnMismoPadreException, ActualizadoContenidoNoAutorizadoException
    {
        if (!personaService.isNodoMapaAdmin(nodoMapa.getUrlCompleta(), persona.getId()))
        {
            throw new ActualizadoContenidoNoAutorizadoException();
        }

        nodoMapa.setBloqueado(!nodoMapa.isBloqueado());

        updateForced(persona, nodoMapa);
    }

    public void desbloquearNodoMapaYDescendientes(String urlCompleta)
    {
        nodoMapaDAO.desbloquearNodoMapaYDescendientes(urlCompleta);
    }

    public List<String> getUrlsPrivadas()
    {
        return nodoMapaDAO.getUrlsPrivadas();
    }

    public PropuestaANodosPorTagsRespuesta proponerANodosConEtiquetas(NodoMapa nodoMapaAProponer, List<String> listaTags, Boolean incluirHijos, Persona persona)
    {
        List<NodoMapa> nodosMapaConEtiquetas = nodoMapaDAO.getNodosMapaConEtiquetas(listaTags);
        List<NodoMapa> nodosMapaYaCreadosComoPropuestas = getNodosMapaYaPropuestos(nodoMapaAProponer.getId());
        PropuestaANodosPorTagsRespuesta respuesta = new PropuestaANodosPorTagsRespuesta();

        for (NodoMapa nodoMapaCreadoComoPropuesta : nodosMapaYaCreadosComoPropuestas)
        {
            try
            {
                NodoMapa nodoMapaPadre = nodoMapaCreadoComoPropuesta.getUpoMapa();

                deleteNodoMapa(nodoMapaCreadoComoPropuesta, persona);

                respuesta.addUrlCompletaConExito(nodoMapaPadre.getUrlCompleta());
            } catch (Exception e)
            {
                respuesta.addUrlCompletaConError("BORRADO", nodoMapaCreadoComoPropuesta.getUrlCompleta());
            }
        }

        for (NodoMapa nodoMapaDestino : nodosMapaConEtiquetas)
        {
            try
            {
                NodoMapa nodoMapaCopia = getNodoMapa(nodoMapaAProponer.getId(), true);

                crearNodoMapaHijoYEnlacesAContenidos(nodoMapaDestino, nodoMapaCopia, persona, incluirHijos, true);

                respuesta.addUrlCompletaConExito(nodoMapaDestino.getUrlCompleta());
            } catch (Exception e)
            {
                respuesta.addUrlCompletaConError("PROPUESTA", nodoMapaDestino.getUrlCompleta());
            }
        }

        return respuesta;
    }

    public List<NodoMapa> getNodosMapaYaPropuestos(Long mapaId)
    {
        return nodoMapaDAO.getNodosMapaYaPropuestos(mapaId);
    }

    public List<NodoMapaTag> getListaTagsByNodosMapa(List<NodoMapa> nodosMapa)
    {
        List<Long> nodosMapaId = nodosMapa.stream().map(NodoMapa::getId).collect(Collectors.toList());

        return nodoMapaDAO.getListaTagsByNodosMapaId(nodosMapaId);
    }
}
