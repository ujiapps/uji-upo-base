package es.uji.apps.upo.services;

import es.uji.apps.upo.dao.*;
import es.uji.apps.upo.exceptions.*;
import es.uji.apps.upo.migracion.UrlExtractor;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.*;
import es.uji.apps.upo.model.metadatos.ContenidoMetadato;
import es.uji.apps.upo.utils.YoutubeUrlFormatter;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StreamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.ServletContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ContenidoService extends AuthenticatedService {
    private ContenidoDAO contenidoDAO;
    private ContenidoIdiomaDAO contenidoIdiomaDAO;
    private ContenidoVigenciaDAO contenidoVigenciaDAO;
    private PrioridadContenidoDAO prioridadContenidoDAO;
    private NodoMapaContenidoDAO nodoMapaContenidoDAO;
    private TagDAO contenidoTagDAO;
    private AutoguardadoDAO autoguardadoDAO;
    private PersonaService personaService;
    private AutoguardadoService autoguardadoService;
    private NodoMapaContenidoService nodoMapaContenidoService;
    private IdiomaService idiomaService;
    private ContenidoIdiomaAtributoService contenidoIdiomaAtributoService;
    private ContenidoIdiomaRecursoService contenidoIdiomaRecursoService;
    private AtributoMetadatoService atributoMetadatoService;
    private String documentsServer;
    private YoutubeUrlFormatter youtubeUrlFormatter;
    private NodoMapaDAO nodoMapaDAO;

    @Context
    private ServletContext servletContext;

    @Autowired
    public ContenidoService(ContenidoDAO contenidoDAO, ContenidoIdiomaDAO contenidoIdiomaDAO,
                            ContenidoVigenciaDAO contenidoVigenciaDAO, NodoMapaContenidoDAO nodoMapaContenidoDAO,
                            TagDAO contenidoTagDAO, AutoguardadoDAO autoguardadoDAO, PersonaService personaService,
                            AutoguardadoService autoguardadoService, IdiomaService idiomaService,
                            ContenidoIdiomaAtributoService contenidoIdiomaAtributoService,
                            ContenidoIdiomaRecursoService contenidoIdiomaRecursoService, PrioridadContenidoDAO prioridadContenidoDAO,
                            AtributoMetadatoService atributoMetadatoService) {
        super(personaService);

        this.contenidoDAO = contenidoDAO;
        this.contenidoIdiomaDAO = contenidoIdiomaDAO;
        this.contenidoVigenciaDAO = contenidoVigenciaDAO;
        this.nodoMapaContenidoDAO = nodoMapaContenidoDAO;
        this.contenidoTagDAO = contenidoTagDAO;
        this.autoguardadoDAO = autoguardadoDAO;
        this.personaService = personaService;
        this.autoguardadoService = autoguardadoService;
        this.idiomaService = idiomaService;
        this.contenidoIdiomaAtributoService = contenidoIdiomaAtributoService;
        this.contenidoIdiomaRecursoService = contenidoIdiomaRecursoService;
        this.prioridadContenidoDAO = prioridadContenidoDAO;
        this.atributoMetadatoService = atributoMetadatoService;
    }

    @Autowired
    public void setNodoMapaContenidoService(NodoMapaContenidoService nodoMapaContenidoService) {
        this.nodoMapaContenidoService = nodoMapaContenidoService;
    }

    @Autowired
    public void setNodoMapaDAO(NodoMapaDAO nodoMapaDAO) {
        this.nodoMapaDAO = nodoMapaDAO;
    }

    @Autowired
    public void setServer(@Value("${uji.documentos.server}") String server) {
        this.documentsServer = server;
    }

    @Autowired
    void setYoutubeUrlFormatter(YoutubeUrlFormatter youtubeUrlFormatter) {
        this.youtubeUrlFormatter = youtubeUrlFormatter;
    }

    public String getDocumentsServer() {
        return documentsServer;
    }

    public NodoMapa getNodoMapa(Long contenidoId) {
        return nodoMapaContenidoDAO.getNodoMapaTipoNormalByContenidoId(contenidoId);
    }

    @Transactional(rollbackFor = Exception.class)
    public void update(Contenido contenido, Persona persona, NodoMapa nodoMapa)
            throws ActualizadoContenidoNoAutorizadoException, ContenidoConMismoUrlPathException {
        ParamUtils.checkNotNull(persona);

        if (!personaAdminEnNodo(nodoMapa, persona)) {
            throw new ActualizadoContenidoNoAutorizadoException();
        }

        actualizaEstadoPublicable(contenido);

        verificarUrlPathNoRepetidoEnOtrosContenidos(contenido, nodoMapa);

        contenidoDAO.update(contenido, persona.getId());
    }

    @Transactional(rollbackFor = Exception.class)
    public void insert(Contenido contenido, Persona persona, NodoMapa nodoMapa)
            throws InsertadoContenidoNoAutorizadoException, ContenidoConMismoUrlPathException {
        ParamUtils.checkNotNull(persona);

        if (!personaAdminEnNodo(nodoMapa, persona)) {
            throw new InsertadoContenidoNoAutorizadoException();
        }

        contenido.setEstadoModeracionOfNodoMapaContenidoToAceptado();
        actualizaEstadoPublicable(contenido);

        verificarUrlPathNoRepetidoEnOtrosContenidos(contenido, nodoMapa);

        contenidoDAO.insert(contenido, persona.getId());
    }

    public Contenido getContenido(Long objetoId) {
        return contenidoDAO.getContenidoById(objetoId);
    }

    public Contenido getContenido(Long contenidoId, boolean fetchAll) {
        return contenidoDAO.getContenido(contenidoId, fetchAll);
    }

    public List<Contenido> getContenidosByMapaId(Long mapaId) {
        return contenidoDAO.getContenidosByMapaId(mapaId);
    }

    @Transactional
    public void deleteVigencias(Long contenidoId) {
        contenidoVigenciaDAO.deleteVigenciasByContenido(contenidoId);
    }

    @Transactional
    public void deleteFechasEvento(Long nodoMapaContenidoId) {
        nodoMapaContenidoDAO.deleteFechasEvento(nodoMapaContenidoId);
    }

    @Transactional
    public void deletePrioridades(Long contenidoId) {
        prioridadContenidoDAO.deletePrioridadesByContenidoId(contenidoId);
    }

    @Transactional
    public void deleteTags(Long contenidoId) {
        contenidoTagDAO.deleteTagsByContenido(contenidoId);
    }

    @Transactional
    public void deleteByPersonaAndNodoMapa(Contenido contenido, Long personaId, Long mapaId)
            throws BorradoContenidoNoAutorizadoException {
        ParamUtils.checkNotNull(personaId, mapaId);

        Persona persona = personaService.getPersona(personaId);
        NodoMapa nodoMapa = getNodoMapa(mapaId, false);

        NodoMapaContenido nodoMapaContenido =
                nodoMapaContenidoService.getNodoMapaContenidoByContenidoIdAndMapaId(contenido.getId(),
                        nodoMapa.getId());

        if (!personaAdminEnNodo(nodoMapa, persona) && !esQuienPropone(nodoMapaContenido, personaId)) {
            throw new BorradoContenidoNoAutorizadoException();
        }

        if (nodoMapaContenido.isNormal()) {
            for (NodoMapaContenido nodoMapaContenidoALimpiar : nodoMapaContenidoDAO.getNodosMapaContenidoByContenidoId(contenido.getId())) {
                limpiarEnlaces(nodoMapaContenidoALimpiar);
            }

            contenidoDAO.delete(Contenido.class, contenido.getId(), personaId);
        } else {
            limpiarEnlaces(nodoMapaContenido);

            nodoMapaContenidoDAO.delete(NodoMapaContenido.class, nodoMapaContenido.getId());
        }
    }

    private boolean esQuienPropone(NodoMapaContenido nodoMapaContenido, Long personaId) {
        return nodoMapaContenido.getPersonaPropuesta() != null && nodoMapaContenido.getPersonaPropuesta()
                .getId()
                .equals(personaId);
    }

    public NodoMapa getNodoMapa(Long nodoMapaId, boolean fetchAll) {
        List<NodoMapa> listaMapas;

        if (fetchAll) {
            listaMapas = nodoMapaDAO.getNodoMapaAndRelations(nodoMapaId);
        } else {
            listaMapas = nodoMapaDAO.get(NodoMapa.class, nodoMapaId);
        }

        if (listaMapas != null && !listaMapas.isEmpty()) {
            return listaMapas.get(0);
        }
        return null;
    }

    public String getAutoguardado(Contenido contenido, Long personaId, String idiomaCodigoISO) {
        Long mapaId = contenido.getNodoMapa().getId();

        Autoguardado autoguardado =
                autoguardadoService.getAutoguardadoMasActualByParameters(mapaId, contenido.getId(), personaId,
                        idiomaCodigoISO);

        if (autoguardado != null) {
            return autoguardado.getContenidoEditora();
        }

        return null;
    }

    public void updateVisibility(Persona persona, List<String> contenidoIdList, String visible, String textoNoVisible,
                                 String mapaId)
            throws ActualizadoContenidoNoAutorizadoException, ContenidoConMismoUrlPathException {
        for (String contenidoId : contenidoIdList) {
            Contenido contenido = getContenido(Long.parseLong(contenidoId));
            NodoMapaContenido nodoMapaContenido =
                    nodoMapaContenidoService.getNodoMapaContenidoByContenidoIdAndMapaId(Long.parseLong(contenidoId),
                            Long.parseLong(mapaId));

            if (nodoMapaContenido.getTipo().equals(TipoReferenciaContenido.NORMAL)) {
                contenido.setVisible(visible);
                contenido.setTextoNoVisible(textoNoVisible);

                update(contenido, persona, nodoMapaContenido.getUpoMapa());
            }
        }
    }

    public List<String> getOtrasPersonasEditandoContenido(Long contenidoId, Long personaId) {
        return autoguardadoDAO.getOtrasPersonasEditandoContenido(contenidoId, personaId);
    }

    public void verificarUrlPathNoRepetidoEnOtrosContenidos(Contenido contenidoOriginal, NodoMapa nodoMapa)
            throws ContenidoConMismoUrlPathException {
        if (nodoMapa != null) {
            List<Contenido> contenidos = getContenidosByMapaId(nodoMapa.getId());

            for (Contenido contenido : contenidos) {
                if (contenido.getUrlPath().equals(contenidoOriginal.getUrlPath()) && !contenido.getId()
                        .equals(contenidoOriginal.getId())) {
                    throw new ContenidoConMismoUrlPathException();
                }
            }
        }
    }

    public void verificarUrlPathNoRepetidoEnOtrosContenidos(NodoMapa nodoMapa, String urlPath)
            throws ContenidoConMismoUrlPathException {
        if (nodoMapa != null) {
            List<Contenido> contenidos = getContenidosByMapaId(nodoMapa.getId());

            for (Contenido contenido : contenidos) {
                if (contenido.getUrlPath().equals(urlPath)) {
                    throw new ContenidoConMismoUrlPathException();
                }
            }
        }
    }

    public List<Idioma> getIdiomasObligatorios(NodoMapa nodoMapa) {
        if (nodoMapa != null) {
            return nodoMapaDAO.getIdiomasObligatorios(nodoMapa.getId());
        } else {
            return new ArrayList<Idioma>();
        }
    }

    public boolean validarIdiomas(Contenido contenido, NodoMapa nodoMapa) {
        List<Idioma> idiomasObligatorios = getIdiomasObligatorios(nodoMapa);
        List<Idioma> idiomasAInsertar = new ArrayList<Idioma>();

        for (ContenidoIdioma contenidoIdioma : contenido.getContenidoIdiomas()) {
            if (contenidoIdioma.getTipoFormato()
                    .equals(TipoFormatoContenido.PAGINA.toString()) && contenidoIdioma.getContenido() != null && contenidoIdioma
                    .getContenido().length > 0) {
                idiomasAInsertar.add(contenidoIdioma.getUpoIdioma());
            }

            if (contenidoIdioma.getTipoFormato()
                    .equals(TipoFormatoContenido.BINARIO.toString()) && ((contenidoIdioma.getContenido() != null && contenidoIdioma
                    .getContenido().length > 0) || contenidoIdioma.getIdDescarga() != null)) {
                idiomasAInsertar.add(contenidoIdioma.getUpoIdioma());
            }
        }

        if (idiomasAInsertar.isEmpty()) return true;

        for (Idioma idioma : idiomasObligatorios) {
            if (!idiomasAInsertar.contains(idioma)) {
                return false;
            }
        }

        return true;
    }

    private boolean validarIdiomas(Contenido contenido) {
        return validarIdiomas(contenido, contenido.getUpoMapasObjetos().iterator().next().getUpoMapa());
    }

    public boolean isPublicable(Contenido contenido) {
        Boolean idiomasValidos;

        if (contenido.getNodoMapa() == null) {
            idiomasValidos = validarIdiomas(contenido);
        } else {
            idiomasValidos = validarIdiomas(contenido, contenido.getNodoMapa());
        }

        return idiomasValidos && contenido.isAccesible();
    }

    public void actualizaEstadoPublicable(Contenido contenido) {
        contenido.setPublicable(isPublicable(contenido) ? "S" : "N");
    }

    public void limpiarEnlaces(NodoMapaContenido nmc) {
        NodoMapa nm = nmc.getUpoMapa();

        Set<NodoMapaContenido> nuevaLista = new HashSet<NodoMapaContenido>();

        for (NodoMapaContenido nodoMapaContenido : nodoMapaContenidoService.getNodoMapasContenidoByMapaId(nmc.getId())) {
            if (nodoMapaContenido != null && !nodoMapaContenido.getId().equals(nmc.getId())) {
                nuevaLista.add(nodoMapaContenido);
            } else {
                nuevaLista.add(null);
            }
        }

        nm.setUpoMapasObjetos(nuevaLista);
    }

    public void deleteMetadatosByContenidoId(Long contenidoId) {
        contenidoDAO.deleteMetadatosByContenidoId(contenidoId);
    }

    public Contenido insertContenido(String nodoMapaId, String origenInformacion, String urlPath, String urlOriginal,
                                     Long personaResponsable, String orden, String latitud, String longitud, String lugar, String otrosAutores,
                                     String tituloCA, String subtituloCA, String tituloLargoCA, String resumenCA, String htmlCA,
                                     String contenidoCA, byte[] dataBinaryCA, String enlaceDestinoCA, String tituloES, String subtituloES,
                                     String tituloLargoES, String resumenES, String htmlES, String contenidoES, byte[] dataBinaryES,
                                     String enlaceDestinoES, String tituloEN, String subtituloEN, String tituloLargoEN, String resumenEN,
                                     String htmlEN, String contenidoEN, byte[] dataBinaryEN, String enlaceDestinoEN, String vigencia,
                                     Boolean ajustarAFechasVigencia, String fechasEvento, String prioridad, String tags, String nombreFicheroCA,
                                     String nombreFicheroES, String nombreFicheroEN, String tipoMimeFicheroCA, String tipoMimeFicheroES,
                                     String tipoMimeFicheroEN, Boolean repetirFicheroCA, Boolean repetirFicheroES, Boolean repetirFicheroEN,
                                     String fechaCreacion, String tipoFormatoCA, String tipoFormatoES, String tipoFormatoEN,
                                     Map<String, String> atributosCA, Map<String, String> atributosES, Map<String, String> atributosEN,
                                     String visible, String textoNoVisible, Set<ContenidoMetadato> metadatos)
            throws InsertadoContenidoException {
        Contenido contenido = new Contenido();
        contenido.setUrlPath(urlPath);
        contenido.setUrlOriginal(urlOriginal);
        contenido.setLatitud(latitud);
        contenido.setLongitud(longitud);
        contenido.setLugar(lugar);
        contenido.setOtrosAutores(otrosAutores);
        contenido.setFechaCreacion(getDate(fechaCreacion + " 00:00:00"));
        contenido.setAjustarAFechasVigencia(ajustarAFechasVigencia);

        actualizaResponsable(personaResponsable, contenido);

        actualizaOrigenInformacion(origenInformacion, contenido);

        NodoMapa nodoMapa = getNodoMapa(Long.parseLong(nodoMapaId), false);

        NodoMapaContenido nodoMapaContenido = new NodoMapaContenido();
        nodoMapaContenido.setUpoExtPersona(contenido.getUpoExtPersona1());
        nodoMapaContenido.setUpoMapa(nodoMapa);
        nodoMapaContenido.setUpoObjeto(contenido);
        nodoMapaContenido.setOrden(orden);
        contenido.setVisible(visible);
        contenido.setTextoNoVisible(textoNoVisible);

        contenido.getUpoMapasObjetos().add(nodoMapaContenido);

        actualizarIdiomasContenido(tituloCA, subtituloCA, tituloLargoCA, resumenCA, htmlCA, contenidoCA, dataBinaryCA,
                enlaceDestinoCA, tituloES, subtituloES, tituloLargoES, resumenES, htmlES, contenidoES, dataBinaryES,
                enlaceDestinoES, tituloEN, subtituloEN, tituloLargoEN, resumenEN, htmlEN, contenidoEN, dataBinaryEN,
                enlaceDestinoEN, nombreFicheroCA, nombreFicheroES, nombreFicheroEN, tipoMimeFicheroCA,
                tipoMimeFicheroES, tipoMimeFicheroEN, contenido, repetirFicheroCA, repetirFicheroEN, repetirFicheroES,
                tipoFormatoCA, tipoFormatoES, tipoFormatoEN, atributosCA, atributosES, atributosEN);

        Persona persona = new Persona();
        persona.setId(personaResponsable);

        try {
            actualizarVigenciasContenido(vigencia, contenido);
            actualizarFechasEvento(fechasEvento, nodoMapaContenido);
            actualizarTagsContenido(tags, contenido);
            actualizarPrioridadContenido(prioridad, contenido);
            actualizarMetadatos(metadatos, contenido);

            insert(contenido, persona, nodoMapa);
        } catch (Exception e) {
            throw new InsertadoContenidoException(e);
        }

        return contenido;
    }

    public Contenido updateContenido(String id, String nodoMapaId, String origenInformacion, String urlPath,
                                     Long personaId, String orden, String latitud, String longitud, String lugar, String otrosAutores,
                                     String tituloCA, String subtituloCA, String tituloLargoCA, String resumenCA, String htmlCA,
                                     String contenidoCA, byte[] dataBinaryCA, String enlaceDestinoCA, String tituloES, String subtituloES,
                                     String tituloLargoES, String resumenES, String htmlES, String contenidoES, byte[] dataBinaryES,
                                     String enlaceDestinoES, String tituloEN, String subtituloEN, String tituloLargoEN, String resumenEN,
                                     String htmlEN, String contenidoEN, byte[] dataBinaryEN, String enlaceDestinoEN, String vigencia,
                                     Boolean ajustarAFechasVigencia, String fechasEvento, String prioridad, String tags, String nombreFicheroCA,
                                     String nombreFicheroES, String nombreFicheroEN, String tipoMimeFicheroCA, String tipoMimeFicheroES,
                                     String tipoMimeFicheroEN, Boolean repetirFicheroCA, Boolean repetirFicheroES, Boolean repetirFicheroEN,
                                     String fechaCreacion, String tipoFormatoCA, String tipoFormatoES, String tipoFormatoEN,
                                     Map<String, String> atributosCA, Map<String, String> atributosES, Map<String, String> atributosEN,
                                     String visible, String textoNoVisible, Set<ContenidoMetadato> metadatos)
            throws ActualizadoContenidoException {
        NodoMapaContenido nodoMapaContenido =
                nodoMapaContenidoService.getNodoMapaContenidoByContenidoIdAndMapaId(Long.parseLong(id),
                        Long.parseLong(nodoMapaId));

        nodoMapaContenido.setOrden(orden);

        deleteFechasEvento(nodoMapaContenido.getId());
        actualizarFechasEvento(fechasEvento, nodoMapaContenido);

        Persona persona = new Persona();
        persona.setId(personaId);

        try {
            nodoMapaContenidoService.update(nodoMapaContenido, persona, nodoMapaContenido.getUpoMapa());
        } catch (Exception e) {
            throw new ActualizadoContenidoException(e);
        }

        Contenido contenido = getContenido(Long.parseLong(id));

        if (nodoMapaContenido.isNormal()) {
            eliminarRegistrosVinculados(contenido);

            contenido.setUrlPath(urlPath);
            contenido.setLatitud(latitud);
            contenido.setLongitud(longitud);
            contenido.setLugar(lugar);
            contenido.setOtrosAutores(otrosAutores);
            contenido.setFechaCreacion(getDate(fechaCreacion + " 00:00:00"));
            contenido.setVisible(visible);
            contenido.setTextoNoVisible(textoNoVisible);
            contenido.setAjustarAFechasVigencia(ajustarAFechasVigencia);

            actualizaOrigenInformacion(origenInformacion, contenido);

            actualizarIdiomasContenido(tituloCA, subtituloCA, tituloLargoCA, resumenCA, htmlCA, contenidoCA,
                    dataBinaryCA, enlaceDestinoCA, tituloES, subtituloES, tituloLargoES, resumenES, htmlES, contenidoES,
                    dataBinaryES, enlaceDestinoES, tituloEN, subtituloEN, tituloLargoEN, resumenEN, htmlEN, contenidoEN,
                    dataBinaryEN, enlaceDestinoEN, nombreFicheroCA, nombreFicheroES, nombreFicheroEN, tipoMimeFicheroCA,
                    tipoMimeFicheroES, tipoMimeFicheroEN, contenido, repetirFicheroCA, repetirFicheroEN,
                    repetirFicheroES, tipoFormatoCA, tipoFormatoES, tipoFormatoEN, atributosCA, atributosES,
                    atributosEN);

            try {
                actualizarVigenciasContenido(vigencia, contenido);
                actualizarPrioridadContenido(prioridad, contenido);
                actualizarTagsContenido(tags, contenido);
                actualizarMetadatos(metadatos, contenido);

                update(contenido, persona, nodoMapaContenido.getUpoMapa());
            } catch (Exception e) {
                throw new ActualizadoContenidoException(e);
            }
        }

        return contenido;
    }

    private void actualizarIdiomasContenido(String tituloCA, String subtituloCA, String tituloLargoCA, String resumenCA,
                                            String htmlCA, String contenidoCA, byte[] dataBinaryCA, String enlaceDestinoCA, String tituloES,
                                            String subtituloES, String tituloLargoES, String resumenES, String htmlES, String contenidoES,
                                            byte[] dataBinaryES, String enlaceDestinoES, String tituloEN, String subtituloEN, String tituloLargoEN,
                                            String resumenEN, String htmlEN, String contenidoEN, byte[] dataBinaryEN, String enlaceDestinoEN,
                                            String nombreFicheroCA, String nombreFicheroES, String nombreFicheroEN, String tipoMimeFicheroCA,
                                            String tipoMimeFicheroES, String tipoMimeFicheroEN, Contenido contenido, Boolean repetirFicheroCA,
                                            Boolean repetirFicheroEN, Boolean repetirFicheroES, String tipoFormatoCA, String tipoFormatoES,
                                            String tipoFormatoEN, Map<String, String> atributosCA, Map<String, String> atributosES,
                                            Map<String, String> atributosEN) {
        HashSet<ContenidoIdioma> idiomas = new HashSet<ContenidoIdioma>();
        ContenidoIdioma contenidoIdiomaOriginalCA = new ContenidoIdioma();
        ContenidoIdioma contenidoIdiomaOriginalES = new ContenidoIdioma();
        ContenidoIdioma contenidoIdiomaOriginalEN = new ContenidoIdioma();

        for (ContenidoIdioma contenidoIdioma : contenido.getContenidoIdiomas()) {
            if (IdiomaPublicacion.CA.toString().equals(contenidoIdioma.getUpoIdioma().getCodigoISO().toUpperCase())) {
                contenidoIdiomaOriginalCA = contenidoIdioma;
            }
            if (IdiomaPublicacion.ES.toString().equals(contenidoIdioma.getUpoIdioma().getCodigoISO().toUpperCase())) {
                contenidoIdiomaOriginalES = contenidoIdioma;
            }
            if (IdiomaPublicacion.EN.toString().equals(contenidoIdioma.getUpoIdioma().getCodigoISO().toUpperCase())) {
                contenidoIdiomaOriginalEN = contenidoIdioma;
            }
        }

        if (repetirFicheroCA) {
            if (dataBinaryCA != null && dataBinaryCA.length > 0) {
                dataBinaryES = dataBinaryCA;
                htmlES = htmlCA;
                nombreFicheroES = nombreFicheroCA;
                tipoMimeFicheroES = tipoMimeFicheroCA;
                tipoFormatoES = tipoFormatoCA;
                dataBinaryEN = dataBinaryCA;
                htmlEN = htmlCA;
                nombreFicheroEN = nombreFicheroCA;
                tipoMimeFicheroEN = tipoMimeFicheroCA;
                tipoFormatoEN = tipoFormatoCA;
            } else {
                dataBinaryES = contenidoIdiomaOriginalCA.getContenido();
                dataBinaryEN = contenidoIdiomaOriginalCA.getContenido();
                htmlES = contenidoIdiomaOriginalCA.getHtml();
                nombreFicheroES = contenidoIdiomaOriginalCA.getNombreFichero();
                tipoFormatoES = contenidoIdiomaOriginalCA.getTipoFormato();
                tipoMimeFicheroES = contenidoIdiomaOriginalCA.getMimeType();
                htmlEN = contenidoIdiomaOriginalCA.getHtml();
                nombreFicheroEN = contenidoIdiomaOriginalCA.getNombreFichero();
                tipoMimeFicheroEN = contenidoIdiomaOriginalCA.getMimeType();
                tipoFormatoEN = contenidoIdiomaOriginalCA.getTipoFormato();
            }
        }

        if (repetirFicheroES) {
            if (dataBinaryES != null && dataBinaryES.length > 0) {
                dataBinaryCA = dataBinaryES;
                htmlCA = htmlES;
                nombreFicheroCA = nombreFicheroES;
                tipoMimeFicheroCA = tipoMimeFicheroES;
                tipoFormatoCA = tipoFormatoES;
                dataBinaryEN = dataBinaryES;
                htmlEN = htmlES;
                nombreFicheroEN = nombreFicheroES;
                tipoMimeFicheroEN = tipoMimeFicheroES;
                tipoFormatoEN = tipoFormatoES;
            } else {
                dataBinaryCA = contenidoIdiomaOriginalES.getContenido();
                htmlCA = contenidoIdiomaOriginalES.getHtml();
                nombreFicheroCA = contenidoIdiomaOriginalES.getNombreFichero();
                tipoMimeFicheroCA = contenidoIdiomaOriginalES.getMimeType();
                tipoFormatoCA = contenidoIdiomaOriginalES.getTipoFormato();
                dataBinaryEN = contenidoIdiomaOriginalES.getContenido();
                htmlEN = contenidoIdiomaOriginalES.getHtml();
                nombreFicheroEN = contenidoIdiomaOriginalES.getNombreFichero();
                tipoMimeFicheroEN = contenidoIdiomaOriginalES.getMimeType();
                tipoFormatoEN = contenidoIdiomaOriginalES.getTipoFormato();
            }
        }

        if (repetirFicheroEN) {
            if (dataBinaryEN != null && dataBinaryEN.length > 0) {
                dataBinaryCA = dataBinaryEN;
                htmlCA = htmlEN;
                nombreFicheroCA = nombreFicheroEN;
                tipoMimeFicheroCA = tipoMimeFicheroEN;
                tipoFormatoCA = tipoFormatoEN;
                dataBinaryES = dataBinaryEN;
                htmlES = htmlEN;
                nombreFicheroES = nombreFicheroEN;
                tipoMimeFicheroES = tipoMimeFicheroEN;
                tipoFormatoES = tipoFormatoEN;
            } else {
                dataBinaryCA = contenidoIdiomaOriginalEN.getContenido();
                htmlCA = contenidoIdiomaOriginalEN.getHtml();
                nombreFicheroCA = contenidoIdiomaOriginalEN.getNombreFichero();
                tipoMimeFicheroCA = contenidoIdiomaOriginalEN.getMimeType();
                tipoFormatoCA = contenidoIdiomaOriginalEN.getTipoFormato();
                dataBinaryES = contenidoIdiomaOriginalEN.getContenido();
                htmlES = contenidoIdiomaOriginalEN.getHtml();
                nombreFicheroES = contenidoIdiomaOriginalEN.getNombreFichero();
                tipoMimeFicheroES = contenidoIdiomaOriginalEN.getMimeType();
                tipoFormatoES = contenidoIdiomaOriginalEN.getTipoFormato();
            }
        }

        ContenidoIdioma contenidoIdiomaCA =
                setContenidoIdioma(IdiomaPublicacion.CA.toString(), tituloCA, subtituloCA, tituloLargoCA, resumenCA,
                        dataBinaryCA, contenidoCA, htmlCA, nombreFicheroCA, tipoMimeFicheroCA, enlaceDestinoCA,
                        contenido, contenidoIdiomaOriginalCA, tipoFormatoCA, atributosCA);

        ContenidoIdioma contenidoIdiomaES =
                setContenidoIdioma(IdiomaPublicacion.ES.toString(), tituloES, subtituloES, tituloLargoES, resumenES,
                        dataBinaryES, contenidoES, htmlES, nombreFicheroES, tipoMimeFicheroES, enlaceDestinoES,
                        contenido, contenidoIdiomaOriginalES, tipoFormatoES, atributosES);

        ContenidoIdioma contenidoIdiomaEN =
                setContenidoIdioma(IdiomaPublicacion.EN.toString(), tituloEN, subtituloEN, tituloLargoEN, resumenEN,
                        dataBinaryEN, contenidoEN, htmlEN, nombreFicheroEN, tipoMimeFicheroEN, enlaceDestinoEN,
                        contenido, contenidoIdiomaOriginalEN, tipoFormatoEN, atributosEN);

        idiomas.add(contenidoIdiomaES);
        idiomas.add(contenidoIdiomaCA);
        idiomas.add(contenidoIdiomaEN);

        actualizarContenidoIdiomaRecursos(contenidoIdiomaES, contenidoES);
        actualizarContenidoIdiomaRecursos(contenidoIdiomaCA, contenidoCA);
        actualizarContenidoIdiomaRecursos(contenidoIdiomaEN, contenidoEN);

        contenido.setContenidoIdiomas(idiomas);
    }

    private ContenidoIdioma setContenidoIdioma(String codigoISO, String titulo, String subtitulo, String tituloLargo,
                                               String resumen, byte[] dataBinary, String valorContenido, String isHtml, String nombreFichero,
                                               String tipoMimeFichero, String enlaceDestino, Contenido contenido, ContenidoIdioma contenidoIdiomaOriginal,
                                               String tipoFormato, Map<String, String> atributos) {
        Idioma idioma = idiomaService.getIdiomaByCodigoISO(codigoISO.toUpperCase());

        ContenidoIdioma contenidoIdioma = new ContenidoIdioma(idioma);

        if (contenidoIdiomaOriginal == null || contenidoIdiomaOriginal.getId() == null) {
            contenidoIdioma.setUpoObjeto(contenido);
        } else {
            contenidoIdioma = contenidoIdiomaOriginal;
        }

        if (!contenidoIdioma.esIgual(titulo, subtitulo, tituloLargo, isHtml, valorContenido, tipoMimeFichero,
                dataBinary, nombreFichero, resumen, enlaceDestino, tipoFormato)) {
            contenidoIdioma.setFechaModificacion(new Date());
        }

        contenidoIdioma.setTitulo(titulo);
        contenidoIdioma.setSubtitulo(subtitulo);
        contenidoIdioma.setTituloLargo(tituloLargo);
        contenidoIdioma.setEnlaceDestino(enlaceDestino);
        contenidoIdioma.setResumen(resumen);

        if (!contenidoIdioma.hasFormat("BINARIO") && TipoFormatoContenido.BINARIO.toString()
                .equals(tipoFormato.toUpperCase()) && (dataBinary == null || dataBinary.length == 0)) {
            contenidoIdioma.setMimeType(MediaType.APPLICATION_OCTET_STREAM);
            contenidoIdioma.setContenido(null);
            contenidoIdioma.setNombreFichero(null);
            contenidoIdioma.setIdDescarga(null);
            contenidoIdioma.setLongitud(0L);
        }

        contenidoIdioma.setHtml(isHtml);
        contenidoIdioma.setTipoFormato(TipoFormatoContenido.valueOf(tipoFormato.toUpperCase()).toString());

        if (contenidoIdioma.hasFormat("PAGINA")) {
            contenidoIdioma.setMimeType(MediaType.TEXT_HTML);
            contenidoIdioma.setContenido(valorContenido.getBytes());
            contenidoIdioma.setNombreFichero(null);
            contenidoIdioma.setIdDescarga(null);
            contenidoIdioma.setLongitud(Long.valueOf(valorContenido.length()));
        } else if (contenidoIdioma.hasFormat("BINARIO")) {
            if (dataBinary != null && dataBinary.length > 0) {
                contenidoIdioma.setMimeType(tipoMimeFichero);
                contenidoIdioma.setContenido(dataBinary);
                contenidoIdioma.setNombreFichero(nombreFichero);
                contenidoIdioma.setIdDescarga(null);
                contenidoIdioma.setLongitud(Long.valueOf(dataBinary.length));
            }
        } else {
            contenidoIdioma.setMimeType(getMimeTypeVideo(atributos, contenidoIdioma));
            contenidoIdioma.setContenido(null);
            contenidoIdioma.setNombreFichero(null);
            contenidoIdioma.setIdDescarga(null);
            contenidoIdioma.setLongitud(0L);

            actualizarContenidoIdiomaAtributos(contenidoIdioma, atributos);
        }

        return contenidoIdioma;
    }

    private String getMimeTypeVideo(Map<String, String> atributos, ContenidoIdioma contenidoIdioma) {
        String resultado = null;

        for (Map.Entry<String, String> atributo : atributos.entrySet()) {
            resultado = servletContext.getMimeType(atributo.getValue());

            if (atributo.getValue() != null && (contenidoIdioma.hasFormat("VIDEO_YOUTUBE") || contenidoIdioma.hasFormat(
                    "VIDEO_VIMEO"))) {
                return servletContext.getMimeType("video.mp4");
            }

            if (resultado != null && !resultado.isEmpty() && resultado.contains("video")) {
                return resultado;
            }
        }

        return resultado;
    }

    @Transactional
    public void eliminarRegistrosVinculados(Contenido contenido) {
        contenidoIdiomaAtributoService.deleteByContenido(contenido.getId());
        contenidoIdiomaRecursoService.deleteByContenido(contenido.getId());
        deleteVigencias(contenido.getId());
        deleteTags(contenido.getId());
        deletePrioridades(contenido.getId());
        deleteMetadatosByContenidoId(contenido.getId());
    }

    private void actualizarContenidoIdiomaAtributos(ContenidoIdioma contenidoIdioma, Map<String, String> atributos) {
        HashSet<ContenidoIdiomaAtributo> contenidoIdiomaAtributos = new HashSet<ContenidoIdiomaAtributo>();
        ContenidoIdiomaAtributo contenidoIdiomaAtributo = null;

        for (Map.Entry<String, String> atributo : atributos.entrySet()) {
            String valor = atributo.getValue();
            if (valor != null && !valor.isEmpty()) {
                contenidoIdiomaAtributo = new ContenidoIdiomaAtributo();

                contenidoIdiomaAtributo.setClave(atributo.getKey());

                if (contenidoIdioma.getTipoFormato()
                        .equals(TipoFormatoContenido.VIDEO_YOUTUBE.toString()) && isAtributoDeUrlDelVideo(atributo.getKey())) {
                    valor = youtubeUrlFormatter.format(valor);
                }

                contenidoIdiomaAtributo.setValor(valor);
                contenidoIdiomaAtributo.setContenidoIdioma(contenidoIdioma);

                contenidoIdiomaAtributos.add(contenidoIdiomaAtributo);
            }
        }

        contenidoIdioma.setContenidoIdiomaAtributos(contenidoIdiomaAtributos);
    }

    private Boolean isAtributoDeUrlDelVideo(String key) {
        return (key.startsWith("_video") && key.length() == 8);
    }

    private void actualizarContenidoIdiomaRecursos(ContenidoIdioma contenidoIdioma, String contenido) {
        UrlExtractor urlExtractor = new UrlExtractor();
        List<String> recursos;
        ContenidoIdiomaRecurso contenidoIdiomaRecurso = new ContenidoIdiomaRecurso();
        HashSet<ContenidoIdiomaRecurso> contenidoIdiomaRecursos = new HashSet<ContenidoIdiomaRecurso>();

        if (contenido != null && !contenido.isEmpty()) {
            urlExtractor.setHtml(contenido);
            recursos = urlExtractor.extractAll();

            for (String recurso : recursos) {
                contenidoIdiomaRecurso = new ContenidoIdiomaRecurso();
                contenidoIdiomaRecurso.setUrl(recurso);
                contenidoIdiomaRecurso.setContentType(ContenidoIdiomaRecurso.getTipoMime(recurso));
                contenidoIdiomaRecurso.setFecha(new Date());
                contenidoIdiomaRecurso.setContenidoIdioma(contenidoIdioma);

                contenidoIdiomaRecursos.add(contenidoIdiomaRecurso);
            }

            contenidoIdioma.setContenidoIdiomaRecursos(contenidoIdiomaRecursos);
        }
    }

    private void actualizaResponsable(Long personaResponsable, Contenido contenido) {
        if (personaResponsable != null && !"".equals(personaResponsable)) {
            Persona responsable = new Persona();
            responsable.setId(personaResponsable);
            contenido.setUpoExtPersona1(responsable);
        } else {
            contenido.setUpoExtPersona1(null);
        }
    }

    private void actualizaOrigenInformacion(String origenInformacion, Contenido contenido) {
        if (origenInformacion != null && !"".equals(origenInformacion)) {
            OrigenInformacion origen = new OrigenInformacion();
            origen.setId(Long.parseLong(origenInformacion));
            contenido.setOrigenInformacion(origen);
        } else {
            contenido.setOrigenInformacion(null);
        }

    }

    private void actualizarTagsContenido(String tags, Contenido contenido) {
        contenido.setUpoContenidosTags(new HashSet<>());

        if (tags != null && !tags.isEmpty()) {
            Set<ContenidoTag> contenidoTags = convertStringTocontenidoTags(tags, contenido);
            contenido.setUpoContenidosTags(contenidoTags);
        }
    }

    private void actualizarMetadatos(Set<ContenidoMetadato> metadatos, Contenido contenido) {
        for (ContenidoMetadato metadato : metadatos) {
            metadato.setUpoObjeto(contenido);
        }

        contenido.setUpoContenidosMetadatos(metadatos);
    }

    private void actualizarPrioridadContenido(String prioridad, Contenido contenido)
            throws SeIndicaUnaHoraSinFechaAsociadaException, PrioridadSinNivelDePrioridadException, ParseException,
            FechaFinPrioridadSuperiorAFechaInicioException {
        Set<PrioridadContenido> prioridadContenido = null;

        if (prioridad != null && !prioridad.isEmpty()) {
            prioridadContenido = convertPrioridadStringToSet(prioridad, contenido);
        }

        contenido.setUpoObjetosPrioridades(prioridadContenido);
    }

    private Set<PrioridadContenido> convertPrioridadStringToSet(String prioridad, Contenido contenido)
            throws ParseException, SeIndicaUnaHoraSinFechaAsociadaException,
            FechaFinPrioridadSuperiorAFechaInicioException, PrioridadSinNivelDePrioridadException {
        String[] prioridadArray = prioridad.split("\\,");

        Set<PrioridadContenido> prioridadContenido = new HashSet<>();

        for (String fecha : prioridadArray) {
            String[] linea = fecha.split("\\|");

            if (linea.length != 5) {
                throw new PrioridadSinNivelDePrioridadException();
            }

            PrioridadContenido prioridadContenidoTemp = new PrioridadContenido();

            prioridadContenidoTemp.setPrioridad(TipoPrioridadContenido.valueOf(linea[4].toUpperCase()));
            prioridadContenidoTemp.setFechaInicio(getDate(linea[0] + " 00:00:00"));
            prioridadContenidoTemp.setFechaFin(getDate(linea[2] + " 00:00:00"));

            if (linea[1] != null && !linea[1].trim().isEmpty()) {
                prioridadContenidoTemp.setHoraInicio(getTime(linea[1] + ":00"));
            }

            if (linea[3] != null && !linea[3].trim().isEmpty()) {
                prioridadContenidoTemp.setHoraFin(getTime(linea[3] + ":00"));
            }

            prioridadContenidoTemp.checkFechasCorrectas();

            prioridadContenidoTemp.setUpoObjeto(contenido);
            prioridadContenido.add(prioridadContenidoTemp);
        }

        return prioridadContenido;
    }

    private void actualizarVigenciasContenido(String vigencia, Contenido contenido) {
        Set<VigenciaContenido> vigenciaContenido = null;

        if (vigencia != null && !vigencia.isEmpty()) {
            vigenciaContenido = convertVigenciasStringToSet(vigencia, contenido);
        }

        contenido.setUpoVigenciasObjetos(vigenciaContenido);
    }

    private void actualizarFechasEvento(String fechasEvento, NodoMapaContenido nodoMapaContenido) {
        Set<FechaEvento> fechasEventoSet = null;

        if (fechasEvento != null && !fechasEvento.isEmpty()) {
            fechasEventoSet = convertFechasEventoStringToSet(fechasEvento, nodoMapaContenido);
        }

        nodoMapaContenido.setFechasEvento(fechasEventoSet);
    }

    private Set<VigenciaContenido> convertVigenciasStringToSet(String vigencia, Contenido contenido) {
        String[] vigenciaArray = vigencia.split("\\,");

        Set<VigenciaContenido> vigenciaContenido = new HashSet<VigenciaContenido>();

        for (String fecha : vigenciaArray) {
            String[] fechaYHoras = fecha.split("\\|");

            VigenciaContenido vigenciaContenidoTemp = new VigenciaContenido();

            vigenciaContenidoTemp.setFecha(getDate(fechaYHoras[0] + " 00:00:00"));

            if (fechaYHoras.length >= 2 && fechaYHoras[1] != null && !fechaYHoras[1].trim().isEmpty()) {
                vigenciaContenidoTemp.setHoraInicio(getDate(fechaYHoras[0] + " " + fechaYHoras[1] + ":00"));
            }

            if (fechaYHoras.length == 3 && !fechaYHoras[2].trim().isEmpty()) {
                vigenciaContenidoTemp.setHoraFin(getDate(fechaYHoras[0] + " " + fechaYHoras[2] + ":00"));
            }

            vigenciaContenidoTemp.setContenido(contenido);

            vigenciaContenido.add(vigenciaContenidoTemp);
        }

        return vigenciaContenido;
    }

    private Set<FechaEvento> convertFechasEventoStringToSet(String fechasEvento, NodoMapaContenido nodoMapaContenido) {
        String[] fechasEventoArray = fechasEvento.split("\\,");

        Set<FechaEvento> fechasEventoSet = new HashSet<>();

        for (String fecha : fechasEventoArray) {
            String[] fechaYHoras = fecha.split("\\|");

            FechaEvento fechaEventoTemp = new FechaEvento();

            fechaEventoTemp.setFecha(getDate(fechaYHoras[0] + " 00:00:00"));

            if (fechaYHoras.length >= 2 && fechaYHoras[1] != null && !fechaYHoras[1].trim().isEmpty()) {
                fechaEventoTemp.setHoraInicio(getDate(fechaYHoras[0] + " " + fechaYHoras[1] + ":00"));
            }

            if (fechaYHoras.length == 3 && !fechaYHoras[2].trim().isEmpty()) {
                fechaEventoTemp.setHoraFin(getDate(fechaYHoras[0] + " " + fechaYHoras[2] + ":00"));
            }

            fechaEventoTemp.setNodoMapaContenido(nodoMapaContenido);

            fechasEventoSet.add(fechaEventoTemp);
        }

        return fechasEventoSet;
    }

    private Set<ContenidoTag> convertStringTocontenidoTags(String tags, Contenido contenido) {
        String[] tagsArray = tags.split("\\,");

        Set<ContenidoTag> contenidoTags = new HashSet<ContenidoTag>();

        for (String tag : tagsArray) {
            ContenidoTag contenidoTag = new ContenidoTag();
            contenidoTag.setTagname(tag.trim());
            contenidoTag.setUpoObjeto(contenido);
            contenidoTags.add(contenidoTag);
        }

        return contenidoTags;
    }

    public Contenido crearNuevoContenido(Contenido contenido)
            throws CrearNuevoContenidoException {
        try {
            contenido.setId(null);

            contenido.setFechaCreacion(new Date());

            for (VigenciaContenido vigenciaContenido : contenido.getUpoVigenciasObjetos()) {
                vigenciaContenido.setContenido(contenido);
                vigenciaContenido.setId(null);
            }

            for (PrioridadContenido prioridadContenido : contenido.getUpoObjetosPrioridades()) {
                prioridadContenido.setUpoObjeto(contenido);
                prioridadContenido.setId(null);
            }

            for (ContenidoTag contenidoTag : contenido.getUpoContenidosTags()) {
                contenidoTag.setUpoObjeto(contenido);
                contenidoTag.setId(null);
            }

            for (ContenidoMetadato contenidoMetadato : contenido.getUpoContenidosMetadatos()) {
                contenidoMetadato.setUpoObjeto(contenido);
                contenidoMetadato.setId(null);
            }

            for (ContenidoIdioma contenidoIdioma : contenido.getContenidoIdiomas()) {
                crearNuevoContenidoIdioma(contenidoIdioma, contenido);
            }

            return contenido;
        } catch (Exception e) {
            throw new CrearNuevoContenidoException(e);
        }
    }

    private ContenidoIdioma crearNuevoContenidoIdioma(ContenidoIdioma contenidoIdioma, Contenido contenido)
            throws IOException, ImportarDocumentoException {
        contenidoIdioma.setUpoObjeto(contenido);
        contenidoIdioma.setId(null);

        if (hasAExternalReference(contenidoIdioma)) {
            URL urlParsed = new URL(getDocumentsServer() + contenidoIdioma.getIdDescarga());
            URLConnection urlConnection = urlParsed.openConnection();
            HttpURLConnection httpConnection = (HttpURLConnection) urlConnection;

            if (httpConnection.getResponseCode() != 200) {
                throw new ImportarDocumentoException("Errada al importar un document amb url: " + urlParsed.toString());
            }

            InputStream urlContent = urlParsed.openStream();

            contenidoIdioma.setIdDescarga(null);
            contenidoIdioma.setContenido(StreamUtils.inputStreamToByteArray(urlContent));
        }

        for (ContenidoIdiomaRecurso contenidoIdiomaRecurso : contenidoIdioma.getContenidoIdiomaRecursos()) {
            contenidoIdiomaRecurso.setContenidoIdioma(contenidoIdioma);
            contenidoIdiomaRecurso.setId(null);
        }

        for (ContenidoIdiomaAtributo contenidoIdiomaAtributo : contenidoIdioma.getContenidoIdiomaAtributos()) {
            contenidoIdiomaAtributo.setContenidoIdioma(contenidoIdioma);
            contenidoIdiomaAtributo.setId(null);
        }

        return contenidoIdioma;
    }

    private boolean hasAExternalReference(ContenidoIdioma contenidoIdioma) {
        return contenidoIdioma.getIdDescarga() != null && !contenidoIdioma.getIdDescarga().isEmpty();
    }

    private Date getTime(String time) {
        if (time != null && !time.isEmpty()) {
            return getDate("01/01/2011 " + time + ":00");
        } else {
            return null;
        }
    }

    private Date getDate(String value) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        try {
            return formatter.parse(value);
        } catch (Exception e) {
            return null;
        }
    }

    public void ajustaAPropuestaExterna(Contenido contenido) {
        NodoMapaContenido nodoMapaContenido = contenido.getUpoMapasObjetos().stream().findFirst().orElseGet(null);

        if (nodoMapaContenido == null) return;

        nodoMapaContenido.setEstadoModeracion(EstadoModeracion.PENDIENTE);
        nodoMapaContenido.setPropuestaExterna(true);

        nodoMapaContenidoDAO.update(nodoMapaContenido);
    }
}
