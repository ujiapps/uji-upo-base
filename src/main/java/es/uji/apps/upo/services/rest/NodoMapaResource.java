package es.uji.apps.upo.services.rest;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import es.uji.apps.upo.exceptions.*;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.TipoAccionCrearContenidoDesdeRecurso;
import es.uji.apps.upo.model.enums.TipoReferenciaContenido;
import es.uji.apps.upo.services.*;
import es.uji.apps.upo.services.nodomapa.EstructuraFechaService;
import es.uji.apps.upo.services.nodomapa.EstructuraRevistaService;
import es.uji.apps.upo.services.nodomapa.EstructuraVoxUjiService;
import es.uji.apps.upo.services.nodomapa.NodoMapaService;
import es.uji.commons.rest.*;
import es.uji.commons.rest.exceptions.ParametrosObligatoriosException;
import es.uji.commons.sso.AccessManager;

import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Path("mapa")
public class NodoMapaResource extends CoreBaseService
{
    public static final Integer DEFAULT_NODO_MAPA_NIVEL = 1;
    private static final Integer ID_FOR_GET_ROOT_NODE = -1;
    private static final String URL_PATH_FOR_FIRST_NODE = "first";

    @Context
    ServletContext servletContext;

    @InjectParam
    private PlantillaService plantillaService;

    @InjectParam
    private FranquiciaService franquiciaService;

    @InjectParam
    private NodoMapaService nodoMapaService;

    @InjectParam
    private EstructuraRevistaService estructuraRevistaService;

    @InjectParam
    private EstructuraFechaService estructuraFechaService;

    @InjectParam
    private EstructuraVoxUjiService estructuraVoxujiService;

    @InjectParam
    private NodoMapaContenidoService nodoMapaContenidoService;

    @InjectParam
    private NodoMapaPlantillaService nodoMapaPlantillaService;

    @InjectParam
    private PersonaService personaService;

    @InjectParam
    private IdiomaService idiomaService;

    @POST
    @Path("{id}/contenido")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity insertarContenido(@PathParam("id") String mapaId, @FormParam("contenidoId") String contenidoId,
                                      @FormParam("nodoMapaContenidoId") String nodoMapaContenidoId,
                                      @FormParam("nodoMapaOrigenId") String nodoMapaOrigenId)
            throws NodoMapaContenidoDebeTenerUnContenidoNormalException, NumberFormatException,
            ContenidoYaPropuestoException, ContenidoConMismoUrlPathException, InsertadoContenidoNoAutorizadoException
    {
        Long personaId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(contenidoId, nodoMapaContenidoId, nodoMapaOrigenId);

        Persona persona = new Persona();
        persona.setId(personaId);

        NodoMapaContenido nodoMapaContenido = new NodoMapaContenido();
        nodoMapaContenido.setPersonaPropuesta(persona);
        nodoMapaContenido.setTipo(TipoReferenciaContenido.LINK);

        nodoMapaContenidoService.insertContenidoAModerar(nodoMapaContenido,
                nodoMapaService.getNodoMapa(Long.parseLong(mapaId), false), Long.parseLong(contenidoId),
                Long.parseLong(nodoMapaContenidoId));

        return UIEntity.toUI(nodoMapaContenido);
    }

    @POST
    @Path("{id}/recurso")
    @Produces(MediaType.TEXT_XML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void creaContenidoDesdeRecurso(@PathParam("id") String mapaId, @FormParam("url") String url,
                                          @FormParam("contenidoId") String contenidoId, @FormParam("idioma") String idiomaCodigoIso,
                                          @FormParam("tipo") String tipoAccion)
            throws InsertadoContenidoNoAutorizadoException, ImportarDocumentoException, IOException,
            ContenidoConMismoUrlPathException, CrearNuevoContenidoException
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona();
        persona.setId(personaId);

        ParamUtils.checkNotNull(mapaId, url, contenidoId);

        NodoMapa nodoMapa = nodoMapaService.getNodoMapa(ParamUtils.parseLong(mapaId), true);
        TipoAccionCrearContenidoDesdeRecurso tipo = TipoAccionCrearContenidoDesdeRecurso.COPIAR;

        if ("link".equals(tipoAccion))
        {
            tipo = TipoAccionCrearContenidoDesdeRecurso.ENLAZAR;
        }

        nodoMapaService.crearContenidoDesdeRecurso(nodoMapa, persona, url, ParamUtils.parseLong(contenidoId),
                idiomaCodigoIso, tipo);
    }

    @POST
    @Path("{id}/fichero")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_HTML)
    public UIEntity insertDocument(@PathParam("id") Long mapaId, FormDataMultiPart multipart)
            throws GeneralUPOException, IOException
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona();
        persona.setId(personaId);

        FormDataBodyPart cd = (FormDataBodyPart) multipart.getBodyParts().get(0);
        String nombre = getFileName(cd.getFormDataContentDisposition());
        byte[] file = StreamUtils.inputStreamToByteArray(cd.getValueAs(InputStream.class));

        if (file.length > 0 && nombre != null)
        {
            try
            {
                String tipoMime = getMimeType(cd.getMediaType().toString());

                nodoMapaService.crearContenidoDesdeFichero(persona, nombre, tipoMime, file, mapaId);
                return new UIEntity();
            } catch (Exception e)
            {
                throw new GeneralUPOException();
            }
        } else
        {
            throw new GeneralUPOException();
        }
    }

    private String getFileName(FormDataContentDisposition fileDetail)
    {
        String nombre = null;

        if (fileDetail != null)
        {
            nombre = fileDetail.getFileName();
        }
        return nombre;
    }

    private String getMimeType(String tipoMime)
    {
        if (tipoMime == null)
        {
            tipoMime = MediaType.APPLICATION_OCTET_STREAM;
        }
        return tipoMime;
    }

    @GET
    @Path("{id}/plantilla")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPlantillasByMapaId(@PathParam("id") String mapaId,
                                                @DefaultValue("0") @QueryParam("noAsignadas") String noAsignadas)
    {
        List<Plantilla> plantillas = null;

        if (noAsignadas.equals("1"))
        {
            return UIEntity.toUI(plantillaService.getPlantillasVigentes());
        }

        plantillas = nodoMapaService.getPlantillasByMapaId(Long.parseLong(mapaId));

        return getUIEntityFromPlantillasWithAttributes(ParamUtils.parseLong(mapaId), plantillas);
    }

    private List<UIEntity> getUIEntityFromPlantillasWithAttributes(Long mapaId, List<Plantilla> plantillas)
    {
        List<UIEntity> entities = new ArrayList<UIEntity>();
        UIEntity entity = null;

        for (Plantilla plantilla : plantillas)
        {
            entity = UIEntity.toUI(plantilla);
            setAttributesToPlantillaUIEntity(mapaId, entity, plantilla);
            entities.add(entity);
        }

        return entities;
    }

    private void setAttributesToPlantillaUIEntity(Long mapaId, UIEntity entity, Plantilla plantilla)
    {
        entity.put("tipo", plantilla.getTipo());
        entity.put("nivel", plantillaService.getNivelByMapaId(mapaId, plantilla));
    }

    @DELETE
    @Path("{id}/plantilla/{plantillaId}")
    public void deletePlantilla(@PathParam("id") String mapaId, @PathParam("plantillaId") String plantillaId)
            throws BorradoContenidoNoAutorizadoException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona();
        persona.setId(userId);

        NodoMapa nodoMapa = nodoMapaService.getNodoMapa(Long.parseLong(mapaId), false);
        nodoMapaService.deletePlantilla(persona, nodoMapa, Long.parseLong(plantillaId));
    }

    @POST
    @Path("{id}/plantilla")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insertarPlantilla(@PathParam("id") String mapaId, UIEntity uiEntity)
            throws InsertarPlantillaNoAutorizadoException, InsertadaPlantillaDelMismoTipoException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona();
        persona.setId(userId);

        if (uiEntity.get("id") == null)
        {
            return uiEntity;
        }

        Plantilla plantilla = uiEntity.toModel(Plantilla.class);
        String nivel = uiEntity.get("nivel");

        ParamUtils.checkNotNull(mapaId, nivel);

        NodoMapa nodoMapa = nodoMapaService.getNodoMapa(Long.parseLong(mapaId), false);

        plantilla = plantillaService.getPlantillaById(plantilla.getId());
        nodoMapaService.insertarPlantilla(persona, plantilla, nodoMapa, Integer.parseInt(nivel));

        uiEntity = UIEntity.toUI(plantilla);

        setAttributesToPlantillaUIEntity(ParamUtils.parseLong(mapaId), uiEntity, plantilla);

        return uiEntity;
    }

    @PUT
    @Path("{id}/plantilla/{plantillaId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updatePlantilla(@PathParam("id") String mapaId, @PathParam("plantillaId") String plantillaId,
                                    UIEntity uiEntity)
    {
        Long userId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona();
        persona.setId(userId);

        Plantilla plantilla = plantillaService.getPlantillaById(ParamUtils.parseLong(plantillaId));
        String nivel = uiEntity.get("nivel");

        ParamUtils.checkNotNull(mapaId, plantillaId, nivel);

        uiEntity = UIEntity.toUI(plantilla);

        plantillaService.setNivelByMapaId(ParamUtils.parseLong(mapaId), Integer.parseInt(nivel), plantilla);

        setAttributesToPlantillaUIEntity(ParamUtils.parseLong(mapaId), uiEntity, plantilla);

        return uiEntity;
    }

    @POST
    @Path("{id}/franquicia")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> insertarFranquicia(@PathParam("id") String mapaId,
                                             @FormParam("franquiciaId") String franquiciaId)
            throws InsertadoContenidoNoAutorizadoException, ActualizadoContenidoNoAutorizadoException,
            NodoConNombreYaExistenteEnMismoPadreException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona();
        persona.setId(userId);

        NodoMapa nodoMapa = nodoMapaService.getNodoMapa(Long.parseLong(mapaId), false);

        Franquicia franquicia = franquiciaService.getFranquicia(Long.parseLong(franquiciaId));
        nodoMapa.setUpoFranquicia(franquicia);

        nodoMapaService.insertOrUpdate(persona, nodoMapa);

        UIEntity uiEntity = UIEntity.toUI(nodoMapa);

        return Collections.singletonList(uiEntity);
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getNodoMapaById(@PathParam("id") String mapaId)
    {
        List<NodoMapa> listaNodoMapa = nodoMapaService.getNodoMapaByMapaId(Long.parseLong(mapaId));

        if (listaNodoMapa != null && !listaNodoMapa.isEmpty())
        {
            NodoMapa nodoMapa = listaNodoMapa.get(0);
            UIEntity entity = UIEntity.toUI(nodoMapa);

            entity.put("franquiciaId", nodoMapa.getUpoFranquicia().getId());

            if (nodoMapa.getMenu() != null)
            {
                entity.put("menuId", nodoMapa.getMenu().getId());
            }

            List<NodoMapaPlantilla> nodoMapaPlantillas =
                    nodoMapaPlantillaService.getNodoMapaPlantillaByNodoMapaId(nodoMapa.getId());

            if (nodoMapaPlantillas.size() == 1)
            {
                entity.put("plantillaId", nodoMapaPlantillas.get(0).getUpoPlantilla().getId());
            }

            List<String> tags = getListaTagsDeNodosDondeYaEstaPropuesto(mapaId);
            entity.put("nodoMapaPropuestaTags", org.apache.commons.lang3.StringUtils.join(tags.toArray(), ","));

            return Collections.singletonList(entity);
        }

        return Collections.emptyList();
    }

    private List<String> getListaTagsDeNodosDondeYaEstaPropuesto(String mapaId)
    {
        List<NodoMapa> nodosMapaYaCreadosComoPropuestas = nodoMapaService.getNodosMapaYaPropuestos(Long.parseLong(mapaId));
        List<NodoMapa> nodosMapaPadres = nodosMapaYaCreadosComoPropuestas.stream().map(NodoMapa::getUpoMapa).collect(Collectors.toList());

        return nodoMapaService.getListaTagsByNodosMapa(nodosMapaPadres).stream().map(NodoMapaTag::getTag).distinct().collect(Collectors.toList());
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> getNodoMapa(@QueryParam("node") String nodoId)
    {
        List<NodoMapa> hijos = null;

        if ("root".equals(nodoId))
        {
            hijos = nodoMapaService.getHijos(new Long(ID_FOR_GET_ROOT_NODE));
        } else
        {
            hijos = nodoMapaService.getHijos(Long.parseLong(nodoId));
        }

        return toUI(hijos);
    }

    public List<UIEntity> toUI(List<NodoMapa> nodos)
    {
        return nodos.stream().map(nodo -> nodoMapaToUI(nodo)).collect(Collectors.toList());
    }

    private UIEntity nodoMapaToUI(NodoMapa hijo)
    {
        UIEntity uiEntity = new UIEntity();

        Long personaId = AccessManager.getConnectedUserId(request);

        Persona persona = new Persona();
        persona.setId(personaId);

        Boolean permisosFranquicia = personaService.isNodoMapaAdmin(hijo.getUrlCompleta(), persona.getId());
        Boolean bloqueado = personaService.isNodoMapaLocked(hijo.getUrlCompleta());

        uiEntity.put("id", String.valueOf(hijo.getId()));
        uiEntity.put("title", hijo.getUrlPath());
        uiEntity.put("text", hijo.getUrlPath());
        uiEntity.put("urlPath", (hijo.getUrlPath().equals("/") ? URL_PATH_FOR_FIRST_NODE : hijo.getUrlPath()));
        uiEntity.put("administrador", permisosFranquicia && !bloqueado);
        uiEntity.put("bloqueado", bloqueado);
        uiEntity.put("permisosFranquicia", permisosFranquicia);
        uiEntity.put("nodoBloqueado", hijo.isBloqueado());
        uiEntity.put("personaPropuestaId",
                (hijo.getPersonaPropuesta() != null) ? hijo.getPersonaPropuesta().getId() : null);
        uiEntity.put("urlCompleta", hijo.getUrlCompleta());
        uiEntity.put("leaf", false);

        setTieneNodoMapasHijoToTreeRowNodo(hijo, uiEntity);

        return uiEntity;
    }

    private void setTieneNodoMapasHijoToTreeRowNodo(NodoMapa hijo, UIEntity uiEntity)
    {
        if (nodoMapaService.hasNodosMapaHijo(hijo))
        {
            uiEntity.put("nodosMapaHijo", true);
            uiEntity.put("leaf", false);
        } else
        {
            uiEntity.put("nodosMapaHijo", false);
            uiEntity.put("children", "[]");
            uiEntity.put("expanded", true);
        }
    }

    @POST
    @Path("{id}/proponer")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void updateNodoMapa(@PathParam("id") String nodoMapaDestinoId,
                               @FormParam("nodoMapaOrigenId") String nodoMapaOrigenId, @FormParam("incluirHijos") Boolean incluirHijos)
            throws NodoNoSePuedeProponerException, NodoConNombreYaExistenteEnMismoPadreException,
            NodoMapaContenidoDebeTenerUnContenidoNormalException, ContenidoYaPropuestoException,
            ContenidoConMismoUrlPathException, TipoContenidoNoAdecuadoException,
            ActualizadoContenidoNoAutorizadoException, CrearNuevoContenidoException
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona();
        persona.setId(personaId);

        if (nodoMapaOrigenId != null && !"".equals(nodoMapaOrigenId))
        {
            NodoMapa nodoMapaOrigen = nodoMapaService.getNodoMapa(Long.parseLong(nodoMapaOrigenId), true);
            NodoMapa nodoMapaDestino = nodoMapaService.getNodoMapa(Long.parseLong(nodoMapaDestinoId), false);

            nodoMapaOrigen = nodoMapaService.asignaUltimaPosicionANodo(nodoMapaOrigen, nodoMapaDestino);

            nodoMapaService.crearNodoMapaHijoYEnlacesAContenidos(nodoMapaDestino, nodoMapaOrigen, persona,
                    incluirHijos, false);
        }
    }

    @POST
    @Path("{id}/duplicar")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void duplicateNodoMapa(@PathParam("id") String nodoMapaDestinoId,
                                  @FormParam("nodoMapaOrigenId") String nodoMapaOrigenId, @FormParam("nombreNodo") String nombreNodo,
                                  @FormParam("duplicarContenidos") Boolean duplicarContenidos)
            throws NodoNoSePuedeDuplicarException, NodoConNombreYaExistenteEnMismoPadreException,
            ContenidoYaPropuestoException, NodoMapaContenidoDebeTenerUnContenidoNormalException,
            ContenidoConMismoUrlPathException, TipoContenidoNoAdecuadoException,
            ActualizadoContenidoNoAutorizadoException, CrearNuevoContenidoException
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona();
        persona.setId(personaId);

        NodoMapa nodoMapaOrigen = null;
        NodoMapa nodoMapaDestino = nodoMapaService.getNodoMapa(Long.parseLong(nodoMapaDestinoId), false);

        if (nodoMapaOrigenId != null && !"".equals(nodoMapaOrigenId))
        {
            nodoMapaOrigen = nodoMapaService.getNodoMapa(Long.parseLong(nodoMapaOrigenId), true);

            nombreNodo = nodoMapaOrigen.getUrlPath();
        } else
        {
            nodoMapaOrigen = nodoMapaDestino;
            nodoMapaDestino = nodoMapaDestino.getUpoMapa();
        }

        nodoMapaService.duplicarEstructura(nodoMapaOrigen, nodoMapaDestino, nombreNodo, duplicarContenidos, persona);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void updateNodoMapa(@PathParam("id") String nodoMapaId, @FormParam("franquiciaId") String franquiciaId,
                               @FormParam("menuId") String menuId, @FormParam("privado") Boolean privado,
                               @FormParam("menuHeredado") Boolean menuHeredado, @FormParam("nombreNodo") String nombreNodo,
                               @FormParam("nodoMapaDestinoId") String nodoMapaDestinoId, @FormParam("sincronizado") Boolean sincronizado,
                               @FormParam("sincroDesdeNodoMapa") String sincroDesdeNodoMapa,
                               @FormParam("periodicidadSincro") String periodicidadSincro,
                               @FormParam("numItemsSincro") String numItemsSincro, @FormParam("numNivelesSincro") String numNivelesSincro,
                               @FormParam("tituloPublicacionCA") String tituloPublicacionCA,
                               @FormParam("tituloPublicacionES") String tituloPublicacionES,
                               @FormParam("tituloPublicacionEN") String tituloPublicacionEN,
                               @FormParam("enlaceDestino") String enlaceDestino,
                               @FormParam("contenidoRelacionado") Boolean contenidoRelacionado,
                               @FormParam("activarPropuestasWeb") Boolean activarPropuestasWeb,
                               @FormParam("descripcionPropuestasWeb") String descripcionPropuestasWeb,
                               @FormParam("formGestionNodo") Boolean formGestionNodo, @FormParam("mostrarReloj") Boolean mostrarReloj,
                               @FormParam("mostrarMenuPerfilesPlegado") Boolean mostrarMenuPerfilesPlegado,
                               @FormParam("mostrarRecursosRelacionados") Boolean mostrarRecursosRelacionados,
                               @FormParam("mostrarTags") Boolean mostrarTags,
                               @FormParam("mostrarFechaModificacion") Boolean mostrarFechaModificacion,
                               @FormParam("mostrarFuente") Boolean mostrarFuente,
                               @FormParam("mostrarRedesSociales") Boolean mostrarRedesSociales,
                               @FormParam("mostrarInformacionProporcionada") Boolean mostrarInformacionProporcionada,
                               @FormParam("mostrarBarraGris") Boolean mostrarBarraGris, @FormParam("paginado") Boolean paginado,
                               @FormParam("mostrarComoMiga") Boolean mostrarComoMiga, @FormParam("tituloMigaCA") String tituloMigaCA,
                               @FormParam("tituloMigaES") String tituloMigaES, @FormParam("tituloMigaEN") String tituloMigaEN,
                               @FormParam("mostrarFechasEvento") Boolean mostrarFechasEvento)
            throws NodoNoSePuedeRenombrarException, NodoNoSePuedeArrastrarException,
            NodoConNombreYaExistenteEnMismoPadreException, ActualizadoContenidoNoAutorizadoException
    {
        Long personaId = AccessManager.getConnectedUserId(request);

        Persona persona = new Persona();
        persona.setId(personaId);

        NodoMapa nodoMapa = nodoMapaService.getNodoMapa(Long.parseLong(nodoMapaId), false);

        if (formGestionNodo != null)
        {
            nodoMapa.setPrivado(isTrue(privado));
            nodoMapa.setContenidoRelacionado(isTrue(contenidoRelacionado));
            nodoMapa.setMostrarReloj(isTrue(mostrarReloj));
            nodoMapa.setMostrarMenuPerfilesPlegado(isTrue(mostrarMenuPerfilesPlegado));
            nodoMapa.setMostrarRecursosRelacionados(isTrue(mostrarRecursosRelacionados));
            nodoMapa.setMostrarTags(isTrue(mostrarTags));
            nodoMapa.setMostrarFechaModificacion(isTrue(mostrarFechaModificacion));
            nodoMapa.setMostrarFuente(isTrue(mostrarFuente));
            nodoMapa.setMostrarRedesSociales(isTrue(mostrarRedesSociales));
            nodoMapa.setMostrarInformacionProporcionada(isTrue(mostrarInformacionProporcionada));
            nodoMapa.setMostrarBarraGris(isTrue(mostrarBarraGris));
            nodoMapa.setPaginado(isTrue(paginado));
            nodoMapa.setMostrarFechasEvento(isTrue(mostrarFechasEvento));

            nodoMapaService.update(persona, nodoMapa);

            if (franquiciaId != null && !"".equals(franquiciaId))
            {
                Franquicia franquicia = franquiciaService.getFranquicia(Long.parseLong(franquiciaId));
                nodoMapa.setUpoFranquicia(franquicia);

                nodoMapaService.update(persona, nodoMapa);
            }

            if (menuHeredado != null && menuHeredado == true)
            {
                nodoMapaService.updateMenuAHeredado(nodoMapa, persona);
            }

            if (menuHeredado == null && menuId != null && !"".equals(menuId))
            {
                nodoMapaService.updateMenuANoHeredado(nodoMapa, persona, ParamUtils.parseLong(menuId));
            }

            if (sincronizado != null && sincronizado == true)
            {
                ParamUtils.checkNotNull(sincroDesdeNodoMapa, numItemsSincro);

                nodoMapa.setSincroDesdeNodoMapa(sincroDesdeNodoMapa);
                nodoMapa.setPeriodicidadSincro(
                        periodicidadSincro.isEmpty() ? null : (Integer.parseInt(periodicidadSincro)));
                nodoMapa.setNumItemsSincro(Long.parseLong(numItemsSincro));
                nodoMapa.setNumNivelesSincro(numNivelesSincro.isEmpty() ? null : (Integer.parseInt(numNivelesSincro)));

                nodoMapaService.update(persona, nodoMapa);
            }

            if (sincronizado == null || sincronizado == false)
            {
                nodoMapa.setSincroDesdeNodoMapa(null);
                nodoMapa.setPeriodicidadSincro(null);
                nodoMapa.setNumItemsSincro(null);
                nodoMapa.setNumNivelesSincro(null);

                nodoMapaService.update(persona, nodoMapa);
            }

            if (tituloPublicacionCA != null || tituloPublicacionES != null || tituloPublicacionEN != null)
            {
                nodoMapa.setTituloPublicacionCA(tituloPublicacionCA);
                nodoMapa.setTituloPublicacionES(tituloPublicacionES);
                nodoMapa.setTituloPublicacionEN(tituloPublicacionEN);

                nodoMapaService.update(persona, nodoMapa);
            }

            if (enlaceDestino != null)
            {
                nodoMapa.setEnlaceDestino(enlaceDestino);

                nodoMapaService.update(persona, nodoMapa);
            }

            if (activarPropuestasWeb == null || activarPropuestasWeb == false)
            {
                nodoMapa.setActivarPropuestasWeb(false);
                nodoMapa.setDescripcionPropuestasWeb(null);

                nodoMapaService.update(persona, nodoMapa);
            }

            if (activarPropuestasWeb != null && activarPropuestasWeb == true)
            {
                ParamUtils.checkNotNull(descripcionPropuestasWeb);

                nodoMapa.setActivarPropuestasWeb(true);
                nodoMapa.setDescripcionPropuestasWeb(descripcionPropuestasWeb);

                nodoMapaService.update(persona, nodoMapa);
            }

            if (mostrarComoMiga == null || mostrarComoMiga == false)
            {
                nodoMapa.setMostrarComoMiga(false);

                nodoMapa.setTituloMigaCA(null);
                nodoMapa.setTituloMigaES(null);
                nodoMapa.setTituloMigaEN(null);

                nodoMapaService.update(persona, nodoMapa);
            }

            if (mostrarComoMiga != null && mostrarComoMiga == true)
            {
                nodoMapa.setMostrarComoMiga(true);

                nodoMapa.setTituloMigaCA(tituloMigaCA);
                nodoMapa.setTituloMigaES(tituloMigaES);
                nodoMapa.setTituloMigaEN(tituloMigaEN);

                nodoMapaService.update(persona, nodoMapa);
            }
        }

        if (nombreNodo != null && !"".equals(nombreNodo))
        {
            nodoMapaService.actualizarUrlPathyUrlCompletaDeNodoyNodosHijo(nodoMapa, nombreNodo, persona);
        }

        if (nodoMapaDestinoId != null && !"".equals(nodoMapaDestinoId))
        {
            NodoMapa nodoMapaDestino = nodoMapaService.getNodoMapa(Long.parseLong(nodoMapaDestinoId), false);

            nodoMapa = nodoMapaService.asignaUltimaPosicionANodo(nodoMapa, nodoMapaDestino);

            nodoMapaService.actualizarNodoPadreyUrlCompletaDeNodoyNodosHijo(nodoMapa, nodoMapaDestino, persona);
        }
    }

    private Boolean isTrue(Boolean condition)
    {
        if (condition != null && condition == true)
        {
            return true;
        }

        return false;
    }

    @DELETE
    @Path("{id}")
    public void deleteNodoMapa(@PathParam("id") String mapaId)
            throws BorradoContenidoNoAutorizadoException, UsuarioNoAutenticadoException,
            NodoConNombreYaExistenteEnMismoPadreException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona();
        persona.setId(userId);

        NodoMapa nodoMapa = nodoMapaService.getNodoMapa(Long.parseLong(mapaId), false);
        nodoMapaService.deleteNodoMapa(nodoMapa, persona);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addNodoMapa(UIEntity entity)
            throws InsertadoContenidoNoAutorizadoException, ParametrosObligatoriosException,
            NodoConNombreYaExistenteEnMismoPadreException
    {
        Long personaId = AccessManager.getConnectedUserId(request);

        if (checkRequiredParametersToNodoMapa(entity.get("franquiciaId"), personaId, entity.get("mapaId")))
        {
            throw new ParametrosObligatoriosException("Franquicia", "Nombre");
        }

        Long idParentNodoMapa = Long.parseLong(entity.get("mapaId"));
        String idFranquiciaAux = entity.get("franquiciaId");
        String idPlantillaAux = entity.get("plantillaId");

        NodoMapa parentNodoMapa = nodoMapaService.getNodoMapa(idParentNodoMapa, false);

        NodoMapa newNodoMapa = entity.toModel(NodoMapa.class);

        newNodoMapa.setMenuHeredado(true);
        newNodoMapa.setMenu(parentNodoMapa.getMenu());

        newNodoMapa.setUrlCompleta(parentNodoMapa.getUrlCompleta() + entity.get("urlPath") + "/");
        newNodoMapa.setUpoMapa(parentNodoMapa);

        Long idFranquicia = Long.parseLong(idFranquiciaAux);
        Franquicia franquicia = franquiciaService.getFranquicia(idFranquicia);
        newNodoMapa.setUpoFranquicia(franquicia);

        if (idPlantillaAux != null && !idPlantillaAux.isEmpty())
        {
            Long idPlantilla = Long.parseLong(idPlantillaAux);
            Plantilla plantilla = plantillaService.getPlantillaById(idPlantilla);
            NodoMapaPlantilla nodoMapaPlantilla = new NodoMapaPlantilla();

            nodoMapaPlantilla.setUpoPlantilla(plantilla);
            nodoMapaPlantilla.setNivel(DEFAULT_NODO_MAPA_NIVEL);
            nodoMapaPlantilla.setUpoMapa(newNodoMapa);

            newNodoMapa.setUpoMapasPlantillas(Collections.singleton(nodoMapaPlantilla));
        }

        Persona persona = new Persona();
        persona.setId(personaId);

        nodoMapaService.insert(persona, newNodoMapa);
        entity.put("id", newNodoMapa.getId());

        return entity;
    }

    private boolean checkRequiredParametersToNodoMapa(String franquiciaId, Long personaResponsable, String mapaId)
    {
        return franquiciaId == null || "".equals(
                franquiciaId) || personaResponsable == null || mapaId == null || "".equals(mapaId);
    }

    @POST
    @Path("{id}/estructurafecha")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage addNodoMapaWithDataStructure(@PathParam("id") String mapaId, @FormParam("mes") String mes,
                                                        @FormParam("anyo") String anyo)
            throws UsuarioNoAutenticadoException, NumberFormatException, NodoConNombreYaExistenteEnMismoPadreException,
            InsertadoContenidoNoAutorizadoException, InsertarEstructuraDeFechaEnNodoMapaException
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona(personaId);

        NodoMapa nodoMapa = nodoMapaService.getNodoMapa(Long.parseLong(mapaId), false);
        estructuraFechaService.insertaConEstructuraDeFecha(persona, nodoMapa, mes, anyo);

        return new ResponseMessage(true);
    }

    @POST
    @Path("{id}/estructurarevista")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage addNodoMapaWithRevistaStructure(@PathParam("id") String mapaId, @FormParam("dia") String dia)
            throws UsuarioNoAutenticadoException, NumberFormatException, NodoConNombreYaExistenteEnMismoPadreException,
            InsertadoContenidoNoAutorizadoException, InsertarEstructuraDeRevistaEnNodoMapaException
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona(personaId);

        ParamUtils.checkNotNull(mapaId, dia);

        NodoMapa nodoMapa = nodoMapaService.getNodoMapa(Long.parseLong(mapaId), false);
        estructuraRevistaService.insertaConEstructuraDeRevista(persona, nodoMapa, dia);

        return new ResponseMessage(true);
    }

    @POST
    @Path("{id}/estructuravoxuji")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage addNodoMapaWithVoxujiStructure(@PathParam("id") String mapaId, @FormParam("dia") String dia)
            throws UsuarioNoAutenticadoException, NumberFormatException, NodoConNombreYaExistenteEnMismoPadreException,
            InsertadoContenidoNoAutorizadoException, InsertarEstructuraDeVoxujiEnNodoMapaException
    {
        Long personaId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona(personaId);

        ParamUtils.checkNotNull(mapaId, dia);

        NodoMapa nodoMapa = nodoMapaService.getNodoMapa(Long.parseLong(mapaId), false);
        estructuraVoxujiService.insertaConEstructuraDeVoxuji(persona, nodoMapa, dia);

        return new ResponseMessage(true);
    }

    @PUT
    @Path("{id}/visibilidad")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void actualizarVisibilidad(@PathParam("id") String id, @FormParam("visible") String visible,
                                      @FormParam("textoNoVisible") String texto, @FormParam("extenderAHijos") String extenderAHijos)
            throws ActualizadoContenidoNoAutorizadoException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona();
        persona.setId(userId);

        NodoMapa nodoMapa = nodoMapaService.getNodoMapa(Long.parseLong(id), false);

        nodoMapaService.setVisibilidad(visible, texto, extenderAHijos, nodoMapa, persona);
    }

    @PUT
    @Path("{id}/bloquear")
    @Consumes(MediaType.APPLICATION_JSON)
    public void alternaBloqueoNodo(@PathParam("id") String id)
            throws NodoConNombreYaExistenteEnMismoPadreException, ActualizadoContenidoNoAutorizadoException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona();
        persona.setId(userId);

        NodoMapa nodoMapa = nodoMapaService.getNodoMapa(Long.parseLong(id), false);

        nodoMapaService.alternaBloqueoNodo(nodoMapa, persona);
    }

    @PUT
    @Path("{id}/reordenar-nodos")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void reordenarNodos(@PathParam("id") String id, @FormParam("nodoMapaOrigenId") String nodoMapaOrigenId,
                               @FormParam("nodoMapaJuntoId") String nodoMapaJuntoId, @FormParam("point") String point)
            throws ReordenadoDeNodosNoAutorizadoException, NumberFormatException

    {
        Long userId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona();
        persona.setId(userId);

        ParamUtils.checkNotNull(id, nodoMapaOrigenId, nodoMapaJuntoId, point);

        NodoMapa nodoMapa = nodoMapaService.getNodoMapa(Long.parseLong(id), false);

        nodoMapaService.reordenarNodos(nodoMapa, Long.parseLong(nodoMapaOrigenId), Long.parseLong(nodoMapaJuntoId),
                point, persona);
    }

    @GET
    @Path("{id}/default-values-new-content")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getDefaultValuesForNewContents(@PathParam("id") String mapaId)
    {
        Long userId = AccessManager.getConnectedUserId(request);
        NodoMapa nodo = nodoMapaService.getNodoMapaByMapaId(Long.parseLong(mapaId)).get(0);

        UIEntity entity = new UIEntity();

        if (nodo.getUpoFranquicia() != null)
        {
            entity.put("otrosAutores", nodo.getUpoFranquicia().getOtrosAutores());
        }

        if (nodo.getUpoFranquicia() != null && nodo.getUpoFranquicia().getOrigenInformacion() != null)
        {
            entity.put("origenId", franquiciaService.getFranquicia(nodo.getUpoFranquicia().getId())
                    .getOrigenInformacion().getId());
        }

        entity.put("personaResponsable", personaService.getPersona(userId).getNombreCompleto());

        return entity;
    }

    @POST
    @Path("{id}/proponer-a-tags")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity proponerANodosConEtiquetas(@PathParam("id") Long mapaId, @FormParam("tags") String tags, @FormParam("incluirHijos") Boolean incluirHijos)
    {
        UIEntity uiEntity = new UIEntity();

        Long userId = AccessManager.getConnectedUserId(request);
        Persona persona = new Persona();
        persona.setId(userId);

        List<String> listaTags = new ArrayList<>();

        if (tags != null && !tags.isEmpty())
        {
            listaTags = Arrays.asList(tags.split("\\,"));
            listaTags = listaTags.stream().map(tag -> StringUtils.limpiaAcentos(tag.toLowerCase().trim()))
                    .collect(Collectors.toList());
        }

        NodoMapa nodoMapaAProponer = nodoMapaService.getNodoMapa(mapaId, true);
        PropuestaANodosPorTagsRespuesta respuesta = nodoMapaService.proponerANodosConEtiquetas(nodoMapaAProponer, listaTags, incluirHijos, persona);

        uiEntity.put("urlsCompletasConExito", respuesta.getUrlCompletasConExito());
        List<UIEntity> errorEntities = respuesta.getGetUrlCompletasConError().stream().map(UIEntity::toUI).collect(Collectors.toList());

        uiEntity.put("urlsCompletasConError", errorEntities);

        return uiEntity;
    }
}
