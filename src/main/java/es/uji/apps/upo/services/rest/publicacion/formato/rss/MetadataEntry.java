package es.uji.apps.upo.services.rest.publicacion.formato.rss;

import com.sun.syndication.feed.module.Module;
import org.jdom.Namespace;

import java.util.List;
import java.util.Map;

public interface MetadataEntry extends Module
{
    public static final Namespace namespace_general = Namespace.getNamespace(MetadataEntryImpl.PREFIX_UPO_GENERAL,
            MetadataEntryImpl.URI);

    public static final Namespace namespace_metadato = Namespace.getNamespace(MetadataEntryImpl.PREFIX_UPO_METADATO,
            MetadataEntryImpl.URI);

    public static final String URI = "http://www.uji.es/namespaces/rss#";
    public static final String PREFIX_UPO_GENERAL = "upo-general";
    public static final String PREFIX_UPO_METADATO = "upo-metadato";

    Map<String, ValorAtributo> getFields();

    void setFields(Map<String, ValorAtributo> fields);

    void addField(String name, ValorAtributo valorAtributo);

    Namespace getNamespace(String namespace);
}
