package es.uji.apps.upo.services.rest.publicacion.formato.search;

public class FiltroIgualCadena extends Filtro<String>
{
    public FiltroIgualCadena(String clave, String valor)
    {
        super(clave, valor);
    }

    @Override
    public String generate()
    {
        return "(" + clave + " = '" + valor + "')";
    }
}