package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class ActualizadoContenidoNoAutorizadoException extends GeneralUPODataBaseException
{
    public ActualizadoContenidoNoAutorizadoException()
    {
        super("No estás autoritzat per a actualitzar aquest contingut");
    }

    public ActualizadoContenidoNoAutorizadoException(String message)
    {
        super(message);
    }
}
