package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class NodoNoSePuedeDuplicarException extends GeneralUPODataBaseException
{

    public NodoNoSePuedeDuplicarException()
    {
        super("Aquest node no es pot duplicar, heu de ser administrador del seu node pare.");
    }
    
    public NodoNoSePuedeDuplicarException(String message)
    {
        super(message);
    }

}
