package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class NodoNoSePuedeRenombrarException extends GeneralUPODataBaseException
{

    public NodoNoSePuedeRenombrarException()
    {
        super("Aquest node no es pot renombrar");
    }
    
    public NodoNoSePuedeRenombrarException(String message)
    {
        super(message);
    }

}
