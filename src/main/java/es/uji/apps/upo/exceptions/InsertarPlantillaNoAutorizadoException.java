package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class InsertarPlantillaNoAutorizadoException extends GeneralUPODataBaseException
{

    public InsertarPlantillaNoAutorizadoException()
    {
        super("Només l'Administrador pot afegir plantilles");
    }
    
    public InsertarPlantillaNoAutorizadoException(String message)
    {
        super(message);
    }

}
