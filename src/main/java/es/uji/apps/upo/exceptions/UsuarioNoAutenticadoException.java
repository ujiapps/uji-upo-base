package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class UsuarioNoAutenticadoException extends GeneralUPOException
{
    public UsuarioNoAutenticadoException()
    {
        super("Usuari No autenticat");
    }
    
    public UsuarioNoAutenticadoException(String message)
    {
        super(message);
    }
    
}
