package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class BuscadoFicherosEnElReservoriException extends GeneralUPODataBaseException
{

    public BuscadoFicherosEnElReservoriException()
    {
        super("Errada al cercar fitxers en el repositori");
    }
    
    public BuscadoFicherosEnElReservoriException(String message)
    {
        super(message);
    }

}
