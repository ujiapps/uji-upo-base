package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class LongitudMaximaExcedidaEnMetadatosException extends GeneralUPODataBaseException
{
    private static final String message = "Longitud màxima excedida en les metadades";

    public LongitudMaximaExcedidaEnMetadatosException()
    {
        super(message);
    }

    public LongitudMaximaExcedidaEnMetadatosException(String message)
    {
        super(message);
    }

    public LongitudMaximaExcedidaEnMetadatosException(Throwable e)
    {
        super(message, e);
    }
}
