package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class ActualizadoContenidoException extends GeneralUPODataBaseException
{
    private static final String message = "Errada actualitzant un contingut";

    public ActualizadoContenidoException()
    {
        super(message);
    }

    public ActualizadoContenidoException(String message)
    {
        super(message);
    }

    public ActualizadoContenidoException(Throwable e)
    {
        super(message, e);
    }
}
