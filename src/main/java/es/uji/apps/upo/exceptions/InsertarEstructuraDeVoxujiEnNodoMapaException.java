package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class InsertarEstructuraDeVoxujiEnNodoMapaException extends GeneralUPODataBaseException
{

    public InsertarEstructuraDeVoxujiEnNodoMapaException()
    {
        super("Hi ha una errada en la estructura de revista");
    }

    public InsertarEstructuraDeVoxujiEnNodoMapaException(String message)
    {
        super(message);
    }

}
