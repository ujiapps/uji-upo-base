package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class InglesIdiomaObligatorioException extends IdiomaObligatorioException
{
    public InglesIdiomaObligatorioException()
    {
        super("L'Anglès és un idioma obligatori");
    }
    
    public InglesIdiomaObligatorioException(String message)
    {
        super(message);
    }

}
