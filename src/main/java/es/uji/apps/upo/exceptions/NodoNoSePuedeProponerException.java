package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class NodoNoSePuedeProponerException extends GeneralUPODataBaseException
{

    public NodoNoSePuedeProponerException()
    {
        super("Aquest node no es pot proposar o bé no es pot deixar en aquest altre");
    }
    
    public NodoNoSePuedeProponerException(String message)
    {
        super(message);
    }

}
