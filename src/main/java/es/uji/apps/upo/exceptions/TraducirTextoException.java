package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class TraducirTextoException extends GeneralUPOException
{
    public TraducirTextoException()
    {
        super("Errada intentant traduïr el texte. Torneu-ho a intentar més tard.");
    }

    public TraducirTextoException(String message)
    {
        super(message);
    }
    
}
