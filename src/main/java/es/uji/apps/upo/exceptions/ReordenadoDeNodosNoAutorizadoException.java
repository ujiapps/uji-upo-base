package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class ReordenadoDeNodosNoAutorizadoException extends GeneralUPODataBaseException
{

    public ReordenadoDeNodosNoAutorizadoException()
    {
        super("Només l'Administrador del node pare pot reordenadar els nodes fills");
    }
    
    public ReordenadoDeNodosNoAutorizadoException(String message)
    {
        super(message);
    }

}
