package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class EspanolIdiomaObligatorioException extends IdiomaObligatorioException
{
    public EspanolIdiomaObligatorioException()
    {
        super("L'Espanyol és un idioma obligatori");
    }
    
    public EspanolIdiomaObligatorioException(String message)
    {
        super(message);
    }
}
