package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class FechaFinPrioridadSuperiorAFechaInicioException extends GeneralUPOException
{
    public FechaFinPrioridadSuperiorAFechaInicioException()
    {
        super("La data fi d'una prioritat ha de ser superior a la data d'inici");
    }
    
    public FechaFinPrioridadSuperiorAFechaInicioException(String message)
    {
        super(message);
    }

}
