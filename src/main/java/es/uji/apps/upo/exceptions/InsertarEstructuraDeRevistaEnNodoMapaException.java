package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class InsertarEstructuraDeRevistaEnNodoMapaException extends GeneralUPODataBaseException
{

    public InsertarEstructuraDeRevistaEnNodoMapaException()
    {
        super("Hi ha una errada en la estructura de revista");
    }

    public InsertarEstructuraDeRevistaEnNodoMapaException(String message)
    {
        super(message);
    }

}
