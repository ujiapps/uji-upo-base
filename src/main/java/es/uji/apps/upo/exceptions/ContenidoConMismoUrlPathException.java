package es.uji.apps.upo.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class ContenidoConMismoUrlPathException extends CoreDataBaseException
{

    public ContenidoConMismoUrlPathException()
    {
        super("Existe un contenido en el mismo nodo con la misma url.");
    }

    public ContenidoConMismoUrlPathException(String message)
    {
        super(message);
    }

}
