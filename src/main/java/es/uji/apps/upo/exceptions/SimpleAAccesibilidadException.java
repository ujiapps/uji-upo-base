package es.uji.apps.upo.exceptions;

import es.uji.apps.upo.exceptions.AccesibilidadException;

@SuppressWarnings("serial")
public class SimpleAAccesibilidadException extends AccesibilidadException
{

    public SimpleAAccesibilidadException()
    {
        super("Contingut no accesible: nivell A");
    }

    public SimpleAAccesibilidadException(String message)
    {
        super(message);
    }

}
