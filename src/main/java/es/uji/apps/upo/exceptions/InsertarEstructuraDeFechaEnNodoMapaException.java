package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class InsertarEstructuraDeFechaEnNodoMapaException extends GeneralUPODataBaseException
{

    public InsertarEstructuraDeFechaEnNodoMapaException()
    {
        super("Hi ha una errada en la estructura de data");
    }
    
    public InsertarEstructuraDeFechaEnNodoMapaException(String message)
    {
        super(message);
    }

}
