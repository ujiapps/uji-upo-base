package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class InsertarModeracionLotesNoAutorizadoException extends GeneralUPODataBaseException
{

    public InsertarModeracionLotesNoAutorizadoException()
    {
        super("Només l'Administrador d'un node pot sol·licitar una migració de continguts");
    }
    
    public InsertarModeracionLotesNoAutorizadoException(String message)
    {
        super(message);
    }

}
