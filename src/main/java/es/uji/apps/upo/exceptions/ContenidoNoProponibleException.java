package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class ContenidoNoProponibleException extends GeneralUPOException
{
    public ContenidoNoProponibleException()
    {
        super("Només els continguts originals es podem proposar.");
    }
    
    public ContenidoNoProponibleException(String message)
    {
        super(message);
    }

}
