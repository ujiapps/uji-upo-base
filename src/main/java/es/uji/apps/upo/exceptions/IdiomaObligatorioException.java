package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class IdiomaObligatorioException extends GeneralUPOException
{
    public IdiomaObligatorioException()
    {
        super("Hi ha varios idiomes obligatoris per omplir");
    }
    
    public IdiomaObligatorioException(String message)
    {
        super(message);
    }

}
