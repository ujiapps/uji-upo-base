package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class TipoContenidoNoAdecuadoException extends GeneralUPOException
{
    public TipoContenidoNoAdecuadoException()
    {
        super("El tipo del contenido no es adecuado para esta operación.");
    }

    public TipoContenidoNoAdecuadoException(String message)
    {
        super(message);
    }

}
