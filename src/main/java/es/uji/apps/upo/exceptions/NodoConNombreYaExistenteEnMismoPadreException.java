package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class NodoConNombreYaExistenteEnMismoPadreException extends GeneralUPODataBaseException
{

    public NodoConNombreYaExistenteEnMismoPadreException()
    {
        super("Operació no permesa. Ja existeix un node amb aquest nom.");
    }

    public NodoConNombreYaExistenteEnMismoPadreException(String message)
    {
        super(message);
    }

}
