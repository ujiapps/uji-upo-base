package es.uji.apps.upo.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class GeneralUPODataBaseException extends CoreDataBaseException
{
    public GeneralUPODataBaseException()
    {
        super("S'ha produït un error a la base de dades.");
    }
    
    public GeneralUPODataBaseException(String message)
    {
        super(message);
    }

    public GeneralUPODataBaseException(String message, Throwable e)
    {
        super(message, e);
    }

}
