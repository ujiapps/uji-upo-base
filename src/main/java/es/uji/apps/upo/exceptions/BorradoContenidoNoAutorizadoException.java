package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class BorradoContenidoNoAutorizadoException extends GeneralUPODataBaseException
{

    public BorradoContenidoNoAutorizadoException()
    {
        super("No esteu autoritzat per a esborrar aquest contingut");
    }
    
    public BorradoContenidoNoAutorizadoException(String message)
    {
        super(message);
    }

}
