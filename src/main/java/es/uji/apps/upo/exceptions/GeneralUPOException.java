package es.uji.apps.upo.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

@SuppressWarnings("serial")
public class GeneralUPOException extends CoreBaseException
{
    public GeneralUPOException()
    {
        super("S'ha produït un error en l'operació");
    }
    
    public GeneralUPOException(String message)
    {
        super(message);
    }

    public GeneralUPOException(String message, Throwable e)
    {
        super(message, e);
    }
}