package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class AccesoNoAutorizadoException extends GeneralUPOException
{
    public AccesoNoAutorizadoException()
    {
        super("S'ha d'estar autenticat");
    }

    public AccesoNoAutorizadoException(String message)
    {
        super(message);
    }

}
