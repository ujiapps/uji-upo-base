package es.uji.apps.upo.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class ContenidoYaPropuestoException extends CoreDataBaseException
{

    public ContenidoYaPropuestoException()
    {
        super("El contenido ya ha sido propuesto.");
    }

    public ContenidoYaPropuestoException(String message)
    {
        super(message);
    }

}
