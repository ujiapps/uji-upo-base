package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class StorageException extends GeneralUPOException
{
    public StorageException()
    {
        super("Problema accediendo al repositorio");
    }
    
    public StorageException(String message)
    {
        super(message);
    }
    
}
