package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class CrearNuevoContenidoException extends GeneralUPODataBaseException
{
    private static final String message = "Errada al crear un contingut nou";

    public CrearNuevoContenidoException()
    {
        super(message);
    }

    public CrearNuevoContenidoException(String message)
    {
        super(message);
    }

    public CrearNuevoContenidoException(Throwable e)
    {
        super(message, e);
    }
}
