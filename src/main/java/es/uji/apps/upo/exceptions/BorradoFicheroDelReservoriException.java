package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class BorradoFicheroDelReservoriException extends GeneralUPODataBaseException
{

    public BorradoFicheroDelReservoriException()
    {
        super("Errada al esborrar un fitxer del repositori");
    }
    
    public BorradoFicheroDelReservoriException(String message)
    {
        super(message);
    }

}
