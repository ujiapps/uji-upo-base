package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class CatalanIdiomaObligatorioException extends IdiomaObligatorioException
{
    public CatalanIdiomaObligatorioException()
    {
        super("El Català és un idioma obligatori");
    }
    
    public CatalanIdiomaObligatorioException(String message)
    {
        super(message);
    }

}
