package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class InsertadoContenidoNoAutorizadoException extends GeneralUPODataBaseException
{
    public InsertadoContenidoNoAutorizadoException()
    {
        super("No estàs autoritzat per a insertar aquest contingut");
    }
    
    public InsertadoContenidoNoAutorizadoException(String message)
    {
        super(message);
    }
}
