package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class EstructuraDeLosParametrosException extends GeneralUPOException
{
    public EstructuraDeLosParametrosException()
    {
        super("Errada en la estructura dels paràmetres: codi:nom:tipus:grup:discriminador");
    }

    public EstructuraDeLosParametrosException(String message)
    {
        super(message);
    }

}
