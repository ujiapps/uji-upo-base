package es.uji.apps.upo.exceptions;

import es.uji.apps.upo.exceptions.AccesibilidadException;

@SuppressWarnings("serial")
public class TripleAAccesibilidadException extends AccesibilidadException
{

    public TripleAAccesibilidadException()
    {
        super("Contingut no accesible: nivell AAA");
    }

    public TripleAAccesibilidadException(String message)
    {
        super(message);
    }

}
