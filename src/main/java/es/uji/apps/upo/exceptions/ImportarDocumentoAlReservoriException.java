package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class ImportarDocumentoAlReservoriException extends GeneralUPODataBaseException
{

    public ImportarDocumentoAlReservoriException()
    {
        super("Errada al importar un document al reservori");
    }
    
    public ImportarDocumentoAlReservoriException(String message)
    {
        super(message);
    }

}
