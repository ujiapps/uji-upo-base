package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class SeIndicaUnaHoraSinFechaAsociadaException extends GeneralUPOException
{
    public SeIndicaUnaHoraSinFechaAsociadaException()
    {
        super("No es pot indicar una hora si no s'indica la data associada");
    }
    
    public SeIndicaUnaHoraSinFechaAsociadaException(String message)
    {
        super(message);
    }

}
