package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class NodoNoSePuedeArrastrarException extends GeneralUPODataBaseException
{

    public NodoNoSePuedeArrastrarException()
    {
        super("Aquest node no es pot arrastrar o bé no es pot deixar en aquest altre");
    }
    
    public NodoNoSePuedeArrastrarException(String message)
    {
        super(message);
    }

}
