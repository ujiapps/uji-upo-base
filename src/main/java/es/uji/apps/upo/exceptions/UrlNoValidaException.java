package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class UrlNoValidaException extends GeneralUPOException
{
    public UrlNoValidaException()
    {
        super("Aquest URL no és correcta");
    }
    
    public UrlNoValidaException(String message)
    {
        super(message);
    }
    
}
