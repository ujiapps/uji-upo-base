package es.uji.apps.upo.exceptions;

import es.uji.apps.upo.exceptions.AccesibilidadException;

@SuppressWarnings("serial")
public class DobleAAccesibilidadException extends AccesibilidadException
{

    public DobleAAccesibilidadException()
    {
        super("Contingut no accesible: nivell AA");
    }

    public DobleAAccesibilidadException(String message)
    {
        super(message);
    }

}
