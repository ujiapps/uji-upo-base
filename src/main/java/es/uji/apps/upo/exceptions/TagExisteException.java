package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class TagExisteException extends GeneralUPOException {
    public TagExisteException() {
        super("Aquesta etiqueta ja existeix");
    }

    public TagExisteException(String message) {
        super(message);
    }

}
