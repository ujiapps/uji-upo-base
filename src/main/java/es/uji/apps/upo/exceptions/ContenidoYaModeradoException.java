package es.uji.apps.upo.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class ContenidoYaModeradoException extends CoreDataBaseException
{

    public ContenidoYaModeradoException()
    {
        super("El contenido ya ha sido moderado.");
    }

    public ContenidoYaModeradoException(String message)
    {
        super(message);
    }

}
