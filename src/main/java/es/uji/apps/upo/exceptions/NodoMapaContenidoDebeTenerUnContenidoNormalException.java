package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class NodoMapaContenidoDebeTenerUnContenidoNormalException extends GeneralUPOException
{
    public NodoMapaContenidoDebeTenerUnContenidoNormalException()
    {
        super("No es permet que un contingut tinga més d'un tipus normal");
    }
    
    public NodoMapaContenidoDebeTenerUnContenidoNormalException(String message)
    {
        super(message);
    }

}
