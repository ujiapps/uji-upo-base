package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class InsertadoContenidoException extends GeneralUPODataBaseException
{
    private static final String message = "Errada inserint un contingut";

    public InsertadoContenidoException()
    {
        super(message);
    }

    public InsertadoContenidoException(String message)
    {
        super(message);
    }

    public InsertadoContenidoException(Throwable e)
    {
        super(message, e);
    }
}
