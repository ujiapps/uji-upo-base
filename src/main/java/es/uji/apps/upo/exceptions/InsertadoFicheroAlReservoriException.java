package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class InsertadoFicheroAlReservoriException extends GeneralUPODataBaseException
{

    public InsertadoFicheroAlReservoriException()
    {
        super("Errada al inserir el fitxer en el repositori");
    }
    
    public InsertadoFicheroAlReservoriException(String message)
    {
        super(message);
    }

}
