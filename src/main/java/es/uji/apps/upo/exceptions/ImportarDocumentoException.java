package es.uji.apps.upo.exceptions;


@SuppressWarnings("serial")
public class ImportarDocumentoException extends GeneralUPODataBaseException
{

    public ImportarDocumentoException()
    {
        super("Errada al importar un document");
    }
    
    public ImportarDocumentoException(String message)
    {
        super(message);
    }

}
