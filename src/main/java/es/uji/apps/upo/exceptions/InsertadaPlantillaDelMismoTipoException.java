package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class InsertadaPlantillaDelMismoTipoException extends GeneralUPODataBaseException {

    public InsertadaPlantillaDelMismoTipoException()
    {
        super("No se pueden insertar varias plantillas del mismo tipo");
    }

    public InsertadaPlantillaDelMismoTipoException(String message)
    {
        super(message);
    }
}