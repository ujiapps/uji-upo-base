package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class PrioridadSinNivelDePrioridadException extends GeneralUPOException
{
    public PrioridadSinNivelDePrioridadException()
    {
        super("S'està indicant una prioritat sense nivell de prioritat");
    }

    public PrioridadSinNivelDePrioridadException(String message)
    {
        super(message);
    }

}
