package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class EdicionNoAutorizadaException extends GeneralUPOException
{
    public EdicionNoAutorizadaException()
    {
        super("Edición no autorizada");
    }

    public EdicionNoAutorizadaException(String message)
    {
        super(message);
    }
    
}
