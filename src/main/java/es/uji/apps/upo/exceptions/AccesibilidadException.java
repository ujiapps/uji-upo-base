package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class AccesibilidadException extends GeneralUPOException
{
    public AccesibilidadException()
    {
        super("Contingut no accesible");
    }
    
    public AccesibilidadException(String message)
    {
        super(message);
    }
    
}
