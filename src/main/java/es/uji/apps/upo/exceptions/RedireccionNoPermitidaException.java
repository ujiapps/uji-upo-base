package es.uji.apps.upo.exceptions;

@SuppressWarnings("serial")
public class RedireccionNoPermitidaException extends GeneralUPOException
{
    public RedireccionNoPermitidaException()
    {
        super("Redirección no permitida");
    }

    public RedireccionNoPermitidaException(String message)
    {
        super(message);
    }
    
}
