package es.uji.apps.upo.accessibility;

public interface AccesibleTemplate
{
    public String getHeader();

    public String getFooter();
}
