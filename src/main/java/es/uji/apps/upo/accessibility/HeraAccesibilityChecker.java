package es.uji.apps.upo.accessibility;

import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.commons.rest.StreamUtils;

@Component
public class HeraAccesibilityChecker implements AccesibilityChecker
{
    private AccesibleTemplate accesibleTemplate;
    public String accesibilityServiceUrl;
    private int[] result;

    @Autowired
    public HeraAccesibilityChecker(String accesibilityServiceUrl, AccesibleTemplate accesibleTemplate)
    {
        this.accesibilityServiceUrl = accesibilityServiceUrl;
        this.accesibleTemplate = accesibleTemplate;
    }

    private int[] check(byte[] source)
    {
        try
        {
            String params = buildAccesiblePageFromContent(source);

            URL url = new URL(accesibilityServiceUrl);

            URLConnection urlConnection = url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection
                    .addRequestProperty(
                            "User-Agent",
                            "Mozilla/5.0 (X11; U; Linux x86_64; cs-CZ; rv:1.9.1.7) Gecko/20100106 Ubuntu/9.10 (karmic) Firefox/3.5.7");

            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
                    urlConnection.getOutputStream());
            outputStreamWriter.write("source=" + URLEncoder.encode(params, "UTF-8"));
            outputStreamWriter.flush();

            String data = new String(StreamUtils.inputStreamToByteArray(urlConnection.getInputStream()));

            JSONObject result = new JSONObject(data);
            JSONObject resumen = result.getJSONObject("resumen");

            return new int[] { resumen.getJSONObject("a").getInt("mal"),
                    resumen.getJSONObject("aa").getInt("mal"),
                    resumen.getJSONObject("aaa").getInt("mal") };
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return new int[] { -1, -1, -1 };
        }
    }

    @Override
    public String buildAccesiblePageFromContent(byte[] source) throws UnsupportedEncodingException
    {
        return buildAccesiblePageFromContent(new String(source));
    }

    @Override
    public String buildAccesiblePageFromContent(String source) throws UnsupportedEncodingException
    {
        if (source == null)
        {
            return "";
        }

        StringBuilder params = new StringBuilder();
        params.append(accesibleTemplate.getHeader());
        params.append(source);
        params.append(accesibleTemplate.getFooter());

        return params.toString();
    }

    @Override
    public boolean checkValidA(byte[] source)
    {
        this.result = check(source);

        return (this.result[0] == 0);
    }

    @Override
    public boolean checkValidAA(byte[] source)
    {
        this.result = check(source);

        return (result[0] == 0 && result[1] == 0);
    }

    @Override
    public boolean checkValidAAA(byte[] source)
    {
        this.result = check(source);

        return (result[0] == 0 && result[1] == 0 && result[2] == 0);
    }

    @Override
    public int getNumberOfLevelAErrors()
    {
        return this.result[0];
    }

    @Override
    public int getNumberOfLevelAAErrors()
    {
        return this.result[1];
    }

    @Override
    public int getNumberOfLevelAAAErrors()
    {
        return this.result[2];
    }
}