package es.uji.apps.upo.accessibility;

import org.springframework.stereotype.Component;

@Component
public class AccesibleHtml5Template implements AccesibleTemplate
{
    public String getHeader()
    {
        StringBuilder result = new StringBuilder();
        result.append("<!DOCTYPE html>\n");
        result.append("<html lang=\"ca\">");
        result.append("  <head>");
        result.append("    <meta charset=\"utf-8\">");
        result.append("    <title>Portal UJI</title>");
        result.append("    <style></style>");
        result.append("  </head>");
        result.append("  <body>");
        result.append("    <h1><a href=\"#\" accesskey=\"1\">logo</a></h1>");
        result.append("    <h2>Título página</h2>");

        return result.toString();
    }

    public String getFooter()
    {
        StringBuilder result = new StringBuilder();
        result.append("  </body>");
        result.append("</html>");

        return result.toString();
    }
}
