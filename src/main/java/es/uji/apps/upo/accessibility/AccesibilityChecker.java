package es.uji.apps.upo.accessibility;

import java.io.UnsupportedEncodingException;

public interface AccesibilityChecker
{
    boolean checkValidA(byte[] source);

    boolean checkValidAA(byte[] source);

    boolean checkValidAAA(byte[] source);

    int getNumberOfLevelAErrors();

    int getNumberOfLevelAAErrors();

    int getNumberOfLevelAAAErrors();
    
    String buildAccesiblePageFromContent(byte[] source) throws UnsupportedEncodingException;
    
    String buildAccesiblePageFromContent(String source) throws UnsupportedEncodingException;
}