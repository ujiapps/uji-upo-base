package es.uji.apps.upo.accessibility;

public class AccesibleHtml4Template implements AccesibleTemplate
{
    public String getHeader()
    {
        StringBuilder result = new StringBuilder();
        result.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n");
        result.append("<html lang=\"ca\">");
        result.append("  <head>");
        result.append("    <meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\">");
        result.append("    <title>Portal UJI</title>");
        result.append("    <link type=\"text/css\" href=\"/upo/css/style.css\" rel=\"stylesheet\">");
        result.append("  </head>");
        result.append("  <body>");
        result.append("    <h1><a href=\"#\" accesskey=\"1\">logo</a></h1>");
        result.append("    <h2>Título página</h2>");

        return result.toString();
    }

    public String getFooter()
    {
        StringBuilder result = new StringBuilder();
        result.append("  </body>");
        result.append("</html>");

        return result.toString();
    }
}
