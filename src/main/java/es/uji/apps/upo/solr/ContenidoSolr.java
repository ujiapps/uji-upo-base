package es.uji.apps.upo.solr;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.solr.client.solrj.beans.Field;

import es.uji.apps.upo.model.enums.IdiomaPublicacion;

@XmlRootElement
public class ContenidoSolr
{
    @Field
    private String id;

    @Field
    private String url;

    @Field("url_completa")
    private String urlCompleta;

    @Field
    private String titulo;

    private IdiomaPublicacion idioma;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getUrlCompleta()
    {
        return urlCompleta;
    }

    public void setUrlCompleta(String urlCompleta)
    {
        this.urlCompleta = urlCompleta;
    }

    public IdiomaPublicacion getIdioma()
    {
        return idioma;
    }

    public void setIdioma(IdiomaPublicacion idioma)
    {
        this.idioma = idioma;
    }
}