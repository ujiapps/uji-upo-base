package es.uji.apps.upo.solr;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SolrIndexer
{
    public static Logger log = Logger.getLogger(SolrIndexer.class);

    @Autowired
    private SolrServer server;

    public void add(Object bean)
    {
        add(bean, true);
    }

    public void add(Object bean, boolean commit)
    {
        log.debug("Indexing bean " + bean.getClass().getName());

        try
        {
            server.addBean(bean);
            server.commit();
        }
        catch (Exception e)
        {
            log.error(e);
        }
    }

    public void deleteById(String id)
    {
        try
        {
            server.deleteById(id);
            server.commit();
        }
        catch (Exception e)
        {
            log.error(e);
        }
    }

    public <T> T get(Class<T> type, String id)
    {
        SolrQuery query = new SolrQuery();
        query.setQuery("id:" + id);

        try
        {
            QueryResponse response = server.query(query);
            List<T> result = response.getBeans(type);

            if (result != null && result.size() > 0)
            {
                return result.get(0);
            }
        }
        catch (Exception e)
        {
            log.error(e);
        }

        return null;
    }

    public <T> List<T> getList(Class<T> type, String queryText)
    {
        SolrQuery query = new SolrQuery();
        query.setQuery(queryText);

        List<T> result = new ArrayList<T>();

        try
        {
            QueryResponse response = server.query(query);
            result = response.getBeans(type);
        }
        catch (Exception e)
        {
            log.error(e);
        }

        return result;
    }
}