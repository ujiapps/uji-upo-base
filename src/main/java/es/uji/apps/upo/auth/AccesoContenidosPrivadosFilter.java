package es.uji.apps.upo.auth;

import es.uji.apps.upo.services.nodomapa.NodoMapaService;
import es.uji.apps.upo.services.rest.publicacion.UrlNormalizer;
import es.uji.commons.sso.RequestPatternExcluder;
import es.uji.commons.sso.providers.LogoutSessionProvider;
import es.uji.si.SSO.filter.LsmFilter;

import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Service
public class AccesoContenidosPrivadosFilter extends LsmFilter
{
    private FilterConfig filterConfig = null;

    private UrlNormalizer urlNormalizer;
    private NodoMapaService nodoMapaService;

    private RequestPatternExcluder excluder;

    public AccesoContenidosPrivadosFilter()
    {
        super();
    }

    public void init(FilterConfig filterConfig)
            throws ServletException
    {
        this.filterConfig = filterConfig;

        WebApplicationContext springContext =
                WebApplicationContextUtils.getWebApplicationContext(filterConfig.getServletContext());
        urlNormalizer = springContext.getBean(UrlNormalizer.class);
        nodoMapaService = springContext.getBean(NodoMapaService.class);

        String excludePattern = this.filterConfig.getInitParameter("exclude");
        excluder = new RequestPatternExcluder(excludePattern);

        super.init(filterConfig);
    }

    public void destroy()
    {
        this.filterConfig = null;
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException
    {
        HttpServletRequest clientRequest = (HttpServletRequest) request;
        HttpServletResponse clientResponse = (HttpServletResponse) response;

        String urlPublicacion = urlNormalizer.normalize(clientRequest.getParameter("url"));
        String endPointUrl = clientRequest.getRequestURL().toString();

        if (endPointUrl.contains("logout"))
        {
            LogoutSessionProvider logoutSessionProvider = new LogoutSessionProvider();
            logoutSessionProvider.check(clientRequest, clientResponse);
            return;
        }

        if (isUrlExcluded(endPointUrl))
        {
            chain.doFilter(request, response);
            return;
        }

        if (!esPrivada(urlPublicacion, endPointUrl))
        {
            chain.doFilter(request, response);
            return;
        }

        clientResponse.addHeader("ETag", UUID.randomUUID().toString());
        super.doFilter(request, response, chain);
    }

    private boolean esPrivada(String url, String endPointUrl)
    {
        if (!esUrlDePublicacion(endPointUrl))
        {
            return true;
        }

        if (url == null)
        {
            return true;
        }

        List<String> urlsPrivadas = nodoMapaService.getUrlsPrivadas();

        for (String urlPrivada : urlsPrivadas)
        {
            if (url.startsWith(urlPrivada))
            {
                return true;
            }
        }

        return false;
    }

    private boolean esUrlDePublicacion(String endPointUrl)
    {
        return endPointUrl.contains("/upo/rest/publicacion");
    }

    private boolean isUrlExcluded(String url)
    {
        return excluder.isExcluded(url);
    }
}
