package es.uji.apps.upo.auth;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class SSOCookie
{
    private HttpServletRequest request;
    private String cookieName;

    public SSOCookie(HttpServletRequest request, String cookieName)
    {
        this.request = request;
        this.cookieName = cookieName;
    }

    public String getSessionValue()
    {
        Cookie[] cookies = request.getCookies();

        if (cookies != null)
        {
            for (Cookie cookie : cookies)
            {
                if (cookie.getName() != null && cookie.getName().equals(cookieName))
                {
                    return cookie.getValue();
                }
            }
        }

        return null;
    }
}