package es.uji.apps.upo.storage.alfresco;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

//@Component
public class AlfrescoCredentials
{
    private String server;
    private String url;
    private String userName;
    private String password;
    private String workingPath;

    @Autowired
    public AlfrescoCredentials(@Value("${uji.alfresco.server}") String server,
            @Value("${uji.alfresco.url}") String url,
            @Value("${uji.alfresco.username}") String userName,
            @Value("${uji.alfresco.password}") String password,
            @Value("${uji.alfresco.workingPath}") String workingPath)
    {
        this.server = server;
        this.url = url;
        this.userName = userName;
        this.password = password;
        this.workingPath = workingPath;
    }

    public String getServer()
    {
        return server;
    }

    public void setServer(String server)
    {
        this.server = server;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getWorkingPath()
    {
        return workingPath;
    }

    public void setWorkingPath(String workingPath)
    {
        this.workingPath = workingPath;
    }
}