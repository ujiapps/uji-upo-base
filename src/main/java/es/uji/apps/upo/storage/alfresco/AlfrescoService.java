package es.uji.apps.upo.storage.alfresco;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

//@Component
public class AlfrescoService
{
    protected AlfrescoCredentials credentials;
    protected WebResource service;

    @Autowired
    public AlfrescoService(AlfrescoCredentials credentials)
    {
        this.credentials = credentials;
        
        initHttpService();
    }

    protected void initHttpService()
    {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);

        String server = credentials.getServer();

        if (server.startsWith("//")) server = "https:" + server;

        service = client.resource(server + "/alfresco/service/api");
    }
}