package es.uji.apps.upo.storage.alfresco;

import java.io.InputStream;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.cmis.client.AlfrescoDocument;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.OperationContext;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.OperationContextImpl;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.Ace;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.AclPropagation;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;
import org.apache.lucene.queryParser.QueryParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import es.uji.apps.upo.exceptions.StorageException;
import es.uji.apps.upo.storage.StorageClient;
import es.uji.apps.upo.utils.Hasher;

import javax.print.Doc;

//@Component
public class AlfrescoStorageClient implements StorageClient
{
    public static final String CMIS_DOCUMENT = "cmis:document";
    public static final String UJI_DOCUMENT = "D:uji:doc";

    private Session session;
    private Folder workingFolder;
    private OperationContext context;

    private final String workingPath;

    private final AlfrescoThumbailService thumbailService;

    @Autowired
    public AlfrescoStorageClient(AlfrescoCredentials credentials, AlfrescoThumbailService thumbailService,
            @Value("${uji.alfresco.workingPath}") String workingPath)
    {
        this.thumbailService = thumbailService;
        this.workingPath = workingPath;

        Map<String, String> parameter = new HashMap<String, String>();
        parameter.put(SessionParameter.USER, credentials.getUserName());
        parameter.put(SessionParameter.PASSWORD, credentials.getPassword());
        parameter.put(SessionParameter.ATOMPUB_URL, credentials.getUrl());
        parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
        parameter.put(SessionParameter.OBJECT_FACTORY_CLASS, "org.alfresco.cmis.client.impl.AlfrescoObjectFactoryImpl");

        SessionFactory factory = SessionFactoryImpl.newInstance();
        session = factory.getRepositories(parameter).get(0).createSession();
        workingFolder = (Folder) session.getObjectByPath(workingPath);

        context = session.createOperationContext();
        context.setRenditionFilterString("cmis:thumbnail");
    }

    public Document add(String fileName, String contentType, int contentLength, InputStream content, String urlNode)
            throws StorageException
    {
        return add(fileName, contentType, contentLength, content, urlNode, null, "");
    }

    public Document add(String fileName, String contentType, int contentLength, InputStream content, String urlNode,
            String urlOriginal, String hash)
            throws StorageException
    {
        Map<String, Object> properties = new HashMap<String, Object>();

        properties.put(PropertyIds.NAME, urlNode.replace("/", "_") + hash + fileName);
        properties.put(PropertyIds.OBJECT_TYPE_ID, UJI_DOCUMENT);
        properties.put("uji:urlNode", urlNode);
        properties.put("uji:name", fileName);
        properties.put("uji:urlOriginal", urlOriginal);

        ContentStream contentStream =
                new ContentStreamImpl(fileName, BigInteger.valueOf(contentLength), contentType, content);

        AlfrescoDocument document =
                (AlfrescoDocument) workingFolder.createDocument(properties, contentStream, VersioningState.MAJOR);

        document = addAspects(document);
        document = addPublicAccessACLToDocument(document);

        thumbailService.build(document);

        return document;
    }

    public Document replace(Document document, String fileName, String contentType, int contentLength,
            InputStream content, String urlNode)
            throws StorageException
    {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(PropertyIds.NAME, urlNode.replace("/", "_") + fileName);
        properties.put("uji:name", fileName);

        document.updateProperties(properties, true);

        AlfrescoDocument alfrescoDocument = (AlfrescoDocument) document;
        alfrescoDocument.removeAspect("P:rn:renditioned");

        ContentStream contentStream =
                session.getObjectFactory().createContentStream(fileName, contentLength, contentType, content);

        document.setContentStream(contentStream, true);

        alfrescoDocument = addAspects(alfrescoDocument);
        thumbailService.build(alfrescoDocument);

        return document;
    }

    private AlfrescoDocument addAspects(AlfrescoDocument document)
    {
        document.addAspect("P:rn:renditioned");

        return document;
    }

    private AlfrescoDocument addPublicAccessACLToDocument(AlfrescoDocument document)
    {
        OperationContextImpl operationContext = new OperationContextImpl();
        operationContext.setIncludeAcls(true);

        document = (AlfrescoDocument) session.getObject(document, operationContext);

        List<Ace> aceListIn = Collections.singletonList(
                session.getObjectFactory().createAce("guest", Collections.singletonList("cmis:read")));
        document.addAcl(aceListIn, AclPropagation.PROPAGATE);

        return document;
    }

    public CmisObject getByName(String urlNode, String fileName)
    {
        return session.getObjectByPath(workingPath + "/" + urlNode.replace("/", "_") + fileName);
    }

    public void removeByName(String urlNode, String fileName)
    {
        CmisObject object = getByName(urlNode, fileName);
        object.delete(false);
    }

    public CmisObject getById(String fileId)
    {
        return session.getObject(fileId, context);
    }

    public ContentStream getContentStreamById(String fileId)
    {
        Document doc = (Document) session.getObject(fileId, context);

        return doc.getContentStream();
    }

    public void removeById(String fileId)
    {
        CmisObject object = getById(fileId);
        object.delete(false);
    }

    public ItemIterable<QueryResult> searchByNameAndMimeType(String name, String urlNode, String stringInMimeType,
            int start, int limit)
    {
        String searchQuery = buildQueryForSearchByNameAndMimeType(name, urlNode, stringInMimeType);

        return getResult(start, limit, searchQuery);
    }

    public ItemIterable<QueryResult> searchByNameAndNotMimeType(String name, String urlNode, String stringInMimeType,
            int start, int limit)
    {
        String searchQuery = buildQueryForSearchByNameAndNotMimeType(name, urlNode, stringInMimeType);

        return getResult(start, limit, searchQuery);
    }

    public ItemIterable<QueryResult> searchByContentAndNotMimeType(String content, String urlNode,
            String stringInMimeType, int start, int limit)
    {
        String searchQuery = buildQueryForSearchByContentAndNotMimeType(content, urlNode, stringInMimeType);

        return getResult(start, limit, searchQuery);
    }

    public Long getTotalNumItemsByNameAndMimeType(String name, String urlNode, String stringInMimeType)
    {
        String searchQuery = buildQueryForSearchByNameAndMimeType(name, urlNode, stringInMimeType);

        return getTotalNumItems(searchQuery);
    }

    public Long getTotalNumItemsByContentAndNotMimeType(String content, String urlNode, String stringInMimeType)
    {
        String searchQuery = buildQueryForSearchByContentAndNotMimeType(content, urlNode, stringInMimeType);

        return getTotalNumItems(searchQuery);
    }

    public Long getTotalNumItemsByNameAndNotMimeType(String content, String urlNode, String stringInMimeType)
    {
        String searchQuery = buildQueryForSearchByNameAndNotMimeType(content, urlNode, stringInMimeType);

        return getTotalNumItems(searchQuery);
    }

    private String buildQueryForSearchByContentAndNotMimeType(String content, String urlNode, String stringInMimeType)
    {
        content = QueryParser.escape(content);

        String searchQuery = MessageFormat.format(
                "SELECT * FROM uji:doc where in_folder(''{2}'') and (contains(''\\''{0}\\'''') or contains(''uji:name:\\''*{0}*\\'''')) and cmis:contentStreamMimeType not like ''%{1}%'' and uji:urlNode like ''{3}%''",
                content, stringInMimeType, this.workingFolder.getId(), urlNode);
        return searchQuery;
    }

    private String buildQueryForSearchByNameAndMimeType(String name, String urlNode, String stringInMimeType)
    {
        String searchQuery = MessageFormat.format(
                "SELECT * FROM uji:doc where in_folder(''{2}'') and (contains(''uji:name:\\''*{0}*\\'''')) and cmis:contentStreamMimeType like ''%{1}%'' and uji:urlNode like ''%{3}%'' ",
                name, stringInMimeType, this.workingFolder.getId(), urlNode);

        return searchQuery;
    }

    private String buildQueryForSearchByNameAndNotMimeType(String name, String urlNode, String stringInMimeType)
    {
        String searchQuery = MessageFormat.format(
                "SELECT * FROM uji:doc where in_folder(''{2}'') and (contains(''uji:name:\\''*{0}*\\'''')) and cmis:contentStreamMimeType not like ''%{1}%'' and uji:urlNode like ''%{3}%'' ",
                name, stringInMimeType, this.workingFolder.getId(), urlNode);

        return searchQuery;
    }

    private ItemIterable<QueryResult> getResult(int start, int limit, String searchQuery)
    {
        ItemIterable<QueryResult> queryResult = session.query(searchQuery, false, context);
        queryResult = queryResult.skipTo(start).getPage(limit);

        return queryResult;
    }

    private Long getTotalNumItems(String searchQuery)
    {
        ItemIterable<QueryResult> queryResult = session.query(searchQuery, false);

        return queryResult.getPageNumItems();
    }
}
