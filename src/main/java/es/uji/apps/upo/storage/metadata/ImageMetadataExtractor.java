package es.uji.apps.upo.storage.metadata;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import es.uji.apps.upo.storage.RecursoMetadato;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Component
public class ImageMetadataExtractor
{
    public List<RecursoMetadato> extract(InputStream image)
    {
        List<RecursoMetadato> metadatos = new ArrayList<>();

        try
        {
            Metadata metadata = ImageMetadataReader.readMetadata(image);

            for (Directory directory : metadata.getDirectories())
            {
                for (Tag tag : directory.getTags())
                {
                    RecursoMetadato metadato = new RecursoMetadato();

                    metadato.setKey(tag.getTagName());
                    metadato.setType(tag.getDirectoryName());
                    metadato.setValue(tag.getDescription());

                    metadatos.add(metadato);
                }
            }
        } catch (Exception e)
        {
        }

        return metadatos;
    }
}
