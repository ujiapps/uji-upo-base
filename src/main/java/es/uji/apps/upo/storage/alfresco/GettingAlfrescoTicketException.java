package es.uji.apps.upo.storage.alfresco;

import es.uji.apps.upo.exceptions.StorageException;

@SuppressWarnings("serial")
public class GettingAlfrescoTicketException extends StorageException 
{
    public GettingAlfrescoTicketException()
    {
        super("Error recuperando un ticket de autenticación de Alfresco");
    }
    
    public GettingAlfrescoTicketException(String message)
    {
        super(message);
    }
    
}
