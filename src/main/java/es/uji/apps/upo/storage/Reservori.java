package es.uji.apps.upo.storage;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.uji.apps.upo.migracion.FileSettings;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.client.api.Rendition;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.upo.exceptions.BorradoFicheroDelReservoriException;
import es.uji.apps.upo.exceptions.BuscadoFicherosEnElReservoriException;
import es.uji.apps.upo.exceptions.ImportarDocumentoAlReservoriException;
import es.uji.apps.upo.exceptions.StorageException;
import es.uji.apps.upo.utils.Hasher;

//@Component
public class Reservori
{
    public StorageClient client;

    public String server;

    @Autowired
    public Reservori(String server, StorageClient client)
    {
        this.server = server;
        this.client = client;
    }

    private Map<String, Object> getResourcesList(RepositoryQuery search, String query, String urlNode, String mimeType,
            int start, int limit)
            throws BuscadoFicherosEnElReservoriException
    {
        Map<String, Object> respuesta = new HashMap<String, Object>();
        List<RecursoReservori> listaDocumentos = new ArrayList<RecursoReservori>();

        try
        {
            ItemIterable<QueryResult> result = search.search(query, urlNode, mimeType, start, limit);
            Long numItems = search.getTotalNumItems(query, urlNode, mimeType);
            respuesta.put("numDocumentos", numItems);

            if (numItems > 0)
            {
                listaDocumentos = queryResultToRecursoReservori(result);
            }
        } catch (Exception e)
        {
            throw new BuscadoFicherosEnElReservoriException();
        }

        respuesta.put("listaDocumentos", listaDocumentos);

        return respuesta;
    }

    public Map<String, Object> getListImagesURL(String query, String urlNode, int start, int limit)
            throws BuscadoFicherosEnElReservoriException
    {
        return getResourcesList(new RepositoryQuery()
        {
            @Override
            public ItemIterable<QueryResult> search(String query, String urlNode, String mimeType, int start, int limit)
            {
                return client.searchByNameAndMimeType(query, urlNode, mimeType, start, limit);
            }

            @Override
            public Long getTotalNumItems(String query, String urlNode, String mimeType)
            {
                return client.getTotalNumItemsByNameAndMimeType(query, urlNode, mimeType);
            }

        }, query, urlNode, "image", start, limit);
    }

    public Map<String, Object> getListDocumentsURL(String query, String urlNode, int start, int limit)
            throws BuscadoFicherosEnElReservoriException
    {
        if (query.length() < 3)
        {
            return getDocumentsURLByName(query, urlNode, start, limit);
        }

        return getDocumentsURLByContent(query, urlNode, start, limit);
    }

    private Map<String, Object> getDocumentsURLByName(String query, String urlNode, int start, int limit)
            throws BuscadoFicherosEnElReservoriException
    {
        return getResourcesList(new RepositoryQuery()
        {
            @Override
            public ItemIterable<QueryResult> search(String query, String urlNode, String mimeType, int start, int limit)
            {
                return client.searchByNameAndNotMimeType(query, urlNode, mimeType, start, limit);
            }

            @Override
            public Long getTotalNumItems(String query, String urlNode, String mimeType)
            {
                return client.getTotalNumItemsByNameAndNotMimeType(query, urlNode, mimeType);
            }

        }, query, urlNode, "image", start, limit);
    }

    private Map<String, Object> getDocumentsURLByContent(String query, String urlNode, int start, int limit)
            throws BuscadoFicherosEnElReservoriException
    {
        return getResourcesList(new RepositoryQuery()
        {
            @Override
            public ItemIterable<QueryResult> search(String query, String urlNode, String mimeType, int start, int limit)
            {
                return client.searchByContentAndNotMimeType(query, urlNode, mimeType, start, limit);
            }

            @Override
            public Long getTotalNumItems(String query, String urlNode, String mimeType)
            {
                return client.getTotalNumItemsByContentAndNotMimeType(query, urlNode, mimeType);
            }

        }, query, urlNode, "image", start, limit);
    }

    private List<RecursoReservori> queryResultToRecursoReservori(ItemIterable<QueryResult> result)
    {
        List<RecursoReservori> listaDocumentos = new ArrayList<RecursoReservori>();

        for (QueryResult item : result)
        {
            RecursoReservori recurso = new RecursoReservori();

            String fileName = (String) item.getPropertyById("uji:name").getFirstValue();
            String idRecurso = (String) item.getPropertyById("alfcmis:nodeRef").getFirstValue();

            recurso.setId(idRecurso);
            recurso.setNombre(fileName);
            recurso.setTypeMime((String) item.getPropertyById("cmis:contentStreamMimeType").getFirstValue());
            recurso.setUrl(getUrlRecurso(fileName, idRecurso));

            for (Rendition rendition : item.getRenditions())
            {
                if (rendition.getKind().contains("thumbnail"))
                {
                    recurso.setUrlThumbnail(getUrlThumbnailRecurso(rendition.getStreamId()));
                }
            }

            if (recurso.getUrlThumbnail() == null || recurso.getUrlThumbnail().isEmpty())
            {
                recurso.setUrlThumbnail(recurso.getUrl());
            }

            listaDocumentos.add(recurso);
        }

        return listaDocumentos;
    }

    public String insertFile(byte[] file, String tipoMime, String nombre, String urlNode)
            throws StorageException
    {
        try
        {
            CmisObject cmisObject = client.getByName(urlNode, nombre);
            String idRecurso = cmisObject.getProperty(PropertyIds.OBJECT_ID).getValueAsString();

            return getUrlRecurso(nombre, idRecurso);
        } catch (Exception e)
        {
            Integer sizeRecurso = file.length;

            Document document = client.add(nombre, tipoMime, sizeRecurso, new ByteArrayInputStream(file), urlNode);

            String idRecurso = document.getProperty(PropertyIds.OBJECT_ID).getValueAsString();

            return getUrlRecurso(nombre, idRecurso);
        }
    }

    public void updateFile(String id, byte[] file, String tipoMime, String nombre, String urlNode)
            throws StorageException
    {
        CmisObject cmisObject = client.getById(id);

        client.replace((Document) cmisObject, nombre, tipoMime, file.length, new ByteArrayInputStream(file), urlNode);
    }

    public void deleteFile(String fileId)
            throws BorradoFicheroDelReservoriException
    {
        try
        {
            client.removeById(fileId);
        } catch (Exception e)
        {
            throw new BorradoFicheroDelReservoriException();
        }
    }

    public String convierteURL(FileSettings fileSettings)
            throws ImportarDocumentoAlReservoriException
    {
        try
        {
            if (fileSettings.getUrl() == null)
            {
                return null;
            }

            URLConnection urlConnection = fileSettings.getUrlParsed().openConnection();
            HttpURLConnection httpConnection = (HttpURLConnection) urlConnection;

            if (httpConnection.getResponseCode() != 200)
            {
                return fileSettings.getUrl();
            }

            try
            {
                return checkInRepository(fileSettings);
            } catch (Exception e)
            {
                InputStream urlContent = fileSettings.getUrlParsed().openStream();
                String contentTypeRecurso = urlConnection.getContentType();
                Integer sizeRecurso = urlConnection.getContentLength();

                return insertFileFromContenido(fileSettings, urlContent, contentTypeRecurso, sizeRecurso);
            }
        } catch (Exception e)
        {
            throw new ImportarDocumentoAlReservoriException(
                    "Errada al importar un document al reservori, amb url: " + fileSettings.getUrl());
        }
    }

    public String convierteContenido(FileSettings fileSettings, InputStream content, String contentTypeRecurso,
            Integer sizeRecurso)
            throws ImportarDocumentoAlReservoriException
    {
        try
        {
            try
            {
                return checkInRepository(fileSettings);
            } catch (Exception e)
            {
                return insertFileFromContenido(fileSettings, content, contentTypeRecurso, sizeRecurso);
            }
        } catch (Exception e)
        {
            throw new ImportarDocumentoAlReservoriException(
                    "Errada al importar un document al reservori, amb url: " + fileSettings.getUrl());
        }
    }

    private String checkInRepository(FileSettings fileSettings)
    {
        CmisObject cmisObject =
                client.getByName(fileSettings.getUrlNode(), fileSettings.getHash() + fileSettings.getFileName());
        String idRecurso = cmisObject.getProperty("alfcmis:nodeRef").getValueAsString();

        return getUrlRecurso(fileSettings.getFileName(), idRecurso);
    }

    private String insertFileFromContenido(FileSettings fileSettings, InputStream content, String contentTypeRecurso,
            Integer sizeRecurso)
            throws ImportarDocumentoAlReservoriException
    {
        try
        {
            Document document = client.add(fileSettings.getFileName(), contentTypeRecurso, sizeRecurso, content,
                    fileSettings.getUrlNode(), fileSettings.getFileNameCompleto(), fileSettings.getHash());

            String idRecurso = document.getProperty("alfcmis:nodeRef").getValueAsString();

            return getUrlRecurso(fileSettings.getFileName(), idRecurso);
        } catch (Exception e)
        {
            throw new ImportarDocumentoAlReservoriException(
                    "Errada al importar un document al reservori, amb url: " + fileSettings.getUrl());
        }
    }

    private String getUrlRecurso(String fileName, String idRecurso)
    {
        idRecurso = idRecurso.replace(":/", "");

        try
        {
            fileName = URLEncoder.encode(fileName, "UTF-8");
        } catch (Exception e)
        {
            fileName = "file." + FilenameUtils.getExtension(fileName);
        }

        return server + "/alfresco/d/d/" + idRecurso + "/" + fileName + "?guest=true";
    }

    private String getUrlThumbnailRecurso(String idThumbnail)
    {
        return getUrlRecurso("image", idThumbnail);
    }

    public ContentStream getFileById(String id)
    {
        return client.getContentStreamById(id);
    }
}
