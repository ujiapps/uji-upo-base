package es.uji.apps.upo.storage;

import es.uji.apps.upo.exceptions.BorradoFicheroDelReservoriException;
import es.uji.apps.upo.exceptions.BuscadoFicherosEnElReservoriException;
import es.uji.apps.upo.exceptions.StorageException;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Map;

//@Component
@XmlRootElement
public class RecursoReservori
{
    private String id;
    private String url;
    private String urlThumbnail;
    private String urlRedirect;
    private String nombre;
    private String typeMime;
    private static Reservori reservori;

    @Autowired
    public void setReservori(Reservori reservori)
    {
        RecursoReservori.reservori = reservori;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getUrlThumbnail()
    {
        return urlThumbnail;
    }

    public void setUrlThumbnail(String urlThumbnail)
    {
        this.urlThumbnail = urlThumbnail;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getTypeMime()
    {
        return typeMime;
    }

    public void setTypeMime(String typeMime)
    {
        this.typeMime = typeMime;
    }

    public static Map<String, Object> getListImagesURL(String query, String urlNode, int start, int limit)
            throws BuscadoFicherosEnElReservoriException
    {
        return reservori.getListImagesURL(query, urlNode, start, limit);
    }

    public static Map<String, Object> getListDocumentsURL(String query, String urlNode, int start, int limit)
            throws BuscadoFicherosEnElReservoriException
    {
        return reservori.getListDocumentsURL(query, urlNode, start, limit);
    }

    public static String insertFile(byte[] file, String tipoMime, String nombre, String urlNode)
            throws StorageException
    {
        return reservori.insertFile(file, tipoMime, nombre, urlNode);
    }

    public static void updateFile(String id, byte[] file, String tipoMime, String nombre, String urlNode)
            throws StorageException
    {
        reservori.updateFile(id, file, tipoMime, nombre, urlNode);
    }

    public static void deleteFile(String recursoId)
            throws BorradoFicheroDelReservoriException
    {
        reservori.deleteFile(recursoId);
    }

    public static ContentStream getFileById(String id)
    {
        return reservori.getFileById(id);
    }

    public String getUrlRedirect()
    {
        return urlRedirect;
    }

    public void setUrlRedirect(String urlRedirect)
    {
        this.urlRedirect = urlRedirect;
    }
}