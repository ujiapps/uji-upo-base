package es.uji.apps.upo.storage;

public class RecursoBinario
{
    private Long id;
    private Long contenidoIdiomaId;
    private String nombre;
    private String typeMime;
    private String idioma;

    public RecursoBinario()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getTypeMime()
    {
        return typeMime;
    }

    public void setTypeMime(String typeMime)
    {
        this.typeMime = typeMime;
    }

    public String getIdioma()
    {
        return idioma;
    }

    public void setIdioma(String idioma)
    {
        this.idioma = idioma;
    }

    public Long getContenidoIdiomaId()
    {
        return contenidoIdiomaId;
    }

    public void setContenidoIdiomaId(Long contenidoIdiomaId)
    {
        this.contenidoIdiomaId = contenidoIdiomaId;
    }
}