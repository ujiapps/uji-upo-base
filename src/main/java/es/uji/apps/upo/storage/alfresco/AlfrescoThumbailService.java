package es.uji.apps.upo.storage.alfresco;

import org.alfresco.cmis.client.AlfrescoDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sun.jersey.api.client.ClientResponse;

//@Component
public class AlfrescoThumbailService extends AlfrescoService
{
    private final AlfrescoTicketService ticketService;

    @Autowired
    public AlfrescoThumbailService(AlfrescoCredentials credentials,
            AlfrescoTicketService ticketService)
    {
        super(credentials);

        this.ticketService = ticketService;
    }

    public void build(AlfrescoDocument document) throws GettingAlfrescoTicketException
    {
        String ticketValue = ticketService.getTicket();
        String id = document.getPropertyValue("alfcmis:nodeRef");

        service.path("/node/" + id.replace(":/", "") + "/content/thumbnails/doclib")
                .queryParam("c", "force").queryParam("alf_ticket", ticketValue)
                .get(ClientResponse.class);
    }
}
