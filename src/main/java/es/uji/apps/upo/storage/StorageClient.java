package es.uji.apps.upo.storage;

import java.io.InputStream;
import java.security.NoSuchAlgorithmException;

import org.alfresco.cmis.client.AlfrescoDocument;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.QueryResult;

import es.uji.apps.upo.exceptions.StorageException;
import es.uji.apps.upo.storage.alfresco.GettingAlfrescoTicketException;
import org.apache.chemistry.opencmis.commons.data.ContentStream;

public interface StorageClient
{
    Document add(String fileName, String contentType, int contentLength, InputStream content,
            String urlNode, String urlOriginal, String hash) throws StorageException;

    Document add(String fileName, String contentType, int contentLength, InputStream content,
            String urlNode) throws StorageException;

    Document replace(Document document, String fileName, String contentType, int contentLength,
            InputStream content, String urlNode) throws StorageException;

    CmisObject getByName(String urlNode, String fileName);

    void removeByName(String urlNode, String fileName);

    CmisObject getById(String fileId);

    ContentStream getContentStreamById(String fileId);

    void removeById(String fileId);

    ItemIterable<QueryResult> searchByNameAndMimeType(String name, String urlNode,
            String stringInMimeType, int start, int limit);

    ItemIterable<QueryResult> searchByContentAndNotMimeType(String content, String urlNode,
            String stringInMimeType, int start, int limit);

    ItemIterable<QueryResult> searchByNameAndNotMimeType(String query, String urlNode,
            String mimeType, int start, int limit);

    Long getTotalNumItemsByNameAndMimeType(String name, String urlNode, String stringInMimeType);

    Long getTotalNumItemsByContentAndNotMimeType(String content, String urlNode,
            String stringInMimeType);

    Long getTotalNumItemsByNameAndNotMimeType(String content, String urlNode,
            String stringInMimeType);
}
