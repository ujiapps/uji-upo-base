package es.uji.apps.upo.storage.alfresco;

import java.io.InputStream;

import javax.ws.rs.core.MediaType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.sun.jersey.api.client.ClientResponse;

//@Component
public class AlfrescoTicketService extends AlfrescoService
{
    @Autowired
    public AlfrescoTicketService(AlfrescoCredentials credentials)
    {
        super(credentials);
    }

    public String getTicket() throws GettingAlfrescoTicketException
    {
        ClientResponse clientResponse = service.path("/login")
                .queryParam("u", credentials.getUserName())
                .queryParam("pw", credentials.getPassword()).accept(MediaType.TEXT_XML)
                .get(ClientResponse.class);
        InputStream streamResultado = clientResponse.getEntity(InputStream.class);

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);

        Document document;

        try
        {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            document = documentBuilder.parse(streamResultado);
        }
        catch (Exception e)
        {
            throw new GettingAlfrescoTicketException();
        }

        NodeList tickets = document.getElementsByTagName("ticket");
        return tickets.item(0).getFirstChild().getNodeValue();
    }
}
