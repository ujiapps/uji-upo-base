package es.uji.apps.upo.storage;

import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.QueryResult;

public interface RepositoryQuery
{
    ItemIterable<QueryResult> search(String query, String urlNode, String mimeType, int start, int limit);
    
    // Existe un error al reguperar el result.getTotalNumItems
    // Este procedimiento sirve para evitar este error
    Long getTotalNumItems(String query, String urlNode, String mimeType);
}
