package es.uji.apps.upo.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.upo.model.Persona;
import es.uji.apps.upo.model.PersonaTodas;
import es.uji.apps.upo.model.QFranquiciaAcceso;
import es.uji.apps.upo.model.QPersona;
import es.uji.apps.upo.model.QPersonaTodas;
import es.uji.apps.upo.model.QUsuarioExterno;
import es.uji.apps.upo.model.UsuarioExterno;
import es.uji.apps.upo.model.enums.TipoFranquiciaAcceso;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;

import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PersonaDAO extends BaseDAODatabaseImpl implements LookupDAO
{
    private QPersona qPersona = QPersona.persona;
    private QPersonaTodas qPersonaTodas = QPersonaTodas.personaTodas;
    private QUsuarioExterno qUsuarioExterno = QUsuarioExterno.usuarioExterno;
    private QFranquiciaAcceso qFranquiciaAcceso = QFranquiciaAcceso.franquiciaAcceso;

    public Persona getPersona(Long personaId)
    {
        List<Persona> listaPersonas = get(Persona.class, personaId);

        if (listaPersonas != null && listaPersonas.size() > 0)
        {
            return listaPersonas.get(0);
        }

        return new Persona();
    }

    public List<Persona> getPersonas()
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        return query.from(qPersona).orderBy(qPersona.apellido1.asc(), qPersona.apellido2.asc())
                .list(qPersona);
    }

    public List<Persona> getPersonasByFranquiciaIdNoAsignados(Long franquiciaId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        query = query.from(qPersona).orderBy(qPersona.apellido1.asc(), qPersona.apellido2.asc());

        List<Long> listadoPersonas = getListaIdsPersonaConAccesoAFranquicia(franquiciaId);

        if (listadoPersonas != null && !listadoPersonas.isEmpty())
        {
            query = query.where(qPersona.id.notIn(listadoPersonas)).orderBy(
                    qPersona.apellido1.asc(), qPersona.apellido2.asc());
        }

        return query.list(qPersona);
    }

    public List<Persona> getPersonasAdministradorasByFranquiciaId(Long franquiciaId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        return query
                .from(qPersona)
                .join(qPersona.upoFranquiciasAccesos, qFranquiciaAcceso)
                .where(qFranquiciaAcceso.upoFranquicia.id.eq(franquiciaId).and(
                        qFranquiciaAcceso.tipo.eq(TipoFranquiciaAcceso.ADMINISTRADOR)))
                .distinct().list(qPersona);
    }

    private List<Long> getListaIdsPersonaConAccesoAFranquicia(Long franquiciaId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        return query
                .from(qFranquiciaAcceso)
                .where(qFranquiciaAcceso.upoFranquicia.id.eq(franquiciaId))
                .orderBy(qFranquiciaAcceso.upoExtPersona.apellido1.asc(),
                        qFranquiciaAcceso.upoExtPersona.apellido2.asc())
                .list(qFranquiciaAcceso.upoExtPersona.id);
    }

    public List<PersonaTodas> getUsuariosExternos()
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        return query.from(qPersonaTodas).join(qPersonaTodas.usuariosExternos, qUsuarioExterno)
                .orderBy(qPersonaTodas.apellido1.asc(), qPersonaTodas.apellido2.asc())
                .list(qPersonaTodas);
    }

    @Override
    public List<LookupItem> search(String cadena)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<LookupItem> result = new ArrayList<LookupItem>();

        if (cadena != null && !cadena.isEmpty())
        {
            query.from(qPersonaTodas).where(
                    qPersonaTodas.nombreBusqueda.lower().like("%" + cadena.toLowerCase() + "%"));

            List<PersonaTodas> listaPersonas = query.list(qPersonaTodas);

            for (PersonaTodas personaEncontrada : listaPersonas)
            {
                LookupItem lookupItem = new LookupItem();

                lookupItem.setId(String.valueOf(personaEncontrada.getId()));
                lookupItem.setNombre(personaEncontrada.getNombreCompleto().trim());

                result.add(lookupItem);
            }
        }

        return result;
    }

}