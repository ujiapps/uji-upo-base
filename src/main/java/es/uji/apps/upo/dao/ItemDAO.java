package es.uji.apps.upo.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.upo.model.Item;
import es.uji.apps.upo.model.QGrupoItem;
import es.uji.apps.upo.model.QItem;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ItemDAO extends BaseDAODatabaseImpl
{
    private QItem item = QItem.item;
    private QGrupoItem grupoItem = QGrupoItem.grupoItem;

    public List<Item> getItemsByGrupoNoAsignados(Long grupoId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        List<Long> listadoItemsIds = getItemsIdsByGrupo(grupoId);

        if (listadoItemsIds != null && !listadoItemsIds.isEmpty())
        {
            query = query.from(item).where(item.id.notIn(listadoItemsIds)).orderBy(item.nombreCA.asc());
        }
        else
        {
            query = query.from(item).orderBy(item.nombreCA.asc());
        }

        return query.list(item);

    }

    private List<Long> getItemsIdsByGrupo(Long grupoId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        query.from(grupoItem).where(grupoItem.grupo.id.eq(grupoId)).orderBy(grupoItem.item.nombreCA.asc());

        return query.list(grupoItem.item.id);
    }

    public List<Item> getItemsByGrupoId(Long grupoId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        QGrupoItem grupoItem = QGrupoItem.grupoItem;

        query.from(grupoItem).where(grupoItem.grupo.id.eq(grupoId)).orderBy(grupoItem.orden.asc());

        return query.list(grupoItem.item);
    }

    public List<Item> getByPersonaId(Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        return query.from(item).where(item.persona.id.eq(connectedUserId)).list(item);
    }

    public List<Item> getItemsByGrupoNoAsignadosByPersonaId(Long grupoId, Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        List<Long> listadoItemsIds = getItemsIdsByGrupo(grupoId);

        if (listadoItemsIds != null && !listadoItemsIds.isEmpty())
        {
            query = query.from(item)
                    .where(item.id.notIn(listadoItemsIds).and(item.persona.id.eq(connectedUserId)))
                    .orderBy(item.nombreCA.asc());
        }
        else
        {
            query = query.from(item).where(item.persona.id.eq(connectedUserId)).orderBy(item.nombreCA.asc());
        }

        return query.list(item);
    }
}
