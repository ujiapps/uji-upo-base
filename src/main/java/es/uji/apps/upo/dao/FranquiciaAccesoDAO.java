package es.uji.apps.upo.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.upo.model.FranquiciaAcceso;
import es.uji.apps.upo.model.QFranquiciaAcceso;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

@Repository
public class FranquiciaAccesoDAO extends BaseDAODatabaseImpl
{
    private QFranquiciaAcceso qFranquiciaAcceso = QFranquiciaAcceso.franquiciaAcceso;

    public FranquiciaAcceso getFranquiciaAccesoByFranquiciaIdAndPersonaId(
            Long franquiciaId,Long personaId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        return query.from(qFranquiciaAcceso)
                .where(qFranquiciaAcceso.upoFranquicia.id.eq(franquiciaId)
                        .and(qFranquiciaAcceso.upoExtPersona.id.eq(personaId)))
                .uniqueResult(qFranquiciaAcceso);
    }
}
