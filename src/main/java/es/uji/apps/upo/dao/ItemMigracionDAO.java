package es.uji.apps.upo.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.upo.model.ItemMigracion;
import es.uji.apps.upo.model.QItemMigracion;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ItemMigracionDAO extends BaseDAODatabaseImpl
{
    private QItemMigracion itemMigracion = QItemMigracion.itemMigracion;

    public ItemMigracion getContent(String url)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(itemMigracion)
                .where(itemMigracion.url.isNotNull().and(itemMigracion.url.eq(url)))
                .uniqueResult(itemMigracion);
    }
}