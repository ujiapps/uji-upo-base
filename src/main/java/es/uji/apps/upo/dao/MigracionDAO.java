package es.uji.apps.upo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import es.uji.apps.upo.model.Migracion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class MigracionDAO extends BaseDAODatabaseImpl
{
    public static final String COMMON_SQL = "not exists (select '1' from Contenido c where url = c.urlOriginal)";

    public List<Migracion> getURLsByPaginationAndURL(Integer start, Integer limit, String query)
    {
        List<Migracion> migracionList = null;

        if (query != null)
        {
            migracionList = getPaginated(Migracion.class, "lower(url) like lower('%" + query
                    + "%') and " + COMMON_SQL, "url", "ASC", start, limit);
        }

        if (query == null)
        {
            migracionList = getPaginated(Migracion.class, COMMON_SQL, "url", "ASC", start, limit);
        }

        return migracionList;
    }

    public int getCountURl(String query)
    {
        if (query != null)
        {
            return getCount(Migracion.class, "lower(url) like lower('%" + query + "%') and "
                    + COMMON_SQL);
        }
        else
        {
            return getCount(Migracion.class, COMMON_SQL);
        }
    }
}
