package es.uji.apps.upo.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.upo.model.Autoguardado;
import es.uji.apps.upo.model.Persona;
import es.uji.apps.upo.model.QAutoguardado;
import es.uji.apps.upo.model.QPersona;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class AutoguardadoDAO extends BaseDAODatabaseImpl
{
    private QAutoguardado qautoguardado = QAutoguardado.autoguardado;
    private QPersona qpersona = QPersona.persona;

    @Transactional
    public void deleteByParameters(Long mapaId, Long contenidoId, Long personaId)
    {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qautoguardado);

        if (contenidoId == null)
        {
            deleteClause.where(
                    qautoguardado.nodoMapa.id.eq(mapaId).and(qautoguardado.perId.eq(personaId))
                            .and(qautoguardado.contenido.isNull())).execute();
        }
        else
        {
            deleteClause.where(
                    qautoguardado.nodoMapa.id.eq(mapaId).and(qautoguardado.perId.eq(personaId))
                            .and(qautoguardado.contenido.id.eq(contenidoId))).execute();
        }
    }

    public Autoguardado getByParameters(Long mapaId, Long contenidoId, Long personaId,
            String codigoIdiomaISO)
    {
        JPAQuery query = new JPAQuery(entityManager);

        if (contenidoId != null)
        {

            query.from(qautoguardado).where(
                    qautoguardado.nodoMapa.id.eq(mapaId).and(qautoguardado.perId.eq(personaId))
                            .and(qautoguardado.contenido.id.eq(contenidoId))
                            .and(qautoguardado.idiomaCodigoIso.eq(codigoIdiomaISO.toUpperCase())));
        }
        else
        {
            query.from(qautoguardado).where(
                    qautoguardado.nodoMapa.id.eq(mapaId).and(qautoguardado.perId.eq(personaId))
                            .and(qautoguardado.contenido.id.isNull())
                            .and(qautoguardado.idiomaCodigoIso.eq(codigoIdiomaISO.toUpperCase())));
        }

        if (query.list(qautoguardado).size() > 0)
        {
            query.where(qautoguardado.fecha.in(query.list(qautoguardado.fecha.max())));

            List<Autoguardado> autoguardados = query.list(qautoguardado);

            return autoguardados.get(0);
        }

        return null;
    }

    public List<String> getOtrasPersonasEditandoContenido(Long contenidoId, Long personaId)
    {
        JPAQuery queryAutoguardado = new JPAQuery(entityManager);
        JPAQuery queryPersona = new JPAQuery(entityManager);

        queryAutoguardado.from(qautoguardado).where(
                qautoguardado.perId.ne(personaId).and(qautoguardado.contenido.id.eq(contenidoId)));

        if (queryAutoguardado.list(qautoguardado).size() > 0)
        {
            List<Long> idsPersonas = new ArrayList<Long>();
            List<String> nombresPersonas = new ArrayList<String>();

            for (Autoguardado autoguardado : queryAutoguardado.list(qautoguardado))
            {
                idsPersonas.add(autoguardado.getPerId());
            }

            queryPersona.from(qpersona).where(qpersona.id.in(idsPersonas));

            for (Persona persona : queryPersona.list(qpersona))
            {
                nombresPersonas.add(persona.getNombreCompleto());
            }

            return nombresPersonas;
        }

        return null;
    }
}
