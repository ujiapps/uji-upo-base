package es.uji.apps.upo.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.upo.model.ContenidoIdioma;
import es.uji.apps.upo.model.ContenidoIdiomaRecurso;
import es.uji.apps.upo.model.QContenidoIdioma;
import es.uji.apps.upo.model.QContenidoIdiomaRecurso;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class ContenidoIdiomaRecursoDAO extends BaseDAODatabaseImpl
{
    private QContenidoIdiomaRecurso contenidoIdiomaRecurso = QContenidoIdiomaRecurso.contenidoIdiomaRecurso;
    private QContenidoIdioma contenidoIdioma = QContenidoIdioma.contenidoIdioma;

    @Transactional
    public void deleteByContenido(Long contenidoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(contenidoIdioma).where(contenidoIdioma.upoObjeto.id.eq(contenidoId));

        if (query.list(contenidoIdioma).size() > 0)
        {
            JPADeleteClause deleteClause = new JPADeleteClause(entityManager,
                    contenidoIdiomaRecurso).where(contenidoIdiomaRecurso.contenidoIdioma.in(query
                    .list(contenidoIdioma)));

            deleteClause.execute();
        }
    }

    public List<ContenidoIdiomaRecurso> getContenidoIdiomaRecursosUrlByContenidoIdioma(
            ContenidoIdioma contenidoIdioma)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(contenidoIdiomaRecurso)
                .where(contenidoIdiomaRecurso.contenidoIdioma.eq(contenidoIdioma))
                .orderBy(contenidoIdiomaRecurso.url.asc())
                .distinct()
                .list(contenidoIdiomaRecurso);
    }
}
