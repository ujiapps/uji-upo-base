package es.uji.apps.upo.dao.eujier;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.upo.model.eujier.*;
import es.uji.apps.upo.model.eujier.EujierResultadosBusqueda;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Repository
public class EujierDAO extends BaseDAODatabaseImpl
{
    public static final int RESULTADOS_POR_PAGINA = 12;

    private QEujierRegion qRegion = QEujierRegion.eujierRegion;
    private QEujierAplicacion qAplicacion = QEujierAplicacion.eujierAplicacion;
    private QEujierGrupoItem qGrupoItem = QEujierGrupoItem.eujierGrupoItem;
    private QEujierGrupo qGrupo = QEujierGrupo.eujierGrupo;
    private QEujierItem qItem = QEujierItem.eujierItem;
    private QEujierItemBusqueda qItemBusqueda = QEujierItemBusqueda.eujierItemBusqueda;
    private QEujierSeccion qSeccion = QEujierSeccion.eujierSeccion;
    private QEujierItemFavorito qItemFavorito = QEujierItemFavorito.eujierItemFavorito;
    private QEujierFavorito qFavorito = QEujierFavorito.eujierFavorito;
    private QEujierItemDestacado qItemDestacado = QEujierItemDestacado.eujierItemDestacado;
    private QEujierAviso qAviso = QEujierAviso.eujierAviso;
    private QEujierItemMasUsado qItemMasUsado = QEujierItemMasUsado.eujierItemMasUsado;
    private QEujierItemMasUsadoPerfil qItemMasUsadoPerfil = QEujierItemMasUsadoPerfil.eujierItemMasUsadoPerfil;
    private QEujierItemUbicacion qEujierItemUbicacion = QEujierItemUbicacion.eujierItemUbicacion;

    private EujiItemDataMapper itemDataMapper = new EujiItemDataMapper();

    public List<EujierSeccion> getSecciones(String idioma, Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<EujierSeccion> secciones = query.from(qSeccion)
                .where(qSeccion.personaId.eq(connectedUserId))
                .orderBy(qSeccion.orden.asc())
                .list(qSeccion);

        for (EujierSeccion seccion : secciones)
        {
            seccion.setNombre(itemDataMapper.getField(idioma, seccion.getNombreCA(), seccion.getNombreES(),
                    seccion.getNombreEN()));
        }

        return secciones;
    }

    public EujierSeccion getSeccionByCodigo(String idioma, Long connectedUserId, String codigoSeccion)
    {
        JPAQuery query = new JPAQuery(entityManager);

        EujierSeccion seccion = query.from(qSeccion)
                .where(qSeccion.personaId.eq(connectedUserId).and(qSeccion.codigo.eq(codigoSeccion)))
                .uniqueResult(qSeccion);

        seccion.setNombre(
                itemDataMapper.getField(idioma, seccion.getNombreCA(), seccion.getNombreES(), seccion.getNombreEN()));

        return seccion;
    }

    public List<EujierRegion> getRegiones(String idioma)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qRegion).where(qRegion.idioma.eq(idioma)).orderBy(qRegion.id.asc()).list(qRegion);
    }

    public List<EujierAplicacion> getAplicaciones(String idioma)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qAplicacion)
                .where(qAplicacion.idioma.eq(idioma))
                .orderBy(qAplicacion.nombre.asc())
                .list(qAplicacion);
    }

    public List<EujierItem> getFavoritos(String idioma, Long connectedUserId, Long numItems)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<EujierItemFavorito> favoritos = query.from(qItemFavorito)
                .where(qItemFavorito.idioma.eq(idioma).and(qItemFavorito.personaId.eq(connectedUserId)))
                .orderBy(qItemFavorito.nombre.asc())
                .limit(numItems)
                .list(qItemFavorito);

        return itemDataMapper.favoritosToItems(favoritos);
    }

    public List<EujierItem> getDestacados(String idioma, Long connectedUserId, Long grupoPerfilId, Long numItems)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<EujierItemDestacado> destacados = query.from(qItemDestacado)
                .where(qItemDestacado.personaId.eq(connectedUserId).and(qItemDestacado.grupoPerfilId.eq(grupoPerfilId)))
                .limit(numItems)
                .list(qItemDestacado);

        List<EujierItem> items = itemDataMapper.destacadosToItems(destacados, idioma);

        Collections.sort(items);

        return items;
    }

    public List<EujierAviso> getAvisos(String idioma, Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<EujierAviso> avisos = query.from(qAviso).where(qAviso.personaId.eq(connectedUserId)).list(qAviso);

        for (EujierAviso aviso : avisos)
        {
            aviso.setAviso(itemDataMapper.getField(idioma, aviso.getAvisoCA(), aviso.getAvisoES(), aviso.getAvisoEN()));
        }

        return avisos;
    }

    public EujierAplicacion getAplicacion(String idioma, Long aplicacionId, Long regionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qAplicacion)
                .where(qAplicacion.idioma.eq(idioma)
                        .and(qAplicacion.aplicacionId.eq(aplicacionId).and(qAplicacion.regionId.eq(regionId))))
                .orderBy(qAplicacion.nombre.asc())
                .uniqueResult(qAplicacion);
    }

    public String getGrupoName(String idioma, Long aplicacionId, Long regionId, Long grupoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qGrupo)
                .where(qGrupo.regionId.eq(regionId)
                        .and(qGrupo.aplicacionId.eq(aplicacionId)
                                .and(qGrupo.grupoId.eq(grupoId))
                                .and(qGrupo.idioma.eq(idioma))))
                .uniqueResult(qGrupo.nombre);
    }

    public List<EujierItem> getItemsByGrupo(String idioma, Long connectedUserId, Long aplicacionId, Long regionId,
            Long grupoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<EujierGrupoItem> gruposItems = query.from(qGrupoItem)
                .where(qGrupoItem.personaId.eq(connectedUserId)
                        .and(qGrupoItem.regionId.eq(regionId))
                        .and(qGrupoItem.aplicacionId.eq(aplicacionId))
                        .and(qGrupoItem.regionId.eq(regionId))
                        .and(qGrupoItem.grupoId.eq(grupoId)))
                .orderBy(qGrupoItem.orden.asc())
                .list(qGrupoItem);

        List<EujierItem> items = itemDataMapper.gruposItemsToItems(gruposItems, idioma);

        return items;
    }

    public List<EujierGrupo> getGruposByAplicacion(String idioma, Long connectedUserId, Long aplicacionId,
            Long regionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<Tuple> resultQuery = query.from(qGrupoItem)
                .where(qGrupoItem.personaId.eq(connectedUserId)
                        .and(qGrupoItem.regionId.eq(regionId))
                        .and(qGrupoItem.aplicacionId.eq(aplicacionId)))
                .groupBy(qGrupoItem.regionId, qGrupoItem.aplicacionId, qGrupoItem.grupoId, qGrupoItem.personaId,
                        qGrupoItem.nombreGrupoCA, qGrupoItem.nombreGrupoEN, qGrupoItem.nombreGrupoES)
                .list(qGrupoItem.regionId, qGrupoItem.aplicacionId, qGrupoItem.grupoId, qGrupoItem.personaId,
                        qGrupoItem.nombreGrupoCA, qGrupoItem.nombreGrupoEN, qGrupoItem.nombreGrupoES,
                        qGrupoItem.grupoId.count());

        List<EujierGrupo> grupos = new ArrayList<>();

        for (Tuple row : resultQuery)
        {
            EujierGrupo grupo = new EujierGrupo();

            grupo.setNombre(itemDataMapper.getField(idioma, row.get(qGrupoItem.nombreGrupoCA),
                    row.get(qGrupoItem.nombreGrupoES), row.get(qGrupoItem.nombreGrupoEN)));
            grupo.setAplicacionId(row.get(qGrupoItem.aplicacionId));
            grupo.setRegionId(row.get(qGrupoItem.regionId));
            grupo.setGrupoId(row.get(qGrupoItem.grupoId));
            grupo.setNumItems(row.get(qGrupoItem.grupoId.count()));

            grupos.add(grupo);
        }

        Collections.sort(grupos);

        return grupos;
    }

    public List<EujierGrupo> getGruposBySearch(String idioma, Long connectedUserId, String texto)
    {
        JPAQuery query = new JPAQuery(entityManager);
        texto = StringUtils.limpiaAcentos(texto).toUpperCase();

        query.from(qGrupoItem).where(qGrupoItem.personaId.eq(connectedUserId));

        BooleanBuilder builder = new BooleanBuilder();

        for (String word : texto.split(" "))
        {
            if ("ca".equalsIgnoreCase(idioma))
            {
                builder.and(qGrupoItem.nombreGrupoBusquedaCA.like('%' + word + '%'));
            }

            if ("es".equalsIgnoreCase(idioma))
            {
                builder.and(qGrupoItem.nombreGrupoBusquedaES.like('%' + word + '%'));
            }

            if ("en".equalsIgnoreCase(idioma))
            {
                builder.and(qGrupoItem.nombreGrupoBusquedaEN.like('%' + word + '%'));
            }
        }

        query.where(builder);

        List<Tuple> resultQuery = query.distinct()
                .list(qGrupoItem.regionId, qGrupoItem.aplicacionId, qGrupoItem.grupoId, qGrupoItem.personaId,
                        qGrupoItem.nombreGrupoCA, qGrupoItem.nombreGrupoEN, qGrupoItem.nombreGrupoES,
                        qGrupoItem.nombreAplicacionCA, qGrupoItem.nombreAplicacionES, qGrupoItem.nombreAplicacionEN,
                        qGrupoItem.nombreRegionCA, qGrupoItem.nombreRegionES, qGrupoItem.nombreRegionEN);

        List<EujierGrupo> grupos = new ArrayList<>();

        for (Tuple row : resultQuery)
        {
            EujierGrupo grupo = new EujierGrupo();

            grupo.setNombre(itemDataMapper.getField(idioma, row.get(qGrupoItem.nombreGrupoCA),
                    row.get(qGrupoItem.nombreGrupoES), row.get(qGrupoItem.nombreGrupoEN)));
            grupo.setAplicacionNombre(itemDataMapper.getField(idioma, row.get(qGrupoItem.nombreAplicacionCA),
                    row.get(qGrupoItem.nombreAplicacionES), row.get(qGrupoItem.nombreAplicacionEN)));
            grupo.setRegionNombre(itemDataMapper.getField(idioma, row.get(qGrupoItem.nombreRegionCA),
                    row.get(qGrupoItem.nombreRegionES), row.get(qGrupoItem.nombreRegionEN)));
            grupo.setAplicacionId(row.get(qGrupoItem.aplicacionId));
            grupo.setRegionId(row.get(qGrupoItem.regionId));
            grupo.setGrupoId(row.get(qGrupoItem.grupoId));

            grupos.add(grupo);
        }

        Collections.sort(grupos);

        return grupos;
    }

    public List<EujierAplicacion> getAplicacionesBySearch(String idioma, String texto)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qAplicacion).where(qAplicacion.idioma.eq(idioma));

        texto = StringUtils.limpiaAcentos(texto).toUpperCase();

        BooleanBuilder builder = new BooleanBuilder();

        for (String word : texto.split(" "))
        {
            builder.and((qAplicacion.nombreBusqueda.like('%' + word + '%')
                    .or(qAplicacion.descripcionBusqueda.like('%' + word + '%'))));
        }

        query.where(builder);

        return query.orderBy(qAplicacion.regionNombre.asc(), qAplicacion.nombre.asc()).list(qAplicacion);
    }

    public EujierResultadosBusqueda getItemsBySearch(String idioma, Long connectedUserId, String texto, Integer pagina)
    {
        JPAQuery query = new JPAQuery(entityManager);
        EujierResultadosBusqueda results = new EujierResultadosBusqueda();

        query.from(qItemBusqueda).where(qItemBusqueda.personaId.eq(connectedUserId)).list(qItemBusqueda);

        texto = StringUtils.limpiaAcentos(texto).toUpperCase();

        BooleanBuilder builder = new BooleanBuilder();

        for (String word : texto.split(" "))
        {
            if ("ca".equalsIgnoreCase(idioma))
            {
                builder.and((qItemBusqueda.nombreBusquedaCA.like('%' + word + '%')
                        .or(qItemBusqueda.descripcionBusquedaCA.like('%' + word + '%'))));
            }

            if ("es".equalsIgnoreCase(idioma))
            {
                builder.and((qItemBusqueda.nombreBusquedaES.like('%' + word + '%')
                        .or(qItemBusqueda.descripcionBusquedaES.like('%' + word + '%'))));
            }

            if ("en".equalsIgnoreCase(idioma))
            {
                builder.and((qItemBusqueda.nombreBusquedaEN.like('%' + word + '%')
                        .or(qItemBusqueda.descripcionBusquedaEN.like('%' + word + '%'))));
            }
        }

        query.where(builder);

        if ("ca".equalsIgnoreCase(idioma))
        {
            query.orderBy(qItemBusqueda.nombreCA.asc());
        }

        if ("es".equalsIgnoreCase(idioma))
        {
            query.orderBy(qItemBusqueda.nombreES.asc());
        }

        if ("en".equalsIgnoreCase(idioma))
        {
            query.orderBy(qItemBusqueda.nombreEN.asc());
        }

        Long numItems = query.uniqueResult(qItemBusqueda.count());

        List<EujierItemBusqueda> itemsBusqueda =
                query.offset((pagina - 1) * RESULTADOS_POR_PAGINA).limit(RESULTADOS_POR_PAGINA).list(qItemBusqueda);

        results.setNumItems(numItems);
        results.setItems(itemDataMapper.itemsBusquedaToItems(itemsBusqueda, idioma));

        return results;
    }

    public List<EujierItem> getNuevos(String idioma, Long connectedUserId, Long numItems)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<EujierItem> results = query.from(qItem)
                .where(qItem.personaId.eq(connectedUserId))
                .orderBy(qItem.fecha.desc())
                .limit(numItems)
                .list(qItem);

        itemDataMapper.completa(results, idioma);

        return results;
    }

    public List<EujierItem> getMasUsados(String idioma, Long connectedUserId, Long numItems)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<EujierItemMasUsado> results = query.from(qItemMasUsado)
                .where(qItemMasUsado.personaId.eq(connectedUserId))
                .orderBy(qItemMasUsado.num.desc())
                .limit(numItems)
                .list(qItemMasUsado);

        return itemDataMapper.itemsMasUsadosToItems(results, idioma);
    }

    public List<EujierItem> getMasUsadosPerfil(String idioma, Long connectedUserId, Long grupoPerfilId, Long numItems)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<EujierItemMasUsadoPerfil> results = query.from(qItemMasUsadoPerfil)
                .where(qItemMasUsadoPerfil.personaId.eq(connectedUserId)
                        .and(qItemMasUsadoPerfil.grupoPerfilId.eq(grupoPerfilId)))
                .orderBy(qItemMasUsadoPerfil.num.desc())
                .limit(numItems)
                .list(qItemMasUsadoPerfil);

        return itemDataMapper.itemsMasUsadosPerfilToItems(results, idioma);
    }

    public void addToFavoritos(Long itemId, Long connectedUserId)
    {
        insert(new EujierFavorito(itemId, connectedUserId));
    }

    @Transactional
    public void delFromFavoritos(Long itemId, Long connectedUserId)
    {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qFavorito).where(
                qFavorito.perId.eq(connectedUserId).and(qFavorito.itemId.eq(itemId)));

        deleteClause.execute();
    }

    public void markAviso(Long avisoId, Long connectedUserId)
    {
        insert(new EujierAvisoVisto(avisoId, connectedUserId));
    }

    public List<EujierItemAplicacion> getAplicacionesByItemId(Long itemId, String idioma)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<EujierItemUbicacion> itemsUbicaciones = query.from(qEujierItemUbicacion)
                .where(qEujierItemUbicacion.itemId.eq(itemId).and(qEujierItemUbicacion.idioma.eq(idioma)))
                .list(qEujierItemUbicacion);

        return itemDataMapper.itemsUbicacionesToItemsAplicaciones(itemsUbicaciones);
    }
}
