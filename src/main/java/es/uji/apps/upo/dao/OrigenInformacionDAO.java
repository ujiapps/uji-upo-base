package es.uji.apps.upo.dao;

import java.util.List;

import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.TipoReferenciaContenido;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class OrigenInformacionDAO extends BaseDAODatabaseImpl
{
    public static final String CATALOGO_URL_BASE = "/seu/cataleg/";

    private QOrigenInformacion qOrigenInformacion = QOrigenInformacion.origenInformacion;
    private QContenido qContenido = QContenido.contenido;
    private QNodoMapaContenido qNodoMapaContenido = QNodoMapaContenido.nodoMapaContenido;

    public List<OrigenInformacion> getOrigenesInformacion()
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        query.from(qOrigenInformacion).orderBy(qOrigenInformacion.nombre.asc());

        return query.list(qOrigenInformacion);
    }

    public List<OrigenInformacion> origenesInformacionUsadosEnCatalogoDeProcedimientos()
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qOrigenInformacion)
                .join(qOrigenInformacion.contenido, qContenido)
                .join(qContenido.upoMapasObjetos, qNodoMapaContenido)
                .where(qNodoMapaContenido.upoMapa.urlCompleta.like(CATALOGO_URL_BASE + "%"));

        return query.orderBy(qOrigenInformacion.nombre.asc()).distinct().list(qOrigenInformacion);
    }
}