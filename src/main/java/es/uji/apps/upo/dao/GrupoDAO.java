package es.uji.apps.upo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.upo.model.Grupo;
import es.uji.apps.upo.model.GrupoItem;
import es.uji.apps.upo.model.Item;
import es.uji.apps.upo.model.QGrupo;
import es.uji.apps.upo.model.QGrupoItem;
import es.uji.apps.upo.model.QMenuGrupo;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class GrupoDAO extends BaseDAODatabaseImpl
{
    private QGrupo grupo = QGrupo.grupo;
    private QMenuGrupo menuGrupo = QMenuGrupo.menuGrupo;
    private QGrupoItem grupoItem = QGrupoItem.grupoItem;

    public List<Grupo> getByPersonaId(Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        return query.from(grupo).where(grupo.persona.id.eq(connectedUserId)).list(grupo);
    }

    public List<Grupo> getGruposByMenuNoAsignadosByPersonaId(Long menuId, Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        List<Long> listadoGruposIds = getGruposIdsByMenu(menuId);

        if (listadoGruposIds != null && !listadoGruposIds.isEmpty())
        {
            query = query.from(grupo)
                    .where(grupo.id.notIn(listadoGruposIds).and(grupo.persona.id.eq(connectedUserId)))
                    .orderBy(grupo.nombreCA.asc());
        }
        else
        {
            query = query.from(grupo).where(grupo.persona.id.eq(connectedUserId)).orderBy(grupo.nombreCA.asc());
        }

        return query.list(grupo);
    }

    public List<Grupo> getGruposByMenuNoAsignados(Long menuId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        List<Long> listadoGruposIds = getGruposIdsByMenu(menuId);

        if (listadoGruposIds != null && !listadoGruposIds.isEmpty())
        {
            query = query.from(grupo).where(grupo.id.notIn(listadoGruposIds)).orderBy(grupo.nombreCA.asc());
        }
        else
        {
            query = query.from(grupo).orderBy(grupo.nombreCA.asc());
        }

        return query.list(grupo);
    }

    private List<Long> getGruposIdsByMenu(Long menuId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        query.from(menuGrupo).where(menuGrupo.menu.id.eq(menuId)).orderBy(menuGrupo.grupo.nombreCA.asc());

        return query.list(menuGrupo.grupo.id);
    }

    @Transactional
    public void addItem(Long grupoId, Long itemId, Integer orden)
    {
        Grupo grupo = new Grupo();
        Item item = new Item();
        GrupoItem grupoItem = new GrupoItem();

        grupo.setId(grupoId);
        item.setId(itemId);

        grupoItem.setGrupo(grupo);
        grupoItem.setItem(item);
        grupoItem.setOrden(orden);

        insert(grupoItem);
    }

    @Transactional
    public void deleteItem(Long grupoId, Long itemId)
    {
        QGrupoItem grupoItem = QGrupoItem.grupoItem;

        JPADeleteClause deleteClause = new JPADeleteClause(this.entityManager, grupoItem);

        deleteClause.where(grupoItem.grupo.id.eq(grupoId), grupoItem.item.id.eq(itemId)).execute();

    }

    public List<Grupo> getGruposByMenuId(Long menuId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        query.from(menuGrupo).where(menuGrupo.menu.id.eq(menuId)).orderBy(menuGrupo.orden.asc());

        return query.list(menuGrupo.grupo);
    }

    @Transactional
    public void updateItem(long grupoId, long itemId, Integer orden)
    {
        GrupoItem grupoItem = getGrupoItemByGrupoIdAndItemId(grupoId, itemId);

        if (grupoItem != null)
        {
            grupoItem.setOrden(orden);
        }

        update(grupoItem);
    }

    public List<GrupoItem> getItemsByGrupoId(Long grupoId)
    {
        QGrupoItem grupoItem = QGrupoItem.grupoItem;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(grupoItem).where(grupoItem.grupo.id.eq(grupoId)).orderBy(grupoItem.orden.asc());

        return query.list(grupoItem);
    }

    private GrupoItem getGrupoItemByGrupoIdAndItemId(long grupoId, long itemId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(grupoItem).where(grupoItem.grupo.id.eq(grupoId).and(grupoItem.item.id.eq(itemId)));

        List<GrupoItem> gruposItems = query.list(grupoItem);

        if (gruposItems.size() > 0)
        {
            return gruposItems.get(0);
        }

        return null;
    }
}
