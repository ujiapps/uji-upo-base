package es.uji.apps.upo.dao;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.upo.model.QRevista;
import es.uji.apps.upo.model.Revista;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

@Repository
public class RevistaDAO extends BaseDAODatabaseImpl
{
    private QRevista qRevista = QRevista.revista;

    public List<Revista> doSearch(String titulo, String resumen, String resumenOP, String palabrasClave,
            String palabrasClaveOP, String seccion, String medio, Date fechaInicio, Date fechaFin)
    {
        JPAQuery query = new JPAQuery(entityManager);
        StringTokenizer st;
        BooleanBuilder builder = new BooleanBuilder();
        BooleanBuilder tituloBuilder = new BooleanBuilder();
        BooleanBuilder resumenBuilder = new BooleanBuilder();
        BooleanBuilder tagsBuilder = new BooleanBuilder();

        query.from(qRevista);

        st = new StringTokenizer(titulo);

        while (st.hasMoreTokens())
        {
            String token = st.nextToken();

            if (token.equalsIgnoreCase("or"))
            {
                tituloBuilder.or(qRevista.tituloLargoSinAcentos.like('%' + limpia(st.nextToken()) + '%'));
            }
            else if (token.equalsIgnoreCase("and"))
            {
                tituloBuilder.and(qRevista.tituloLargoSinAcentos.like('%' + limpia(st.nextToken()) + '%'));
            }
            else
            {
                tituloBuilder.or(qRevista.tituloLargoSinAcentos.like('%' + limpia(token) + '%'));
            }
        }

        builder.and(tituloBuilder);

        st = new StringTokenizer(resumen);

        while (st.hasMoreTokens())
        {
            String token = st.nextToken();

            if (token.equalsIgnoreCase("or"))
            {
                resumenBuilder.or(qRevista.resumenSinAcentos.like('%' + limpia(st.nextToken()) + '%'));
            }
            else if (token.equalsIgnoreCase("and"))
            {
                resumenBuilder.and(qRevista.resumenSinAcentos.like('%' + limpia(st.nextToken()) + '%'));
            }
            else
            {
                resumenBuilder.or(qRevista.resumenSinAcentos.like('%' + limpia(token) + '%'));
            }
        }

        if (resumenOP.equalsIgnoreCase("or"))
        {
            builder.or(resumenBuilder);
        }
        else if (resumenOP.equalsIgnoreCase("and"))
        {
            builder.and(resumenBuilder);
        }

        st = new StringTokenizer(palabrasClave);

        while (st.hasMoreTokens())
        {
            String token = st.nextToken();

            if (token.equalsIgnoreCase("or"))
            {
                tagsBuilder.or(qRevista.tags.like('%' + limpia(st.nextToken()) + '%'));
            }
            else if (token.equalsIgnoreCase("and"))
            {
                tagsBuilder.and(qRevista.tags.like('%' + limpia(st.nextToken()) + '%'));
            }
            else
            {
                tagsBuilder.or(qRevista.tags.like('%' + limpia(token) + '%'));
            }
        }

        if (palabrasClaveOP.equalsIgnoreCase("or"))
        {
            builder.or(tagsBuilder);
        }
        else if (palabrasClaveOP.equalsIgnoreCase("and"))
        {
            builder.and(tagsBuilder);
        }

        query.where(builder);

        if (seccion != null && !seccion.isEmpty())
        {
            query.where(qRevista.urlCompletaBusqueda.like('%' + limpia(seccion) + '%'));
        }

        if (medio != null && !medio.isEmpty())
        {
            query.where(qRevista.otrosAutoresSinAcentos.like('%' + limpia(medio) + '%'));
        }

        if (fechaInicio != null)
        {
            query.where(qRevista.primeraFechaVigencia.goe(fechaInicio));
        }

        if (fechaFin != null)
        {
            query.where(qRevista.primeraFechaVigencia.loe(fechaFin));
        }

        return query.orderBy(qRevista.primeraFechaVigencia.desc()).list(qRevista);
    }

    private String limpia(String string)
    {
        return StringUtils.limpiaAcentos(string.toLowerCase());
    }
}
