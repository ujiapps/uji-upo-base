package es.uji.apps.upo.dao;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.upo.model.Idioma;
import es.uji.apps.upo.model.QIdioma;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class IdiomaDAO extends BaseDAODatabaseImpl
{
    private QIdioma idioma = QIdioma.idioma;

    public Idioma getIdiomaByCodigoISO(String codigoISO)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(idioma).where(idioma.codigoISO.eq(codigoISO.toLowerCase()))
                .uniqueResult(idioma);
    }
}
