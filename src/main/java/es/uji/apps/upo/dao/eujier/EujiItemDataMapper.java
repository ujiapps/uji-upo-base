package es.uji.apps.upo.dao.eujier;

import es.uji.apps.upo.model.eujier.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EujiItemDataMapper
{
    public EujierItem favoritoToItem(EujierItemFavorito favorito)
    {
        EujierItem item = new EujierItem();

        item.setId(favorito.getId());
        item.setNombre(favorito.getNombre());
        item.setUrl(favorito.getUrl());
        item.setUrlProcedimiento(favorito.getUrlProcedimiento());
        item.setDescripcion(favorito.getDescripcion());
        item.setFavorito(favorito.getFavorito());

        return item;
    }

    public List<EujierItem> favoritosToItems(List<EujierItemFavorito> favoritos)
    {
        List<EujierItem> items = new ArrayList<>();

        for (EujierItemFavorito favorito : favoritos)
        {
            items.add(favoritoToItem(favorito));
        }

        return items;
    }

    public EujierItem destacadoToItem(EujierItemDestacado destacado, String idioma)
    {
        EujierItem item = new EujierItem();

        item.setId(destacado.getId());
        item.setUrl(destacado.getUrl());
        item.setUrlProcedimiento(destacado.getUrlProcedimiento());
        item.setFavorito(destacado.getFavorito());
        item.setNombre(getField(idioma, destacado.getNombreCA(), destacado.getNombreES(), destacado.getNombreEN()));
        item.setDescripcion(getField(idioma, destacado.getDescripcionCA(), destacado.getDescripcionES(),
                destacado.getDescripcionEN()));

        return item;
    }

    public List<EujierItem> destacadosToItems(List<EujierItemDestacado> destacados, String idioma)
    {
        List<EujierItem> items = new ArrayList<>();

        for (EujierItemDestacado destacado : destacados)
        {
            items.add(destacadoToItem(destacado, idioma));
        }

        return items;
    }

    public EujierItem grupoItemToItem(EujierGrupoItem gItem, String idioma)
    {
        EujierItem item = new EujierItem();

        item.setNombre(getField(idioma, gItem.getNombreItemCA(), gItem.getNombreItemES(), gItem.getNombreItemEN()));
        item.setDescripcion(getField(idioma, gItem.getDescripcionItemCA(), gItem.getDescripcionItemES(),
                gItem.getDescripcionItemEN()));

        item.setId(gItem.getItemId());
        item.setUrl(gItem.getUrl());
        item.setFavorito(gItem.getFavorito());

        return item;
    }

    public List<EujierItem> gruposItemsToItems(List<EujierGrupoItem> grupos, String idioma)
    {
        List<EujierItem> items = new ArrayList<>();

        for (EujierGrupoItem grupo : grupos)
        {
            items.add(grupoItemToItem(grupo, idioma));
        }

        return items;
    }

    public List<EujierItem> itemsBusquedaToItems(List<EujierItemBusqueda> itemsBusqueda, String idioma)
    {
        List<EujierItem> items = new ArrayList<>();

        for (EujierItemBusqueda itemBusqueda : itemsBusqueda)
        {
            items.add(itemBusquedaToItem(itemBusqueda, idioma));
        }

        return items;
    }

    public EujierItem itemBusquedaToItem(EujierItemBusqueda itemBusqueda, String idioma)
    {
        EujierItem item = new EujierItem();

        item.setNombre(
                getField(idioma, itemBusqueda.getNombreCA(), itemBusqueda.getNombreES(), itemBusqueda.getNombreEN()));
        item.setDescripcion(getField(idioma, itemBusqueda.getDescripcionCA(), itemBusqueda.getDescripcionES(),
                itemBusqueda.getDescripcionEN()));

        item.setId(itemBusqueda.getId());
        item.setUrl(itemBusqueda.getUrl());
        item.setUrlProcedimiento(itemBusqueda.getUrlProcedimiento());
        item.setFavorito(itemBusqueda.getFavorito());

        return item;
    }

    public EujierItem itemMasUsadoToItem(EujierItemMasUsado itemMasUsado, String idioma)
    {
        EujierItem item = new EujierItem();

        item.setNombre(
                getField(idioma, itemMasUsado.getNombreCA(), itemMasUsado.getNombreES(), itemMasUsado.getNombreEN()));
        item.setDescripcion(getField(idioma, itemMasUsado.getDescripcionCA(), itemMasUsado.getDescripcionES(),
                itemMasUsado.getDescripcionEN()));

        item.setId(itemMasUsado.getId());
        item.setUrl(itemMasUsado.getUrl());
        item.setUrlProcedimiento(itemMasUsado.getUrlProcedimiento());
        item.setFavorito(itemMasUsado.getFavorito());

        return item;
    }

    public List<EujierItem> itemsMasUsadosToItems(List<EujierItemMasUsado> itemsMasUsados, String idioma)
    {
        List<EujierItem> items = new ArrayList<>();

        for (EujierItemMasUsado itemMasUsado : itemsMasUsados)
        {
            items.add(itemMasUsadoToItem(itemMasUsado, idioma));
        }

        return items;
    }

    public EujierItem itemMasUsadoPerfilToItem(EujierItemMasUsadoPerfil itemMasUsadoPerfil, String idioma)
    {
        EujierItem item = new EujierItem();

        item.setNombre(getField(idioma, itemMasUsadoPerfil.getNombreCA(), itemMasUsadoPerfil.getNombreES(),
                itemMasUsadoPerfil.getNombreEN()));
        item.setDescripcion(
                getField(idioma, itemMasUsadoPerfil.getDescripcionCA(), itemMasUsadoPerfil.getDescripcionES(),
                        itemMasUsadoPerfil.getDescripcionEN()));

        item.setId(itemMasUsadoPerfil.getId());
        item.setUrl(itemMasUsadoPerfil.getUrl());
        item.setUrlProcedimiento(itemMasUsadoPerfil.getUrlProcedimiento());
        item.setFavorito(itemMasUsadoPerfil.getFavorito());

        return item;
    }

    public List<EujierItem> itemsMasUsadosPerfilToItems(List<EujierItemMasUsadoPerfil> itemsMasUsadosPerfil,
            String idioma)
    {
        List<EujierItem> items = new ArrayList<>();

        for (EujierItemMasUsadoPerfil itemMasUsadoPerfil : itemsMasUsadosPerfil)
        {
            items.add(itemMasUsadoPerfilToItem(itemMasUsadoPerfil, idioma));
        }

        return items;
    }

    public EujierItem completa(EujierItem item, String idioma)
    {
        item.setNombre(getField(idioma, item.getNombreCA(), item.getNombreES(), item.getNombreEN()));
        item.setDescripcion(
                getField(idioma, item.getDescripcionCA(), item.getDescripcionES(), item.getDescripcionEN()));

        return item;
    }

    public List<EujierItem> completa(List<EujierItem> items, String idioma)
    {
        for (EujierItem item : items)
        {
            completa(item, idioma);
        }

        return items;
    }

    public String getField(String idioma, String nombreCA, String nombreES, String nombreEN)
    {
        if ("es".equalsIgnoreCase(idioma))
        {
            return nombreES;
        }

        if ("en".equalsIgnoreCase(idioma))
        {
            return nombreEN;
        }

        return nombreCA;
    }

    public List<EujierItemAplicacion> itemsUbicacionesToItemsAplicaciones(List<EujierItemUbicacion> itemsUbicaciones)
    {
        List<EujierItemAplicacion> itemsAplicaciones = new ArrayList<>();

        for (EujierItemUbicacion itemUbicacion : itemsUbicaciones)
        {
            EujierItemAplicacion itemAplicacion = new EujierItemAplicacion();

            itemAplicacion.setNombre(itemUbicacion.getNombre());
            itemAplicacion.setAplicacionId(itemUbicacion.getAplicacionId());
            itemAplicacion.setRegionId(itemUbicacion.getRegionId());
            itemAplicacion.setCamino(itemUbicacion.getCamino());

            itemsAplicaciones.add(itemAplicacion);
        }

        return itemsAplicaciones;
    }
}
