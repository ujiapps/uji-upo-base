package es.uji.apps.upo.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.upo.model.Persona;
import es.uji.apps.upo.model.enums.EstadoModeracion;
import es.uji.apps.upo.model.ModeracionLote;
import es.uji.apps.upo.model.QModeracionLote;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class ModeracionLoteDAO extends BaseDAODatabaseImpl
{
    private QModeracionLote moderacionLote = QModeracionLote.moderacionLote;

    public List<ModeracionLote> getElementosPendientesModeracion()
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(moderacionLote)
                .where(moderacionLote.estadoModeracion.eq(EstadoModeracion.PENDIENTE))
                .list(moderacionLote);
    }

    public void marcaLoteComoAceptado(Long moderacionLoteId, Long userId)
    {
        updateModeracionEstadoLote(moderacionLoteId, EstadoModeracion.ACEPTADO, null, userId);
    }

    public void marcaLoteComoRechazado(Long moderacionLoteId, String motivoRechazo, Long userId)
    {
        updateModeracionEstadoLote(moderacionLoteId, EstadoModeracion.RECHAZADO, motivoRechazo, userId);
    }

    @Transactional
    private void updateModeracionEstadoLote(Long moderacionLoteId,
                                            EstadoModeracion estadoModeracion, String motivoRechazo, Long userId)
    {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager,
                moderacionLote);

        updateClause.set(moderacionLote.estadoModeracion, estadoModeracion);
        updateClause.set(moderacionLote.textoRechazo, motivoRechazo);
        updateClause.set(moderacionLote.upoExtPersonaModeracion, new Persona(userId));

        updateClause.where(moderacionLote.estadoModeracion.eq(EstadoModeracion.PENDIENTE)
                .and(moderacionLote.id.eq(moderacionLoteId))).execute();
    }

    public List<ModeracionLote> getElementosPendientesProcesamiento()
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(moderacionLote)
                .where(moderacionLote.estadoModeracion.eq(EstadoModeracion.ACEPTADO)
                        .and(moderacionLote.fechaFinMigracion.isNull()))
                .list(moderacionLote);
    }
}