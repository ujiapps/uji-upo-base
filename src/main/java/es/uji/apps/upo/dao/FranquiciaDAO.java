package es.uji.apps.upo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.upo.model.Franquicia;
import es.uji.apps.upo.model.FranquiciaAcceso;
import es.uji.apps.upo.model.Persona;
import es.uji.apps.upo.model.QFranquicia;
import es.uji.apps.upo.model.QFranquiciaAcceso;
import es.uji.apps.upo.model.enums.TipoFranquiciaAcceso;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class FranquiciaDAO extends BaseDAODatabaseImpl
{
    QFranquiciaAcceso franquiciaAcceso = QFranquiciaAcceso.franquiciaAcceso;
    QFranquicia qFranquicia = QFranquicia.franquicia;

    @Transactional
    public void addAdministrador(Long franquiciaId, Long personaId, TipoFranquiciaAcceso tipoPermiso)
    {
        FranquiciaAcceso upoFranquiciasAcceso = new FranquiciaAcceso();
        Persona upoExtPersona = new Persona();
        upoExtPersona.setId(personaId);
        Franquicia upoFranquicia = new Franquicia();
        upoFranquicia.setId(franquiciaId);

        upoFranquiciasAcceso.setTipo(tipoPermiso);
        upoFranquiciasAcceso.setUpoExtPersona(upoExtPersona);
        upoFranquiciasAcceso.setUpoFranquicia(upoFranquicia);

        insert(upoFranquiciasAcceso);
    }

    @Transactional
    public void deleteAdministrador(Long franquiciaId, Long personaId)
    {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, franquiciaAcceso);

        deleteClause.where(franquiciaAcceso.upoExtPersona.id.eq(personaId),
                franquiciaAcceso.upoFranquicia.id.eq(franquiciaId)).execute();
    }

    public List<Franquicia> getFranquiciaByOrder(Class<Franquicia> class1)
    {
        QFranquicia franquicia = QFranquicia.franquicia;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(franquicia).leftJoin(franquicia.origenInformacion).fetch().orderBy(franquicia.nombre.asc());

        return query.list(franquicia);
    }

    public void addAdministradoresFromOtherFranquicia(Long franquiciaDestinoId, Long franquiciaOrigenId)
    {
        List<FranquiciaAcceso> permisosOrigen = get(FranquiciaAcceso.class, "upoFranquicia =" + franquiciaOrigenId);

        List<FranquiciaAcceso> permisosDestino = get(FranquiciaAcceso.class, "upoFranquicia =" + franquiciaDestinoId);

        Franquicia franquiciaDestino = get(Franquicia.class, franquiciaDestinoId).get(0);

        for (FranquiciaAcceso permiso : permisosOrigen)
        {
            if (!existePermiso(permiso, permisosDestino))
            {
                FranquiciaAcceso permisoNuevo = new FranquiciaAcceso();

                permisoNuevo.setUpoFranquicia(franquiciaDestino);
                permisoNuevo.setTipo(permiso.getTipo());
                permisoNuevo.setUpoExtPersona(permiso.getUpoExtPersona());

                insert(permisoNuevo);
            }
        }
    }

    private Boolean existePermiso(FranquiciaAcceso permiso, List<FranquiciaAcceso> permisos)
    {
        for (FranquiciaAcceso permisoLista : permisos)
        {
            if (permiso.getUpoExtPersona().getId().equals(permisoLista.getUpoExtPersona().getId()))
            {
                return true;
            }
        }

        return false;
    }

    public Boolean hasFranquicia(Long userId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(franquiciaAcceso).where(franquiciaAcceso.upoExtPersona.id.eq(userId));

        if (query.list(franquiciaAcceso).size() > 0)
        {
            return true;
        }

        return false;
    }

    public Franquicia getFranquiciaById(Long franquiciaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qFranquicia)
                .leftJoin(qFranquicia.origenInformacion)
                .fetch()
                .where(qFranquicia.id.eq(franquiciaId))
                .uniqueResult(qFranquicia);
    }

    public List<FranquiciaAcceso> getAdministradores(Long franquiciaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(franquiciaAcceso)
                .where(franquiciaAcceso.upoFranquicia.id.eq(franquiciaId))
                .join(franquiciaAcceso.upoFranquicia)
                .fetch()
                .leftJoin(franquiciaAcceso.upoExtPersona)
                .fetch()
                .list(franquiciaAcceso);
    }
}
