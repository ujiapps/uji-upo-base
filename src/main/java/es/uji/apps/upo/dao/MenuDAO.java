package es.uji.apps.upo.dao;

import java.util.List;

import es.uji.apps.upo.model.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class MenuDAO extends BaseDAODatabaseImpl
{
    private QMenuGrupo menuGrupo = QMenuGrupo.menuGrupo;
    private QMenu menu = QMenu.menu;

    public List<Menu> getByPersonaId(Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        return query.from(menu).where(menu.persona.id.eq(connectedUserId)).orderBy(menu.nombre.asc()).list(menu);
    }

    public List<Menu> getAll()
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        return query.from(menu).orderBy(menu.nombre.asc()).list(menu);
    }

    @Transactional
    public void addGrupo(Long menuId, Long grupoId, Integer orden)
    {
        MenuGrupo menuGrupo = new MenuGrupo();
        Menu menu = new Menu();
        Grupo grupo = new Grupo();

        menu.setId(menuId);
        grupo.setId(grupoId);

        menuGrupo.setMapasMenu(menu);
        menuGrupo.setGrupo(grupo);
        menuGrupo.setOrden(orden);

        insert(menuGrupo);
    }

    @Transactional
    public void deleteGrupo(Long menuId, Long grupoId)
    {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, menuGrupo);

        deleteClause.where(menuGrupo.grupo.id.eq(grupoId), menuGrupo.menu.id.eq(menuId)).execute();
    }

    public List<MenuGrupo> getByMenu(Long menuId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(menuGrupo).where(menuGrupo.menu.id.eq(menuId)).orderBy(menuGrupo.orden.asc());

        return query.list(menuGrupo);
    }

    @Transactional
    public void updateGrupo(long menuId, Long grupoId, Integer orden)
    {
        MenuGrupo menuGrupo = getMenuGrupoByMenuIdAndGrupoId(menuId, grupoId);

        if (menuGrupo != null)
        {
            menuGrupo.setOrden(orden);
        }

        update(menuGrupo);
    }

    private MenuGrupo getMenuGrupoByMenuIdAndGrupoId(Long menuId, Long grupoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(menuGrupo).where(menuGrupo.menu.id.eq(menuId).and(menuGrupo.grupo.id.eq(grupoId)));

        List<MenuGrupo> menuGrupos = query.list(menuGrupo);

        if (menuGrupos.size() > 0)
        {
            return menuGrupos.get(0);
        }

        return null;
    }
}
