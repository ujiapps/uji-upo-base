package es.uji.apps.upo.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.upo.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.StringUtils;
import org.springframework.stereotype.Repository;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

@Repository
public class ContenidoIdiomaDAO extends BaseDAODatabaseImpl
{
    private QContenidoIdioma qContenidoIdioma = QContenidoIdioma.contenidoIdioma;
    private QBinarioBusqueda qBinarioBusqueda = QBinarioBusqueda.binarioBusqueda;

    public List<BinarioBusqueda> getBinariosBySearchAndUrlCompleta(ParametrosBusquedaBinarios parametros,
            Boolean imagenes)
    {
        JPAQuery query = generaQuery(parametros, imagenes);

        query.offset(parametros.getStart()).limit(parametros.getLimit());

        List<BinarioBusqueda> results =
                query.orderBy(qBinarioBusqueda.urlNumNiveles.asc(), qBinarioBusqueda.ficheroNombre.asc())
                        .list(qBinarioBusqueda);

        return results;
    }

    public Long getNumBinariosBySearchAndUrlCompleta(ParametrosBusquedaBinarios parametros, Boolean imagenes)
    {
        JPAQuery query = generaQuery(parametros, imagenes);

        return query.count();
    }

    public JPAQuery generaQuery(ParametrosBusquedaBinarios parametros, Boolean imagenes)
    {
        JPAQuery query = new JPAQuery(entityManager);

        parametros.setQuery(StringUtils.limpiaAcentos(parametros.getQuery().toLowerCase()));

        query.from(qBinarioBusqueda)
                .where(qBinarioBusqueda.mapaUrlCompleta.like(parametros.getUrlNode() + "%")
                        .and(qBinarioBusqueda.idiomaCodigoIso.eq(parametros.getIdioma().toLowerCase()))
                        .and((qBinarioBusqueda.nombreFicheroSinAcentos.lower().like("%" + parametros.getQuery() + "%")
                                .or(qBinarioBusqueda.resumenSinAcentos.lower().like("%" + parametros.getQuery() + "%"))
                                .or(qBinarioBusqueda.subtituloSinAcentos.lower().like("%" + parametros.getQuery() + "%"))
                                .or(qBinarioBusqueda.tituloSinAcentos.lower().like("%" + parametros.getQuery() + "%"))
                                .or(qBinarioBusqueda.tituloLargoSinAcentos.lower().like("%" + parametros.getQuery() + "%")))));

        if (imagenes)
        {
            query.where(qBinarioBusqueda.ficheroMimeType.like("%image%"));
        }
        else
        {
            query.where(qBinarioBusqueda.ficheroMimeType.notLike("%image%"));
        }

        return query;
    }

    public InputStream getStreamById(Long id)
    {
        JPAQuery query = new JPAQuery(entityManager);

        ContenidoIdioma contenidoIdioma =
                query.from(qContenidoIdioma).where(qContenidoIdioma.id.eq(id)).uniqueResult(qContenidoIdioma);

        if (contenidoIdioma.getContenido() == null) return null;

        return new ByteArrayInputStream(contenidoIdioma.getContenido());
    }
}
