package es.uji.apps.upo.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.upo.model.PrioridadContenido;
import es.uji.apps.upo.model.QPrioridadContenido;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PrioridadContenidoDAO extends BaseDAODatabaseImpl
{
    private QPrioridadContenido prioridadContenido = QPrioridadContenido.prioridadContenido;

    public List<PrioridadContenido> getPrioridadesByContenido(Long contenidoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<PrioridadContenido> prioridades = query.from(prioridadContenido)
                .where(prioridadContenido.upoObjeto.id.eq(contenidoId))
                .list(prioridadContenido);

        return prioridades;
    }

    public PrioridadContenido getPrioridadByIdAndContenido(Long contenidoId, Long prioridadId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        PrioridadContenido prioridad = query.from(prioridadContenido)
                .where(prioridadContenido.upoObjeto.id.eq(contenidoId).and(prioridadContenido.id.eq(prioridadId)))
                .uniqueResult(prioridadContenido);

        return prioridad;
    }

    public void deletePrioridadesByContenidoId(Long contenidoId)
    {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, prioridadContenido);

        deleteClause.where(prioridadContenido.upoObjeto.id.eq(contenidoId)).execute();
    }
}
