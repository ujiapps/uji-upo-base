package es.uji.apps.upo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import es.uji.apps.upo.model.Criterio;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CriterioDAO extends BaseDAODatabaseImpl
{
    public List<Criterio> getCriterios()
    {
        return get(Criterio.class);
    }
}
