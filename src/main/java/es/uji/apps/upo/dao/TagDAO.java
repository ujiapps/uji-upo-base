package es.uji.apps.upo.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.upo.model.QContenidoTag;
import es.uji.apps.upo.model.QNodoMapaTag;
import es.uji.apps.upo.model.QTag;
import es.uji.apps.upo.model.Tag;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class TagDAO extends BaseDAODatabaseImpl {
    private QContenidoTag contenidoTag = QContenidoTag.contenidoTag;
    private QNodoMapaTag nodoMapaTag = QNodoMapaTag.nodoMapaTag;
    private QTag tag = QTag.tag;

    @Transactional
    public void deleteTagsByContenido(Long contenidoId) {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, contenidoTag);

        deleteClause.where(contenidoTag.upoObjeto.id.eq(contenidoId)).execute();
    }

    public List<Tag> getAllTags() {
        JPAQuery query = new JPAQuery(this.entityManager);

        return query.from(tag)
                .orderBy(tag.tagname.asc())
                .list(tag);
    }

    public boolean existsTag(String name) {
        JPAQuery query = new JPAQuery(this.entityManager);

        return query.from(tag)
                .where(tag.tagname.eq(name))
                .exists();
    }

    public List<String> getTagsAsChannels() {
        JPAQuery query = new JPAQuery(this.entityManager);

        return query.from(nodoMapaTag)
                .distinct()
                .orderBy(nodoMapaTag.tag.asc())
                .list(nodoMapaTag.tag);
    }

    public Tag getById(Long id) {
        JPAQuery query = new JPAQuery(this.entityManager);

        return query.from(tag)
                .where(tag.id.eq(id))
                .singleResult(tag);
    }

    public void updateAllValuesOnObjetoTag(String oldName, String newName) {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, contenidoTag);

        updateClause.where(contenidoTag.tagname.eq(oldName))
                .set(contenidoTag.tagname, newName)
                .execute();
    }
}
