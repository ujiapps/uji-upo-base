package es.uji.apps.upo.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.upo.model.metadatos.AtributoMetadato;
import es.uji.apps.upo.model.metadatos.QAtributoMetadato;
import es.uji.apps.upo.model.metadatos.QValorMetadato;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AtributoMetadatoDAO extends BaseDAODatabaseImpl
{
    private QAtributoMetadato qAtributoMetadato = QAtributoMetadato.atributoMetadato;

    public List<AtributoMetadato> getByEsquemaId(Long esquemaId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        query.from(qAtributoMetadato)
                .where(qAtributoMetadato.esquemaMetadato.id.eq(esquemaId))
                .leftJoin(qAtributoMetadato.valoresMetadatos)
                .fetch();

        return query.distinct().orderBy(qAtributoMetadato.orden.asc()).list(qAtributoMetadato);
    }

    public AtributoMetadato getById(Long atributoId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        query.from(qAtributoMetadato)
                .where(qAtributoMetadato.id.eq(atributoId))
                .leftJoin(qAtributoMetadato.valoresMetadatos)
                .fetch();

        return query.list(qAtributoMetadato).get(0);
    }

    public void deleteAtributosValoresByAtributoIdAndValoresMetadatos(Long id, List<Long> idValoresMetadatos)
    {
        QValorMetadato qValorMetadato = QValorMetadato.valorMetadato;

        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qValorMetadato).where(
                qValorMetadato.atributoMetadato.id.eq(id).and(qValorMetadato.id.notIn(idValoresMetadatos)));

        deleteClause.execute();
    }
}
