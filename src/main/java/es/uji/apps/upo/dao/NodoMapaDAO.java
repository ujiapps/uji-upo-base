package es.uji.apps.upo.dao;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.TipoReferenciaContenido;
import es.uji.apps.upo.utils.UrlUtils;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;

@Repository
public class NodoMapaDAO extends BaseDAODatabaseImpl
{
    private static final int ID_FOR_GET_ROOT_NODE = -1;
    private QNodoMapa qNodoMapa = QNodoMapa.nodoMapa;
    private QNodoMapaContenido qNodoMapaContenido = QNodoMapaContenido.nodoMapaContenido;
    private QContenido qContenido = QContenido.contenido;
    private QPlantilla qPlantilla = QPlantilla.plantilla;
    private QNodoMapaPlantilla qNodoMapaPlantilla = QNodoMapaPlantilla.nodoMapaPlantilla;
    private QIdioma qIdioma = QIdioma.idioma;
    private QFranquicia qFranquicia = QFranquicia.franquicia;
    private QFranquiciaAcceso qFranquiciaAcceso = QFranquiciaAcceso.franquiciaAcceso;
    private QNodoMapaTag qNodoMapaTag = QNodoMapaTag.nodoMapaTag;

    public List<NodoMapa> getHijos(Long nodoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        if (nodoId == ID_FOR_GET_ROOT_NODE)
        {
            query.from(qNodoMapa).where(qNodoMapa.upoMapa.id.isNull());
        } else
        {
            query.from(qNodoMapa).where(qNodoMapa.upoMapa.id.eq(nodoId));
        }

        return query.orderBy(qNodoMapa.orden.asc(), qNodoMapa.urlPath.asc()).list(qNodoMapa);
    }

    public List<NodoMapa> getDescendientes(String urlBase)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qNodoMapa)
                .where(qNodoMapa.urlCompleta.like(urlBase + '%'))
                .leftJoin(qNodoMapa.upoMapasPlantillas)
                .fetch()
                .leftJoin(qNodoMapa.upoMapasIdiomasExclusiones)
                .fetch();

        return query.orderBy(qNodoMapa.urlCompleta.asc()).distinct().list(qNodoMapa);
    }

    public List<Plantilla> getPlantillasByMapaId(Long mapaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QNodoMapaPlantilla upoMapasPlantilla = new QNodoMapaPlantilla("mp");

        return query.from(qPlantilla)
                .join(qPlantilla.upoMapasPlantillas, upoMapasPlantilla)
                .where(upoMapasPlantilla.upoMapa.id.eq(mapaId))
                .list(qPlantilla);
    }

    @Transactional
    public void insertarPlantilla(Long mapaId, Long plantillaId, Integer nivel)
    {
        Plantilla upoPlantilla = new Plantilla();
        upoPlantilla.setId(plantillaId);
        NodoMapa upoMapa = new NodoMapa();
        upoMapa.setId(mapaId);

        NodoMapaPlantilla upoMapasPlantilla = new NodoMapaPlantilla();
        upoMapasPlantilla.setUpoMapa(upoMapa);
        upoMapasPlantilla.setUpoPlantilla(upoPlantilla);
        upoMapasPlantilla.setNivel(nivel);

        insert(upoMapasPlantilla);
    }

    @Transactional
    public void deletePlantilla(Long mapaId, Long plantillaId)
    {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qNodoMapaPlantilla);

        deleteClause.where(qNodoMapaPlantilla.upoMapa.id.eq(mapaId), qNodoMapaPlantilla.upoPlantilla.id.eq(plantillaId))
                .execute();
    }

    public List<Idioma> getIdiomasObligatorios(Long nodoId)
    {
        QNodoMapaExclusionIdioma exclusionIdioma = QNodoMapaExclusionIdioma.nodoMapaExclusionIdioma;

        JPAQuery query = new JPAQuery(entityManager);
        JPAQuery query2 = new JPAQuery(entityManager);

        List<Long> idiomasExcluyentesIds = query.from(exclusionIdioma)
                .where(exclusionIdioma.upoMapa.id.eq(nodoId))
                .list(exclusionIdioma.upoIdioma.id);

        List<Idioma> idiomasObligatorios;

        if (idiomasExcluyentesIds.size() > 0)
        {
            idiomasObligatorios = query.from(qIdioma).where(qIdioma.id.notIn(idiomasExcluyentesIds)).list(qIdioma);
        } else
        {
            idiomasObligatorios =
                    query2.from(qIdioma).where(qIdioma.codigoISO.toUpperCase().in("CA", "ES")).list(qIdioma);
        }

        return idiomasObligatorios;
    }

    public List<NodoMapa> getNodoMapaById(Long mapaId)
    {
        return this.get(NodoMapa.class, mapaId);
    }

    public NodoMapa getNodoMapaByUrlCompleta(String urlCompleta)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qNodoMapa).where(qNodoMapa.urlCompleta.eq(urlCompleta)).uniqueResult(qNodoMapa);
    }

    public Boolean isNodoMapaAdmin(String urlCompleta, Long perId)
    {
        if (urlCompleta == null || perId == null)
        {
            return false;
        }

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qNodoMapa)
                .join(qNodoMapa.upoFranquicia, qFranquicia)
                .join(qFranquicia.upoFranquiciasAccesos, qFranquiciaAcceso)
                .where(qFranquiciaAcceso.upoExtPersona.id.eq(perId));

        BooleanBuilder builder = new BooleanBuilder();

        for (String url : UrlUtils.convertStringToArray(urlCompleta))
        {
            builder.or(qNodoMapa.urlCompleta.eq(url));
        }

        builder.or(qNodoMapa.urlCompleta.eq("/"));

        query.where(builder);

        return (query.count() > 0);
    }

    public Boolean isNodoMapaLocked(String urlCompleta)
    {
        if (urlCompleta == null)
        {
            return false;
        }

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qNodoMapa).where(qNodoMapa.bloqueado.isTrue());

        BooleanBuilder builder = new BooleanBuilder();

        for (String url : UrlUtils.convertStringToArray(urlCompleta))
        {
            builder.or(qNodoMapa.urlCompleta.eq(url));
        }

        builder.or(qNodoMapa.urlCompleta.eq("/"));

        query.where(builder);

        return (query.count() > 0);
    }

    @Transactional
    public void actualizarUrlCompletaNodosHijoPorRenombrado(String urlBase, String urlNueva)
    {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qNodoMapa);

        updateClause.where(qNodoMapa.urlCompleta.like(urlBase + '%'))
                .set(qNodoMapa.urlCompleta, qNodoMapa.urlCompleta.substring(urlBase.length()).prepend(urlNueva))
                .execute();
    }

    @Transactional
    public void deleteByNodoMapaId(Long mapaId)
    {
        delete(NodoMapa.class, mapaId);
    }

    @Transactional
    public void actualizarUrlCompletaNodosHijoPorDragAndDrop(String urlBase, String urlBaseSinNodo, String nuevaUrlBase)
    {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qNodoMapa);

        updateClause.where(qNodoMapa.urlCompleta.like(urlBase + '%'))
                .set(qNodoMapa.urlCompleta,
                        qNodoMapa.urlCompleta.substring(urlBaseSinNodo.length()).prepend(nuevaUrlBase))
                .execute();
    }

    @Transactional
    public void updateMenuEnCascada(NodoMapa nodoMapa, Menu menu)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<NodoMapa> nodoMapasQueNoHeredan = query.from(qNodoMapa)
                .where(qNodoMapa.urlCompleta.like(nodoMapa.getUrlCompleta() + '%')
                        .and(qNodoMapa.urlCompleta.ne(nodoMapa.getUrlCompleta()))
                        .and(qNodoMapa.menuHeredado.eq(false)))
                .list(qNodoMapa);

        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qNodoMapa);

        updateClause.where(qNodoMapa.urlCompleta.like(nodoMapa.getUrlCompleta() + '%')).set(qNodoMapa.menu, menu);

        BooleanBuilder builder = new BooleanBuilder();

        for (NodoMapa nodoMapaAExcluir : nodoMapasQueNoHeredan)
        {
            builder.andNot(qNodoMapa.urlCompleta.like(nodoMapaAExcluir.getUrlCompleta() + "%"));
        }

        updateClause.where(builder);

        updateClause.execute();
    }

    @Transactional
    public void insertarDiasDelMesComoNodoMapas(NodoMapa nodoMapa, String mes, String anyo, Long plantillaId,
                                                Integer plantillaNivel)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Integer.parseInt(anyo), Integer.parseInt(mes) - 1, 1);

        StringBuilder subSelect = new StringBuilder();
        StringBuilder select = new StringBuilder();

        for (Integer it = 1; it <= calendar.getActualMaximum(Calendar.DAY_OF_MONTH); it++)
        {
            String dia = it.toString();
            String diaFormateado = (dia.length() == 2) ? dia : '0' + dia;
            String urlCompleta = nodoMapa.getUrlCompleta() + diaFormateado + "/";
            Long menuId = nodoMapa.getMenu().getId();
            Long franquiciaId = nodoMapa.getUpoFranquicia().getId();

            if (subSelect != null && subSelect.length() > 0)
            {
                subSelect.append(" union ");
            }

            subSelect.append("select ");
            subSelect.append(nodoMapa.getId());
            subSelect.append(" a, '");
            subSelect.append(diaFormateado);
            subSelect.append("' b, '");
            subSelect.append(urlCompleta);
            subSelect.append("' c, ");
            subSelect.append(menuId);
            subSelect.append(" d, ");
            subSelect.append(franquiciaId);
            subSelect.append(" e, 1 f,  ");
            subSelect.append(it.toString());
            subSelect.append(" g from dual ");
        }

        select.append(
                "insert into upo_mapas (id, mapa_id, url_path, url_completa, menu_id, franquicia_id, menu_heredado, orden) ");

        select.append("select hibernate_sequence.nextval, s.* from (");
        select.append(subSelect);
        select.append(") s ");

        entityManager.createNativeQuery(select.toString()).executeUpdate();

        insertNodoMapaPlantilla(nodoMapa, plantillaId, plantillaNivel);
    }

    @Transactional
    private void insertNodoMapaPlantilla(NodoMapa nodoMapa, Long plantillaId, Integer plantillaNivel)
    {
        if (plantillaId != null && plantillaNivel != null)
        {
            StringBuilder select = new StringBuilder();

            select.append("insert into upo_mapas_plantillas (id, plantilla_id, mapa_id, nivel) ");
            select.append("select hibernate_sequence.nextval, s.* from (");
            select.append("select ");
            select.append(plantillaId);
            select.append(" plantilla_id, id mapa_id, ");
            select.append(plantillaNivel);
            select.append(" nivel from upo_mapas where url_completa like ");
            select.append("'");
            select.append(nodoMapa.getUrlCompleta());
            select.append("_%'");
            select.append(") s ");

            System.out.println(select.toString());

            entityManager.createNativeQuery(select.toString()).executeUpdate();
        }
    }

    public List<NodoMapa> getNodoMapaAndRelations(Long nodoMapaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QNodoMapa mapa = QNodoMapa.nodoMapa;
        QNodoMapaContenido mapaContenido = QNodoMapaContenido.nodoMapaContenido;

        query.from(mapa)
                .where(mapa.id.eq(nodoMapaId))
                .leftJoin(mapa.upoMapasObjetos, mapaContenido)
                .fetch()
                .leftJoin(mapa.upoMapasPlantillas)
                .fetch()
                .leftJoin(mapa.upoMapasIdiomasExclusiones)
                .fetch();

        return query.list(mapa);
    }

    public boolean hasNodosMapaHijos(NodoMapa nodoMapa)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qNodoMapa).where(qNodoMapa.upoMapa.id.eq(nodoMapa.getId()));

        return (query.count() > 0);
    }

    @Transactional
    public void actualizarOrdenNodos(Long idPadre, List<NodoMapa> nodosMapaAActualizar, Long orden)
    {
        if (nodosMapaAActualizar == null || nodosMapaAActualizar.isEmpty())
        {
            return;
        }

        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qNodoMapa);
        updateClause.where(qNodoMapa.upoMapa.id.eq(idPadre).and(qNodoMapa.in(nodosMapaAActualizar)))
                .set(qNodoMapa.orden, qNodoMapa.orden.add(1L))
                .execute();

        Long hueco = ordenMinimoNodosAActualizar(nodosMapaAActualizar) - orden;

        JPAUpdateClause defragmentaClause = new JPAUpdateClause(entityManager, qNodoMapa);
        defragmentaClause.where(qNodoMapa.upoMapa.id.eq(idPadre).and(qNodoMapa.in(nodosMapaAActualizar)))
                .set(qNodoMapa.orden, qNodoMapa.orden.subtract(hueco))
                .execute();
    }

    private Long ordenMinimoNodosAActualizar(List<NodoMapa> nodosMapaAActualizar)
    {
        Long result = Long.MAX_VALUE;

        for (NodoMapa current : nodosMapaAActualizar)
        {
            if (current.getOrden() < result)
            {
                result = current.getOrden();
            }
        }

        return result;
    }

    public Long getNewOrden(NodoMapa nodoMapaPadre)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QNodoMapa mapa = QNodoMapa.nodoMapa;

        mapa.orden.max();

        query.from(mapa).where(mapa.upoMapa.id.eq(nodoMapaPadre.getId()));

        Long result = query.list(mapa.orden.max()).get(0);

        if (result != null)
        {
            return result + 1L;
        }

        return 1L;
    }

    public NodoMapa getNodoMapaOfNodoMapaContenidoNormalByContenidoId(Long contenidoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qNodoMapa)
                .join(qNodoMapa.upoMapasObjetos, qNodoMapaContenido)
                .join(qNodoMapaContenido.upoObjeto, qContenido)
                .where(qContenido.id.eq(contenidoId).and(qNodoMapaContenido.tipo.eq(TipoReferenciaContenido.NORMAL)))
                .uniqueResult(qNodoMapa);
    }

    public List<String> getUrlsPrivadas()
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qNodoMapa).where(qNodoMapa.privado.isTrue()).list(qNodoMapa.urlCompleta);
    }

    @Transactional
    public void desbloquearNodoMapaYDescendientes(String urlCompleta)
    {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qNodoMapa);

        updateClause.where(qNodoMapa.urlCompleta.like(urlCompleta + '%')).set(qNodoMapa.bloqueado, false).execute();
    }

    public List<NodoMapa> getNodosMapaConEtiquetas(List<String> tags)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qNodoMapaTag).where(qNodoMapaTag.tag.in(tags)).distinct().list(qNodoMapaTag.upoMapa);
    }

    public List<NodoMapa> getNodosMapaYaPropuestos(Long nodoMapaAProponerId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qNodoMapa).where(qNodoMapa.propuestaMapaId.eq(nodoMapaAProponerId)
                .and(qNodoMapa.urlCompleta.notLike("/paperera/%"))).list(qNodoMapa);
    }

    public List<NodoMapaTag> getListaTagsByNodosMapaId(List<Long> nodosMapaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qNodoMapaTag).where(qNodoMapaTag.upoMapa.id.in(nodosMapaId)).list(qNodoMapaTag);
    }
}