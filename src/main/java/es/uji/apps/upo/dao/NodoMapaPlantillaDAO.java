package es.uji.apps.upo.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.upo.model.NodoMapaPlantilla;
import es.uji.apps.upo.model.QNodoMapaPlantilla;
import es.uji.apps.upo.model.enums.TipoPlantilla;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class NodoMapaPlantillaDAO extends BaseDAODatabaseImpl
{
    private QNodoMapaPlantilla mapaPlantilla = QNodoMapaPlantilla.nodoMapaPlantilla;

    public List<NodoMapaPlantilla> getNodoMapaPlantillaByNodoMapaIdAndType(
            Long nodoMapaId, TipoPlantilla tipoPlantilla)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(mapaPlantilla).where(mapaPlantilla.upoMapa.id.eq(nodoMapaId)
                .and(mapaPlantilla.upoPlantilla.tipo.eq(tipoPlantilla)))
                .list(mapaPlantilla);
    }

    public List<NodoMapaPlantilla> getNodoMapaPlantillasByNodoMapaId(Long nodoMapaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(mapaPlantilla)
                .where(mapaPlantilla.upoMapa.id.eq(nodoMapaId))
                .list(mapaPlantilla);
    }
}