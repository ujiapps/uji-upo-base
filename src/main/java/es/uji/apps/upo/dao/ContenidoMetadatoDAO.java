package es.uji.apps.upo.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.upo.model.metadatos.ContenidoMetadato;
import es.uji.apps.upo.model.metadatos.QContenidoMetadato;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ContenidoMetadatoDAO extends BaseDAODatabaseImpl
{
    private QContenidoMetadato metadato = QContenidoMetadato.contenidoMetadato;

    public List<ContenidoMetadato> getMetadatosByContenidoId(long contenidoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(metadato).where(metadato.upoObjeto.id.eq(contenidoId)).list(metadato);
    }
}
