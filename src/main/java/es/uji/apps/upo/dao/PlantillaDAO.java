package es.uji.apps.upo.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.TipoPlantilla;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PlantillaDAO extends BaseDAODatabaseImpl
{
    private QPlantilla qPlantilla = QPlantilla.plantilla;
    private QNodoMapaPlantilla qNodoMapaPlantilla = QNodoMapaPlantilla.nodoMapaPlantilla;

    public List<Plantilla> getPlantillaByTipo(String tipoPlantilla)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPlantilla).where(
                qPlantilla.tipo.eq(TipoPlantilla.valueOf(tipoPlantilla.toUpperCase())));

        return query.list(qPlantilla);
    }

    public NodoMapaPlantilla getNodoMapaPlantillaByMapaId(Long plantillaId, Long mapaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qNodoMapaPlantilla).where(
                qNodoMapaPlantilla.upoPlantilla.id.eq(plantillaId).and(
                        qNodoMapaPlantilla.upoMapa.id.eq(mapaId)));

        return query.uniqueResult(qNodoMapaPlantilla);
    }

    public Plantilla getByFileName(String fileName)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPlantilla).where(qPlantilla.fichero.eq(fileName));

        return query.uniqueResult(qPlantilla);
    }

    public List<Plantilla> getPlantillasVigentes()
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qPlantilla).where(qPlantilla.obsoleta.isFalse()).list(qPlantilla);
    }
}
