package es.uji.apps.upo.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.upo.model.NodoMapa;
import es.uji.apps.upo.model.NodoMapaContenido;
import es.uji.apps.upo.model.QFechaEvento;
import es.uji.apps.upo.model.QNodoMapaContenido;
import es.uji.apps.upo.model.enums.TipoReferenciaContenido;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class NodoMapaContenidoDAO extends BaseDAODatabaseImpl
{
    QNodoMapaContenido qNodoMapaContenido = QNodoMapaContenido.nodoMapaContenido;
    QFechaEvento qFechaEvento = QFechaEvento.fechaEvento;

    public NodoMapa getNodoMapaTipoNormalByContenidoId(Long contenidoId)
    {
        if (contenidoId == null)
        {
            return null;
        }

        JPAQuery query = new JPAQuery(entityManager);

        return query
                .from(qNodoMapaContenido)
                .where(qNodoMapaContenido.upoObjeto.id.eq(contenidoId).and(
                        qNodoMapaContenido.tipo.eq(TipoReferenciaContenido.NORMAL)))
                .uniqueResult(qNodoMapaContenido.upoMapa);
    }

    public NodoMapaContenido getNodoMapaContenidoByContenidoIdAndMapaId(Long contenidoId,
                                                                        Long mapaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query
                .from(qNodoMapaContenido)
                .where(qNodoMapaContenido.upoObjeto.id.eq(contenidoId).and(
                        qNodoMapaContenido.upoMapa.id.eq(mapaId))).uniqueResult(qNodoMapaContenido);
    }

    public List<NodoMapaContenido> getNodoMapaContenidoByMapaId(Long mapaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qNodoMapaContenido).where(qNodoMapaContenido.upoMapa.id.eq(mapaId))
                .list(qNodoMapaContenido);
    }

    public NodoMapaContenido getNodoMapaContenidoTipoNormalByContenidoId(Long contenidoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query
                .from(qNodoMapaContenido)
                .where(qNodoMapaContenido.upoObjeto.id.eq(contenidoId).and(
                        qNodoMapaContenido.tipo.eq(TipoReferenciaContenido.NORMAL)))
                .uniqueResult(qNodoMapaContenido);
    }

    public NodoMapaContenido getNodoMapaContenidoAndFechasEvento(Long nodoMapaContenidoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query
                .from(qNodoMapaContenido)
                .leftJoin(qNodoMapaContenido.fechasEvento, qFechaEvento).fetch()
                .where(qNodoMapaContenido.id.eq(nodoMapaContenidoId))
                .uniqueResult(qNodoMapaContenido);
    }

    @Transactional
    public void deleteFechasEvento(Long nodoMapaContenidoId)
    {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qFechaEvento);

        deleteClause.where(qFechaEvento.nodoMapaContenido.id.eq(nodoMapaContenidoId)).execute();
    }

    public List<NodoMapaContenido> getNodosMapaContenidoByContenidoId(Long contenidoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qNodoMapaContenido).where(qNodoMapaContenido.upoObjeto.id.eq(contenidoId))
                .list(qNodoMapaContenido);
    }
}