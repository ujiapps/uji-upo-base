package es.uji.apps.upo.dao;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.QTuple;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.TipoPlantilla;
import es.uji.apps.upo.model.enums.TipoReferenciaContenido;
import es.uji.apps.upo.services.rest.publicacion.model.RequestParams;
import es.uji.apps.upo.utils.UrlUtils;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.web.template.model.Miga;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Types;
import java.util.*;

@Repository
public class PublicacionDAO extends BaseDAODatabaseImpl
{
    private QNodoMapa qNodoMapa = QNodoMapa.nodoMapa;
    private QNodoMapaPlantilla qNodoMapaPlantilla = QNodoMapaPlantilla.nodoMapaPlantilla;
    private QPlantilla qPlantilla = QPlantilla.plantilla;
    private QPublicacion qPublicacion = QPublicacion.publicacion;
    private QPublicacionBinarios qPublicacionBinarios = QPublicacionBinarios.publicacionBinarios;
    private QMenuGrupo qMenuGrupo = QMenuGrupo.menuGrupo;
    private QGrupoItem qGrupoItem = QGrupoItem.grupoItem;
    private ObtenMigas obtenMigas;

    @Autowired
    private DataSource dataSource;

    @PostConstruct
    public void init()
    {
        this.obtenMigas = new ObtenMigas(dataSource);
    }

    public String getMigas(Long mapaId, String idiomaISO)
    {
        try
        {
            return this.obtenMigas.execute(mapaId, idiomaISO);
        }
        catch (Exception e)
        {
            return null;
        }
    }

    private class ObtenMigas extends StoredProcedure
    {
        private static final String SQL = "dame_migas";
        private static final String P_MAPA_ID = "p_mapa_id";
        private static final String P_IDIOMA_ISO = "p_idioma_iso";

        public ObtenMigas(DataSource dataSource)
        {
            setDataSource(dataSource);
            setFunction(true);
            setSql(SQL);

            declareParameter(new SqlOutParameter("response", Types.VARCHAR));
            declareParameter(new SqlParameter(P_MAPA_ID, Types.VARCHAR));
            declareParameter(new SqlParameter(P_IDIOMA_ISO, Types.VARCHAR));

            compile();
        }

        public String execute(Long mapaId, String idiomaISO)
        {
            Map<String, Object> inParams = new HashMap<String, Object>();

            inParams.put(P_MAPA_ID, mapaId);
            inParams.put(P_IDIOMA_ISO, idiomaISO);

            Map<String, Object> results = execute(inParams);

            return (String) results.get("response");
        }
    }

    public String getUrlNodoHeredado(String nombreNodo, String urlCompletaNodoActual)
    {
        if (nombreNodo == null || urlCompletaNodoActual == null)
        {
            return null;
        }

        JPAQuery query = new JPAQuery(entityManager);
        QNodoMapa qNodoMapaAux = new QNodoMapa("qNodoMapaAux");

        query.from(qNodoMapa)
                .where(new JPASubQuery().from(qNodoMapaAux)
                        .where(qNodoMapaAux.urlCompleta.eq(qNodoMapa.urlCompleta.append(nombreNodo + "/")))
                        .exists());

        BooleanBuilder builder = new BooleanBuilder();

        for (String url : UrlUtils.convertStringToArray(urlCompletaNodoActual))
        {
            builder.or(qNodoMapa.urlCompleta.eq(url));
        }

        builder.or(qNodoMapa.urlCompleta.eq("/"));

        List<Tuple> tuples = query.where(builder).list(new QTuple(qNodoMapa.urlCompleta));

        String url = "";

        for (Tuple tuple : tuples)
        {
            String urlCompleta = tuple.get(qNodoMapa.urlCompleta);

            if (url.length() < (urlCompleta + nombreNodo + "/").length())
            {
                url = urlCompleta + nombreNodo + "/";
            }
        }

        return url;
    }

    public NodoMapa getNodoMapaByUrlCompleta(String urlCompleta)
    {
        JPAQuery query = new JPAQuery(entityManager);

        Tuple tuple = query.from(qNodoMapa)
                .where(qNodoMapa.urlCompleta.eq(urlCompleta))
                .uniqueResult(new QTuple(qNodoMapa.id, qNodoMapa.enlaceDestino, qNodoMapa.tituloPublicacionCA,
                        qNodoMapa.tituloPublicacionES, qNodoMapa.tituloPublicacionEN, qNodoMapa.contenidoRelacionado,
                        qNodoMapa.mostrarReloj, qNodoMapa.mostrarMenuPerfilesPlegado,
                        qNodoMapa.mostrarRecursosRelacionados, qNodoMapa.menu, qNodoMapa.paginado,
                        qNodoMapa.mostrarTags, qNodoMapa.mostrarFechaModificacion, qNodoMapa.mostrarFuente,
                        qNodoMapa.mostrarRedesSociales, qNodoMapa.mostrarInformacionProporcionada,
                        qNodoMapa.mostrarBarraGris, qNodoMapa.mostrarFechasEvento));

        if (tuple == null)
        {
            return null;
        }

        NodoMapa nodoMapa = new NodoMapa();

        nodoMapa.setId(tuple.get(qNodoMapa.id));
        nodoMapa.setEnlaceDestino(tuple.get(qNodoMapa.enlaceDestino));
        nodoMapa.setTituloPublicacionCA(tuple.get(qNodoMapa.tituloPublicacionCA));
        nodoMapa.setTituloPublicacionES(tuple.get(qNodoMapa.tituloPublicacionES));
        nodoMapa.setTituloPublicacionEN(tuple.get(qNodoMapa.tituloPublicacionEN));
        nodoMapa.setContenidoRelacionado(tuple.get(qNodoMapa.contenidoRelacionado));
        nodoMapa.setMostrarReloj(tuple.get(qNodoMapa.mostrarReloj));
        nodoMapa.setMostrarMenuPerfilesPlegado(tuple.get(qNodoMapa.mostrarMenuPerfilesPlegado));
        nodoMapa.setMostrarRecursosRelacionados(tuple.get(qNodoMapa.mostrarRecursosRelacionados));
        nodoMapa.setMostrarTags(tuple.get(qNodoMapa.mostrarTags));
        nodoMapa.setMostrarFechaModificacion(tuple.get(qNodoMapa.mostrarFechaModificacion));
        nodoMapa.setMostrarFuente(tuple.get(qNodoMapa.mostrarFuente));
        nodoMapa.setMostrarRedesSociales(tuple.get(qNodoMapa.mostrarRedesSociales));
        nodoMapa.setMostrarInformacionProporcionada(tuple.get(qNodoMapa.mostrarInformacionProporcionada));
        nodoMapa.setMostrarBarraGris(tuple.get(qNodoMapa.mostrarBarraGris));
        nodoMapa.setMenu(tuple.get(qNodoMapa.menu));
        nodoMapa.setPaginado(tuple.get(qNodoMapa.paginado));
        nodoMapa.setMostrarFechasEvento(tuple.get(qNodoMapa.mostrarFechasEvento));

        return nodoMapa;
    }

    public List<NodoMapaPlantilla> getNodoMapaPlantillasByNodoMapaId(Long nodoMapaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<Tuple> tuples = query.from(qNodoMapaPlantilla)
                .where(qNodoMapaPlantilla.upoMapa.id.eq(nodoMapaId))
                .list(new QTuple(qNodoMapaPlantilla.id, qNodoMapaPlantilla.upoPlantilla.tipo));

        List<NodoMapaPlantilla> plantillas = new ArrayList<>();

        for (Tuple tuple : tuples)
        {
            NodoMapaPlantilla plantilla = new NodoMapaPlantilla();

            plantilla.setId(tuple.get(qNodoMapaPlantilla.id));
            plantilla.setUpoPlantilla(new Plantilla());
            plantilla.getUpoPlantilla().setTipo(tuple.get(qNodoMapaPlantilla.upoPlantilla.tipo));

            plantillas.add(plantilla);
        }

        return plantillas;
    }

    public Plantilla getPlantillaByFileName(String fileName)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPlantilla).where(qPlantilla.fichero.eq(fileName).and(qPlantilla.obsoleta.isFalse()));

        Tuple tuple = query.uniqueResult(new QTuple(qPlantilla.id, qPlantilla.tipo));

        if (tuple == null) return null;

        Plantilla plantilla = new Plantilla();

        plantilla.setId(tuple.get(qPlantilla.id));
        plantilla.setTipo(tuple.get(qPlantilla.tipo));

        return plantilla;
    }

    public NodoMapaPlantilla getNodoMapaPlantillaByNodoMapaIdAndType(Long nodoMapaId, TipoPlantilla tipoPlantilla)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<Tuple> tuples = query.from(qNodoMapaPlantilla)
                .where(qNodoMapaPlantilla.upoMapa.id.eq(nodoMapaId)
                        .and(qNodoMapaPlantilla.upoPlantilla.tipo.eq(tipoPlantilla)))
                .list(new QTuple(qNodoMapaPlantilla.id, qNodoMapaPlantilla.upoPlantilla.fichero));

        if (tuples.size() != 0)
        {
            NodoMapaPlantilla nodoMapaPlantilla = new NodoMapaPlantilla();

            nodoMapaPlantilla.setId(tuples.get(0).get(qNodoMapaPlantilla.id));
            nodoMapaPlantilla.setUpoPlantilla(new Plantilla());
            nodoMapaPlantilla.getUpoPlantilla().setFichero(tuples.get(0).get(qNodoMapaPlantilla.upoPlantilla.fichero));

            return nodoMapaPlantilla;
        }

        return null;
    }

    public Publicacion getContenidoIdiomaByContenidoIdAndLanguage(Long contenidoId, String language)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qPublicacion)
                .where(qPublicacion.contenidoId.eq(contenidoId)
                        .and(qPublicacion.idioma.eq(language.toLowerCase())
                                .and(qPublicacion.tipoContenido.toUpperCase()
                                        .eq(TipoReferenciaContenido.NORMAL.toString()))))
                .uniqueResult(qPublicacion);
    }

    public List<Publicacion> getContenidosIdiomasByUrlNodoAndLanguage(String urlNodo, String language)
    {
        JPAQuery query = new JPAQuery(entityManager);

        int numNivelesUrlNodo = urlNodo.split("/").length;
        int nivelNodoMapa = getNivelUrlCompleta(urlNodo);

        return query.from(qPublicacion)
                .where(qPublicacion.urlNodo.like(urlNodo + '%')
                        .and(qPublicacion.idioma.eq(language.toLowerCase()))
                        .and(qPublicacion.urlNumNiveles.between(numNivelesUrlNodo, nivelNodoMapa + numNivelesUrlNodo)))
                .orderBy(qPublicacion.urlNumNiveles.asc(), qPublicacion.nodoMapaPadreOrden.asc(),
                        qPublicacion.nodoMapaOrden.asc(), qPublicacion.contenidoOrden.asc())
                .list(qPublicacion);
    }

    private Integer getNivelUrlCompleta(String urlCompleta)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qNodoMapa)
                .join(qNodoMapa.upoMapasPlantillas, qNodoMapaPlantilla)
                .where(qNodoMapa.urlCompleta.eq(urlCompleta));

        Tuple tuple = query.uniqueResult(new QTuple(qNodoMapaPlantilla.nivel));

        if (tuple != null)
        {
            return tuple.get(qNodoMapaPlantilla.nivel);
        }

        return 1;
    }

    public PublicacionBinarios getBinarioByUrlCompletaAndLanguage(String urlCompleta, String language)
    {
        JPAQuery query = new JPAQuery(entityManager);

        String urlNodo = urlCompleta.substring(0, urlCompleta.lastIndexOf("/") + 1);
        String urlPath = urlCompleta.substring(urlCompleta.lastIndexOf("/") + 1);

        return query.from(qPublicacionBinarios)
                .where(qPublicacionBinarios.urlNodo.eq(urlNodo)
                        .and(qPublicacionBinarios.urlPath.eq(urlPath))
                        .and(qPublicacionBinarios.idioma.eq(language.toLowerCase())))
                .uniqueResult(qPublicacionBinarios);
    }

    public Map<String, Object> getContenidosIdiomasByUrlCompletaAndLanguageAndSearchFields(RequestParams requestParams,
            String searchQuery)
    {
        Map<String, Object> returnValues = new HashMap<String, Object>();

        List<String> listaMapaUrlCompleta = entityManager.createNativeQuery(searchQuery).getResultList();

        if (listaMapaUrlCompleta.isEmpty())
        {
            returnValues.put("numSearchItems", 0);
            returnValues.put("search", new ArrayList<Publicacion>());

            return returnValues;
        }

        String[] urlsUnicas = setDistinctOnList(listaMapaUrlCompleta);
        String[] urlsUnicasSubset = getSubsetOfArray(urlsUnicas, requestParams.getSearchParams().getStartSearch(),
                requestParams.getSearchParams().getNumResultados());

        String urlsUnicasSubsetString = "'" + StringUtils.join(urlsUnicasSubset, "','") + "'";
        String orderSearch = requestParams.getSearchParams().getOrden();

        if (orderSearch == null || orderSearch.isEmpty())
        {
            orderSearch =
                    "prioridad asc, distancia_sysdate asc, orden asc, decode(es_html, 'S', fecha_creacion, to_date('1/1/1900')) desc";
        }

        String nativeQuery = "select * from UPO_VW_PUBLICACION where idioma='" + requestParams.getConfigPublicacion()
                .getIdioma()
                .toLowerCase() + "' and visible='S' and publicable='S' and url_nodo in (" + urlsUnicasSubsetString + ")" + "order by " + orderSearch;

        List<Publicacion> result = entityManager.createNativeQuery(nativeQuery, Publicacion.class).getResultList();

        returnValues.put("numSearchItems", urlsUnicas.length);
        returnValues.put("search", result);

        return returnValues;
    }

    private String[] getSubsetOfArray(String[] urlsUnicas, Integer startSearch, Integer limitSearch)
    {
        Integer limit = (startSearch + limitSearch > urlsUnicas.length) ? urlsUnicas.length : startSearch + limitSearch;

        return Arrays.copyOfRange(urlsUnicas, startSearch, limit);
    }

    private String[] setDistinctOnList(List<String> listaMapaUrlCompleta)
    {
        Set<String> temp = new LinkedHashSet<String>(listaMapaUrlCompleta);
        return temp.toArray(new String[temp.size()]);
    }

    public List<Grupo> getGruposByMenuId(Long menuId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        query.from(qMenuGrupo).where(qMenuGrupo.menu.id.eq(menuId)).orderBy(qMenuGrupo.orden.asc());

        List<Grupo> grupos = query.list(qMenuGrupo.grupo);

        Set setItems = new LinkedHashSet(grupos);
        grupos.clear();
        grupos.addAll(setItems);

        return grupos;
    }

    public List<Item> getItemsByGrupoId(Long grupoId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        query.from(qGrupoItem).where(qGrupoItem.grupo.id.eq(grupoId)).orderBy(qGrupoItem.orden.asc());

        List<Item> items = query.list(qGrupoItem.item);

        Set setItems = new LinkedHashSet(items);
        items.clear();
        items.addAll(setItems);

        return items;
    }
}