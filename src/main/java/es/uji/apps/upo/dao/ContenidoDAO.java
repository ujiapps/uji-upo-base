package es.uji.apps.upo.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.upo.model.Contenido;
import es.uji.apps.upo.model.ContenidoIdioma;
import es.uji.apps.upo.model.ContenidoIdiomaLog;
import es.uji.apps.upo.model.ContenidoLog;
import es.uji.apps.upo.model.Idioma;
import es.uji.apps.upo.model.QContenido;
import es.uji.apps.upo.model.QContenidoIdioma;
import es.uji.apps.upo.model.QContenidoIdiomaAtributo;
import es.uji.apps.upo.model.QContenidoIdiomaRecurso;
import es.uji.apps.upo.model.QIdioma;
import es.uji.apps.upo.model.QNodoMapa;
import es.uji.apps.upo.model.QNodoMapaContenido;
import es.uji.apps.upo.model.enums.EstadoModeracion;
import es.uji.apps.upo.model.enums.TipoReferenciaContenido;
import es.uji.apps.upo.model.metadatos.QContenidoMetadato;
import es.uji.apps.upo.services.ContenidoIdiomaLogService;
import es.uji.apps.upo.services.ContenidoLogService;
import es.uji.apps.upo.solr.ContenidoSolr;
import es.uji.apps.upo.solr.SolrIndexer;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ContenidoDAO extends BaseDAODatabaseImpl
{
    private static final String DELETE_RECORD = "D";
    private static final String UPDATE_RECORD = "U";
    private static final String INSERT_RECORD = "I";
    private QIdioma qIdioma = QIdioma.idioma;
    private QContenido qContenido = QContenido.contenido;
    private QContenidoIdioma qContenidoIdioma = QContenidoIdioma.contenidoIdioma;
    private QContenidoIdiomaRecurso qContenidoIdiomaRecurso = QContenidoIdiomaRecurso.contenidoIdiomaRecurso;
    private QContenidoIdiomaAtributo qContenidoIdiomaAtributo = QContenidoIdiomaAtributo.contenidoIdiomaAtributo;
    private QContenidoMetadato qContenidoMetadato = QContenidoMetadato.contenidoMetadato;
    private QNodoMapa qNodoMapa = QNodoMapa.nodoMapa;
    private QNodoMapaContenido qNodoMapaContenido = QNodoMapaContenido.nodoMapaContenido;
    private SolrIndexer solrIndexer;
    private ContenidoLogService contenidoLogService;
    private ContenidoIdiomaLogService contenidoIdiomaLogService;

    @Autowired
    public ContenidoDAO(SolrIndexer solrIndexer, ContenidoLogService contenidoLogService,
            ContenidoIdiomaLogService contenidoIdiomaLogService)
    {
        this.solrIndexer = solrIndexer;
        this.contenidoLogService = contenidoLogService;
        this.contenidoIdiomaLogService = contenidoIdiomaLogService;
    }

    public Contenido getContenidoById(Long contenidoId)
    {
        return getContenido(contenidoId, false);
    }

    public Contenido getContenido(Long contenidoId, boolean fetchAll)
    {
        JPAQuery query = new JPAQuery(entityManager);

        if (!fetchAll)
        {
            query.from(qContenido).leftJoin(qContenido.contenidoIdiomas, qContenidoIdioma).fetch()
                    .leftJoin(qContenidoIdioma.upoIdioma, qIdioma).fetch()
                    .leftJoin(qContenido.upoVigenciasObjetos).fetch()
                    //.leftJoin(qContenido.upoMapasObjetos).fetch()
                    .leftJoin(qContenido.upoContenidosTags).fetch()
                    .where(qContenido.id.eq(contenidoId));
        }
        else
        {
            query.from(qContenido).leftJoin(qContenido.upoVigenciasObjetos).fetch()
                    .leftJoin(qContenido.upoContenidosTags).fetch()
                    .leftJoin(qContenido.contenidoPrioridades).fetch()
                    .leftJoin(qContenido.upoContenidosMetadatos).fetch()
                    .leftJoin(qContenido.upoExtPersona1).fetch()
                    .leftJoin(qContenido.contenidoIdiomas, qContenidoIdioma).fetch()
                    .leftJoin(qContenidoIdioma.upoIdioma, qIdioma).fetch()
                    .leftJoin(qContenidoIdioma.contenidoIdiomaRecursos, qContenidoIdiomaRecurso)
                    .fetch()
                    .leftJoin(qContenidoIdioma.contenidoIdiomaAtributos, qContenidoIdiomaAtributo)
                    .fetch().leftJoin(qContenido.origenInformacion).fetch()
                    .where(qContenido.id.eq(contenidoId));
        }

        List<Contenido> listadoContenidos = query.list(qContenido);

        return listadoContenidos.size() > 0 ? listadoContenidos.get(0) : null;
    }

    private List<Tuple> extraerFilasContenido(List<Tuple> filasConTodoLosContenidos)
    {
        Long idActual = null;
        List<Tuple> filasParaUnContenido = new ArrayList<Tuple>();

        for (Tuple tuple : filasConTodoLosContenidos)
        {
            Long idContenido = tuple.get(qContenido.id);

            if (idActual == null)
            {
                idActual = idContenido;
            }
            else if (!idActual.equals(idContenido))
            {
                continue;
            }

            filasParaUnContenido.add(tuple);
        }

        filasConTodoLosContenidos.removeAll(filasParaUnContenido);

        return filasParaUnContenido;
    }

    private Contenido extraerContenido(List<Tuple> filasConDatosDeUnContenido)
    {
        Tuple tuple = filasConDatosDeUnContenido.get(0);

        Contenido contenidoAux = new Contenido(tuple.get(qContenido.urlPath));

        contenidoAux.setId(tuple.get(qContenido.id));
        contenidoAux.setPublicable(tuple.get(qContenido.publicable));
        contenidoAux.setVisible(tuple.get(qContenido.visible));
        contenidoAux.setUrlOriginal(tuple.get(qContenido.urlOriginal));
        contenidoAux.setTextoNoVisible(tuple.get(qContenido.textoNoVisible));

        return contenidoAux;
    }

    public List<Contenido> getContenidosByMapaId(Long mapaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        ArrayList<Contenido> resultList = new ArrayList<Contenido>();

        query.from(qContenido)
                .join(qContenido.contenidoIdiomas, qContenidoIdioma)
                .join(qContenidoIdioma.upoIdioma, qIdioma)
                .join(qContenido.upoMapasObjetos, qNodoMapaContenido)
                .where(qNodoMapaContenido.upoMapa.id.eq(mapaId).and(
                        qNodoMapaContenido.estadoModeracion.ne(EstadoModeracion.RECHAZADO)))
                .orderBy(qNodoMapaContenido.orden.asc());

        List<Tuple> listaContenidos = query.distinct().list(qContenido.id, qContenido.urlPath,
                qContenido.publicable, qContenido.visible, qContenido.urlOriginal,
                qContenido.textoNoVisible, qNodoMapaContenido.orden, qContenidoIdioma.id,
                qContenidoIdioma.titulo, qContenidoIdioma.html, qContenidoIdioma.mimeType,
                qNodoMapaContenido.orden, qContenidoIdioma.longitud, qContenidoIdioma.tipoFormato,
                qIdioma.id, qIdioma.codigoISO);

        while (!listaContenidos.isEmpty())
        {
            List<Tuple> filasConDatosDeUnContenido = extraerFilasContenido(listaContenidos);
            Contenido contenido = extraerContenido(filasConDatosDeUnContenido);
            Set<ContenidoIdioma> contenidosIdiomas = extraerContenidosIdiomas(filasConDatosDeUnContenido);

            contenido.setContenidoIdiomas(contenidosIdiomas);
            resultList.add(contenido);
        }

        return resultList;
    }

    private Set<ContenidoIdioma> extraerContenidosIdiomas(List<Tuple> filasConDatosDeUnContenido)
    {
        HashSet<ContenidoIdioma> contenidoIdiomas = new HashSet<ContenidoIdioma>();

        for (Tuple tuple : filasConDatosDeUnContenido)
        {
            ContenidoIdioma contenidoIdioma = new ContenidoIdioma();
            contenidoIdioma.setId(tuple.get(qContenidoIdioma.id));
            contenidoIdioma.setTitulo(tuple.get(qContenidoIdioma.titulo));
            contenidoIdioma.setHtml(tuple.get(qContenidoIdioma.html));
            contenidoIdioma.setMimeType(tuple.get(qContenidoIdioma.mimeType));
            contenidoIdioma.setLongitud(tuple.get(qContenidoIdioma.longitud));
            contenidoIdioma.setTipoFormato(tuple.get(qContenidoIdioma.tipoFormato));

            Idioma idioma = new Idioma();
            idioma.setId(tuple.get(qIdioma.id));
            idioma.setCodigoISO(tuple.get(qIdioma.codigoISO));

            contenidoIdioma.setUpoIdioma(idioma);
            contenidoIdiomas.add(contenidoIdioma);
        }
        return contenidoIdiomas;
    }

    public List<ContenidoIdioma> getContenidosIdiomasByObjetoId(Long objetoId)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        query.from(qContenidoIdioma).where(qContenidoIdioma.upoObjeto.id.eq(objetoId));

        List<ContenidoIdioma> listadoIdiomas = query.list(qContenidoIdioma);

        return listadoIdiomas.size() > 0 ? listadoIdiomas : null;
    }

    @Transactional
    public <T> T insert(final T entity, Long personaId) throws PersistenceException
    {
        T result = super.insert(entity);

        registraOperacion(INSERT_RECORD, (Contenido) result, personaId);
        indexaContenido((Contenido) result, ((Contenido) result).getUrlCompletaNodoMapa());

        return result;
    }

    @Transactional
    public void registraInsert(Contenido contenido, Long personaId) throws PersistenceException
    {
        registraOperacion(INSERT_RECORD, contenido, personaId);
        indexaContenido(contenido, contenido.getUrlCompletaNodoMapa());
    }

    @Transactional
    public <T> T update(final T entity, Long personaId) throws PersistenceException
    {
        T result = super.update(entity);

        registraOperacion(UPDATE_RECORD, (Contenido) result, personaId);
        indexaContenido((Contenido) result, ((Contenido) result).getUrlCompletaNodoMapa());

        return result;
    }

    @Transactional
    public <T> void delete(final Class<T> entity, final Long id, Long personaId)
            throws PersistenceException
    {
        List<T> listaContenidos = get(entity, id);

        if (listaContenidos != null && !listaContenidos.isEmpty())
        {
            Contenido contenido = (Contenido) listaContenidos.get(0);

            String urlCompleta = contenido.getUrlCompletaNodoMapa();

            super.delete(contenido);

            registraOperacion(DELETE_RECORD, contenido, personaId);
            borraContenidoIndexado(contenido, urlCompleta);
        }
    }

    private ContenidoLog generateContenidoLogFromContenido(Contenido contenido)
    {
        ContenidoLog contenidoLog = new ContenidoLog();

        if (contenido.getId() != null)
        {
            contenidoLog.setId(contenido.getId());
        }

        contenidoLog.setUrlOriginal(contenido.getUrlOriginal());
        contenidoLog.setUrlPath(contenido.getUrlPath());
        contenidoLog.setPublicable(contenido.getPublicable());
        contenidoLog.setLatitud(contenido.getLatitud());
        contenidoLog.setLongitud(contenido.getLongitud());
        contenidoLog.setLugar(contenido.getLugar());
        contenidoLog.setOtrosAutores(contenido.getOtrosAutores());
        contenidoLog.setFechaOperacion(new Date());
        contenidoLog.setTextoNoVisible(contenido.getTextoNoVisible());
        contenidoLog.setVisible(contenido.getVisible());
        contenidoLog.setFechaCreacion(contenido.getFechaCreacion());
        contenidoLog.setAjustarAFechasVigencia(contenido.isAjustarAFechasVigencia());

        if (contenido.getUpoExtPersona1() != null)
        {
            contenidoLog.setPerIdResponsable(contenido.getUpoExtPersona1().getId());
        }

        if (contenido.getOrigenInformacion() != null)
        {
            contenidoLog.setOrigenInformacion(contenido.getOrigenInformacion().getId());
        }

        return contenidoLog;
    }

    private void registrarContenidoIdiomaLogFromContenido(String operacion, Contenido contenido,
            Long personaId)
    {
        for (ContenidoIdioma contenidoIdioma : contenido.getContenidoIdiomas())
        {
            registrarContenidoIdiomaLog(operacion, personaId, contenidoIdioma);
        }
    }

    @Transactional
    public void registrarContenidoIdiomaLog(String operacion, Long personaId,
            ContenidoIdioma contenidoIdioma)
    {
        ContenidoIdiomaLog contenidoIdiomaLog = new ContenidoIdiomaLog();
        contenidoIdiomaLog.setId(contenidoIdioma.getId());
        contenidoIdiomaLog.setObjetoId(contenidoIdioma.getUpoObjeto().getId());
        contenidoIdiomaLog.setIdiomaId(contenidoIdioma.getUpoIdioma().getId());
        contenidoIdiomaLog.setTitulo(contenidoIdioma.getTitulo());
        contenidoIdiomaLog.setSubtitulo(contenidoIdioma.getSubtitulo());
        contenidoIdiomaLog.setTituloLargo(contenidoIdioma.getTituloLargo());
        contenidoIdiomaLog.setResumen(contenidoIdioma.getResumen());
        contenidoIdiomaLog.setMimeType(contenidoIdioma.getMimeType());
        contenidoIdiomaLog.setNombreFichero(contenidoIdioma.getNombreFichero());
        contenidoIdiomaLog.setFechaOperacion(new Date());
        contenidoIdiomaLog.setTipoOperacion(operacion);
        contenidoIdiomaLog.setContenido(contenidoIdioma.getContenido());
        contenidoIdiomaLog.setLongitud(contenidoIdioma.getLongitud());
        contenidoIdiomaLog.setHtml(contenidoIdioma.getHtml());
        contenidoIdiomaLog.setUsuarioOperacion(personaId);
        contenidoIdiomaLog.setFechaModificacion(contenidoIdioma.getFechaModificacion());
        contenidoIdiomaLog.setEnlaceDestino(contenidoIdioma.getEnlaceDestino());
        contenidoIdiomaLog.setTipoFormato(contenidoIdioma.getTipoFormato());

        // TODO: DAO usando un Service!!!!
        contenidoIdiomaLogService.insert(contenidoIdiomaLog);
    }

    @Transactional
    private void registraOperacion(String operacion, Contenido contenido, Long personaId)
    {
        ContenidoLog contenidoLog = generateContenidoLogFromContenido(contenido);
        contenidoLog.setUsuarioOperacion(personaId);
        contenidoLog.setTipoOperacion(operacion);

        contenidoLogService.insertar(contenidoLog);

        registrarContenidoIdiomaLogFromContenido(operacion, contenido, personaId);
    }

    @Transactional
    private void borraContenidoIndexado(Contenido contenido, String urlCompleta)
    {
        for (ContenidoSolr contenidoSolr : contenido.toSolrContent(urlCompleta))
        {
            //solrIndexer.deleteById(contenidoSolr.getId());
        }
    }

    @Transactional
    private void indexaContenido(Contenido contenido, String urlCompleta)
    {
        for (ContenidoSolr contenidoSolr : contenido.toSolrContent(urlCompleta))
        {
            //solrIndexer.add(contenidoSolr);
        }
    }

    public List<Contenido> getContenidosByUrlCompleta(String urlCompleta)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query
                .from(qContenido)
                .join(qContenido.upoMapasObjetos, qNodoMapaContenido)
                .fetch()
                .where(qNodoMapaContenido.upoMapa.urlCompleta.like(urlCompleta + '%').and(
                        qNodoMapaContenido.tipo.eq(TipoReferenciaContenido.NORMAL)))
                .list(qContenido);
    }

    @Transactional
    public void setVisibilidadByNodoMapa(Long id, String visible, String texto)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qContenido)
                .join(qContenido.upoMapasObjetos, qNodoMapaContenido)
                .join(qNodoMapaContenido.upoMapa, qNodoMapa)
                .where(qNodoMapa.id.eq(id).and(
                        qNodoMapaContenido.tipo.eq(TipoReferenciaContenido.NORMAL)));

        updateVibility(visible, texto, query);
    }

    @Transactional
    public void setVisibilidadByUrlCompleta(String urlCompleta, String visible, String texto)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qContenido)
                .join(qContenido.upoMapasObjetos, qNodoMapaContenido)
                .join(qNodoMapaContenido.upoMapa, qNodoMapa)
                .where(qNodoMapa.urlCompleta.like(urlCompleta + '%').and(
                        qNodoMapaContenido.tipo.eq(TipoReferenciaContenido.NORMAL)));

        updateVibility(visible, texto, query);
    }

    @Transactional
    private void updateVibility(String visible, String texto, JPAQuery query)
    {
        QContenido contenido = QContenido.contenido;

        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, contenido);

        List<Contenido> listaContenidos = query.list(contenido);

        if (listaContenidos.size() == 0)
        {
            return;
        }

        updateClause.where(contenido.in(query.list(contenido))).set(contenido.visible, visible)
                .set(contenido.textoNoVisible, texto).execute();
    }

    @Transactional
    public void deleteMetadatosByContenidoId(Long contenidoId)
    {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qContenidoMetadato);

        deleteClause.where(qContenidoMetadato.upoObjeto.id.eq(contenidoId)).execute();
    }
}
