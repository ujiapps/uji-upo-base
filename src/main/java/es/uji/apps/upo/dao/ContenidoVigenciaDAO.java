package es.uji.apps.upo.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;

import es.uji.apps.upo.model.QVigenciaContenido;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ContenidoVigenciaDAO extends BaseDAODatabaseImpl
{
    private QVigenciaContenido vigenciaContenido = QVigenciaContenido.vigenciaContenido;

    @Transactional
    public void deleteVigenciasByContenido(Long contenidoId)
    {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, vigenciaContenido);

        deleteClause.where(vigenciaContenido.upoObjeto.id.eq(contenidoId)).execute();
    }
}
