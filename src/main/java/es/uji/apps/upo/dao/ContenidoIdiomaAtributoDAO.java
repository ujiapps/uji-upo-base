package es.uji.apps.upo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.upo.model.ContenidoIdioma;
import es.uji.apps.upo.model.ContenidoIdiomaAtributo;
import es.uji.apps.upo.model.ContenidoIdiomaRecurso;
import es.uji.apps.upo.model.QContenidoIdioma;
import es.uji.apps.upo.model.QContenidoIdiomaAtributo;
import es.uji.apps.upo.model.QContenidoIdiomaRecurso;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ContenidoIdiomaAtributoDAO extends BaseDAODatabaseImpl
{
    private QContenidoIdiomaAtributo contenidoIdiomaAtributo = QContenidoIdiomaAtributo.contenidoIdiomaAtributo;
    private QContenidoIdioma contenidoIdioma = QContenidoIdioma.contenidoIdioma;

    @Transactional
    public void deleteByContenido(Long contenidoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(contenidoIdioma).where(contenidoIdioma.upoObjeto.id.eq(contenidoId));

        if (query.list(contenidoIdioma).size() > 0)
        {
            JPADeleteClause deleteClause = new JPADeleteClause(entityManager,
                    contenidoIdiomaAtributo).where(contenidoIdiomaAtributo.contenidoIdioma.in(query
                    .list(contenidoIdioma)));

            deleteClause.execute();
        }
    }

    public List<ContenidoIdiomaAtributo> getContenidoIdiomaAtributosUrlByContenidoIdioma(
            ContenidoIdioma contenidoIdioma)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(contenidoIdiomaAtributo)
                .where(contenidoIdiomaAtributo.contenidoIdioma.eq(contenidoIdioma))
                .list(contenidoIdiomaAtributo);
    }
}
