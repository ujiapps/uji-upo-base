package es.uji.apps.upo.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.upo.model.*;
import es.uji.apps.upo.model.enums.EstadoModeracion;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class ModeracionDAO extends BaseDAODatabaseImpl
{
    private QModeracion qModeracion = QModeracion.moderacion;
    private QModeracionHistorico qModeracionHistorico = QModeracionHistorico.moderacionHistorico;
    private QModeracionMapaObjeto qModeracionMapaObjeto = QModeracionMapaObjeto.moderacionMapaObjeto;

    public List<Moderacion> getNodosPendientesModeracion(Long personaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        List<Moderacion> pendientesModeracion = new ArrayList<>();

        List<Tuple> lista = query.from(qModeracion)
                .where(qModeracion.estadoModeracion.eq(String.valueOf(EstadoModeracion.PENDIENTE))
                        .and(qModeracion.personaId.eq(personaId)))
                .orderBy(qModeracion.mapaUrlCompleta.asc())
                .list(qModeracion.mapaId, qModeracion.mapaUrlCompleta, qModeracion.contieneOriginales);

        lista = lista.stream().distinct().collect(Collectors.toList());

        lista.forEach(t -> {
            Moderacion pendienteModeracion = new Moderacion();

            pendienteModeracion.setId(t.get(qModeracion.mapaId).toString());
            pendienteModeracion.setMapaId(t.get(qModeracion.mapaId));
            pendienteModeracion.setMapaUrlCompleta(t.get(qModeracion.mapaUrlCompleta));
            pendienteModeracion.setContieneOriginales(t.get(qModeracion.contieneOriginales));

            pendientesModeracion.add(pendienteModeracion);
        });

        return pendientesModeracion;
    }

    public Moderacion getElementoPendientesModeracionById(Long personaId, Long nodoMapaContenidoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qModeracion)
                .where(qModeracion.estadoModeracion.eq(String.valueOf(EstadoModeracion.PENDIENTE))
                        .and(qModeracion.personaId.eq(personaId).and(qModeracion.mapaObjetoId.eq(nodoMapaContenidoId))))
                .uniqueResult(qModeracion);
    }

    public List<Moderacion> getElementosPendientesModeracionByMapaId(Long personaId, Long mapaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qModeracion)
                .where(qModeracion.estadoModeracion.eq(String.valueOf(EstadoModeracion.PENDIENTE))
                        .and(qModeracion.personaId.eq(personaId).and(qModeracion.mapaId.eq(mapaId))))
                .distinct()
                .orderBy(qModeracion.mapaUrlCompleta.asc())
                .list(qModeracion);
    }

    public List<Moderacion> getElementosDescendientesPendientesModeracionByUrlCompleta(Long personaId,
            String urlCompleta)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qModeracion)
                .where(qModeracion.estadoModeracion.eq(String.valueOf(EstadoModeracion.PENDIENTE))
                        .and(qModeracion.personaId.eq(personaId)
                                .and(qModeracion.mapaUrlCompleta.like(urlCompleta + '%'))))
                        .distinct()
                        .orderBy(qModeracion.mapaUrlCompleta.asc())
                        .list(qModeracion);
    }

    public List<ModeracionHistorico> getElementosHistoricoModeracion(Long personaId, Long start, Long limit)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qModeracionHistorico)
                .where(qModeracionHistorico.perIdModeracion.eq(personaId))
                .offset(start)
                .limit(limit)
                .orderBy(qModeracionHistorico.mapaObjetoId.desc())
                .list(qModeracionHistorico);
    }

    public Long getNumElementosHistoricoModeracion(Long personaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qModeracionHistorico).where(qModeracionHistorico.perIdModeracion.eq(personaId)).count();
    }

    public ModeracionHistorico obtenerElementoHistoricoModeracion(Long nodoMapaObjetoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qModeracionHistorico)
                .where(qModeracionHistorico.id.eq(nodoMapaObjetoId.toString()))
                .uniqueResult(qModeracionHistorico);
    }

    public ModeracionMapaObjeto obtenerModeracionMapaObjeto(Long nodoMapaObjetoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qModeracionMapaObjeto)
                .where(qModeracionMapaObjeto.id.eq(nodoMapaObjetoId.toString()))
                .uniqueResult(qModeracionMapaObjeto);
    }
}