package es.uji.apps.upo.model.eujier;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "XPFI_ITM_FAVORITOS")
@SuppressWarnings("serial")
public class EujierFavorito implements Serializable
{
    @Id
    @Column(name = "per_id")
    private Long perId;

    @Id
    @Column(name = "xii_id")
    private Long itemId;

    public EujierFavorito()
    {
    }

    public EujierFavorito(Long itemId, Long perId)
    {
        this.perId = perId;
        this.itemId = itemId;
    }

    public Long getPerId()
    {
        return perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }

    public Long getItemId()
    {
        return itemId;
    }

    public void setItemId(Long itemId)
    {
        this.itemId = itemId;
    }
}
