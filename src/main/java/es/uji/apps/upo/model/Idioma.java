package es.uji.apps.upo.model;

import es.uji.apps.upo.dao.IdiomaDAO;
import es.uji.commons.rest.Role;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Component
@Entity
@Table(name = "UPO_IDIOMAS")
@SuppressWarnings("serial")
public class Idioma implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    @Column(name = "CODIGO_ISO")
    private String codigoISO;

    @OneToMany(mappedBy = "upoIdioma")
    private Set<NodoMapaExclusionIdioma> upoMapasIdiomasExclusiones;

    @OneToMany(mappedBy = "upoIdioma")
    private Set<ContenidoIdioma> upoObjetosIdiomas;

    public Idioma()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCodigoISO()
    {
        if (codigoISO == null)
        {
            return null;
        }

        return codigoISO.toUpperCase();
    }

    public void setCodigoISO(String codigoISO)
    {
        this.codigoISO = codigoISO;
    }

    public Set<NodoMapaExclusionIdioma> getUpoMapasIdiomasExclusiones()
    {
        return this.upoMapasIdiomasExclusiones;
    }

    public void setUpoMapasIdiomasExclusiones(
            Set<NodoMapaExclusionIdioma> upoMapasIdiomasExclusiones)
    {
        this.upoMapasIdiomasExclusiones = upoMapasIdiomasExclusiones;
    }

    public Set<ContenidoIdioma> getUpoObjetosIdiomas()
    {
        return this.upoObjetosIdiomas;
    }

    public void setUpoObjetosIdiomas(Set<ContenidoIdioma> upoObjetosIdiomas)
    {
        this.upoObjetosIdiomas = upoObjetosIdiomas;
    }

    @Override
    public boolean equals(Object otroObjeto)
    {
        boolean result = false;
        if (otroObjeto instanceof Idioma)
        {
            Idioma idioma = (Idioma) otroObjeto;
            result = this.getCodigoISO().equals(idioma.getCodigoISO());
        }

        return result;
    }

    @Override
    public int hashCode()
    {
        int num = 1;
        String iso = this.getCodigoISO();

        if ("ES".equalsIgnoreCase(iso))
        {
            num = 200;
        }

        if ("CA".equalsIgnoreCase(iso))
        {
            num = 300;
        }

        if ("EN".equalsIgnoreCase(iso))
        {
            num = 400;

        }

        return (41 * (41 * num) + num);
    }
}