package es.uji.apps.upo.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "UPO_MAPAS_IDIOMAS_EXCLUSIONES")
public class NodoMapaExclusionIdioma implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "IDIOMAS_ID")
    private Idioma upoIdioma;

    @ManyToOne
    @JoinColumn(name = "MAPA_ID")
    private NodoMapa upoMapa;

    public NodoMapaExclusionIdioma()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Idioma getUpoIdioma()
    {
        return this.upoIdioma;
    }

    public void setUpoIdioma(Idioma upoIdioma)
    {
        this.upoIdioma = upoIdioma;
    }

    public NodoMapa getUpoMapa()
    {
        return this.upoMapa;
    }

    public void setUpoMapa(NodoMapa upoMapa)
    {
        this.upoMapa = upoMapa;
    }

}