package es.uji.apps.upo.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "UPO_VW_BUSQUEDA_SIN_FILTRAR")
public class BusquedaSinFiltrar implements Serializable
{
    @Id
    @Column(name = "CONTENIDO_IDIOMA_ID")
    private Long contenidoIdiomaId;

    @Column(name = "MAPA_URL_COMPLETA")
    private String mapaUrlCompleta;

    @Column(name = "IDIOMA_CODIGO_ISO")
    private String idiomaCodigoIso;

    @Column(name = "TITULO")
    private String titulo;

    @Column(name = "TITULO_LARGO")
    private String tituloLargo;

    @Column(name = "SUBTITULO")
    private String subtitulo;

    @Column(name = "RESUMEN")
    private String resumen;

    @Column(name = "CONTENIDO")
    private String contenido;

    @Column(name = "FECHA_VIGENCIA")
    private Date fechaVigencia;

    private Long orden;

    private String prioridad;

    @Column(name = "DISTANCIA_SYSDATE")
    private Float distanciaSysdate;

    public BusquedaSinFiltrar()
    {
    }

    public String getMapaUrlCompleta()
    {
        return mapaUrlCompleta;
    }

    public void setMapaUrlCompleta(String mapaUrlCompleta)
    {
        this.mapaUrlCompleta = mapaUrlCompleta;
    }

    public String getIdiomaCodigoIso()
    {
        return idiomaCodigoIso;
    }

    public void setIdiomaCodigoIso(String idiomaCodigoIso)
    {
        this.idiomaCodigoIso = idiomaCodigoIso;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getTituloLargo()
    {
        return tituloLargo;
    }

    public void setTituloLargo(String tituloLargo)
    {
        this.tituloLargo = tituloLargo;
    }

    public String getSubtitulo()
    {
        return subtitulo;
    }

    public void setSubtitulo(String subtitulo)
    {
        this.subtitulo = subtitulo;
    }

    public String getResumen()
    {
        return resumen;
    }

    public void setResumen(String resumen)
    {
        this.resumen = resumen;
    }

    public String getContenido()
    {
        return contenido;
    }

    public void setContenido(String contenido)
    {
        this.contenido = contenido;
    }

    public Long getContenidoIdiomaId()
    {
        return contenidoIdiomaId;
    }

    public void setContenidoIdiomaId(Long contenidoIdiomaId)
    {
        this.contenidoIdiomaId = contenidoIdiomaId;
    }

    public Long getOrden()
    {
        return orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public String getPrioridad()
    {
        return prioridad;
    }

    public void setPrioridad(String prioridad)
    {
        this.prioridad = prioridad;
    }

    public Float getDistanciaSysdate()
    {
        return distanciaSysdate;
    }

    public void setDistanciaSysdate(Float distanciaSysdate)
    {
        this.distanciaSysdate = distanciaSysdate;
    }

    public Date getFechaVigencia()
    {
        return fechaVigencia;
    }

    public void setFechaVigencia(Date fechaVigencia)
    {
        this.fechaVigencia = fechaVigencia;
    }
}
