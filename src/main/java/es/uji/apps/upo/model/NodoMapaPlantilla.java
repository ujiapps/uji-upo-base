package es.uji.apps.upo.model;

import es.uji.apps.upo.dao.NodoMapaPlantillaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Component
@Entity
@Table(name = "UPO_MAPAS_PLANTILLAS")
@SuppressWarnings("serial")
public class NodoMapaPlantilla implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "MAPA_ID")
    private NodoMapa upoMapa;

    @ManyToOne
    @JoinColumn(name = "PLANTILLA_ID")
    private Plantilla upoPlantilla;

    @Column(name = "NIVEL")
    private Integer nivel;

    public NodoMapaPlantilla()
    {
        this.nivel = Plantilla.DEFAULT_LEVEL;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public NodoMapa getUpoMapa()
    {
        return this.upoMapa;
    }

    public void setUpoMapa(NodoMapa upoMapa)
    {
        this.upoMapa = upoMapa;
    }

    public Plantilla getUpoPlantilla()
    {
        return this.upoPlantilla;
    }

    public void setUpoPlantilla(Plantilla upoPlantilla)
    {
        this.upoPlantilla = upoPlantilla;
    }

    public Integer getNivel()
    {
        return nivel;
    }

    public void setNivel(Integer nivel)
    {
        this.nivel = nivel;
    }
}