package es.uji.apps.upo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "UPO_VW_REVISTA")
public class Revista implements Serializable
{
    @Id
    @Column(name = "CONTENIDO_IDIOMA_ID")
    private Long contenidoIdiomaId;

    @Column(name = "TITULO_LARGO")
    private String tituloLargo;

    @Column(name = "OTROS_AUTORES")
    private String otrosAutores;

    private String resumen;

    @Column(name = "URL_COMPLETA")
    private String urlCompleta;

    @Column(name = "PRIMERA_FECHA_VIGENCIA")
    private Date primeraFechaVigencia;

    @Column(name = "TITULO_LARGO_SIN_ACENTOS")
    private String tituloLargoSinAcentos;

    @Column(name = "RESUMEN_SIN_ACENTOS")
    private String resumenSinAcentos;

    @Column(name = "OTROS_AUTORES_SIN_ACENTOS")
    private String otrosAutoresSinAcentos;

    private String tags;

    @Column(name = "URL_COMPLETA_BUSQUEDA")
    private String urlCompletaBusqueda;

    @Column(name = "MAPA_URL_COMPLETA")
    private String mapaUrlCompleta;

    public Revista()
    {
    }

    public Long getContenidoIdiomaId()
    {
        return contenidoIdiomaId;
    }

    public void setContenidoIdiomaId(Long contenidoIdiomaId)
    {
        this.contenidoIdiomaId = contenidoIdiomaId;
    }

    public String getTituloLargo()
    {
        return tituloLargo;
    }

    public void setTituloLargo(String tituloLargo)
    {
        this.tituloLargo = tituloLargo;
    }

    public String getOtrosAutores()
    {
        return otrosAutores;
    }

    public void setOtrosAutores(String otrosAutores)
    {
        this.otrosAutores = otrosAutores;
    }

    public String getResumen()
    {
        return resumen;
    }

    public void setResumen(String resumen)
    {
        this.resumen = resumen;
    }

    public String getUrlCompleta()
    {
        return urlCompleta;
    }

    public void setUrlCompleta(String urlCompleta)
    {
        this.urlCompleta = urlCompleta;
    }

    public Date getPrimeraFechaVigencia()
    {
        return primeraFechaVigencia;
    }

    public void setPrimeraFechaVigencia(Date primeraFechaVigencia)
    {
        this.primeraFechaVigencia = primeraFechaVigencia;
    }

    public String getTituloLargoSinAcentos()
    {
        return tituloLargoSinAcentos;
    }

    public void setTituloLargoSinAcentos(String tituloLargoSinAcentos)
    {
        this.tituloLargoSinAcentos = tituloLargoSinAcentos;
    }

    public String getResumenSinAcentos()
    {
        return resumenSinAcentos;
    }

    public void setResumenSinAcentos(String resumenSinAcentos)
    {
        this.resumenSinAcentos = resumenSinAcentos;
    }

    public String getOtrosAutoresSinAcentos()
    {
        return otrosAutoresSinAcentos;
    }

    public void setOtrosAutoresSinAcentos(String otrosAutoresSinAcentos)
    {
        this.otrosAutoresSinAcentos = otrosAutoresSinAcentos;
    }

    public String getTags()
    {
        return tags;
    }

    public void setTags(String tags)
    {
        this.tags = tags;
    }

    public String getUrlCompletaBusqueda()
    {
        return urlCompletaBusqueda;
    }

    public void setUrlCompletaBusqueda(String urlCompletaBusqueda)
    {
        this.urlCompletaBusqueda = urlCompletaBusqueda;
    }

    public String getMapaUrlCompleta()
    {
        return mapaUrlCompleta;
    }

    public void setMapaUrlCompleta(String mapaUrlCompleta)
    {
        this.mapaUrlCompleta = mapaUrlCompleta;
    }
}
