package es.uji.apps.upo.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;

@Component
@SuppressWarnings("serial")
@Entity
@Table(name = "UPO_MIGRACION_URLS_PER")
public class MigracionUrlsPer implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "URL_BASE")
    private String urlBase;

    @ManyToOne
    @JoinColumn(name = "PER_ID")
    private Persona persona;

    public MigracionUrlsPer()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getUrlBase()
    {
        return this.urlBase;
    }

    public void setUrlBase(String urlBase)
    {
        this.urlBase = urlBase;
    }

    public Persona getPersona()
    {
        return this.persona;
    }

    public void setPersona(Persona persona)
    {
        this.persona = persona;
    }
}