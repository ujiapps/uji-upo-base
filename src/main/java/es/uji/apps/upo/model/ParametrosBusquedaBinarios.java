package es.uji.apps.upo.model;

public class ParametrosBusquedaBinarios
{
    private String query;
    private String urlNode;
    private int start;
    private int limit;

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        if (query.equals("*")) query = "";

        this.query = query;
    }

    public String getUrlNode()
    {
        return urlNode;
    }

    public void setUrlNode(String urlNode)
    {
        this.urlNode = urlNode;
    }

    public int getStart()
    {
        return start;
    }

    public void setStart(int start)
    {
        this.start = start;
    }

    public int getLimit()
    {
        return limit;
    }

    public void setLimit(int limit)
    {
        this.limit = limit;
    }

    public String getIdioma()
    {
        return idioma;
    }

    public void setIdioma(String idioma)
    {
        this.idioma = idioma;
    }

    private String idioma;

}
