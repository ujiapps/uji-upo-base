package es.uji.apps.upo.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "UPO_OBJETOS_LOG")
@SuppressWarnings("serial")
public class ContenidoLog implements Serializable
{
    @Id
    private Long id;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_OPERACION")
    private Date fechaOperacion;

    private String latitud;

    private String longitud;

    private String lugar;

    private String visible;

    @Column(name = "URL_ORIGINAL")
    private String urlOriginal;

    @Column(name = "URL_PATH")
    private String urlPath;

    @Column(name = "TEXTO_NOVISIBLE")
    private String textoNoVisible;

    @Column(name = "PER_ID_RESPONSABLE")
    private Long perIdResponsable;

    private String publicable;

    @Column(name = "TIPO_OPERACION")
    private String tipoOperacion;

    @Column(name = "USUARIO_OPERACION")
    private Long usuarioOperacion;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_CREACION")
    private Date fechaCreacion;
    @Column(name = "OTROS_AUTORES")
    private String otrosAutores;

    @Column(name = "ORIGEN_INFORMACION_ID")
    private Long origenInformacion;

    @Column(name = "AJUSTAR_A_FECHAS_VIGENCIA")
    private Boolean ajustarAFechasVigencia;

    public ContenidoLog()
    {
    }

    public Date getFechaOperacion()
    {
        return this.fechaOperacion;
    }

    public void setFechaOperacion(Date fechaOperacion)
    {
        this.fechaOperacion = fechaOperacion;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getUrlOriginal()
    {
        return urlOriginal;
    }

    public void setUrlOriginal(String urlOriginal)
    {
        this.urlOriginal = urlOriginal;
    }

    public String getLatitud()
    {
        return this.latitud;
    }

    public void setLatitud(String latitud)
    {
        this.latitud = latitud;
    }

    public String getLongitud()
    {
        return this.longitud;
    }

    public void setLongitud(String longitud)
    {
        this.longitud = longitud;
    }

    public String getLugar()
    {
        return this.lugar;
    }

    public void setLugar(String lugar)
    {
        this.lugar = lugar;
    }

    public String getUrlPath()
    {
        return this.urlPath;
    }

    public void setUrlPath(String urlPath)
    {
        this.urlPath = urlPath;
    }

    public Long getPerIdResponsable()
    {
        return this.perIdResponsable;
    }

    public void setPerIdResponsable(Long perIdResponsable)
    {
        this.perIdResponsable = perIdResponsable;
    }

    public String getPublicable()
    {
        return this.publicable;
    }

    public void setPublicable(String publicable)
    {
        this.publicable = publicable;
    }

    public String getTipoOperacion()
    {
        return this.tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion)
    {
        this.tipoOperacion = tipoOperacion;
    }

    public Long getUsuarioOperacion()
    {
        return this.usuarioOperacion;
    }

    public void setUsuarioOperacion(Long usuarioOperacion)
    {
        this.usuarioOperacion = usuarioOperacion;
    }

    public String getTextoNoVisible()
    {
        return textoNoVisible;
    }

    public void setTextoNoVisible(String textoNoVisible)
    {
        this.textoNoVisible = textoNoVisible;
    }

    public String getOtrosAutores()
    {
        return this.otrosAutores;
    }

    public void setOtrosAutores(String otrosAutores)
    {
        this.otrosAutores = otrosAutores;
    }

    public Date getFechaCreacion()
    {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion)
    {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getOrigenInformacion()
    {
        return origenInformacion;
    }

    public void setOrigenInformacion(Long origenInformacion)
    {
        this.origenInformacion = origenInformacion;
    }

    public String getVisible()
    {
        return visible;
    }

    public void setVisible(String visible)
    {
        this.visible = visible;
    }

    public Boolean isAjustarAFechasVigencia()
    {
        return ajustarAFechasVigencia;
    }

    public void setAjustarAFechasVigencia(Boolean ajustarAFechasVigencia)
    {
        this.ajustarAFechasVigencia = ajustarAFechasVigencia;
    }
}