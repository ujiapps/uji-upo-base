package es.uji.apps.upo.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "UPO_GRUPOS_ITEMS")
@SuppressWarnings("serial")
public class GrupoItem implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "GRUPO_ID")
    private Grupo grupo;

    @ManyToOne
    @JoinColumn(name = "ITEM_ID")
    private Item item;
    private Integer orden;

    public GrupoItem()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Grupo getGrupo()
    {
        return grupo;
    }

    public void setGrupo(Grupo grupo)
    {
        this.grupo = grupo;
    }

    public Item getItem()
    {
        return item;
    }

    public void setItem(Item item)
    {
        this.item = item;
    }

    public Integer getOrden()
    {
        return orden;
    }

    public void setOrden(Integer orden)
    {
        this.orden = orden;
    }
}