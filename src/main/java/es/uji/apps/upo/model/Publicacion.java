package es.uji.apps.upo.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "UPO_VW_PUBLICACION")
public class Publicacion implements Serializable
{
    @Id
    @Column(name = "URL_COMPLETA")
    private String urlCompleta;

    @Lob
    private byte[] contenido;

    @Column(name = "CONTENIDO_IDIOMA_ID")
    private Long contenidoIdiomaId;

    @Column(name = "CONTENIDO_ID")
    private Long contenidoId;

    @Column(name = "ES_HTML")
    private String esHtml;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_MODIFICACION")
    private Date fechaModificacion;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_CREACION")
    private Date fechaCreacion;

    private String idioma;

    @Column(name = "MIME_TYPE")
    private String mimeType;

    private String resumen;

    private String subtitulo;

    private String titulo;

    @Column(name = "TITULO_LARGO")
    private String tituloLargo;

    @Column(name = "URL_NODO")
    private String urlNodo;

    @Column(name = "URL_NODO_ORIGINAL")
    private String urlNodoOriginal;

    @Column(name = "URL_PATH_NODO")
    private String urlPathNodo;

    @Column(name = "URL_CONTENIDO")
    private String urlContenido;

    @Column(name = "URL_COMPLETA_ORIGINAL")
    private String urlCompletaOriginal;

    @Column(name = "URL_DESTINO")
    private String urlDestino;

    @Column(name = "URL_NUM_NIVELES")
    private Integer urlNumNiveles;

    @Column(name = "NIVEL")
    private Integer nivel;

    @Column(name = "TIPO_CONTENIDO")
    private String tipoContenido;

    @Column(name = "NODO_MAPA_ORDEN")
    private Long nodoMapaOrden;

    @Column(name = "NODO_MAPA_PADRE_ORDEN")
    private Long nodoMapaPadreOrden;

    @Column(name = "CONTENIDO_ORDEN")
    private Long contenidoOrden;

    @Column(name = "AUTOR")
    private String autor;

    @Column(name = "ORIGEN_NOMBRE")
    private String origenNombre;

    @Column(name = "ORIGEN_URL")
    private String origenUrl;

    @Column(name = "LUGAR")
    private String lugar;

    @Column(name = "PROXIMA_FECHA_VIGENCIA")
    private Date proximaFechaVigencia;

    @Column(name = "PRIMERA_FECHA_VIGENCIA")
    private Date primeraFechaVigencia;

    private String metadata;

    @Column(name = "TITULO_NODO")
    private String tituloNodo;

    private Long orden;

    private String prioridad;

    @Column(name = "DISTANCIA_SYSDATE")
    private Float distanciaSysdate;

    @Column(name = "TIPO_FORMATO")
    private String tipoFormato;

    private String atributos;

    private String visible;

    @Column(name = "TEXTO_NOVISIBLE")
    private String textoNoVisible;

    private String publicable;

    private String tags;

    @Column(name = "FECHAS_EVENTO")
    private String fechasEvento;

    public Publicacion()
    {
    }

    public byte[] getContenido()
    {
        return this.contenido;
    }

    public void setContenido(byte[] contenido)
    {
        this.contenido = contenido;
    }

    public Long getContenidoIdiomaId()
    {
        return this.contenidoIdiomaId;
    }

    public void setContenidoIdiomaId(Long contenidoIdiomaId)
    {
        this.contenidoIdiomaId = contenidoIdiomaId;
    }

    public Long getContenidoId()
    {
        return contenidoId;
    }

    public void setContenidoId(Long contenidoId)
    {
        this.contenidoId = contenidoId;
    }

    public String getEsHtml()
    {
        return this.esHtml;
    }

    public void setEsHtml(String esHtml)
    {
        this.esHtml = esHtml;
    }

    public Date getFechaModificacion()
    {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion)
    {
        this.fechaModificacion = fechaModificacion;
    }

    public Date getFechaCreacion()
    {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion)
    {
        this.fechaCreacion = fechaCreacion;
    }

    public String getIdioma()
    {
        return this.idioma;
    }

    public void setIdioma(String idioma)
    {
        this.idioma = idioma;
    }

    public String getMimeType()
    {
        return this.mimeType;
    }

    public void setMimeType(String mimeType)
    {
        this.mimeType = mimeType;
    }

    public String getResumen()
    {
        return this.resumen;
    }

    public void setResumen(String resumen)
    {
        this.resumen = resumen;
    }

    public String getSubtitulo()
    {
        return this.subtitulo;
    }

    public void setSubtitulo(String subtitulo)
    {
        this.subtitulo = subtitulo;
    }

    public String getTitulo()
    {
        return this.titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getTituloLargo()
    {
        return this.tituloLargo;
    }

    public void setTituloLargo(String tituloLargo)
    {
        this.tituloLargo = tituloLargo;
    }

    public String getUrlNodo()
    {
        return urlNodo;
    }

    public void setUrlNodo(String urlNodo)
    {
        this.urlNodo = urlNodo;
    }

    public String getUrlContenido()
    {
        return urlContenido;
    }

    public void setUrlContenido(String urlContenido)
    {
        this.urlContenido = urlContenido;
    }

    public String getUrlCompleta()
    {
        return urlCompleta;
    }

    public void setUrlCompleta(String urlCompleta)
    {
        this.urlCompleta = urlCompleta;
    }

    public Integer getUrlNumNiveles()
    {
        return urlNumNiveles;
    }

    public void setUrlNumNiveles(Integer urlNumNiveles)
    {
        this.urlNumNiveles = urlNumNiveles;
    }

    public Integer getNivel()
    {
        return nivel;
    }

    public void setNivel(Integer nivel)
    {
        this.nivel = nivel;
    }

    public String getUrlNodoOriginal()
    {
        return urlNodoOriginal;
    }

    public void setUrlNodoOriginal(String urlNodoOriginal)
    {
        this.urlNodoOriginal = urlNodoOriginal;
    }

    public String getUrlCompletaOriginal()
    {
        return urlCompletaOriginal;
    }

    public String getUrlPathNodo()
    {
        return urlPathNodo;
    }

    public void setUrlPathNodo(String urlPathNodo)
    {
        this.urlPathNodo = urlPathNodo;
    }

    public void setUrlCompletaOriginal(String urlCompletaOriginal)
    {
        this.urlCompletaOriginal = urlCompletaOriginal;
    }

    public String getTipoContenido()
    {
        return tipoContenido;
    }

    public void setTipoContenido(String tipoContenido)
    {
        this.tipoContenido = tipoContenido;
    }

    public Long getNodoMapaOrden()
    {
        return nodoMapaOrden;
    }

    public void setNodoMapaOrden(Long nodoMapaOrden)
    {
        this.nodoMapaOrden = nodoMapaOrden;
    }

    public Long getNodoMapaPadreOrden()
    {
        return nodoMapaPadreOrden;
    }

    public void setNodoMapaPadreOrden(Long nodoMapaPadreOrden)
    {
        this.nodoMapaPadreOrden = nodoMapaPadreOrden;
    }


    public String getUrlDestino()
    {
        return urlDestino;
    }

    public void setUrlDestino(String urlDestino)
    {
        this.urlDestino = urlDestino;
    }

    public Long getContenidoOrden()
    {
        return contenidoOrden;
    }

    public void setContenidoOrden(Long contenidoOrden)
    {
        this.contenidoOrden = contenidoOrden;
    }

    public String getAutor()
    {
        return autor;
    }

    public void setAutor(String autor)
    {
        this.autor = autor;
    }

    public String getOrigenNombre()
    {
        return origenNombre;
    }

    public void setOrigenNombre(String origenNombre)
    {
        this.origenNombre = origenNombre;
    }

    public String getOrigenUrl()
    {
        return origenUrl;
    }

    public void setOrigenUrl(String origenUrl)
    {
        this.origenUrl = origenUrl;
    }

    public String getLugar()
    {
        return lugar;
    }

    public void setLugar(String lugar)
    {
        this.lugar = lugar;
    }

    public Date getProximaFechaVigencia()
    {
        return proximaFechaVigencia;
    }

    public void setProximaFechaVigencia(Date proximaFechaVigencia)
    {
        this.proximaFechaVigencia = proximaFechaVigencia;
    }

    public Date getPrimeraFechaVigencia()
    {
        return primeraFechaVigencia;
    }

    public void setPrimeraFechaVigencia(Date primeraFechaVigencia)
    {
        this.primeraFechaVigencia = primeraFechaVigencia;
    }

    public String getMetadata()
    {
        return metadata;
    }

    public void setMetadata(String metadata)
    {
        this.metadata = metadata;
    }

    public String getTituloNodo()
    {
        return tituloNodo;
    }

    public void setTituloNodo(String tituloNodo)
    {
        this.tituloNodo = tituloNodo;
    }

    public Long getOrden()
    {
        return orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public String getPrioridad()
    {
        return prioridad;
    }

    public void setPrioridad(String prioridad)
    {
        this.prioridad = prioridad;
    }

    public Float getDistanciaSysdate()
    {
        return distanciaSysdate;
    }

    public void setDistanciaSysdate(Float distanciaSysdate)
    {
        this.distanciaSysdate = distanciaSysdate;
    }

    public String getTipoFormato()
    {
        return tipoFormato;
    }

    public void setTipoFormato(String tipoFormato)
    {
        this.tipoFormato = tipoFormato;
    }

    public String getAtributos()
    {
        return atributos;
    }

    public void setAtributos(String atributos)
    {
        this.atributos = atributos;
    }

    public String getVisible()
    {
        return visible;
    }

    public void setVisible(String visible)
    {
        this.visible = visible;
    }

    public String getTextoNoVisible()
    {
        return textoNoVisible;
    }

    public void setTextoNoVisible(String textoNoVisible)
    {
        this.textoNoVisible = textoNoVisible;
    }

    public String getPublicable()
    {
        return publicable;
    }

    public void setPublicable(String publicable)
    {
        this.publicable = publicable;
    }

    public String getTags()
    {
        return tags;
    }

    public void setTags(String tags)
    {
        this.tags = tags;
    }

    public String getFechasEvento()
    {
        return fechasEvento;
    }

    public void setFechasEvento(String fechasEvento)
    {
        this.fechasEvento = fechasEvento;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }

        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Publicacion that = (Publicacion) o;

        if (!urlCompleta.equals(that.urlCompleta))
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        return urlCompleta.hashCode();
    }
}
