package es.uji.apps.upo.model;

import es.uji.apps.upo.dao.NodoMapaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Component
@Entity
@Table(name = "UPO_MAPAS")
@SuppressWarnings("serial")
public class NodoMapa implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "URL_PATH")
    private String urlPath;

    @Column(name = "URL_COMPLETA")
    private String urlCompleta;

    @Column(name = "MENU_HEREDADO")
    private Boolean menuHeredado;

    @Column(name = "PRIVADO")
    private Boolean privado;

    @Column(name = "ORDEN")
    private Long orden;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CRITERIO_ID")
    private Criterio upoCriterio;

    @ManyToOne
    @JoinColumn(name = "FRANQUICIA_ID")
    private Franquicia upoFranquicia;

    @ManyToOne
    @JoinColumn(name = "MENU_ID", nullable = false)
    private Menu menu;

    @ManyToOne()
    @JoinColumn(name = "MAPA_ID")
    private NodoMapa upoMapa;

    @Column(name = "NUM_ITEMS")
    private Long numItemsSincro;

    @Column(name = "URL_NUM_NIVELES")
    private Long urlNumNiveles;

    @Column(name = "SINCRO_DESDE_MAPA")
    private String sincroDesdeNodoMapa;

    @Column(name = "PERIODICIDAD_SINCRO")
    private Integer periodicidadSincro;

    @Column(name = "NUM_NIVELES_SINCRO")
    private Integer numNivelesSincro;

    @Column(name = "TITULO_PUBLICACION_CA")
    private String tituloPublicacionCA;

    @Column(name = "TITULO_PUBLICACION_ES")
    private String tituloPublicacionES;

    @Column(name = "TITULO_PUBLICACION_EN")
    private String tituloPublicacionEN;

    @Column(name = "ENLACE_DESTINO")
    private String enlaceDestino;

    @Column(name = "ES_CONTENIDO_RELACIONADO")
    private Boolean contenidoRelacionado;

    @Column(name = "ACTIVAR_PROPUESTAS_WEB")
    private Boolean activarPropuestasWeb;

    @Column(name = "DESCRIPCION_PROPUESTAS_WEB")
    private String descripcionPropuestasWeb;

    @Column(name = "MOSTRAR_RELOJ")
    private Boolean mostrarReloj;

    @Column(name = "MOSTRAR_MENU_PERFILES_PLEGADO")
    private Boolean mostrarMenuPerfilesPlegado;

    @Column(name = "MOSTRAR_RECURSOS_RELACIONADOS")
    private Boolean mostrarRecursosRelacionados;

    @Column(name = "MOSTRAR_TAGS")
    private Boolean mostrarTags;

    @Column(name = "MOSTRAR_FECHA_MODIFICACION")
    private Boolean mostrarFechaModificacion;

    @Column(name = "MOSTRAR_FUENTE")
    private Boolean mostrarFuente;

    @Column(name = "MOSTRAR_REDES_SOCIALES")
    private Boolean mostrarRedesSociales;

    @Column(name = "MOSTRAR_INF_PROPORCIONADA")
    private Boolean mostrarInformacionProporcionada;

    @Column(name = "MOSTRAR_BARRA_GRIS")
    private Boolean mostrarBarraGris;

    private Boolean paginado;

    private Boolean bloqueado;

    @Column(name = "TITULO_MIGA_CA")
    private String tituloMigaCA;

    @Column(name = "TITULO_MIGA_ES")
    private String tituloMigaES;

    @Column(name = "TITULO_MIGA_EN")
    private String tituloMigaEN;

    @Column(name = "MOSTRAR_COMO_MIGA")
    private Boolean mostrarComoMiga;

    @Column(name = "MOSTRAR_FECHAS_EVENTO")
    private Boolean mostrarFechasEvento;

    @Column(name = "PROPUESTA_MAPA_ID")
    private Long propuestaMapaId;

    @ManyToOne
    @JoinColumn(name = "PER_ID_PROPUESTA")
    private Persona personaPropuesta;

    @OneToMany(mappedBy = "upoMapa", cascade = CascadeType.ALL)
    private Set<NodoMapa> upoMapas;

    @OneToMany(mappedBy = "upoMapa", cascade = CascadeType.ALL)
    private Set<NodoMapaExclusionIdioma> upoMapasIdiomasExclusiones;

    @OneToMany(mappedBy = "upoMapa", cascade = CascadeType.ALL)
    private Set<NodoMapaContenido> upoMapasObjetos;

    @OneToMany(mappedBy = "upoMapa", cascade = CascadeType.ALL)
    private Set<NodoMapaPlantilla> upoMapasPlantillas;

    @OneToMany(mappedBy = "upoMapa", cascade = CascadeType.ALL)
    private Set<ModeracionLote> upoModeracionLotes;

    @OneToMany(mappedBy = "nodoMapa", cascade = CascadeType.ALL)
    private Set<Autoguardado> autoguardado;

    @OneToMany(mappedBy = "upoMapa", cascade = CascadeType.ALL)
    private Set<NodoMapaTag> upoMapasTags;

    @Transient
    private NodoMapaDAO nodoMapaDAO;

    public NodoMapa()
    {
        this.upoMapas = new HashSet<NodoMapa>();
        this.upoMapasObjetos = new HashSet<NodoMapaContenido>();
        this.setMenuHeredado(true);
        this.setPrivado(false);
        this.setContenidoRelacionado(true);
        this.setActivarPropuestasWeb(false);
        this.setMostrarReloj(false);
        this.setMostrarMenuPerfilesPlegado(true);
        this.setMostrarRecursosRelacionados(false);
        this.setMostrarTags(false);
        this.setMostrarFechaModificacion(true);
        this.setMostrarFuente(true);
        this.setMostrarRedesSociales(true);
        this.setMostrarInformacionProporcionada(true);
        this.setMostrarBarraGris(true);
        this.setPaginado(false);
        this.setBloqueado(false);
        this.setMostrarComoMiga(false);
        this.setMostrarFechasEvento(false);
    }

    @Autowired
    public void setNodoMapaDAO(NodoMapaDAO nodoMapaDAO)
    {
        this.nodoMapaDAO = nodoMapaDAO;
    }

    public NodoMapa(String url)
    {
        this();
        this.urlCompleta = url;
    }

    @SuppressWarnings("unused")
    @PrePersist
    private void setOrden()
    {
        if (getOrden() == null)
        {
            this.orden = nodoMapaDAO.getNewOrden(this.getUpoMapa());
        }
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getUrlPath()
    {
        return this.urlPath;
    }

    public void setUrlPath(String urlPath)
    {
        this.urlPath = normalizeUrlPath(urlPath);
    }

    public String normalizeUrlPath(String urlPath)
    {
        return urlPath.replaceAll("[^A-Za-z0-9_.\\-]+", "");
    }

    public Long getNumItemsSincro()
    {
        return this.numItemsSincro;
    }

    public void setNumItemsSincro(Long numItemsSincro)
    {
        this.numItemsSincro = numItemsSincro;
    }

    public String getSincroDesdeNodoMapa()
    {
        return this.sincroDesdeNodoMapa;
    }

    public void setSincroDesdeNodoMapa(String sincroDesdeNodoMapa)
    {
        this.sincroDesdeNodoMapa = sincroDesdeNodoMapa;
    }

    public Integer getPeriodicidadSincro()
    {
        return this.periodicidadSincro;
    }

    public void setPeriodicidadSincro(Integer periodicidadSincro)
    {
        this.periodicidadSincro = periodicidadSincro;
    }

    public Integer getNumNivelesSincro()
    {
        return this.numNivelesSincro;
    }

    public void setNumNivelesSincro(Integer numNivelesSincro)
    {
        this.numNivelesSincro = numNivelesSincro;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public String getUrlCompleta()
    {
        return this.urlCompleta;
    }

    public void setUrlCompleta(String urlCompleta)
    {
        this.urlCompleta = urlCompleta;
    }

    public Criterio getUpoCriterio()
    {
        return this.upoCriterio;
    }

    public void setUpoCriterio(Criterio upoCriterio)
    {
        this.upoCriterio = upoCriterio;
    }

    public Franquicia getUpoFranquicia()
    {
        return this.upoFranquicia;
    }

    public void setUpoFranquicia(Franquicia upoFranquicia)
    {
        this.upoFranquicia = upoFranquicia;
    }

    public NodoMapa getUpoMapa()
    {
        return this.upoMapa;
    }

    public void setUpoMapa(NodoMapa upoMapa)
    {
        this.upoMapa = upoMapa;
    }

    public Set<NodoMapa> getUpoMapas()
    {
        return this.upoMapas;
    }

    public void setUpoMapas(Set<NodoMapa> upoMapas)
    {
        this.upoMapas = upoMapas;
    }

    public Set<NodoMapaExclusionIdioma> getUpoMapasIdiomasExclusiones()
    {
        return this.upoMapasIdiomasExclusiones;
    }

    public void setUpoMapasIdiomasExclusiones(Set<NodoMapaExclusionIdioma> upoMapasIdiomasExclusiones)
    {
        this.upoMapasIdiomasExclusiones = upoMapasIdiomasExclusiones;
    }

    public Set<NodoMapaContenido> getUpoMapasObjetos()
    {
        return this.upoMapasObjetos;
    }

    public void setUpoMapasObjetos(Set<NodoMapaContenido> upoMapasObjetos)
    {
        this.upoMapasObjetos = upoMapasObjetos;
    }

    public Set<NodoMapaPlantilla> getUpoMapasPlantillas()
    {
        return this.upoMapasPlantillas;
    }

    public void setUpoMapasPlantillas(Set<NodoMapaPlantilla> upoMapasPlantillas)
    {
        this.upoMapasPlantillas = upoMapasPlantillas;
    }

    public Set<ModeracionLote> getUpoModeracionLotes()
    {
        return this.upoModeracionLotes;
    }

    public void setUpoModeracionLotes(Set<ModeracionLote> upoModeracionLotes)
    {
        this.upoModeracionLotes = upoModeracionLotes;
    }

    public Menu getMenu()
    {
        return menu;
    }

    public void setMenu(Menu menu)
    {
        this.menu = menu;
    }

    public Set<Autoguardado> getAutoguardado()
    {
        return autoguardado;
    }

    public void setAutoguardado(Set<Autoguardado> autoguardado)
    {
        this.autoguardado = autoguardado;
    }

    public void setMenuHeredado(Boolean menuHeredado)
    {
        this.menuHeredado = menuHeredado;
    }

    public Boolean isMenuHeredado()
    {
        return menuHeredado;
    }

    public Boolean isPrivado()
    {
        return privado;
    }

    public void setPrivado(Boolean privado)
    {
        this.privado = privado;
    }

    public Boolean isContenidoRelacionado()
    {
        return contenidoRelacionado;
    }

    public void setContenidoRelacionado(Boolean contenidoRelacionado)
    {
        this.contenidoRelacionado = contenidoRelacionado;
    }

    public Boolean isActivarPropuestasWeb()
    {
        return activarPropuestasWeb;
    }

    public void setActivarPropuestasWeb(Boolean activarPropuestasWeb)
    {
        this.activarPropuestasWeb = activarPropuestasWeb;
    }

    public String getDescripcionPropuestasWeb()
    {
        return descripcionPropuestasWeb;
    }

    public void setDescripcionPropuestasWeb(String descripcionPropuestasWeb)
    {
        this.descripcionPropuestasWeb = descripcionPropuestasWeb;
    }

    public Boolean isMostrarReloj()
    {
        return mostrarReloj;
    }

    public void setMostrarReloj(Boolean mostrarReloj)
    {
        this.mostrarReloj = mostrarReloj;
    }

    public Boolean isMostrarMenuPerfilesPlegado()
    {
        return mostrarMenuPerfilesPlegado;
    }

    public void setMostrarMenuPerfilesPlegado(Boolean mostrarMenuPerfilesPlegado)
    {
        this.mostrarMenuPerfilesPlegado = mostrarMenuPerfilesPlegado;
    }

    public Boolean isMostrarRecursosRelacionados()
    {
        return mostrarRecursosRelacionados;
    }

    public void setMostrarRecursosRelacionados(Boolean mostrarRecursosRelacionados)
    {
        this.mostrarRecursosRelacionados = mostrarRecursosRelacionados;
    }

    public Boolean isMostrarTags()
    {
        return mostrarTags;
    }

    public void setMostrarTags(Boolean mostrarTags)
    {
        this.mostrarTags = mostrarTags;
    }

    public Boolean isMostrarFechaModificacion()
    {
        return mostrarFechaModificacion;
    }

    public void setMostrarFechaModificacion(Boolean mostrarFechaModificacion)
    {
        this.mostrarFechaModificacion = mostrarFechaModificacion;
    }

    public Boolean isMostrarFuente()
    {
        return mostrarFuente;
    }

    public void setMostrarFuente(Boolean mostrarFuente)
    {
        this.mostrarFuente = mostrarFuente;
    }

    public Boolean isMostrarRedesSociales()
    {
        return mostrarRedesSociales;
    }

    public void setMostrarRedesSociales(Boolean mostrarRedesSociales)
    {
        this.mostrarRedesSociales = mostrarRedesSociales;
    }

    public Boolean isMostrarInformacionProporcionada()
    {
        return mostrarInformacionProporcionada;
    }

    public void setMostrarInformacionProporcionada(Boolean mostrarInformacionProporcionada)
    {
        this.mostrarInformacionProporcionada = mostrarInformacionProporcionada;
    }

    public Boolean isMostrarBarraGris()
    {
        return mostrarBarraGris;
    }

    public void setMostrarBarraGris(Boolean mostrarBarraGris)
    {
        this.mostrarBarraGris = mostrarBarraGris;
    }

    public Boolean isPaginado()
    {
        return paginado;
    }

    public void setPaginado(Boolean paginado)
    {
        this.paginado = paginado;
    }

    public Boolean isBloqueado()
    {
        return bloqueado;
    }

    public void setBloqueado(Boolean bloqueado)
    {
        this.bloqueado = bloqueado;
    }

    public String getTituloPublicacionCA()
    {
        return tituloPublicacionCA;
    }

    public void setTituloPublicacionCA(String tituloPublicacionCA)
    {
        this.tituloPublicacionCA = tituloPublicacionCA;
    }

    public String getTituloPublicacionES()
    {
        return tituloPublicacionES;
    }

    public void setTituloPublicacionES(String tituloPublicacionES)
    {
        this.tituloPublicacionES = tituloPublicacionES;
    }

    public String getTituloPublicacionEN()
    {
        return tituloPublicacionEN;
    }

    public void setTituloPublicacionEN(String tituloPublicacionEN)
    {
        this.tituloPublicacionEN = tituloPublicacionEN;
    }

    public String getEnlaceDestino()
    {
        return enlaceDestino;
    }

    public void setEnlaceDestino(String enlaceDestino)
    {
        this.enlaceDestino = enlaceDestino;
    }

    public boolean isSetContenidos()
    {
        return (upoCriterio != null && numItemsSincro != null && numItemsSincro.intValue() > 0);
    }

    public void addNodoRelacionado(NodoMapa nodo)
    {
        this.upoMapas.add(nodo);
    }

    public void addNodoMapaContenidoRelacionado(NodoMapaContenido nodoMapaContenido)
    {
        this.upoMapasObjetos.add(nodoMapaContenido);
    }

    public Persona getPersonaPropuesta()
    {
        return personaPropuesta;
    }

    public void setPersonaPropuesta(Persona personaPropuesta)
    {
        this.personaPropuesta = personaPropuesta;
    }

    public Long getUrlNumNiveles()
    {
        return urlNumNiveles;
    }

    public String getTituloMigaCA()
    {
        return tituloMigaCA;
    }

    public void setTituloMigaCA(String tituloMigaCA)
    {
        this.tituloMigaCA = tituloMigaCA;
    }

    public String getTituloMigaES()
    {
        return tituloMigaES;
    }

    public void setTituloMigaES(String tituloMigaES)
    {
        this.tituloMigaES = tituloMigaES;
    }

    public String getTituloMigaEN()
    {
        return tituloMigaEN;
    }

    public void setTituloMigaEN(String tituloMigaEN)
    {
        this.tituloMigaEN = tituloMigaEN;
    }

    public Boolean isMostrarComoMiga()
    {
        return mostrarComoMiga;
    }

    public void setMostrarComoMiga(Boolean mostrarComoMiga)
    {
        this.mostrarComoMiga = mostrarComoMiga;
    }

    public Boolean isMostrarFechasEvento()
    {
        return mostrarFechasEvento;
    }

    public void setMostrarFechasEvento(Boolean mostrarFechasEvento)
    {
        this.mostrarFechasEvento = mostrarFechasEvento;
    }

    public Long getPropuestaMapaId()
    {
        return propuestaMapaId;
    }

    public void setPropuestaMapaId(Long propuestaMapaId)
    {
        this.propuestaMapaId = propuestaMapaId;
    }

    public Set<NodoMapaTag> getUpoMapsTags()
    {
        return upoMapasTags;
    }

    public void setTags(Set<NodoMapaTag> upoMapasTags)
    {
        this.upoMapasTags = upoMapasTags;
    }

    public void setCommonInheritedfields(NodoMapa parentNodoMapa, NodoMapa newNodoMapa,
            NodoMapaPlantilla nodoMapaPlantillaOriginal)
    {
        newNodoMapa.setUpoMapa(parentNodoMapa);
        newNodoMapa.setMenu(parentNodoMapa.getMenu());
        newNodoMapa.setMenuHeredado(true);
        newNodoMapa.setUpoFranquicia(parentNodoMapa.getUpoFranquicia());

        if (nodoMapaPlantillaOriginal != null)
        {
            NodoMapaPlantilla nodoMapaPlantillaCopia = new NodoMapaPlantilla();
            nodoMapaPlantillaCopia.setNivel(nodoMapaPlantillaOriginal.getNivel());
            nodoMapaPlantillaCopia.setUpoPlantilla(nodoMapaPlantillaOriginal.getUpoPlantilla());
            nodoMapaPlantillaCopia.setUpoMapa(newNodoMapa);

            newNodoMapa.setUpoMapasPlantillas(Collections.singleton(nodoMapaPlantillaCopia));
        }
    }

    public String getNuevaUrlCompleta(String url, String nombreNodo)
    {
        Integer posicionDelNodo = url.lastIndexOf(this.getUrlPath());
        String urlBase = url.substring(0, posicionDelNodo);

        return urlBase + nombreNodo + '/';
    }

    public String getUrlBaseSinNodo()
    {
        String url = this.getUrlCompleta();
        Integer posicionDelNodo = url.lastIndexOf(this.getUrlPath());

        return url.substring(0, posicionDelNodo);
    }
}