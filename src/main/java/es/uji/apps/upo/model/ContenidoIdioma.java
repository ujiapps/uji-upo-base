package es.uji.apps.upo.model;

import es.uji.apps.upo.exceptions.UsuarioNoAutenticadoException;
import es.uji.apps.upo.model.enums.TipoFormatoContenido;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.Set;

@Component
@Entity
@Table(name = "UPO_OBJETOS_IDIOMAS")
@SuppressWarnings("serial")
public class ContenidoIdioma implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Lob
    private byte[] contenido;

    @Column(name = "MIME_TYPE")
    private String mimeType;

    private String resumen;

    private String titulo;

    private String subtitulo;

    private String html;

    private Long longitud;

    @Column(name = "TIPO_FORMATO")
    private String tipoFormato;

    @Column(name = "TITULO_LARGO")
    private String tituloLargo;

    @Column(name = "ENLACE_DESTINO")
    private String enlaceDestino;

    @Column(name = "NOMBRE_FICHERO")
    private String nombreFichero;

    @Column(name = "ID_DESCARGA")
    private String idDescarga;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_MODIFICACION")
    private Date fechaModificacion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDIOMA_ID")
    private Idioma upoIdioma;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OBJETO_ID")
    private Contenido upoObjeto;

    @OneToMany(mappedBy = "contenidoIdioma", cascade = CascadeType.ALL)
    private Set<ContenidoIdiomaRecurso> contenidoIdiomaRecursos;

    @OneToMany(mappedBy = "contenidoIdioma", cascade = CascadeType.ALL)
    private Set<ContenidoIdiomaAtributo> contenidoIdiomaAtributos;

    public ContenidoIdioma()
    {
        this.html = "S";
        this.tipoFormato = TipoFormatoContenido.PAGINA.toString();
        this.setFechaModificacion(new Date());
        this.longitud = 0L;
    }

    public ContenidoIdioma(Idioma idioma)
    {
        this();
        this.upoIdioma = idioma;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public byte[] getContenido()
    {
        return this.contenido;
    }

    public void setContenido(byte[] contenido)
    {
        this.contenido = (byte[]) contenido;
    }

    public String getMimeType()
    {
        return this.mimeType;
    }

    public void setMimeType(String mimeType)
    {
        this.mimeType = mimeType;
    }

    public String getResumen()
    {
        return this.resumen;
    }

    public void setResumen(String resumen)
    {
        this.resumen = resumen;
    }

    public String getTitulo()
    {
        return this.titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getSubtitulo()
    {
        return this.subtitulo;
    }

    public void setSubtitulo(String subtitulo)
    {
        this.subtitulo = subtitulo;
    }

    public String getTipoFormato()
    {
        return this.tipoFormato;
    }

    public void setTipoFormato(String tipoFormato)
    {
        this.tipoFormato = tipoFormato;
    }

    public String getTituloLargo()
    {
        return this.tituloLargo;
    }

    public void setTituloLargo(String tituloLargo)
    {
        this.tituloLargo = tituloLargo;
    }

    public String getEnlaceDestino()
    {
        return this.enlaceDestino;
    }

    public void setEnlaceDestino(String enlaceDestino)
    {
        this.enlaceDestino = enlaceDestino;
    }

    public Idioma getUpoIdioma()
    {
        return this.upoIdioma;
    }

    public void setUpoIdioma(Idioma upoIdioma)
    {
        this.upoIdioma = upoIdioma;
    }

    public Contenido getUpoObjeto()
    {
        return this.upoObjeto;
    }

    public void setUpoObjeto(Contenido upoObjeto)
    {
        this.upoObjeto = upoObjeto;
    }

    public String getHtml()
    {
        return html;
    }

    public void setHtml(String html)
    {
        this.html = html.trim();
    }

    public Long getLongitud()
    {
        return longitud;
    }

    public void setLongitud(Long longitud)
    {
        this.longitud = longitud;
    }

    public void setContenidoIdiomaRecursos(Set<ContenidoIdiomaRecurso> contenidoIdiomaRecursos)
    {
        this.contenidoIdiomaRecursos = contenidoIdiomaRecursos;
    }

    public Set<ContenidoIdiomaRecurso> getContenidoIdiomaRecursos()
    {
        return contenidoIdiomaRecursos;
    }

    public void setContenidoIdiomaAtributos(Set<ContenidoIdiomaAtributo> contenidoIdiomaAtributos)
    {
        this.contenidoIdiomaAtributos = contenidoIdiomaAtributos;
    }

    public Set<ContenidoIdiomaAtributo> getContenidoIdiomaAtributos()
    {
        return contenidoIdiomaAtributos;
    }

    public void setNombreFichero(String nombreFichero)
    {
        this.nombreFichero = nombreFichero;
    }

    public String getNombreFichero()
    {
        return nombreFichero;
    }

    public void setIdDescarga(String idDescarga)
    {
        this.idDescarga = idDescarga;
    }

    public String getIdDescarga()
    {
        return idDescarga;
    }

    public Date getFechaModificacion()
    {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion)
    {
        this.fechaModificacion = fechaModificacion;
    }

    private void verificarUsuarioConectado(Persona persona)
            throws UsuarioNoAutenticadoException
    {
        if (persona == null)
        {
            throw new UsuarioNoAutenticadoException();
        }
    }

    public boolean esIgual(String titulo, String subtitulo, String tituloLargo, String isHtml, String valorContenido,
            String tipoMimeFichero, byte[] dataBinary, String nombreFichero, String resumen, String enlaceDestino,
            String tipoFormato)
    {
        if (!sonIguales(titulo, getTitulo())) return false;
        if (!sonIguales(subtitulo, getSubtitulo())) return false;
        if (!sonIguales(tituloLargo, getTituloLargo())) return false;
        if (!sonIguales(enlaceDestino, getEnlaceDestino())) return false;
        if (!sonIguales(resumen, getResumen())) return false;
        if (!sonIguales(isHtml, getHtml())) return false;
        if (!sonIguales(longitud, getLongitud())) return false;
        if (!sonIguales(tipoFormato, getTipoFormato())) return false;

        if ("S".equals(isHtml))
        {
            if (valorContenido == null || "".equals(valorContenido))
            {
                if (getContenido() != null) return false;
            }
            else if (!Arrays.equals(valorContenido.getBytes(), getContenido())) return false;
        }
        else
        {
            if (dataBinary != null && dataBinary.length > 0)
            {
                if (!sonIguales(tipoMimeFichero, getMimeType())) return false;
                if (!sonIguales(nombreFichero, getNombreFichero())) return false;

                if (dataBinary == null || "".equals(dataBinary))
                {
                    if (getContenido() != null) return false;
                }
                else if (!Arrays.equals(dataBinary, getContenido())) return false;
            }

            return true;
        }

        return true;
    }

    public boolean sonIguales(String campo1, String campo2)
    {
        if ((campo1 == null || campo1.isEmpty()) && campo2 == null)
        {
            return true;
        }

        if (campo1 != null && campo2 != null)
        {
            if (campo1.equals(campo2) || (campo1.isEmpty() && campo2.isEmpty()))
            {
                return true;
            }
        }

        return false;
    }

    public boolean sonIguales(Long campo1, Long campo2)
    {
        if (campo1 == null && campo2 == null)
        {
            return true;
        }

        if (campo1 != null && campo2 != null)
        {
            if (campo1.equals(campo2))
            {
                return true;
            }
        }

        return false;
    }

    public boolean isHTML()
    {
        return "S".equals(getHtml());
    }

    public boolean hasFormat(String formato)
    {
        return TipoFormatoContenido.valueOf(formato.toUpperCase()).toString().equals(getTipoFormato().toUpperCase());
    }
}