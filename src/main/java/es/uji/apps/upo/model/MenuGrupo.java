package es.uji.apps.upo.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "UPO_MENUS_GRUPOS")
@SuppressWarnings("serial")
public class MenuGrupo implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "GRUPO_ID")
    private Grupo grupo;

    @ManyToOne
    @JoinColumn(name = "MENU_ID")
    private Menu menu;

    private Integer orden;

    public MenuGrupo()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Grupo getGrupo()
    {
        return grupo;
    }

    public void setGrupo(Grupo grupo)
    {
        this.grupo = grupo;
    }

    public void setMapasMenu(Menu menu)
    {
        this.menu = menu;
    }

    public Menu getMenu()
    {
        return menu;
    }

    public Integer getOrden()
    {
        return orden;
    }

    public void setOrden(Integer orden)
    {
        this.orden = orden;
    }
}