package es.uji.apps.upo.model;

import java.io.Serializable;

import javax.persistence.*;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "UPO_OBJETOS_IDIOMAS_ATRIBUTOS")
@SuppressWarnings("serial")
public class ContenidoIdiomaAtributo implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String clave;
    private String valor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OBJETO_IDIOMA_ID")
    private ContenidoIdioma contenidoIdioma;

    public ContenidoIdiomaAtributo()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public ContenidoIdioma getContenidoIdioma()
    {
        return contenidoIdioma;
    }

    public void setContenidoIdioma(ContenidoIdioma contenidoIdioma)
    {
        this.contenidoIdioma = contenidoIdioma;
    }

    public String getClave()
    {
        return clave;
    }

    public void setClave(String clave)
    {
        this.clave = clave;
    }

    public String getValor()
    {
        return valor;
    }

    public void setValor(String valor)
    {
        this.valor = valor;
    }
}
