package es.uji.apps.upo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "UPO_VW_BINARIOS_BUSQUEDA")
public class BinarioBusqueda implements Serializable
{
    @Id
    @Column(name = "OBJETO_IDIOMA_ID")
    private Long contenidoIdiomaId;

    @Id
    @Column(name = "OBJETO_ID")
    private Long objetoId;

    @Column(name = "MAPA_URL_COMPLETA")
    private String mapaUrlCompleta;

    @Column(name = "URL_NUM_NIVELES")
    private Integer urlNumNiveles;

    @Column(name = "MIME_TYPE")
    private String ficheroMimeType;

    @Column(name = "NOMBRE_FICHERO")
    private String ficheroNombre;

    @Column(name = "IDIOMA_CODIGO_ISO")
    private String idiomaCodigoIso;

    @Column(name = "NOMBRE_FICHERO_SIN_ACENTOS")
    private String nombreFicheroSinAcentos;

    @Column(name = "TITULO_SIN_ACENTOS")
    private String tituloSinAcentos;

    @Column(name = "TITULO_LARGO_SIN_ACENTOS")
    private String tituloLargoSinAcentos;

    @Column(name = "SUBTITULO_SIN_ACENTOS")
    private String subtituloSinAcentos;

    @Column(name = "RESUMEN_SIN_ACENTOS")
    private String resumenSinAcentos;

    @Column(name = "ID_DESCARGA")
    private String descargaId;

    public BinarioBusqueda()
    {
    }

    public Long getContenidoIdiomaId()
    {
        return contenidoIdiomaId;
    }

    public void setContenidoIdiomaId(Long contenidoIdiomaId)
    {
        this.contenidoIdiomaId = contenidoIdiomaId;
    }

    public Long getObjetoId()
    {
        return objetoId;
    }

    public void setObjetoId(Long objetoId)
    {
        this.objetoId = objetoId;
    }

    public String getMapaUrlCompleta()
    {
        return mapaUrlCompleta;
    }

    public void setMapaUrlCompleta(String mapaUrlCompleta)
    {
        this.mapaUrlCompleta = mapaUrlCompleta;
    }

    public String getFicheroMimeType()
    {
        return ficheroMimeType;
    }

    public void setFicheroMimeType(String ficheroMimeType)
    {
        this.ficheroMimeType = ficheroMimeType;
    }

    public String getFicheroNombre()
    {
        return ficheroNombre;
    }

    public void setFicheroNombre(String ficheroNombre)
    {
        this.ficheroNombre = ficheroNombre;
    }

    public String getIdiomaCodigoIso()
    {
        return idiomaCodigoIso;
    }

    public void setIdiomaCodigoIso(String idiomaCodigoIso)
    {
        this.idiomaCodigoIso = idiomaCodigoIso;
    }

    public String getNombreFicheroSinAcentos()
    {
        return nombreFicheroSinAcentos;
    }

    public void setNombreFicheroSinAcentos(String nombreFicheroSinAcentos)
    {
        this.nombreFicheroSinAcentos = nombreFicheroSinAcentos;
    }

    public String getTituloSinAcentos()
    {
        return tituloSinAcentos;
    }

    public void setTituloSinAcentos(String tituloSinAcentos)
    {
        this.tituloSinAcentos = tituloSinAcentos;
    }

    public String getTituloLargoSinAcentos()
    {
        return tituloLargoSinAcentos;
    }

    public void setTituloLargoSinAcentos(String tituloLargoSinAcentos)
    {
        this.tituloLargoSinAcentos = tituloLargoSinAcentos;
    }

    public String getSubtituloSinAcentos()
    {
        return subtituloSinAcentos;
    }

    public void setSubtituloSinAcentos(String subtituloSinAcentos)
    {
        this.subtituloSinAcentos = subtituloSinAcentos;
    }

    public String getResumenSinAcentos()
    {
        return resumenSinAcentos;
    }

    public void setResumenSinAcentos(String resumenSinAcentos)
    {
        this.resumenSinAcentos = resumenSinAcentos;
    }

    public Integer getUrlNumNiveles()
    {
        return urlNumNiveles;
    }

    public void setUrlNumNiveles(Integer urlNumNiveles)
    {
        this.urlNumNiveles = urlNumNiveles;
    }

    public String getDescargaId()
    {
        return descargaId;
    }

    public void setDescargaId(String descargaId)
    {
        this.descargaId = descargaId;
    }
}
