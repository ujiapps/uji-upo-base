package es.uji.apps.upo.model.eujier.enums;

public enum TipoVista
{
    CUADRICULA, LISTA;

    @Override
    public String toString()
    {
        return name().toLowerCase();
    }
}
