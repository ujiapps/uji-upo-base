package es.uji.apps.upo.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;

@Component
@Entity
@Table(name = "UPO_EXT_MIGRACION")
public class ItemMigracion
{
    @Id
    private String id;

    private String titulo;
    private String tipo;
    private String url;
    private String resumen;
    private String tags;

    @Column(name = "MIME_TYPE")
    private String mimeType;

    @Lob
    private byte[] contenido;

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String value)
    {
        this.url = value;
    }

    public byte[] getContenido()
    {
        return this.contenido;
    }

    public void setContenido(byte[] value)
    {
        this.contenido = value;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getResumen()
    {
        return resumen;
    }

    public void setResumen(String resumen)
    {
        this.resumen = resumen;
    }

    public String getTags()
    {
        return tags;
    }

    public void setTags(String tags)
    {
        this.tags = tags;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getMimeType()
    {
        return mimeType;
    }

    public void setMimeType(String mimeType)
    {
        this.mimeType = mimeType;
    }
}