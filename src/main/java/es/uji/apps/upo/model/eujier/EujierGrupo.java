package es.uji.apps.upo.model.eujier;

import java.io.Serializable;

import javax.persistence.*;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "XPFI_VW_GRUPOS")
@SuppressWarnings("serial")
public class EujierGrupo implements Serializable, Comparable
{
    @Id
    private String id;

    private String nombre;
    private String idioma;

    @Column(name = "region_id")
    private Long regionId;

    @Column(name = "aplicacion_id")
    private Long aplicacionId;

    @Column(name = "grupo_id")
    private Long grupoId;

    @Transient
    private Long numItems;

    @Transient
    private String aplicacionNombre;

    @Transient
    private String regionNombre;

    public EujierGrupo()
    {
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getIdioma()
    {
        return idioma;
    }

    public void setIdioma(String idioma)
    {
        this.idioma = idioma;
    }

    public Long getRegionId()
    {
        return regionId;
    }

    public void setRegionId(Long regionId)
    {
        this.regionId = regionId;
    }

    public Long getAplicacionId()
    {
        return aplicacionId;
    }

    public void setAplicacionId(Long aplicacionId)
    {
        this.aplicacionId = aplicacionId;
    }

    public Long getGrupoId()
    {
        return grupoId;
    }

    public void setGrupoId(Long grupoId)
    {
        this.grupoId = grupoId;
    }

    public Long getNumItems()
    {
        return numItems;
    }

    public void setNumItems(Long numItems)
    {
        this.numItems = numItems;
    }

    public String getAplicacionNombre()
    {
        return aplicacionNombre;
    }

    public void setAplicacionNombre(String aplicacionNombre)
    {
        this.aplicacionNombre = aplicacionNombre;
    }

    public String getRegionNombre()
    {
        return regionNombre;
    }

    public void setRegionNombre(String regionNombre)
    {
        this.regionNombre = regionNombre;
    }

    @Override
    public int compareTo(Object o)
    {
        if (this.nombre == null)
        {
            return 0;
        }

        if (this.regionNombre == null && this.aplicacionNombre == null)
        {
            return this.nombre.compareTo(((EujierGrupo) o).nombre);
        }

        if (this.regionNombre == null)
        {
            int sComp = this.aplicacionNombre.compareTo(((EujierGrupo) o).aplicacionNombre);

            if (sComp != 0) return sComp;

            return this.nombre.compareTo(((EujierGrupo) o).nombre);
        }

        int sComp = this.regionNombre.compareTo(((EujierGrupo) o).regionNombre);

        if (sComp != 0) return sComp;

        sComp = this.aplicacionNombre.compareTo(((EujierGrupo) o).aplicacionNombre);

        if (sComp != 0) return sComp;

        return this.nombre.compareTo(((EujierGrupo) o).nombre);
    }
}