package es.uji.apps.upo.model.metadatos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import es.uji.apps.upo.model.metadatos.AtributoMetadato;

@Component
@Entity
@Table(name = "UPO_VALORES_METADATOS")
@SuppressWarnings("serial")
public class ValorMetadato implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "VALOR_CA")
    private String valorCA;

    @Column(name = "VALOR_ES")
    private String valorES;

    @Column(name = "VALOR_EN")
    private String valorEN;

    private Integer orden;

    @ManyToOne
    @JoinColumn(name = "ATRIBUTO_ID")
    private AtributoMetadato atributoMetadato;

    public ValorMetadato()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getValorCA()
    {
        return valorCA;
    }

    public void setValorCA(String valorCA)
    {
        this.valorCA = valorCA;
    }

    public String getValorES()
    {
        return valorES;
    }

    public void setValorES(String valorES)
    {
        this.valorES = valorES;
    }

    public String getValorEN()
    {
        return valorEN;
    }

    public void setValorEN(String valorEN)
    {
        this.valorEN = valorEN;
    }

    public AtributoMetadato getAtributoMetadato()
    {
        return atributoMetadato;
    }

    public void setAtributoMetadato(AtributoMetadato atributoMetadato)
    {
        this.atributoMetadato = atributoMetadato;
    }

    public Integer getOrden()
    {
        return orden;
    }

    public void setOrden(Integer orden)
    {
        this.orden = orden;
    }
}
