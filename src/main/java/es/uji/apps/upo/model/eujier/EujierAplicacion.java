package es.uji.apps.upo.model.eujier;

import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Component
@Entity
@Table(name = "XPFI_VW_APLICACIONES")
@SuppressWarnings("serial")
public class EujierAplicacion implements Serializable
{
    @Id
    private String id;

    private String nombre;
    private String descripcion;

    @Column(name = "nombre_busqueda")
    private String nombreBusqueda;

    @Column(name = "descripcion_busqueda")
    private String descripcionBusqueda;

    private String idioma;
    private String color;
    private String colorClass;

    @Column(name = "region_id")
    private Long regionId;

    @Column(name = "region_nombre")
    private String regionNombre;

    @Column(name = "aplicacion_id")
    private Long aplicacionId;

    public EujierAplicacion()
    {
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getIdioma()
    {
        return idioma;
    }

    public void setIdioma(String idioma)
    {
        this.idioma = idioma;
    }

    public Long getRegionId()
    {
        return regionId;
    }

    public void setRegionId(Long regionId)
    {
        this.regionId = regionId;
    }

    public Long getAplicacionId()
    {
        return aplicacionId;
    }

    public void setAplicacionId(Long aplicacionId)
    {
        this.aplicacionId = aplicacionId;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public String getNombreBusqueda()
    {
        return nombreBusqueda;
    }

    public void setNombreBusqueda(String nombreBusqueda)
    {
        this.nombreBusqueda = nombreBusqueda;
    }

    public String getDescripcionBusqueda()
    {
        return descripcionBusqueda;
    }

    public void setDescripcionBusqueda(String descripcionBusqueda)
    {
        this.descripcionBusqueda = descripcionBusqueda;
    }

    public String getRegionNombre()
    {
        return regionNombre;
    }

    public void setRegionNombre(String regionNombre)
    {
        this.regionNombre = regionNombre;
    }

    public String getColorClass()
    {
        return colorClass;
    }

    public void setColorClass(String colorClass)
    {
        this.colorClass = colorClass;
    }
}