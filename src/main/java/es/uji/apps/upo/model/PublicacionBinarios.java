package es.uji.apps.upo.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "UPO_VW_PUBLICACION_BINARIOS")
public class PublicacionBinarios implements Serializable
{
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "URL_PATH")
    private String urlPath;

    @Lob
    private byte[] contenido;

    private String idioma;

    @Column(name = "MIME_TYPE")
    private String mimeType;

    @Column(name = "URL_NODO")
    private String urlNodo;

    @Column(name = "ID_DESCARGA")
    private String idDescarga;

    public PublicacionBinarios()
    {
    }

    public byte[] getContenido()
    {
        return contenido;
    }

    public void setContenido(byte[] contenido)
    {
        this.contenido = contenido;
    }

    public String getIdioma()
    {
        return idioma;
    }

    public void setIdioma(String idioma)
    {
        this.idioma = idioma;
    }

    public String getMimeType()
    {
        return mimeType;
    }

    public void setMimeType(String mimeType)
    {
        this.mimeType = mimeType;
    }

    public String getUrlNodo()
    {
        return urlNodo;
    }

    public void setUrlNodo(String urlNodo)
    {
        this.urlNodo = urlNodo;
    }

    public String getUrlPath()
    {
        return urlPath;
    }

    public void setUrlPath(String urlPath)
    {
        this.urlPath = urlPath;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getIdDescarga()
    {
        return idDescarga;
    }

    public void setIdDescarga(String idDescarga)
    {
        this.idDescarga = idDescarga;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }

        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        PublicacionBinarios that = (PublicacionBinarios) o;

        if (!(urlNodo + urlPath).equals(that.urlNodo + that.urlPath))
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        return (urlNodo + urlPath).hashCode();
    }
}