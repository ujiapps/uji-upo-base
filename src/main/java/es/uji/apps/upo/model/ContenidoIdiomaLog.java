package es.uji.apps.upo.model;

import es.uji.apps.upo.dao.ContenidoIdiomaLogDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Component
@Entity
@Table(name = "UPO_OBJETOS_IDIOMAS_LOG")
@SuppressWarnings("serial")
public class ContenidoIdiomaLog implements Serializable
{
    @Id
    private Long id;

    @Lob
    private byte[] contenido;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_OPERACION")
    private Date fechaOperacion;

    @Column(name = "IDIOMA_ID")
    private Long idiomaId;

    @Column(name = "MIME_TYPE")
    private String mimeType;

    @Column(name = "OBJETO_ID")
    private Long objetoId;
    private String resumen;

    @Column(name = "TIPO_OPERACION")
    private String tipoOperacion;

    private String titulo;
    private String subtitulo;
    private String html;
    private Long longitud;

    @Column(name = "TITULO_LARGO")
    private String tituloLargo;

    @Column(name = "ENLACE_DESTINO")
    private String enlaceDestino;

    @Column(name = "NOMBRE_FICHERO")
    private String nombreFichero;

    @Column(name = "USUARIO_OPERACION")
    private Long usuarioOperacion;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_MODIFICACION")
    private Date fechaModificacion;

    @Column(name = "TIPO_FORMATO")
    private String tipoFormato;

    public ContenidoIdiomaLog()
    {
    }

    public byte[] getContenido()
    {
        return this.contenido;
    }

    public void setContenido(byte[] contenido)
    {
        this.contenido = (byte[]) contenido;
    }

    public Date getFechaOperacion()
    {
        return this.fechaOperacion;
    }

    public void setFechaOperacion(Date fechaOperacion)
    {
        this.fechaOperacion = fechaOperacion;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getIdiomaId()
    {
        return this.idiomaId;
    }

    public void setIdiomaId(Long idiomaId)
    {
        this.idiomaId = idiomaId;
    }

    public String getMimeType()
    {
        return this.mimeType;
    }

    public void setMimeType(String mimeType)
    {
        this.mimeType = mimeType;
    }

    public Long getObjetoId()
    {
        return this.objetoId;
    }

    public void setObjetoId(Long objetoId)
    {
        this.objetoId = objetoId;
    }

    public String getResumen()
    {
        return this.resumen;
    }

    public void setResumen(String resumen)
    {
        this.resumen = resumen;
    }

    public String getTipoOperacion()
    {
        return this.tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion)
    {
        this.tipoOperacion = tipoOperacion;
    }

    public String getTitulo()
    {
        return this.titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getSubtitulo()
    {
        return subtitulo;
    }

    public void setSubtitulo(String subtitulo)
    {
        this.subtitulo = subtitulo;
    }

    public String getTituloLargo()
    {
        return this.tituloLargo;
    }

    public void setTituloLargo(String tituloLargo)
    {
        this.tituloLargo = tituloLargo;
    }

    public String getEnlaceDestino()
    {
        return this.enlaceDestino;
    }

    public void setEnlaceDestino(String enlaceDestino)
    {
        this.enlaceDestino = enlaceDestino;
    }

    public Long getUsuarioOperacion()
    {
        return this.usuarioOperacion;
    }

    public void setUsuarioOperacion(Long usuarioOperacion)
    {
        this.usuarioOperacion = usuarioOperacion;
    }

    public String getNombreFichero()
    {
        return nombreFichero;
    }

    public void setNombreFichero(String nombreFichero)
    {
        this.nombreFichero = nombreFichero;
    }

    public String getHtml()
    {
        return html;
    }

    public void setHtml(String html)
    {
        this.html = html;
    }

    public Date getFechaModificacion()
    {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion)
    {
        this.fechaModificacion = fechaModificacion;
    }

    public Long getLongitud()
    {
        return longitud;
    }

    public void setLongitud(Long longitud)
    {
        this.longitud = longitud;
    }

    public String getTipoFormato() {
        return tipoFormato;
    }

    public void setTipoFormato(String tipoFormato) {
        this.tipoFormato = tipoFormato;
    }
}