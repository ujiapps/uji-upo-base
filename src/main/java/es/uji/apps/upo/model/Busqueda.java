package es.uji.apps.upo.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "UPO_VW_BUSQUEDA")
public class Busqueda implements Serializable
{
    @Id
    @Column(name = "CONTENIDO_IDIOMA_ID")
    private Long contenidoIdiomaId;

    @Column(name = "MAPA_URL_COMPLETA")
    private String mapaUrlCompleta;

    @Column(name = "IDIOMA_CODIGO_ISO")
    private String idiomaCodigoIso;

    @Column(name = "TITULO_SIN_ACENTOS")
    private String tituloSinAcentos;

    @Column(name = "TITULO_LARGO_SIN_ACENTOS")
    private String tituloLargoSinAcentos;

    @Column(name = "SUBTITULO_SIN_ACENTOS")
    private String subtituloSinAcentos;

    @Column(name = "RESUMEN_SIN_ACENTOS")
    private String resumenSinAcentos;

    @Column(name = "CONTENIDO_SIN_ACENTOS")
    private String contenidoSinAcentos;

    @Column(name = "FECHA_VIGENCIA")
    private Date fechaVigencia;

    private Long orden;

    private String prioridad;

    @Column(name = "DISTANCIA_SYSDATE")
    private Float distanciaSysdate;

    public Busqueda()
    {
    }

    public String getMapaUrlCompleta()
    {
        return mapaUrlCompleta;
    }

    public void setMapaUrlCompleta(String mapaUrlCompleta)
    {
        this.mapaUrlCompleta = mapaUrlCompleta;
    }

    public String getIdiomaCodigoIso()
    {
        return idiomaCodigoIso;
    }

    public void setIdiomaCodigoIso(String idiomaCodigoIso)
    {
        this.idiomaCodigoIso = idiomaCodigoIso;
    }

    public String getTituloSinAcentos()
    {
        return tituloSinAcentos;
    }

    public void setTituloSinAcentos(String tituloSinAcentos)
    {
        this.tituloSinAcentos = tituloSinAcentos;
    }

    public String getTituloLargoSinAcentos()
    {
        return tituloLargoSinAcentos;
    }

    public void setTituloLargoSinAcentos(String tituloLargoSinAcentos)
    {
        this.tituloLargoSinAcentos = tituloLargoSinAcentos;
    }

    public String getSubtituloSinAcentos()
    {
        return subtituloSinAcentos;
    }

    public void setSubtituloSinAcentos(String subtituloSinAcentos)
    {
        this.subtituloSinAcentos = subtituloSinAcentos;
    }

    public String getResumenSinAcentos()
    {
        return resumenSinAcentos;
    }

    public void setResumenSinAcentos(String resumenSinAcentos)
    {
        this.resumenSinAcentos = resumenSinAcentos;
    }

    public String getContenidoSinAcentos()
    {
        return contenidoSinAcentos;
    }

    public void setContenidoSinAcentos(String contenidoSinAcentos)
    {
        this.contenidoSinAcentos = contenidoSinAcentos;
    }

    public Long getContenidoIdiomaId()
    {
        return contenidoIdiomaId;
    }

    public void setContenidoIdiomaId(Long contenidoIdiomaId)
    {
        this.contenidoIdiomaId = contenidoIdiomaId;
    }

    public Long getOrden()
    {
        return orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public String getPrioridad()
    {
        return prioridad;
    }

    public void setPrioridad(String prioridad)
    {
        this.prioridad = prioridad;
    }

    public Float getDistanciaSysdate()
    {
        return distanciaSysdate;
    }

    public void setDistanciaSysdate(Float distanciaSysdate)
    {
        this.distanciaSysdate = distanciaSysdate;
    }

    public Date getFechaVigencia()
    {
        return fechaVigencia;
    }

    public void setFechaVigencia(Date fechaVigencia)
    {
        this.fechaVigencia = fechaVigencia;
    }
}
