package es.uji.apps.upo.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
@Entity
@Table(name = "UPO_FECHAS_EVENTO")
@SuppressWarnings("serial")
public class FechaEvento implements Serializable, Comparable<FechaEvento>
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Date fecha;

    @Temporal(TemporalType.TIME)
    @Column(name = "HORA_INICIO")
    private Date horaInicio;

    @Temporal(TemporalType.TIME)
    @Column(name = "HORA_FIN")
    private Date horaFin;

    @Transient
    private SimpleDateFormat dateFormat;

    @Transient
    private SimpleDateFormat timeFormat;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MAPA_OBJETO_ID")
    private NodoMapaContenido nodoMapaContenido;

    public FechaEvento()
    {
        this.dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        this.timeFormat = new SimpleDateFormat("HH:mm");
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Date getFecha()
    {
        return this.fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public NodoMapaContenido getNodoMapaContenido()
    {
        return nodoMapaContenido;
    }

    public void setNodoMapaContenido(NodoMapaContenido nodoMapaContenido)
    {
        this.nodoMapaContenido = nodoMapaContenido;
    }

    public Date getHoraInicio()
    {
        return horaInicio;
    }

    public void setHoraInicio(Date horaInicio)
    {
        this.horaInicio = horaInicio;
    }

    public Date getHoraFin()
    {
        return horaFin;
    }

    public void setHoraFin(Date horaFin)
    {
        this.horaFin = horaFin;
    }

    @Override
    public int compareTo(FechaEvento fechaEvento)
    {
        if (fechaEvento.fecha.before(this.fecha))
        {
            return 1;
        }

        if (fechaEvento.fecha.equals(this.fecha))
        {
            return 0;
        }

        if (fechaEvento.fecha.after(this.fecha))
        {
            return -1;
        }

        return 0;
    }

    @Override
    public String toString()
    {
        String horaInicio = (this.horaInicio != null) ? timeFormat.format(this.horaInicio) : "";
        String horaFin = (this.horaFin != null) ? timeFormat.format(this.horaFin) : "";

        return dateFormat.format(fecha) + "|" + horaInicio + "|" + horaFin;
    }
}