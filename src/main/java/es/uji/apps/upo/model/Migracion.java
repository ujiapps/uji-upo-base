package es.uji.apps.upo.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.upo.dao.MigracionDAO;

@Component
@SuppressWarnings("serial")
@Entity
@Table(name = "UPO_VW_MIGRACION")
public class Migracion implements Serializable
{
    @Id
    private String id;

    private String url;
    private String tipo;

    public Migracion()
    {
    }

    public String getUrl()
    {
        return this.url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }
}