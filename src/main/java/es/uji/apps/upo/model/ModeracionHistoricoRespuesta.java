package es.uji.apps.upo.model;

import java.io.Serializable;
import java.util.List;

public class ModeracionHistoricoRespuesta implements Serializable
{
    private Long numResultados;
    private List<ModeracionHistorico> resultados;

    public Long getNumResultados()
    {
        return numResultados;
    }

    public void setNumResultados(Long numResultados)
    {
        this.numResultados = numResultados;
    }

    public List<ModeracionHistorico> getResultados()
    {
        return resultados;
    }

    public void setResultados(List<ModeracionHistorico> resultados)
    {
        this.resultados = resultados;
    }
}