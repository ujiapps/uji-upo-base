package es.uji.apps.upo.model;

import es.uji.apps.upo.storage.RecursoReservori;

import java.util.List;

public class ResultadoBusquedaBinarios
{
    private Long numResultados;
    private List<RecursoReservori> binarios;

    public Long getNumResultados()
    {
        return numResultados;
    }

    public void setNumResultados(Long numResultados)
    {
        this.numResultados = numResultados;
    }

    public List<RecursoReservori> getBinarios()
    {
        return binarios;
    }

    public void setBinarios(List<RecursoReservori> binarios)
    {
        this.binarios = binarios;
    }
}
