package es.uji.apps.upo.model.metadatos;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "UPO_TIPOS_METADATOS")
@SuppressWarnings("serial")
public class TipoMetadato implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    @OneToMany(mappedBy = "tipoMetadato")
    private Set<AtributoMetadato> atributosMetadatos;

    @OneToMany(mappedBy = "tipoMetadato")
    private Set<ContenidoMetadato> contenidosMetadatos;

    public TipoMetadato()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Set<AtributoMetadato> getAtributosMetadatos()
    {
        return atributosMetadatos;
    }

    public void setAtributosMetadatos(Set<AtributoMetadato> atributosMetadatos)
    {
        this.atributosMetadatos = atributosMetadatos;
    }

    public Set<ContenidoMetadato> getContenidosMetadatos()
    {
        return contenidosMetadatos;
    }

    public void setContenidosMetadatos(Set<ContenidoMetadato> contenidosMetadatos)
    {
        this.contenidosMetadatos = contenidosMetadatos;
    }
}
