package es.uji.apps.upo.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ClientConfiguration
{
    private static String host;
    private static String hostHttp;
    private static String heraEditoraUrl;

    @Autowired
    public void setApaDAO(@Value("${uji.webapp.host}") String host, @Value("${uji.webapp.host.http}") String hostHttp,
            @Value("${uji.hera.editora.url}") String heraEditoraUrl)
    {
        ClientConfiguration.host = host;
        ClientConfiguration.hostHttp = hostHttp;
        ClientConfiguration.heraEditoraUrl = heraEditoraUrl;
    }

    public static String getHost()
    {
        return host;
    }

    public static String getHostHttp()
    {
        return hostHttp;
    }

    public static String getHeraEditoraUrl()
    {
        return heraEditoraUrl;
    }
}
