package es.uji.apps.upo.model.enums;

public enum TipoFranquiciaAcceso
{
    ADMINISTRADOR, EDITOR;
}
