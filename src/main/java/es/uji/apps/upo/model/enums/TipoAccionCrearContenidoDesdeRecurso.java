package es.uji.apps.upo.model.enums;

public enum TipoAccionCrearContenidoDesdeRecurso
{
   COPIAR, ENLAZAR
}
