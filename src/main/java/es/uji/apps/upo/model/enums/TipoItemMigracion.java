package es.uji.apps.upo.model.enums;

public enum TipoItemMigracion
{
    TEXTO, BINARIO;
}
