package es.uji.apps.upo.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Component
@Entity
@Table(name = "UPO_MENUS")
@SuppressWarnings("serial")
public class Menu implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    @OneToMany(mappedBy = "menu")
    private Set<MenuGrupo> menusGrupos;

    @OneToMany(mappedBy = "menu")
    private Set<NodoMapa> nodoMapa;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PER_ID")
    private Persona persona;

    public Menu()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Set<MenuGrupo> getMenusGrupos()
    {
        return menusGrupos;
    }

    public void setMenusGrupos(Set<MenuGrupo> menusGrupos)
    {
        this.menusGrupos = menusGrupos;
    }

    public Set<NodoMapa> getNodoMapa()
    {
        return nodoMapa;
    }

    public void setNodoMapa(Set<NodoMapa> nodoMapa)
    {
        this.nodoMapa = nodoMapa;
    }

    public Persona getPersona()
    {
        return persona;
    }

    public void setPersona(Persona persona)
    {
        this.persona = persona;
    }
}