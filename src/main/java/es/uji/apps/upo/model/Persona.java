package es.uji.apps.upo.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.upo.dao.NodoMapaDAO;
import es.uji.apps.upo.dao.PersonaDAO;
import es.uji.commons.sso.dao.ApaDAO;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Component
@Entity
@Table(name = "UPO_EXT_PERSONAS")
@SuppressWarnings("serial")
public class Persona implements Serializable
{
    public static final String ROLE_ADMIN = "ADMIN";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String apellido1;

    private String apellido2;

    private String nombre;

    private String email;

    @Column(name = "NOMBRE_COMPLETO")
    private String nombreCompleto;

    @OneToMany(mappedBy = "upoExtPersona", cascade = CascadeType.ALL)
    private Set<FranquiciaAcceso> upoFranquiciasAccesos;

    @OneToMany(mappedBy = "upoExtPersona")
    private Set<NodoMapaContenido> upoMapasObjetos;

    @OneToMany(mappedBy = "upoExtPersona1")
    private Set<Contenido> upoObjetos1;

    @OneToMany(mappedBy = "upoExtPersonaSolicitud")
    private Set<ModeracionLote> upoModeracionLotes1;

    @OneToMany(mappedBy = "upoExtPersonaModeracion")
    private Set<ModeracionLote> upoModeracionLotes2;

    @OneToMany(mappedBy = "persona")
    private Set<MigracionUrlsPer> upoMigracionUrlsPers;

    @OneToMany(mappedBy = "persona")
    private Set<Item> items;

    @OneToMany(mappedBy = "persona")
    private Set<Grupo> grupos;

    @OneToMany(mappedBy = "persona")
    private Set<Menu> menus;

    public Persona()
    {
    }

    public Persona(Long id)
    {
        this.id = id;
    }
    
    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getApellido1()
    {
        return this.apellido1;
    }

    public void setApellido1(String apellido1)
    {
        this.apellido1 = apellido1;
    }

    public String getApellido2()
    {
        return this.apellido2;
    }

    public void setApellido2(String apellido2)
    {
        this.apellido2 = apellido2;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombreCompleto()
    {
        return this.nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto)
    {
        this.nombreCompleto = nombreCompleto;
    }

    public Set<FranquiciaAcceso> getUpoFranquiciasAccesos()
    {
        return this.upoFranquiciasAccesos;
    }

    public void setUpoFranquiciasAccesos(Set<FranquiciaAcceso> upoFranquiciasAccesos)
    {
        this.upoFranquiciasAccesos = upoFranquiciasAccesos;
    }

    public Set<NodoMapaContenido> getUpoMapasObjetos()
    {
        return this.upoMapasObjetos;
    }

    public void setUpoMapasObjetos(Set<NodoMapaContenido> upoMapasObjetos)
    {
        this.upoMapasObjetos = upoMapasObjetos;
    }

    public Set<Contenido> getUpoObjetos1()
    {
        return this.upoObjetos1;
    }

    public void setUpoObjetos1(Set<Contenido> upoObjetos1)
    {
        this.upoObjetos1 = upoObjetos1;
    }

    public Set<MigracionUrlsPer> getUpoMigracionUrlsPers()
    {
        return upoMigracionUrlsPers;
    }

    public void setUpoMigracionUrlsPers(Set<MigracionUrlsPer> upoMigracionUrlsPers)
    {
        this.upoMigracionUrlsPers = upoMigracionUrlsPers;
    }

    public Set<ModeracionLote> getUpoModeracionLotes1()
    {
        return this.upoModeracionLotes1;
    }

    public void setUpoModeracionLotes1(Set<ModeracionLote> upoModeracionLotes1)
    {
        this.upoModeracionLotes1 = upoModeracionLotes1;
    }

    public Set<ModeracionLote> getUpoModeracionLotes2()
    {
        return this.upoModeracionLotes2;
    }

    public void setUpoModeracionLotes2(Set<ModeracionLote> upoModeracionLotes2)
    {
        this.upoModeracionLotes2 = upoModeracionLotes2;
    }

    public Set<Item> getItems()
    {
        return items;
    }

    public void setItems(Set<Item> items)
    {
        this.items = items;
    }

    public Set<Grupo> getGrupos()
    {
        return grupos;
    }

    public void setGrupos(Set<Grupo> grupos)
    {
        this.grupos = grupos;
    }

    public Set<Menu> getMenus()
    {
        return menus;
    }

    public void setMenus(Set<Menu> menus)
    {
        this.menus = menus;
    }
}