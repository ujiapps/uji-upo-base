package es.uji.apps.upo.model;

import es.uji.apps.upo.model.enums.TipoFecha;
import es.uji.apps.upo.model.enums.TipoPrioridadContenido;
import es.uji.apps.upo.utils.DateUtils;
import es.uji.apps.upo.exceptions.FechaFinPrioridadSuperiorAFechaInicioException;
import es.uji.apps.upo.exceptions.SeIndicaUnaHoraSinFechaAsociadaException;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;

@Component
@Entity
@Table(name = "UPO_OBJETOS_PRIORIDADES")
@SuppressWarnings("serial")
public class PrioridadContenido implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO")
    private Date fechaInicio;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    @Temporal(TemporalType.TIME)
    @Column(name = "HORA_INICIO")
    private Date horaInicio;

    @Temporal(TemporalType.TIME)
    @Column(name = "HORA_FIN")
    private Date horaFin;

    @Enumerated(EnumType.STRING)
    private TipoPrioridadContenido prioridad;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OBJETO_ID")
    private Contenido upoObjeto;

    public PrioridadContenido()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Date getFechaFin()
    {
        return this.fechaFin;
    }

    public void setFechaFin(Date fechaFin)
    {
        this.fechaFin = fechaFin;
    }

    public Date getFechaInicio()
    {
        return this.fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio)
    {
        this.fechaInicio = fechaInicio;
    }

    public Date getHoraInicio()
    {
        return horaInicio;
    }

    public void setHoraInicio(Date horaInicio)
    {
        this.horaInicio = horaInicio;
    }

    public Date getHoraFin()
    {
        return horaFin;
    }

    public void setHoraFin(Date horaFin)
    {
        this.horaFin = horaFin;
    }

    public TipoPrioridadContenido getPrioridad()
    {
        return this.prioridad;
    }

    public void setPrioridad(TipoPrioridadContenido prioridad)
    {
        this.prioridad = prioridad;
    }

    public Contenido getUpoObjeto()
    {
        return this.upoObjeto;
    }

    public void setUpoObjeto(Contenido upoObjeto)
    {
        this.upoObjeto = upoObjeto;
    }

    public NodoMapa getNodoMapa()
    {
        return this.getUpoObjeto().getNodoMapa();
    }

    public void checkFechasCorrectas() throws SeIndicaUnaHoraSinFechaAsociadaException,
            ParseException, FechaFinPrioridadSuperiorAFechaInicioException
    {
        if (esHoraSinFechaAsociada())
        {
            throw new SeIndicaUnaHoraSinFechaAsociadaException();
        }

        Date fechaInicio = DateUtils.buildDateTime(getFechaInicio(), getHoraInicio(), TipoFecha.INICIO);
        Date fechaFin = DateUtils.buildDateTime(getFechaFin(), getHoraFin(), TipoFecha.FIN);

        if (!esFechaFinSuperiorAFechaInicio(fechaInicio, fechaFin))
        {
            throw new FechaFinPrioridadSuperiorAFechaInicioException();
        }
    }

    private boolean esHoraSinFechaAsociada()
    {
        return (fechaInicio == null && horaInicio != null) ||
                (fechaFin == null && horaFin != null);
    }

    private boolean esFechaFinSuperiorAFechaInicio(Date fechaInicio, Date fechaFin)
    {
        if (fechaInicio == null || fechaFin == null)
        {
            return true;
        }

        return (fechaFin.after(fechaInicio));
    }
}