package es.uji.apps.upo.model.eujier;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;

@Component
@Entity
@Table(name = "XPFI_VW_ITEMS_UBICACION")
@SuppressWarnings("serial")
public class EujierItemUbicacion implements Serializable
{
    @Id
    private String id;

    @Column(name = "item_id")
    private Long itemId;

    @Column(name = "region_id")
    private Long regionId;

    @Column(name = "aplicacion_id")
    private Long aplicacionId;

    @Column(name = "grupo_id")
    private Long grupoId;

    @Column(name = "nombre")
    private String nombre;

    private String idioma;

    private String camino;

    public EujierItemUbicacion()
    {
    }

    public Long getItemId()
    {
        return itemId;
    }

    public void setItemId(Long itemId)
    {
        this.itemId = itemId;
    }

    public Long getRegionId()
    {
        return regionId;
    }

    public void setRegionId(Long regionId)
    {
        this.regionId = regionId;
    }

    public Long getAplicacionId()
    {
        return aplicacionId;
    }

    public void setAplicacionId(Long aplicacionId)
    {
        this.aplicacionId = aplicacionId;
    }

    public Long getGrupoId()
    {
        return grupoId;
    }

    public void setGrupoId(Long grupoId)
    {
        this.grupoId = grupoId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getIdioma()
    {
        return idioma;
    }

    public void setIdioma(String idioma)
    {
        this.idioma = idioma;
    }

    public String getCamino()
    {
        return camino;
    }

    public void setCamino(String camino)
    {
        this.camino = camino;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }
}