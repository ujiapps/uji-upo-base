package es.uji.apps.upo.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "UPO_EXT_PERSONAS_TODAS")
@SuppressWarnings("serial")
public class PersonaTodas implements Serializable
{
    public static final String ROLE_ADMIN = "ADMIN";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String apellido1;

    private String apellido2;

    private String nombre;

    private String email;

    @Column(name = "NOMBRE_COMPLETO")
    private String nombreCompleto;

    @Column(name = "NOMBRE_BUSQUEDA")
    private String nombreBusqueda;

    @OneToMany(mappedBy = "persona")
    private Set<UsuarioExterno> usuariosExternos;

    public PersonaTodas()
    {
    }

    public PersonaTodas(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getApellido1()
    {
        return this.apellido1;
    }

    public void setApellido1(String apellido1)
    {
        this.apellido1 = apellido1;
    }

    public String getApellido2()
    {
        return this.apellido2;
    }

    public void setApellido2(String apellido2)
    {
        this.apellido2 = apellido2;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getNombreCompleto()
    {
        return this.nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto)
    {
        this.nombreCompleto = nombreCompleto;
    }

    public Set<UsuarioExterno> getUsuariosExternos()
    {
        return usuariosExternos;
    }

    public void setUsuarisoExternos(Set<UsuarioExterno> usuariosExternos)
    {
        this.usuariosExternos = usuariosExternos;
    }

    public String getNombreBusqueda()
    {
        return nombreBusqueda;
    }

    public void setNombreBusqueda(String nombreBusqueda)
    {
        this.nombreBusqueda = nombreBusqueda;
    }
}
