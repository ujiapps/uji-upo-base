package es.uji.apps.upo.model;

import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Component
@Entity
@Table(name = "UPO_HISTORICO_MODERACIONES")
public class ModeracionHistorico implements Serializable
{
    @Id
    @Column(name = "ID")
    private String id;
    @Column(name = "MAPA_OBJETO_ID")
    private Long mapaObjetoId;
    @Column(name = "OBJETO_ID")
    private Long objetoId;
    @Column(name = "MAPA_ID")
    private Long mapaId;
    @Column(name = "ESTADO_MODERACION")
    private String estadoModeracion;
    @Column(name = "PER_ID_MODERACION")
    private Long perIdModeracion;
    @Column(name = "PER_ID_PROPUESTA")
    private Long perIdPropuesta;
    @Column(name = "TEXTO_RECHAZO")
    private String textoRechazo;
    @Column(name = "OBJETO_URL_PATH")
    private String objetoUrlPath;
    @Column(name = "URL_COMPLETA")
    private String urlCompleta;
    @Column(name = "MAPA_URL_COMPLETA")
    private String mapaUrlCompleta;
    @Column(name = "OBJETO_TITULO")
    private String objetoTitulo;
    @Column(name = "PROPUESTA_EXTERNA")
    private Boolean propuestaExterna;

    public ModeracionHistorico()
    {
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Long getMapaObjetoId()
    {
        return mapaObjetoId;
    }

    public void setMapaObjetoId(Long mapaObjetoId)
    {
        this.mapaObjetoId = mapaObjetoId;
    }

    public Long getObjetoId()
    {
        return objetoId;
    }

    public void setObjetoId(Long objetoId)
    {
        this.objetoId = objetoId;
    }

    public Long getMapaId()
    {
        return mapaId;
    }

    public void setMapaId(Long mapaId)
    {
        this.mapaId = mapaId;
    }

    public String getEstadoModeracion()
    {
        return estadoModeracion;
    }

    public void setEstadoModeracion(String estadoModeracion)
    {
        this.estadoModeracion = estadoModeracion;
    }

    public Long getPerIdModeracion()
    {
        return perIdModeracion;
    }

    public void setPerIdModeracion(Long perIdModeracion)
    {
        this.perIdModeracion = perIdModeracion;
    }

    public Long getPerIdPropuesta()
    {
        return perIdPropuesta;
    }

    public void setPerIdPropuesta(Long perIdPropuesta)
    {
        this.perIdPropuesta = perIdPropuesta;
    }

    public String getTextoRechazo()
    {
        return textoRechazo;
    }

    public void setTextoRechazo(String textoRechazo)
    {
        this.textoRechazo = textoRechazo;
    }

    public String getObjetoUrlPath()
    {
        return objetoUrlPath;
    }

    public void setObjetoUrlPath(String objetoUrlPath)
    {
        this.objetoUrlPath = objetoUrlPath;
    }

    public String getUrlCompleta()
    {
        return urlCompleta;
    }

    public void setUrlCompleta(String urlCompleta)
    {
        this.urlCompleta = urlCompleta;
    }

    public String getMapaUrlCompleta()
    {
        return mapaUrlCompleta;
    }

    public void setMapaUrlCompleta(String mapaUrlCompleta)
    {
        this.mapaUrlCompleta = mapaUrlCompleta;
    }

    public String getObjetoTitulo()
    {
        return objetoTitulo;
    }

    public void setObjetoTitulo(String objetoTitulo)
    {
        this.objetoTitulo = objetoTitulo;
    }

    public Boolean getPropuestaExterna()
    {
        return propuestaExterna;
    }

    public void setPropuestaExterna(Boolean propuestaExterna)
    {
        this.propuestaExterna = propuestaExterna;
    }
}