package es.uji.apps.upo.model.eujier;

import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Component
@Entity
@Table(name = "XPFI_AVISOS_VISTOS")
@SuppressWarnings("serial")
public class EujierAvisoVisto implements Serializable
{
    @Id
    @Column(name = "per_id")
    private Long perId;

    @Id
    @Column(name = "aviso_id")
    private Long avisoId;

    public EujierAvisoVisto()
    {
    }

    public EujierAvisoVisto(Long avisoId, Long perId)
    {
        this.perId = perId;
        this.avisoId = avisoId;
    }

    public Long getPerId()
    {
        return perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }

    public Long getAvisoId()
    {
        return avisoId;
    }

    public void setAvisoId(Long avisoId)
    {
        this.avisoId = avisoId;
    }
}
