package es.uji.apps.upo.model;

import es.uji.apps.upo.model.enums.TipoFranquiciaAcceso;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Component
@Table(name = "UPO_FRANQUICIAS_ACCESOS")
public class FranquiciaAcceso implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    private TipoFranquiciaAcceso tipo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PER_ID")
    private Persona upoExtPersona;

    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn(name = "FRANQUICIA_ID")
    private Franquicia upoFranquicia;

    public FranquiciaAcceso()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public TipoFranquiciaAcceso getTipo()
    {
        return this.tipo;
    }

    public void setTipo(TipoFranquiciaAcceso tipo)
    {
        this.tipo = tipo;
    }

    public Persona getUpoExtPersona()
    {
        return this.upoExtPersona;
    }

    public void setUpoExtPersona(Persona upoExtPersona)
    {
        this.upoExtPersona = upoExtPersona;
    }

    public Franquicia getUpoFranquicia()
    {
        return this.upoFranquicia;
    }

    public void setUpoFranquicia(Franquicia upoFranquicia)
    {
        this.upoFranquicia = upoFranquicia;
    }
}