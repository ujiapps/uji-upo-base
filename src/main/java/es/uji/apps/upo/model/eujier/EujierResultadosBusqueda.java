package es.uji.apps.upo.model.eujier;

import es.uji.apps.upo.model.eujier.EujierItem;

import java.util.List;

public class EujierResultadosBusqueda
{
    private Long numItems;
    private List<EujierItem> items;

    private Integer pagina;

    public Long getNumItems()
    {
        return numItems;
    }

    public void setNumItems(Long numItems)
    {
        this.numItems = numItems;
    }

    public List<EujierItem> getItems()
    {
        return items;
    }

    public void setItems(List<EujierItem> items)
    {
        this.items = items;
    }

    public Integer getPagina()
    {
        return pagina;
    }

    public void setPagina(Integer pagina)
    {
        this.pagina = pagina;
    }
}
