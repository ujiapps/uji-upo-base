package es.uji.apps.upo.model.enums;

public enum TipoFormatoContenido
{
   PAGINA, BINARIO, VIDEO_UJI, VIDEO_YOUTUBE, VIDEO_VIMEO;
}
