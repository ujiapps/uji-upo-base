package es.uji.apps.upo.model.enums;

public enum TipoMime
{
    PDF("application/pdf"),
    DOC("application/msword"),
    BMP("image/bmp"),
    JPG("image/jpeg"),
    JPEG("image/jpeg"),
    GIF("image/gif"),
    PNG("image/png");
    
    private String tipoMime;
    
    TipoMime(String tipoMime) {
        this.tipoMime = tipoMime;
    }

    public String getTipoMime()
    {
        return tipoMime;
    }

    public void setTipoMime(String tipoMime)
    {
        this.tipoMime = tipoMime;
    }
}
