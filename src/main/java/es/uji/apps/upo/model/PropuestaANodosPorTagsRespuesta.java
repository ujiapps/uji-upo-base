package es.uji.apps.upo.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PropuestaANodosPorTagsRespuesta implements Serializable
{
    private List<String> urlCompletasConExito;

    private List<PropuestaANodosPorTagsError> urlCompletasConError;

    public PropuestaANodosPorTagsRespuesta()
    {
        urlCompletasConExito = new ArrayList<>();
        urlCompletasConError = new ArrayList<>();
    }

    public List<String> getUrlCompletasConExito()
    {
        return urlCompletasConExito;
    }

    public void setUrlCompletasConExito(List<String> urlCompletasConExito)
    {
        this.urlCompletasConExito = urlCompletasConExito;
    }

    public List<PropuestaANodosPorTagsError> getGetUrlCompletasConError()
    {
        return urlCompletasConError;
    }

    public void setGetUrlCompletasConError(List<PropuestaANodosPorTagsError> getUrlCompletasConError)
    {
        this.urlCompletasConError = getUrlCompletasConError;
    }

    public void addUrlCompletaConExito(String urlCompleta)
    {
        if (!this.urlCompletasConExito.contains(urlCompleta)) this.urlCompletasConExito.add(urlCompleta);
    }

    public void addUrlCompletaConError(String tipo, String urlCompleta)
    {
        this.urlCompletasConError.add(new PropuestaANodosPorTagsError(tipo, urlCompleta));
    }
}