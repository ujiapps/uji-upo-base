package es.uji.apps.upo.model;

import java.io.Serializable;

public class PropuestaANodosPorTagsError implements Serializable
{
    private String tipo;
    private String urlCompleta;

    public PropuestaANodosPorTagsError(String tipo, String urlCompleta)
    {
        this.tipo = tipo;
        this.urlCompleta = urlCompleta;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getUrlCompleta()
    {
        return urlCompleta;
    }

    public void setUrlCompleta(String urlCompleta)
    {
        this.urlCompleta = urlCompleta;
    }
}