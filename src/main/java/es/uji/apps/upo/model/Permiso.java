package es.uji.apps.upo.model;

import es.uji.apps.upo.dao.FranquiciaDAO;
import org.apache.xpath.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import es.uji.commons.sso.dao.ApaDAO;

@Component
public class Permiso
{
    private static ApaDAO apaDAO;
    private static FranquiciaDAO franquiciaDAO;

    @Autowired
    public void setApaDAO(ApaDAO apaDAO, FranquiciaDAO franquiciaDAO)
    {
        Permiso.apaDAO = apaDAO;
        Permiso.franquiciaDAO = franquiciaDAO;
    }

    public static Boolean isAdmin(Long userId)
    {
        return apaDAO.hasPerfil("UPO", "ADMIN", userId);
    }

    public static Boolean isDocumentalista(Long userId)
    {
        return apaDAO.hasPerfil("UPO", "DOCUMENTALISTA", userId);
    }

    public static Boolean hasFranquicia(Long userId)
    {
        return franquiciaDAO.hasFranquicia(userId);
    }
}
