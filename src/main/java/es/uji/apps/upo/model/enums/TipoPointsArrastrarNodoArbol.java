package es.uji.apps.upo.model.enums;

public enum TipoPointsArrastrarNodoArbol
{
    AFTER, BEFORE
}
