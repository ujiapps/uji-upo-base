package es.uji.apps.upo.model;

import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Component
@Entity
@Table(name = "UPO_EXT_FICHEROS")
public class Fichero
{
    @Id
    private Long id;

    @Column(name = "MIME_TYPE")
    private String mimeType;

    private String nombre;

    private byte[] contenido;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getMimeType()
    {
        return mimeType;
    }

    public void setMimeType(String mimeType)
    {
        this.mimeType = mimeType;
    }

    public byte[] getContenido()
    {
        return contenido;
    }

    public void setContenido(byte[] contenido)
    {
        this.contenido = contenido;
    }
}