package es.uji.apps.upo.model.eujier;

import es.uji.apps.upo.model.eujier.enums.TipoVista;

import java.util.ArrayList;
import java.util.List;

public class EujierSeccionConfiguracion
{
    private String vista;
    private List<EujierItem> items;
    private EujierSeccion configuracion;

    public EujierSeccionConfiguracion()
    {
        this.items = new ArrayList<>();
        this.vista = TipoVista.CUADRICULA.toString();
    }

    public String getVista()
    {
        return vista;
    }

    public void setVista(String vista)
    {
        this.vista = vista;
    }

    public List<EujierItem> getItems()
    {
        return items;
    }

    public void setItems(List<EujierItem> items)
    {
        this.items = items;
    }

    public EujierSeccion getConfiguracion()
    {
        return configuracion;
    }

    public void setConfiguracion(EujierSeccion configuracion)
    {
        this.configuracion = configuracion;
    }

    public void toggleVista(String vista, EujierSeccion seccion)
    {
        if (vista == null || vista.isEmpty())
        {
            this.vista = seccion.getVista();
            return;
        }

        if (this.vista.equalsIgnoreCase(vista))
        {
            return;
        }

        this.vista = vista;
    }
}
