package es.uji.apps.upo.model;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;
import javax.ws.rs.core.MediaType;

import es.uji.apps.upo.model.enums.EstadoModeracion;
import es.uji.apps.upo.model.enums.TipoReferenciaContenido;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.upo.exceptions.ActualizadoContenidoNoAutorizadoException;
import es.uji.apps.upo.exceptions.ContenidoConMismoUrlPathException;
import es.uji.apps.upo.exceptions.ContenidoNoProponibleException;
import es.uji.apps.upo.exceptions.ContenidoYaPropuestoException;
import es.uji.apps.upo.exceptions.NodoMapaContenidoDebeTenerUnContenidoNormalException;
import es.uji.apps.upo.exceptions.TipoContenidoNoAdecuadoException;
import es.uji.apps.upo.exceptions.UsuarioNoAutenticadoException;
import es.uji.apps.upo.dao.ContenidoDAO;
import es.uji.apps.upo.dao.NodoMapaContenidoDAO;
import es.uji.apps.upo.dao.NodoMapaDAO;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.MailMessage;

@Component
@Entity
@Table(name = "UPO_MAPAS_OBJETOS")
@SuppressWarnings("serial")
public class NodoMapaContenido implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "ESTADO_MODERACION")
    private EstadoModeracion estadoModeracion;

    @Enumerated(EnumType.STRING)
    private TipoReferenciaContenido tipo;

    // bi-directional many-to-one association to UpoExtPersona
    @ManyToOne
    @JoinColumn(name = "PER_ID_MODERACION")
    private Persona upoExtPersona;

    @ManyToOne
    @JoinColumn(name = "PER_ID_PROPUESTA")
    private Persona personaPropuesta;

    // bi-directional many-to-one association to UpoMapa
    @ManyToOne
    @JoinColumn(name = "MAPA_ID")
    private NodoMapa upoMapa;

    // bi-directional many-to-one association to UpoObjeto
    @ManyToOne
    @JoinColumn(name = "OBJETO_ID")
    private Contenido upoObjeto;

    @Column(name = "TEXTO_RECHAZO")
    private String textoRechazo;

    @Column(name = "PROPUESTA_EXTERNA")
    private Boolean propuestaExterna;

    @Column(name = "PROPUESTA_POR_TAG")
    private Boolean propuestaPorTag;

    @OneToMany(mappedBy = "nodoMapaContenido", cascade = CascadeType.ALL)
    private Set<FechaEvento> fechasEvento;

    private Long orden;

    public NodoMapaContenido()
    {
        this.estadoModeracion = EstadoModeracion.PENDIENTE;
        this.tipo = TipoReferenciaContenido.NORMAL;
        this.propuestaExterna = false;
        this.propuestaPorTag = false;

        this.fechasEvento = new HashSet<>();
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public EstadoModeracion getEstadoModeracion()
    {
        return this.estadoModeracion;
    }

    public void setEstadoModeracion(EstadoModeracion estadoModeracion)
    {
        this.estadoModeracion = estadoModeracion;
    }

    public TipoReferenciaContenido getTipo()
    {
        return this.tipo;
    }

    public void setTipo(TipoReferenciaContenido tipo)
    {
        this.tipo = tipo;
    }

    public Persona getUpoExtPersona()
    {
        return this.upoExtPersona;
    }

    public void setUpoExtPersona(Persona upoExtPersona)
    {
        this.upoExtPersona = upoExtPersona;
    }

    public Persona getPersonaPropuesta()
    {
        return personaPropuesta;
    }

    public void setPersonaPropuesta(Persona personaPropuesta)
    {
        this.personaPropuesta = personaPropuesta;
    }

    public NodoMapa getUpoMapa()
    {
        return this.upoMapa;
    }

    public void setUpoMapa(NodoMapa upoMapa)
    {
        this.upoMapa = upoMapa;
    }

    public Contenido getUpoObjeto()
    {
        return this.upoObjeto;
    }

    public void setUpoObjeto(Contenido upoObjeto)
    {
        this.upoObjeto = upoObjeto;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public void setOrden(String orden)
    {
        if (orden != null && !orden.isEmpty())
        {
            this.orden = new Long(orden);
        }
        else
        {
            this.orden = null;
        }
    }

    public String getTextoRechazo()
    {
        return textoRechazo;
    }

    public void setTextoRechazo(String textoRechazo)
    {
        this.textoRechazo = textoRechazo;
    }

    public Boolean getPropuestaExterna()
    {
        return propuestaExterna;
    }

    public void setPropuestaExterna(Boolean propuestaExterna)
    {
        this.propuestaExterna = propuestaExterna;
    }

    public Boolean getPropuestaPorTag()
    {
        return propuestaPorTag;
    }

    public void setPropuestaPorTag(Boolean propuestaPorTag)
    {
        this.propuestaPorTag = propuestaPorTag;
    }

    public Set<FechaEvento> getFechasEvento()
    {
        return fechasEvento;
    }

    public void setFechasEvento(Set<FechaEvento> fechasEvento)
    {
        this.fechasEvento = fechasEvento;
    }

    public Boolean isNormal()
    {
        return TipoReferenciaContenido.NORMAL.equals(this.getTipo());
    }

    public void rechaza(Long connectedUserId, String motivoRechazo)
    {
        actualizaEstado(connectedUserId, EstadoModeracion.RECHAZADO, motivoRechazo);
    }

    public void acepta(Long connectedUserId)
    {
        actualizaEstado(connectedUserId, EstadoModeracion.ACEPTADO, null);
    }

    private void actualizaEstado(Long connectedUserId, EstadoModeracion estadoModeracion, String motivoRechazo)
    {
        Persona persona = new Persona();
        persona.setId(connectedUserId);
        upoExtPersona = persona;

        this.estadoModeracion = estadoModeracion;
        textoRechazo = motivoRechazo;
    }
}