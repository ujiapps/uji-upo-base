package es.uji.apps.upo.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Component
@Entity
@Table(name = "UPO_FRANQUICIAS")
@SuppressWarnings("serial")
public class Franquicia implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    @Column(name = "OTROS_AUTORES")
    private String otrosAutores;

    @OneToMany(mappedBy = "upoFranquicia", cascade = CascadeType.ALL)
    private Set<FranquiciaAcceso> upoFranquiciasAccesos;

    @OneToMany(mappedBy = "upoFranquicia")
    private Set<NodoMapa> upoMapas;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORIGENES_INFORMACION_ID")
    private OrigenInformacion origenInformacion;

    public Franquicia()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Set<FranquiciaAcceso> getUpoFranquiciasAccesos()
    {
        return this.upoFranquiciasAccesos;
    }

    public void setUpoFranquiciasAccesos(Set<FranquiciaAcceso> upoFranquiciasAccesos)
    {
        this.upoFranquiciasAccesos = upoFranquiciasAccesos;
    }

    public Set<NodoMapa> getUpoMapas()
    {
        return this.upoMapas;
    }

    public void setUpoMapas(Set<NodoMapa> upoMapas)
    {
        this.upoMapas = upoMapas;
    }

    public String getOtrosAutores()
    {
        return otrosAutores;
    }

    public void setOtrosAutores(String otrosAutores)
    {
        this.otrosAutores = otrosAutores;
    }

    public OrigenInformacion getOrigenInformacion()
    {
        return origenInformacion;
    }

    public void setOrigenInformacion(OrigenInformacion origenInformacion)
    {
        this.origenInformacion = origenInformacion;
    }
}