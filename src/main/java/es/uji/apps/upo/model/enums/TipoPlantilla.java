package es.uji.apps.upo.model.enums;

public enum TipoPlantilla
{
    WEB, PDF;
}
