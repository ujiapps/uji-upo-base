package es.uji.apps.upo.model;

import es.uji.apps.upo.model.enums.EstadoModeracion;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
@Component
@Entity
@Table(name = "UPO_MODERACION_LOTES")
public class ModeracionLote implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "ESTADO_MODERACION")
    private EstadoModeracion estadoModeracion;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN_MIGRACION")
    private Date fechaFinMigracion;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_SOLICITUD")
    private Date fechaSolicitud;

    @Column(name = "TEXTO_RECHAZO")
    private String textoRechazo;

    private String url;

    @ManyToOne
    @JoinColumn(name = "PER_ID_SOLICITUD")
    private Persona upoExtPersonaSolicitud;

    @ManyToOne
    @JoinColumn(name = "PER_ID_MODERACION")
    private Persona upoExtPersonaModeracion;

    @ManyToOne
    @JoinColumn(name = "MAPA_ID")
    private NodoMapa upoMapa;


    public ModeracionLote()
    {
        this.estadoModeracion = EstadoModeracion.PENDIENTE;
        this.fechaSolicitud = new Date();
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public EstadoModeracion getEstadoModeracion()
    {
        return this.estadoModeracion;
    }

    public void setEstadoModeracion(EstadoModeracion estadoModeracion)
    {
        this.estadoModeracion = estadoModeracion;
    }

    public Date getFechaFinMigracion()
    {
        return this.fechaFinMigracion;
    }

    public void setFechaFinMigracion(Date fechaFinMigracion)
    {
        this.fechaFinMigracion = fechaFinMigracion;
    }

    public Date getFechaSolicitud()
    {
        return this.fechaSolicitud;
    }

    public void setFechaSolicitud(Date fechaSolicitud)
    {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getTextoRechazo()
    {
        return this.textoRechazo;
    }

    public void setTextoRechazo(String textoRechazo)
    {
        this.textoRechazo = textoRechazo;
    }

    public String getUrl()
    {
        return this.url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public Persona getUpoExtPersonaSolicitud()
    {
        return this.upoExtPersonaSolicitud;
    }

    public void setUpoExtPersonaSolicitud(Persona upoExtPersonaSolicitud)
    {
        this.upoExtPersonaSolicitud = upoExtPersonaSolicitud;
    }

    public Persona getUpoExtPersonaModeracion()
    {
        return this.upoExtPersonaModeracion;
    }

    public void setUpoExtModeracion(Persona upoExtModeracion)
    {
        this.upoExtPersonaModeracion = upoExtModeracion;
    }

    public NodoMapa getUpoMapa()
    {
        return this.upoMapa;
    }

    public void setUpoMapa(NodoMapa upoMapa)
    {
        this.upoMapa = upoMapa;
    }

    public void migrar()
    {
        this.fechaFinMigracion = new Date();
    }
}