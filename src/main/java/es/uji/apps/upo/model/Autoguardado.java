package es.uji.apps.upo.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "UPO_AUTOGUARDADO")
@SuppressWarnings("serial")
public class Autoguardado implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Column(name = "IDIOMA_CODIGO_ISO")
    private String idiomaCodigoIso;

    @Column(name = "PER_ID")
    private Long perId;

    @Lob
    @Column(name = "CONTENIDO_EDITORA")
    private String contenidoEditora;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OBJETO_ID")
    private Contenido contenido;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MAPA_ID")
    private NodoMapa nodoMapa;

    public Autoguardado()
    {
        this.fecha = new Date();
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Date getFecha()
    {
        return this.fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public String getIdiomaCodigoIso()
    {
        return this.idiomaCodigoIso;
    }

    public void setIdiomaCodigoIso(String idiomaCodigoIso)
    {
        this.idiomaCodigoIso = idiomaCodigoIso;
    }

    public Long getPerId()
    {
        return this.perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }

    public String getContenidoEditora()
    {
        return contenidoEditora;
    }

    public void setContenidoEditora(String contenidoEditora)
    {
        this.contenidoEditora = contenidoEditora;
    }

    public Contenido getContenido()
    {
        return contenido;
    }

    public void setContenido(Contenido contenido)
    {
        this.contenido = contenido;
    }

    public NodoMapa getNodoMapa()
    {
        return nodoMapa;
    }

    public void setNodoMapa(NodoMapa nodoMapa)
    {
        this.nodoMapa = nodoMapa;
    }
}