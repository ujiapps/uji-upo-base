package es.uji.apps.upo.model.eujier;

import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Component
@Entity
@Table(name = "XPFI_VW_REGIONES")
@SuppressWarnings("serial")
public class EujierRegion implements Serializable
{
    @Id
    private String id;

    private String nombre;
    private String idioma;

    public EujierRegion()
    {
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getIdioma()
    {
        return idioma;
    }

    public void setIdioma(String idioma)
    {
        this.idioma = idioma;
    }
}