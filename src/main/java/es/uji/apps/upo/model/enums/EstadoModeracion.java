package es.uji.apps.upo.model.enums;

public enum EstadoModeracion
{
    PENDIENTE, ACEPTADO, RECHAZADO;
}
