package es.uji.apps.upo.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;

@Component
@Entity
@Table(name = "UPO_OBJETOS_TAGS")
@SuppressWarnings("serial")
public class ContenidoTag implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String tagname;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OBJETO_ID")
    private Contenido upoObjeto;

    public ContenidoTag()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getTagname()
    {
        return this.tagname;
    }

    public void setTagname(String tagname)
    {
        this.tagname = tagname;
    }

    public Contenido getUpoObjeto()
    {
        return this.upoObjeto;
    }

    public void setUpoObjeto(Contenido upoObjeto)
    {
        this.upoObjeto = upoObjeto;
    }

    @Override
    public String toString()
    {
        return tagname;
    }
}