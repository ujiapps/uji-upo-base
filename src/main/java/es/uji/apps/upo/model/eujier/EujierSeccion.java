package es.uji.apps.upo.model.eujier;

import java.io.Serializable;

import javax.persistence.*;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "XPFI_VW_SECCIONES")
@SuppressWarnings("serial")
public class EujierSeccion implements Serializable
{
    @Id
    private Long id;

    @Column(name = "nombre_ca")
    private String nombreCA;

    @Column(name = "nombre_es")
    private String nombreES;

    @Column(name = "nombre_en")
    private String nombreEN;

    @Column(name = "persona_id")
    private Long personaId;

    @Transient
    private String nombre;

    private String color;
    private String colorClass;
    private String codigo;
    private Integer orden;
    private String vista;

    @Column(name = "grupo_perfil_id")
    private Long grupoPerfilId;

    @Column(name = "num_items")
    private Long numItems;

    public EujierSeccion()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public Integer getOrden()
    {
        return orden;
    }

    public void setOrden(Integer orden)
    {
        this.orden = orden;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN)
    {
        this.nombreEN = nombreEN;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public Long getGrupoPerfilId()
    {
        return grupoPerfilId;
    }

    public void setGrupoPerfilId(Long grupoPerfilId)
    {
        this.grupoPerfilId = grupoPerfilId;
    }

    public Long getNumItems()
    {
        return numItems;
    }

    public void setNumItems(Long numItems)
    {
        this.numItems = numItems;
    }

    public String getVista()
    {
        return vista;
    }

    public void setVista(String vista)
    {
        this.vista = vista;
    }

    public String getColorClass()
    {
        return colorClass;
    }

    public void setColorClass(String colorClass)
    {
        this.colorClass = colorClass;
    }
}