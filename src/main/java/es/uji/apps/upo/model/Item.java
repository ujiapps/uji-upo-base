package es.uji.apps.upo.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Component
@Entity
@Table(name = "UPO_ITEMS")
@SuppressWarnings("serial")
public class Item implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nombre_ca")
    private String nombreCA;

    @Column(name = "nombre_es")
    private String nombreES;

    @Column(name = "nombre_EN")
    private String nombreEN;

    private String url;

    @OneToMany(mappedBy = "item")
    private Set<GrupoItem> gruposItems;

    @ManyToOne
    @JoinColumn(name = "PER_ID")
    private Persona persona;

    public Item()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombreCA()
    {
        return this.nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return this.nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreEN()
    {
        return this.nombreEN;
    }

    public void setNombreEN(String nombreEN)
    {
        this.nombreEN = nombreEN;
    }

    public String getUrl()
    {
        return this.url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public Set<GrupoItem> getGruposItems()
    {
        return gruposItems;
    }

    public void setGruposItems(Set<GrupoItem> gruposItems)
    {
        this.gruposItems = gruposItems;
    }

    public Persona getPersona()
    {
        return persona;
    }

    public void setPersona(Persona persona)
    {
        this.persona = persona;
    }
}