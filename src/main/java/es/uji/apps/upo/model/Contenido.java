package es.uji.apps.upo.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import es.uji.apps.upo.model.enums.EstadoModeracion;
import es.uji.apps.upo.model.enums.IdiomaPublicacion;

import org.apache.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.upo.exceptions.UsuarioNoAutenticadoException;
import es.uji.apps.upo.accessibility.AccesibilityChecker;
import es.uji.apps.upo.dao.ContenidoDAO;
import es.uji.apps.upo.dao.NodoMapaContenidoDAO;
import es.uji.apps.upo.model.metadatos.ContenidoMetadato;
import es.uji.apps.upo.solr.ContenidoSolr;

@Component
@Entity
@Table(name = "UPO_OBJETOS")
@SuppressWarnings("serial")
public class Contenido implements Serializable
{
    public static Logger log = Logger.getLogger(ContenidoDAO.class);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String latitud;

    private String longitud;

    private String lugar;

    @Column(name = "URL_PATH")
    private String urlPath;

    private String publicable;

    private String visible;

    @Column(name = "URL_ORIGINAL")
    private String urlOriginal;

    @Column(name = "TEXTO_NOVISIBLE")
    private String textoNoVisible;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_CREACION")
    private Date fechaCreacion;

    @Column(name = "OTROS_AUTORES")
    private String otrosAutores;

    @Column(name = "AJUSTAR_A_FECHAS_VIGENCIA")
    private Boolean ajustarAFechasVigencia;

    @OneToMany(mappedBy = "upoObjeto", cascade = CascadeType.ALL)
    private Set<NodoMapaContenido> upoMapasObjetos;

    @ManyToOne
    @JoinColumn(name = "PER_ID_RESPONSABLE")
    private Persona upoExtPersona1;

    @ManyToOne
    @JoinColumn(name = "ORIGEN_INFORMACION_ID")
    private OrigenInformacion origenInformacion;

    @OneToMany(mappedBy = "upoObjeto", cascade = CascadeType.ALL)
    private Set<ContenidoIdioma> contenidoIdiomas;

    @OneToMany(mappedBy = "upoObjeto", cascade = CascadeType.ALL)
    private Set<PrioridadContenido> contenidoPrioridades;

    @OneToMany(mappedBy = "upoObjeto", cascade = CascadeType.ALL)
    private Set<VigenciaContenido> upoVigenciasObjetos;

    @OneToMany(mappedBy = "upoObjeto", cascade = CascadeType.ALL)
    private Set<ContenidoTag> upoContenidosTags;

    @OneToMany(mappedBy = "upoObjeto", cascade = CascadeType.ALL)
    private Set<ContenidoMetadato> upoContenidosMetadatos;

    @OneToMany(mappedBy = "contenido", cascade = CascadeType.ALL)
    private Set<Autoguardado> autoguardados;

    @Transient
    private static AccesibilityChecker accesibilityChecker;

    @Transient
    private static NodoMapaContenidoDAO nodoMapaContenidoDAO;

    public Contenido()
    {
        this("index.html");
    }

    public Contenido(String url)
    {
        this.publicable = "N";
        this.setVisible("S");
        this.setFechaCreacion(new Date());
        this.setUrlPath(url);
        this.setAjustarAFechasVigencia(false);

        this.upoVigenciasObjetos = new HashSet<VigenciaContenido>();
        this.upoMapasObjetos = new HashSet<NodoMapaContenido>();
        this.contenidoIdiomas = new HashSet<ContenidoIdioma>();
        this.upoContenidosTags = new HashSet<ContenidoTag>();
        this.upoContenidosMetadatos = new HashSet<ContenidoMetadato>();
    }

    @Autowired
    public void setAccesibilityChecker(AccesibilityChecker accesibilityChecker)
    {
        Contenido.accesibilityChecker = accesibilityChecker;
    }

    @Autowired
    public void setNodoMapaContenidoDAO(NodoMapaContenidoDAO nodoMapaContenidoDAO)
    {
        Contenido.nodoMapaContenidoDAO = nodoMapaContenidoDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setUrlOriginal(String urlOriginal)
    {
        this.urlOriginal = urlOriginal;
    }

    public String getUrlOriginal()
    {
        return urlOriginal;
    }

    public String getLatitud()
    {
        return this.latitud;
    }

    public void setLatitud(String latitud)
    {
        this.latitud = latitud;
    }

    public String getLongitud()
    {
        return this.longitud;
    }

    public void setLongitud(String longitud)
    {
        this.longitud = longitud;
    }

    public String getLugar()
    {
        return this.lugar;
    }

    public void setLugar(String lugar)
    {
        this.lugar = lugar;
    }

    public String getUrlPath()
    {
        return this.urlPath;
    }

    public void setUrlPath(String urlPath)
    {
        this.urlPath = urlPath.replaceAll("[^A-Za-z0-9_.\\-]+", "");
    }

    public String getPublicable()
    {
        return this.publicable;
    }

    public void setPublicable(String publicable)
    {
        this.publicable = publicable;
    }

    public String getVisible()
    {
        return this.visible;
    }

    public void setVisible(String visible)
    {
        this.visible = visible;
    }

    public String getTextoNoVisible()
    {
        return this.textoNoVisible;
    }

    public void setTextoNoVisible(String textoNoVisible)
    {
        this.textoNoVisible = textoNoVisible;
    }

    public String getOtrosAutores()
    {
        return this.otrosAutores;
    }

    public void setOtrosAutores(String otrosAutores)
    {
        this.otrosAutores = otrosAutores;
    }

    public Set<NodoMapaContenido> getUpoMapasObjetos()
    {
        return this.upoMapasObjetos;
    }

    public void setUpoMapasObjetos(Set<NodoMapaContenido> upoMapasObjetos)
    {
        this.upoMapasObjetos = upoMapasObjetos;
    }

    public Persona getUpoExtPersona1()
    {
        return this.upoExtPersona1;
    }

    public void setUpoExtPersona1(Persona upoExtPersona1)
    {
        this.upoExtPersona1 = upoExtPersona1;
    }

    public OrigenInformacion getOrigenInformacion()
    {
        return origenInformacion;
    }

    public void setOrigenInformacion(OrigenInformacion origenInformacion)
    {
        this.origenInformacion = origenInformacion;
    }

    public Date getFechaCreacion()
    {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion)
    {
        if (fechaCreacion != null)
        {
            this.fechaCreacion = fechaCreacion;
        }
    }

    public Set<ContenidoIdioma> getContenidoIdiomas()
    {
        return this.contenidoIdiomas;
    }

    public void setContenidoIdiomas(Set<ContenidoIdioma> contenidoIdiomas)
    {
        this.contenidoIdiomas = contenidoIdiomas;
    }

    public Set<PrioridadContenido> getUpoObjetosPrioridades()
    {
        return this.contenidoPrioridades;
    }

    public void setUpoObjetosPrioridades(Set<PrioridadContenido> upoObjetosPrioridades)
    {
        this.contenidoPrioridades = upoObjetosPrioridades;
    }

    public Set<VigenciaContenido> getUpoVigenciasObjetos()
    {
        return this.upoVigenciasObjetos;
    }

    public void setUpoVigenciasObjetos(Set<VigenciaContenido> upoVigenciasObjetos)
    {
        this.upoVigenciasObjetos = upoVigenciasObjetos;
    }

    public Set<ContenidoTag> getUpoContenidosTags()
    {
        return this.upoContenidosTags;
    }

    public void setUpoContenidosTags(Set<ContenidoTag> upoContenidosTags)
    {
        this.upoContenidosTags = upoContenidosTags;
    }

    public Set<ContenidoMetadato> getUpoContenidosMetadatos()
    {
        return this.upoContenidosMetadatos;
    }

    public void setUpoContenidosMetadatos(Set<ContenidoMetadato> upoContenidosMetadatos)
    {
        this.upoContenidosMetadatos = upoContenidosMetadatos;
    }

    public Set<Autoguardado> getAutoguardados()
    {
        return autoguardados;
    }

    public void setAutoguardados(Set<Autoguardado> autoguardados)
    {
        this.autoguardados = autoguardados;
    }

    public Boolean isAjustarAFechasVigencia()
    {
        return ajustarAFechasVigencia;
    }

    public void setAjustarAFechasVigencia(Boolean ajustarAFechasVigencia)
    {
        this.ajustarAFechasVigencia = ajustarAFechasVigencia;
    }

    @Transient
    public boolean isVigente()
    {
        Date hoy = Calendar.getInstance().getTime();

        boolean vigente = false;

        Iterator<VigenciaContenido> iterator = upoVigenciasObjetos.iterator();

        while (iterator.hasNext())
        {
            VigenciaContenido vigencia = iterator.next();

            if (vigencia.getFecha().after(hoy))
            {
                return true;
            }
        }

        return vigente;
    }

    private void verificarUsuarioConectado(Persona persona)
            throws UsuarioNoAutenticadoException
    {
        if (persona == null)
        {
            throw new UsuarioNoAutenticadoException();
        }
    }

    public void setEstadoModeracionOfNodoMapaContenidoToAceptado()
    {
        Set<NodoMapaContenido> nodoMapaContenidos = this.getUpoMapasObjetos();

        if (nodoMapaContenidos != null && nodoMapaContenidos.size() == 1)
        {
            NodoMapaContenido nodoMapaContenido = nodoMapaContenidos.iterator().next();
            nodoMapaContenido.setEstadoModeracion(EstadoModeracion.ACEPTADO);
        }
    }

    public boolean isAccesible(String text)
    {
        if (text == null || text.isEmpty())
        {
            return true;
        }

        return accesibilityChecker.checkValidAA(text.getBytes());
    }

    public boolean isAccesible()
    {
        for (ContenidoIdioma contenidoIdioma : this.getContenidoIdiomas())
        {
            if ("S".equals(contenidoIdioma.getHtml()) && contenidoIdioma.getHtml() != null)
            {
                if (!accesibilityChecker.checkValidAA(contenidoIdioma.getContenido()))
                {
                    return false;
                }
            }
        }
        return true;
    }

    public void addFechaVigencia(Date date)
    {
        VigenciaContenido vigencia = new VigenciaContenido();
        vigencia.setFecha(new Timestamp(date.getTime()));

        this.upoVigenciasObjetos.add(vigencia);
    }

    public List<ContenidoSolr> toSolrContent(String urlCompleta)
    {
        List<ContenidoSolr> contentList = new ArrayList<ContenidoSolr>();

        for (ContenidoIdioma contenidoIdioma : getContenidoIdiomas())
        {
            ContenidoSolr contenidoSolr = new ContenidoSolr();
            contenidoSolr.setId(String.valueOf(id));
            contenidoSolr.setUrl(urlPath);
            contenidoSolr.setUrlCompleta(urlCompleta);
            contenidoSolr.setIdioma(
                    IdiomaPublicacion.valueOf(contenidoIdioma.getUpoIdioma().getCodigoISO().toUpperCase()));
            contenidoSolr.setTitulo(contenidoIdioma.getTitulo());

            contentList.add(contenidoSolr);
        }

        return contentList;
    }

    public String getUrlCompletaNodoMapa()
    {
        NodoMapa nodo = this.getNodoMapa();
        return nodo.getUrlCompleta();
    }

    public ContenidoIdioma getContenidoIdioma(String idioma)
    {
        for (ContenidoIdioma contenidoIdioma : getContenidoIdiomas())
        {
            if (idioma.equalsIgnoreCase(contenidoIdioma.getUpoIdioma().getCodigoISO()))
            {
                return contenidoIdioma;
            }
        }

        return null;
    }

    public NodoMapa getNodoMapa()
    {
        return nodoMapaContenidoDAO.getNodoMapaTipoNormalByContenidoId(getId());
    }
}