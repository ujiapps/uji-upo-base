package es.uji.apps.upo.model.enums;

public enum TipoPrioridadContenido
{
    URGENTE, ALTA, NORMAL, BAJA;
}
