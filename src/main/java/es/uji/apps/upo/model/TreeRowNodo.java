package es.uji.apps.upo.model;

import es.uji.commons.rest.json.tree.TreeRow;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class TreeRowNodo extends TreeRow
{
    @XmlAttribute
    protected String administrador;

    @XmlAttribute
    protected String nodosMapaHijo;

    @XmlAttribute
    protected String idiomasObligatorios;

    @XmlAttribute
    protected String urlCompleta;

    @XmlAttribute
    protected String urlPath;

    public String getAdministrador()
    {
        return administrador;
    }

    public void setAdministrador(Boolean administrador)
    {
        if (administrador)
        {
            this.administrador = "S";
        }
        else
        {
            this.administrador = "N";
        }
    }

    public void setNodosMapaHijo(String nodosMapaHijo)
    {
        this.nodosMapaHijo = nodosMapaHijo;
    }

    public void setNodosMapaHijo(Boolean nodosMapaHijo)
    {
        this.nodosMapaHijo = (nodosMapaHijo) ? "S" : "N";
    }

    public void setAdministrador(String administrador)
    {
        this.administrador = administrador;
    }

    public void setIdiomasObligatorios(String idiomasObligatorios)
    {
        this.idiomasObligatorios = idiomasObligatorios;
    }

    public String getUrlCompleta()
    {
        return urlCompleta;
    }

    public void setUrlCompleta(String urlCompleta)
    {
        this.urlCompleta = urlCompleta;
    }

    public String getUrlPath()
    {
        return urlPath;
    }

    public void setUrlPath(String urlPath)
    {
        this.urlPath = urlPath;
    }
}
