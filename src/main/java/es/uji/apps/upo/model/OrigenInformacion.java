package es.uji.apps.upo.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Component
@Entity
@Table(name = "UPO_ORIGENES_INFORMACION")
@SuppressWarnings("serial")
public class OrigenInformacion implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    @Column(name = "NOMBRE_ES")
    private String nombreEs;

    @Column(name = "NOMBRE_EN")
    private String nombreEn;

    @OneToMany(mappedBy = "origenInformacion")
    private Set<Contenido> contenido;

    @OneToMany(mappedBy = "origenInformacion")
    private Set<Franquicia> franquicias;

    private String url;

    public OrigenInformacion()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getUrl()
    {
        return this.url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public Set<Contenido> getContenido()
    {
        return contenido;
    }

    public void setContenido(Set<Contenido> contenido)
    {
        this.contenido = contenido;
    }

    public Set<Franquicia> getFranquicias()
    {
        return franquicias;
    }

    public void setFranquicias(Set<Franquicia> franquicias)
    {
        this.franquicias = franquicias;
    }

    public String getNombreEs()
    {
        return nombreEs;
    }

    public void setNombreEs(String nombreEs)
    {
        this.nombreEs = nombreEs;
    }

    public String getNombreEn()
    {
        return nombreEn;
    }

    public void setNombreEn(String nombreEn)
    {
        this.nombreEn = nombreEn;
    }
}