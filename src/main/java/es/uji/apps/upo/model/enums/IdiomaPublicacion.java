package es.uji.apps.upo.model.enums;

public enum IdiomaPublicacion
{
    ES, EN, CA;
}
