package es.uji.apps.upo.model.eujier;

import java.io.Serializable;

public class EujierItemAplicacion implements Serializable
{
    private String nombre;
    private String camino;
    private Long aplicacionId;
    private Long regionId;

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCamino()
    {
        return camino;
    }

    public void setCamino(String camino)
    {
        this.camino = camino;
    }

    public Long getAplicacionId()
    {
        return aplicacionId;
    }

    public void setAplicacionId(Long aplicacionId)
    {
        this.aplicacionId = aplicacionId;
    }

    public Long getRegionId()
    {
        return regionId;
    }

    public void setRegionId(Long regionId)
    {
        this.regionId = regionId;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EujierItemAplicacion that = (EujierItemAplicacion) o;

        if (aplicacionId != null ? !aplicacionId.equals(that.aplicacionId) : that.aplicacionId != null) return false;
        return regionId != null ? regionId.equals(that.regionId) : that.regionId == null;
    }
}