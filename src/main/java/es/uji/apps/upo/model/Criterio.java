package es.uji.apps.upo.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "UPO_CRITERIOS")
@SuppressWarnings("serial")
public class Criterio implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "DIAS_ALTA")
    private Integer diasAlta;

    @Column(name = "DIAS_URGENTE")
    private Integer diasUrgente;

    @Column(name = "DIAS_VENTANA_POPULARIDAD")
    private Integer diasVentanaPopularidad;

    private String nombre;

    @Column(name = "NUMERO_DIAS")
    private Integer numeroDias;

    @Column(name = "PESO_DIAS_EVENTO")
    private Integer pesoDiasEvento;

    @Column(name = "PESO_POPULARIDAD")
    private Integer pesoPopularidad;

    @Column(name = "PESO_PRIORIDAD")
    private Integer pesoPrioridad;

    private String popularidad;

    private String prioridad;

    @Column(name = "PROXIMIDAD_DIAS_EVENTO")
    private String proximidadDiasEvento;

    @OneToMany(mappedBy = "upoCriterio")
    private Set<NodoMapa> upoMapas;

    public Criterio()
    {
    }

    public Criterio(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Integer getDiasAlta()
    {
        return this.diasAlta;
    }

    public void setDiasAlta(Integer diasAlta)
    {
        this.diasAlta = diasAlta;
    }

    public Integer getDiasUrgente()
    {
        return this.diasUrgente;
    }

    public void setDiasUrgente(Integer diasUrgente)
    {
        this.diasUrgente = diasUrgente;
    }

    public Integer getDiasVentanaPopularidad()
    {
        return this.diasVentanaPopularidad;
    }

    public void setDiasVentanaPopularidad(Integer diasVentanaPopularidad)
    {
        this.diasVentanaPopularidad = diasVentanaPopularidad;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Integer getNumeroDias()
    {
        return this.numeroDias;
    }

    public void setNumeroDias(Integer numeroDias)
    {
        this.numeroDias = numeroDias;
    }

    public Integer getPesoDiasEvento()
    {
        return this.pesoDiasEvento;
    }

    public void setPesoDiasEvento(Integer pesoDiasEnvento)
    {
        this.pesoDiasEvento = pesoDiasEnvento;
    }

    public Integer getPesoPopularidad()
    {
        return this.pesoPopularidad;
    }

    public void setPesoPopularidad(Integer pesoPopularidad)
    {
        this.pesoPopularidad = pesoPopularidad;
    }

    public Integer getPesoPrioridad()
    {
        return this.pesoPrioridad;
    }

    public void setPesoPrioridad(Integer pesoPrioridad)
    {
        this.pesoPrioridad = pesoPrioridad;
    }

    public String getPopularidad()
    {
        return this.popularidad;
    }

    public void setPopularidad(String popularidad)
    {
        this.popularidad = popularidad;
    }

    public String getPrioridad()
    {
        return this.prioridad;
    }

    public void setPrioridad(String prioridad)
    {
        this.prioridad = prioridad;
    }

    public String getProximidadDiasEvento()
    {
        return this.proximidadDiasEvento;
    }

    public void setProximidadDiasEvento(String proximidadDiasEvento)
    {
        this.proximidadDiasEvento = proximidadDiasEvento;
    }

    public Set<NodoMapa> getUpoMapas()
    {
        return this.upoMapas;
    }

    public void setUpoMapas(Set<NodoMapa> upoMapas)
    {
        this.upoMapas = upoMapas;
    }
}