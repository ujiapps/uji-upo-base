package es.uji.apps.upo.model.eujier;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "XPFI_VW_ITEMS_FAVORITOS")
@SuppressWarnings("serial")
public class EujierItemFavorito implements Serializable
{
    @Id
    private Long id;

    private String nombre;
    private String descripcion;
    private String url;
    private String idioma;
    private Boolean favorito;

    @Column(name = "url_procedimiento")
    private String urlProcedimiento;

    @Column(name = "per_id")
    private Long personaId;

    public EujierItemFavorito()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getIdioma()
    {
        return idioma;
    }

    public void setIdioma(String idioma)
    {
        this.idioma = idioma;
    }

    public Boolean getFavorito()
    {
        return favorito;
    }

    public void setFavorito(Boolean favorito)
    {
        this.favorito = favorito;
    }

    public String getUrlProcedimiento()
    {
        return urlProcedimiento;
    }

    public void setUrlProcedimiento(String urlProcedimiento)
    {
        this.urlProcedimiento = urlProcedimiento;
    }
}
