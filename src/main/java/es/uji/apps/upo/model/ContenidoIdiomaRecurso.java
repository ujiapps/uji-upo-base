package es.uji.apps.upo.model;

import es.uji.apps.upo.model.enums.TipoMime;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

@Component
@Entity
@Table(name = "UPO_OBJETOS_IDIOMAS_RECURSOS")
@SuppressWarnings("serial")
public class ContenidoIdiomaRecurso implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "CONTENT_TYPE")
    private String contentType;

    @Temporal(TemporalType.DATE)
    private Date fecha;
    private String url;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OBJETO_IDIOMA_ID")
    private ContenidoIdioma contenidoIdioma;

    public ContenidoIdiomaRecurso()
    {
    }

    public static String getTipoMime(String recurso)
    {
        String tipoMime = null;
        String stringUrl;

        try
        {
            URL url = new URL(recurso);
            stringUrl = url.getPath();
        }
        catch (MalformedURLException e)
        {
            stringUrl = recurso;
        }

        try
        {
            String extension = stringUrl.substring(stringUrl.lastIndexOf(".") + 1);
            tipoMime = TipoMime.valueOf(extension.toUpperCase()).getTipoMime();
        }
        catch (Exception e)
        {
        }

        return tipoMime;
    }

    public static String getNombreRecurso(String recurso)
    {
        String nombre = null;
        String stringUrl;

        try
        {
            URL url = new URL(recurso);
            stringUrl = url.getPath().substring(1);
        }
        catch (Exception e)
        {
            stringUrl = recurso;
        }

        try
        {
            nombre = stringUrl.substring(stringUrl.lastIndexOf("/") + 1);
        }
        catch (Exception e)
        {
        }

        return nombre;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getContentType()
    {
        return this.contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public Date getFecha()
    {
        return this.fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public String getUrl()
    {
        return this.url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public ContenidoIdioma getContenidoIdioma()
    {
        return contenidoIdioma;
    }

    public void setContenidoIdioma(ContenidoIdioma contenidoIdioma)
    {
        this.contenidoIdioma = contenidoIdioma;
    }
}