package es.uji.apps.upo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@SuppressWarnings("serial")
@Entity
@Table(name = "UPO_USUARIOS_EXTERNOS")
public class UsuarioExterno implements Serializable
{
    @Id
    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaTodas persona;

    public UsuarioExterno()
    {
    }

    public PersonaTodas getPersona()
    {
        return this.persona;
    }

    public void setPersonaTodas(PersonaTodas persona)
    {
        this.persona = persona;
    }
}
