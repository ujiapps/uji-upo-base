package es.uji.apps.upo.model.eujier.enums;

public enum GrupoPerfil
{
    FAVORITOS, DESTACADOS, NUEVOS, USOPROPIO, USOPERFIL
}
