package es.uji.apps.upo.model.eujier;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;

@Component
@Entity
@Table(name = "XPFI_VW_AVISOS")
@SuppressWarnings("serial")
public class EujierAviso implements Serializable
{
    @Id
    private String id;

    @Column(name = "aviso_ca")
    private String avisoCA;

    @Column(name = "aviso_es")
    private String avisoES;

    @Column(name = "aviso_uk")
    private String avisoEN;

    @Column(name = "persona_id")
    private Long personaId;

    @Transient
    private String aviso;

    public EujierAviso()
    {
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }


    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getAvisoCA()
    {
        return avisoCA;
    }

    public void setAvisoCA(String avisoCA)
    {
        this.avisoCA = avisoCA;
    }

    public String getAvisoES()
    {
        return avisoES;
    }

    public void setAvisoES(String avisoES)
    {
        this.avisoES = avisoES;
    }

    public String getAvisoEN()
    {
        return avisoEN;
    }

    public void setAvisoEN(String avisoEN)
    {
        this.avisoEN = avisoEN;
    }

    public String getAviso()
    {
        return aviso;
    }

    public void setAviso(String aviso)
    {
        this.aviso = aviso;
    }
}