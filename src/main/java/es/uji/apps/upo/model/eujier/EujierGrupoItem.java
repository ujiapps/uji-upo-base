package es.uji.apps.upo.model.eujier;

import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Component
@Entity
@Table(name = "XPFI_VW_GRUPOS_ITEMS")
@SuppressWarnings("serial")
public class EujierGrupoItem implements Serializable
{
    @Id
    private String id;

    @Column(name = "item_id")
    private Long itemId;

    @Column(name = "region_id")
    private Long regionId;

    @Column(name = "nombre_region_ca")
    private String nombreRegionCA;

    @Column(name = "nombre_region_es")
    private String nombreRegionES;

    @Column(name = "nombre_region_uk")
    private String nombreRegionEN;

    @Column(name = "aplicacion_id")
    private Long aplicacionId;

    @Column(name = "nombre_aplicacion_ca")
    private String nombreAplicacionCA;

    @Column(name = "nombre_aplicacion_es")
    private String nombreAplicacionES;

    @Column(name = "nombre_aplicacion_uk")
    private String nombreAplicacionEN;

    @Column(name = "grupo_id")
    private Long grupoId;

    @Column(name = "nombre_grupo_ca")
    private String nombreGrupoCA;

    @Column(name = "nombre_grupo_es")
    private String nombreGrupoES;

    @Column(name = "nombre_grupo_uk")
    private String nombreGrupoEN;

    @Column(name = "nombre_grupo_busqueda_ca")
    private String nombreGrupoBusquedaCA;

    @Column(name = "nombre_grupo_busqueda_es")
    private String nombreGrupoBusquedaES;

    @Column(name = "nombre_grupo_busqueda_uk")
    private String nombreGrupoBusquedaEN;

    @Column(name = "nombre_item_ca")
    private String nombreItemCA;

    @Column(name = "nombre_item_es")
    private String nombreItemES;

    @Column(name = "nombre_item_uk")
    private String nombreItemEN;

    @Column(name = "descripcion_item_ca")
    private String descripcionItemCA;

    @Column(name = "descripcion_item_es")
    private String descripcionItemES;

    @Column(name = "descripcion_item_uk")
    private String descripcionItemEN;

    @Column(name = "per_id")
    private Long personaId;

    private String url;
    private Boolean favorito;
    private Integer orden;

    public EujierGrupoItem()
    {
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Long getItemId()
    {
        return itemId;
    }

    public void setItemId(Long itemId)
    {
        this.itemId = itemId;
    }

    public Long getRegionId()
    {
        return regionId;
    }

    public void setRegionId(Long regionId)
    {
        this.regionId = regionId;
    }

    public Long getAplicacionId()
    {
        return aplicacionId;
    }

    public void setAplicacionId(Long aplicacionId)
    {
        this.aplicacionId = aplicacionId;
    }

    public Long getGrupoId()
    {
        return grupoId;
    }

    public void setGrupoId(Long grupoId)
    {
        this.grupoId = grupoId;
    }

    public String getNombreGrupoCA()
    {
        return nombreGrupoCA;
    }

    public void setNombreGrupoCA(String nombreGrupoCA)
    {
        this.nombreGrupoCA = nombreGrupoCA;
    }

    public String getNombreGrupoES()
    {
        return nombreGrupoES;
    }

    public void setNombreGrupoES(String nombreGrupoES)
    {
        this.nombreGrupoES = nombreGrupoES;
    }

    public String getNombreGrupoEN()
    {
        return nombreGrupoEN;
    }

    public void setNombreGrupoEN(String nombreGrupoEN)
    {
        this.nombreGrupoEN = nombreGrupoEN;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getNombreItemCA()
    {
        return nombreItemCA;
    }

    public void setNombreItemCA(String nombreItemCA)
    {
        this.nombreItemCA = nombreItemCA;
    }

    public String getNombreItemES()
    {
        return nombreItemES;
    }

    public void setNombreItemES(String nombreItemES)
    {
        this.nombreItemES = nombreItemES;
    }

    public String getNombreItemEN()
    {
        return nombreItemEN;
    }

    public void setNombreItemEN(String nombreItemEN)
    {
        this.nombreItemEN = nombreItemEN;
    }

    public String getDescripcionItemCA()
    {
        return descripcionItemCA;
    }

    public void setDescripcionItemCA(String descripcionItemCA)
    {
        this.descripcionItemCA = descripcionItemCA;
    }

    public String getDescripcionItemES()
    {
        return descripcionItemES;
    }

    public void setDescripcionItemES(String descripcionItemES)
    {
        this.descripcionItemES = descripcionItemES;
    }

    public String getDescripcionItemEN()
    {
        return descripcionItemEN;
    }

    public void setDescripcionItemEN(String descripcionItemEN)
    {
        this.descripcionItemEN = descripcionItemEN;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public Boolean getFavorito()
    {
        return favorito;
    }

    public void setFavorito(Boolean favorito)
    {
        this.favorito = favorito;
    }

    public String getNombreGrupoBusquedaCA()
    {
        return nombreGrupoBusquedaCA;
    }

    public void setNombreGrupoBusquedaCA(String nombreGrupoBusquedaCA)
    {
        this.nombreGrupoBusquedaCA = nombreGrupoBusquedaCA;
    }

    public String getNombreGrupoBusquedaES()
    {
        return nombreGrupoBusquedaES;
    }

    public void setNombreGrupoBusquedaES(String nombreGrupoBusquedaES)
    {
        this.nombreGrupoBusquedaES = nombreGrupoBusquedaES;
    }

    public String getNombreGrupoBusquedaEN()
    {
        return nombreGrupoBusquedaEN;
    }

    public void setNombreGrupoBusquedaEN(String nombreGrupoBusquedaEN)
    {
        this.nombreGrupoBusquedaEN = nombreGrupoBusquedaEN;
    }

    public String getNombreRegionCA()
    {
        return nombreRegionCA;
    }

    public void setNombreRegionCA(String nombreRegionCA)
    {
        this.nombreRegionCA = nombreRegionCA;
    }

    public String getNombreRegionES()
    {
        return nombreRegionES;
    }

    public void setNombreRegionES(String nombreRegionES)
    {
        this.nombreRegionES = nombreRegionES;
    }

    public String getNombreRegionEN()
    {
        return nombreRegionEN;
    }

    public void setNombreRegionEN(String nombreRegionEN)
    {
        this.nombreRegionEN = nombreRegionEN;
    }

    public String getNombreAplicacionCA()
    {
        return nombreAplicacionCA;
    }

    public void setNombreAplicacionCA(String nombreAplicacionCA)
    {
        this.nombreAplicacionCA = nombreAplicacionCA;
    }

    public String getNombreAplicacionES()
    {
        return nombreAplicacionES;
    }

    public void setNombreAplicacionES(String nombreAplicacionES)
    {
        this.nombreAplicacionES = nombreAplicacionES;
    }

    public String getNombreAplicacionEN()
    {
        return nombreAplicacionEN;
    }

    public void setNombreAplicacionEN(String nombreAplicacionEN)
    {
        this.nombreAplicacionEN = nombreAplicacionEN;
    }

    public Integer getOrden()
    {
        return orden;
    }

    public void setOrden(Integer orden)
    {
        this.orden = orden;
    }
}