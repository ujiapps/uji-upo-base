package es.uji.apps.upo.model;

import es.uji.apps.upo.model.enums.TipoPlantilla;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Component
@Entity
@Table(name = "UPO_PLANTILLAS")
@SuppressWarnings("serial")
public class Plantilla implements Serializable
{
    public static final Integer DEFAULT_LEVEL = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;

    @Enumerated(EnumType.STRING)
    private TipoPlantilla tipo;

    private String fichero;

    private Boolean obsoleta;

    @OneToMany(mappedBy = "upoPlantilla")
    private Set<NodoMapaPlantilla> upoMapasPlantillas;

    public Plantilla()
    {
        this.obsoleta = false;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public TipoPlantilla getTipo()
    {
        return this.tipo;
    }

    public void setTipo(TipoPlantilla tipo)
    {
        this.tipo = tipo;
    }

    public String getFichero()
    {
        return fichero;
    }

    public void setFichero(String fichero)
    {
        this.fichero = fichero;
    }

    public Set<NodoMapaPlantilla> getUpoMapasPlantillas()
    {
        return this.upoMapasPlantillas;
    }

    public void setUpoMapasPlantillas(Set<NodoMapaPlantilla> upoMapasPlantillas)
    {
        this.upoMapasPlantillas = upoMapasPlantillas;
    }

    public Boolean getObsoleta() {
        return obsoleta;
    }

    public void setObsoleta(Boolean obsoleta) {
        this.obsoleta = obsoleta;
    }
}