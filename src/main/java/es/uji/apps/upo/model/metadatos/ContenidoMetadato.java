package es.uji.apps.upo.model.metadatos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import es.uji.apps.upo.model.Contenido;
import es.uji.apps.upo.model.NodoMapa;

@Component
@Entity
@Table(name = "UPO_OBJETOS_METADATOS")
@SuppressWarnings("serial")
public class ContenidoMetadato implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String clave;

    @Column(name = "NOMBRE_CLAVE_CA")
    private String nombreClaveCA;

    @Column(name = "NOMBRE_CLAVE_ES")
    private String nombreClaveES;

    @Column(name = "NOMBRE_CLAVE_EN")
    private String nombreClaveEN;

    @Column(name = "VALOR_CA")
    private String valorCA;

    @Column(name = "VALOR_ES")
    private String valorES;

    @Column(name = "VALOR_EN")
    private String valorEN;

    @ManyToOne
    @JoinColumn(name = "OBJETO_ID")
    private Contenido upoObjeto;

    @ManyToOne
    @JoinColumn(name = "TIPO_ID")
    private TipoMetadato tipoMetadato;

    @ManyToOne
    @JoinColumn(name = "ATRIBUTO_ID")
    private AtributoMetadato atributoMetadato;

    public ContenidoMetadato()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getClave()
    {
        return this.clave;
    }

    public void setClave(String clave)
    {
        this.clave = clave;
    }

    public String getValorCA()
    {
        return valorCA;
    }

    public void setValorCA(String valorCA)
    {
        this.valorCA = valorCA;
    }

    public String getValorES()
    {
        return valorES;
    }

    public void setValorES(String valorES)
    {
        this.valorES = valorES;
    }

    public String getValorEN()
    {
        return valorEN;
    }

    public void setValorEN(String valorEN)
    {
        this.valorEN = valorEN;
    }

    public Contenido getUpoObjeto()
    {
        return this.upoObjeto;
    }

    public void setUpoObjeto(Contenido upoObjeto)
    {
        this.upoObjeto = upoObjeto;
    }

    public NodoMapa getNodoMapa()
    {
        return this.getUpoObjeto().getNodoMapa();
    }

    public TipoMetadato getTipoMetadato()
    {
        return tipoMetadato;
    }

    public void setTipoMetadato(TipoMetadato tipoMetadato)
    {
        this.tipoMetadato = tipoMetadato;
    }

    public AtributoMetadato getAtributoMetadato()
    {
        return atributoMetadato;
    }

    public void setAtributoMetadato(AtributoMetadato atributoMetadato)
    {
        this.atributoMetadato = atributoMetadato;
    }

    public String getNombreClaveCA()
    {
        return nombreClaveCA;
    }

    public void setNombreClaveCA(String nombreClaveCA)
    {
        this.nombreClaveCA = nombreClaveCA;
    }

    public String getNombreClaveES()
    {
        return nombreClaveES;
    }

    public void setNombreClaveES(String nombreClaveES)
    {
        this.nombreClaveES = nombreClaveES;
    }

    public String getNombreClaveEN()
    {
        return nombreClaveEN;
    }

    public void setNombreClaveEN(String nombreClaveEN)
    {
        this.nombreClaveEN = nombreClaveEN;
    }
}
