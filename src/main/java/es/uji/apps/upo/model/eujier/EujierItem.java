package es.uji.apps.upo.model.eujier;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.*;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "XPFI_VW_ITEMS")
@SuppressWarnings("serial")
public class EujierItem implements Serializable, Comparable
{
    @Id
    private Long id;

    @Column(name = "nombre_ca")
    private String nombreCA;

    @Column(name = "nombre_es")
    private String nombreES;

    @Column(name = "nombre_uk")
    private String nombreEN;

    @Column(name = "descripcion_ca")
    private String descripcionCA;

    @Column(name = "descripcion_es")
    private String descripcionES;

    @Column(name = "descripcion_uk")
    private String descripcionEN;

    private String url;

    @Column(name = "url_procedimiento")
    private String urlProcedimiento;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Column(name = "per_id")
    private Long personaId;

    private Boolean favorito;

    @Transient
    private String nombre;

    @Transient
    private String descripcion;

    @Transient
    private List<EujierItemAplicacion> aplicaciones;

    public EujierItem()
    {
        aplicaciones = new ArrayList<>();
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN)
    {
        this.nombreEN = nombreEN;
    }

    public String getDescripcionCA()
    {
        return descripcionCA;
    }

    public void setDescripcionCA(String descripcionCA)
    {
        this.descripcionCA = descripcionCA;
    }

    public String getDescripcionES()
    {
        return descripcionES;
    }

    public void setDescripcionES(String descripcionES)
    {
        this.descripcionES = descripcionES;
    }

    public String getDescripcionEN()
    {
        return descripcionEN;
    }

    public void setDescripcionEN(String descripcionEN)
    {
        this.descripcionEN = descripcionEN;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }


    public Boolean getFavorito()
    {
        return favorito;
    }

    public void setFavorito(Boolean favorito)
    {
        this.favorito = favorito;
    }

    @Override
    public int compareTo(Object o)
    {
        if (this.nombre == null)
        {
            return 0;
        }

        return this.nombre.compareTo(((EujierItem) o).nombre);
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public String getUrlProcedimiento()
    {
        return urlProcedimiento;
    }

    public void setUrlProcedimiento(String urlProcedimiento)
    {
        this.urlProcedimiento = urlProcedimiento;
    }

    public List<EujierItemAplicacion> getAplicaciones()
    {
        return this.aplicaciones;
    }

    public void setAplicaciones(List<EujierItemAplicacion> aplicaciones)
    {
        for (EujierItemAplicacion aplicacion : aplicaciones)
        {
            addAplicacion(aplicacion);
        }
    }

    public void addAplicacion(EujierItemAplicacion aplicacion)
    {
        Optional<EujierItemAplicacion> item = this.aplicaciones.stream().filter(a -> a.equals(aplicacion)).findFirst();

        if (item.isPresent()) {
            item.get().setCamino(item.get().getCamino() + " /" + aplicacion.getCamino().substring(aplicacion.getCamino().lastIndexOf('>')+1));
            return;
        }

        this.aplicaciones.add(aplicacion);
    }
}