package es.uji.apps.upo.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;

@Component
@Entity
@Table(name = "UPO_MAPAS_TAGS")
@SuppressWarnings("serial")
public class NodoMapaTag implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MAPA_ID")
    private NodoMapa upoMapa;

    @Column(name = "TAGNAME")
    private String tag;

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public NodoMapa getUpoMapa()
    {
        return this.upoMapa;
    }

    public void setUpoMapa(NodoMapa upoMapa)
    {
        this.upoMapa = upoMapa;
    }


    public String getTag()
    {
        return tag;
    }

    public void setTag(String tag)
    {
        this.tag = tag;
    }
}