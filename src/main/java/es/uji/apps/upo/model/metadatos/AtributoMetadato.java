package es.uji.apps.upo.model.metadatos;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "UPO_ATRIBUTOS_METADATOS")
@SuppressWarnings("serial")
public class AtributoMetadato implements Serializable
{
    public static final int DEFAULT_ORDER = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String clave;

    @Column(name = "NOMBRE_CLAVE_CA")
    private String nombreClaveCA;

    @Column(name = "NOMBRE_CLAVE_ES")
    private String nombreClaveES;

    @Column(name = "NOMBRE_CLAVE_EN")
    private String nombreClaveEN;

    @Column(name = "VALOR_DEFECTO_CA")
    private String valorDefectoCA;

    @Column(name = "VALOR_DEFECTO_ES")
    private String valorDefectoES;

    @Column(name = "VALOR_DEFECTO_EN")
    private String valorDefectoEN;

    @Column(name = "PERMITE_BLANCO")
    private Integer permiteBlanco;

    private Integer publicable;

    private Integer orden;

    @OneToMany(mappedBy = "atributoMetadato", cascade = CascadeType.ALL)
    private Set<ValorMetadato> valoresMetadatos;

    @OneToMany(mappedBy = "atributoMetadato")
    private Set<ContenidoMetadato> contenidosMetadatos;

    @ManyToOne
    @JoinColumn(name = "ESQUEMA_ID")
    private EsquemaMetadato esquemaMetadato;

    @ManyToOne
    @JoinColumn(name = "TIPO_ID")
    private TipoMetadato tipoMetadato;

    public AtributoMetadato()
    {
        this.orden = DEFAULT_ORDER;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getClave()
    {
        return this.clave;
    }

    public void setClave(String clave)
    {
        this.clave = clave;
    }

    public Set<ValorMetadato> getValoresMetadatos()
    {
        return valoresMetadatos;
    }

    public void setValoresMetadatos(Set<ValorMetadato> valoresMetadatos)
    {
        this.valoresMetadatos = valoresMetadatos;
    }

    public String getValorDefectoCA()
    {
        return valorDefectoCA;
    }

    public void setValorDefectoCA(String valorDefectoCA)
    {
        this.valorDefectoCA = valorDefectoCA;
    }

    public String getValorDefectoES()
    {
        return valorDefectoES;
    }

    public void setValorDefectoES(String valorDefectoES)
    {
        this.valorDefectoES = valorDefectoES;
    }

    public String getValorDefectoEN()
    {
        return valorDefectoEN;
    }

    public void setValorDefectoEN(String valorDefectoEN)
    {
        this.valorDefectoEN = valorDefectoEN;
    }

    public EsquemaMetadato getEsquemaMetadato()
    {
        return esquemaMetadato;
    }

    public void setEsquemaMetadato(EsquemaMetadato esquemaMetadato)
    {
        this.esquemaMetadato = esquemaMetadato;
    }

    public TipoMetadato getTipoMetadato()
    {
        return tipoMetadato;
    }

    public void setTipoMetadato(TipoMetadato tipoMetadato)
    {
        this.tipoMetadato = tipoMetadato;
    }

    public Integer getPermiteBlanco()
    {
        return permiteBlanco;
    }

    public void setPermiteBlanco(Integer permiteBlanco)
    {
        this.permiteBlanco = permiteBlanco;
    }

    public Set<ContenidoMetadato> getContenidosMetadatos()
    {
        return contenidosMetadatos;
    }

    public void setContenidosMetadatos(Set<ContenidoMetadato> contenidosMetadatos)
    {
        this.contenidosMetadatos = contenidosMetadatos;
    }

    public String getNombreClaveCA()
    {
        return nombreClaveCA;
    }

    public void setNombreClaveCA(String nombreClaveCA)
    {
        this.nombreClaveCA = nombreClaveCA;
    }

    public String getNombreClaveES()
    {
        return nombreClaveES;
    }

    public void setNombreClaveES(String nombreClaveES)
    {
        this.nombreClaveES = nombreClaveES;
    }

    public String getNombreClaveEN()
    {
        return nombreClaveEN;
    }

    public void setNombreClaveEN(String nombreClaveEN)
    {
        this.nombreClaveEN = nombreClaveEN;
    }

    public Integer getPublicable()
    {
        return publicable;
    }

    public void setPublicable(Integer publicable)
    {
        this.publicable = publicable;
    }

    public Integer getOrden()
    {
        return orden;
    }

    public void setOrden(Integer orden)
    {
        this.orden = orden;
    }
}
