package es.uji.apps.upo.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Component
@Entity
@Table(name = "UPO_GRUPOS")
@SuppressWarnings("serial")
public class Grupo implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nombre_ca")
    private String nombreCA;

    @Column(name = "nombre_es")
    private String nombreES;

    @Column(name = "nombre_EN")
    private String nombreEN;

    @OneToMany(mappedBy = "grupo")
    private Set<GrupoItem> gruposItems;

    @OneToMany(mappedBy = "menu")
    private Set<MenuGrupo> menusGrupos;

    @ManyToOne
    @JoinColumn(name = "PER_ID")
    private Persona persona;

    public Grupo()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombreCA()
    {
        return this.nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return this.nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreEN()
    {
        return this.nombreEN;
    }

    public void setNombreEN(String nombreEN)
    {
        this.nombreEN = nombreEN;
    }

    public Set<MenuGrupo> getMenusGrupos()
    {
        return menusGrupos;
    }

    public void setMenusGrupos(Set<MenuGrupo> menusGrupos)
    {
        this.menusGrupos = menusGrupos;
    }

    public Set<GrupoItem> getGruposItems()
    {
        return gruposItems;
    }

    public void setGruposItems(Set<GrupoItem> gruposItems)
    {
        this.gruposItems = gruposItems;
    }

    public Persona getPersona()
    {
        return persona;
    }

    public void setPersona(Persona persona)
    {
        this.persona = persona;
    }
}