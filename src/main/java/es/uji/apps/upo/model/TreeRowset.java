package es.uji.apps.upo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "rowset")
public class TreeRowset
{
    @XmlElement(name = "row")
    protected List<TreeRowNodo> row;

    public List<TreeRowNodo> getRow()
    {
        if (row == null)
        {
            row = new ArrayList<TreeRowNodo>();
        }
        return this.row;
    }

}
