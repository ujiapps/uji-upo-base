package es.uji.apps.upo.utils;

import es.uji.apps.upo.model.enums.TipoFecha;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils
{
    public static String getTimeStamp()
    {
        return new Timestamp(new Date().getTime()).toString();
    }

    public static Date buildDateTime(Date date, Date time, TipoFecha tipoFecha)
            throws ParseException
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        if (date != null)
        {

            String fecha = convertDateToString(date);
            fecha += convertTimeToString(time, tipoFecha);

            return formatter.parse(fecha);
        }

        return null;
    }

    public static Date buildDateTimeFromString(String date, String time)
            throws ParseException
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        if (time == null)
        {
            time = "00:00";
        }

        date = date + ' ' + time;

        return formatter.parse(date);
    }

    public static String convertDateToStringToPrint(Date date)
    {
        Calendar dateCal = Calendar.getInstance();
        dateCal.setTime(date);

        return dateCal.get(Calendar.DAY_OF_MONTH) + "/" + (dateCal.get(Calendar.MONTH) + 1) + "/" + dateCal.get(
                Calendar.YEAR);
    }

    private static String convertDateToString(Date date)
    {
        Calendar dateCal = Calendar.getInstance();
        dateCal.setTime(date);

        return dateCal.get(Calendar.YEAR) + "/" + dateCal.get(Calendar.MONTH) + 1 + "/" + dateCal.get(
                Calendar.DAY_OF_MONTH);
    }

    public static String convertTimeToString(Date time)
    {
        if (time == null) return null;

        return convertTimeToString(time, TipoFecha.INICIO).trim();
    }

    private static String convertTimeToString(Date time, TipoFecha tipoFecha)
    {
        String fecha = " ";
        Calendar timeCal = Calendar.getInstance();

        if (time != null)
        {
            timeCal.setTime(time);
            fecha += timeCal.get(Calendar.HOUR_OF_DAY) + ":" + timeCal.get(Calendar.MINUTE) + ":00";
        }
        else
        {
            fecha += ((TipoFecha.INICIO.equals(tipoFecha)) ? "00:00:00" : "23:59:00");
        }

        return fecha;
    }
}
