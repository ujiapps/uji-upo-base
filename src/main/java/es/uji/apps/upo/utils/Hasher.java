package es.uji.apps.upo.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;

public class Hasher
{
    public static String hash(String value) throws NoSuchAlgorithmException
    {
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        digest.update(value.getBytes());
        byte[] sha1Digest = digest.digest();

        return new String(Hex.encodeHex(sha1Digest));
    }

    public static void main(String[] args) throws NoSuchAlgorithmException
    {
        System.out.println(Hasher.hash("http://perico.com/xx"));
    }
}
