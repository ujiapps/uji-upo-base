package es.uji.apps.upo.utils;

import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class YoutubeUrlFormatter
{
    public String format(String provisionalUrl)
    {
        if (provisionalUrl == null || provisionalUrl.isEmpty()) return provisionalUrl;

        if (!provisionalUrl.startsWith("http"))
        {
            return "https://www.youtube.com/embed/" + provisionalUrl;
        }

        try
        {
            return "https://www.youtube.com/embed/" + getId(provisionalUrl);
        } catch (Exception e)
        {
            return provisionalUrl;
        }
    }

    private String getId(String provisionalUrl)
    {
        String videoId = null;
        String expression =
                "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|watch\\?v%3D|%2Fvideos%2F|embed%2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";

        CharSequence input = provisionalUrl;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(input);

        if (matcher.find())
        {
            String groupIndex1 = matcher.group();
            if (groupIndex1 != null && groupIndex1.length() == 11) videoId = groupIndex1;
        }

        return videoId;
    }
}
