package es.uji.apps.upo.utils;

import java.util.ArrayList;
import java.util.List;

public class UrlUtils
{
    public static String getUrlPath(String url)
    {
        return getUrlPath(url, true);
    }

    public static String getUrlPath(String url, boolean normalize)
    {
        if (url != null)
        {
            String[] urlPathList = url.split("\\/");

            if (isNotEmpty(urlPathList))
            {
                String urlPath = urlPathList[getLastPosition(urlPathList)];

                return normalize ? normalize(urlPath) : urlPath;
            }
        }

        return null;
    }

    public static String getQueryString(String url)
    {
        if (!url.contains("?"))
        {
            return "";
        }

        return url.substring(url.indexOf("?"));
    }

    private static String normalize(String url)
    {
        if (url == null)
        {
            return null;
        }

        if (url.endsWith(".html"))
        {
            return url;
        }

        return url + ".html";
    }

    private static int getLastPosition(String[] urlPathList)
    {
        return urlPathList.length - 1;
    }

    private static boolean isNotEmpty(String[] urlPathList)
    {
        return urlPathList.length > 0;
    }

    public static List<String> convertStringToArray(String url)
    {
        List<String> listaUrlBase = new ArrayList<String>();

        if (!"/".equals(url) && !"".equals(url) && url != null)
        {

            if (url.endsWith("/"))
            {
                url = url.substring(0, url.length() - 1);
            }

            listaUrlBase.add(url + "/");

            while (url.lastIndexOf("/") > 0)
            {
                url = url.substring(0, url.lastIndexOf("/"));
                listaUrlBase.add(url + "/");
            }

            return listaUrlBase;
        }

        return new ArrayList<String>();
    }
}
