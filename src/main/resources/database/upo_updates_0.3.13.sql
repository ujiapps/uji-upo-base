CREATE OR REPLACE FUNCTION UJI_PORTAL.concat_metadata(v_objeto_id NUMBER, v_idioma_iso VARCHAR2)
  RETURN CLOB

IS
  vRdo   CLOB;
  amount INTEGER := 3;
  len    NUMBER;

  CURSOR metadatos IS
    SELECT decode(v_idioma_iso, 'ca', clave || '@@@' || nombre_clave_ca || '@@@' || valor_ca,
                  'es', clave || '@@@' || nombre_clave_es || '@@@' || valor_es,
                  'en', clave || '@@@' || nombre_clave_en || '@@@' || valor_en) || '~~~' result
    FROM (SELECT
            om.clave,
            1 publicable,
            om.nombre_clave_ca,
            om.nombre_clave_es,
            om.nombre_clave_en,
            om.valor_ca,
            om.valor_es,
            om.valor_en
          FROM upo_objetos_metadatos om
          WHERE objeto_id = v_objeto_id
                AND ((v_idioma_iso = 'ca' AND valor_ca IS NOT NULL AND atributo_id IS NULL)
                     OR
                     (v_idioma_iso = 'es' AND valor_es IS NOT NULL AND atributo_id IS NULL)
                     OR
                     (v_idioma_iso = 'en' AND valor_en IS NOT NULL AND atributo_id IS NULL)
                )
          UNION
          SELECT
            am.clave,
            am.publicable,
            am.nombre_clave_ca,
            am.nombre_clave_es,
            am.nombre_clave_en,
            nvl((SELECT valor_ca
                 FROM upo_valores_metadatos
                 WHERE atributo_id = am.id AND to_char(id) = om.valor_ca), om.valor_ca) valor_ca,
            nvl((SELECT valor_es
                 FROM upo_valores_metadatos
                 WHERE atributo_id = am.id AND to_char(id) = om.valor_es), om.valor_es) valor_es,
            nvl((SELECT valor_en
                 FROM upo_valores_metadatos
                 WHERE atributo_id = am.id AND to_char(id) = om.valor_en), om.valor_en) valor_en
          FROM upo_objetos_metadatos om, upo_atributos_metadatos am
          WHERE om.objeto_id = v_objeto_id
                AND om.atributo_id = am.id
                AND ((v_idioma_iso = 'ca' AND om.valor_ca IS NOT NULL)
                     OR
                     (v_idioma_iso = 'es' AND om.valor_es IS NOT NULL)
                     OR
                     (v_idioma_iso = 'en' AND om.valor_en IS NOT NULL)
                )
          UNION
          SELECT
            'esquema',
            0 publicable,
            'esquema',
            'esquema',
            'esquema',
            lower(em.nombre),
            lower(em.nombre),
            lower(em.nombre)
          FROM upo_objetos_metadatos om, upo_atributos_metadatos am, upo_esquemas_metadatos em
          WHERE om.objeto_id = v_objeto_id
                AND om.atributo_id = am.id
                AND em.id = am.esquema_id

    );

  BEGIN

    dbms_lob.createtemporary(vRdo, TRUE);

    FOR m IN metadatos LOOP

      dbms_lob.writeappend(vRdo, length(m.result), m.result);

    END LOOP;

    len := dbms_lob.getlength(vRdo);

    IF (len = 0)
    THEN

      RETURN NULL;

    END IF;

    IF (len > amount)
    THEN

      dbms_lob.erase(vRdo, amount, len - amount + 1);
      dbms_lob.trim(vRdo, len - 3);

    END IF;

    RETURN vRdo;
  END;
/


CREATE OR REPLACE VIEW upo_vw_metadatos AS
  SELECT
    clave,
    nombre_clave,
    quita_acentos(valor) valor,
    objeto_id,
    idioma
  FROM (
    SELECT
      om.clave,
      om.nombre_clave_ca nombre_clave,
      om.valor_ca        valor,
      'ca'               idioma,
      objeto_id
    FROM upo_objetos_metadatos om
    WHERE atributo_id IS NULL
          AND valor_ca IS NOT NULL
    UNION
    SELECT
      am.clave,
      am.nombre_clave_ca                                                          nombre_clave,
      nvl((SELECT valor_ca
           FROM upo_valores_metadatos
           WHERE atributo_id = am.id AND to_char(id) = om.valor_ca), om.valor_ca) valor,
      'ca'                                                                        idioma,
      objeto_id
    FROM upo_objetos_metadatos om, upo_atributos_metadatos am
    WHERE om.atributo_id = am.id
          AND om.valor_ca IS NOT NULL
    UNION
    SELECT
      'esquema',
      'esquema',
      lower(em.nombre),
      'ca' idioma,
      objeto_id
    FROM upo_objetos_metadatos om, upo_atributos_metadatos am, upo_esquemas_metadatos em
    WHERE om.atributo_id = am.id
          AND em.id = am.esquema_id
    UNION
    SELECT
      om.clave,
      om.nombre_clave_es nombre_clave,
      om.valor_es        valor,
      'es'               idioma,
      objeto_id
    FROM upo_objetos_metadatos om
    WHERE atributo_id IS NULL
          AND valor_es IS NOT NULL
    UNION
    SELECT
      am.clave,
      am.nombre_clave_es                                                          nombre_clave,
      nvl((SELECT valor_es
           FROM upo_valores_metadatos
           WHERE atributo_id = am.id AND to_char(id) = om.valor_es), om.valor_es) valor,
      'es'                                                                        idioma,
      objeto_id
    FROM upo_objetos_metadatos om, upo_atributos_metadatos am
    WHERE om.atributo_id = am.id
          AND om.valor_es IS NOT NULL
    UNION
    SELECT
      'esquema',
      'esquema',
      lower(em.nombre),
      'es' idioma,
      objeto_id
    FROM upo_objetos_metadatos om, upo_atributos_metadatos am, upo_esquemas_metadatos em
    WHERE om.atributo_id = am.id
          AND em.id = am.esquema_id
    UNION
    SELECT
      om.clave,
      om.nombre_clave_en nombre_clave,
      om.valor_en        valor,
      'en'               idioma,
      objeto_id
    FROM upo_objetos_metadatos om
    WHERE atributo_id IS NULL
          AND valor_en IS NOT NULL
    UNION
    SELECT
      am.clave,
      am.nombre_clave_en                                                          nombre_clave,
      nvl((SELECT valor_en
           FROM upo_valores_metadatos
           WHERE atributo_id = am.id AND to_char(id) = om.valor_en), om.valor_en) valor,
      'en'                                                                        idioma,
      objeto_id
    FROM upo_objetos_metadatos om, upo_atributos_metadatos am
    WHERE om.atributo_id = am.id
          AND om.valor_en IS NOT NULL
    UNION
    SELECT
      'esquema',
      'esquema',
      lower(em.nombre),
      'en' idioma,
      objeto_id
    FROM upo_objetos_metadatos om, upo_atributos_metadatos am, upo_esquemas_metadatos em
    WHERE om.atributo_id = am.id
          AND em.id = am.esquema_id
  );


CREATE OR REPLACE PROCEDURE borra_nodo(pUrl VARCHAR2, pPapelera VARCHAR2 := 'N') IS
  vUrlPath        upo_mapas.url_path%TYPE;
  vNombre         VARCHAR2(4000);
  vUrlCompleta    upo_mapas.url_completa%TYPE;
  vUrlBaseSinNodo upo_mapas.url_completa%TYPE;
  vFranquiciaId   upo_franquicias.id%TYPE;
  vMenuId         upo_menus.id%TYPE;
  vMapaId         upo_mapas.id%TYPE;

  CURSOR nodos IS
    SELECT m.id
    FROM upo_mapas m
    WHERE m.url_completa LIKE pUrl || '%'
    ORDER BY length(m.url_completa) DESC;

  CURSOR objetos(pNodoId NUMBER) IS
    SELECT o.*
    FROM upo_objetos o, upo_mapas_objetos mo
    WHERE mo.objeto_id = o.id
          AND mo.mapa_id = pNodoId
          AND lower(mo.tipo) = 'normal';

  CURSOR objetos_idiomas(pObjetoId NUMBER) IS
    SELECT *
    FROM upo_objetos_idiomas oi
    WHERE oi.objeto_id = pObjetoId;
  BEGIN
    IF pPapelera = 'S'
    THEN

      SELECT
        url_path,
        franquicia_id,
        menu_id
      INTO vUrlPath, vFranquiciaId, vMenuId
      FROM upo_mapas
      WHERE url_completa = pUrl;

      vNombre := vUrlPath || to_char(systimestamp, 'rrrr-mm-ddhh24miss.ff3');
      vUrlCompleta := '/paperera/' || vNombre || '/';
      vUrlBaseSinNodo := substr(pUrl, 1, instr(pUrl, vUrlPath, -1));

      INSERT INTO
        upo_mapas (id, url_path, url_completa, mapa_id, franquicia_id, menu_id, menu_heredado)
      VALUES
        (
          hibernate_sequence.nextval,
          vNombre,
          vUrlCompleta,
          -2, --id papelera
          vFranquiciaId,
          vMenuId,
          1
        )
      RETURNING id INTO vMapaId;

      UPDATE upo_mapas
      SET mapa_id = vMapaId
      WHERE url_completa = pUrl;

      UPDATE upo_mapas
      SET url_completa = vUrlCompleta || substr(url_completa, length(vUrlBaseSinNodo))
      WHERE url_completa LIKE pUrl || '%';

      COMMIT;

    ELSE

      FOR n IN nodos LOOP
        FOR o IN objetos(n.id) LOOP
          FOR oi IN objetos_idiomas(o.id) LOOP
            INSERT INTO upo_objetos_idiomas_log (ID, OBJETO_ID, IDIOMA_ID, TITULO, TITULO_LARGO, CONTENIDO, RESUMEN, MIME_TYPE, FECHA_OPERACION, TIPO_OPERACION, USUARIO_OPERACION, NOMBRE_FICHERO, HTML, SUBTITULO, FECHA_MODIFICACION,
                                                 ENLACE_DESTINO, LONGITUD, TIPO_FORMATO)
            VALUES (hibernate_sequence.nextval, oi.objeto_id, oi.idioma_id, oi.titulo, oi.titulo_largo, oi.contenido,
                                                oi.resumen, oi.mime_type, sysdate, 'D', -1, oi.nombre_fichero, oi.html,
                    oi.subtitulo, oi.fecha_modificacion,
                    oi.enlace_destino, oi.longitud, oi.tipo_formato);

            DELETE FROM upo_objetos_idiomas_recursos
            WHERE objeto_idioma_id = oi.id;

            DELETE FROM upo_objetos_idiomas_atributos
            WHERE objeto_idioma_id = oi.id;
          END LOOP;

          INSERT INTO upo_objetos_log (ID, URL_PATH, PER_ID_RESPONSABLE, PUBLICABLE, LATITUD, LONGITUD, VISIBLE, TEXTO_NOVISIBLE, URL_ORIGINAL, ORIGEN_INFORMACION_ID, FECHA_CREACION, OTROS_AUTORES, LUGAR, FECHA_OPERACION, TIPO_OPERACION,
                                       USUARIO_OPERACION)
          VALUES (hibernate_sequence.nextval, o.url_path, o.per_id_responsable, o.publicable, o.latitud, o.longitud,
                                              o.visible, o.texto_novisible, o.url_original, o.origen_informacion_id,
                                              o.fecha_creacion, o.otros_autores, o.lugar,
                  sysdate, 'D', -1);

          DELETE FROM upo_objetos_idiomas
          WHERE objeto_id = o.id;
          DELETE FROM upo_objetos_metadatos
          WHERE objeto_id = o.id;
          DELETE FROM upo_objetos_tags
          WHERE objeto_id = o.id;
          DELETE FROM upo_vigencias_objetos
          WHERE objeto_id = o.id;
          DELETE FROM upo_objetos_prioridades
          WHERE objeto_id = o.id;
          DELETE FROM upo_mapas_objetos
          WHERE objeto_id = o.id;
          DELETE FROM upo_autoguardado
          WHERE objeto_id = o.id;

          DELETE FROM upo_objetos
          WHERE id = o.id;

          COMMIT;
        END LOOP;

        DELETE FROM upo_autoguardado
        WHERE mapa_id = n.id;
        DELETE FROM upo_mapas_objetos
        WHERE mapa_id = n.id;
        DELETE FROM upo_mapas_plantillas
        WHERE mapa_id = n.id;
        DELETE FROM upo_moderacion_lotes
        WHERE mapa_id = n.id;
        DELETE FROM upo_mapas_idiomas_exclusiones
        WHERE mapa_id = n.id;

        DELETE FROM upo_mapas
        WHERE id = n.id;

        COMMIT;
      END LOOP;
    END IF;
    EXCEPTION
    WHEN OTHERS THEN
    dbms_output.put_line(sqlerrm);
  END;

CREATE OR REPLACE FORCE VIEW "UJI_PORTAL"."UPO_VW_PUBLICACION" ("CONTENIDO_ID", "CONTENIDO_IDIOMA_ID", "URL_NODO", "URL_NODO_ORIGINAL", "URL_CONTENIDO", "URL_COMPLETA", "URL_COMPLETA_ORIGINAL", "URL_NUM_NIVELES", "URL_DESTINO", "NIVEL", "IDIOMA", "TITULO", "SUBTITULO", "TITULO_LARGO", "RESUMEN", "MIME_TYPE", "ES_HTML", "CONTENIDO", "FECHA_MODIFICACION", "NODO_MAPA_ORDEN", "NODO_MAPA_PADRE_ORDEN", "CONTENIDO_ORDEN", "TIPO_CONTENIDO", "AUTOR", "ORIGEN_NOMBRE", "ORIGEN_URL", "LUGAR", "PROXIMA_FECHA_VIGENCIA", "PRIMERA_FECHA_VIGENCIA", "METADATA", "TITULO_NODO", "ORDEN", "PRIORIDAD", "DISTANCIA_SYSDATE", "TIPO_FORMATO", "ATRIBUTOS", "VISIBLE", "TEXTO_NOVISIBLE") AS
  SELECT
    o.id                                                                       contenido_id,
    oi.id                                                                      contenido_idioma_id,
    m.url_completa                                                             url_nodo,
    m.url_completa                                                             url_nodo_original,
    o.url_path                                                                 url_contenido,
    m.url_completa || o.url_path                                               url_completa,
    m.url_completa || o.url_path                                               url_completa_original,
    url_num_niveles                                                            niveles,
    oi.enlace_destino,
    nvl(mp.nivel, 1),
    i.codigo_iso                                                               idioma,
    oi.titulo,
    oi.subtitulo,
    oi.titulo_largo,
    oi.resumen,
    oi.mime_type,
    oi.html                                                                    es_html,
    decode(oi.html, 'S', oi.contenido, NULL)                                   contenido,
    oi.fecha_modificacion,
    m.orden                                                                    nodo_mapa_orden,
    nvl((SELECT mpadre.orden
         FROM upo_mapas mpadre
         WHERE m.mapa_id = mpadre.id),
        -1)
                                                                               orden_padre,
    mo.orden                                                                   contenido_orden,
    'NORMAL'                                                                   tipo_contenido,
    (SELECT nvl(otros_autores,
                (SELECT upper(nombre || ' ' || apellido1 || ' ' || apellido2)
                 FROM upo_ext_personas
                 WHERE id = per_id_responsable))
     FROM upo_objetos
     WHERE id = o.id)
                                                                               autor,
    decode(i.codigo_iso, 'es', oin.nombre_es, 'en', oin.nombre_en, oin.nombre) origen_nombre,
    oin.url                                                                    origen_url,
    o.lugar,
    get_proxima_fecha_vigencia(o.id)                                           proxima_fecha_vigencia,
    get_primera_fecha_vigencia(o.id)                                           primera_fecha_vigencia,
    concat_metadata(o.id, i.codigo_iso)                                        metadata,
    decode(i.codigo_iso, 'es', m.titulo_publicacion_es, 'en', m.titulo_publicacion_en,
           m.titulo_publicacion_ca)                                            tituloNodo,
    mo.orden,
    get_prioridad(o.id)                                                        prioridad,
    abs(get_inicio_vigencia_contenido(o.id) - sysdate)                         distancia_sysdate,
    oi.tipo_formato,
    concat_atributos(oi.id),
    o.visible,
    o.texto_novisible
  FROM upo_mapas m, upo_mapas_plantillas mp, upo_mapas_objetos mo, upo_objetos o, upo_objetos_idiomas oi, upo_idiomas i,
    upo_origenes_informacion oin
  WHERE m.id = mo.mapa_id
        AND m.id = mp.mapa_id (+)
        AND o.id = mo.objeto_id
        AND o.id = oi.objeto_id
        AND i.id = oi.idioma_id
        AND mo.tipo = 'NORMAL'
        AND o.origen_informacion_id = oin.id (+)
        AND m.url_completa NOT LIKE '/paperera/%'
  UNION ALL
  SELECT
    o.id                                                                       contenido_id,
    oi.id                                                                      contenido_idioma_id,
    m.url_completa                                                             url_nodo,
    xm.url_completa                                                            url_nodo_original,
    o.url_path                                                                 url_contenido,
    m.url_completa || o.url_path                                               url_completa,
    xm.url_completa || o.url_path
                                                                               url_completa_original,
    m.url_num_niveles                                                          niveles,
    oi.enlace_destino,
    nvl(mp.nivel, 1),
    i.codigo_iso                                                               idioma,
    oi.titulo,
    oi.subtitulo,
    oi.titulo_largo,
    oi.resumen,
    oi.mime_type,
    oi.html                                                                    es_html,
    decode(oi.html, 'S', oi.contenido, NULL)                                   contenido,
    oi.fecha_modificacion,
    m.orden                                                                    nodo_mapa_orden,
    nvl((SELECT mpadre.orden
         FROM upo_mapas mpadre
         WHERE m.mapa_id = mpadre.id),
        -1)
                                                                               orden_padre,
    mo.orden                                                                   contenido_orden,
    'LINK'                                                                     tipo_contenido,
    (SELECT nvl(otros_autores,
                (SELECT upper(nombre || ' ' || apellido1 || ' ' || apellido2)
                 FROM upo_ext_personas
                 WHERE id = per_id_responsable))
     FROM upo_objetos
     WHERE id = o.id)
                                                                               autor,
    decode(i.codigo_iso, 'es', oin.nombre_es, 'en', oin.nombre_en, oin.nombre) origen_nombre,
    oin.url                                                                    origen_url,
    o.lugar,
    get_proxima_fecha_vigencia(o.id)                                           proxima_fecha_vigencia,
    get_primera_fecha_vigencia(o.id)                                           primera_fecha_vigencia,
    concat_metadata(o.id, i.codigo_iso)                                        metadata,
    decode(i.codigo_iso, 'es', m.titulo_publicacion_es, 'en', m.titulo_publicacion_en,
           m.titulo_publicacion_ca)                                            tituloNodo,
    mo.orden,
    get_prioridad(o.id)                                                        prioridad,
    abs(get_inicio_vigencia_contenido(o.id) - sysdate)                         distancia_sysdate,
    oi.tipo_formato,
    concat_atributos(oi.id),
    o.visible,
    o.texto_novisible
  FROM upo_mapas m, upo_mapas_plantillas mp, upo_mapas_objetos mo, upo_objetos o, upo_objetos_idiomas oi, upo_idiomas i,
    upo_origenes_informacion oin, upo_mapas xm, upo_mapas_objetos xmo
  WHERE m.id = mo.mapa_id
        AND m.id = mp.mapa_id (+)
        AND o.id = mo.objeto_id
        AND o.id = oi.objeto_id
        AND i.id = oi.idioma_id
        AND mo.tipo = 'LINK'
        AND o.origen_informacion_id = oin.id (+)
        AND mo.estado_moderacion = 'ACEPTADO'
        AND xm.id = xmo.mapa_id
        AND o.id = xmo.objeto_id
        AND xmo.tipo = 'NORMAL'
        AND xm.url_completa NOT LIKE '/paperera%';

ALTER TABLE upo_origenes_informacion MODIFY (nombre VARCHAR2(4000) NOT NULL);


CREATE OR REPLACE FORCE VIEW UPO_VW_PUBLICACION_BINARIOS(ID, URL_NODO, URL_PATH, CONTENIDO, IDIOMA, mime_type) AS
  SELECT
    mo.id,
    m.url_completa url_modo,
    o.url_path,
    oi.contenido,
    i.codigo_iso,
    oi.mime_type
  FROM upo_mapas m, upo_mapas_objetos mo, upo_objetos o, upo_objetos_idiomas oi, upo_idiomas i
  WHERE m.id = mo.mapa_id
        AND o.id = mo.objeto_id
        AND o.id = oi.objeto_id
        AND i.id = oi.idioma_id
        AND ((mo.tipo = 'NORMAL')
             OR (mo.tipo = 'LINK'
                 AND mo.estado_moderacion = 'ACEPTADO'))
        AND nvl(oi.html, 'N') = 'N'
        AND o.visible = 'S';