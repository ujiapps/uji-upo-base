create or replace force view UJI_PORTAL.UPO_VW_PUBLICACION(CONTENIDO_ID, CONTENIDO_IDIOMA_ID, URL_NODO, URL_NODO_ORIGINAL, URL_CONTENIDO, URL_COMPLETA, URL_COMPLETA_ORIGINAL, URL_NUM_NIVELES, URL_DESTINO, NIVEL, IDIOMA, TITULO, SUBTITULO,
TITULO_LARGO, RESUMEN, MIME_TYPE, ES_HTML, CONTENIDO, FECHA_MODIFICACION, NODO_MAPA_ORDEN, NODO_MAPA_PADRE_ORDEN, CONTENIDO_ORDEN, TIPO_CONTENIDO, AUTOR, ORIGEN_NOMBRE, ORIGEN_URL, LUGAR, PROXIMA_FECHA_VIGENCIA, PRIMERA_FECHA_VIGENCIA,
METADATA, TITULO_NODO, ORDEN, PRIORIDAD, DISTANCIA_SYSDATE, TIPO_FORMATO, ATRIBUTOS, VISIBLE, TEXTO_NOVISIBLE) as
   select o.id contenido_id, oi.id contenido_idioma_id, m.url_completa url_nodo, m.url_completa url_nodo_original, o.url_path url_contenido, m.url_completa || o.url_path url_completa, m.url_completa || o.url_path url_completa_original,
          url_num_niveles niveles, oi.enlace_destino, nvl(mp.nivel, 1), i.codigo_iso idioma, oi.titulo, oi.subtitulo, oi.titulo_largo, oi.resumen, oi.mime_type, oi.html es_html, decode(oi.html, 'S', oi.contenido, null) contenido,
          oi.fecha_modificacion, m.orden nodo_mapa_orden, nvl((select mpadre.orden
                                                                 from upo_mapas mpadre
                                                                where m.mapa_id = mpadre.id),
                                                              -1)
                                                             orden_padre, mo.orden contenido_orden, 'NORMAL' tipo_contenido, (select nvl(otros_autores,
                                                                                                                                         (select upper(nombre || ' ' || apellido1 || ' ' || apellido2)
                                                                                                                                            from upo_ext_personas
                                                                                                                                           where id = per_id_responsable))
                                                                                                                                from upo_objetos
                                                                                                                               where id = o.id)
                                                                                                                                autor, oin.nombre origen_nombre, oin.url origen_url, o.lugar,
          get_proxima_fecha_vigencia(o.id) proxima_fecha_vigencia, get_primera_fecha_vigencia(o.id) primera_fecha_vigencia, concat_metadata(o.id, i.codigo_iso) metadata,
          decode(i.codigo_iso, 'es', m.titulo_publicacion_es, 'en', m.titulo_publicacion_en, m.titulo_publicacion_ca) tituloNodo, mo.orden, get_prioridad(o.id) prioridad, abs(get_inicio_vigencia_contenido(o.id) - sysdate) distancia_sysdate,
          oi.tipo_formato, concat_atributos(oi.id), o.visible, o.texto_novisible
     from upo_mapas m, upo_mapas_plantillas mp, upo_mapas_objetos mo, upo_objetos o, upo_objetos_idiomas oi, upo_idiomas i, upo_origenes_informacion oin
    where m.id = mo.mapa_id
      and m.id = mp.mapa_id(+)
      and o.id = mo.objeto_id
      and o.id = oi.objeto_id
      and i.id = oi.idioma_id
      and mo.tipo = 'NORMAL'
      and o.origen_informacion_id = oin.id(+)
      and m.url_completa not like '/paperera/%'
   union all
   select o.id contenido_id, oi.id contenido_idioma_id, m.url_completa url_nodo, xm.url_completa url_nodo_original, o.url_path url_contenido, m.url_completa || o.url_path url_completa, xm.url_completa || o.url_path
                                                                                                                                                                               url_completa_original, m.url_num_niveles niveles,
          oi.enlace_destino, nvl(mp.nivel, 1), i.codigo_iso idioma, oi.titulo, oi.subtitulo, oi.titulo_largo, oi.resumen, oi.mime_type, oi.html es_html, decode(oi.html, 'S', oi.contenido, null) contenido, oi.fecha_modificacion,
          m.orden nodo_mapa_orden, nvl((select mpadre.orden
                                          from upo_mapas mpadre
                                         where m.mapa_id = mpadre.id),
                                       -1)
                                      orden_padre, mo.orden contenido_orden, 'LINK' tipo_contenido, (select nvl(otros_autores,
                                                                                                                (select upper(nombre || ' ' || apellido1 || ' ' || apellido2)
                                                                                                                   from upo_ext_personas
                                                                                                                  where id = per_id_responsable))
                                                                                                       from upo_objetos
                                                                                                      where id = o.id)
                                                                                                       autor, oin.nombre origen_nombre, oin.url origen_url, o.lugar, get_proxima_fecha_vigencia(o.id) proxima_fecha_vigencia,
          get_primera_fecha_vigencia(o.id) primera_fecha_vigencia, concat_metadata(o.id, i.codigo_iso) metadata, decode(i.codigo_iso, 'es', m.titulo_publicacion_es, 'en', m.titulo_publicacion_en, m.titulo_publicacion_ca) tituloNodo,
          mo.orden, get_prioridad(o.id) prioridad, abs(get_inicio_vigencia_contenido(o.id) - sysdate) distancia_sysdate, oi.tipo_formato, concat_atributos(oi.id), o.visible, o.texto_novisible
     from upo_mapas m, upo_mapas_plantillas mp, upo_mapas_objetos mo, upo_objetos o, upo_objetos_idiomas oi, upo_idiomas i, upo_origenes_informacion oin, upo_mapas xm, upo_mapas_objetos xmo
    where m.id = mo.mapa_id
      and m.id = mp.mapa_id(+)
      and o.id = mo.objeto_id
      and o.id = oi.objeto_id
      and i.id = oi.idioma_id
      and mo.tipo = 'LINK'
      and o.origen_informacion_id = oin.id(+)
      and mo.estado_moderacion = 'ACEPTADO'
      and xm.id = xmo.mapa_id
      and o.id = xmo.objeto_id
      and xmo.tipo = 'NORMAL'
      and xm.url_completa not like '/paperera%';


create or replace force view UJI_PORTAL.UPO_VW_BUSQUEDA(CONTENIDO_ID, CONTENIDO_IDIOMA_ID, MAPA_URL_COMPLETA, IDIOMA_CODIGO_ISO, TITULO_SIN_ACENTOS, TITULO_LARGO_SIN_ACENTOS, RESUMEN_SIN_ACENTOS, /*CONTENIDO_SIN_ACENTOS,*/ CONTENIDO, SUBTITULO_SIN_ACENTOS,
ORDEN, PRIORIDAD, DISTANCIA_SYSDATE, FECHA_VIGENCIA, ORIGEN_INFORMACION_ID) as
   select o.id contenido_id, oi.id contenido_idioma_id, m.url_completa mapa_url_completa, lower(i.codigo_iso) idioma_codigo_iso, quita_acentos(oi.titulo) titulo_sin_acentos, quita_acentos(oi.titulo_largo) titulo_largo_sin_acentos,
          quita_acentos(oi.resumen) resumen_sin_acentos,
          /*quita_acentos_clob(blob_to_clob(decode(oi.html, 'S', oi.contenido, null))) contenido_sin_acentos,*/
          decode(oi.html, 'S', oi.contenido, null) contenido,
          quita_acentos(oi.subtitulo) subtitulo_sin_acentos, mo.orden, get_prioridad(o.id) prioridad, abs(get_inicio_vigencia_contenido(o.id) - sysdate) distancia_sysdate, trunc(vo.fecha) fecha,
          origen_informacion_id
     from upo_mapas_objetos mo, upo_objetos o, upo_mapas m, upo_objetos_idiomas oi, upo_idiomas i, upo_vigencias_objetos vo
    where mo.mapa_id = m.id
      and mo.objeto_id = o.id
      and oi.objeto_id = o.id
      and i.id = oi.idioma_id
      and ((mo.tipo = 'NORMAL')
        or (mo.tipo = 'LINK'
        and mo.estado_moderacion = 'ACEPTADO'))
      and o.visible = 'S'
      and oi.html = 'S'
      and vo.objeto_id(+) = o.id;


create or replace view upo_vw_metadatos as
select clave, nombre_clave, quita_acentos(valor) valor, objeto_id, idioma
from (
  select om.clave,
         om.nombre_clave_ca nombre_clave,
         om.valor_ca valor,
         'ca' idioma,
         objeto_id
    from upo_objetos_metadatos om
   where atributo_id is null
     and valor_ca is not null
  union
  select am.clave,
         am.nombre_clave_ca nombre_clave,
         nvl((select valor_ca from upo_valores_metadatos where atributo_id = am.id and to_char(id) = om.valor_ca), om.valor_ca) valor,
         'ca' idioma,
         objeto_id
       from upo_objetos_metadatos om, upo_atributos_metadatos am
       where om.atributo_id = am.id
         and am.publicable = 1
         and om.valor_ca is not null
  union
  select 'esquema', 'esquema', lower(em.nombre), 'ca' idioma, objeto_id
       from upo_objetos_metadatos om, upo_atributos_metadatos am, upo_esquemas_metadatos em
       where om.atributo_id = am.id
         and em.id = am.esquema_id
  union
  select om.clave,
         om.nombre_clave_es nombre_clave,
         om.valor_es valor,
         'es' idioma,
         objeto_id
    from upo_objetos_metadatos om
   where atributo_id is null
     and valor_es is not null
  union
  select am.clave,
         am.nombre_clave_es nombre_clave,
         nvl((select valor_es from upo_valores_metadatos where atributo_id = am.id and to_char(id) = om.valor_es), om.valor_es) valor,
         'es' idioma,
         objeto_id
       from upo_objetos_metadatos om, upo_atributos_metadatos am
       where om.atributo_id = am.id
         and am.publicable = 1
         and om.valor_es is not null
  union
  select 'esquema', 'esquema', lower(em.nombre), 'es' idioma, objeto_id
       from upo_objetos_metadatos om, upo_atributos_metadatos am, upo_esquemas_metadatos em
       where om.atributo_id = am.id
         and em.id = am.esquema_id
  union
  select om.clave,
         om.nombre_clave_en nombre_clave,
         om.valor_en valor,
         'en' idioma,
         objeto_id
    from upo_objetos_metadatos om
   where atributo_id is null
     and valor_en is not null
  union
  select am.clave,
         am.nombre_clave_en nombre_clave,
         nvl((select valor_en from upo_valores_metadatos where atributo_id = am.id and to_char(id) = om.valor_en), om.valor_en) valor,
         'en' idioma,
         objeto_id
       from upo_objetos_metadatos om, upo_atributos_metadatos am
       where om.atributo_id = am.id
         and am.publicable = 1
         and om.valor_en is not null
  union
  select 'esquema', 'esquema', lower(em.nombre), 'en' idioma, objeto_id
       from upo_objetos_metadatos om, upo_atributos_metadatos am, upo_esquemas_metadatos em
       where om.atributo_id = am.id
         and em.id = am.esquema_id
)

alter table upo_origenes_informacion add (nombre_es varchar2(4000), nombre_en varchar2(4000));


CREATE OR REPLACE TRIGGER UJI_PORTAL.upo_mapas_control_huerfanos
BEFORE INSERT OR UPDATE
OF URL_COMPLETA
ON UJI_PORTAL.UPO_MAPAS
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
BEGIN

  if :new.id != 0 and :new.mapa_id is null then

    raise_application_error(-20000, 'ERRADA: node s''ha quedat orfe. Contacteu amb l''Administrador');

  end if;

END ;
/