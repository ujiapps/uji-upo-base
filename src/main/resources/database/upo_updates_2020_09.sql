CREATE OR REPLACE function UJI_PORTAL.concat_fechas_evento(v_mapa_objeto_id number)
return clob
is
   vRdo clob;
   amount INTEGER := 3;
   len number;

   cursor fechas is
     select to_char(fecha, 'dd/mm/rrrr') || '@@@' || to_char(hora_inicio, 'hh24:mi') || '@@@' || to_char(hora_fin, 'hh24:mi') || '~~~' fecha
       from upo_fechas_evento fe
      where fe.mapa_objeto_id = v_mapa_objeto_id
      order by fe.fecha, fe.hora_inicio;

begin

    dbms_lob.createtemporary(vRdo, true);

    for f in fechas loop

      dbms_lob.writeappend(vRdo, length(f.fecha), f.fecha);

    end loop;

    len := dbms_lob.getlength(vRdo);

    if (len = 0) then

      return null;

    end if;

    if (len > amount) then

      dbms_lob.erase(vRdo, amount, len - amount + 1);
      dbms_lob.trim(vRdo, len - 3);

    end if;

    return vRdo;
end;
/

alter table upo_mapas add mostrar_fechas_evento number(1) default 0 not null

