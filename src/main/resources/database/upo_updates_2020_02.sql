create table upo_historico_moderaciones
(
    id                varchar2(4000) not null,
    mapa_objeto_id    number         not null,
    objeto_id         number         not null,
    mapa_id           number         not null,
    objeto_titulo     varchar2(4000),
    objeto_url_path   varchar2(4000),
    mapa_url_completa varchar2(4000),
    url_completa      varchar2(4000),
    estado_moderacion varchar2(20),
    per_id_moderacion number,
    per_id_propuesta  number,
    propuesta_externa number,
    texto_rechazo     varchar2(4000)
);

ALTER TABLE UJI_PORTAL.UPO_HISTORICO_MODERACIONES
    ADD
        CONSTRAINT UPO_HISTORICO_MODERACIONES_PK
            PRIMARY KEY (ID);

CREATE INDEX UJI_PORTAL.UPO_HIST_MODS_PER_MOD_I ON UJI_PORTAL.UPO_HISTORICO_MODERACIONES
    (PER_ID_MODERACION);

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_MAPAS_OBJETOS
            (
             ID,
             MAPA_OBJETO_ID,
             OBJETO_ID,
             MAPA_ID,
             ESTADO_MODERACION,
             PER_ID_MODERACION,
             PER_ID_PROPUESTA,
             TEXTO_RECHAZO,
             OBJETO_URL_PATH,
             URL_COMPLETA,
             MAPA_URL_COMPLETA,
             PROPUESTA_EXTERNA,
             OBJETO_TITULO
                )
            BEQUEATH DEFINER
AS
SELECT mapa_objeto_id id,
       mapa_objeto_id,
       objeto_id,
       mapa_id,
       estado_moderacion,
       per_id_moderacion,
       per_id_propuesta,
       texto_rechazo,
       objeto_url_path,
       url_completa,
       mapa_url_completa,
       propuesta_externa,
       (CASE
            WHEN titulo_ca IS NOT NULL THEN titulo_ca
            WHEN titulo_es IS NOT NULL THEN titulo_es
            WHEN titulo_en IS NOT NULL THEN titulo_en
            ELSE NULL
           END)       titulo
FROM (SELECT mo.id                              mapa_objeto_id,
             mo.objeto_id                       objeto_id,
             mo.mapa_id                         mapa_id,
             mo.estado_moderacion               estado_moderacion,
             mo.per_id_moderacion               per_id_moderacion,
             mo.per_id_propuesta                per_id_propuesta,
             mo.texto_rechazo                   texto_rechazo,
             o.url_path                         objeto_url_path,
             m.url_completa || o.url_path       url_completa,
             m.url_completa                     mapa_url_completa,
             mo.propuesta_externa               propuesta_externa,
             (SELECT titulo
              FROM upo_objetos_idiomas oi,
                   upo_idiomas i
              WHERE oi.objeto_id = o.id
                AND i.id = oi.idioma_id
                AND UPPER(i.codigo_iso) = 'CA') titulo_ca,
             (SELECT titulo
              FROM upo_objetos_idiomas oi,
                   upo_idiomas i
              WHERE oi.objeto_id = o.id
                AND i.id = oi.idioma_id
                AND UPPER(i.codigo_iso) = 'ES') titulo_es,
             (SELECT titulo
              FROM upo_objetos_idiomas oi,
                   upo_idiomas i
              WHERE oi.objeto_id = o.id
                AND i.id = oi.idioma_id
                AND UPPER(i.codigo_iso) = 'EN') titulo_en
      FROM upo_mapas_objetos mo,
           upo_objetos o,
           upo_mapas m
      WHERE mo.mapa_id = m.id
        AND mo.objeto_id = o.id) s;



create table upo_fechas_evento
(
    id             number primary key not null,
    mapa_objeto_id number             not null,
    fecha          timestamp(6)       not null,
    hora_inicio    date,
    hora_fin       date
);

ALTER TABLE UJI_PORTAL.upo_fechas_evento
    ADD
        CONSTRAINT upo_fechas_evento_R01
            FOREIGN KEY (MAPA_OBJETO_ID)
                REFERENCES UJI_PORTAL.UPO_MAPAS_OBJETOS (ID);

CREATE INDEX UJI_PORTAL.upo_fechas_evento_FK_I ON UJI_PORTAL.upo_fechas_evento
    (MAPA_OBJETO_ID);

CREATE OR REPLACE procedure UJI_PORTAL.borra_nodo_papelera(pUrl varchar2) is
    vUrlPath        upo_mapas.url_path%type;
    vNombre         varchar2(4000);
    vUrlCompleta    upo_mapas.url_completa%type;
    vUrlBaseSinNodo upo_mapas.url_completa%type;
    vFranquiciaId   upo_franquicias.id%type;
    vMenuId         upo_menus.id%type;
    vMapaId         upo_mapas.id%type;
    cursor nodos is
        select m.id, m.url_completa
        from upo_mapas m
        where m.url_completa like pUrl || '%'
        order by length(m.url_completa) desc;
    cursor objetos(pNodoId number) is
        select o.*
        from upo_objetos o,
             upo_mapas_objetos mo
        where mo.objeto_id = o.id
          and mo.mapa_id = pNodoId
          and lower(mo.tipo) = 'normal';
    cursor objetos_idiomas(pObjetoId number) is
        select *
        from upo_objetos_idiomas oi
        where oi.objeto_id = pObjetoId;
begin

    if pUrl is null or pUrl = '' then
        return;
    end if;

    if not pUrl like '/paperera/%' then
        return;
    end if;

    dbms_output.put_line(pUrl);

    for n in nodos
        loop

            dbms_output.put_line(n.id || ' ' || n.url_completa);

            for o in objetos(n.id)
                loop
                    for oi in objetos_idiomas(o.id)
                        loop
                            insert into upo_objetos_idiomas_log(ID, OBJETO_ID, IDIOMA_ID, TITULO, TITULO_LARGO,
                                                                CONTENIDO, RESUMEN, MIME_TYPE, FECHA_OPERACION,
                                                                TIPO_OPERACION, USUARIO_OPERACION, NOMBRE_FICHERO, HTML,
                                                                SUBTITULO, FECHA_MODIFICACION,
                                                                ENLACE_DESTINO, LONGITUD, TIPO_FORMATO)
                            values (hibernate_sequence.nextval, oi.objeto_id, oi.idioma_id, oi.titulo, oi.titulo_largo,
                                    oi.contenido, oi.resumen, oi.mime_type, sysdate, 'D', -1, oi.nombre_fichero,
                                    oi.html, oi.subtitulo, oi.fecha_modificacion,
                                    oi.enlace_destino, oi.longitud, oi.tipo_formato);

                            delete
                            from upo_objetos_idiomas_recursos
                            where objeto_idioma_id = oi.id;

                            delete
                            from upo_objetos_idiomas_atributos
                            where objeto_idioma_id = oi.id;
                        end loop;

                    insert into upo_objetos_log(ID, URL_PATH, PER_ID_RESPONSABLE, PUBLICABLE, LATITUD, LONGITUD,
                                                VISIBLE, TEXTO_NOVISIBLE, URL_ORIGINAL, ORIGEN_INFORMACION_ID,
                                                FECHA_CREACION, OTROS_AUTORES, LUGAR, FECHA_OPERACION, TIPO_OPERACION,
                                                USUARIO_OPERACION)
                    values (hibernate_sequence.nextval, o.url_path, o.per_id_responsable, o.publicable, o.latitud,
                            o.longitud, o.visible, o.texto_novisible, o.url_original, o.origen_informacion_id,
                            o.fecha_creacion, o.otros_autores, o.lugar,
                            sysdate, 'D', -1);

                    delete
                    from upo_objetos_idiomas
                    where objeto_id = o.id;
                    delete
                    from upo_objetos_metadatos
                    where objeto_id = o.id;
                    delete
                    from upo_objetos_tags
                    where objeto_id = o.id;
                    delete
                    from upo_vigencias_objetos
                    where objeto_id = o.id;
                    delete
                    from upo_objetos_prioridades
                    where objeto_id = o.id;
                    delete
                    from upo_fechas_evento
                    where mapa_objeto_id in (select id
                                             from upo_mapas_objetos
                                             where objeto_id = o.id
                    );
                    delete
                    from upo_mapas_objetos
                    where objeto_id = o.id;
                    delete
                    from upo_autoguardado
                    where objeto_id = o.id;

                    delete from upo_objetos where id = o.id;

                    commit;
                end loop;

            delete
            from upo_autoguardado
            where mapa_id = n.id;
            delete
            from upo_fechas_evento
            where mapa_objeto_id in (select id
                                     from upo_mapas_objetos
                                     where mapa_id = n.id
            );
            delete
            from upo_mapas_objetos
            where mapa_id = n.id;
            delete
            from upo_mapas_plantillas
            where mapa_id = n.id;
            delete
            from upo_moderacion_lotes
            where mapa_id = n.id;
            delete
            from upo_mapas_idiomas_exclusiones
            where mapa_id = n.id;

            delete from upo_mapas where id = n.id;

            commit;
        end loop;

exception
    when others then
        dbms_output.put_line(sqlerrm);
end;
/

DROP VIEW UJI_PORTAL.UPO_VW_MODERACION;

/* Formatted on 12/02/2020 15:53:10 (QP5 v5.336) */
CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_MODERACION
            (
             ID,
             MAPA_OBJETO_ID,
             OBJETO_ID,
             MAPA_ID,
             ESTADO_MODERACION,
             PER_ID_MODERACION,
             PER_ID_PROPUESTA,
             TEXTO_RECHAZO,
             OBJETO_URL_PATH,
             URL_COMPLETA,
             MAPA_URL_COMPLETA,
             PERSONA_ID,
             OBJETO_TITULO,
             URL_COMPLETA_ORIGINAL
                )
            BEQUEATH DEFINER
AS
SELECT mapa_objeto_id || '-' || persona_id,
       mapa_objeto_id,
       objeto_id,
       mapa_id,
       estado_moderacion,
       per_id_moderacion,
       per_id_propuesta,
       texto_rechazo,
       objeto_url_path,
       url_completa,
       mapa_url_completa,
       persona_id,
       (CASE
            WHEN titulo_ca IS NOT NULL THEN titulo_ca
            WHEN titulo_es IS NOT NULL THEN titulo_es
            WHEN titulo_en IS NOT NULL THEN titulo_en
            ELSE NULL
           END)                   titulo,
       (SELECT m.url_completa
        FROM upo_mapas m,
             upo_mapas_objetos mo,
             upo_objetos o
        WHERE o.id = mo.objeto_id
          AND m.id = mo.mapa_id
          AND mo.tipo = 'NORMAL'
          AND o.id = s.objeto_id) url_completa_original
FROM (SELECT DISTINCT mo.id                              mapa_objeto_id,
                      mo.objeto_id                       objeto_id,
                      mo.mapa_id                         mapa_id,
                      mo.estado_moderacion               estado_moderacion,
                      mo.per_id_moderacion               per_id_moderacion,
                      mo.per_id_propuesta                per_id_propuesta,
                      mo.texto_rechazo                   texto_rechazo,
                      o.url_path                         objeto_url_path,
                      m.url_completa || o.url_path       url_completa,
                      m.url_completa                     mapa_url_completa,
                      p.id                               persona_id,
                      (SELECT titulo
                       FROM upo_objetos_idiomas oi,
                            upo_idiomas i
                       WHERE oi.objeto_id = o.id
                         AND i.id = oi.idioma_id
                         AND UPPER(i.codigo_iso) = 'CA') titulo_ca,
                      (SELECT titulo
                       FROM upo_objetos_idiomas oi,
                            upo_idiomas i
                       WHERE oi.objeto_id = o.id
                         AND i.id = oi.idioma_id
                         AND UPPER(i.codigo_iso) = 'ES') titulo_es,
                      (SELECT titulo
                       FROM upo_objetos_idiomas oi,
                            upo_idiomas i
                       WHERE oi.objeto_id = o.id
                         AND i.id = oi.idioma_id
                         AND UPPER(i.codigo_iso) = 'EN') titulo_en
      FROM upo_mapas_objetos mo,
           upo_objetos o,
           upo_mapas m,
           upo_ext_personas p
      WHERE mo.estado_moderacion = 'PENDIENTE'
        AND mo.mapa_id = m.id
        AND mo.objeto_id = o.id
        AND m.url_completa NOT LIKE '/paperera/%'
        AND p.id IN
            (SELECT DISTINCT per_id
             FROM upo_franquicias_accesos fa,
                  upo_franquicias f,
                  upo_mapas m2
             WHERE m2.franquicia_id = f.id
               AND fa.franquicia_id = f.id
               AND fa.tipo = 'ADMINISTRADOR'
               AND m.url_completa LIKE
                   m2.url_completa || '%')) s;

ALTER TABLE UJI_PORTAL.UPO_MAPAS
    ADD (bloqueado NUMBER DEFAULT 0 NOT NULL);

CREATE OR REPLACE package body UJI_PORTAL.sync_contenidos as

    procedure clean_target_directory(p_destino varchar2) is
    begin

        delete
        from upo_fechas_evento
        where mapa_objeto_id in (select mo.id
                                 from upo_mapas m,
                                      upo_mapas_objetos mo
                                 where m.url_completa like p_destino || '_%'
                                   and mo.mapa_id = m.id
        );

        delete
        from upo_mapas_objetos
        where mapa_id in (select id
                          from upo_mapas
                          where url_completa like p_destino || '_%');

        delete
        from upo_mapas_plantillas
        where mapa_id in (select id
                          from upo_mapas
                          where url_completa like p_destino || '_%');

        delete
        from upo_mapas
        where url_completa like p_destino || '_%';
    end;

    function get_source_directories_setting(p_origen varchar2, p_num_niveles number)
        return t_contents
        is
        vContenidos t_contents;
    begin
        execute immediate 'select url_nodo
                           from upo_vw_sync_contenidos
                          where url_nodo like ''' || p_origen || '%''
                            and ((''' || p_num_niveles || ''' is null)
                                 or
                                 (''' || p_num_niveles || ''' is not null and (REGEXP_COUNT(''' || p_origen ||
                          ''', ''/'') + ''' || p_num_niveles || ''') >= url_num_niveles)
                                )
                           order by prioridad, distancia_sysdate, orden, fecha_creacion desc'
            bulk collect into vContenidos;

        return vContenidos;
    end;

    function get_recent_directories(p_origen varchar2, p_max_resultados number)
        return t_contents
        is
        vContenidos t_contents;
    begin

        execute immediate 'select url_completa
                          from (select m.url_completa
                            from upo_objetos o,
                                 upo_objetos_idiomas oi,
                                 upo_idiomas i,
                                 upo_mapas_objetos mo,
                                 upo_mapas m
                           where oi.objeto_id = o.id
                             and oi.idioma_id = i.id
                             and i.codigo_iso = ''ca''
                             and html = ''S''
                             and mo.objeto_id = o.id
                             and mo.mapa_id = m.id
                             and mo.tipo = ''NORMAL''
                             and m.url_completa like ''' || p_origen || '%''
                             and m.url_completa not like ''/paperera/%''
                             and m.url_completa != ''' || p_origen || '''
                           order by o.fecha_creacion desc)
                          where rownum <=' || p_max_resultados
            bulk collect into vContenidos;

        return vContenidos;
    end;

    function contains_path(vPaths t_array_output, vPath varchar2)
        return boolean
        is
    begin
        for i in 1..vPaths.count
            loop
                if vPaths(i) = vPath then
                    return true;
                end if;
            end loop;

        return false;
    end;

    procedure add_source_dirs_to_target(p_contents t_contents, p_destino varchar2, p_max_contenidos number) is
        vDirectory                varchar2(4000);
        vTargetNode               number;
        vNewNode                  number;
        vFranquicia               number;
        vMenu                     number;
        vAux                      varchar2(4000);
        vPaths                    t_array_output := t_array_output();
        v_new_nodo_mapa_objeto_id number;
        cursor mapas_objetos(p_url_completa varchar2) is
            select id, objeto_id
            from upo_mapas_objetos
            where mapa_id = (select id
                             from upo_mapas
                             where url_completa = p_url_completa);
        cursor fechas_evento(p_nodo_mapa_objeto_id number) is
            select *
            from upo_fechas_evento
            where mapa_objeto_id = p_nodo_mapa_objeto_id;

    begin
        select id, franquicia_id, menu_id
        into vTargetNode, vFranquicia, vMenu
        from upo_mapas
        where url_completa = p_destino;

        for i in 1..p_contents.count
            loop
                begin
                    vAux := replace(p_contents(i).url_nodo, '/', '-');
                    vDirectory := substr(vAux, 2, length(vAux) - 2);

                    if not contains_path(vPaths, vDirectory) then
                        if vPaths.count < p_max_contenidos then
                            insert into upo_mapas(id, url_path, franquicia_id, url_completa, menu_id, mapa_id,
                                                  menu_heredado, orden)
                            values (hibernate_sequence.nextval, vDirectory, vFranquicia, p_destino || vDirectory || '/',
                                    vMenu, vTargetNode, 1, i)
                            returning id into vNewNode;

                            vPaths.extend;
                            vPaths(vPaths.count) := vDirectory;

                            /*insert into upo_mapas_objetos (id, mapa_id, objeto_id, estado_moderacion, tipo)
                              select hibernate_sequence.nextval, vNewNode, objeto_id, 'ACEPTADO', 'LINK'
                              from upo_mapas_objetos
                              where mapa_id = (select id
                                               from upo_mapas
                                               where url_completa = p_contents(i).url_nodo);*/

                            for mp in mapas_objetos(p_contents(i).url_nodo)
                                loop

                                    insert into upo_mapas_objetos (id, mapa_id, objeto_id, estado_moderacion, tipo)
                                    values (hibernate_sequence.nextval, vNewNode, mp.objeto_id, 'ACEPTADO', 'LINK')
                                    returning id into v_new_nodo_mapa_objeto_id;

                                    for fe in fechas_evento(mp.id)
                                        loop

                                            insert into upo_fechas_evento (id, mapa_objeto_id, fecha, hora_inicio, hora_fin)
                                            values (hibernate_sequence.nextval, v_new_nodo_mapa_objeto_id, fe.fecha,
                                                    fe.hora_inicio, fe.hora_fin);

                                        end loop;

                                end loop;
                        end if;
                    end if;
                exception
                    when others then
                        rollback;
                        dbms_output.put_line('Directory: ' || vDirectory || ' - Error: ' || sqlerrm);
                end;
            end loop;
    end;

    procedure sync_directory_from_settings(p_origen varchar2,
                                           p_destino varchar2,
                                           p_max_contenidos number,
                                           p_num_niveles number) is
    begin
        clean_target_directory(p_destino);
        vSourceDirectories := get_source_directories_setting(p_origen, p_num_niveles);
        add_source_dirs_to_target(vSourceDirectories, p_destino, p_max_contenidos);

        commit;
    end;

    procedure sync_recent_contents(p_origen varchar2,
                                   p_destino varchar2,
                                   p_max_contenidos number) is
    begin
        clean_target_directory(p_destino);
        vSourceDirectories := get_recent_directories(p_origen, p_max_contenidos);
        add_source_dirs_to_target(vSourceDirectories, p_destino, p_max_contenidos);

        commit;
    end;

end;
/

CREATE OR REPLACE function UJI_PORTAL.concat_fechas_evento(v_mapa_objeto_id number)
    return clob
    is
    vRdo   clob;
    amount INTEGER := 3;
    len    number;
    cursor fechas is
        select to_char(fecha, 'dd/mm/rrrr') || '@@@' || to_char(hora_inicio, 'hh24:mi') || '@@@' ||
               to_char(hora_fin, 'hh24:mi') || '~~~' fecha
        from upo_fechas_evento fe
        where fe.mapa_objeto_id = v_mapa_objeto_id
        order by fecha, hora_inicio;

begin

    dbms_lob.createtemporary(vRdo, true);

    for f in fechas
        loop

            dbms_lob.writeappend(vRdo, length(f.fecha), f.fecha);

        end loop;

    len := dbms_lob.getlength(vRdo);

    if (len = 0) then

        return null;

    end if;

    if (len > amount) then

        dbms_lob.erase(vRdo, amount, len - amount + 1);
        dbms_lob.trim(vRdo, len - 3);

    end if;

    return vRdo;
end;
/

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_PUBLICACION
            (
             CONTENIDO_ID,
             CONTENIDO_IDIOMA_ID,
             URL_NODO,
             URL_NODO_ORIGINAL,
             URL_CONTENIDO,
             URL_COMPLETA,
             URL_COMPLETA_ORIGINAL,
             URL_NUM_NIVELES,
             URL_DESTINO,
             NIVEL,
             IDIOMA,
             TITULO,
             SUBTITULO,
             TITULO_LARGO,
             RESUMEN,
             MIME_TYPE,
             ES_HTML,
             CONTENIDO,
             FECHA_MODIFICACION,
             FECHA_CREACION,
             NODO_MAPA_ORDEN,
             NODO_MAPA_PADRE_ORDEN,
             CONTENIDO_ORDEN,
             TIPO_CONTENIDO,
             AUTOR,
             ORIGEN_NOMBRE,
             ORIGEN_URL,
             LUGAR,
             PROXIMA_FECHA_VIGENCIA,
             PRIMERA_FECHA_VIGENCIA,
             METADATA,
             TITULO_NODO,
             ORDEN,
             PRIORIDAD,
             DISTANCIA_SYSDATE,
             TIPO_FORMATO,
             ATRIBUTOS,
             VISIBLE,
             TEXTO_NOVISIBLE,
             TAGS,
             ORDEN_MAPA,
             FECHAS_EVENTO
                )
            BEQUEATH DEFINER
AS
SELECT o.id
           contenido_id,
       oi.id
           contenido_idioma_id,
       m.url_completa
           url_nodo,
       m.url_completa
           url_nodo_original,
       o.url_path
           url_contenido,
       m.url_completa || o.url_path
           url_completa,
       m.url_completa || o.url_path
           url_completa_original,
       url_num_niveles
           niveles,
       oi.enlace_destino,
       NVL(mp.nivel, 1),
       i.codigo_iso
           idioma,
       oi.titulo,
       oi.subtitulo,
       oi.titulo_largo,
       oi.resumen,
       oi.mime_type,
       oi.html
           es_html,
       DECODE(oi.html, 'S', oi.contenido, NULL)
           contenido,
       oi.fecha_modificacion,
       o.fecha_creacion,
       m.orden
           nodo_mapa_orden,
       dame_orden_padre(m.mapa_id)
           orden_padre,
       mo.orden
           contenido_orden,
       'NORMAL'
           tipo_contenido,
       dame_autores(o.id)
           autor,
       DECODE(i.codigo_iso,
              'es', oin.nombre_es,
              'en', oin.nombre_en,
              oin.nombre)
           origen_nombre,
       oin.url
           origen_url,
       o.lugar,
       get_proxima_fecha_vigencia(o.id)
           proxima_fecha_vigencia,
       get_primera_fecha_vigencia(o.id)
           primera_fecha_vigencia,
       concat_metadata(o.id, i.codigo_iso)
           metadata,
       DECODE(i.codigo_iso,
              'es', m.titulo_publicacion_es,
              'en', m.titulo_publicacion_en,
              m.titulo_publicacion_ca)
           tituloNodo,
       mo.orden,
       get_prioridad(o.id)
           prioridad,
       ABS(get_inicio_vigencia_contenido(o.id) - SYSDATE)
           distancia_sysdate,
       oi.tipo_formato,
       concat_atributos(oi.id),
       o.visible,
       o.texto_novisible,
       concat_tags(o.id),
       m.orden,
       concat_fechas_evento(mo.id)
FROM upo_mapas m,
     upo_mapas_plantillas mp,
     upo_mapas_objetos mo,
     upo_objetos o,
     upo_objetos_idiomas oi,
     upo_idiomas i,
     upo_origenes_informacion oin
WHERE m.id = mo.mapa_id
  AND m.id = mp.mapa_id(+)
  AND o.id = mo.objeto_id
  AND o.id = oi.objeto_id
  AND i.id = oi.idioma_id
  AND mo.tipo = 'NORMAL'
  AND mo.estado_moderacion = 'ACEPTADO'
  AND o.origen_informacion_id = oin.id(+)
  AND m.url_completa NOT LIKE '/paperera/%'
UNION ALL
SELECT o.id
           contenido_id,
       oi.id
           contenido_idioma_id,
       m.url_completa
           url_nodo,
       xm.url_completa
           url_nodo_original,
       o.url_path
           url_contenido,
       m.url_completa || o.url_path
           url_completa,
       xm.url_completa || o.url_path
           url_completa_original,
       m.url_num_niveles
           niveles,
       oi.enlace_destino,
       NVL(mp.nivel, 1),
       i.codigo_iso
           idioma,
       oi.titulo,
       oi.subtitulo,
       oi.titulo_largo,
       oi.resumen,
       oi.mime_type,
       oi.html
           es_html,
       DECODE(oi.html, 'S', oi.contenido, NULL)
           contenido,
       oi.fecha_modificacion,
       o.fecha_creacion,
       m.orden
           nodo_mapa_orden,
       dame_orden_padre(m.mapa_id)
           orden_padre,
       mo.orden
           contenido_orden,
       'LINK'
           tipo_contenido,
       dame_autores(o.id)
           autor,
       DECODE(i.codigo_iso,
              'es', oin.nombre_es,
              'en', oin.nombre_en,
              oin.nombre)
           origen_nombre,
       oin.url
           origen_url,
       o.lugar,
       get_proxima_fecha_vigencia(o.id)
           proxima_fecha_vigencia,
       get_primera_fecha_vigencia(o.id)
           primera_fecha_vigencia,
       concat_metadata(o.id, i.codigo_iso)
           metadata,
       DECODE(i.codigo_iso,
              'es', m.titulo_publicacion_es,
              'en', m.titulo_publicacion_en,
              m.titulo_publicacion_ca)
           tituloNodo,
       mo.orden,
       get_prioridad(o.id)
           prioridad,
       ABS(get_inicio_vigencia_contenido(o.id) - SYSDATE)
           distancia_sysdate,
       oi.tipo_formato,
       concat_atributos(oi.id),
       o.visible,
       o.texto_novisible,
       concat_tags(o.id),
       m.orden,
       concat_fechas_evento(mo.id)
FROM upo_mapas m,
     upo_mapas_plantillas mp,
     upo_mapas_objetos mo,
     upo_objetos o,
     upo_objetos_idiomas oi,
     upo_idiomas i,
     upo_origenes_informacion oin,
     upo_mapas xm,
     upo_mapas_objetos xmo
WHERE m.id = mo.mapa_id
  AND m.id = mp.mapa_id(+)
  AND o.id = mo.objeto_id
  AND o.id = oi.objeto_id
  AND i.id = oi.idioma_id
  AND mo.tipo = 'LINK'
  AND o.origen_informacion_id = oin.id(+)
  AND mo.estado_moderacion = 'ACEPTADO'
  AND xm.id = xmo.mapa_id
  AND o.id = xmo.objeto_id
  AND xmo.tipo = 'NORMAL'
  AND xmo.estado_moderacion = 'ACEPTADO'
  AND xm.url_completa NOT LIKE '/paperera%';
