-- deletes
delete from uji_portal.apa_ext_personas_cuentas;
delete from uji_portal.apa_ext_aplicaciones_items;
delete from uji_portal.apa_ext_aplicaciones_perfiles;
delete from uji_portal.apa_ext_aplicaciones_extras;
delete from uji_portal.apa_ext_aplicaciones;
delete from uji_portal.apa_ext_tipos;
delete from uji_portal.apa_ext_perfiles_per;
delete from uji_portal.apa_ext_perfiles;

-- apa_ext_tipos
insert into uji_portal.apa_ext_tipos (id, nombre) values (1, 'ADMIN');
insert into uji_portal.apa_ext_tipos (id, nombre) values (2, 'USUARIO');
insert into uji_portal.apa_ext_tipos (id, nombre) values (3, 'TRADUCTOR');


-- apa_ext_perfiles
insert into uji_portal.apa_ext_perfiles (id, nombre, codigo) values (1, 'UJI',9000000);
insert into uji_portal.apa_ext_perfiles (id, nombre, codigo) values (2, 'PAS',5000004);
insert into uji_portal.apa_ext_perfiles (id, nombre, codigo) values (3, 'PDI',5000009);
insert into uji_portal.apa_ext_perfiles (id, nombre, codigo) values (4, 'SLT',0);


-- apa_ext_perfiles
insert into uji_portal.apa_ext_perfiles_per (id, perfil_id, persona_id, plaza_id) values (101, 2, 9792 , null);
insert into uji_portal.apa_ext_perfiles_per (id, perfil_id, persona_id, plaza_id) values (102, 2, 34712 , null);
insert into uji_portal.apa_ext_perfiles_per (id, perfil_id, persona_id, plaza_id) values (103, 2, 65394 , null);
insert into uji_portal.apa_ext_perfiles_per (id, perfil_id, persona_id, plaza_id) values (104, 3, 95760 , null);
insert into uji_portal.apa_ext_perfiles_per (id, perfil_id, persona_id, plaza_id) values (105, 3, 153205 , null);


-- apa_ext_personas_cuentas
insert into uji_portal.apa_ext_personas_cuentas (persona_id, cuenta) values (9792,'borillo');


-- apa_ext_aplicaciones
insert into uji_portal.apa_ext_aplicaciones (id, nombre, codigo) values (1, 'Poral', 'UPO');

-- apa_ext_aplicaciones_perfiles
insert into uji_portal.apa_ext_aplicaciones_perfiles (id, aplicacion_id, perfil_id, tipo_id) values (201, 1, 2, 2);
insert into uji_portal.apa_ext_aplicaciones_perfiles (id, aplicacion_id, perfil_id, tipo_id) values (202, 1, 3, 2);


-- apa_ext_aplicaciones_extra
insert into uji_portal.apa_ext_aplicaciones_extras (id, aplicacion_id, persona_id, plaza_id, tipo_id) values (301, 1, 65394, null, 1);


-- apa_ext_aplicaciones_items
insert into uji_portal.apa_ext_aplicaciones_items (id, aplicacion_id, nombre, jsfile) values (1, 1, 'Gestió de guies docents', 'administracion.js');
insert into uji_portal.apa_ext_aplicaciones_items (id, aplicacion_id, nombre, jsfile) values (2, 1, 'Asignació de professors a assignatures', 'asignaProfesor.js');
insert into uji_portal.apa_ext_aplicaciones_items (id, aplicacion_id, nombre, jsfile) values (3, 1, 'Gestió de cursos', 'cursos.js');
insert into uji_portal.apa_ext_aplicaciones_items (id, aplicacion_id, nombre, jsfile) values (4, 1, 'Gestió administradors extra', 'roles.js');
