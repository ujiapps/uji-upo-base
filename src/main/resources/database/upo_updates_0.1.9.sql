ALTER TABLE upo_mapas_objetos ADD (orden NUMBER);

UPDATE upo_mapas_objetos mo
SET mo.orden = (SELECT o.orden
                  FROM upo_objetos o
                 WHERE o.id = mo.objeto_id);

ALTER TABLE upo_objetos DROP COLUMN orden;

ALTER TABLE upo_objetos_log DROP COLUMN orden;