alter table upo_mapas
    add (titulo_miga_ca varchar2(4000),
         titulo_miga_es varchar2(4000),
         titulo_miga_en varchar2(4000),
         mostrar_como_miga number(1) default 0 not null);

CREATE OR REPLACE function UJI_PORTAL.dame_migas(p_mapa_id number, p_idioma_iso varchar2)
    return clob

    is
    vRdo clob;
    vAux varchar2(4000);
    vTitulo varchar2(4000);

    cursor mapas is
        select decode(p_idioma_iso, 'ca', titulo_miga_ca, 'es', titulo_miga_es, 'en', titulo_miga_en) titulo_miga, nvl(enlace_destino, url_completa) url, id
        from upo_mapas m
        where mostrar_como_miga = 1
          and id != p_mapa_id
        start with id = p_mapa_id
        connect by prior m.mapa_id = m.id
        order by url_completa;

begin

    dbms_lob.createtemporary(vRdo, true);

    for m in mapas loop

            if dbms_lob.getlength(vRdo) > 0 then
                dbms_lob.writeappend(vRdo, 3, '~~~');
            end if;

            vTitulo := m.titulo_miga;

            if m.titulo_miga is null then
                vTitulo := dame_titulo_pagina(m.id, p_idioma_iso);
            end if;

            vAux := vTitulo || '@@@' || m.url;

            dbms_lob.writeappend(vRdo, length(vAux), vAux);

        end loop;

    return vRdo;

exception
    when others then
        return null;

end;
/


CREATE OR REPLACE function UJI_PORTAL.dame_titulo_pagina(p_mapa_id number, p_idioma_iso varchar2)
    return varchar2 is

    v_titulo_defecto varchar2(4000) := 'Universitat Jaume I';
    v_titulo varchar2(4000);
    v_cuenta number;

    cursor contenidos is
        select titulo_largo
        from upo_mapas m,
             upo_mapas_objetos mo,
             upo_objetos o,
             upo_objetos_idiomas oi,
             upo_idiomas i
        where m.id = mo.mapa_id
          and o.id = mo.objeto_id
          and oi.objeto_id = o.id
          and html = 'S'
          and m.id = p_mapa_id
          and i.ID = oi.idioma_id
          and lower(i.codigo_iso) = p_idioma_iso;

begin

    select decode(p_idioma_iso, 'ca', titulo_publicacion_ca, 'es', titulo_publicacion_es, 'en', titulo_publicacion_en) titulo
    into v_titulo
    from upo_mapas m
    where id = p_mapa_id;

    if v_titulo is not null then
        return v_titulo;
    end if;

    v_cuenta := 0;
    for c in contenidos loop
            v_cuenta := v_cuenta + 1;
            v_titulo := c.titulo_largo;
        end loop;

    if v_cuenta != 1 then
        return v_titulo_defecto;
    end if;

    if v_titulo is not null then
        return v_titulo;
    end if;

    return v_titulo_defecto;

exception
    when others then
        return v_titulo_defecto;

end;