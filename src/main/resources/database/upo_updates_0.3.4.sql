ALTER TABLE UPO_OBJETOS_IDIOMAS ADD (TIPO_FORMATO VARCHAR2(200));

UPDATE UPO_OBJETOS_IDIOMAS SET TIPO_FORMATO = DECODE(HTML, 'S', 'PAGINA', 'BINARIO');

ALTER TABLE UJI_PORTAL.UPO_OBJETOS_IDIOMAS MODIFY(TIPO_FORMATO  NOT NULL);

create table UPO_OBJETOS_IDIOMAS_ATRIBUTOS (ID NUMBER NOT NULL PRIMARY KEY, OBJETO_IDIOMA_ID NUMBER NOT NULL, CLAVE VARCHAR2(4000) NOT NULL, VALOR VARCHAR2(4000) NOT NULL);

ALTER TABLE UPO_OBJETOS_IDIOMAS_ATRIBUTOS ADD CONSTRAINT UPO_OBJ_IDI_ATRIB_FK FOREIGN KEY (OBJETO_IDIOMA_ID) REFERENCES UPO_OBJETOS_IDIOMAS (ID);

----------------------------------------------------

ALTER TABLE UPO_OBJETOS_IDIOMAS_LOG ADD (TIPO_FORMATO VARCHAR2(200));

UPDATE UPO_OBJETOS_IDIOMAS_LOG SET TIPO_FORMATO = DECODE(HTML, 'S', 'PAGINA', 'BINARIO');

ALTER TABLE UJI_PORTAL.UPO_OBJETOS_IDIOMAS_LOG MODIFY(TIPO_FORMATO  NOT NULL);

CREATE OR REPLACE function UJI_PORTAL.concat_atributos(v_objeto_idioma_id number)
return clob
is
   vRdo clob;
begin

  select replace(replace(wm_concat(replace(clave, ',', '###') || '@@@' || replace(valor, ',', '###')), ',', '~~~'), '###', ',')
    into vRdo
    from upo_objetos_idiomas_atributos oia
   where oia.objeto_idioma_id = v_objeto_idioma_id;

 return vRdo;

end;
/

create or replace force view UJI_PORTAL.UPO_VW_PUBLICACION
(
   CONTENIDO_ID
 , CONTENIDO_IDIOMA_ID
 , URL_NODO
 , URL_NODO_ORIGINAL
 , URL_CONTENIDO
 , URL_COMPLETA
 , URL_COMPLETA_ORIGINAL
 , URL_NUM_NIVELES
 , URL_DESTINO
 , NIVEL
 , IDIOMA
 , TITULO
 , SUBTITULO
 , TITULO_LARGO
 , RESUMEN
 , MIME_TYPE
 , ES_HTML
 , CONTENIDO
 , FECHA_MODIFICACION
 , NODO_MAPA_ORDEN
 , CONTENIDO_ORDEN
 , TIPO_CONTENIDO
 , AUTOR
 , ORIGEN_NOMBRE
 , ORIGEN_URL
 , LUGAR
 , PROXIMA_FECHA_VIGENCIA
 , PRIMERA_FECHA_VIGENCIA
 , METADATA
 , TITULO_NODO
 , ORDEN
 , PRIORIDAD
 , DISTANCIA_SYSDATE
 , TIPO_FORMATO
 , ATRIBUTOS
) as
   select o.id contenido_id
        , oi.id contenido_idioma_id
        , m.url_completa url_nodo
        , m.url_completa url_nodo_original
        , o.url_path url_contenido
        , m.url_completa || o.url_path url_completa
        , m.url_completa || o.url_path url_completa_original
        , times_value_in_string(m.url_completa, '/') niveles
        , oi.enlace_destino
        , nvl(mp.nivel, 1)
        , i.codigo_iso idioma
        , oi.titulo
        , oi.subtitulo
        , oi.titulo_largo
        , oi.resumen
        , oi.mime_type
        , oi.html es_html
        , decode(oi.html, 'S', oi.contenido, null) contenido
        , oi.fecha_modificacion
        , m.orden nodo_mapa_orden
        , mo.orden contenido_orden
        , 'NORMAL' tipo_contenido
        , (select nvl(otros_autores, (select upper(nombre || ' ' || apellido1 || ' ' || apellido2)
                                        from upo_ext_personas
                                       where id = per_id_responsable))
             from upo_objetos
            where id = o.id)
             autor
        , oin.nombre origen_nombre
        , oin.url origen_url
        , o.lugar
        , get_proxima_fecha_vigencia(o.id) proxima_fecha_vigencia
        , get_primera_fecha_vigencia(o.id) primera_fecha_vigencia
        , concat_metadata(o.id, i.codigo_iso) metadata
        , decode(i.codigo_iso, 'es', m.titulo_publicacion_es, 'en', m.titulo_publicacion_en, m.titulo_publicacion_ca) tituloNodo
        , mo.orden
        , get_prioridad(o.id) prioridad
        , abs(get_inicio_vigencia_contenido(o.id) - sysdate) distancia_sysdate
        , oi.tipo_formato
        , concat_atributos(oi.id)
     from upo_mapas m
        , upo_mapas_plantillas mp
        , upo_mapas_objetos mo
        , upo_objetos o
        , upo_objetos_idiomas oi
        , upo_idiomas i
        , upo_origenes_informacion oin
    where m.id = mo.mapa_id
      and m.id = mp.mapa_id(+)
      and o.id = mo.objeto_id
      and o.id = oi.objeto_id
      and i.id = oi.idioma_id
      and mo.tipo = 'NORMAL'
      and o.origen_informacion_id = oin.id(+)
      and o.visible = 'S'
   union all
   select o.id contenido_id
        , oi.id contenido_idioma_id
        , m.url_completa url_nodo
        , (select xm.url_completa
             from upo_mapas xm, upo_mapas_objetos xmo
            where xm.id = xmo.mapa_id
              and o.id = xmo.objeto_id
              and xmo.tipo = 'NORMAL')
             url_nodo_original
        , o.url_path url_contenido
        , m.url_completa || o.url_path url_completa
        , (select xm.url_completa || o.url_path
             from upo_mapas xm, upo_mapas_objetos xmo
            where xm.id = xmo.mapa_id
              and o.id = xmo.objeto_id
              and xmo.tipo = 'NORMAL')
             url_completa_original
        , times_value_in_string(m.url_completa, '/') niveles
        , oi.enlace_destino
        , nvl(mp.nivel, 1)
        , i.codigo_iso idioma
        , oi.titulo
        , oi.subtitulo
        , oi.titulo_largo
        , oi.resumen
        , oi.mime_type
        , oi.html es_html
        , decode(oi.html, 'S', oi.contenido, null) contenido
        , oi.fecha_modificacion
        , m.orden nodo_mapa_orden
        , mo.orden contenido_orden
        , 'LINK' tipo_contenido
        , (select nvl(otros_autores, (select upper(nombre || ' ' || apellido1 || ' ' || apellido2)
                                        from upo_ext_personas
                                       where id = per_id_responsable))
             from upo_objetos
            where id = o.id)
             autor
        , oin.nombre origen_nombre
        , oin.url origen_url
        , o.lugar
        , get_proxima_fecha_vigencia(o.id) proxima_fecha_vigencia
        , get_primera_fecha_vigencia(o.id) primera_fecha_vigencia
        , concat_metadata(o.id, i.codigo_iso) metadata
        , decode(i.codigo_iso, 'es', m.titulo_publicacion_es, 'en', m.titulo_publicacion_en, m.titulo_publicacion_ca) tituloNodo
        , mo.orden
        , get_prioridad(o.id) prioridad
        , abs(get_inicio_vigencia_contenido(o.id) - sysdate) distancia_sysdate
        , oi.tipo_formato
        , concat_atributos(oi.id)
     from upo_mapas m
        , upo_mapas_plantillas mp
        , upo_mapas_objetos mo
        , upo_objetos o
        , upo_objetos_idiomas oi
        , upo_idiomas i
        , upo_origenes_informacion oin
    where m.id = mo.mapa_id
      and m.id = mp.mapa_id(+)
      and o.id = mo.objeto_id
      and o.id = oi.objeto_id
      and i.id = oi.idioma_id
      and mo.tipo = 'LINK'
      and o.origen_informacion_id = oin.id(+)
      and o.visible = 'S'
      and mo.estado_moderacion = 'ACEPTADO';

