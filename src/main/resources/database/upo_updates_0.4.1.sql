ALTER TABLE upo_objetos_idiomas
  ADD (id_descarga VARCHAR2(1000));

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_PUBLICACION_BINARIOS(ID, URL_NODO, URL_PATH, CONTENIDO, IDIOMA, MIME_TYPE, ID_DESCARGA) AS
  SELECT
    mo.id,
    m.url_completa url_modo,
    o.url_path,
    oi.contenido,
    i.codigo_iso,
    oi.mime_type,
    oi.id_descarga
  FROM upo_mapas m, upo_mapas_objetos mo, upo_objetos o, upo_objetos_idiomas oi, upo_idiomas i
  WHERE m.id = mo.mapa_id
        AND o.id = mo.objeto_id
        AND o.id = oi.objeto_id
        AND i.id = oi.idioma_id
        AND ((mo.tipo = 'NORMAL')
             OR (mo.tipo = 'LINK'
                 AND mo.estado_moderacion = 'ACEPTADO'))
        AND nvl(oi.html, 'N') = 'N'
        AND o.visible = 'S';

ALTER TABLE upo_objetos
  ADD (ajustar_a_fechas_vigencia NUMBER DEFAULT 0 NOT NULL );
ALTER TABLE upo_objetos_log
  ADD (ajustar_a_fechas_vigencia NUMBER DEFAULT 0 NOT NULL )


CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_SYNC_CONTENIDOS(OBJETO_ID, URL_NODO, ORDEN, PRIORIDAD, DISTANCIA_SYSDATE, FECHA_CREACION) AS
  SELECT
    o.id,
    m.url_completa,
    mo.orden,
    get_prioridad(o.id)                                prioridad,
    abs(get_inicio_vigencia_contenido(o.id) - sysdate) distancia_sysdate,
    o.fecha_creacion
  FROM upo_mapas m, upo_mapas_objetos mo, upo_objetos o
  WHERE m.id = mo.mapa_id
        AND o.id = mo.objeto_id -- cualquier contenido visible
        AND ((mo.tipo = 'NORMAL')
             OR (mo.tipo = 'LINK'
                 AND mo.estado_moderacion = 'ACEPTADO'))
        AND o.visible = 'S'
        AND (-- no hay vigencia definida
          NOT exists(SELECT id
                     FROM upo_vigencias_objetos vo
                     WHERE vo.objeto_id = o.id)
          OR -- vigencia de 72 horas y dentro de rango de fechas
          exists(SELECT id
                 FROM upo_vigencias_objetos vo
                 WHERE vo.objeto_id = o.id
                       AND (
                         ((to_date(
                               to_char(fecha, 'dd/mm/yyyy') || ' ' || to_char(nvl(hora_inicio, sysdate), 'hh24:mi:ss'),
                               'dd/mm/yyyy hh24:mi:ss') BETWEEN sysdate AND sysdate + 3
                           OR
                           to_date(to_char(fecha, 'dd/mm/yyyy') || ' ' || to_char(nvl(hora_fin, sysdate), 'hh24:mi:ss'),
                                   'dd/mm/yyyy hh24:mi:ss') BETWEEN sysdate AND sysdate + 3
                          )
                          AND o.ajustar_a_fechas_vigencia = 0
                         )
                         OR
                         ((to_date(
                               to_char(fecha, 'dd/mm/yyyy') || ' ' || to_char(nvl(hora_inicio, sysdate), 'hh24:mi:ss'),
                               'dd/mm/yyyy hh24:mi:ss') <= sysdate
                           AND
                           to_date(to_char(fecha, 'dd/mm/yyyy') || ' ' || to_char(nvl(hora_fin, sysdate), 'hh24:mi:ss'),
                                   'dd/mm/yyyy hh24:mi:ss') >= sysdate
                          )
                          AND o.ajustar_a_fechas_vigencia = 1
                         )
                       )

          )
        )
        -- Obligatorio en CA y ES
        AND ((exists(SELECT i.codigo_iso
                     FROM upo_objetos_idiomas oi, upo_idiomas i
                     WHERE i.id = oi.idioma_id
                           AND lower(i.codigo_iso) = 'ca'
                           AND o.id = oi.objeto_id
                           AND nvl(oi.html, 'N') = 'S')
              AND exists(SELECT i.codigo_iso
                         FROM upo_objetos_idiomas oi, upo_idiomas i
                         WHERE i.id = oi.idioma_id
                               AND lower(i.codigo_iso) = 'es'
                               AND o.id = oi.objeto_id
                               AND nvl(oi.html, 'N') = 'S'))
             OR (has_tag(o.id, 'megabanner') = 'S'));
