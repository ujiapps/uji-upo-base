CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_PUBLICACION
(
    CONTENIDO_ID,
    CONTENIDO_IDIOMA_ID,
    URL_NODO,
    URL_NODO_ORIGINAL,
    URL_PATH_NODO,
    URL_CONTENIDO,
    URL_COMPLETA,
    URL_COMPLETA_ORIGINAL,
    URL_NUM_NIVELES,
    URL_DESTINO,
    NIVEL,
    IDIOMA,
    TITULO,
    SUBTITULO,
    TITULO_LARGO,
    RESUMEN,
    MIME_TYPE,
    ES_HTML,
    CONTENIDO,
    FECHA_MODIFICACION,
    FECHA_CREACION,
    NODO_MAPA_ORDEN,
    NODO_MAPA_PADRE_ORDEN,
    CONTENIDO_ORDEN,
    TIPO_CONTENIDO,
    AUTOR,
    ORIGEN_NOMBRE,
    ORIGEN_URL,
    LUGAR,
    PROXIMA_FECHA_VIGENCIA,
    PRIMERA_FECHA_VIGENCIA,
    METADATA,
    TITULO_NODO,
    ORDEN,
    PRIORIDAD,
    DISTANCIA_SYSDATE,
    TIPO_FORMATO,
    ATRIBUTOS,
    VISIBLE,
    TEXTO_NOVISIBLE,
    TAGS,
    ORDEN_MAPA,
    FECHAS_EVENTO,
    PUBLICABLE
)
BEQUEATH DEFINER
AS
    SELECT o.id
               contenido_id,
           oi.id
               contenido_idioma_id,
           m.url_completa
               url_nodo,
           m.url_completa
               url_nodo_original,
           m.url_path
               url_path_nodo,
           o.url_path
               url_contenido,
           m.url_completa || o.url_path
               url_completa,
           m.url_completa || o.url_path
               url_completa_original,
           url_num_niveles
               niveles,
           oi.enlace_destino,
           NVL (mp.nivel, 1),
           i.codigo_iso
               idioma,
           oi.titulo,
           oi.subtitulo,
           oi.titulo_largo,
           oi.resumen,
           oi.mime_type,
           oi.html
               es_html,
           DECODE (oi.html, 'S', oi.contenido, NULL)
               contenido,
           oi.fecha_modificacion,
           o.fecha_creacion,
           m.orden
               nodo_mapa_orden,
           dame_orden_padre (m.mapa_id)
               orden_padre,
           mo.orden
               contenido_orden,
           'NORMAL'
               tipo_contenido,
           dame_autores (o.id)
               autor,
           DECODE (i.codigo_iso,
                   'es', oin.nombre_es,
                   'en', oin.nombre_en,
                   oin.nombre)
               origen_nombre,
           oin.url
               origen_url,
           o.lugar,
           get_proxima_fecha_vigencia (o.id)
               proxima_fecha_vigencia,
           get_primera_fecha_vigencia (o.id)
               primera_fecha_vigencia,
           concat_metadata (o.id, i.codigo_iso)
               metadata,
           DECODE (i.codigo_iso,
                   'es', m.titulo_publicacion_es,
                   'en', m.titulo_publicacion_en,
                   m.titulo_publicacion_ca)
               tituloNodo,
           mo.orden,
           get_prioridad (o.id)
               prioridad,
           ABS (get_inicio_vigencia_contenido (o.id) - SYSDATE)
               distancia_sysdate,
           oi.tipo_formato,
           concat_atributos (oi.id),
           o.visible,
           o.texto_novisible,
           concat_tags (o.id),
           m.orden,
           concat_fechas_evento (mo.id),
           publicable
      FROM upo_mapas                 m,
           upo_mapas_plantillas      mp,
           upo_mapas_objetos         mo,
           upo_objetos               o,
           upo_objetos_idiomas       oi,
           upo_idiomas               i,
           upo_origenes_informacion  oin
     WHERE     m.id = mo.mapa_id
           AND m.id = mp.mapa_id(+)
           AND o.id = mo.objeto_id
           AND o.id = oi.objeto_id
           AND i.id = oi.idioma_id
           AND mo.tipo = 'NORMAL'
           AND mo.estado_moderacion = 'ACEPTADO'
           AND o.origen_informacion_id = oin.id(+)
           AND m.url_completa NOT LIKE '/paperera/%'
    UNION ALL
    SELECT o.id
               contenido_id,
           oi.id
               contenido_idioma_id,
           m.url_completa
               url_nodo,
           xm.url_completa
               url_nodo_original,
           m.url_path
               url_path_nodo,
           o.url_path
               url_contenido,
           m.url_completa || o.url_path
               url_completa,
           xm.url_completa || o.url_path
               url_completa_original,
           m.url_num_niveles
               niveles,
           oi.enlace_destino,
           NVL (mp.nivel, 1),
           i.codigo_iso
               idioma,
           oi.titulo,
           oi.subtitulo,
           oi.titulo_largo,
           oi.resumen,
           oi.mime_type,
           oi.html
               es_html,
           DECODE (oi.html, 'S', oi.contenido, NULL)
               contenido,
           oi.fecha_modificacion,
           o.fecha_creacion,
           m.orden
               nodo_mapa_orden,
           dame_orden_padre (m.mapa_id)
               orden_padre,
           mo.orden
               contenido_orden,
           'LINK'
               tipo_contenido,
           dame_autores (o.id)
               autor,
           DECODE (i.codigo_iso,
                   'es', oin.nombre_es,
                   'en', oin.nombre_en,
                   oin.nombre)
               origen_nombre,
           oin.url
               origen_url,
           o.lugar,
           get_proxima_fecha_vigencia (o.id)
               proxima_fecha_vigencia,
           get_primera_fecha_vigencia (o.id)
               primera_fecha_vigencia,
           concat_metadata (o.id, i.codigo_iso)
               metadata,
           DECODE (i.codigo_iso,
                   'es', m.titulo_publicacion_es,
                   'en', m.titulo_publicacion_en,
                   m.titulo_publicacion_ca)
               tituloNodo,
           mo.orden,
           get_prioridad (o.id)
               prioridad,
           ABS (get_inicio_vigencia_contenido (o.id) - SYSDATE)
               distancia_sysdate,
           oi.tipo_formato,
           concat_atributos (oi.id),
           o.visible,
           o.texto_novisible,
           concat_tags (o.id),
           m.orden,
           concat_fechas_evento (mo.id),
           publicable
      FROM upo_mapas                 m,
           upo_mapas_plantillas      mp,
           upo_mapas_objetos         mo,
           upo_objetos               o,
           upo_objetos_idiomas       oi,
           upo_idiomas               i,
           upo_origenes_informacion  oin,
           upo_mapas                 xm,
           upo_mapas_objetos         xmo
     WHERE     m.id = mo.mapa_id
           AND m.id = mp.mapa_id(+)
           AND o.id = mo.objeto_id
           AND o.id = oi.objeto_id
           AND i.id = oi.idioma_id
           AND mo.tipo = 'LINK'
           AND o.origen_informacion_id = oin.id(+)
           AND mo.estado_moderacion = 'ACEPTADO'
           AND xm.id = xmo.mapa_id
           AND o.id = xmo.objeto_id
           AND xmo.tipo = 'NORMAL'
           AND xmo.estado_moderacion = 'ACEPTADO'
           AND xm.url_completa NOT LIKE '/paperera%';
