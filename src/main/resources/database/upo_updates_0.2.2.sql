alter table upo_grupos rename column nombre to nombre_ca;
alter table upo_grupos add (nombre_es varchar2(4000), nombre_en varchar2(4000));
update upo_grupos set nombre_es = nombre_ca, nombre_en = nombre_ca;
alter table upo_grupos modify nombre_es not null;
alter table upo_grupos modify nombre_en not null;

alter table upo_items rename column nombre to nombre_ca;
alter table upo_items add (nombre_es varchar2(4000), nombre_en varchar2(4000));
update upo_items set nombre_es = nombre_ca, nombre_en = nombre_ca;
alter table upo_items modify nombre_es not null;
alter table upo_items modify nombre_en not null;

alter table upo_objetos add (lugar varchar2(4000));
alter table upo_objetos_log add(lugar varchar2(4000));

alter table upo_menus_grupos add (orden number default '1' not null);
alter table upo_grupos_items add (orden number default '1' not null);

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_PUBLICACION
(
    CONTENIDO_ID,
    CONTENIDO_IDIOMA_ID,
    URL_NODO,
    URL_NODO_ORIGINAL,
    URL_CONTENIDO,
    URL_COMPLETA,
    URL_COMPLETA_ORIGINAL,
    URL_NUM_NIVELES,
    URL_DESTINO,
    NIVEL,
    IDIOMA,
    TITULO,
    SUBTITULO,
    TITULO_LARGO,
    RESUMEN,
    MIME_TYPE,
    ES_HTML,
    CONTENIDO,
    FECHA_MODIFICACION,
    NODO_MAPA_ORDEN,
    CONTENIDO_ORDEN,
    TIPO_CONTENIDO,
    AUTOR,
    ORIGEN_NOMBRE,
    ORIGEN_URL,
    LUGAR,
    PROXIMA_FECHA_VIGENCIA
)
AS
  SELECT   o.id contenido_id,
           oi.id contenido_idioma_id,
           m.url_completa url_nodo,
           m.url_completa url_nodo_original,
           o.url_path url_contenido,
           m.url_completa || o.url_path url_completa,
           m.url_completa || o.url_path url_completa_original,
           times_value_in_string (m.url_completa, '/') niveles,
    oi.enlace_destino,
    NVL (mp.nivel, 1),
           i.codigo_iso idioma,
    oi.titulo,
    oi.subtitulo,
    oi.titulo_largo,
    oi.resumen,
    oi.mime_type,
           oi.html es_html,
           decode(oi.html, 'S', oi.contenido, null) contenido,
    oi.fecha_modificacion,
           m.orden nodo_mapa_orden,
           mo.orden contenido_orden,
           'NORMAL' tipo_contenido,
           (SELECT   NVL (
               otros_autores,
               (SELECT   UPPER(   nombre
                                  || ' '
                                  || apellido1
                                  || ' '
                                  || apellido2)
                FROM   upo_ext_personas
                WHERE   id = per_id_responsable)
           )
            FROM   upo_objetos
            WHERE   id = o.id)
             autor,
           oin.nombre origen_nombre,
           oin.url origen_url,
    o.lugar,
           get_proxima_fecha_vigencia (o.id) proxima_fecha_vigencia
  FROM   upo_mapas m,
    upo_mapas_plantillas mp,
    upo_mapas_objetos mo,
    upo_objetos o,
    upo_objetos_idiomas oi,
    upo_idiomas i,
    upo_origenes_informacion oin
  WHERE       m.id = mo.mapa_id
              AND m.id = mp.mapa_id(+)
              AND o.id = mo.objeto_id
              AND o.id = oi.objeto_id
              AND i.id = oi.idioma_id
              AND mo.tipo = 'NORMAL'
              AND o.origen_informacion_id = oin.id(+)
  UNION ALL
  SELECT   o.id contenido_id,
           oi.id contenido_idioma_id,
           m.url_completa url_nodo,
           (SELECT   xm.url_completa
            FROM   upo_mapas xm, upo_mapas_objetos xmo
            WHERE       xm.id = xmo.mapa_id
                        AND o.id = xmo.objeto_id
                        AND xmo.tipo = 'NORMAL')
             url_nodo_original,
           o.url_path url_contenido,
           m.url_completa || o.url_path url_completa,
           (SELECT   xm.url_completa || o.url_path
            FROM   upo_mapas xm, upo_mapas_objetos xmo
            WHERE       xm.id = xmo.mapa_id
                        AND o.id = xmo.objeto_id
                        AND xmo.tipo = 'NORMAL')
             url_completa_original,
           times_value_in_string (m.url_completa, '/') niveles,
    oi.enlace_destino,
    NVL (mp.nivel, 1),
           i.codigo_iso idioma,
    oi.titulo,
    oi.subtitulo,
    oi.titulo_largo,
    oi.resumen,
    oi.mime_type,
           oi.html es_html,
           decode(oi.html, 'S', oi.contenido, null) contenido,
    oi.fecha_modificacion,
           m.orden nodo_mapa_orden,
           mo.orden contenido_orden,
           'LINK' tipo_contenido,
           (SELECT   NVL (
               otros_autores,
               (SELECT   UPPER(   nombre
                                  || ' '
                                  || apellido1
                                  || ' '
                                  || apellido2)
                FROM   upo_ext_personas
                WHERE   id = per_id_responsable)
           )
            FROM   upo_objetos
            WHERE   id = o.id)
             autor,
           oin.nombre origen_nombre,
           oin.url origen_url,
    o.lugar,
           get_proxima_fecha_vigencia (o.id) proxima_fecha_vigencia
  FROM   upo_mapas m,
    upo_mapas_plantillas mp,
    upo_mapas_objetos mo,
    upo_objetos o,
    upo_objetos_idiomas oi,
    upo_idiomas i,
    upo_origenes_informacion oin
  WHERE       m.id = mo.mapa_id
              AND m.id = mp.mapa_id(+)
              AND o.id = mo.objeto_id
              AND o.id = oi.objeto_id
              AND i.id = oi.idioma_id
              AND mo.tipo = 'LINK'
              AND o.origen_informacion_id = oin.id(+);

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_PUBLICACION_BINARIOS
(
    URL_COMPLETA,
    IDIOMA,
    MIME_TYPE,
    CONTENIDO,
    url_nodo
)
AS
  SELECT   m.url_completa || o.url_path url_completa,
           i.codigo_iso idioma,
    oi.mime_type,
    oi.contenido,
           m.url_completa url_nodo
  FROM   upo_mapas m,
    upo_mapas_plantillas mp,
    upo_mapas_objetos mo,
    upo_objetos o,
    upo_objetos_idiomas oi,
    upo_idiomas i,
    upo_origenes_informacion oin
  WHERE       m.id = mo.mapa_id
              AND m.id = mp.mapa_id(+)
              AND o.id = mo.objeto_id
              AND o.id = oi.objeto_id
              AND i.id = oi.idioma_id
              AND mo.tipo in ('NORMAL', 'LINK')
              AND nvl(oi.html, 'N') = 'N'
              AND o.origen_informacion_id = oin.id(+);

CREATE OR REPLACE function UJI_PORTAL.get_proxima_fecha_vigencia(p_objeto_id number) return date is
begin
declare
  vFecha date;

begin

  select nvl(min(to_date(to_char(fecha, 'dd/mm/rrrr') || to_char(hora_inicio, 'hh24:mi'), 'dd/mm/rrrrhh24:mi')), trunc(sysdate))
    into vFecha
    from upo_vigencias_objetos
   where objeto_id = p_objeto_id
     and ((hora_inicio is not null
           and to_date(to_char(fecha, 'dd/mm/rrrr') || to_char(hora_inicio, 'hh24:mi'), 'dd/mm/rrrrhh24:mi') >= sysdate
          )
          or
          (hora_inicio is null
           and trunc(fecha) >= trunc(sysdate)
          )
         );

  return vFecha;

end;
end;
/

alter table upo_mapas add (sincro_desde_mapa varchar2(4000), periodicidad_sincro number)