ALTER TABLE UJI_PORTAL.UPO_MAPAS
  ADD (paginado NUMBER(1) DEFAULT 0 NOT NULL);

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_BUSQUEDA
(
    CONTENIDO_ID,
    CONTENIDO_IDIOMA_ID,
    MAPA_URL_COMPLETA,
    IDIOMA_CODIGO_ISO,
    TITULO_SIN_ACENTOS,
    TITULO_LARGO_SIN_ACENTOS,
    RESUMEN_SIN_ACENTOS,
    CONTENIDO,
    SUBTITULO_SIN_ACENTOS,
    ORDEN,
    PRIORIDAD,
    DISTANCIA_SYSDATE,
    FECHA_VIGENCIA,
    ORIGEN_INFORMACION_ID,
    ES_HTML,
    FECHA_CREACION,
    ORDEN_MAPA
)
AS
  SELECT
    o.id                                     contenido_id,
    oi.id                                    contenido_idioma_id,
    m.url_completa                           mapa_url_completa,
    LOWER(i.codigo_iso)                      idioma_codigo_iso,
    quita_acentos(oi.titulo)                 titulo_sin_acentos,
    quita_acentos(oi.titulo_largo)           titulo_largo_sin_acentos,
    quita_acentos(oi.resumen)                resumen_sin_acentos,
    /*quita_acentos_clob(blob_to_clob(decode(oi.html, 'S', oi.contenido, null))) contenido_sin_acentos,*/
    DECODE(oi.html, 'S', oi.contenido, NULL) contenido,
    quita_acentos(oi.subtitulo)              subtitulo_sin_acentos,
    mo.orden,
    get_prioridad(o.id)                      prioridad,
    ABS(get_inicio_vigencia_contenido(o.id) - SYSDATE)
                                             distancia_sysdate,
    TRUNC(vo.fecha)                          fecha,
    origen_informacion_id,
    oi.html,
    o.fecha_creacion,
    m.orden
  FROM upo_mapas_objetos mo,
    upo_objetos o,
    upo_mapas m,
    upo_objetos_idiomas oi,
    upo_idiomas i,
    upo_vigencias_objetos vo
  WHERE mo.mapa_id = m.id
        AND mo.objeto_id = o.id
        AND oi.objeto_id = o.id
        AND i.id = oi.idioma_id
        AND ((mo.tipo = 'NORMAL')
             OR (mo.tipo = 'LINK' AND mo.estado_moderacion = 'ACEPTADO'))
        AND o.visible = 'S'
        AND oi.html = 'S'
        AND vo.objeto_id (+) = o.id;

CREATE OR REPLACE FUNCTION UJI_PORTAL.dame_autores(p_id IN NUMBER)
  RETURN VARCHAR2 IS

  vAutores VARCHAR2(4000);

  BEGIN
    SELECT nvl(otros_autores, (SELECT UPPER(nombre || ' ' || apellido1 || ' ' || apellido2)
                               FROM upo_ext_personas
                               WHERE id = per_id_responsable))
    INTO vAutores
    FROM upo_objetos
    WHERE id = p_id;

    RETURN vAutores;
    EXCEPTION
    WHEN OTHERS THEN RETURN '';
  END;
/

CREATE OR REPLACE FUNCTION UJI_PORTAL.dame_orden_padre(p_id IN NUMBER)
  RETURN NUMBER IS
  vOrden NUMBER;
  BEGIN
    SELECT mpadre.orden
    INTO vOrden
    FROM upo_mapas mpadre
    WHERE mpadre.id = p_id;

    RETURN vOrden;
    EXCEPTION
    WHEN OTHERS THEN RETURN -1;
  END;
/

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_PUBLICACION
(
    CONTENIDO_ID,
    CONTENIDO_IDIOMA_ID,
    URL_NODO,
    URL_NODO_ORIGINAL,
    URL_CONTENIDO,
    URL_COMPLETA,
    URL_COMPLETA_ORIGINAL,
    URL_NUM_NIVELES,
    URL_DESTINO,
    NIVEL,
    IDIOMA,
    TITULO,
    SUBTITULO,
    TITULO_LARGO,
    RESUMEN,
    MIME_TYPE,
    ES_HTML,
    CONTENIDO,
    FECHA_MODIFICACION,
    FECHA_CREACION,
    NODO_MAPA_ORDEN,
    NODO_MAPA_PADRE_ORDEN,
    CONTENIDO_ORDEN,
    TIPO_CONTENIDO,
    AUTOR,
    ORIGEN_NOMBRE,
    ORIGEN_URL,
    LUGAR,
    PROXIMA_FECHA_VIGENCIA,
    PRIMERA_FECHA_VIGENCIA,
    METADATA,
    TITULO_NODO,
    ORDEN,
    PRIORIDAD,
    DISTANCIA_SYSDATE,
    TIPO_FORMATO,
    ATRIBUTOS,
    VISIBLE,
    TEXTO_NOVISIBLE,
    TAGS,
    ORDEN_MAPA
)
AS
  SELECT
    o.id                                     contenido_id,
    oi.id                                    contenido_idioma_id,
    m.url_completa                           url_nodo,
    m.url_completa                           url_nodo_original,
    o.url_path                               url_contenido,
    m.url_completa || o.url_path             url_completa,
    m.url_completa || o.url_path             url_completa_original,
    url_num_niveles                          niveles,
    oi.enlace_destino,
    NVL(mp.nivel, 1),
    i.codigo_iso                             idioma,
    oi.titulo,
    oi.subtitulo,
    oi.titulo_largo,
    oi.resumen,
    oi.mime_type,
    oi.html                                  es_html,
    DECODE(oi.html, 'S', oi.contenido, NULL) contenido,
    oi.fecha_modificacion,
    o.fecha_creacion,
    m.orden                                  nodo_mapa_orden,
    dame_orden_padre(m.mapa_id)              orden_padre,
    mo.orden                                 contenido_orden,
    'NORMAL'                                 tipo_contenido,
    dame_autores(o.id)                       autor,
    DECODE(i.codigo_iso,
           'es', oin.nombre_es,
           'en', oin.nombre_en,
           oin.nombre)
                                             origen_nombre,
    oin.url                                  origen_url,
    o.lugar,
    get_proxima_fecha_vigencia(o.id)         proxima_fecha_vigencia,
    get_primera_fecha_vigencia(o.id)         primera_fecha_vigencia,
    concat_metadata(o.id, i.codigo_iso)      metadata,
    DECODE(i.codigo_iso,
           'es', m.titulo_publicacion_es,
           'en', m.titulo_publicacion_en,
           m.titulo_publicacion_ca)
                                             tituloNodo,
    mo.orden,
    get_prioridad(o.id)                      prioridad,
    ABS(get_inicio_vigencia_contenido(o.id) - SYSDATE)
                                             distancia_sysdate,
    oi.tipo_formato,
    concat_atributos(oi.id),
    o.visible,
    o.texto_novisible,
    concat_tags(o.id),
    m.orden
  FROM upo_mapas m,
    upo_mapas_plantillas mp,
    upo_mapas_objetos mo,
    upo_objetos o,
    upo_objetos_idiomas oi,
    upo_idiomas i,
    upo_origenes_informacion oin
  WHERE m.id = mo.mapa_id
        AND m.id = mp.mapa_id (+)
        AND o.id = mo.objeto_id
        AND o.id = oi.objeto_id
        AND i.id = oi.idioma_id
        AND mo.tipo = 'NORMAL'
        AND o.origen_informacion_id = oin.id (+)
        AND m.url_completa NOT LIKE '/paperera/%'
  UNION ALL
  SELECT
    o.id                                     contenido_id,
    oi.id                                    contenido_idioma_id,
    m.url_completa                           url_nodo,
    xm.url_completa                          url_nodo_original,
    o.url_path                               url_contenido,
    m.url_completa || o.url_path             url_completa,
    xm.url_completa || o.url_path            url_completa_original,
    m.url_num_niveles                        niveles,
    oi.enlace_destino,
    NVL(mp.nivel, 1),
    i.codigo_iso                             idioma,
    oi.titulo,
    oi.subtitulo,
    oi.titulo_largo,
    oi.resumen,
    oi.mime_type,
    oi.html                                  es_html,
    DECODE(oi.html, 'S', oi.contenido, NULL) contenido,
    oi.fecha_modificacion,
    o.fecha_creacion,
    m.orden                                  nodo_mapa_orden,
    dame_orden_padre(m.mapa_id)              orden_padre,
    mo.orden                                 contenido_orden,
    'LINK'                                   tipo_contenido,
    dame_autores(o.id)                       autor,
    DECODE(i.codigo_iso,
           'es', oin.nombre_es,
           'en', oin.nombre_en,
           oin.nombre)
                                             origen_nombre,
    oin.url                                  origen_url,
    o.lugar,
    get_proxima_fecha_vigencia(o.id)         proxima_fecha_vigencia,
    get_primera_fecha_vigencia(o.id)         primera_fecha_vigencia,
    concat_metadata(o.id, i.codigo_iso)      metadata,
    DECODE(i.codigo_iso,
           'es', m.titulo_publicacion_es,
           'en', m.titulo_publicacion_en,
           m.titulo_publicacion_ca)
                                             tituloNodo,
    mo.orden,
    get_prioridad(o.id)                      prioridad,
    ABS(get_inicio_vigencia_contenido(o.id) - SYSDATE)
                                             distancia_sysdate,
    oi.tipo_formato,
    concat_atributos(oi.id),
    o.visible,
    o.texto_novisible,
    concat_tags(o.id),
    m.orden
  FROM upo_mapas m,
    upo_mapas_plantillas mp,
    upo_mapas_objetos mo,
    upo_objetos o,
    upo_objetos_idiomas oi,
    upo_idiomas i,
    upo_origenes_informacion oin,
    upo_mapas xm,
    upo_mapas_objetos xmo
  WHERE m.id = mo.mapa_id
        AND m.id = mp.mapa_id (+)
        AND o.id = mo.objeto_id
        AND o.id = oi.objeto_id
        AND i.id = oi.idioma_id
        AND mo.tipo = 'LINK'
        AND o.origen_informacion_id = oin.id (+)
        AND mo.estado_moderacion = 'ACEPTADO'
        AND xm.id = xmo.mapa_id
        AND o.id = xmo.objeto_id
        AND xmo.tipo = 'NORMAL'
        AND xm.url_completa NOT LIKE '/paperera%';


ALTER TABLE UJI_PORTAL.UPO_OBJETOS_IDIOMAS
  MODIFY (TITULO VARCHAR2(4000 BYTE) )
/

ALTER TABLE UJI_PORTAL.UPO_OBJETOS_IDIOMAS_LOG
  MODIFY (TITULO VARCHAR2(4000 BYTE) )
/

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_BUSQUEDA
(
    CONTENIDO_ID,
    CONTENIDO_IDIOMA_ID,
    MAPA_URL_COMPLETA,
    IDIOMA_CODIGO_ISO,
    TITULO_SIN_ACENTOS,
    TITULO,
    TITULO_LARGO_SIN_ACENTOS,
    TITULO_LARGO,
    RESUMEN_SIN_ACENTOS,
    RESUMEN,
    CONTENIDO,
    SUBTITULO_SIN_ACENTOS,
    SUBTITULO,
    ORDEN,
    PRIORIDAD,
    DISTANCIA_SYSDATE,
    FECHA_VIGENCIA,
    ORIGEN_INFORMACION_ID,
    ES_HTML,
    FECHA_CREACION,
    ORDEN_MAPA
)
AS
  SELECT
    o.id                                     contenido_id,
    oi.id                                    contenido_idioma_id,
    m.url_completa                           mapa_url_completa,
    LOWER(i.codigo_iso)                      idioma_codigo_iso,
    quita_acentos(oi.titulo)                 titulo_sin_acentos,
    oi.titulo                                titulo,
    quita_acentos(oi.titulo_largo)           titulo_largo_sin_acentos,
    oi.titulo_largo                          titulo_largo,
    quita_acentos(oi.resumen)                resumen_sin_acentos,
    oi.resumen                               resumen,
    DECODE(oi.html, 'S', oi.contenido, NULL) contenido,
    quita_acentos(oi.subtitulo)              subtitulo_sin_acentos,
    oi.subtitulo                             subtitulo,
    mo.orden,
    get_prioridad(o.id)                      prioridad,
    ABS(get_inicio_vigencia_contenido(o.id) - SYSDATE)
                                             distancia_sysdate,
    TRUNC(vo.fecha)                          fecha,
    origen_informacion_id,
    oi.html,
    o.fecha_creacion,
    m.orden
  FROM upo_mapas_objetos mo,
    upo_objetos o,
    upo_mapas m,
    upo_objetos_idiomas oi,
    upo_idiomas i,
    upo_vigencias_objetos vo
  WHERE mo.mapa_id = m.id
        AND mo.objeto_id = o.id
        AND oi.objeto_id = o.id
        AND i.id = oi.idioma_id
        AND ((mo.tipo = 'NORMAL')
             OR (mo.tipo = 'LINK' AND mo.estado_moderacion = 'ACEPTADO'))
        AND o.visible = 'S'
        AND oi.html = 'S'
        AND vo.objeto_id (+) = o.id;


CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_MODERACION
(
    ID,
    MAPA_OBJETO_ID,
    OBJETO_ID,
    MAPA_ID,
    ESTADO_MODERACION,
    PER_ID_MODERACION,
    PER_ID_PROPUESTA,
    TEXTO_RECHAZO,
    OBJETO_URL_PATH,
    URL_COMPLETA,
    MAPA_URL_COMPLETA,
    PERSONA_ID,
    OBJETO_TITULO,
    url_completa_original
)
AS
    SELECT mapa_objeto_id || '-' || persona_id,
           mapa_objeto_id,
           objeto_id,
           mapa_id,
           estado_moderacion,
           per_id_moderacion,
           per_id_propuesta,
           texto_rechazo,
           objeto_url_path,
           url_completa,
           mapa_url_completa,
           persona_id,
           (CASE
                WHEN titulo_ca IS NOT NULL THEN titulo_ca
                WHEN titulo_es IS NOT NULL THEN titulo_es
                WHEN titulo_en IS NOT NULL THEN titulo_en
                ELSE NULL
            END)    titulo,
            (select m.url_completa
               from upo_mapas m,
                    upo_mapas_objetos mo,
                    upo_objetos o
              where o.id = mo.objeto_id
                and m.id = mo.mapa_id
                and mo.tipo = 'NORMAL'
                and o.id = s.objeto_id) url_completa_original
      FROM (SELECT DISTINCT
                   mo.id                                       mapa_objeto_id,
                   mo.objeto_id                                objeto_id,
                   mo.mapa_id                                  mapa_id,
                   mo.estado_moderacion                        estado_moderacion,
                   mo.per_id_moderacion                        per_id_moderacion,
                   mo.per_id_propuesta                         per_id_propuesta,
                   mo.texto_rechazo                            texto_rechazo,
                   o.url_path                                  objeto_url_path,
                   m.url_completa || o.url_path                url_completa,
                   m.url_completa                              mapa_url_completa,
                   p.id                                        persona_id,
                   (SELECT titulo
                      FROM upo_objetos_idiomas oi, upo_idiomas i
                     WHERE     oi.objeto_id = o.id
                           AND i.id = oi.idioma_id
                           AND UPPER (i.codigo_iso) = 'CA')    titulo_ca,
                   (SELECT titulo
                      FROM upo_objetos_idiomas oi, upo_idiomas i
                     WHERE     oi.objeto_id = o.id
                           AND i.id = oi.idioma_id
                           AND UPPER (i.codigo_iso) = 'ES')    titulo_es,
                   (SELECT titulo
                      FROM upo_objetos_idiomas oi, upo_idiomas i
                     WHERE     oi.objeto_id = o.id
                           AND i.id = oi.idioma_id
                           AND UPPER (i.codigo_iso) = 'EN')    titulo_en
              FROM upo_mapas_objetos  mo,
                   upo_objetos        o,
                   upo_mapas          m,
                   upo_ext_personas   p
             WHERE     mo.tipo = 'LINK'
                   AND mo.mapa_id = m.id
                   AND mo.objeto_id = o.id
                   AND p.id IN
                           (SELECT DISTINCT per_id
                              FROM upo_franquicias_accesos  fa,
                                   upo_franquicias          f,
                                   upo_mapas                m2
                             WHERE     m2.franquicia_id = f.id
                                   AND fa.franquicia_id = f.id
                                   AND fa.tipo = 'ADMINISTRADOR'
                                   AND m.url_completa LIKE
                                           m2.url_completa || '%')) s;


create or replace view UPO_VW_binarios_busqueda as
select m.url_completa mapa_url_completa,
       o.id objeto_id,
       oi.id objeto_idioma_id,
       i.codigo_iso idioma_codigo_iso,
       oi.mime_type,
       oi.nombre_fichero,
       limpia(oi.nombre_fichero) nombre_fichero_sin_acentos,
       oi.titulo,
       limpia(oi.titulo) titulo_sin_acentos,
       oi.titulo_largo,
       limpia(oi.titulo_largo) titulo_largo_sin_acentos,
       oi.subtitulo,
       limpia(oi.subtitulo) subtitulo_sin_acentos,
       oi.resumen,
       limpia(oi.resumen) resumen_sin_acentos,
       m.url_num_niveles,
       oi.id_descarga
  from upo_mapas m,
       upo_mapas_objetos mo,
       upo_objetos o,
       upo_objetos_idiomas oi,
       upo_idiomas i
 where m.id = mo.mapa_id
   and o.id  = mo.objeto_id
   and oi.objeto_id = o.id
   and i.id = oi.idioma_id
   and mo.estado_moderacion != 'RECHAZADO'
   and oi.tipo_formato = 'BINARIO'
   and oi.longitud > 0;

   CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_MODERACION_HISTORICO
(
    ID,
    MAPA_OBJETO_ID,
    OBJETO_ID,
    MAPA_ID,
    ESTADO_MODERACION,
    PER_ID_MODERACION,
    PER_ID_PROPUESTA,
    TEXTO_RECHAZO,
    OBJETO_URL_PATH,
    URL_COMPLETA,
    MAPA_URL_COMPLETA,
    OBJETO_TITULO
)
AS
    SELECT mapa_objeto_id || '-' || per_id_moderacion,
           mapa_objeto_id,
           objeto_id,
           mapa_id,
           estado_moderacion,
           per_id_moderacion,
           per_id_propuesta,
           texto_rechazo,
           objeto_url_path,
           url_completa,
           mapa_url_completa,
           (CASE
                WHEN titulo_ca IS NOT NULL THEN titulo_ca
                WHEN titulo_es IS NOT NULL THEN titulo_es
                WHEN titulo_en IS NOT NULL THEN titulo_en
                ELSE NULL
            END)                              titulo
      FROM (SELECT DISTINCT
                   mo.id                                       mapa_objeto_id,
                   mo.objeto_id                                objeto_id,
                   mo.mapa_id                                  mapa_id,
                   mo.estado_moderacion                        estado_moderacion,
                   mo.per_id_moderacion                        per_id_moderacion,
                   mo.per_id_propuesta                         per_id_propuesta,
                   mo.texto_rechazo                            texto_rechazo,
                   o.url_path                                  objeto_url_path,
                   m.url_completa || o.url_path                url_completa,
                   m.url_completa                              mapa_url_completa,
                   (SELECT titulo
                      FROM upo_objetos_idiomas oi, upo_idiomas i
                     WHERE     oi.objeto_id = o.id
                           AND i.id = oi.idioma_id
                           AND UPPER (i.codigo_iso) = 'CA')    titulo_ca,
                   (SELECT titulo
                      FROM upo_objetos_idiomas oi, upo_idiomas i
                     WHERE     oi.objeto_id = o.id
                           AND i.id = oi.idioma_id
                           AND UPPER (i.codigo_iso) = 'ES')    titulo_es,
                   (SELECT titulo
                      FROM upo_objetos_idiomas oi, upo_idiomas i
                     WHERE     oi.objeto_id = o.id
                           AND i.id = oi.idioma_id
                           AND UPPER (i.codigo_iso) = 'EN')    titulo_en
              FROM upo_mapas_objetos  mo,
                   upo_objetos        o,
                   upo_mapas          m
             WHERE     mo.tipo = 'LINK'
                   AND mo.mapa_id = m.id
                   AND mo.objeto_id = o.id) s;


CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_BUSQUEDA
(
    CONTENIDO_ID,
    CONTENIDO_IDIOMA_ID,
    MAPA_URL_COMPLETA,
    IDIOMA_CODIGO_ISO,
    TITULO_SIN_ACENTOS,
    TITULO,
    TITULO_LARGO_SIN_ACENTOS,
    TITULO_LARGO,
    RESUMEN_SIN_ACENTOS,
    RESUMEN,
    CONTENIDO,
    SUBTITULO_SIN_ACENTOS,
    SUBTITULO,
    ORDEN,
    PRIORIDAD,
    DISTANCIA_SYSDATE,
    FECHA_VIGENCIA,
    ORIGEN_INFORMACION_ID,
    ES_HTML,
    FECHA_CREACION,
    ORDEN_MAPA,
    ENLACE_DESTINO
)
AS
    SELECT o.id
               contenido_id,
           oi.id
               contenido_idioma_id,
           m.url_completa
               mapa_url_completa,
           LOWER (i.codigo_iso)
               idioma_codigo_iso,
           quita_acentos (oi.titulo)
               titulo_sin_acentos,
           oi.titulo
               titulo,
           quita_acentos (oi.titulo_largo)
               titulo_largo_sin_acentos,
           oi.titulo_largo
               titulo_largo,
           quita_acentos (oi.resumen)
               resumen_sin_acentos,
           oi.resumen
               resumen,
           DECODE (oi.html, 'S', oi.contenido, NULL)
               contenido,
           quita_acentos (oi.subtitulo)
               subtitulo_sin_acentos,
           oi.subtitulo
               subtitulo,
           mo.orden,
           get_prioridad (o.id)
               prioridad,
           ABS (get_inicio_vigencia_contenido (o.id) - SYSDATE)
               distancia_sysdate,
           TRUNC (vo.fecha)
               fecha,
           origen_informacion_id,
           oi.html,
           o.fecha_creacion,
           m.orden,
           oi.enlace_destino
      FROM upo_mapas_objetos      mo,
           upo_objetos            o,
           upo_mapas              m,
           upo_objetos_idiomas    oi,
           upo_idiomas            i,
           upo_vigencias_objetos  vo
     WHERE     mo.mapa_id = m.id
           AND mo.objeto_id = o.id
           AND oi.objeto_id = o.id
           AND i.id = oi.idioma_id
           AND (   (mo.tipo = 'NORMAL')
                OR (mo.tipo = 'LINK' AND mo.estado_moderacion = 'ACEPTADO'))
           AND o.visible = 'S'
           AND oi.html = 'S'
           AND vo.objeto_id(+) = o.id;

CREATE OR REPLACE function UJI_PORTAL.concat_metadata(v_objeto_id number, v_idioma_iso varchar2)
  return clob

is
  vRdo clob;
  amount INTEGER := 3;
  len number;

  cursor metadatos is
    select decode(v_idioma_iso, 'ca', clave || to_clob('@@@') || nombre_clave_ca || '@@@' || valor_ca || '@@@' || publicable,
                                'es', clave || to_clob('@@@') || nombre_clave_es || '@@@' || valor_es || '@@@' || publicable,
                                'en', clave || to_clob('@@@') || nombre_clave_en || '@@@' || valor_en || '@@@' || publicable) || '~~~' result
    from ( select om.clave, 1 publicable,
             om.nombre_clave_ca,
             om.nombre_clave_es,
             om.nombre_clave_en,
             om.valor_ca,
             om.valor_es,
             om.valor_en,
             -1 orden
           from upo_objetos_metadatos om
           where objeto_id = v_objeto_id
                 and ((v_idioma_iso = 'ca' and valor_ca is not null and atributo_id is null)
                      or
                      (v_idioma_iso = 'es' and valor_es is not null and atributo_id is null)
                      or
                      (v_idioma_iso = 'en' and valor_en is not null and atributo_id is null)
           )
           union
           select am.clave, am.publicable,
             am.nombre_clave_ca,
             am.nombre_clave_es,
             am.nombre_clave_en,
             nvl((select valor_ca from upo_valores_metadatos where atributo_id = am.id and to_char(id) = om.valor_ca), om.valor_ca) valor_ca,
             nvl((select valor_es from upo_valores_metadatos where atributo_id = am.id and to_char(id) = om.valor_es), om.valor_es) valor_es,
             nvl((select valor_en from upo_valores_metadatos where atributo_id = am.id and to_char(id) = om.valor_en), om.valor_en) valor_en,
             orden
           from upo_objetos_metadatos om, upo_atributos_metadatos am
           where om.objeto_id = v_objeto_id
                 and om.atributo_id = am.id
                 and ((v_idioma_iso = 'ca' and om.valor_ca is not null)
                      or
                      (v_idioma_iso = 'es' and om.valor_es is not null)
                      or
                      (v_idioma_iso = 'en' and om.valor_en is not null)
           )
           union
           select 'esquema', 0 publicable, 'esquema', 'esquema', 'esquema', lower(em.nombre), lower(em.nombre), lower(em.nombre), -1 orden
           from upo_objetos_metadatos om, upo_atributos_metadatos am, upo_esquemas_metadatos em
           where om.objeto_id = v_objeto_id
                 and om.atributo_id = am.id
                 and em.id = am.esquema_id

    ) order by orden asc;

  begin

    dbms_lob.createtemporary(vRdo, true);

    for m in metadatos loop

      dbms_lob.writeappend(vRdo, length(m.result), m.result);

    end loop;

    len := dbms_lob.getlength(vRdo);

    if (len = 0) then

      return null;

    end if;

    if (len > amount) then

      dbms_lob.erase(vRdo, amount, len - amount + 1);
      dbms_lob.trim(vRdo, len - 3);

    end if;

    return vRdo;
  end;
/

alter table upo_mapas_objetos add (propuesta_externa number default 0 not null);

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_PUBLICACION_BINARIOS
(
    ID,
    URL_NODO,
    URL_PATH,
    CONTENIDO,
    IDIOMA,
    MIME_TYPE,
    ID_DESCARGA
)
BEQUEATH DEFINER
AS
    SELECT mo.id,
           m.url_completa     url_modo,
           o.url_path,
           oi.contenido,
           i.codigo_iso,
           oi.mime_type,
           oi.id_descarga
      FROM upo_mapas            m,
           upo_mapas_objetos    mo,
           upo_objetos          o,
           upo_objetos_idiomas  oi,
           upo_idiomas          i
     WHERE     m.id = mo.mapa_id
           AND o.id = mo.objeto_id
           AND o.id = oi.objeto_id
           AND i.id = oi.idioma_id
           AND mo.estado_moderacion = 'ACEPTADO'
           AND NVL (oi.html, 'N') = 'N'
           AND o.visible = 'S';

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_PUBLICACION
(
    CONTENIDO_ID,
    CONTENIDO_IDIOMA_ID,
    URL_NODO,
    URL_NODO_ORIGINAL,
    URL_CONTENIDO,
    URL_COMPLETA,
    URL_COMPLETA_ORIGINAL,
    URL_NUM_NIVELES,
    URL_DESTINO,
    NIVEL,
    IDIOMA,
    TITULO,
    SUBTITULO,
    TITULO_LARGO,
    RESUMEN,
    MIME_TYPE,
    ES_HTML,
    CONTENIDO,
    FECHA_MODIFICACION,
    FECHA_CREACION,
    NODO_MAPA_ORDEN,
    NODO_MAPA_PADRE_ORDEN,
    CONTENIDO_ORDEN,
    TIPO_CONTENIDO,
    AUTOR,
    ORIGEN_NOMBRE,
    ORIGEN_URL,
    LUGAR,
    PROXIMA_FECHA_VIGENCIA,
    PRIMERA_FECHA_VIGENCIA,
    METADATA,
    TITULO_NODO,
    ORDEN,
    PRIORIDAD,
    DISTANCIA_SYSDATE,
    TIPO_FORMATO,
    ATRIBUTOS,
    VISIBLE,
    TEXTO_NOVISIBLE,
    TAGS,
    ORDEN_MAPA
)
BEQUEATH DEFINER
AS
    SELECT o.id
               contenido_id,
           oi.id
               contenido_idioma_id,
           m.url_completa
               url_nodo,
           m.url_completa
               url_nodo_original,
           o.url_path
               url_contenido,
           m.url_completa || o.url_path
               url_completa,
           m.url_completa || o.url_path
               url_completa_original,
           url_num_niveles
               niveles,
           oi.enlace_destino,
           NVL (mp.nivel, 1),
           i.codigo_iso
               idioma,
           oi.titulo,
           oi.subtitulo,
           oi.titulo_largo,
           oi.resumen,
           oi.mime_type,
           oi.html
               es_html,
           DECODE (oi.html, 'S', oi.contenido, NULL)
               contenido,
           oi.fecha_modificacion,
           o.fecha_creacion,
           m.orden
               nodo_mapa_orden,
           dame_orden_padre (m.mapa_id)
               orden_padre,
           mo.orden
               contenido_orden,
           'NORMAL'
               tipo_contenido,
           dame_autores (o.id)
               autor,
           DECODE (i.codigo_iso,
                   'es', oin.nombre_es,
                   'en', oin.nombre_en,
                   oin.nombre)
               origen_nombre,
           oin.url
               origen_url,
           o.lugar,
           get_proxima_fecha_vigencia (o.id)
               proxima_fecha_vigencia,
           get_primera_fecha_vigencia (o.id)
               primera_fecha_vigencia,
           concat_metadata (o.id, i.codigo_iso)
               metadata,
           DECODE (i.codigo_iso,
                   'es', m.titulo_publicacion_es,
                   'en', m.titulo_publicacion_en,
                   m.titulo_publicacion_ca)
               tituloNodo,
           mo.orden,
           get_prioridad (o.id)
               prioridad,
           ABS (get_inicio_vigencia_contenido (o.id) - SYSDATE)
               distancia_sysdate,
           oi.tipo_formato,
           concat_atributos (oi.id),
           o.visible,
           o.texto_novisible,
           concat_tags (o.id),
           m.orden
      FROM upo_mapas                 m,
           upo_mapas_plantillas      mp,
           upo_mapas_objetos         mo,
           upo_objetos               o,
           upo_objetos_idiomas       oi,
           upo_idiomas               i,
           upo_origenes_informacion  oin
     WHERE     m.id = mo.mapa_id
           AND m.id = mp.mapa_id(+)
           AND o.id = mo.objeto_id
           AND o.id = oi.objeto_id
           AND i.id = oi.idioma_id
           AND mo.tipo = 'NORMAL'
           AND mo.estado_moderacion = 'ACEPTADO'
           AND o.origen_informacion_id = oin.id(+)
           AND m.url_completa NOT LIKE '/paperera/%'
    UNION ALL
    SELECT o.id
               contenido_id,
           oi.id
               contenido_idioma_id,
           m.url_completa
               url_nodo,
           xm.url_completa
               url_nodo_original,
           o.url_path
               url_contenido,
           m.url_completa || o.url_path
               url_completa,
           xm.url_completa || o.url_path
               url_completa_original,
           m.url_num_niveles
               niveles,
           oi.enlace_destino,
           NVL (mp.nivel, 1),
           i.codigo_iso
               idioma,
           oi.titulo,
           oi.subtitulo,
           oi.titulo_largo,
           oi.resumen,
           oi.mime_type,
           oi.html
               es_html,
           DECODE (oi.html, 'S', oi.contenido, NULL)
               contenido,
           oi.fecha_modificacion,
           o.fecha_creacion,
           m.orden
               nodo_mapa_orden,
           dame_orden_padre (m.mapa_id)
               orden_padre,
           mo.orden
               contenido_orden,
           'LINK'
               tipo_contenido,
           dame_autores (o.id)
               autor,
           DECODE (i.codigo_iso,
                   'es', oin.nombre_es,
                   'en', oin.nombre_en,
                   oin.nombre)
               origen_nombre,
           oin.url
               origen_url,
           o.lugar,
           get_proxima_fecha_vigencia (o.id)
               proxima_fecha_vigencia,
           get_primera_fecha_vigencia (o.id)
               primera_fecha_vigencia,
           concat_metadata (o.id, i.codigo_iso)
               metadata,
           DECODE (i.codigo_iso,
                   'es', m.titulo_publicacion_es,
                   'en', m.titulo_publicacion_en,
                   m.titulo_publicacion_ca)
               tituloNodo,
           mo.orden,
           get_prioridad (o.id)
               prioridad,
           ABS (get_inicio_vigencia_contenido (o.id) - SYSDATE)
               distancia_sysdate,
           oi.tipo_formato,
           concat_atributos (oi.id),
           o.visible,
           o.texto_novisible,
           concat_tags (o.id),
           m.orden
      FROM upo_mapas                 m,
           upo_mapas_plantillas      mp,
           upo_mapas_objetos         mo,
           upo_objetos               o,
           upo_objetos_idiomas       oi,
           upo_idiomas               i,
           upo_origenes_informacion  oin,
           upo_mapas                 xm,
           upo_mapas_objetos         xmo
     WHERE     m.id = mo.mapa_id
           AND m.id = mp.mapa_id(+)
           AND o.id = mo.objeto_id
           AND o.id = oi.objeto_id
           AND i.id = oi.idioma_id
           AND mo.tipo = 'LINK'
           AND o.origen_informacion_id = oin.id(+)
           AND mo.estado_moderacion = 'ACEPTADO'
           AND xm.id = xmo.mapa_id
           AND o.id = xmo.objeto_id
           AND xmo.tipo = 'NORMAL'
           AND xmo.estado_moderacion = 'ACEPTADO'
           AND xm.url_completa NOT LIKE '/paperera%';

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_SYNC_CONTENIDOS
(
    OBJETO_ID,
    URL_NODO,
    ORDEN,
    PRIORIDAD,
    DISTANCIA_SYSDATE,
    FECHA_CREACION
)
BEQUEATH DEFINER
AS
    SELECT o.id,
           m.url_completa,
           mo.orden,
           get_prioridad (o.id)
               prioridad,
           ABS (get_inicio_vigencia_contenido (o.id) - SYSDATE)
               distancia_sysdate,
           o.fecha_creacion
      FROM upo_mapas m, upo_mapas_objetos mo, upo_objetos o
     WHERE     m.id = mo.mapa_id
           AND o.id = mo.objeto_id              -- cualquier contenido visible
           AND mo.estado_moderacion = 'ACEPTADO'
           AND o.visible = 'S'
           AND (                                   -- no hay vigencia definida
                   NOT EXISTS
                       (SELECT id
                          FROM upo_vigencias_objetos vo
                         WHERE vo.objeto_id = o.id)
                OR         -- vigencia de 72 horas y dentro de rango de fechas
                   EXISTS
                       (SELECT id
                          FROM upo_vigencias_objetos vo
                         WHERE     vo.objeto_id = o.id
                               AND (   (    (   TO_DATE (
                                                       TO_CHAR (fecha,
                                                                'dd/mm/yyyy')
                                                    || ' '
                                                    || TO_CHAR (
                                                           NVL (hora_inicio,
                                                                SYSDATE),
                                                           'hh24:mi:ss'),
                                                    'dd/mm/yyyy hh24:mi:ss') BETWEEN SYSDATE
                                                                                 AND   SYSDATE
                                                                                     + 3
                                             OR TO_DATE (
                                                       TO_CHAR (fecha,
                                                                'dd/mm/yyyy')
                                                    || ' '
                                                    || TO_CHAR (
                                                           NVL (hora_fin,
                                                                SYSDATE),
                                                           'hh24:mi:ss'),
                                                    'dd/mm/yyyy hh24:mi:ss') BETWEEN SYSDATE
                                                                                 AND   SYSDATE
                                                                                     + 3)
                                        AND o.ajustar_a_fechas_vigencia = 0)
                                    OR (    (    TO_DATE (
                                                        TO_CHAR (
                                                            fecha,
                                                            'dd/mm/yyyy')
                                                     || ' '
                                                     || TO_CHAR (
                                                            NVL (hora_inicio,
                                                                 SYSDATE),
                                                            'hh24:mi:ss'),
                                                     'dd/mm/yyyy hh24:mi:ss') <=
                                                 SYSDATE
                                             AND TO_DATE (
                                                        TO_CHAR (
                                                            fecha,
                                                            'dd/mm/yyyy')
                                                     || ' '
                                                     || TO_CHAR (
                                                            NVL (hora_fin,
                                                                 SYSDATE),
                                                            'hh24:mi:ss'),
                                                     'dd/mm/yyyy hh24:mi:ss') >=
                                                 SYSDATE)
                                        AND o.ajustar_a_fechas_vigencia = 1))))
           -- Obligatorio en CA y ES
           AND (   (    EXISTS
                            (SELECT i.codigo_iso
                               FROM upo_objetos_idiomas oi, upo_idiomas i
                              WHERE     i.id = oi.idioma_id
                                    AND LOWER (i.codigo_iso) = 'ca'
                                    AND o.id = oi.objeto_id
                                    AND NVL (oi.html, 'N') = 'S')
                    AND EXISTS
                            (SELECT i.codigo_iso
                               FROM upo_objetos_idiomas oi, upo_idiomas i
                              WHERE     i.id = oi.idioma_id
                                    AND LOWER (i.codigo_iso) = 'es'
                                    AND o.id = oi.objeto_id
                                    AND NVL (oi.html, 'N') = 'S'))
                OR (has_tag (o.id, 'megabanner') = 'S'));


CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_REVISTA
(
    CONTENIDO_IDIOMA_ID,
    TITULO_LARGO,
    OTROS_AUTORES,
    RESUMEN,
    URL_COMPLETA,
    PRIMERA_FECHA_VIGENCIA,
    TITULO_LARGO_SIN_ACENTOS,
    RESUMEN_SIN_ACENTOS,
    OTROS_AUTORES_SIN_ACENTOS,
    TAGS,
    URL_COMPLETA_BUSQUEDA,
    MAPA_URL_COMPLETA
)
BEQUEATH DEFINER
AS
    SELECT oi.id                                  contenido_idioma_id,
           oi.titulo_largo,
           o.otros_autores,
           oi.resumen,
           m.url_completa,
           get_primera_fecha_vigencia (o.id)      primera_fecha_vigencia,
           quita_acentos (titulo_largo)           titulo_largo_sin_acentos,
           quita_acentos (resumen)                resumen_sin_acentos,
           quita_acentos (otros_autores)          otros_autores_sin_acentos,
           quita_acentos (concat_tags (o.id))     tags,
           quita_acentos (m.url_completa)         url_completa_busqueda,
           m.url_completa
      FROM upo_mapas_objetos    mo,
           upo_objetos          o,
           upo_mapas            m,
           upo_objetos_idiomas  oi,
           upo_idiomas          i
     WHERE     mo.mapa_id = m.id
           AND mo.objeto_id = o.id
           AND oi.objeto_id = o.id
           AND i.id = oi.idioma_id
           AND mo.estado_moderacion = 'ACEPTADO'
           AND oi.tipo_formato = 'BINARIO'
           AND (   INSTR (mime_type, 'image') > 0
                OR mime_type = 'application/pdf')
           AND m.url_completa LIKE '/com/revista/base/%'
           AND LOWER (i.codigo_iso) = 'ca'
    UNION
    SELECT r.c_id                           contenido_idioma_id,
           titular                          titulo_largo,
           m.nombre                         otros_autores,
           resum                            resumen,
              '/upo/rest/revista/'
           || xp_get_num_fichero (r.c_id)
           || '?revistaId='
           || r.c_id                        url_completa,
           data_publicacio                  data_publicacio,
           quita_acentos (titular)          titulo_largo_sin_acentos,
           quita_acentos (resum)            resumen_sin_acentos,
           quita_acentos (m.nombre)         otros_autores_sin_acentos,
           quita_acentos (descriptors)      tags,
           quita_acentos (sr.nom_seccio)    url_completa_busqueda,
           NULL                             mapa_url_completa
      FROM xpfdm.xpv$revista          r,
           xpfdm.xp$lov_medios        m,
           xpfdm.xp$seccions_revista  sr
     WHERE m.c_id = r.mitja AND r.seccio = sr.c_id;


CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_BUSQUEDA_SIN_FILTRAR
(
    CONTENIDO_IDIOMA_ID,
    MAPA_URL_COMPLETA,
    IDIOMA_CODIGO_ISO,
    TITULO,
    TITULO_LARGO,
    RESUMEN,
    CONTENIDO,
    SUBTITULO,
    ORDEN,
    PRIORIDAD,
    DISTANCIA_SYSDATE,
    FECHA_VIGENCIA
)
BEQUEATH DEFINER
AS
    SELECT oi.id
               contenido_idioma_id,
           m.url_completa
               mapa_url_completa,
           LOWER (i.codigo_iso)
               idioma_codigo_iso,
           oi.titulo
               titulo,
           oi.titulo_largo
               titulo_largo,
           oi.resumen
               resumen,
           DECODE (oi.html, 'S', oi.contenido, NULL)
               contenido,
           oi.subtitulo
               subtitulo,
           mo.orden,
           get_prioridad (o.id)
               prioridad,
           ABS (get_inicio_vigencia_contenido (o.id) - SYSDATE)
               distancia_sysdate,
           TRUNC (vo.fecha)
               fecha
      FROM upo_mapas_objetos      mo,
           upo_objetos            o,
           upo_mapas              m,
           upo_objetos_idiomas    oi,
           upo_idiomas            i,
           upo_vigencias_objetos  vo
     WHERE     mo.mapa_id = m.id
           AND mo.objeto_id = o.id
           AND oi.objeto_id = o.id
           AND i.id = oi.idioma_id
           AND mo.estado_moderacion = 'ACEPTADO'
           AND o.visible = 'S'
           AND oi.html = 'S'
           AND vo.objeto_id(+) = o.id;


CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_BUSQUEDA
(
    CONTENIDO_ID,
    CONTENIDO_IDIOMA_ID,
    MAPA_URL_COMPLETA,
    IDIOMA_CODIGO_ISO,
    TITULO_SIN_ACENTOS,
    TITULO,
    TITULO_LARGO_SIN_ACENTOS,
    TITULO_LARGO,
    RESUMEN_SIN_ACENTOS,
    RESUMEN,
    CONTENIDO,
    SUBTITULO_SIN_ACENTOS,
    SUBTITULO,
    ORDEN,
    PRIORIDAD,
    DISTANCIA_SYSDATE,
    FECHA_VIGENCIA,
    ORIGEN_INFORMACION_ID,
    ES_HTML,
    FECHA_CREACION,
    ORDEN_MAPA,
    ENLACE_DESTINO
)
BEQUEATH DEFINER
AS
    SELECT o.id
               contenido_id,
           oi.id
               contenido_idioma_id,
           m.url_completa
               mapa_url_completa,
           LOWER (i.codigo_iso)
               idioma_codigo_iso,
           quita_acentos (oi.titulo)
               titulo_sin_acentos,
           oi.titulo
               titulo,
           quita_acentos (oi.titulo_largo)
               titulo_largo_sin_acentos,
           oi.titulo_largo
               titulo_largo,
           quita_acentos (oi.resumen)
               resumen_sin_acentos,
           oi.resumen
               resumen,
           DECODE (oi.html, 'S', oi.contenido, NULL)
               contenido,
           quita_acentos (oi.subtitulo)
               subtitulo_sin_acentos,
           oi.subtitulo
               subtitulo,
           mo.orden,
           get_prioridad (o.id)
               prioridad,
           ABS (get_inicio_vigencia_contenido (o.id) - SYSDATE)
               distancia_sysdate,
           TRUNC (vo.fecha)
               fecha,
           origen_informacion_id,
           oi.html,
           o.fecha_creacion,
           m.orden,
           oi.enlace_destino
      FROM upo_mapas_objetos      mo,
           upo_objetos            o,
           upo_mapas              m,
           upo_objetos_idiomas    oi,
           upo_idiomas            i,
           upo_vigencias_objetos  vo
     WHERE     mo.mapa_id = m.id
           AND mo.objeto_id = o.id
           AND oi.objeto_id = o.id
           AND i.id = oi.idioma_id
           AND mo.estado_moderacion = 'ACEPTADO'
           AND o.visible = 'S'
           AND oi.html = 'S'
           AND vo.objeto_id(+) = o.id;


CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_MODERACION_HISTORICO
(
    ID,
    MAPA_OBJETO_ID,
    OBJETO_ID,
    MAPA_ID,
    ESTADO_MODERACION,
    PER_ID_MODERACION,
    PER_ID_PROPUESTA,
    TEXTO_RECHAZO,
    OBJETO_URL_PATH,
    URL_COMPLETA,
    MAPA_URL_COMPLETA,
    OBJETO_TITULO
)
BEQUEATH DEFINER
AS
    SELECT mapa_objeto_id || '-' || per_id_moderacion,
           mapa_objeto_id,
           objeto_id,
           mapa_id,
           estado_moderacion,
           per_id_moderacion,
           per_id_propuesta,
           texto_rechazo,
           objeto_url_path,
           url_completa,
           mapa_url_completa,
           (CASE
                WHEN titulo_ca IS NOT NULL THEN titulo_ca
                WHEN titulo_es IS NOT NULL THEN titulo_es
                WHEN titulo_en IS NOT NULL THEN titulo_en
                ELSE NULL
            END)    titulo
      FROM (SELECT DISTINCT
                   mo.id                                       mapa_objeto_id,
                   mo.objeto_id                                objeto_id,
                   mo.mapa_id                                  mapa_id,
                   mo.estado_moderacion                        estado_moderacion,
                   mo.per_id_moderacion                        per_id_moderacion,
                   mo.per_id_propuesta                         per_id_propuesta,
                   mo.texto_rechazo                            texto_rechazo,
                   o.url_path                                  objeto_url_path,
                   m.url_completa || o.url_path                url_completa,
                   m.url_completa                              mapa_url_completa,
                   (SELECT titulo
                      FROM upo_objetos_idiomas oi, upo_idiomas i
                     WHERE     oi.objeto_id = o.id
                           AND i.id = oi.idioma_id
                           AND UPPER (i.codigo_iso) = 'CA')    titulo_ca,
                   (SELECT titulo
                      FROM upo_objetos_idiomas oi, upo_idiomas i
                     WHERE     oi.objeto_id = o.id
                           AND i.id = oi.idioma_id
                           AND UPPER (i.codigo_iso) = 'ES')    titulo_es,
                   (SELECT titulo
                      FROM upo_objetos_idiomas oi, upo_idiomas i
                     WHERE     oi.objeto_id = o.id
                           AND i.id = oi.idioma_id
                           AND UPPER (i.codigo_iso) = 'EN')    titulo_en
              FROM upo_mapas_objetos mo, upo_objetos o, upo_mapas m
             WHERE  ((mo.tipo = 'LINK') or (propuesta_externa = 1))
                   and mo.estado_moderacion in ('ACEPTADO', 'RECHAZADO')
                   AND mo.mapa_id = m.id
                   AND mo.objeto_id = o.id) s;

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_MODERACION
(
    ID,
    MAPA_OBJETO_ID,
    OBJETO_ID,
    MAPA_ID,
    ESTADO_MODERACION,
    PER_ID_MODERACION,
    PER_ID_PROPUESTA,
    TEXTO_RECHAZO,
    OBJETO_URL_PATH,
    URL_COMPLETA,
    MAPA_URL_COMPLETA,
    PERSONA_ID,
    OBJETO_TITULO,
    URL_COMPLETA_ORIGINAL
)
BEQUEATH DEFINER
AS
    SELECT mapa_objeto_id || '-' || persona_id,
           mapa_objeto_id,
           objeto_id,
           mapa_id,
           estado_moderacion,
           per_id_moderacion,
           per_id_propuesta,
           texto_rechazo,
           objeto_url_path,
           url_completa,
           mapa_url_completa,
           persona_id,
           (CASE
                WHEN titulo_ca IS NOT NULL THEN titulo_ca
                WHEN titulo_es IS NOT NULL THEN titulo_es
                WHEN titulo_en IS NOT NULL THEN titulo_en
                ELSE NULL
            END)                              titulo,
           (SELECT m.url_completa
              FROM upo_mapas m, upo_mapas_objetos mo, upo_objetos o
             WHERE     o.id = mo.objeto_id
                   AND m.id = mo.mapa_id
                   AND mo.tipo = 'NORMAL'
                   AND o.id = s.objeto_id)    url_completa_original
      FROM (SELECT DISTINCT
                   mo.id                                       mapa_objeto_id,
                   mo.objeto_id                                objeto_id,
                   mo.mapa_id                                  mapa_id,
                   mo.estado_moderacion                        estado_moderacion,
                   mo.per_id_moderacion                        per_id_moderacion,
                   mo.per_id_propuesta                         per_id_propuesta,
                   mo.texto_rechazo                            texto_rechazo,
                   o.url_path                                  objeto_url_path,
                   m.url_completa || o.url_path                url_completa,
                   m.url_completa                              mapa_url_completa,
                   p.id                                        persona_id,
                   (SELECT titulo
                      FROM upo_objetos_idiomas oi, upo_idiomas i
                     WHERE     oi.objeto_id = o.id
                           AND i.id = oi.idioma_id
                           AND UPPER (i.codigo_iso) = 'CA')    titulo_ca,
                   (SELECT titulo
                      FROM upo_objetos_idiomas oi, upo_idiomas i
                     WHERE     oi.objeto_id = o.id
                           AND i.id = oi.idioma_id
                           AND UPPER (i.codigo_iso) = 'ES')    titulo_es,
                   (SELECT titulo
                      FROM upo_objetos_idiomas oi, upo_idiomas i
                     WHERE     oi.objeto_id = o.id
                           AND i.id = oi.idioma_id
                           AND UPPER (i.codigo_iso) = 'EN')    titulo_en
              FROM upo_mapas_objetos  mo,
                   upo_objetos        o,
                   upo_mapas          m,
                   upo_ext_personas   p
             WHERE mo.estado_moderacion = 'PENDIENTE'
                   and mo.mapa_id = m.id
                   AND mo.objeto_id = o.id
                   AND p.id IN
                           (SELECT DISTINCT per_id
                              FROM upo_franquicias_accesos  fa,
                                   upo_franquicias          f,
                                   upo_mapas                m2
                             WHERE     m2.franquicia_id = f.id
                                   AND fa.franquicia_id = f.id
                                   AND fa.tipo = 'ADMINISTRADOR'
                                   AND m.url_completa LIKE
                                           m2.url_completa || '%')) s;

CREATE OR REPLACE function UJI_PORTAL.get_proxima_fecha_vigencia(p_objeto_id number) return date is
  vFecha date;

begin

  select min(to_date(to_char(fecha, 'dd/mm/rrrr') || to_char(hora_inicio, 'hh24:mi'), 'dd/mm/rrrrhh24:mi'))
    into vFecha
    from upo_vigencias_objetos
   where objeto_id = p_objeto_id
     and ((hora_inicio is not null
           and to_date(to_char(fecha, 'dd/mm/rrrr') || to_char(hora_inicio, 'hh24:mi'), 'dd/mm/rrrrhh24:mi') >= sysdate
          )
          or
          (hora_inicio is null
           and trunc(fecha) >= trunc(sysdate)
          )
         );

  if vFecha is not null then
    return vFecha;
  end if;

  select nvl(min(to_date(to_char(fecha, 'dd/mm/rrrr') || to_char(hora_inicio, 'hh24:mi'), 'dd/mm/rrrrhh24:mi')), trunc(sysdate))
    into vFecha
    from upo_vigencias_objetos
   where objeto_id = p_objeto_id;

  return vFecha;

end;
/

ALTER TABLE UJI_PORTAL.UPO_MAPAS
ADD (num_niveles_sincro NUMBER(8));

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_SYNC_CONTENIDOS
(
    OBJETO_ID,
    URL_NODO,
    ORDEN,
    PRIORIDAD,
    DISTANCIA_SYSDATE,
    FECHA_CREACION,
    URL_NUM_NIVELES
)
BEQUEATH DEFINER
AS
    SELECT o.id,
           m.url_completa,
           mo.orden,
           get_prioridad (o.id)
               prioridad,
           ABS (get_inicio_vigencia_contenido (o.id) - SYSDATE)
               distancia_sysdate,
           o.fecha_creacion,
           m.url_num_niveles
      FROM upo_mapas m, upo_mapas_objetos mo, upo_objetos o
     WHERE     m.id = mo.mapa_id
           AND o.id = mo.objeto_id              -- cualquier contenido visible
           AND mo.estado_moderacion = 'ACEPTADO'
           AND o.visible = 'S'
           AND (                                   -- no hay vigencia definida
                   NOT EXISTS
                       (SELECT id
                          FROM upo_vigencias_objetos vo
                         WHERE vo.objeto_id = o.id)
                OR         -- vigencia de 72 horas y dentro de rango de fechas
                   EXISTS
                       (SELECT id
                          FROM upo_vigencias_objetos vo
                         WHERE     vo.objeto_id = o.id
                               AND (   (    (   TO_DATE (
                                                       TO_CHAR (fecha,
                                                                'dd/mm/yyyy')
                                                    || ' '
                                                    || TO_CHAR (
                                                           NVL (hora_inicio,
                                                                SYSDATE),
                                                           'hh24:mi:ss'),
                                                    'dd/mm/yyyy hh24:mi:ss') BETWEEN SYSDATE
                                                                                 AND   SYSDATE
                                                                                     + 3
                                             OR TO_DATE (
                                                       TO_CHAR (fecha,
                                                                'dd/mm/yyyy')
                                                    || ' '
                                                    || TO_CHAR (
                                                           NVL (hora_fin,
                                                                SYSDATE),
                                                           'hh24:mi:ss'),
                                                    'dd/mm/yyyy hh24:mi:ss') BETWEEN SYSDATE
                                                                                 AND   SYSDATE
                                                                                     + 3)
                                        AND o.ajustar_a_fechas_vigencia = 0)
                                    OR (    (    TO_DATE (
                                                        TO_CHAR (
                                                            fecha,
                                                            'dd/mm/yyyy')
                                                     || ' '
                                                     || TO_CHAR (
                                                            NVL (hora_inicio,
                                                                 SYSDATE),
                                                            'hh24:mi:ss'),
                                                     'dd/mm/yyyy hh24:mi:ss') <=
                                                 SYSDATE
                                             AND TO_DATE (
                                                        TO_CHAR (
                                                            fecha,
                                                            'dd/mm/yyyy')
                                                     || ' '
                                                     || TO_CHAR (
                                                            NVL (hora_fin,
                                                                 SYSDATE),
                                                            'hh24:mi:ss'),
                                                     'dd/mm/yyyy hh24:mi:ss') >=
                                                 SYSDATE)
                                        AND o.ajustar_a_fechas_vigencia = 1))))
           -- Obligatorio en CA y ES
           AND (   (    EXISTS
                            (SELECT i.codigo_iso
                               FROM upo_objetos_idiomas oi, upo_idiomas i
                              WHERE     i.id = oi.idioma_id
                                    AND LOWER (i.codigo_iso) = 'ca'
                                    AND o.id = oi.objeto_id
                                    AND NVL (oi.html, 'N') = 'S')
                    AND EXISTS
                            (SELECT i.codigo_iso
                               FROM upo_objetos_idiomas oi, upo_idiomas i
                              WHERE     i.id = oi.idioma_id
                                    AND LOWER (i.codigo_iso) = 'es'
                                    AND o.id = oi.objeto_id
                                    AND NVL (oi.html, 'N') = 'S'))
                OR (has_tag (o.id, 'megabanner') = 'S'));

CREATE OR REPLACE package UJI_PORTAL.sync_contenidos as
  type t_content is record (
    url_nodo varchar2(4000)
  );

  type t_contents is table of t_content;
  type t_array_output is table of varchar2(32767);

  vSourceDirectories t_contents;
  vTargetContents t_contents;

  procedure clean_target_directory(p_destino varchar2);

  procedure sync_directory_from_settings(
    p_origen         varchar2,
    p_destino        varchar2,
    p_max_contenidos number,
    p_num_niveles number);

  procedure sync_recent_contents(
    p_origen         varchar2,
    p_destino        varchar2,
    p_max_contenidos number);

end;
/

CREATE OR REPLACE package body UJI_PORTAL.sync_contenidos as

  procedure clean_target_directory(p_destino varchar2) is
    begin
      delete from upo_mapas_objetos
      where mapa_id in (select id
                        from upo_mapas
                        where url_completa like p_destino || '_%');

      delete from upo_mapas_plantillas
      where mapa_id in (select id
                        from upo_mapas
                        where url_completa like p_destino || '_%');

      delete from upo_mapas
      where url_completa like p_destino || '_%';
    end;

  function get_source_directories_setting(p_origen varchar2, p_num_niveles number)
    return t_contents
  is
    vContenidos t_contents;
    begin
      execute immediate 'select url_nodo
                           from upo_vw_sync_contenidos
                          where url_nodo like ''' || p_origen || '%''
                            and (('''|| p_num_niveles || ''' is null)
                                 or
                                 ('''|| p_num_niveles || ''' is not null and (REGEXP_COUNT(''' || p_origen || ''', ''/'') + ''' || p_num_niveles || ''') >= url_num_niveles)
                                )
                           order by prioridad, distancia_sysdate, orden, fecha_creacion desc'
      bulk collect into vContenidos;

      return vContenidos;
    end;

  function get_recent_directories(p_origen varchar2, p_max_resultados number)
    return t_contents
  is
    vContenidos t_contents;
    begin

      execute immediate 'select url_completa
                          from (select m.url_completa
                            from upo_objetos o,
                                 upo_objetos_idiomas oi,
                                 upo_idiomas i,
                                 upo_mapas_objetos mo,
                                 upo_mapas m
                           where oi.objeto_id = o.id
                             and oi.idioma_id = i.id
                             and i.codigo_iso = ''ca''
                             and html = ''S''
                             and mo.objeto_id = o.id
                             and mo.mapa_id = m.id
                             and mo.tipo = ''NORMAL''
                             and m.url_completa like ''' || p_origen || '%''
                             and m.url_completa not like ''/paperera/%''
                             and m.url_completa != ''' || p_origen || '''
                           order by o.fecha_creacion desc)
                          where rownum <=' || p_max_resultados
      bulk collect into vContenidos;

      return vContenidos;
    end;

  function contains_path(vPaths t_array_output, vPath varchar2)
    return boolean
  is
    begin
      for i in 1..vPaths.count loop
        if vPaths(i) = vPath then
          return true;
        end if;
      end loop;

      return false;
    end;

  procedure add_source_dirs_to_target(p_contents t_contents, p_destino varchar2, p_max_contenidos number) is
    vDirectory  varchar2(4000);

    vTargetNode number;
    vNewNode    number;

    vFranquicia number;
    vMenu       number;
    vAux        varchar2(4000);
    vPaths      t_array_output:= t_array_output();

    begin
      select id, franquicia_id, menu_id
      into vTargetNode, vFranquicia, vMenu
      from upo_mapas
      where url_completa = p_destino;

      for i in 1..p_contents.count loop
        begin
          vAux:= replace(p_contents(i).url_nodo, '/', '-');
          vDirectory:= substr(vAux, 2, length(vAux)-2);

          if not contains_path(vPaths, vDirectory) then
            if vPaths.count < p_max_contenidos then
              insert into upo_mapas(id, url_path, franquicia_id, url_completa, menu_id, mapa_id, menu_heredado, orden)
              values (hibernate_sequence.nextval,vDirectory, vFranquicia, p_destino || vDirectory || '/', vMenu, vTargetNode, 1, i)
              returning id into vNewNode;

              vPaths.extend;
              vPaths(vPaths.count):= vDirectory;

              insert into upo_mapas_objetos (id, mapa_id, objeto_id, estado_moderacion, tipo)
                select hibernate_sequence.nextval, vNewNode, objeto_id, 'ACEPTADO', 'LINK'
                from upo_mapas_objetos
                where mapa_id = (select id
                                 from upo_mapas
                                 where url_completa = p_contents(i).url_nodo);
            end if;
          end if;
          exception
            when others then
                rollback;
                dbms_output.put_line('Directory: ' || vDirectory || ' - Error: ' || sqlerrm);
        end;
      end loop;
    end;

  procedure sync_directory_from_settings(
    p_origen         varchar2,
    p_destino        varchar2,
    p_max_contenidos number,
    p_num_niveles    number) is
  begin
    clean_target_directory(p_destino);
    vSourceDirectories:= get_source_directories_setting(p_origen, p_num_niveles);
    add_source_dirs_to_target(vSourceDirectories, p_destino, p_max_contenidos);

    commit;
  end;

  procedure sync_recent_contents(
    p_origen         varchar2,
    p_destino        varchar2,
    p_max_contenidos number) is
  begin
    clean_target_directory(p_destino);
    vSourceDirectories:= get_recent_directories(p_origen, p_max_contenidos);
    add_source_dirs_to_target(vSourceDirectories, p_destino, p_max_contenidos);

    commit;
  end;


end;
/



CREATE OR REPLACE procedure UJI_PORTAL.cron_sync_contenidos is

  cursor nodos is
    select sincro_desde_mapa origen,
           url_completa destino,
           num_items,
           num_niveles_sincro
      from upo_mapas
     where sincro_desde_mapa is not null
       and url_completa is not null
       and num_items is not null;

begin
   for n in nodos loop
       begin
          sync_contenidos.sync_directory_from_settings(
             p_origen => n.origen,
             p_destino => n.destino,
             p_max_contenidos => n.num_items,
             p_num_niveles => n.num_niveles_sincro
          );
       exception
          when others then
               rollback;
               euji_envios_html.mail ('portal@uji.es', 'fmelia@uji.es', 'ERROR en ejecución de sync contenidos portal'
                        , 'ERROR en ejecución de sync contenidos portal. Origen: ' || n.origen || ' destino: ' || n.destino || chr(10) || sqlerrm, p_permite_dup => 'S'
                        ,p_formato => 'TXT');
       end;
   end loop;

   begin

    SYNC_CONTENIDOS.sync_recent_contents('/', '/serveis/scp/rss/', 50);

   exception
     when others then
       rollback;
       euji_envios_html.mail ('portal@uji.es', 'fmelia@uji.es', 'ERROR en ejecución de sync contenidos portal - RECIENTES'
                , 'ERROR en ejecución de sync contenidos portal RECIENTES' || chr(10) || sqlerrm, p_permite_dup => 'S'
                ,p_formato => 'TXT');

   end;
end;
/

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_BUSQUEDA
(
    CONTENIDO_ID,
    CONTENIDO_IDIOMA_ID,
    MAPA_URL_COMPLETA,
    IDIOMA_CODIGO_ISO,
    TITULO_SIN_ACENTOS,
    TITULO,
    TITULO_LARGO_SIN_ACENTOS,
    TITULO_LARGO,
    RESUMEN_SIN_ACENTOS,
    RESUMEN,
    CONTENIDO,
    SUBTITULO_SIN_ACENTOS,
    SUBTITULO,
    ORDEN,
    PRIORIDAD,
    DISTANCIA_SYSDATE,
    FECHA_VIGENCIA,
    ORIGEN_INFORMACION_ID,
    ES_HTML,
    FECHA_CREACION,
    ORDEN_MAPA,
    ENLACE_DESTINO,
    TAGS_SIN_ACENTOS
)
BEQUEATH DEFINER
AS
    SELECT o.id
               contenido_id,
           oi.id
               contenido_idioma_id,
           m.url_completa
               mapa_url_completa,
           LOWER (i.codigo_iso)
               idioma_codigo_iso,
           quita_acentos (oi.titulo)
               titulo_sin_acentos,
           oi.titulo
               titulo,
           quita_acentos (oi.titulo_largo)
               titulo_largo_sin_acentos,
           oi.titulo_largo
               titulo_largo,
           quita_acentos (oi.resumen)
               resumen_sin_acentos,
           oi.resumen
               resumen,
           DECODE (oi.html, 'S', oi.contenido, NULL)
               contenido,
           quita_acentos (oi.subtitulo)
               subtitulo_sin_acentos,
           oi.subtitulo
               subtitulo,
           mo.orden,
           get_prioridad (o.id)
               prioridad,
           ABS (get_inicio_vigencia_contenido (o.id) - SYSDATE)
               distancia_sysdate,
           TRUNC (vo.fecha)
               fecha,
           origen_informacion_id,
           oi.html,
           o.fecha_creacion,
           m.orden,
           oi.enlace_destino,
           quita_acentos(concat_tags(o.id))
      FROM upo_mapas_objetos      mo,
           upo_objetos            o,
           upo_mapas              m,
           upo_objetos_idiomas    oi,
           upo_idiomas            i,
           upo_vigencias_objetos  vo
     WHERE     mo.mapa_id = m.id
           AND mo.objeto_id = o.id
           AND oi.objeto_id = o.id
           AND i.id = oi.idioma_id
           AND mo.estado_moderacion = 'ACEPTADO'
           AND o.visible = 'S'
           AND oi.html = 'S'
           AND vo.objeto_id(+) = o.id;

ALTER TABLE UJI_PORTAL.UPO_MAPAS
ADD (mostrar_tags NUMBER(1) DEFAULT 0 NOT NULL);

ALTER TABLE UJI_PORTAL.UPO_MAPAS
ADD (mostrar_fecha_modificacion NUMBER(1) DEFAULT 1 NOT NULL);

ALTER TABLE UJI_PORTAL.UPO_MAPAS
ADD (mostrar_fuente NUMBER(1) DEFAULT 1 NOT NULL);

ALTER TABLE UJI_PORTAL.UPO_MAPAS
ADD (mostrar_redes_sociales NUMBER(1) DEFAULT 1 NOT NULL);

ALTER TABLE UJI_PORTAL.UPO_MAPAS
ADD (mostrar_inf_proporcionada NUMBER(1) DEFAULT 1 NOT NULL);

ALTER TABLE UJI_PORTAL.UPO_MAPAS
ADD (mostrar_barra_gris NUMBER(1) DEFAULT 1 NOT NULL);

ALTER TABLE UJI_PORTAL.UPO_VALORES_METADATOS
 ADD (orden  NUMBER DEFAULT 1 NOT NULL);


