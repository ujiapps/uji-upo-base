create table upo_tags(id number primary key, tagname varchar2(200))

insert into upo_tags (id, tagname)
select hibernate_sequence.nextval, tagname
  from (select distinct tagname from upo_objetos_tags);
