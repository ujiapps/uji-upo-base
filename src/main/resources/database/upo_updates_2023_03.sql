CREATE TABLE UJI_PORTAL.UPO_MAPAS_TAGS
(
    ID       NUMBER,
    MAPA_ID  NUMBER                               NOT NULL,
    TAG      VARCHAR2(100 BYTE)                   NOT NULL
)

ALTER TABLE UJI_PORTAL.UPO_MAPAS_TAGS ADD (
  FOREIGN KEY (MAPA_ID)
  REFERENCES UJI_PORTAL.UPO_MAPAS (ID)
  ENABLE VALIDATE);

alter table upo_mapas_objetos add (propuesta_por_tag number default 0 not null);


