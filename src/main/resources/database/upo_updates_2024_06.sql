create table upo_vmc_mapas_permisos_admin (persona_id number not null, url varchar2(4000) not null)

declare
v_cuenta number;
   v_num number;

cursor administradores is
select distinct per_id
from upo_franquicias_accesos fa
where fa.tipo = 'ADMINISTRADOR';

cursor urls(p_persona_id number) is
select m4.url_completa
from upo_mapas m4,
     upo_franquicias_accesos  fa4,
     upo_franquicias f4
where m4.franquicia_id = f4.id
  and fa4.franquicia_id = f4.id
  AND fa4.tipo = 'ADMINISTRADOR'
  and m4.id >= 0
  and m4.url_completa not like '/paperera/%'
  and m4.url_completa not like '/old/%'
  and m4.url_completa not like '/test/%'
  and p_persona_id = fa4.per_id
order by url_completa;
begin
    v_cuenta := 0;
    delete upo_vmc_mapas_permisos_admin;

for a in administradores loop

        for u in urls(a.per_id) loop

            if v_cuenta = 0 then
                insert into upo_vmc_mapas_permisos_admin values (a.per_id, u.url_completa);
end if;

select count(*)
into v_num
from upo_vmc_mapas_permisos_admin mp
where persona_id = a.per_id
  and u.url_completa like mp.url || '%';

if v_num = 0 then
                insert into upo_vmc_mapas_permisos_admin values (a.per_id, u.url_completa);
end if;

            v_cuenta := 1;
end loop;

end loop;

commit;

exception
    when others then
        rollback;
        raise_application_error(-20000, 'Error generando permisos de admins en UPO');
end;

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_MODERACION
(ID, MAPA_OBJETO_ID, OBJETO_ID, MAPA_ID, ESTADO_MODERACION,
 PER_ID_MODERACION, PER_ID_PROPUESTA, TEXTO_RECHAZO, OBJETO_URL_PATH, URL_COMPLETA,
 MAPA_URL_COMPLETA, PERSONA_ID, OBJETO_TITULO, URL_COMPLETA_ORIGINAL, CONTIENE_ORIGINALES)
BEQUEATH DEFINER
AS
SELECT mapa_objeto_id || '-' || persona_id,
       mapa_objeto_id,
       objeto_id,
       mapa_id,
       estado_moderacion,
       per_id_moderacion,
       per_id_propuesta,
       texto_rechazo,
       objeto_url_path,
       url_completa,
       mapa_url_completa,
       persona_id,
       (CASE
            WHEN titulo_ca IS NOT NULL THEN titulo_ca
            WHEN titulo_es IS NOT NULL THEN titulo_es
            WHEN titulo_en IS NOT NULL THEN titulo_en
            ELSE NULL
           END)                              titulo,
       (SELECT m.url_completa
        FROM upo_mapas m, upo_mapas_objetos mo, upo_objetos o
        WHERE     o.id = mo.objeto_id
          AND m.id = mo.mapa_id
          AND mo.tipo = 'NORMAL'
          AND o.id = s.objeto_id)    url_completa_original,
       CASE WHEN EXISTS (SELECT 1
                         FROM upo_mapas m, upo_mapas_objetos mo
                         WHERE m.url_completa like mapa_url_completa || '%'
                           AND m.id = mo.mapa_id
                           AND mo.estado_moderacion = 'ACEPTADO'
       ) then 1
            else 0
           END  contiene_originales
FROM (SELECT
          mo.id                                       mapa_objeto_id,
          mo.objeto_id                                objeto_id,
          mo.mapa_id                                  mapa_id,
          mo.estado_moderacion                        estado_moderacion,
          mo.per_id_moderacion                        per_id_moderacion,
          mo.per_id_propuesta                         per_id_propuesta,
          mo.texto_rechazo                            texto_rechazo,
          o.url_path                                  objeto_url_path,
          m.url_completa || o.url_path                url_completa,
          m.url_completa                              mapa_url_completa,
          pa.persona_id                               persona_id,
          (SELECT titulo
           FROM upo_objetos_idiomas oi, upo_idiomas i
           WHERE     oi.objeto_id = o.id
             AND i.id = oi.idioma_id
             AND UPPER (i.codigo_iso) = 'CA')    titulo_ca,
          (SELECT titulo
           FROM upo_objetos_idiomas oi, upo_idiomas i
           WHERE     oi.objeto_id = o.id
             AND i.id = oi.idioma_id
             AND UPPER (i.codigo_iso) = 'ES')    titulo_es,
          (SELECT titulo
           FROM upo_objetos_idiomas oi, upo_idiomas i
           WHERE     oi.objeto_id = o.id
             AND i.id = oi.idioma_id
             AND UPPER (i.codigo_iso) = 'EN')    titulo_en
      FROM upo_mapas_objetos  mo,
           upo_objetos        o,
           upo_mapas          m,
           upo_vmc_mapas_permisos_admin pa
      WHERE     mo.estado_moderacion = 'PENDIENTE'
        AND mo.mapa_id = m.id
        AND mo.objeto_id = o.id
        AND m.url_completa NOT LIKE '/paperera/%'
        AND m.url_completa NOT LIKE '/old/%'
        AND m.url_completa NOT LIKE '/test/%'
        AND m.url_completa LIKE pa.url || '%') s;