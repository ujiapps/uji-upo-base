CREATE OR REPLACE FUNCTION UJI_PORTAL.blob_to_clob(
  p_blob IN BLOB
)
  RETURN CLOB
IS
  BEGIN
    DECLARE
      v_file_clob    CLOB;
      v_file_size    INTEGER := dbms_lob.lobmaxsize;
      v_dest_offset  INTEGER := 1;
      v_src_offset   INTEGER := 1;
      v_blob_csid    NUMBER := dbms_lob.default_csid;
      v_lang_context NUMBER := dbms_lob.default_lang_ctx;
      v_warning      INTEGER;

    BEGIN

      IF p_blob IS NULL
      THEN

        RETURN NULL;

      END IF;

      dbms_lob.createTemporary(v_file_clob, TRUE);
      dbms_lob.convertToClob(v_file_clob, p_blob, v_file_size, v_dest_offset, v_src_offset, v_blob_csid, v_lang_context,
                             v_warning);
      RETURN v_file_clob;
      EXCEPTION
      WHEN OTHERS THEN
      RETURN NULL;

    END;
  END;
/


CREATE OR REPLACE FUNCTION UJI_PORTAL.quita_acentos(
  pTxt IN VARCHAR2
)
  RETURN VARCHAR2 IS
  vResult VARCHAR2(4000);
  BEGIN

    IF pTxt IS NULL
    THEN

      RETURN NULL;

    ELSE

      RETURN translate(lower(pTxt), 'áéíóúàèìòùäëïöüâêîôû', 'aeiouaeiouaeiouaeiou');

    END IF;
  END;
/


CREATE OR REPLACE FUNCTION UJI_PORTAL.quita_acentos_clob(
  pTxt IN CLOB
)
  RETURN CLOB IS
  vResult   CLOB;
  l_offset  PLS_INTEGER := 1;
  l_segment VARCHAR2(4000);
  BEGIN

    IF pTxt IS NULL
    THEN

      RETURN NULL;

    ELSE

      WHILE l_offset < DBMS_LOB.GETLENGTH(pTxt) LOOP

        l_segment := DBMS_LOB.SUBSTR(pTxt, 3000, l_offset);

        l_segment := quita_acentos(l_segment);

        vResult := vResult || l_segment;

        l_offset := l_offset + 3000;

      END LOOP;

      RETURN vResult;

    END IF;
  END;
/

create or replace force view UJI_PORTAL.UPO_VW_BUSQUEDA(CONTENIDO_IDIOMA_ID, MAPA_URL_COMPLETA, IDIOMA_CODIGO_ISO, TITULO_SIN_ACENTOS, TITULO_LARGO_SIN_ACENTOS, RESUMEN_SIN_ACENTOS, CONTENIDO_SIN_ACENTOS, SUBTITULO_SIN_ACENTOS, ORDEN,
    PRIORIDAD, DISTANCIA_SYSDATE, FECHA_VIGENCIA) as
  select oi.id contenido_idioma_id, m.url_completa mapa_url_completa, lower(i.codigo_iso) idioma_codigo_iso, quita_acentos(oi.titulo) titulo_sin_acentos, quita_acentos(oi.titulo_largo) titulo_largo_sin_acentos,
         quita_acentos(oi.resumen) resumen_sin_acentos, quita_acentos_clob(blob_to_clob(decode(oi.html, 'S', oi.contenido, null))) contenido_sin_acentos, --decode(oi.html, 'S', oi.contenido, null) contenido_sin_acentos,
         quita_acentos(oi.subtitulo) subtitulo_sin_acentos, mo.orden,
         get_prioridad(o.id) prioridad, abs(get_inicio_vigencia_contenido(o.id) - sysdate) distancia_sysdate, trunc(vo.fecha) fecha
  from upo_mapas_objetos mo, upo_objetos o, upo_mapas m, upo_objetos_idiomas oi, upo_idiomas i, upo_vigencias_objetos vo
  where mo.mapa_id = m.id
        and mo.objeto_id = o.id
        and oi.objeto_id = o.id
        and i.id = oi.idioma_id
        and ((mo.tipo = 'NORMAL')
             or (mo.tipo = 'LINK'
                 and mo.estado_moderacion = 'ACEPTADO'))
        and o.visible = 'S'
        and oi.html = 'S'
        and vo.objeto_id(+) = o.id;


CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_BUSQUEDA_SIN_FILTRAR(CONTENIDO_IDIOMA_ID, MAPA_URL_COMPLETA, IDIOMA_CODIGO_ISO, TITULO, TITULO_LARGO, RESUMEN, CONTENIDO, SUBTITULO, ORDEN,
    PRIORIDAD, DISTANCIA_SYSDATE, FECHA_VIGENCIA) AS
  SELECT
    oi.id                                              contenido_idioma_id,
    m.url_completa                                     mapa_url_completa,
    lower(i.codigo_iso)                                idioma_codigo_iso,
    oi.titulo                                          titulo,
    oi.titulo_largo                                    titulo_largo,
    oi.resumen                                         resumen,
    decode(oi.html, 'S', oi.contenido, NULL)           contenido,
    oi.subtitulo                                       subtitulo,
    mo.orden,
    get_prioridad(o.id)                                prioridad,
    abs(get_inicio_vigencia_contenido(o.id) - sysdate) distancia_sysdate,
    trunc(vo.fecha)                                    fecha
  FROM upo_mapas_objetos mo, upo_objetos o, upo_mapas m, upo_objetos_idiomas oi, upo_idiomas i, upo_vigencias_objetos vo
  WHERE mo.mapa_id = m.id
        AND mo.objeto_id = o.id
        AND oi.objeto_id = o.id
        AND i.id = oi.idioma_id
        AND ((mo.tipo = 'NORMAL')
             OR (mo.tipo = 'LINK'
                 AND mo.estado_moderacion = 'ACEPTADO'))
        AND o.visible = 'S'
        AND oi.html = 'S'
        AND vo.objeto_id (+) = o.id;


alter table upo_mapas add (url_num_niveles number);


CREATE OR REPLACE TRIGGER UJI_PORTAL.upo_mapas_num_niveles_tr
BEFORE INSERT OR UPDATE
OF URL_COMPLETA
ON UJI_PORTAL.UPO_MAPAS
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
  BEGIN

    :new.url_num_niveles := times_value_in_string(:new.url_completa, '/');

  END ;
/

update upo_mapas set url_num_niveles = nvl(times_value_in_string(url_completa, '/'),1);


alter table upo_mapas modify (url_num_niveles number not null)


CREATE INDEX UJI_PORTAL.upo_mapas_fk5x ON UJI_PORTAL.UPO_MAPAS
(URL_NUM_NIVELES)
LOGGING
NOPARALLEL;






create or replace force view UJI_PORTAL.UPO_VW_PUBLICACION(CONTENIDO_ID, CONTENIDO_IDIOMA_ID, URL_NODO, URL_NODO_ORIGINAL, URL_CONTENIDO, URL_COMPLETA, URL_COMPLETA_ORIGINAL, URL_NUM_NIVELES, URL_DESTINO, NIVEL, IDIOMA, TITULO, SUBTITULO,
    TITULO_LARGO, RESUMEN, MIME_TYPE, ES_HTML, CONTENIDO, FECHA_MODIFICACION, NODO_MAPA_ORDEN, NODO_MAPA_PADRE_ORDEN, CONTENIDO_ORDEN, TIPO_CONTENIDO, AUTOR, ORIGEN_NOMBRE, ORIGEN_URL, LUGAR, PROXIMA_FECHA_VIGENCIA, PRIMERA_FECHA_VIGENCIA,
    METADATA, TITULO_NODO, ORDEN, PRIORIDAD, DISTANCIA_SYSDATE, TIPO_FORMATO, ATRIBUTOS, VISIBLE, TEXTO_NOVISIBLE) as
  select o.id contenido_id, oi.id contenido_idioma_id, m.url_completa url_nodo, m.url_completa url_nodo_original, o.url_path url_contenido, m.url_completa || o.url_path url_completa, m.url_completa || o.url_path url_completa_original,
                           url_num_niveles niveles, oi.enlace_destino, nvl(mp.nivel, 1), i.codigo_iso idioma, oi.titulo, oi.subtitulo, oi.titulo_largo, oi.resumen, oi.mime_type, oi.html es_html, decode(oi.html, 'S', oi.contenido, null) contenido,
    oi.fecha_modificacion, m.orden nodo_mapa_orden, nvl((select mpadre.orden
                                                         from upo_mapas mpadre
                                                         where m.mapa_id = mpadre.id),
                                                        -1)
    orden_padre, mo.orden contenido_orden, 'NORMAL' tipo_contenido, (select nvl(otros_autores,
                                                                                (select upper(nombre || ' ' || apellido1 || ' ' || apellido2)
                                                                                 from upo_ext_personas
                                                                                 where id = per_id_responsable))
                                                                     from upo_objetos
                                                                     where id = o.id)
    autor, oin.nombre origen_nombre, oin.url origen_url, o.lugar,
                           get_proxima_fecha_vigencia(o.id) proxima_fecha_vigencia, get_primera_fecha_vigencia(o.id) primera_fecha_vigencia, concat_metadata(o.id, i.codigo_iso) metadata,
                           decode(i.codigo_iso, 'es', m.titulo_publicacion_es, 'en', m.titulo_publicacion_en, m.titulo_publicacion_ca) tituloNodo, mo.orden, get_prioridad(o.id) prioridad, abs(get_inicio_vigencia_contenido(o.id) - sysdate) distancia_sysdate,
    oi.tipo_formato, concat_atributos(oi.id),
    o.visible, o.texto_novisible
  from upo_mapas m, upo_mapas_plantillas mp, upo_mapas_objetos mo, upo_objetos o, upo_objetos_idiomas oi, upo_idiomas i, upo_origenes_informacion oin
  where m.id = mo.mapa_id
        and m.id = mp.mapa_id(+)
        and o.id = mo.objeto_id
        and o.id = oi.objeto_id
        and i.id = oi.idioma_id
        and mo.tipo = 'NORMAL'
        and o.origen_informacion_id = oin.id(+)
  union all
  select o.id contenido_id, oi.id contenido_idioma_id, m.url_completa url_nodo, (select xm.url_completa
                                                                                 from upo_mapas xm, upo_mapas_objetos xmo
                                                                                 where xm.id = xmo.mapa_id
                                                                                       and o.id = xmo.objeto_id
                                                                                       and xmo.tipo = 'NORMAL')
              url_nodo_original, o.url_path url_contenido, m.url_completa || o.url_path url_completa, (select xm.url_completa || o.url_path
                                                                                                       from upo_mapas xm, upo_mapas_objetos xmo
                                                                                                       where xm.id = xmo.mapa_id
                                                                                                             and o.id = xmo.objeto_id
                                                                                                             and xmo.tipo = 'NORMAL')
                                                      url_completa_original, url_num_niveles niveles,
    oi.enlace_destino, nvl(mp.nivel, 1), i.codigo_iso idioma, oi.titulo, oi.subtitulo, oi.titulo_largo, oi.resumen, oi.mime_type, oi.html es_html, decode(oi.html, 'S', oi.contenido, null) contenido, oi.fecha_modificacion,
    m.orden nodo_mapa_orden, nvl((select mpadre.orden
                                  from upo_mapas mpadre
                                  where m.mapa_id = mpadre.id),
                                 -1)
                                                      orden_padre, mo.orden contenido_orden, 'LINK' tipo_contenido, (select nvl(otros_autores,
                                                                                                                                (select upper(nombre || ' ' || apellido1 || ' ' || apellido2)
                                                                                                                                 from upo_ext_personas
                                                                                                                                 where id = per_id_responsable))
                                                                                                                     from upo_objetos
                                                                                                                     where id = o.id)
                                                      autor, oin.nombre origen_nombre, oin.url origen_url, o.lugar, get_proxima_fecha_vigencia(o.id) proxima_fecha_vigencia,
    get_primera_fecha_vigencia(o.id) primera_fecha_vigencia, concat_metadata(o.id, i.codigo_iso) metadata, decode(i.codigo_iso, 'es', m.titulo_publicacion_es, 'en', m.titulo_publicacion_en, m.titulo_publicacion_ca) tituloNodo,
    mo.orden, get_prioridad(o.id) prioridad, abs(get_inicio_vigencia_contenido(o.id) - sysdate) distancia_sysdate, oi.tipo_formato, concat_atributos(oi.id),
    o.visible, o.texto_novisible
  from upo_mapas m, upo_mapas_plantillas mp, upo_mapas_objetos mo, upo_objetos o, upo_objetos_idiomas oi, upo_idiomas i, upo_origenes_informacion oin
  where m.id = mo.mapa_id
        and m.id = mp.mapa_id(+)
        and o.id = mo.objeto_id
        and o.id = oi.objeto_id
        and i.id = oi.idioma_id
        and mo.tipo = 'LINK'
        and o.origen_informacion_id = oin.id(+)
        and mo.estado_moderacion = 'ACEPTADO';



