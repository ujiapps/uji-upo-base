CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_PUBLICACION
(
    CONTENIDO_ID
  , CONTENIDO_IDIOMA_ID
  , URL_NODO
  , URL_NODO_ORIGINAL
  , URL_CONTENIDO
  , URL_COMPLETA
  , URL_COMPLETA_ORIGINAL
  , URL_NUM_NIVELES
  , URL_DESTINO
  , NIVEL
  , IDIOMA
  , TITULO
  , SUBTITULO
  , TITULO_LARGO
  , RESUMEN
  , MIME_TYPE
  , ES_HTML
  , CONTENIDO
  , FECHA_MODIFICACION
  , NODO_MAPA_ORDEN
  , CONTENIDO_ORDEN
  , TIPO_CONTENIDO
  , AUTOR
  , ORIGEN_NOMBRE
  , ORIGEN_URL
  , LUGAR
  , PROXIMA_FECHA_VIGENCIA
  , PRIMERA_FECHA_VIGENCIA
  , primera_fecha_vigencia_orden
) AS
  SELECT
    o.id                                       contenido_id,
    oi.id                                      contenido_idioma_id,
    m.url_completa                             url_nodo,
    m.url_completa                             url_nodo_original,
    o.url_path                                 url_contenido,
    m.url_completa || o.url_path               url_completa,
    m.url_completa || o.url_path               url_completa_original,
    times_value_in_string(m.url_completa, '/') niveles,
    oi.enlace_destino,
    nvl(mp.nivel, 1),
    i.codigo_iso                               idioma,
    oi.titulo,
    oi.subtitulo,
    oi.titulo_largo,
    oi.resumen,
    oi.mime_type,
    oi.html                                    es_html,
    decode(oi.html, 'S', oi.contenido, null)   contenido,
    oi.fecha_modificacion,
    m.orden                                    nodo_mapa_orden,
    mo.orden                                   contenido_orden,
    'NORMAL'                                   tipo_contenido,
    (SELECT
    nvl(otros_autores, (SELECT
                          upper(nombre || ' ' || apellido1 || ' ' || apellido2)
                        FROM upo_ext_personas
                        WHERE id = per_id_responsable))
     FROM upo_objetos
     WHERE id = o.id)
                                                              autor,
    oin.nombre                                                origen_nombre,
    oin.url                                                   origen_url,
    o.lugar,
    get_proxima_fecha_vigencia(o.id)                          proxima_fecha_vigencia,
    get_primera_fecha_vigencia(o.id)                          primera_fecha_vigencia,
    nvl(get_inicio_vigencia_contenido(o.id), sysdate - 10000) primera_fecha_vigencia_orden
  FROM upo_mapas m
    , upo_mapas_plantillas mp
    , upo_mapas_objetos mo
    , upo_objetos o
    , upo_objetos_idiomas oi
    , upo_idiomas i
    , upo_origenes_informacion oin
  WHERE m.id = mo.mapa_id
        AND m.id = mp.mapa_id (+)
        AND o.id = mo.objeto_id
        AND o.id = oi.objeto_id
        AND i.id = oi.idioma_id
        AND mo.tipo = 'NORMAL'
        AND o.origen_informacion_id = oin.id (+)
        AND o.visible = 'S'
  UNION ALL
  SELECT
    o.id           contenido_id,
    oi.id          contenido_idioma_id,
    m.url_completa url_nodo,
    (SELECT
    xm.url_completa
     FROM upo_mapas xm, upo_mapas_objetos xmo
     WHERE xm.id = xmo.mapa_id
           AND o.id = xmo.objeto_id
           AND xmo.tipo = 'NORMAL')
                                 url_nodo_original,
    o.url_path                   url_contenido,
    m.url_completa || o.url_path url_completa,
    (SELECT
    xm.url_completa || o.url_path
     FROM upo_mapas xm, upo_mapas_objetos xmo
     WHERE xm.id = xmo.mapa_id
           AND o.id = xmo.objeto_id
           AND xmo.tipo = 'NORMAL')
                                               url_completa_original,
    times_value_in_string(m.url_completa, '/') niveles,
    oi.enlace_destino,
    nvl(mp.nivel, 1),
    i.codigo_iso                               idioma,
    oi.titulo,
    oi.subtitulo,
    oi.titulo_largo,
    oi.resumen,
    oi.mime_type,
    oi.html                                    es_html,
    decode(oi.html, 'S', oi.contenido, null)   contenido,
    oi.fecha_modificacion,
    m.orden                                    nodo_mapa_orden,
    mo.orden                                   contenido_orden,
    'LINK'                                     tipo_contenido,
    (SELECT
    nvl(otros_autores, (SELECT
                          upper(nombre || ' ' || apellido1 || ' ' || apellido2)
                        FROM upo_ext_personas
                        WHERE id = per_id_responsable))
     FROM upo_objetos
     WHERE id = o.id)
                                                              autor,
    oin.nombre                                                origen_nombre,
    oin.url                                                   origen_url,
    o.lugar,
    get_proxima_fecha_vigencia(o.id)                          proxima_fecha_vigencia,
    get_primera_fecha_vigencia(o.id)                          primera_fecha_vigencia,
    nvl(get_inicio_vigencia_contenido(o.id), sysdate - 10000) primera_fecha_vigencia_orden
  FROM upo_mapas m
    , upo_mapas_plantillas mp
    , upo_mapas_objetos mo
    , upo_objetos o
    , upo_objetos_idiomas oi
    , upo_idiomas i
    , upo_origenes_informacion oin
  WHERE m.id = mo.mapa_id
        AND m.id = mp.mapa_id (+)
        AND o.id = mo.objeto_id
        AND o.id = oi.objeto_id
        AND i.id = oi.idioma_id
        AND mo.tipo = 'LINK'
        AND o.origen_informacion_id = oin.id (+)
        AND o.visible = 'S'
        AND mo.estado_moderacion = 'ACEPTADO';

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_PUBLICACION_BINARIOS
(
    URL_COMPLETA
  , IDIOMA
  , MIME_TYPE
  , CONTENIDO
  , URL_NODO
) AS
  SELECT
    m.url_completa || o.url_path url_completa,
    i.codigo_iso                 idioma,
    oi.mime_type,
    oi.contenido,
    m.url_completa               url_nodo
  FROM upo_mapas m
    , upo_mapas_plantillas mp
    , upo_mapas_objetos mo
    , upo_objetos o
    , upo_objetos_idiomas oi
    , upo_idiomas i
    , upo_origenes_informacion oin
  WHERE m.id = mo.mapa_id
        AND m.id = mp.mapa_id (+)
        AND o.id = mo.objeto_id
        AND o.id = oi.objeto_id
        AND i.id = oi.idioma_id
        AND ((mo.tipo = 'NORMAL')
             OR
             (mo.tipo = 'LINK' AND mo.estado_moderacion = 'ACEPTADO'))
        AND nvl(oi.html, 'N') = 'N'
        AND o.origen_informacion_id = oin.id (+)
        AND o.visible = 'S';


CREATE OR REPLACE FUNCTION UJI_PORTAL.has_tag(p_objeto_id NUMBER, p_tag VARCHAR2)
  RETURN VARCHAR2 IS
  CURSOR tags IS
    SELECT
      *
    FROM upo_objetos_tags
    WHERE objeto_id = p_objeto_id;

  BEGIN

    FOR t IN tags LOOP
    IF trim(upper(t.tagname)) = trim(upper(p_tag))
    THEN
      RETURN 'S';
    END IF;
    END LOOP;

    RETURN 'N';
  END;
/


CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_SYNC_CONTENIDOS
(
    OBJETO_ID
  , URL_NODO
  , ORDEN
  , PRIORIDAD
  , DISTANCIA_SYSDATE
) AS
  SELECT
    o.id,
    m.url_completa,
    mo.orden,
    nvl((SELECT
    decode(nvl(prioridad, 'NORMAL')
    , 'URGENTE'
    , 1
    , 'ALTA'
    , 2
    , 'NORMAL'
    , 3
    , 'BAJA'
    , 4)
         FROM upo_objetos_prioridades
         WHERE objeto_id = o.id
               AND sysdate BETWEEN to_date(to_char(nvl(fecha_inicio, sysdate - 1), 'dd/mm/yyyy') || ' ' ||
                                           to_char(nvl(hora_inicio, sysdate), 'hh24:mi:ss')
         , 'dd/mm/yyyy hh24:mi:ss')
         AND to_date(
             to_char(nvl(fecha_fin, sysdate + 1), 'dd/mm/yyyy') || ' ' || to_char(nvl(hora_fin, sysdate), 'hh24:mi:ss'),
             'dd/mm/yyyy hh24:mi:ss')
               AND rownum = 1)
    , 3)
                                                       prioridad,
    abs(get_inicio_vigencia_contenido(o.id) - sysdate) distancia_sysdate
  FROM upo_mapas m, upo_mapas_objetos mo, upo_objetos o
  WHERE m.id = mo.mapa_id
        AND o.id = mo.objeto_id -- cualquier contenido visible
        AND ((mo.tipo = 'NORMAL')
             OR (mo.tipo = 'LINK'
                 AND mo.estado_moderacion = 'ACEPTADO'))
        AND o.visible = 'S'
        AND (-- no hay vigencia definida
             NOT exists(SELECT
                          id
                        FROM upo_vigencias_objetos vo
                        WHERE vo.objeto_id = o.id)
             OR -- vigencia de 72 horas y dentro de rango de fechas
             exists
             (SELECT
                id
              FROM upo_vigencias_objetos vo
              WHERE vo.objeto_id = o.id
                    AND (
                to_date(to_char(fecha, 'dd/mm/yyyy') || ' ' || to_char(nvl(hora_inicio, sysdate), 'hh24:mi:ss'),
                        'dd/mm/yyyy hh24:mi:ss') BETWEEN sysdate AND sysdate + 3
                OR to_date(to_char(fecha, 'dd/mm/yyyy') || ' ' || to_char(nvl(hora_fin, sysdate), 'hh24:mi:ss'),
                           'dd/mm/yyyy hh24:mi:ss') BETWEEN sysdate AND sysdate + 3)))
        -- Obligatorio en CA y ES
        AND (
    (exists(SELECT
              i.codigo_iso
            FROM upo_objetos_idiomas oi, upo_idiomas i
            WHERE i.id = oi.idioma_id
                  AND lower(i.codigo_iso) = 'ca'
                  AND o.id = oi.objeto_id
                  AND nvl(oi.html, 'N') = 'S'
     )
     AND exists(SELECT
                  i.codigo_iso
                FROM upo_objetos_idiomas oi, upo_idiomas i
                WHERE i.id = oi.idioma_id
                      AND lower(i.codigo_iso) = 'es'
                      AND o.id = oi.objeto_id
                      AND nvl(oi.html, 'N') = 'S'
    )
    )
    OR
    (
      has_tag(o.id, 'megabanner') = 'S'
    )
  );

CREATE OR REPLACE FUNCTION UJI_PORTAL.quita_acentos(
  pTxt IN CLOB
)
  RETURN CLOB IS
  vResult   CLOB;
  l_offset  PLS_INTEGER := 1;
  l_segment VARCHAR2(4000);
  BEGIN

    IF pTxt IS null
    THEN

      RETURN null;

    ELSE

      WHILE l_offset < DBMS_LOB.GETLENGTH(pTxt) LOOP

        l_segment := DBMS_LOB.SUBSTR(pTxt, 3000, l_offset);

        l_segment := translate(lower(l_segment), 'áéíóúàèìòùäëïöüâêîôû', 'aeiouaeiouaeiouaeiou');

        vResult := vResult || l_segment;

        l_offset := l_offset + 3000;

      END LOOP;

      RETURN vResult;

    END IF;
  END;
/


CREATE OR REPLACE FUNCTION blob_to_clob(
  p_blob IN BLOB
)
  RETURN CLOB
IS
  BEGIN
    DECLARE
      v_file_clob    CLOB;
      v_file_size    INTEGER := dbms_lob.lobmaxsize;
      v_dest_offset  INTEGER := 1;
      v_src_offset   INTEGER := 1;
      v_blob_csid    NUMBER := dbms_lob.default_csid;
      v_lang_context NUMBER := dbms_lob.default_lang_ctx;
      v_warning      INTEGER;

    BEGIN

      IF p_blob IS null
      THEN

        RETURN null;

      END IF;

      dbms_lob.createTemporary(v_file_clob, TRUE);
      dbms_lob.convertToClob(v_file_clob, p_blob, v_file_size, v_dest_offset, v_src_offset, v_blob_csid, v_lang_context,
                             v_warning);
      RETURN v_file_clob;
    END;
  END;


CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_BUSQUEDA
(
    contenido_idioma_id,
    mapa_url_completa,
    idioma_codigo_iso,
    titulo_sin_acentos,
    titulo_largo_sin_acentos,
    resumen_sin_acentos,
    contenido_sin_acentos,
    subtitulo_sin_acentos,
    primera_fecha_vigencia,
    ultima_fecha_vigencia
) AS
  SELECT
    oi.id                                                                 contenido_idioma_id,
    m.url_completa                                                        mapa_url_completa,
    lower(i.codigo_iso)                                                   idioma_codigo_iso,
    quita_acentos(oi.titulo)                                              titulo_sin_acentos,
    quita_acentos(oi.titulo_largo)                                        titulo_largo_sin_acentos,
    quita_acentos(oi.resumen)                                             resumen_sin_acentos,
    quita_acentos(blob_to_clob(decode(oi.html, 'S', oi.contenido, null))) contenido_sin_acentos,
    quita_acentos(oi.subtitulo)                                           subtitulo_sin_acentos,
    (SELECT
    min(trunc(fecha))
     FROM upo_vigencias_objetos
     WHERE objeto_id = o.id)                                              primera_fecha_vigencia,
    (SELECT
    max(trunc(fecha))
     FROM upo_vigencias_objetos
     WHERE objeto_id = o.id)                                              ultima_fecha_vigencia
  FROM upo_mapas_objetos mo
    , upo_objetos o
    , upo_mapas m
    , upo_objetos_idiomas oi
    , upo_idiomas i
  WHERE mo.mapa_id = m.id
        AND mo.objeto_id = o.id
        AND oi.objeto_id = o.id
        AND i.id = oi.idioma_id
        AND ((mo.tipo = 'NORMAL')
             OR (mo.tipo = 'LINK'
                 AND mo.estado_moderacion = 'ACEPTADO'
  )
  )
        AND o.visible = 'S'
        AND oi.html = 'S';


CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_BUSQUEDA_URL_VIGENCIA(MAPA_URL_COMPLETA, PRIMERA_FECHA_VIGENCIA_ORDEN, IDIOMA_CODIGO_ISO) AS
  SELECT
    b.MAPA_URL_COMPLETA,
    (SELECT
    min(nvl(b2.primera_fecha_vigencia, sysdate - 10000))
     FROM UPO_VW_BUSQUEDA b2
     WHERE b2.mapa_url_completa = b.mapa_url_completa)
                             primera_fecha_vigencia_orden,
    lower(idioma_codigo_iso) idioma_codigo_iso
  FROM UPO_VW_BUSQUEDA b;


ALTER TABLE upo_mapas RENAME COLUMN titulo_publicacion TO titulo_publicacion_ca;

ALTER TABLE upo_mapas ADD (titulo_publicacion_es VARCHAR2(4000), titulo_publicacion_en VARCHAR2(4000));

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_MODERACION
(
    ID
  , MAPA_OBJETO_ID
  , OBJETO_ID
  , MAPA_ID
  , ESTADO_MODERACION
  , PER_ID_MODERACION
  , PER_ID_PROPUESTA
  , TEXTO_RECHAZO
  , OBJETO_URL_PATH
  , URL_COMPLETA
  , MAPA_URL_COMPLETA
  , PERSONA_ID
  , OBJETO_TITULO
) AS
  SELECT
    mapa_objeto_id || '-' || persona_id,
    mapa_objeto_id,
    objeto_id,
    mapa_id,
    estado_moderacion,
    per_id_moderacion,
    per_id_propuesta,
    texto_rechazo,
    objeto_url_path,
    url_completa,
    mapa_url_completa,
    persona_id,
    (CASE
     WHEN titulo_ca IS NOT null THEN titulo_ca
     WHEN titulo_es IS NOT null THEN titulo_es
     WHEN titulo_en IS NOT null THEN titulo_en
     ELSE null
     END)
      titulo
  FROM (SELECT
          DISTINCT mo.id                        mapa_objeto_id,
                   mo.objeto_id                 objeto_id,
                   mo.mapa_id                   mapa_id,
                   mo.estado_moderacion         estado_moderacion,
                   mo.per_id_moderacion         per_id_moderacion,
                   mo.per_id_propuesta          per_id_propuesta,
                   mo.texto_rechazo             texto_rechazo,
                   o.url_path                   objeto_url_path,
                   m.url_completa || o.url_path url_completa,
                   m.url_completa               mapa_url_completa,
                   p.id                         persona_id,
                   (SELECT
                   titulo
                    FROM upo_objetos_idiomas oi, upo_idiomas i
                    WHERE oi.objeto_id = o.id
                          AND i.id = oi.idioma_id
                          AND upper(i.codigo_iso) = 'CA')
                                                titulo_ca,
                   (SELECT
                   titulo
                    FROM upo_objetos_idiomas oi, upo_idiomas i
                    WHERE oi.objeto_id = o.id
                          AND i.id = oi.idioma_id
                          AND upper(i.codigo_iso) = 'ES')
                                                titulo_es,
                   (SELECT
                   titulo
                    FROM upo_objetos_idiomas oi, upo_idiomas i
                    WHERE oi.objeto_id = o.id
                          AND i.id = oi.idioma_id
                          AND upper(i.codigo_iso) = 'EN')
                                                titulo_en
        FROM upo_mapas_objetos mo
          , upo_objetos o
          , upo_mapas m
          , upo_ext_personas p
        WHERE mo.tipo = 'LINK'
              AND mo.mapa_id = m.id
              AND mo.objeto_id = o.id
              AND p.id IN (SELECT
                             DISTINCT per_id
                           FROM upo_franquicias_accesos fa, upo_franquicias f, upo_mapas m2
                           WHERE m2.franquicia_id = f.id
                                 AND fa.franquicia_id = f.id
                                 AND fa.tipo = 'ADMINISTRADOR'
                                 AND m.url_completa LIKE m2.url_completa || '%'));


ALTER TABLE upo_ext_personas ADD email VARCHAR2(4000);

ALTER TABLE upo_franquicias ADD (otros_autores VARCHAR2(4000));

ALTER TABLE upo_franquicias ADD (origenes_informacion_id NUMBER);

ALTER TABLE uji_portal.upo_franquicias ADD
CONSTRAINT upo_franquicias_r01
FOREIGN KEY (origenes_informacion_id)
REFERENCES uji_portal.upo_origenes_informacion (id)

