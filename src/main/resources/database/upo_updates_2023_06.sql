alter table upo_mapas_objetos drop (propuesta_por_tag);

alter table upo_mapas add (propuesta_mapa_id number)

create table upo_mapas_propuesta_tags
(
    id      number primary key not null,
    mapa_id number             not null references upo_mapas (id),
    tagname varchar2(200) not null
)

alter table upo_mapas add (descendientes_en_propuesta number(1) default 0 not null)

alter table uji_portal.upo_mapas_tags
    modify(tag varchar2(200 byte));

alter table uji_portal.upo_mapas_tags
    rename column tag to tagname;