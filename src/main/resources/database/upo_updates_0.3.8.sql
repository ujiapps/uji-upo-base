ALTER TABLE upo_mapas ADD (mostrar_reloj NUMBER(1) DEFAULT 0 NOT NULL, mostrar_menu_perfiles_plegado NUMBER(1) DEFAULT 1 NOT NULL);

CREATE OR REPLACE FUNCTION UJI_PORTAL.times_value_in_string(vString VARCHAR2, vValue VARCHAR2)
  RETURN INT AS
  vLength INT;
  BEGIN
    SELECT length(vString) - length(translate(vString, ' ' || vValue, ' '))
    INTO vLength
    FROM dual;

    RETURN nvl(vLength, 1);
  END;
/

CREATE OR REPLACE VIEW uji_portal.UPO_VW_MIGRACION AS
  SELECT   DISTINCT m.url
  FROM   xpfdm.xpf_paginas_url pu, xpfdm.xp$migracion m
  WHERE   pu.url = m.url
          AND   m.url NOT LIKE '%/9/' ;

CREATE OR REPLACE FUNCTION UJI_PORTAL.concat_metadata(v_objeto_id NUMBER, v_idioma_iso VARCHAR2)
  RETURN CLOB

IS
  vRdo   CLOB;
  amount INTEGER := 3;
  len    NUMBER;

  CURSOR metadatos IS
    SELECT decode(v_idioma_iso, 'ca', clave || '@@@' || nombre_clave_ca || '@@@' || valor_ca,
                  'es', clave || '@@@' || nombre_clave_es || '@@@' || valor_es,
                  'en', clave || '@@@' || nombre_clave_en || '@@@' || valor_en) || '~~~' result
    FROM (SELECT
            om.clave,
            om.nombre_clave_ca,
            om.nombre_clave_es,
            om.nombre_clave_en,
            om.valor_ca,
            om.valor_es,
            om.valor_en
          FROM upo_objetos_metadatos om
          WHERE objeto_id = v_objeto_id
                AND ((v_idioma_iso = 'ca' AND valor_ca IS NOT NULL AND atributo_id IS NULL)
                     OR
                     (v_idioma_iso = 'es' AND valor_es IS NOT NULL AND atributo_id IS NULL)
                     OR
                     (v_idioma_iso = 'en' AND valor_en IS NOT NULL AND atributo_id IS NULL)
                )
          UNION
          SELECT
            am.clave,
            am.nombre_clave_ca,
            am.nombre_clave_es,
            am.nombre_clave_en,
            nvl((SELECT valor_ca
                 FROM upo_valores_metadatos
                 WHERE atributo_id = am.id AND to_char(id) = om.valor_ca), om.valor_ca) valor_ca,
            nvl((SELECT valor_es
                 FROM upo_valores_metadatos
                 WHERE atributo_id = am.id AND to_char(id) = om.valor_es), om.valor_es) valor_es,
            nvl((SELECT valor_en
                 FROM upo_valores_metadatos
                 WHERE atributo_id = am.id AND to_char(id) = om.valor_en), om.valor_en) valor_en
          FROM upo_objetos_metadatos om, upo_atributos_metadatos am
          WHERE om.objeto_id = v_objeto_id
                AND om.atributo_id = am.id
                AND am.publicable = 1
                AND ((v_idioma_iso = 'ca' AND om.valor_ca IS NOT NULL)
                     OR
                     (v_idioma_iso = 'es' AND om.valor_es IS NOT NULL)
                     OR
                     (v_idioma_iso = 'en' AND om.valor_en IS NOT NULL)
                )
          UNION
          SELECT
            'esquema',
            'esquema',
            'esquema',
            'esquema',
            lower(em.nombre),
            lower(em.nombre),
            lower(em.nombre)
          FROM upo_objetos_metadatos om, upo_atributos_metadatos am, upo_esquemas_metadatos em
          WHERE om.objeto_id = v_objeto_id
                AND om.atributo_id = am.id
                AND em.id = am.esquema_id

    );

  BEGIN

    dbms_lob.createtemporary(vRdo, TRUE);

    FOR m IN metadatos LOOP

      dbms_lob.writeappend(vRdo, length(m.result), m.result);

    END LOOP;

    len := dbms_lob.getlength(vRdo);

    IF (len = 0)
    THEN

      RETURN NULL;

    END IF;

    IF (len > amount)
    THEN

      dbms_lob.erase(vRdo, amount, len - amount + 1);
      dbms_lob.trim(vRdo, len - 3);

    END IF;

    RETURN vRdo;
  END;
/


CREATE OR REPLACE FUNCTION UJI_PORTAL.concat_atributos(v_objeto_idioma_id NUMBER)
  RETURN CLOB
IS
  vRdo   CLOB;
  amount INTEGER := 3;
  len    NUMBER;

  CURSOR atributos IS
    SELECT clave || '@@@' || valor || '~~~' result
    FROM upo_objetos_idiomas_atributos oia
    WHERE oia.objeto_idioma_id = v_objeto_idioma_id;

  BEGIN

    dbms_lob.createtemporary(vRdo, TRUE);

    FOR m IN atributos LOOP

      dbms_lob.writeappend(vRdo, length(m.result), m.result);

    END LOOP;

    len := dbms_lob.getlength(vRdo);

    IF (len = 0)
    THEN

      RETURN NULL;

    END IF;

    IF (len > amount)
    THEN

      dbms_lob.erase(vRdo, amount, len - amount + 1);
      dbms_lob.trim(vRdo, len - 3);

    END IF;

    RETURN vRdo;
  END;
/


CREATE OR REPLACE PROCEDURE borra_nodo(pUrl VARCHAR2) IS
  CURSOR nodos IS
    SELECT m.id
    FROM upo_mapas m
    WHERE m.url_completa LIKE pUrl || '%'
    ORDER BY length(m.url_completa) DESC;

  CURSOR objetos(pNodoId NUMBER) IS
    SELECT o.*
    FROM upo_objetos o, upo_mapas_objetos mo
    WHERE mo.objeto_id = o.id
          AND mo.mapa_id = pNodoId
          AND lower(mo.tipo) = 'normal';

  CURSOR objetos_idiomas(pObjetoId NUMBER) IS
    SELECT *
    FROM upo_objetos_idiomas oi
    WHERE oi.objeto_id = pObjetoId;
  BEGIN
    FOR n IN nodos LOOP
      FOR o IN objetos(n.id) LOOP
        FOR oi IN objetos_idiomas(o.id) LOOP
          INSERT INTO upo_objetos_idiomas_log (ID, OBJETO_ID, IDIOMA_ID, TITULO, TITULO_LARGO, CONTENIDO, RESUMEN, MIME_TYPE, FECHA_OPERACION, TIPO_OPERACION, USUARIO_OPERACION, NOMBRE_FICHERO, HTML, SUBTITULO, FECHA_MODIFICACION,
                                               ENLACE_DESTINO, LONGITUD, TIPO_FORMATO)
          VALUES (hibernate_sequence.nextval, oi.objeto_id, oi.idioma_id, oi.titulo, oi.titulo_largo, oi.contenido,
                  oi.resumen, oi.mime_type, sysdate, 'D', -1, oi.nombre_fichero, oi.html, oi.subtitulo,
                  oi.fecha_modificacion,
                  oi.enlace_destino, oi.longitud, oi.tipo_formato);

          DELETE FROM upo_objetos_idiomas_recursos
          WHERE objeto_idioma_id = oi.id;

          DELETE FROM upo_objetos_idiomas_atributos
          WHERE objeto_idioma_id = oi.id;
        END LOOP;

        INSERT INTO upo_objetos_log (ID, URL_PATH, PER_ID_RESPONSABLE, PUBLICABLE, LATITUD, LONGITUD, VISIBLE, TEXTO_NOVISIBLE, URL_ORIGINAL, ORIGEN_INFORMACION_ID, FECHA_CREACION, OTROS_AUTORES, LUGAR, FECHA_OPERACION, TIPO_OPERACION,
                                     USUARIO_OPERACION)
        VALUES
          (hibernate_sequence.nextval, o.url_path, o.per_id_responsable, o.publicable, o.latitud, o.longitud, o.visible,
           o.texto_novisible, o.url_original, o.origen_informacion_id, o.fecha_creacion, o.otros_autores, o.lugar,
           sysdate, 'D', -1);

        DELETE FROM upo_objetos_idiomas
        WHERE objeto_id = o.id;
        DELETE FROM upo_objetos_metadatos
        WHERE objeto_id = o.id;
        DELETE FROM upo_objetos_tags
        WHERE objeto_id = o.id;
        DELETE FROM upo_vigencias_objetos
        WHERE objeto_id = o.id;
        DELETE FROM upo_objetos_prioridades
        WHERE objeto_id = o.id;
        DELETE FROM upo_mapas_objetos
        WHERE objeto_id = o.id;
        DELETE FROM upo_autoguardado
        WHERE objeto_id = o.id;

        DELETE FROM upo_objetos
        WHERE id = o.id;

        COMMIT;
      END LOOP;

      DELETE FROM upo_autoguardado
      WHERE mapa_id = n.id;
      DELETE FROM upo_mapas_objetos
      WHERE mapa_id = n.id;
      DELETE FROM upo_mapas_plantillas
      WHERE mapa_id = n.id;
      DELETE FROM upo_moderacion_lotes
      WHERE mapa_id = n.id;
      DELETE FROM upo_mapas_idiomas_exclusiones
      WHERE mapa_id = n.id;

      DELETE FROM upo_mapas
      WHERE id = n.id;

      COMMIT;
    END LOOP;
    EXCEPTION
    WHEN OTHERS THEN
    dbms_output.put_line(sqlerrm);
  END;

ALTER TABLE UJI_PORTAL.UPO_MAPAS
ADD (fecha_creacion DATE DEFAULT sysdate NOT NULL);

UPDATE upo_mapas
SET fecha_creacion = sysdate;

CREATE OR REPLACE PROCEDURE limpi_papelera IS
  CURSOR nodos_a_borrar IS
    SELECT url_completa
    FROM upo_mapas
    WHERE url_completa LIKE '/paperera/_%'
          AND regexp_count(url_completa, '/') = 3
          AND fecha_creacion < sysdate - 15;
  BEGIN
    FOR n IN nodos_a_borrar LOOP
      borra_nodo(n.url_completa);
    END LOOP;
    commit;
  END;