alter table upo_objetos_metadatos add (NOMBRE_CLAVE_CA VARCHAR2(4000));
alter table upo_objetos_metadatos add (NOMBRE_CLAVE_ES VARCHAR2(4000));
alter table upo_objetos_metadatos add (NOMBRE_CLAVE_EN VARCHAR2(4000));

CREATE OR REPLACE FUNCTION UJI_PORTAL.get_prioridad(p_objeto_id NUMBER)
  RETURN NUMBER IS
  v_prioridad NUMBER;
  BEGIN
    SELECT
      decode(nvl(prioridad, 'NORMAL')
      , 'URGENTE'
      , 1
      , 'ALTA'
      , 2
      , 'NORMAL'
      , 3
      , 'BAJA'
      , 4)
    INTO v_prioridad
    FROM uji_portal.upo_objetos_prioridades
    WHERE objeto_id = p_objeto_id
          AND sysdate BETWEEN to_date(to_char(nvl(fecha_inicio, sysdate - 1), 'dd/mm/yyyy') || ' ' ||
                                      to_char(nvl(hora_inicio, sysdate), 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
    AND to_date(
        to_char(nvl(fecha_fin, sysdate + 1), 'dd/mm/yyyy') || ' ' || to_char(nvl(hora_fin, sysdate), 'hh24:mi:ss'),
        'dd/mm/yyyy hh24:mi:ss')
          AND rownum = 1;

    RETURN nvl(v_prioridad, 3);

    EXCEPTION
    WHEN OTHERS THEN
    RETURN 3;

  END;
/

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_PUBLICACION
(
    CONTENIDO_ID
  , CONTENIDO_IDIOMA_ID
  , URL_NODO
  , URL_NODO_ORIGINAL
  , URL_CONTENIDO
  , URL_COMPLETA
  , URL_COMPLETA_ORIGINAL
  , URL_NUM_NIVELES
  , URL_DESTINO
  , NIVEL
  , IDIOMA
  , TITULO
  , SUBTITULO
  , TITULO_LARGO
  , RESUMEN
  , MIME_TYPE
  , ES_HTML
  , CONTENIDO
  , FECHA_MODIFICACION
  , NODO_MAPA_ORDEN
  , CONTENIDO_ORDEN
  , TIPO_CONTENIDO
  , AUTOR
  , ORIGEN_NOMBRE
  , ORIGEN_URL
  , LUGAR
  , PROXIMA_FECHA_VIGENCIA
  , PRIMERA_FECHA_VIGENCIA
  , METADATA
  , TITULO_NODO
  , ORDEN
  , PRIORIDAD
  , DISTANCIA_SYSDATE
) AS
  SELECT
    o.id                                       contenido_id,
    oi.id                                      contenido_idioma_id,
    m.url_completa                             url_nodo,
    m.url_completa                             url_nodo_original,
    o.url_path                                 url_contenido,
    m.url_completa || o.url_path               url_completa,
    m.url_completa || o.url_path               url_completa_original,
    times_value_in_string(m.url_completa, '/') niveles,
    oi.enlace_destino,
    nvl(mp.nivel, 1),
    i.codigo_iso                               idioma,
    oi.titulo,
    oi.subtitulo,
    oi.titulo_largo,
    oi.resumen,
    oi.mime_type,
    oi.html                                    es_html,
    decode(oi.html, 'S', oi.contenido, null)   contenido,
    oi.fecha_modificacion,
    m.orden                                    nodo_mapa_orden,
    mo.orden                                   contenido_orden,
    'NORMAL'                                   tipo_contenido,
    (SELECT
    nvl(otros_autores, (SELECT
                          upper(nombre || ' ' || apellido1 || ' ' || apellido2)
                        FROM upo_ext_personas
                        WHERE id = per_id_responsable))
     FROM upo_objetos
     WHERE id = o.id)
                                                       autor,
    oin.nombre                                         origen_nombre,
    oin.url                                            origen_url,
    o.lugar,
    get_proxima_fecha_vigencia(o.id)                   proxima_fecha_vigencia,
    get_primera_fecha_vigencia(o.id)                   primera_fecha_vigencia,
    concat_metadata(o.id, i.codigo_iso)                metadata,
    decode(i.codigo_iso, 'es', m.titulo_publicacion_es, 'en', m.titulo_publicacion_en,
           m.titulo_publicacion_ca)                    tituloNodo,
    mo.orden,
    get_prioridad(o.id)                                prioridad,
    abs(get_inicio_vigencia_contenido(o.id) - sysdate) distancia_sysdate
  FROM upo_mapas m
    , upo_mapas_plantillas mp
    , upo_mapas_objetos mo
    , upo_objetos o
    , upo_objetos_idiomas oi
    , upo_idiomas i
    , upo_origenes_informacion oin
  WHERE m.id = mo.mapa_id
        AND m.id = mp.mapa_id (+)
        AND o.id = mo.objeto_id
        AND o.id = oi.objeto_id
        AND i.id = oi.idioma_id
        AND mo.tipo = 'NORMAL'
        AND o.origen_informacion_id = oin.id (+)
        AND o.visible = 'S'
  UNION ALL
  SELECT
    o.id           contenido_id,
    oi.id          contenido_idioma_id,
    m.url_completa url_nodo,
    (SELECT
    xm.url_completa
     FROM upo_mapas xm, upo_mapas_objetos xmo
     WHERE xm.id = xmo.mapa_id
           AND o.id = xmo.objeto_id
           AND xmo.tipo = 'NORMAL')
                                 url_nodo_original,
    o.url_path                   url_contenido,
    m.url_completa || o.url_path url_completa,
    (SELECT
    xm.url_completa || o.url_path
     FROM upo_mapas xm, upo_mapas_objetos xmo
     WHERE xm.id = xmo.mapa_id
           AND o.id = xmo.objeto_id
           AND xmo.tipo = 'NORMAL')
                                               url_completa_original,
    times_value_in_string(m.url_completa, '/') niveles,
    oi.enlace_destino,
    nvl(mp.nivel, 1),
    i.codigo_iso                               idioma,
    oi.titulo,
    oi.subtitulo,
    oi.titulo_largo,
    oi.resumen,
    oi.mime_type,
    oi.html                                    es_html,
    decode(oi.html, 'S', oi.contenido, null)   contenido,
    oi.fecha_modificacion,
    m.orden                                    nodo_mapa_orden,
    mo.orden                                   contenido_orden,
    'LINK'                                     tipo_contenido,
    (SELECT
    nvl(otros_autores, (SELECT
                          upper(nombre || ' ' || apellido1 || ' ' || apellido2)
                        FROM upo_ext_personas
                        WHERE id = per_id_responsable))
     FROM upo_objetos
     WHERE id = o.id)
                                                       autor,
    oin.nombre                                         origen_nombre,
    oin.url                                            origen_url,
    o.lugar,
    get_proxima_fecha_vigencia(o.id)                   proxima_fecha_vigencia,
    get_primera_fecha_vigencia(o.id)                   primera_fecha_vigencia,
    concat_metadata(o.id, i.codigo_iso)                metadata,
    decode(i.codigo_iso, 'es', m.titulo_publicacion_es, 'en', m.titulo_publicacion_en,
           m.titulo_publicacion_ca)                    tituloNodo,
    mo.orden,
    get_prioridad(o.id)                                prioridad,
    abs(get_inicio_vigencia_contenido(o.id) - sysdate) distancia_sysdate
  FROM upo_mapas m
    , upo_mapas_plantillas mp
    , upo_mapas_objetos mo
    , upo_objetos o
    , upo_objetos_idiomas oi
    , upo_idiomas i
    , upo_origenes_informacion oin
  WHERE m.id = mo.mapa_id
        AND m.id = mp.mapa_id (+)
        AND o.id = mo.objeto_id
        AND o.id = oi.objeto_id
        AND i.id = oi.idioma_id
        AND mo.tipo = 'LINK'
        AND o.origen_informacion_id = oin.id (+)
        AND o.visible = 'S'
        AND mo.estado_moderacion = 'ACEPTADO';


CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_SYNC_CONTENIDOS
(
    OBJETO_ID
  , URL_NODO
  , ORDEN
  , PRIORIDAD
  , DISTANCIA_SYSDATE
) AS
  SELECT
    o.id,
    m.url_completa,
    mo.orden,
    get_prioridad(o.id)                                prioridad,
    abs(get_inicio_vigencia_contenido(o.id) - sysdate) distancia_sysdate
  FROM upo_mapas m, upo_mapas_objetos mo, upo_objetos o
  WHERE m.id = mo.mapa_id
        AND o.id = mo.objeto_id -- cualquier contenido visible
        AND ((mo.tipo = 'NORMAL')
             OR (mo.tipo = 'LINK'
                 AND mo.estado_moderacion = 'ACEPTADO'))
        AND o.visible = 'S'
        AND (-- no hay vigencia definida
             NOT exists(SELECT
                          id
                        FROM upo_vigencias_objetos vo
                        WHERE vo.objeto_id = o.id)
             OR -- vigencia de 72 horas y dentro de rango de fechas
             exists
             (SELECT
                id
              FROM upo_vigencias_objetos vo
              WHERE vo.objeto_id = o.id
                    AND (
                to_date(to_char(fecha, 'dd/mm/yyyy') || ' ' || to_char(nvl(hora_inicio, sysdate), 'hh24:mi:ss'),
                        'dd/mm/yyyy hh24:mi:ss') BETWEEN sysdate AND sysdate + 3
                OR to_date(to_char(fecha, 'dd/mm/yyyy') || ' ' || to_char(nvl(hora_fin, sysdate), 'hh24:mi:ss'),
                           'dd/mm/yyyy hh24:mi:ss') BETWEEN sysdate AND sysdate + 3)))
        -- Obligatorio en CA y ES
        AND (
    (exists(SELECT
              i.codigo_iso
            FROM upo_objetos_idiomas oi, upo_idiomas i
            WHERE i.id = oi.idioma_id
                  AND lower(i.codigo_iso) = 'ca'
                  AND o.id = oi.objeto_id
                  AND nvl(oi.html, 'N') = 'S'
     )
     AND exists(SELECT
                  i.codigo_iso
                FROM upo_objetos_idiomas oi, upo_idiomas i
                WHERE i.id = oi.idioma_id
                      AND lower(i.codigo_iso) = 'es'
                      AND o.id = oi.objeto_id
                      AND nvl(oi.html, 'N') = 'S'
    )
    )
    OR
    (
      has_tag(o.id, 'megabanner') = 'S'
    )
  );

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_BUSQUEDA
(
    CONTENIDO_IDIOMA_ID
  , MAPA_URL_COMPLETA
  , IDIOMA_CODIGO_ISO
  , TITULO_SIN_ACENTOS
  , TITULO_LARGO_SIN_ACENTOS
  , RESUMEN_SIN_ACENTOS
  , CONTENIDO_SIN_ACENTOS
  , SUBTITULO_SIN_ACENTOS
  , PRIMERA_FECHA_VIGENCIA
  , ULTIMA_FECHA_VIGENCIA
  , ORDEN
  , PRIORIDAD
  , DISTANCIA_SYSDATE
) AS
  SELECT
    oi.id                                                                 contenido_idioma_id,
    m.url_completa                                                        mapa_url_completa,
    lower(i.codigo_iso)                                                   idioma_codigo_iso,
    quita_acentos(oi.titulo)                                              titulo_sin_acentos,
    quita_acentos(oi.titulo_largo)                                        titulo_largo_sin_acentos,
    quita_acentos(oi.resumen)                                             resumen_sin_acentos,
    quita_acentos(blob_to_clob(decode(oi.html, 'S', oi.contenido, null))) contenido_sin_acentos,
    quita_acentos(oi.subtitulo)                                           subtitulo_sin_acentos,
    (SELECT
    min(trunc(fecha))
     FROM upo_vigencias_objetos
     WHERE objeto_id = o.id)
                                                                          primera_fecha_vigencia,
    (SELECT
    max(trunc(fecha))
     FROM upo_vigencias_objetos
     WHERE objeto_id = o.id)
                                                                          ultima_fecha_vigencia,
    mo.orden,
    get_prioridad(o.id)                                                   prioridad,
    abs(get_inicio_vigencia_contenido(o.id) - sysdate)                    distancia_sysdate
  FROM upo_mapas_objetos mo
    , upo_objetos o
    , upo_mapas m
    , upo_objetos_idiomas oi
    , upo_idiomas i
  WHERE mo.mapa_id = m.id
        AND mo.objeto_id = o.id
        AND oi.objeto_id = o.id
        AND i.id = oi.idioma_id
        AND ((mo.tipo = 'NORMAL')
             OR (mo.tipo = 'LINK'
                 AND mo.estado_moderacion = 'ACEPTADO'))
        AND o.visible = 'S'
        AND oi.html = 'S';

DROP VIEW UJI_PORTAL.UPO_VW_BUSQUEDA_URL_VIGENCIA;


CREATE TABLE UPO_TIPOS_METADATOS (ID NUMBER NOT NULL PRIMARY KEY, NOMBRE VARCHAR2(4000) NOT NULL);

CREATE TABLE UPO_ESQUEMAS_METADATOS (ID NUMBER NOT NULL PRIMARY KEY, NOMBRE VARCHAR2(4000) NOT NULL);

CREATE TABLE UPO_ATRIBUTOS_METADATOS (ID               NUMBER         NOT NULL PRIMARY KEY,
                                      ESQUEMA_ID       NUMBER         NOT NULL,
                                      TIPO_ID          NUMBER         NOT NULL,
                                      CLAVE            VARCHAR2(4000) NOT NULL,
                                      VALOR_DEFECTO_CA VARCHAR2(4000),
                                      VALOR_DEFECTO_ES VARCHAR2(4000),
                                      VALOR_DEFECTO_EN VARCHAR2(4000),
                                      NOMBRE_CLAVE_CA VARCHAR2(4000) not null,
                                      NOMBRE_CLAVE_ES VARCHAR2(4000) not null,
                                      NOMBRE_CLAVE_EN VARCHAR2(4000) not null,
                                      PUBLICABLE NUMBER DEFAULT 1 NOT NULL
);

ALTER TABLE UPO_ATRIBUTOS_METADATOS ADD CONSTRAINT UPO_ATRIB_METADATOS_ESQUEMA_FK FOREIGN KEY (ESQUEMA_ID) REFERENCES UPO_ESQUEMAS_METADATOS (ID);
ALTER TABLE UPO_ATRIBUTOS_METADATOS ADD CONSTRAINT UPO_ATRIB_METADATOS_tIPO_FK FOREIGN KEY (TIPO_ID) REFERENCES UPO_TIPOS_METADATOS (ID);


CREATE TABLE UPO_VALORES_METADATOS (ID          NUMBER NOT NULL PRIMARY KEY,
                                    ATRIBUTO_ID NUMBER NOT NULL,
                                    VALOR_CA    VARCHAR2(4000),
                                    VALOR_ES    VARCHAR2(4000),
                                    VALOR_EN    VARCHAR2(4000),
                                    NOMBRE_CLAVE_CA VARCHAR2(4000),
                                    NOMBRE_CLAVE_ES VARCHAR2(4000),
                                    NOMBRE_CLAVE_EN VARCHAR2(4000)
);

ALTER TABLE UPO_VALORES_METADATOS ADD CONSTRAINT UPO_VALOR_METADATOS_ATRIB_FK FOREIGN KEY (ATRIBUTO_ID) REFERENCES UPO_ATRIBUTOS_METADATOS (ID);

ALTER TABLE UPO_OBJETOS_METADATOS MODIFY (CLAVE NULL);

ALTER TABLE UPO_OBJETOS_METADATOS ADD (TIPO_ID NUMBER, ATRIBUTO_ID NUMBER);

ALTER TABLE UPO_OBJETOS_METADATOS ADD CONSTRAINT UPO_OBJETO_METADATOS_TIPO_FK FOREIGN KEY (TIPO_ID) REFERENCES UPO_TIPOS_METADATOS (ID);
ALTER TABLE UPO_OBJETOS_METADATOS ADD CONSTRAINT UPO_OBJETO_METADATOS_ATRIB_FK FOREIGN KEY (ATRIBUTO_ID) REFERENCES UPO_ATRIBUTOS_METADATOS (ID);

INSERT INTO upo_tipos_metadatos (id, nombre) VALUES (1, 'Campo de texto');
INSERT INTO upo_tipos_metadatos (id, nombre) VALUES (2, 'Campo de fecha');
INSERT INTO upo_tipos_metadatos (id, nombre) VALUES (3, 'Área de texto');
INSERT INTO upo_tipos_metadatos (id, nombre) VALUES (4, 'Lista desplegable');

ALTER TABLE upo_atributos_metadatos ADD permite_blanco NUMBER DEFAULT 1 NOT NULL;

insert into upo_esquemas_metadatos values (1, 'Paranimf');

insert into upo_atributos_metadatos (ID, ESQUEMA_ID, TIPO_ID, CLAVE, VALOR_DEFECTO_CA, VALOR_DEFECTO_ES, VALOR_DEFECTO_EN, PERMITE_BLANCO, NOMBRE_CLAVE_CA, NOMBRE_CLAVE_ES, NOMBRE_CLAVE_EN)
  values (1, 1, 1, 'companyia', '','','',1, 'Companyia', 'Compañía', 'Company');
insert into upo_atributos_metadatos (ID, ESQUEMA_ID, TIPO_ID, CLAVE, VALOR_DEFECTO_CA, VALOR_DEFECTO_ES, VALOR_DEFECTO_EN, PERMITE_BLANCO, NOMBRE_CLAVE_CA, NOMBRE_CLAVE_ES, NOMBRE_CLAVE_EN)
values (2, 1, 1, 'cicle', '','','',1, 'Cicle', 'Ciclo', 'Series');
insert into upo_atributos_metadatos (ID, ESQUEMA_ID, TIPO_ID, CLAVE, VALOR_DEFECTO_CA, VALOR_DEFECTO_ES, VALOR_DEFECTO_EN, PERMITE_BLANCO, NOMBRE_CLAVE_CA, NOMBRE_CLAVE_ES, NOMBRE_CLAVE_EN)
values (3, 1, 1, 'duracio', '','','',1, 'Duració', 'Duración', 'Duration');
insert into upo_atributos_metadatos (ID, ESQUEMA_ID, TIPO_ID, CLAVE, VALOR_DEFECTO_CA, VALOR_DEFECTO_ES, VALOR_DEFECTO_EN, PERMITE_BLANCO, NOMBRE_CLAVE_CA, NOMBRE_CLAVE_ES, NOMBRE_CLAVE_EN)
values (4, 1, 1, 'caracteristiques', '','','',1, 'Característiques', 'Características', 'Features');
insert into upo_atributos_metadatos (ID, ESQUEMA_ID, TIPO_ID, CLAVE, VALOR_DEFECTO_CA, VALOR_DEFECTO_ES, VALOR_DEFECTO_EN, PERMITE_BLANCO, NOMBRE_CLAVE_CA, NOMBRE_CLAVE_ES, NOMBRE_CLAVE_EN)
values (5, 1, 1, 'comentaris', '','','',1, 'Comentaris', 'Comentarios', 'Comments');
insert into upo_atributos_metadatos (ID, ESQUEMA_ID, TIPO_ID, CLAVE, VALOR_DEFECTO_CA, VALOR_DEFECTO_ES, VALOR_DEFECTO_EN, PERMITE_BLANCO, NOMBRE_CLAVE_CA, NOMBRE_CLAVE_ES, NOMBRE_CLAVE_EN)
values (6, 1, 1, 'web', '','','',1, 'Web', 'Web', 'Web');
insert into upo_atributos_metadatos (ID, ESQUEMA_ID, TIPO_ID, CLAVE, VALOR_DEFECTO_CA, VALOR_DEFECTO_ES, VALOR_DEFECTO_EN, PERMITE_BLANCO, NOMBRE_CLAVE_CA, NOMBRE_CLAVE_ES, NOMBRE_CLAVE_EN)
values (7, 1, 1, 'seientsnumerats', '','','',1, 'Seients numerats', 'Asientos numerados', 'Numbered seats');
insert into upo_atributos_metadatos (ID, ESQUEMA_ID, TIPO_ID, CLAVE, VALOR_DEFECTO_CA, VALOR_DEFECTO_ES, VALOR_DEFECTO_EN, PERMITE_BLANCO, NOMBRE_CLAVE_CA, NOMBRE_CLAVE_ES, NOMBRE_CLAVE_EN)
values (8, 1, 1, 'apertura', '','','',1, 'Apertura', 'Apertura', 'Opening');
insert into upo_atributos_metadatos (ID, ESQUEMA_ID, TIPO_ID, CLAVE, VALOR_DEFECTO_CA, VALOR_DEFECTO_ES, VALOR_DEFECTO_EN, PERMITE_BLANCO, NOMBRE_CLAVE_CA, NOMBRE_CLAVE_ES, NOMBRE_CLAVE_EN)
values (9, 1, 4, 'tipo', '','','',1, 'Tipus', 'Tipo', 'Type');


insert into upo_valores_metadatos (ID, ATRIBUTO_ID, VALOR_CA, VALOR_ES, VALOR_EN)
  values (1, 9, 'Cinema', 'Cine', 'Cinema');
insert into upo_valores_metadatos (ID, ATRIBUTO_ID, VALOR_CA, VALOR_ES, VALOR_EN)
  values (2, 9, 'Teatre', 'Teatro', 'Theatre');
insert into upo_valores_metadatos (ID, ATRIBUTO_ID, VALOR_CA, VALOR_ES, VALOR_EN)
  values (3, 9, 'Música', 'Música', 'Music');
insert into upo_valores_metadatos (ID, ATRIBUTO_ID, VALOR_CA, VALOR_ES, VALOR_EN)
  values (4, 9, 'Clown per adults', 'Clown para adultos', 'Clown for adults');

update upo_objetos_metadatos set atributo_id = 3
where clave = 'duracio';
update upo_objetos_metadatos set atributo_id = 1
where clave = 'companyia';
update upo_objetos_metadatos set atributo_id = 8
where clave = 'apertura';
update upo_objetos_metadatos set atributo_id = 2
where clave = 'cicle';
update upo_objetos_metadatos set atributo_id = 6
where clave = 'web';
update upo_objetos_metadatos set atributo_id = 7
where clave = 'seientsnumerats';
update upo_objetos_metadatos set atributo_id = 5
where clave = 'comentaris';
update upo_objetos_metadatos set atributo_id = 4
where clave = 'caracteristiques';

delete upo_objetos_metadatos
where clave = 'esquema';

update upo_objetos_metadatos set atributo_id = 9, valor_ca = 1, valor_es = 1, valor_en = 1
where clave = 'tipo' and upper(valor_ca) like '%CINE%';
update upo_objetos_metadatos set atributo_id = 9, valor_ca = 2, valor_es = 2, valor_en = 2
where clave = 'tipo' and upper(valor_ca) like '%TEA%';
update upo_objetos_metadatos set atributo_id = 9, valor_ca = 3, valor_es = 3, valor_en = 3
where clave = 'tipo' and upper(valor_ca) like '%SIC%';
update upo_objetos_metadatos set atributo_id = 9, valor_ca = 4, valor_es = 4, valor_en = 4
where clave = 'tipo' and upper(valor_ca) like '%CLOWN%';


CREATE OR REPLACE function UJI_PORTAL.concat_metadata(v_objeto_id number, v_idioma_iso varchar2)
  return clob
is
  vRdo clob;
  begin

    select decode(v_idioma_iso, 'ca', replace(replace(wm_concat(replace(clave, ',', '###') || '@@@' || replace(nombre_clave_ca, ',', '###') || '@@@' || replace(valor_ca, ',', '###')), ',', '~~~'), '###', ','),
                  'es', replace(replace(wm_concat(replace(clave, ',', '###') || '@@@' || replace(nombre_clave_es, ',', '###') || '@@@' || replace(valor_es, ',', '###')), ',', '~~~'), '###', ','),
                  'en', replace(replace(wm_concat(replace(clave, ',', '###') || '@@@' || replace(nombre_clave_en, ',', '###') || '@@@' || replace(valor_en, ',', '###')), ',', '~~~'), '###', ','))
    into vRdo
    from ( select om.clave,
             om.nombre_clave_ca,
             om.nombre_clave_es,
             om.nombre_clave_en,
             om.valor_ca,
             om.valor_es,
             om.valor_en
           from upo_objetos_metadatos om
           where objeto_id = v_objeto_id
                 and ((v_idioma_iso = 'ca' and valor_ca is not null and atributo_id is null)
                      or
                      (v_idioma_iso = 'es' and valor_es is not null and atributo_id is null)
                      or
                      (v_idioma_iso = 'en' and valor_en is not null and atributo_id is null)
           )
           union
           select am.clave,
             am.nombre_clave_ca,
             am.nombre_clave_es,
             am.nombre_clave_en,
             nvl((select valor_ca from upo_valores_metadatos where atributo_id = am.id and to_char(id) = om.valor_ca), om.valor_ca) valor_ca,
             nvl((select valor_es from upo_valores_metadatos where atributo_id = am.id and to_char(id) = om.valor_es), om.valor_es) valor_es,
             nvl((select valor_en from upo_valores_metadatos where atributo_id = am.id and to_char(id) = om.valor_en), om.valor_en) valor_en
           from upo_objetos_metadatos om, upo_atributos_metadatos am
           where om.objeto_id = v_objeto_id
                 and om.atributo_id = am.id
                 and am.publicable = 1
                 and ((v_idioma_iso = 'ca' and om.valor_ca is not null)
                      or
                      (v_idioma_iso = 'es' and om.valor_es is not null)
                      or
                      (v_idioma_iso = 'en' and om.valor_en is not null)
           )
           union
           select 'esquema', 'esquema', 'esquema', 'esquema', lower(em.nombre), lower(em.nombre), lower(em.nombre)
           from upo_objetos_metadatos om, upo_atributos_metadatos am, upo_esquemas_metadatos em
           where om.objeto_id = v_objeto_id
                 and om.atributo_id = am.id
                 and em.id = am.esquema_id

    );

    return vRdo;
  end;
/


create or replace force view UJI_PORTAL.UPO_VW_BUSQUEDA
(
    CONTENIDO_IDIOMA_ID
  , MAPA_URL_COMPLETA
  , IDIOMA_CODIGO_ISO
  , TITULO_SIN_ACENTOS
  , TITULO_LARGO_SIN_ACENTOS
  , RESUMEN_SIN_ACENTOS
  , CONTENIDO_SIN_ACENTOS
  , SUBTITULO_SIN_ACENTOS
  , ORDEN
  , PRIORIDAD
  , DISTANCIA_SYSDATE
  , FECHA_VIGENCIA
) as
  select oi.id contenido_idioma_id
    , m.url_completa mapa_url_completa
    , lower(i.codigo_iso) idioma_codigo_iso
    , quita_acentos(oi.titulo) titulo_sin_acentos
    , quita_acentos(oi.titulo_largo) titulo_largo_sin_acentos
    , quita_acentos(oi.resumen) resumen_sin_acentos
    , quita_acentos(blob_to_clob(decode(oi.html, 'S', oi.contenido, null))) contenido_sin_acentos
    , quita_acentos(oi.subtitulo) subtitulo_sin_acentos
    , mo.orden
    , get_prioridad(o.id) prioridad
    , abs(get_inicio_vigencia_contenido(o.id) - sysdate) distancia_sysdate
    , trunc(vo.fecha) fecha
  from upo_mapas_objetos mo
    , upo_objetos o
    , upo_mapas m
    , upo_objetos_idiomas oi
    , upo_idiomas i
    , upo_vigencias_objetos vo
  where mo.mapa_id = m.id
        and mo.objeto_id = o.id
        and oi.objeto_id = o.id
        and i.id = oi.idioma_id
        and ((mo.tipo = 'NORMAL')
             or (mo.tipo = 'LINK'
                 and mo.estado_moderacion = 'ACEPTADO'))
        and o.visible = 'S'
        and oi.html = 'S'
        and vo.objeto_id(+) = o.id;
