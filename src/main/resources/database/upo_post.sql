CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_EXT_PERSONAS
(
    ID
  , NOMBRE
  , APELLIDO1
  , APELLIDO2
  , NOMBRE_COMPLETO
  , EMAIL
) AS
  SELECT
    id,
    nombre,
    apellido1,
    apellido2,
    apellido1 || ' ' || apellido2 || ', ' || nombre nombre_completo,
    busca_cuenta(id)                                email
  FROM per_personas p, per_personas_subvinculos ps
  WHERE p.id = ps.per_id
        AND ps.svi_vin_id = 4
        AND ps.fecha_fin IS null
  UNION
  SELECT
    id,
    nombre,
    apellido1,
    apellido2,
    apellido1 || ' ' || apellido2 || ', ' || nombre nombre_completo,
    busca_cuenta(id)                                email
  FROM per_personas
  WHERE id IN (SELECT
                 per_id
               FROM upo_franquicias_accesos
               UNION
               SELECT
                 per_id
               FROM upo_migracion_urls_per
               UNION
               SELECT
                 per_id_responsable
               FROM upo_objetos
               UNION
               SELECT
                 per_id_moderacion
               FROM upo_mapas_objetos
               UNION
               SELECT
                 per_id_propuesta
               FROM upo_mapas_objetos)
  UNION
  SELECT
    id,
    nombre,
    apellido1,
    apellido2,
    apellido1 || ' ' || apellido2 || ', ' || nombre nombre_completo,
    busca_cuenta(id)                                email
  FROM per_personas p, upo_usuarios_externos ue
  WHERE p.id = ue.persona_id;


DROP TABLE UPO_EXT_PERSONAS_TODAS;


create or replace force view UJI_PORTAL.UPO_EXT_PERSONAS_TODAS
(
    ID
  , NOMBRE
  , APELLIDO1
  , APELLIDO2
  , NOMBRE_COMPLETO
  , NOMBRE_BUSQUEDA
  , EMAIL
) as
  select id
    , nombre
    , apellido1
    , apellido2
    , apellido1 || ' ' || apellido2 || ', ' || nombre nombre_completo
    , limpia(nombre || ' ' || apellido1 || ' ' || apellido2) nombre_busqueda
    , busca_cuenta(id) email
  from per_personas p
