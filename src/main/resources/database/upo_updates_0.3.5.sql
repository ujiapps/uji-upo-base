alter table upo_mapas add (es_contenido_relacionado number default 1 not null);

CREATE OR REPLACE procedure UJI_PORTAL.sync_contenidos (
  p_origen         varchar2,
  p_destino        varchar2,
  p_max_contenidos number
)
is
  type t_content is record (
    objeto_id number,
    url_nodo varchar2(4000)
  );

  type t_contents is table of t_content;
  type t_array_output is table of varchar2(32767);

  vSourceDirectories t_contents;
  vTargetContents t_contents;

  procedure clean_target_directory(p_destino varchar2) is
    begin
      delete from upo_mapas_objetos
      where mapa_id in (select id
                        from upo_mapas
                        where url_completa like p_destino || '_%');

      delete from upo_mapas
      where url_completa like p_destino || '_%';
    end;

  function get_source_directories(p_origen varchar2)
    return t_contents
  is
    vContenidos t_contents;
    begin
      execute immediate 'select objeto_id, url_nodo
                           from (select objeto_id, url_nodo
                                   from upo_vw_sync_contenidos
                                  where url_nodo like ''' || p_origen || '%''
                                  order by prioridad, distancia_sysdate, orden, fecha_creacion desc)'
      bulk collect into vContenidos;

      return vContenidos;
    end;

  function varchar_to_array(
    p_varchar   in varchar2,
    p_separador in varchar2:= ','
  )
    return t_array_output
  is
    vCadena varchar2(32767):= p_varchar;
    vRdo    t_array_output:= t_array_output();
    vAux    varchar2(400);
    begin
      if substr(p_varchar, length(p_varchar)) != p_separador then
        vCadena:= vCadena||p_separador;
      end if;

      while instr(vCadena, p_separador) is not null loop
        vAux:= substr(vCadena, 1, instr(vCadena, p_separador)-1);
        vCadena:= substr(vCadena, instr(vCadena, p_separador)+1);

        vRdo.extend;
        vRdo(vRdo.count):= vAux;
      end loop;

      return vRdo;
    end;

  function get_last_directory_on_path(p_path varchar2)
    return varchar2
  is
    vDatos t_array_output;
    begin
      vDatos:= varchar_to_array(p_path, '/');
      return vDatos(vDatos.count);
    end;

  function contains_path(vPaths t_array_output, vPath varchar2)
    return boolean
  is
    begin
      for i in 1..vPaths.count loop
        if vPaths(i) = vPath then
          return true;
        end if;
      end loop;

      return false;
    end;

  procedure add_source_dirs_to_target(p_contents t_contents, p_destino varchar2, p_max_contenidos number) is
    vDirectory  varchar2(4000);

    vTargetNode number;
    vNewNode    number;

    vFranquicia number;
    vMenu       number;
    vAux        varchar2(4000);
    vPaths      t_array_output:= t_array_output();

    begin
      select id, franquicia_id, menu_id
      into vTargetNode, vFranquicia, vMenu
      from upo_mapas
      where url_completa = p_destino;

      for i in 1..p_contents.count loop
        begin
          vAux:= replace(p_contents(i).url_nodo, '/', '-');
          vDirectory:= substr(vAux, 2, length(vAux)-2);

          if not contains_path(vPaths, vDirectory) then
            if vPaths.count < p_max_contenidos then
              insert into upo_mapas(id, url_path, franquicia_id, url_completa, menu_id, mapa_id, menu_heredado, orden)
              values (hibernate_sequence.nextval,vDirectory, vFranquicia, p_destino || vDirectory || '/', vMenu, vTargetNode, 1, i)
              returning id into vNewNode;

              vPaths.extend;
              vPaths(vPaths.count):= vDirectory;

              insert into upo_mapas_objetos (id, mapa_id, objeto_id, estado_moderacion, tipo)
                select hibernate_sequence.nextval, vNewNode, objeto_id, 'ACEPTADO', 'LINK'
                from upo_mapas_objetos
                where mapa_id = (select id
                                 from upo_mapas
                                 where url_completa = p_contents(i).url_nodo);
            end if;
          end if;
          exception
          when others then
          dbms_output.put_line('Directory: ' || vDirectory || ' - Error: ' || sqlerrm);
        end;
      end loop;
    end;

  begin
    clean_target_directory(p_destino);
    vSourceDirectories:= get_source_directories(p_origen);
    add_source_dirs_to_target(vSourceDirectories, p_destino, p_max_contenidos);

    commit;
  end;
/