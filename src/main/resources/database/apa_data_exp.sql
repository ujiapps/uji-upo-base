-- deletes
delete from uji_portal.apa_ext_personas_cuentas;
delete from uji_portal.apa_ext_aplicaciones_items;
delete from uji_portal.apa_ext_aplicaciones_perfiles;
delete from uji_portal.apa_ext_aplicaciones_extras;
delete from uji_portal.apa_ext_aplicaciones;
delete from uji_portal.apa_ext_tipos;
delete from uji_portal.apa_ext_perfiles_per;
delete from uji_portal.apa_ext_perfiles;

-- apa_ext_tipos
insert into uji_portal.apa_ext_tipos (id, nombre) values (1, 'ADMIN');
insert into uji_portal.apa_ext_tipos (id, nombre) values (2, 'USUARIO');
insert into uji_portal.apa_ext_tipos (id, nombre) values (3, 'TRADUCTOR');

-- apa_ext_perfiles
insert into uji_portal.apa_ext_perfiles (id, nombre, codigo) values (2, 'PAS',5000004);
insert into uji_portal.apa_ext_perfiles (id, nombre, codigo) values (3, 'PDI',5000009);
insert into uji_portal.apa_ext_perfiles (id, nombre, codigo) values (4, 'SLT',0);

-- apa_ext_perfiles
insert into uji_portal.apa_ext_perfiles_per (id, perfil_id, persona_id, plaza_id)
select xpf.per_id||p.codigo, p.id, xpf.per_id, null  
  from xpfdm.xpf_vmc_perfiles_per xpf, 
       uji_portal.apa_ext_perfiles p
 where xpf.perf_id = p.codigo;

-- apa_ext_personas_cuentas
insert into uji_portal.apa_ext_personas_cuentas (persona_id, cuenta)
select distinct psv_per_id, nombre
  from gri_per.per_cuentas;

-- apa_ext_aplicaciones
insert into uji_portal.apa_ext_aplicaciones (id, nombre, codigo) values (1, 'Portal', 'UPO');

-- apa_ext_aplicaciones_perfiles
insert into uji_portal.apa_ext_aplicaciones_perfiles (id, aplicacion_id, perfil_id, tipo_id) values (1, 1, 2, 2);

insert into uji_portal.apa_ext_aplicaciones_perfiles (id, aplicacion_id, perfil_id, tipo_id) values (2, 1, 3, 2);

-- apa_ext_aplicaciones_extra
insert into uji_portal.apa_ext_aplicaciones_extras (id, aplicacion_id, persona_id, plaza_id, tipo_id) values (1, 1, 65394, null, 1);
insert into uji_portal.apa_ext_aplicaciones_extras (id, aplicacion_id, persona_id, plaza_id, tipo_id) values (2, 1, 111204, null, 1);
insert into uji_portal.apa_ext_aplicaciones_extras (id, aplicacion_id, persona_id, plaza_id, tipo_id) values (3, 1, 7667, null, 1);
insert into uji_portal.apa_ext_aplicaciones_extras (id, aplicacion_id, persona_id, plaza_id, tipo_id) values (4, 1, 3358, null, 1);
insert into uji_portal.apa_ext_aplicaciones_extras (id, aplicacion_id, persona_id, plaza_id, tipo_id) values (5, 1, 84297, null, 1);

-- apa_ext_aplicaciones_items
insert into uji_portal.apa_ext_aplicaciones_items (id, aplicacion_id, nombre, jsfile) values (1, 1, 'Gestió de guies docents', 'administracion.js');
insert into uji_portal.apa_ext_aplicaciones_items (id, aplicacion_id, nombre, jsfile) values (2, 1, 'Asignació de professors a assignatures', 'asignaProfesor.js');
insert into uji_portal.apa_ext_aplicaciones_items (id, aplicacion_id, nombre, jsfile) values (3, 1, 'Gestió de cursos', 'cursos.js');
insert into uji_portal.apa_ext_aplicaciones_items (id, aplicacion_id, nombre, jsfile) values (4, 1, 'Gestió administradors extra', 'roles.js');