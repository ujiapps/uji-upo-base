CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_SYNC_CONTENIDOS
(
   OBJETO_ID,
   URL_NODO,
   ORDEN,
   PRIORIDAD,
   DISTANCIA_SYSDATE
)
AS
 select o.id,
          m.url_completa,
          mo.orden,
          nvl((select decode (nvl(prioridad, 'NORMAL'), 'URGENTE', 1, 'ALTA', 2, 'NORMAL', 3, 'BAJA', 4)
             from upo_objetos_prioridades
            where objeto_id = o.id
              and trunc (sysdate) between trunc(nvl (fecha_inicio, sysdate - 1))
                                      and trunc(nvl (fecha_fin, sysdate + 1))
              and sysdate between to_date (to_char (sysdate, 'dd/mm/yyyy') || ' ' || to_char (nvl (hora_inicio, sysdate), 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
                              and to_date (to_char (sysdate, 'dd/mm/yyyy') || ' ' || to_char (nvl (hora_fin, sysdate), 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
              and rownum = 1), 3) prioridad,
          abs(get_inicio_vigencia_contenido(o.id) - sysdate) distancia_sysdate
     FROM upo_mapas m, upo_mapas_objetos mo, upo_objetos o
    where m.id = mo.mapa_id
      and o.id = mo.objeto_id             -- cualquier contenido visible
      AND mo.tipo in ('NORMAL', 'LINK')
      and o.visible = 'S'
      and (-- no hay vigencia definida
           not exists (select id
                         from upo_vigencias_objetos vo
                        where vo.objeto_id = o.id)
           or -- vigencia de 72 horas y dentro de rango de fechas
              exists (select id
                        from upo_vigencias_objetos vo
                       where vo.objeto_id = o.id
                         and trunc (fecha) in
                             (trunc (sysdate), trunc (sysdate + 1), trunc (sysdate + 2), trunc (sysdate + 3))
                         and sysdate between to_date (to_char (sysdate, 'dd/mm/yyyy') || ' ' || to_char (nvl (hora_inicio, sysdate), 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
                         and to_date (to_char (sysdate, 'dd/mm/yyyy') || ' ' || to_char (nvl (hora_fin, sysdate), 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')))
            -- Obligatorio en CA y ES
            and exists (select i.codigo_iso
                          from upo_objetos_idiomas oi, upo_idiomas i
                         where i.id = oi.idioma_id
                           and lower (i.codigo_iso) = 'ca'
                           and o.id = oi.objeto_id
                           and nvl(oi.html, 'N') = 'S')
            and exists (select i.codigo_iso
                          from upo_objetos_idiomas oi, upo_idiomas i
                         where i.id = oi.idioma_id
                           and lower (i.codigo_iso) = 'es'
                           and o.id = oi.objeto_id
                           and nvl(oi.html, 'N') = 'S');

CREATE OR REPLACE procedure UJI_PORTAL.sync_contenidos (
   p_origen         varchar2, 
   p_destino        varchar2, 
   p_max_contenidos number
) 
is
   type t_content is record (
      objeto_id number,
      url_nodo varchar2(4000)
   );
  
   type t_contents is table of t_content;
   type t_array_output is table of varchar2(32767);
      
   vSourceDirectories t_contents;         
   vTargetContents t_contents;
  
   procedure clean_target_directory(p_destino varchar2) is
   begin
      delete from upo_mapas_objetos
       where mapa_id in (select id 
                           from upo_mapas
                          where url_completa like p_destino || '_%');
     
      delete from upo_mapas
       where url_completa like p_destino || '_%';
   end; 
  
   function get_source_directories(p_origen varchar2)
      return t_contents
   is
      vContenidos t_contents;
   begin
      execute immediate 'select objeto_id, url_nodo
                           from (select objeto_id, url_nodo
                                   from upo_vw_sync_contenidos
                                  where url_nodo like ''' || p_origen || '%''
                                  order by prioridad, distancia_sysdate, orden)'
        bulk collect into vContenidos;
        
      return vContenidos;  
   end;
  
   function varchar_to_array(
      p_varchar   in varchar2,
      p_separador in varchar2:= ','
   )
      return t_array_output
   is
      vCadena varchar2(32767):= p_varchar;
      vRdo    t_array_output:= t_array_output();
      vAux    varchar2(400);
   begin
      if substr(p_varchar, length(p_varchar)) != p_separador then
         vCadena:= vCadena||p_separador;
      end if;
      
      while instr(vCadena, p_separador) is not null loop
            vAux:= substr(vCadena, 1, instr(vCadena, p_separador)-1);
         vCadena:= substr(vCadena, instr(vCadena, p_separador)+1);

         vRdo.extend;
         vRdo(vRdo.count):= vAux;
      end loop;
       
      return vRdo;
   end;
   
   function get_last_directory_on_path(p_path varchar2)
      return varchar2
   is
      vDatos t_array_output;
   begin
      vDatos:= varchar_to_array(p_path, '/');
      return vDatos(vDatos.count);
   end;     
   
   function contains_path(vPaths t_array_output, vPath varchar2) 
      return boolean
   is
   begin
      for i in 1..vPaths.count loop
          if vPaths(i) = vPath then
             return true;
          end if;
      end loop;
      
      return false;
   end;
     
   procedure add_source_dirs_to_target(p_contents t_contents, p_destino varchar2, p_max_contenidos number) is
      vDirectory  varchar2(4000);
      
      vTargetNode number;
      vNewNode    number;
      
      vFranquicia number; 
      vMenu       number;
      vAux        varchar2(4000);        
      vPaths      t_array_output:= t_array_output();
      
   begin
      select id, franquicia_id, menu_id
        into vTargetNode, vFranquicia, vMenu
        from upo_mapas
       where url_completa = p_destino;
       
      for i in 1..p_contents.count loop
          begin         
             vAux:= replace(p_contents(i).url_nodo, '/', '-');
             vDirectory:= substr(vAux, 2, length(vAux)-2);
          
             if not contains_path(vPaths, vDirectory) then
                if vPaths.count < p_max_contenidos then
                   insert into upo_mapas(id, url_path, franquicia_id, url_completa, menu_id, mapa_id, menu_heredado, orden)
                     values (hibernate_sequence.nextval,vDirectory, vFranquicia, p_destino || vDirectory || '/', vMenu, vTargetNode, 1, i)
                     returning id into vNewNode;

                   vPaths.extend;          
                   vPaths(vPaths.count):= vDirectory;
                   
                   insert into upo_mapas_objetos (id, mapa_id, objeto_id, estado_moderacion, tipo) 
                     select hibernate_sequence.nextval, vNewNode, objeto_id, 'ACEPTADO', 'LINK' 
                       from upo_mapas_objetos
                      where mapa_id = (select id
                                         from upo_mapas
                                        where url_completa = p_contents(i).url_nodo);
                end if;
             end if;          
          exception
             when others then
                  dbms_output.put_line('Directory: ' || vDirectory || ' - Error: ' || sqlerrm); 
          end;                         
      end loop;  
   end;
    
begin
   clean_target_directory(p_destino);   
   vSourceDirectories:= get_source_directories(p_origen);
   add_source_dirs_to_target(vSourceDirectories, p_destino, p_max_contenidos);
      
   commit;                  
end;
/


CREATE OR REPLACE function UJI_PORTAL.get_inicio_vigencia_contenido(p_contenido number) return date is
   vVigencia date;
begin
   select inicio
     into vVigencia
     from (select to_date(to_char(fecha, 'dd/mm/yyyy') || ' ' || to_char(hora_inicio, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') inicio,
                  to_date(to_char(fecha, 'dd/mm/yyyy') || ' ' || to_char(hora_fin, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') fin
             from upo_vigencias_objetos
            where objeto_id = p_contenido
            order by fecha asc)
    where rownum = 1;
    
   return vVigencia;
end;
/

alter table upo_mapas add titulo_publicacion varchar2(4000);

CREATE OR REPLACE function UJI_PORTAL.get_primera_fecha_vigencia(p_objeto_id number) return date is
  vFecha date;

  begin

    select nvl(min(to_date(to_char(fecha, 'dd/mm/rrrr') || to_char(hora_inicio, 'hh24:mi'), 'dd/mm/rrrrhh24:mi')), trunc(sysdate))
    into vFecha
    from upo_vigencias_objetos
    where objeto_id = p_objeto_id;

    return vFecha;
  end;
/

create or replace force view UJI_PORTAL.UPO_VW_PUBLICACION
(
    CONTENIDO_ID
  , CONTENIDO_IDIOMA_ID
  , URL_NODO
  , URL_NODO_ORIGINAL
  , URL_CONTENIDO
  , URL_COMPLETA
  , URL_COMPLETA_ORIGINAL
  , URL_NUM_NIVELES
  , URL_DESTINO
  , NIVEL
  , IDIOMA
  , TITULO
  , SUBTITULO
  , TITULO_LARGO
  , RESUMEN
  , MIME_TYPE
  , ES_HTML
  , CONTENIDO
  , FECHA_MODIFICACION
  , NODO_MAPA_ORDEN
  , CONTENIDO_ORDEN
  , TIPO_CONTENIDO
  , AUTOR
  , ORIGEN_NOMBRE
  , ORIGEN_URL
  , LUGAR
  , PROXIMA_FECHA_VIGENCIA
  , PRIMERA_FECHA_VIGENCIA
) as
  select o.id contenido_id
    , oi.id contenido_idioma_id
    , m.url_completa url_nodo
    , m.url_completa url_nodo_original
    , o.url_path url_contenido
    , m.url_completa || o.url_path url_completa
    , m.url_completa || o.url_path url_completa_original
    , times_value_in_string(m.url_completa, '/') niveles
    , oi.enlace_destino
    , nvl(mp.nivel, 1)
    , i.codigo_iso idioma
    , oi.titulo
    , oi.subtitulo
    , oi.titulo_largo
    , oi.resumen
    , oi.mime_type
    , oi.html es_html
    , decode(oi.html, 'S', oi.contenido, null) contenido
    , oi.fecha_modificacion
    , m.orden nodo_mapa_orden
    , mo.orden contenido_orden
    , 'NORMAL' tipo_contenido
    , (select nvl(otros_autores, (select upper(nombre || ' ' || apellido1 || ' ' || apellido2)
                                  from upo_ext_personas
                                  where id = per_id_responsable))
       from upo_objetos
       where id = o.id)
    autor
    , oin.nombre origen_nombre
    , oin.url origen_url
    , o.lugar
    , get_proxima_fecha_vigencia(o.id) proxima_fecha_vigencia
    , get_primera_fecha_vigencia(o.id) primera_fecha_vigencia
  from upo_mapas m
    , upo_mapas_plantillas mp
    , upo_mapas_objetos mo
    , upo_objetos o
    , upo_objetos_idiomas oi
    , upo_idiomas i
    , upo_origenes_informacion oin
  where m.id = mo.mapa_id
        and m.id = mp.mapa_id(+)
        and o.id = mo.objeto_id
        and o.id = oi.objeto_id
        and i.id = oi.idioma_id
        and mo.tipo = 'NORMAL'
        and o.origen_informacion_id = oin.id(+)
  union all
  select o.id contenido_id
    , oi.id contenido_idioma_id
    , m.url_completa url_nodo
    , (select xm.url_completa
       from upo_mapas xm, upo_mapas_objetos xmo
       where xm.id = xmo.mapa_id
             and o.id = xmo.objeto_id
             and xmo.tipo = 'NORMAL')
    url_nodo_original
    , o.url_path url_contenido
    , m.url_completa || o.url_path url_completa
    , (select xm.url_completa || o.url_path
       from upo_mapas xm, upo_mapas_objetos xmo
       where xm.id = xmo.mapa_id
             and o.id = xmo.objeto_id
             and xmo.tipo = 'NORMAL')
    url_completa_original
    , times_value_in_string(m.url_completa, '/') niveles
    , oi.enlace_destino
    , nvl(mp.nivel, 1)
    , i.codigo_iso idioma
    , oi.titulo
    , oi.subtitulo
    , oi.titulo_largo
    , oi.resumen
    , oi.mime_type
    , oi.html es_html
    , decode(oi.html, 'S', oi.contenido, null) contenido
    , oi.fecha_modificacion
    , m.orden nodo_mapa_orden
    , mo.orden contenido_orden
    , 'LINK' tipo_contenido
    , (select nvl(otros_autores, (select upper(nombre || ' ' || apellido1 || ' ' || apellido2)
                                  from upo_ext_personas
                                  where id = per_id_responsable))
       from upo_objetos
       where id = o.id)
    autor
    , oin.nombre origen_nombre
    , oin.url origen_url
    , o.lugar
    , get_proxima_fecha_vigencia(o.id) proxima_fecha_vigencia
    , get_primera_fecha_vigencia(o.id) primera_fecha_vigencia
  from upo_mapas m
    , upo_mapas_plantillas mp
    , upo_mapas_objetos mo
    , upo_objetos o
    , upo_objetos_idiomas oi
    , upo_idiomas i
    , upo_origenes_informacion oin
  where m.id = mo.mapa_id
        and m.id = mp.mapa_id(+)
        and o.id = mo.objeto_id
        and o.id = oi.objeto_id
        and i.id = oi.idioma_id
        and mo.tipo = 'LINK'
        and o.origen_informacion_id = oin.id(+);



create or replace force view UJI_PORTAL.UPO_VW_MODERACION
(
    id,
    mapa_objeto_id,
    objeto_id,
    mapa_id,
    estado_moderacion,
    per_id_moderacion,
    per_id_propuesta,
    texto_rechazo,
    objeto_url_path,
    url_completa,
    mapa_url_completa,
    persona_id,
    objeto_titulo
) as
  select mapa_objeto_id || '-' || persona_id,
    mapa_objeto_id,
    objeto_id,
    mapa_id,
    estado_moderacion,
    per_id_moderacion,
    per_id_propuesta,
    texto_rechazo,
    objeto_url_path,
    url_completa,
    mapa_url_completa,
    persona_id,
    (case
     when titulo_ca is not null then titulo_ca
     when titulo_es is not null then titulo_es
     when titulo_en is not null then titulo_en
     else null
     end) titulo
  from (
    select distinct
      mo.id mapa_objeto_id,
      mo.objeto_id objeto_id,
      mo.mapa_id mapa_id,
      mo.estado_moderacion estado_moderacion,
      mo.per_id_moderacion per_id_moderacion,
      mo.per_id_propuesta per_id_propuesta,
      mo.texto_rechazo texto_rechazo,
      o.url_path objeto_url_path,
      m.url_completa || o.url_path url_completa,
      m.url_completa mapa_url_completa,
      p.id persona_id,
      (select titulo from upo_objetos_idiomas oi, upo_idiomas i where oi.objeto_id =o.id and i.id = oi.idioma_id and upper(i.codigo_iso) = 'CA') titulo_ca,
      (select titulo from upo_objetos_idiomas oi, upo_idiomas i where oi.objeto_id =o.id and i.id = oi.idioma_id and upper(i.codigo_iso) = 'ES') titulo_es,
      (select titulo from upo_objetos_idiomas oi, upo_idiomas i where oi.objeto_id =o.id and i.id = oi.idioma_id and upper(i.codigo_iso) = 'EN') titulo_en
    from upo_mapas_objetos mo,
      upo_objetos o,
      upo_mapas m,
      upo_ext_personas p
    where mo.tipo = 'LINK'
          and mo.mapa_id = m.id
          and mo.objeto_id  = o.id
          and p.id in (
      select distinct per_id
      from upo_franquicias_accesos fa,
        upo_franquicias f,
        upo_mapas m2
      where m2.franquicia_id = f.id
            and fa.franquicia_id = f.id
            and m.url_completa like m2.url_completa||'%'
    )
  )