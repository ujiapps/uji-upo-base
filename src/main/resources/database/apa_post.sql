-- Vista para control de accesos

grant select on xpfdm.xpf_vmc_perfiles_per to uji_portal;
grant select on gri_per.per_cuentas to uji_portal;

create or replace view uji_portal.apa_vw_personas_items
(
  persona_id,
  aplicacion_id,
  aplicacion_codigo,
  item_id,
  jsfile,
  role 
)
as
  with base as 
  (select ie.persona_id,
          a.id aplicacion_id,
          a.codigo aplicacion_codigo,
          i.id item_id,
          i.jsfile,
          ie.tipo_id,
          1 orden
     from uji_portal.apa_ext_items_extras ie,
          uji_portal.apa_ext_aplicaciones_items i,
          uji_portal.apa_ext_aplicaciones a
    where ie.item_id = i.id
      and i.aplicacion_id = a.id
    union
   select nvl (pp.persona_id, ppl.persona_id) persona_id,
          a.id,
          a.codigo,
          i.id,
          i.jsfile,
          ip.tipo_id,
          2 orden
     from uji_portal.apa_ext_perfiles_per pp,
          uji_portal.apa_ext_items_perfiles ip,
          uji_portal.apa_ext_personas_plazas ppl,
          uji_portal.apa_ext_aplicaciones_items i,
          uji_portal.apa_ext_aplicaciones a
    where ip.item_id = i.id  
      and pp.perfil_id = ip.perfil_id
      and i.aplicacion_id = a.id
      and pp.plaza_id = ppl.plaza_id(+)
    union
   select nvl (ae.persona_id, ppl.persona_id) persona_id,
          a.id,
          a.codigo,
          i.id,
          i.jsfile,
          ae.tipo_id,
          3 orden
     from uji_portal.apa_ext_aplicaciones_items i,
          uji_portal.apa_ext_aplicaciones_extras ae,
          uji_portal.apa_ext_personas_plazas ppl,
          uji_portal.apa_ext_aplicaciones a
    where i.aplicacion_id = ae.aplicacion_id
      and i.aplicacion_id = a.id
      and ae.plaza_id = ppl.plaza_id(+)
    union
   select nvl (pp.persona_id, ppl.persona_id) persona_id,
          a.id,
          a.codigo,
          i.id,
          i.jsfile,
          ap.tipo_id,
          4 orden
     from uji_portal.apa_ext_perfiles_per pp,
          uji_portal.apa_ext_aplicaciones_items i,
          uji_portal.apa_ext_aplicaciones_perfiles ap,
          uji_portal.apa_ext_personas_plazas ppl,
          uji_portal.apa_ext_aplicaciones a
    where pp.perfil_id = ap.perfil_id
      and i.aplicacion_id = ap.aplicacion_id
      and i.aplicacion_id = a.id
      and pp.plaza_id = ppl.plaza_id(+)
 order by 4)
   select b.persona_id, b.aplicacion_id, b.aplicacion_codigo, b.item_id, b.jsfile, t.nombre role
     from base b, uji_portal.apa_ext_tipos t
    where b.tipo_id = t.id
      and b.orden = (select min(b2.orden) 
                       from base b2 
                      where b.persona_id = b2.persona_id 
                        and b.item_id = b2.item_id);
        
        
        