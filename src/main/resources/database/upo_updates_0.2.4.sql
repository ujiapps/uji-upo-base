update upo_mapas_plantillas set plantilla_id = 5 where plantilla_id = 7693;

delete upo_plantillas where id = 5;

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_SYNC_CONTENIDOS
(
   OBJETO_ID,
   URL_NODO,
   URL_PATH,
   ORDEN,
   PRIORIDAD,
   INICIO_VIGENCIA
)
AS
   SELECT   o.id,
            m.url_completa,
            o.url_path,
            mo.orden,
            (SELECT   DECODE (prioridad,
                              'URGENTE',
                              1,
                              'ALTA',
                              2,
                              'NORMAL',
                              3,
                              'BAJA',
                              4)
               FROM   upo_objetos_prioridades
              WHERE   objeto_id = o.id
                      AND TRUNC (SYSDATE) BETWEEN TRUNC(NVL (fecha_inicio,
                                                             SYSDATE - 1))
                                              AND  TRUNC(NVL (fecha_fin,
                                                              SYSDATE + 1))
                      AND SYSDATE BETWEEN TO_DATE (
                                             TO_CHAR (SYSDATE, 'dd/mm/yyyy')
                                             || ' '
                                             || TO_CHAR (
                                                   NVL (hora_inicio, SYSDATE),
                                                   'hh24:mi:ss'
                                                ),
                                             'dd/mm/yyyy hh24:mi:ss'
                                          )
                                      AND  TO_DATE (
                                              TO_CHAR (SYSDATE, 'dd/mm/yyyy')
                                              || ' '
                                              || TO_CHAR (
                                                    NVL (hora_fin, SYSDATE),
                                                    'hh24:mi:ss'
                                                 ),
                                              'dd/mm/yyyy hh24:mi:ss'
                                           )
                      AND ROWNUM = 1)
               prioridad,
            get_inicio_vigencia_contenido (o.id) inicio_vigencia
     FROM   upo_mapas m, upo_mapas_objetos mo, upo_objetos o
    WHERE       m.id = mo.mapa_id
            AND o.id = mo.objeto_id             -- Cualquier contenido visible
            AND mo.tipo IN ('NORMAL', 'LINK')
            AND o.visible = 'S'
            AND (                                  -- No hay vigencia definida
                 NOT EXISTS (SELECT   id
                               FROM   upo_vigencias_objetos vo
                              WHERE   vo.objeto_id = o.id)
                 OR        -- Vigencia de 72 horas y dentro de rango de fechas
                   EXISTS
                      (SELECT   id
                         FROM   upo_vigencias_objetos vo
                        WHERE   vo.objeto_id = o.id
                                AND TRUNC (fecha) IN
                                         (TRUNC (SYSDATE),
                                          TRUNC (SYSDATE + 1),
                                          TRUNC (SYSDATE + 2),
                                          TRUNC (SYSDATE + 3))
                                AND SYSDATE BETWEEN TO_DATE (
                                                       TO_CHAR (SYSDATE,
                                                                'dd/mm/yyyy')
                                                       || ' '
                                                       || TO_CHAR (
                                                             NVL (
                                                                hora_inicio,
                                                                SYSDATE
                                                             ),
                                                             'hh24:mi:ss'
                                                          ),
                                                       'dd/mm/yyyy hh24:mi:ss'
                                                    )
                                                AND  TO_DATE (
                                                        TO_CHAR (
                                                           SYSDATE,
                                                           'dd/mm/yyyy'
                                                        )
                                                        || ' '
                                                        || TO_CHAR (
                                                              NVL (hora_fin,
                                                                   SYSDATE),
                                                              'hh24:mi:ss'
                                                           ),
                                                        'dd/mm/yyyy hh24:mi:ss'
                                                     )))
            -- Obligatorio en CA y ES
            AND EXISTS
                  (SELECT   i.codigo_iso
                     FROM   upo_objetos_idiomas oi, upo_idiomas i
                    WHERE       i.id = oi.idioma_id
                            AND lower(i.codigo_iso) = 'ca'
                            AND o.id = oi.objeto_id)
            AND EXISTS
                  (SELECT   i.codigo_iso
                     FROM   upo_objetos_idiomas oi, upo_idiomas i
                    WHERE       i.id = oi.idioma_id
                            AND lower(i.codigo_iso) = 'es'
                            AND o.id = oi.objeto_id);

CREATE OR REPLACE procedure UJI_PORTAL.sync_contenidos (
   p_origen         varchar2, 
   p_destino        varchar2, 
   p_max_contenidos number
) 
is
   type t_content is record (
      objeto_id number,
      url_nodo varchar2(4000),
      url_path varchar2(4000)
   );
  
   type t_contents is table of t_content;
   type t_array_output is table of varchar2(32767);
      
   vSourceContents t_contents;         
   vTargetContents t_contents;
  
   procedure clean_target_directory(p_destino varchar2) is
   begin
      delete from upo_mapas_objetos
       where mapa_id in (select id 
                           from upo_mapas
                          where url_completa like p_destino || '_%');
     
      delete from upo_mapas
       where url_completa like p_destino || '_%';
   end; 
  
   function get_source_contents(p_origen varchar2)
      return t_contents
   is
      vContenidos t_contents;
   begin
      execute immediate 'select objeto_id, url_nodo, url_path
                           from (select objeto_id, url_nodo, url_path
                                   from upo_vw_sync_contenidos
                                  where url_nodo like ''' || p_origen || '%''
                                  order by prioridad, inicio_vigencia, orden)'
        bulk collect into vContenidos;
        
      return vContenidos;  
   end;
  
   function varchar_to_array(
      p_varchar   in varchar2,
      p_separador in varchar2:= ','
   )
      return t_array_output
   is
      vCadena varchar2(32767):= p_varchar;
      vRdo    t_array_output:= t_array_output();
      vAux    varchar2(400);
   begin
      if substr(p_varchar, length(p_varchar)) != p_separador then
         vCadena:= vCadena||p_separador;
      end if;
      
      while instr(vCadena, p_separador) is not null loop
            vAux:= substr(vCadena, 1, instr(vCadena, p_separador)-1);
         vCadena:= substr(vCadena, instr(vCadena, p_separador)+1);

         vRdo.extend;
         vRdo(vRdo.count):= vAux;
      end loop;
       
      return vRdo;
   end;
   
   function get_last_directory_on_path(p_path varchar2)
      return varchar2
   is
      vDatos t_array_output;
   begin
      vDatos:= varchar_to_array(p_path, '/');
      return vDatos(vDatos.count);
   end;     
   
   function contains_path(vPaths t_array_output, vPath varchar2) 
      return boolean
   is
   begin
      for i in 1..vPaths.count loop
          if vPaths(i) = vPath then
             return true;
          end if;
      end loop;
      
      return false;
   end;
  
   procedure add_source_contents_to_target(p_contents t_contents, p_destino varchar2, p_max_contenidos number) is
      vDirectory  varchar2(4000);
      vNewNode    number;
      vTargetNode number;
      vFranquicia number; 
      vMenu       number;
      vTargetFolders t_contents;    
      vAux        varchar2(4000);  
      
      vAcceptContent boolean;
      vPaths      t_array_output:= t_array_output();
      
   begin
      select id, franquicia_id, menu_id
        into vTargetNode, vFranquicia, vMenu
        from upo_mapas
       where url_completa = p_destino;
       
      for i in 1..p_contents.count loop
          begin         
             vAux:= replace(p_contents(i).url_nodo, '/', '-');
             vDirectory:= substr(vAux, 2, length(vAux)-2);
             vAcceptContent:= false;
          
             if not contains_path(vPaths, vDirectory) then
                if vPaths.count < p_max_contenidos then
                   insert into upo_mapas(id, url_path, franquicia_id, url_completa, menu_id, mapa_id, menu_heredado, orden)
                     values (hibernate_sequence.nextval,vDirectory, vFranquicia, p_destino || vDirectory || '/', vMenu, vTargetNode, 1, 10) 
                   returning id into vNewNode;

                   vPaths.extend;          
                   vPaths(vPaths.count):= vDirectory;
                   vAcceptContent:= true;
                end if;
             else
                select id
                  into vNewNode
                  from upo_mapas
                 where url_completa = p_destino || vDirectory || '/';
              
                vAcceptContent:= true;          
             end if;

             if vAcceptContent then
                insert into upo_mapas_objetos (id, mapa_id, objeto_id, estado_moderacion, tipo)
                  values (hibernate_sequence.nextval, vNewNode, p_contents(i).objeto_id, 'ACEPTADO', 'LINK');
             end if;
          
             commit;
          exception
             when others then
                  dbms_output.put_line('Directory: ' || vDirectory || ' - Error: ' || sqlerrm); 
          end;                         
      end loop;  
   end;
     
begin
   clean_target_directory(p_destino);   
   vSourceContents:= get_source_contents(p_origen);
   add_source_contents_to_target(vSourceContents, p_destino, p_max_contenidos);         
end;
/
