-- GENERACIÓN DE LA ESTRUCTURA EN EXPLOTACION
-- 
-- Lanzar con el usuario SYSEXP
--

-- Generación de los usuarios y otros objetos
upo_pre.sql

-- Generación de las tablas, indices, etc. de GDO (fichero del Datamodeler)
upo_schema.sql

-- Generación de las tablas, indices, etc. de APA (fichero del Datamodeler)
apa_schema.sql

-- Generación de vistas, etc.
apa_post.sql

-- Conversión de las tablas EXT a VW y asignación de permisos al usuario puente
upo_post.sql

-- Carga de datos iniciales UPO
upo_data_exp.sql

-- Carga de datos iniciales APA
apa_data_exp.sql

