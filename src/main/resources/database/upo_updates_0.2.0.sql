insert into upo_plantillas (id, tipo, nombre, fichero)
 values (hibernate_sequence.nextval, 'WEB', 'Base', 'page-base');
 
insert into upo_plantillas (id, tipo, nombre, fichero)
 values (hibernate_sequence.nextval, 'WEB', 'Reportaje', 'page-reportaje');

insert into upo_plantillas (id, tipo, nombre, fichero)
 values (hibernate_sequence.nextval, 'WEB', 'Grid imágenes', 'page-imagenes-grid');

insert into upo_plantillas (id, tipo, nombre, fichero)
 values (hibernate_sequence.nextval, 'WEB', 'Noticia', 'page-noticia');

 
alter table upo_mapas_plantillas add nivel number default '1' not null;

--DML ANTERIORES YA EJECUTADOS
 
ALTER TABLE UPO_OBJETOS DROP COLUMN PER_ID_AUTORIZADO

ALTER TABLE UPO_OBJETOS_LOG DROP COLUMN PER_ID_AUTORIZADO

ALTER TABLE UPO_OBJETOS ADD OTROS_AUTORES VARCHAR2(4000)    

ALTER TABLE UPO_OBJETOS_LOG ADD OTROS_AUTORES VARCHAR2(4000)

ALTER TABLE UPO_OBJETOS_IDIOMAS ADD ENLACE_DESTINO VARCHAR2(4000)

ALTER TABLE UPO_OBJETOS_IDIOMAS_LOG ADD ENLACE_DESTINO VARCHAR2(4000)

