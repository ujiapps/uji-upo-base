CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_EXT_PERSONAS (ID, NOMBRE, APELLIDO1, APELLIDO2, NOMBRE_COMPLETO)
AS
  SELECT id, nombre, apellido1, apellido2, nombre || ' ' || apellido1 || ' ' || apellido2 nombre_completo
    FROM per_personas p, per_personas_subvinculos ps
   WHERE p.id = ps.per_id AND ps.svi_vin_id = 4 AND ps.fecha_fin IS NULL
  UNION
  SELECT id, nombre, apellido1, apellido2, nombre || ' ' || apellido1 || ' ' || apellido2 nombre_completo
    FROM per_personas
   WHERE id IN (3674, 23553, 40000);

insert into upo_franquicias_accesos (id, franquicia_id, per_id, tipo)
  values (hibernate_sequence.nextval, (select franquicia_id
                                         from upo_mapas
                                        where url_completa = '/'), 40000, 'ADMINISTRADOR');


create or replace view upo_vw_publicacion (
       contenido_id,
       contenido_idioma_id, 
       url_nodo,
       url_contenido, 
       url_completa, 
       idioma,
       titulo,
       subtitulo,
       titulo_largo,
       resumen,
       mime_type,
       es_html,
       contenido,
       fecha_modificacion) as
select o.id contenido_id,
       oi.id contenido_idioma_id, 
       m.url_completa,
       o.url_path, 
       m.url_completa || o.url_path, 
       i.codigo_iso idioma,
       oi.titulo,
       oi.subtitulo,
       oi.titulo_largo,
       oi.resumen,
       oi.mime_type,
       oi.html es_html,
       oi.contenido,
       oi.fecha_modificacion
  from upo_mapas m, upo_mapas_objetos mo, upo_objetos o, upo_objetos_idiomas oi, upo_idiomas i
 where m.id = mo.mapa_id
   and o.id = mo.objeto_id
   and o.id = oi.objeto_id
   and i.id = oi.idioma_id
   and mo.tipo = 'NORMAL'