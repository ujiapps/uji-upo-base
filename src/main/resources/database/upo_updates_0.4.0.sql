CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_REVISTA(CONTENIDO_IDIOMA_ID, TITULO_LARGO, OTROS_AUTORES, RESUMEN, URL_COMPLETA, PRIMERA_FECHA_VIGENCIA, TITULO_LARGO_SIN_ACENTOS, RESUMEN_SIN_ACENTOS, OTROS_AUTORES_SIN_ACENTOS, TAGS,
    URL_COMPLETA_BUSQUEDA, MAPA_URL_COMPLETA) AS
  SELECT
    oi.id                            contenido_idioma_id,
    oi.titulo_largo,
    o.otros_autores,
    oi.resumen,
    m.url_completa,
    get_primera_fecha_vigencia(o.id) primera_fecha_vigencia,
    quita_acentos(titulo_largo)      titulo_largo_sin_acentos,
    quita_acentos(resumen)           resumen_sin_acentos,
    quita_acentos(otros_autores)     otros_autores_sin_acentos,
    quita_acentos(concat_tags(o.id)) tags,
    quita_acentos(m.url_completa)    url_completa_busqueda,
    m.url_completa
  FROM upo_mapas_objetos mo, upo_objetos o, upo_mapas m, upo_objetos_idiomas oi, upo_idiomas i
  WHERE mo.mapa_id = m.id
        AND mo.objeto_id = o.id
        AND oi.objeto_id = o.id
        AND i.id = oi.idioma_id
        AND ((mo.tipo = 'NORMAL')
             OR (mo.tipo = 'LINK'
                 AND mo.estado_moderacion = 'ACEPTADO'))
        AND oi.tipo_formato = 'BINARIO'
        AND (instr(mime_type, 'image') > 0
             OR mime_type = 'application/pdf')
        AND m.url_completa LIKE '/com/revista/base/%'
        AND lower(i.codigo_iso) = 'ca'
  UNION
  SELECT
    r.c_id                                                                        contenido_idioma_id,
    titular                                                                       titulo_largo,
    m.nombre                                                                      otros_autores,
    resum                                                                         resumen,
    '/upo/rest/revista/' || xp_get_num_fichero(r.c_id) || '?revistaId=' || r.c_id url_completa,
    data_publicacio                                                               data_publicacio,
    quita_acentos(titular)                                                        titulo_largo_sin_acentos,
    quita_acentos(resum)                                                          resumen_sin_acentos,
    quita_acentos(m.nombre)                                                       otros_autores_sin_acentos,
    quita_acentos(descriptors)                                                    tags,
    quita_acentos(sr.nom_seccio)                                                  url_completa_busqueda,
    NULL                                                                          mapa_url_completa
  FROM xpfdm.xpv$revista r, xpfdm.xp$lov_medios m, xpfdm.xp$seccions_revista sr
  WHERE m.c_id = r.mitja
        AND r.seccio = sr.c_id;


CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_PUBLICACION(CONTENIDO_ID, CONTENIDO_IDIOMA_ID, URL_NODO, URL_NODO_ORIGINAL, URL_CONTENIDO, URL_COMPLETA, URL_COMPLETA_ORIGINAL, URL_NUM_NIVELES, URL_DESTINO, NIVEL, IDIOMA, TITULO, SUBTITULO,
    TITULO_LARGO, RESUMEN, MIME_TYPE, ES_HTML, CONTENIDO, FECHA_MODIFICACION, NODO_MAPA_ORDEN, NODO_MAPA_PADRE_ORDEN, CONTENIDO_ORDEN, TIPO_CONTENIDO, AUTOR, ORIGEN_NOMBRE, ORIGEN_URL, LUGAR, PROXIMA_FECHA_VIGENCIA, PRIMERA_FECHA_VIGENCIA,
    METADATA, TITULO_NODO, ORDEN, PRIORIDAD, DISTANCIA_SYSDATE, TIPO_FORMATO, ATRIBUTOS, VISIBLE, TEXTO_NOVISIBLE, TAGS) AS
  SELECT
    o.id                                                                       contenido_id,
    oi.id                                                                      contenido_idioma_id,
    m.url_completa                                                             url_nodo,
    m.url_completa                                                             url_nodo_original,
    o.url_path                                                                 url_contenido,
    m.url_completa || o.url_path                                               url_completa,
    m.url_completa || o.url_path                                               url_completa_original,
    url_num_niveles                                                            niveles,
    oi.enlace_destino,
    nvl(mp.nivel, 1),
    i.codigo_iso                                                               idioma,
    oi.titulo,
    oi.subtitulo,
    oi.titulo_largo,
    oi.resumen,
    oi.mime_type,
    oi.html                                                                    es_html,
    decode(oi.html, 'S', oi.contenido, NULL)                                   contenido,
    oi.fecha_modificacion,
    m.orden                                                                    nodo_mapa_orden,
    nvl((SELECT mpadre.orden
         FROM upo_mapas mpadre
         WHERE m.mapa_id = mpadre.id),
        -1)
                                                                               orden_padre,
    mo.orden                                                                   contenido_orden,
    'NORMAL'                                                                   tipo_contenido,
    (SELECT nvl(otros_autores,
                (SELECT upper(nombre || ' ' || apellido1 || ' ' || apellido2)
                 FROM upo_ext_personas
                 WHERE id = per_id_responsable))
     FROM upo_objetos
     WHERE id = o.id)
                                                                               autor,
    decode(i.codigo_iso, 'es', oin.nombre_es, 'en', oin.nombre_en, oin.nombre) origen_nombre,
    oin.url                                                                    origen_url,
    o.lugar,
    get_proxima_fecha_vigencia(o.id)                                           proxima_fecha_vigencia,
    get_primera_fecha_vigencia(o.id)                                           primera_fecha_vigencia,
    concat_metadata(o.id, i.codigo_iso)                                        metadata,
    decode(i.codigo_iso, 'es', m.titulo_publicacion_es, 'en', m.titulo_publicacion_en,
           m.titulo_publicacion_ca)                                            tituloNodo,
    mo.orden,
    get_prioridad(o.id)                                                        prioridad,
    abs(get_inicio_vigencia_contenido(o.id) - sysdate)                         distancia_sysdate,
    oi.tipo_formato,
    concat_atributos(oi.id),
    o.visible,
    o.texto_novisible,
    concat_tags(o.id)
  FROM upo_mapas m, upo_mapas_plantillas mp, upo_mapas_objetos mo, upo_objetos o, upo_objetos_idiomas oi, upo_idiomas i,
    upo_origenes_informacion oin
  WHERE m.id = mo.mapa_id
        AND m.id = mp.mapa_id (+)
        AND o.id = mo.objeto_id
        AND o.id = oi.objeto_id
        AND i.id = oi.idioma_id
        AND mo.tipo = 'NORMAL'
        AND o.origen_informacion_id = oin.id (+)
        AND m.url_completa NOT LIKE '/paperera/%'
  UNION ALL
  SELECT
    o.id                                                                       contenido_id,
    oi.id                                                                      contenido_idioma_id,
    m.url_completa                                                             url_nodo,
    xm.url_completa                                                            url_nodo_original,
    o.url_path                                                                 url_contenido,
    m.url_completa || o.url_path                                               url_completa,
    xm.url_completa || o.url_path                                              url_completa_original,
    m.url_num_niveles                                                          niveles,
    oi.enlace_destino,
    nvl(mp.nivel, 1),
    i.codigo_iso                                                               idioma,
    oi.titulo,
    oi.subtitulo,
    oi.titulo_largo,
    oi.resumen,
    oi.mime_type,
    oi.html                                                                    es_html,
    decode(oi.html, 'S', oi.contenido, NULL)                                   contenido,
    oi.fecha_modificacion,
    m.orden                                                                    nodo_mapa_orden,
    nvl((SELECT mpadre.orden
         FROM upo_mapas mpadre
         WHERE m.mapa_id = mpadre.id),
        -1)
                                                                               orden_padre,
    mo.orden                                                                   contenido_orden,
    'LINK'                                                                     tipo_contenido,
    (SELECT nvl(otros_autores,
                (SELECT upper(nombre || ' ' || apellido1 || ' ' || apellido2)
                 FROM upo_ext_personas
                 WHERE id = per_id_responsable))
     FROM upo_objetos
     WHERE id = o.id)
                                                                               autor,
    decode(i.codigo_iso, 'es', oin.nombre_es, 'en', oin.nombre_en, oin.nombre) origen_nombre,
    oin.url                                                                    origen_url,
    o.lugar,
    get_proxima_fecha_vigencia(o.id)                                           proxima_fecha_vigencia,
    get_primera_fecha_vigencia(o.id)                                           primera_fecha_vigencia,
    concat_metadata(o.id, i.codigo_iso)                                        metadata,
    decode(i.codigo_iso, 'es', m.titulo_publicacion_es, 'en', m.titulo_publicacion_en,
           m.titulo_publicacion_ca)                                            tituloNodo,
    mo.orden,
    get_prioridad(o.id)                                                        prioridad,
    abs(get_inicio_vigencia_contenido(o.id) - sysdate)                         distancia_sysdate,
    oi.tipo_formato,
    concat_atributos(oi.id),
    o.visible,
    o.texto_novisible,
    concat_tags(o.id)
  FROM upo_mapas m, upo_mapas_plantillas mp, upo_mapas_objetos mo, upo_objetos o, upo_objetos_idiomas oi, upo_idiomas i,
    upo_origenes_informacion oin, upo_mapas xm, upo_mapas_objetos xmo
  WHERE m.id = mo.mapa_id
        AND m.id = mp.mapa_id (+)
        AND o.id = mo.objeto_id
        AND o.id = oi.objeto_id
        AND i.id = oi.idioma_id
        AND mo.tipo = 'LINK'
        AND o.origen_informacion_id = oin.id (+)
        AND mo.estado_moderacion = 'ACEPTADO'
        AND xm.id = xmo.mapa_id
        AND o.id = xmo.objeto_id
        AND xmo.tipo = 'NORMAL'
        AND xm.url_completa NOT LIKE '/paperera%';


alter table upo_mapas add (mostrar_recursos_relacionados number(1) default 0 not null);