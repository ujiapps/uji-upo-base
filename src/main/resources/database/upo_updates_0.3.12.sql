CREATE OR REPLACE FUNCTION UJI_PORTAL.concat_tags(v_objeto_id NUMBER)
  RETURN CLOB
IS
  vRdo   CLOB;
  amount INTEGER := 2;
  len    NUMBER;

  CURSOR tags IS
    SELECT tagname || ', ' result
    FROM upo_objetos_tags ot
    WHERE ot.objeto_id = v_objeto_id;

  BEGIN

    dbms_lob.createtemporary(vRdo, TRUE);

    FOR m IN tags LOOP

      dbms_lob.writeappend(vRdo, length(m.result), m.result);

    END LOOP;

    len := dbms_lob.getlength(vRdo);

    IF (len = 0)
    THEN

      RETURN NULL;

    END IF;

    IF (len > amount)
    THEN

      dbms_lob.erase(vRdo, amount, len - amount + 1);
      dbms_lob.trim(vRdo, len - 2);

    END IF;

    RETURN vRdo;
  END;
/

CREATE OR REPLACE FUNCTION xp_get_num_fichero(v_c_id NUMBER)
  RETURN NUMBER AS
  v_fich_id NUMBER;
  v_revista xpfdm.xpv$revista%ROWTYPE;

  BEGIN

    SELECT *
    INTO v_revista
    FROM xpfdm.xpv$revista
    WHERE c_id = v_c_id;

    IF substr(v_revista.url, 1, 5) = '/bin/'
    THEN

      SELECT fich_id
      INTO v_fich_id
      FROM xpfdm.XP$arxius_www a,
        xpfdm.xpf_ficheros f
      WHERE a.url_act = v_revista.url
            AND fich_nombre = 'ARXIUS_WWW/' || a.c_id || '_FITXER_' || a.fitxer;

    ELSIF v_revista.arxiu IS NOT NULL
      THEN

        SELECT fich_id
        INTO v_fich_id
        FROM xpfdm.xpf_ficheros
        WHERE fich_nombre = 'REVISTA/' || v_c_id || '_ARXIU_' || v_revista.arxiu;

    ELSIF v_revista.arxiu_pdf IS NOT NULL
      THEN

        SELECT fich_id
        INTO v_fich_id
        FROM xpfdm.xpf_ficheros
        WHERE fich_nombre = 'REVISTA/' || v_c_id || '_ARXIU_PDF_' || v_revista.arxiu_pdf;

    END IF;

    RETURN v_fich_id;

    EXCEPTION
    WHEN OTHERS THEN
    RETURN NULL;
  END;


CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_REVISTA(CONTENIDO_IDIOMA_ID, TITULO_LARGO, OTROS_AUTORES, RESUMEN, URL_COMPLETA, PRIMERA_FECHA_VIGENCIA, TITULO_LARGO_SIN_ACENTOS, RESUMEN_SIN_ACENTOS, OTROS_AUTORES_SIN_ACENTOS, TAGS,
    URL_COMPLETA_BUSQUEDA) AS
  SELECT
    oi.id                            contenido_idioma_id,
    oi.titulo_largo,
    o.otros_autores,
    oi.resumen,
    m.url_completa,
    get_primera_fecha_vigencia(o.id) primera_fecha_vigencia,
    limpia(titulo_largo)             titulo_largo_sin_acentos,
    limpia(resumen)                  resumen_sin_acentos,
    limpia(otros_autores)            otros_autores_sin_acentos,
    limpia(concat_tags(o.id))        tags,
    limpia(m.url_completa)           url_completa_busqueda
  FROM upo_mapas_objetos mo, upo_objetos o, upo_mapas m, upo_objetos_idiomas oi, upo_idiomas i
  WHERE mo.mapa_id = m.id
        AND mo.objeto_id = o.id
        AND oi.objeto_id = o.id
        AND i.id = oi.idioma_id
        AND ((mo.tipo = 'NORMAL')
             OR (mo.tipo = 'LINK'
                 AND mo.estado_moderacion = 'ACEPTADO'))
        AND oi.tipo_formato = 'BINARIO'
        AND (instr(mime_type, 'image') > 0
             OR mime_type = 'application/pdf')
        AND m.url_completa LIKE '/com/revista/base/%'
        AND lower(i.codigo_iso) = 'ca'
  UNION
  SELECT
    r.c_id                                                                        contenido_idioma_id,
    titular                                                                       titulo_largo,
    m.nombre                                                                      otros_autores,
    resum                                                                         resumen,
    '/upo/rest/revista/' || xp_get_num_fichero(r.c_id) || '?revistaId=' || r.c_id url_completa,
    data_publicacio                                                               data_publicacio,
    limpia(titular)                                                               titulo_largo_sin_acentos,
    limpia(resum)                                                                 resumen_sin_acentos,
    limpia(m.nombre)                                                              otros_autores_sin_acentos,
    limpia(descriptors)                                                           tags,
    limpia(sr.nom_seccio)                                                         url_completa_busqueda
  FROM xpfdm.xpv$revista r, xpfdm.xp$lov_medios m, xpfdm.xp$seccions_revista sr
  WHERE m.c_id = r.mitja
        AND r.seccio = sr.c_id;

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_EXT_FICHEROS(id, nombre, mime_type, contenido) AS
  SELECT
    fich_id           id,
    fich_nombre       nombre,
    fich_content_type content_type,
    fich_cuerpo       contenido
  FROM xpfdm.xpf_ficheros;


CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_EXT_REVISTA(ID, TITULO, AUTOR, FECHA) AS
  SELECT DISTINCT
    r.c_id,
    r.titular         titulo,
    m.nombre          autor,
    r.data_publicacio fecha
  FROM xpfdm.xpv$revista r, xpfdm.xp$lov_medios m
  WHERE m.c_id = r.mitja;

----------------------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION UJI_PORTAL.clob_to_blob(
  p_clob IN CLOB
)
  RETURN BLOB
IS
  BEGIN
    DECLARE
      v_file_blob    BLOB;
      v_file_size    INTEGER := dbms_lob.lobmaxsize;
      v_dest_offset  INTEGER := 1;
      v_src_offset   INTEGER := 1;
      v_blob_csid    NUMBER := dbms_lob.default_csid;
      v_lang_context NUMBER := dbms_lob.default_lang_ctx;
      v_warning      INTEGER;

    BEGIN

      IF p_clob IS NULL
      THEN

        RETURN NULL;

      END IF;

      dbms_lob.createTemporary(v_file_blob, FALSE);
      dbms_lob.convertToBlob(v_file_blob, p_clob, v_file_size, v_dest_offset, v_src_offset, v_blob_csid, v_lang_context,
                             v_warning);
      RETURN v_file_blob;
      EXCEPTION
      WHEN OTHERS THEN
      RETURN NULL;

    END;
  END;
/

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_EXT_MIGRACION(ID, TIPO, URL, TITULO, RESUMEN, TAGS, CONTENIDO, MIME_TYPE) AS
  SELECT
    m.c_id || '-' || m.idio_id || '-MIGRACION' id,
    'TEXTO'                                    tipo,
    url,
    NULL                                       titulo,
    NULL                                       resumen,
    NULL                                       tags,
    clob_to_blob(contenido)                    contenido,
    'text/html'                                mime_type
  FROM xp$migracion m
  UNION ALL
  SELECT
    n.c_id || '-' || n.idio_id || '-NOTICIAS' id,
    'TEXTO'                                   tipo,
    '/' ||
    decode(idio_id,
           1,
           'CA',
           2,
           'ES',
           3,
           'EN') ||
    '/noticies/detall' ||
    '&' ||
    'id_a=' ||
    n.c_id
                                              url,
    titol                                     titulo,
    resum                                     resumen,
    descriptors                               tags,
    clob_to_blob(contingut)                   contenido,
    'text/html'                               mime_type
  FROM xpfdm.xp$noticies n
  UNION ALL
  SELECT
    a.c_id || '-' || a.idio_id || '-AGENDA' id,
    'TEXTO'                                 tipo,
    '/' ||
    decode(idio_id,
           1,
           'CA',
           2,
           'ES',
           3,
           'EN') ||
    '/noticies/agenda/nota' ||
    '&' ||
    'id_a=' ||
    a.c_id
                                            url,
    titol                                   titulo,
    resum                                   resumen,
    descriptors                             tags,
    clob_to_blob(contingut)                 contenido,
    'text/html'                             mime_type
  FROM xpfdm.xp$agenda a
  UNION ALL
  SELECT
    a.c_id || '-' || a.idio_id || '-FICHERO' id,
    'BINARIO'                                tipo,
    url_act                                  url,
    fitxer                                   titulo,
    NULL                                     resumen,
    NULL                                     tags,
    fich_cuerpo                              contenido,
    fich_content_type                        mime_type
  FROM xpfdm.XP$arxius_www a,
    xpfdm.xpf_ficheros f
  WHERE fich_nombre = 'ARXIUS_WWW/' || a.c_id || '_FITXER_' || a.fitxer;


CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_MIGRACION(ID, TIPO, URL) AS
  SELECT
    m.c_id || '-' || m.idio_id || '-MIGRACION' id,
    'TEXTO'                                    texto,
    m.url
  FROM xpfdm.xpf_paginas_url pu, xpfdm.xp$migracion m
  WHERE pu.url = m.url
        AND m.url NOT LIKE '%/9/'
  UNION
  SELECT
    n.c_id || '-' || n.idio_id || '-NOTICIAS'         id,
    'TEXTO'                                           texto,
    '/CA/noticies/detall' || '&' || 'id_a=' || n.c_id url
  FROM xpfdm.xp$noticies n
  WHERE idio_id = 1
  UNION
  SELECT
    a.c_id || '-' || a.idio_id || '-AGENDA'                id,
    'TEXTO'                                                texto,
    '/CA/noticies/agenda/nota' || '&' || 'id_a=' || a.c_id url
  FROM xpfdm.xp$agenda a
  WHERE idio_id = 1
  UNION
  SELECT
    a.c_id || '-' || a.idio_id || '-FICHERO' id,
    'BINARIO'                                texto,
    url_act                                  url
  FROM xpfdm.XP$arxius_www a,
    xpfdm.xpf_ficheros f
  WHERE fich_nombre = 'ARXIUS_WWW/' || a.c_id || '_FITXER_' || a.fitxer;






