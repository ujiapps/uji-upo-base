--Franquicias

INSERT INTO UJI_PORTAL.UPO_FRANQUICIAS (
   ID, NOMBRE) 
VALUES (hibernate_sequence.nextval, 'Portal general UJI');


-- APA Cuentas

Insert into UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS
   (PERSONA_ID, CUENTA)
 Values
   (3245, 'jsoriano');
Insert into UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS
   (PERSONA_ID, CUENTA)
 Values
   (3674, 'agutierr');
Insert into UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS
   (PERSONA_ID, CUENTA)
 Values
   (5121, 'fmelia');
Insert into UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS
   (PERSONA_ID, CUENTA)
 Values
   (7910, 'dgonzale');
Insert into UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS
   (PERSONA_ID, CUENTA)
 Values
   (9792, 'borillo');
Insert into UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS
   (PERSONA_ID, CUENTA)
 Values
   (16989, 'lpascual');
Insert into UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS
   (PERSONA_ID, CUENTA)
 Values
   (23553, 'dcastell');
Insert into UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS
   (PERSONA_ID, CUENTA)
 Values
   (35104, 'lortells');
Insert into UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS
   (PERSONA_ID, CUENTA)
 Values
   (61176, 'fbeltran');
Insert into UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS
   (PERSONA_ID, CUENTA)
 Values
   (64902, 'blanco');
Insert into UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS
   (PERSONA_ID, CUENTA)
 Values
   (65132, 'sgarcia');
Insert into UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS
   (PERSONA_ID, CUENTA)
 Values
   (142385, 'aparisi');
Insert into UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS
   (PERSONA_ID, CUENTA)
 Values
   (145715, 'eredondo');
   
   
-- Acceso franquicias
   
Insert into UJI_PORTAL.UPO_FRANQUICIAS_ACCESOS
   (ID, FRANQUICIA_ID, PER_ID, TIPO)
 Values
   (hibernate_sequence.nextval, (select id from UJI_PORTAL.UPO_FRANQUICIAS where nombre = 'Portal general UJI'),    (select persona_id from UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS where cuenta = 'jsoriano'), 'ADMINISTRADOR');
Insert into UJI_PORTAL.UPO_FRANQUICIAS_ACCESOS
   (ID, FRANQUICIA_ID, PER_ID, TIPO)
 Values
   (hibernate_sequence.nextval, (select id from UJI_PORTAL.UPO_FRANQUICIAS where nombre = 'Portal general UJI'),    (select persona_id from UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS where cuenta = 'agutierr'), 'ADMINISTRADOR');
Insert into UJI_PORTAL.UPO_FRANQUICIAS_ACCESOS
   (ID, FRANQUICIA_ID, PER_ID, TIPO)
 Values
   (hibernate_sequence.nextval, (select id from UJI_PORTAL.UPO_FRANQUICIAS where nombre = 'Portal general UJI'),    (select persona_id from UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS where cuenta = 'fmelia'), 'ADMINISTRADOR');
Insert into UJI_PORTAL.UPO_FRANQUICIAS_ACCESOS
   (ID, FRANQUICIA_ID, PER_ID, TIPO)
 Values
   (hibernate_sequence.nextval, (select id from UJI_PORTAL.UPO_FRANQUICIAS where nombre = 'Portal general UJI'),    (select persona_id from UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS where cuenta = 'dgonzale'), 'ADMINISTRADOR');
Insert into UJI_PORTAL.UPO_FRANQUICIAS_ACCESOS
   (ID, FRANQUICIA_ID, PER_ID, TIPO)
 Values
   (hibernate_sequence.nextval, (select id from UJI_PORTAL.UPO_FRANQUICIAS where nombre = 'Portal general UJI'),    (select persona_id from UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS where cuenta = 'borillo'), 'ADMINISTRADOR');
Insert into UJI_PORTAL.UPO_FRANQUICIAS_ACCESOS
   (ID, FRANQUICIA_ID, PER_ID, TIPO)
 Values
   (hibernate_sequence.nextval, (select id from UJI_PORTAL.UPO_FRANQUICIAS where nombre = 'Portal general UJI'),    (select persona_id from UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS where cuenta = 'lpascual'), 'ADMINISTRADOR');
Insert into UJI_PORTAL.UPO_FRANQUICIAS_ACCESOS
   (ID, FRANQUICIA_ID, PER_ID, TIPO)
 Values
   (hibernate_sequence.nextval, (select id from UJI_PORTAL.UPO_FRANQUICIAS where nombre = 'Portal general UJI'),    (select persona_id from UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS where cuenta = 'dcastell'), 'ADMINISTRADOR');
Insert into UJI_PORTAL.UPO_FRANQUICIAS_ACCESOS
   (ID, FRANQUICIA_ID, PER_ID, TIPO)
 Values
   (hibernate_sequence.nextval, (select id from UJI_PORTAL.UPO_FRANQUICIAS where nombre = 'Portal general UJI'),    (select persona_id from UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS where cuenta = 'lortells'), 'ADMINISTRADOR');
Insert into UJI_PORTAL.UPO_FRANQUICIAS_ACCESOS
   (ID, FRANQUICIA_ID, PER_ID, TIPO)
 Values
   (hibernate_sequence.nextval, (select id from UJI_PORTAL.UPO_FRANQUICIAS where nombre = 'Portal general UJI'),    (select persona_id from UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS where cuenta = 'fbeltran'), 'ADMINISTRADOR');
Insert into UJI_PORTAL.UPO_FRANQUICIAS_ACCESOS
   (ID, FRANQUICIA_ID, PER_ID, TIPO)
 Values
   (hibernate_sequence.nextval, (select id from UJI_PORTAL.UPO_FRANQUICIAS where nombre = 'Portal general UJI'),    (select persona_id from UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS where cuenta = 'blanco'), 'ADMINISTRADOR');
Insert into UJI_PORTAL.UPO_FRANQUICIAS_ACCESOS
   (ID, FRANQUICIA_ID, PER_ID, TIPO)
 Values
   (hibernate_sequence.nextval, (select id from UJI_PORTAL.UPO_FRANQUICIAS where nombre = 'Portal general UJI'),    (select persona_id from UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS where cuenta = 'sgarcia'), 'ADMINISTRADOR');
Insert into UJI_PORTAL.UPO_FRANQUICIAS_ACCESOS
   (ID, FRANQUICIA_ID, PER_ID, TIPO)
 Values
   (hibernate_sequence.nextval, (select id from UJI_PORTAL.UPO_FRANQUICIAS where nombre = 'Portal general UJI'),    (select persona_id from UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS where cuenta = 'aparisi'), 'ADMINISTRADOR');
Insert into UJI_PORTAL.UPO_FRANQUICIAS_ACCESOS
   (ID, FRANQUICIA_ID, PER_ID, TIPO)
 Values
   (hibernate_sequence.nextval, (select id from UJI_PORTAL.UPO_FRANQUICIAS where nombre = 'Portal general UJI'),    (select persona_id from UJI_PORTAL.APA_EXT_PERSONAS_CUENTAS where cuenta = 'eredondo'), 'ADMINISTRADOR');


-- Nodos
   
Insert into UJI_PORTAL.UPO_MAPAS
   (ID, URL_PATH, FRANQUICIA_ID, URL_COMPLETA, NUM_ITEMS, CAJA_ID)
 Values
   (hibernate_sequence.nextval, '/', (select id from UJI_PORTAL.UPO_FRANQUICIAS where nombre = 'Portal general UJI'), '/', 10, 1);
