alter table upo_mapas add enlace_destino varchar2(4000);


create or replace force view UJI_PORTAL.UPO_VW_SYNC_CONTENIDOS
(
   OBJETO_ID
 , URL_NODO
 , ORDEN
 , PRIORIDAD
 , DISTANCIA_SYSDATE
 , FECHA_CREACION
) as
   select o.id
        , m.url_completa
        , mo.orden
        , get_prioridad(o.id) prioridad
        , abs(get_inicio_vigencia_contenido(o.id) - sysdate) distancia_sysdate
        , o.fecha_creacion
     from upo_mapas m, upo_mapas_objetos mo, upo_objetos o
    where m.id = mo.mapa_id
      and o.id = mo.objeto_id -- cualquier contenido visible
      and ((mo.tipo = 'NORMAL')
        or (mo.tipo = 'LINK'
        and mo.estado_moderacion = 'ACEPTADO'))
      and o.visible = 'S'
      and ( -- no hay vigencia definida
           not exists (select id
                         from upo_vigencias_objetos vo
                        where vo.objeto_id = o.id)
        or -- vigencia de 72 horas y dentro de rango de fechas
          exists
              (select id
                 from upo_vigencias_objetos vo
                where vo.objeto_id = o.id
                  and (to_date(to_char(fecha, 'dd/mm/yyyy') || ' ' || to_char(nvl(hora_inicio, sysdate), 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') between sysdate and sysdate + 3
                    or to_date(to_char(fecha, 'dd/mm/yyyy') || ' ' || to_char(nvl(hora_fin, sysdate), 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') between sysdate and sysdate + 3)))
      -- Obligatorio en CA y ES
      and ((exists (select i.codigo_iso
                      from upo_objetos_idiomas oi, upo_idiomas i
                     where i.id = oi.idioma_id
                       and lower(i.codigo_iso) = 'ca'
                       and o.id = oi.objeto_id
                       and nvl(oi.html, 'N') = 'S')
        and exists (select i.codigo_iso
                      from upo_objetos_idiomas oi, upo_idiomas i
                     where i.id = oi.idioma_id
                       and lower(i.codigo_iso) = 'es'
                       and o.id = oi.objeto_id
                       and nvl(oi.html, 'N') = 'S'))
        or (has_tag(o.id, 'megabanner') = 'S'));


CREATE OR REPLACE procedure UJI_PORTAL.sync_contenidos (
   p_origen         varchar2,
   p_destino        varchar2,
   p_max_contenidos number
)
is
   type t_content is record (
      objeto_id number,
      url_nodo varchar2(4000)
   );

   type t_contents is table of t_content;
   type t_array_output is table of varchar2(32767);

   vSourceDirectories t_contents;
   vTargetContents t_contents;

   procedure clean_target_directory(p_destino varchar2) is
   begin
      delete from upo_mapas_objetos
       where mapa_id in (select id
                           from upo_mapas
                          where url_completa like p_destino || '_%');

      delete from upo_mapas
       where url_completa like p_destino || '_%';
   end;

   function get_source_directories(p_origen varchar2)
      return t_contents
   is
      vContenidos t_contents;
   begin
      execute immediate 'select objeto_id, url_nodo
                           from (select objeto_id, url_nodo
                                   from upo_vw_sync_contenidos
                                  where url_nodo like ''' || p_origen || '%''
                                  order by prioridad, distancia_sysdate, orden, fecha_creacion)'
        bulk collect into vContenidos;

      return vContenidos;
   end;

   function varchar_to_array(
      p_varchar   in varchar2,
      p_separador in varchar2:= ','
   )
      return t_array_output
   is
      vCadena varchar2(32767):= p_varchar;
      vRdo    t_array_output:= t_array_output();
      vAux    varchar2(400);
   begin
      if substr(p_varchar, length(p_varchar)) != p_separador then
         vCadena:= vCadena||p_separador;
      end if;

      while instr(vCadena, p_separador) is not null loop
            vAux:= substr(vCadena, 1, instr(vCadena, p_separador)-1);
         vCadena:= substr(vCadena, instr(vCadena, p_separador)+1);

         vRdo.extend;
         vRdo(vRdo.count):= vAux;
      end loop;

      return vRdo;
   end;

   function get_last_directory_on_path(p_path varchar2)
      return varchar2
   is
      vDatos t_array_output;
   begin
      vDatos:= varchar_to_array(p_path, '/');
      return vDatos(vDatos.count);
   end;

   function contains_path(vPaths t_array_output, vPath varchar2)
      return boolean
   is
   begin
      for i in 1..vPaths.count loop
          if vPaths(i) = vPath then
             return true;
          end if;
      end loop;

      return false;
   end;

   procedure add_source_dirs_to_target(p_contents t_contents, p_destino varchar2, p_max_contenidos number) is
      vDirectory  varchar2(4000);

      vTargetNode number;
      vNewNode    number;

      vFranquicia number;
      vMenu       number;
      vAux        varchar2(4000);
      vPaths      t_array_output:= t_array_output();

   begin
      select id, franquicia_id, menu_id
        into vTargetNode, vFranquicia, vMenu
        from upo_mapas
       where url_completa = p_destino;

      for i in 1..p_contents.count loop
          begin
             vAux:= replace(p_contents(i).url_nodo, '/', '-');
             vDirectory:= substr(vAux, 2, length(vAux)-2);

             if not contains_path(vPaths, vDirectory) then
                if vPaths.count < p_max_contenidos then
                   insert into upo_mapas(id, url_path, franquicia_id, url_completa, menu_id, mapa_id, menu_heredado, orden)
                     values (hibernate_sequence.nextval,vDirectory, vFranquicia, p_destino || vDirectory || '/', vMenu, vTargetNode, 1, i)
                     returning id into vNewNode;

                   vPaths.extend;
                   vPaths(vPaths.count):= vDirectory;

                   insert into upo_mapas_objetos (id, mapa_id, objeto_id, estado_moderacion, tipo)
                     select hibernate_sequence.nextval, vNewNode, objeto_id, 'ACEPTADO', 'LINK'
                       from upo_mapas_objetos
                      where mapa_id = (select id
                                         from upo_mapas
                                        where url_completa = p_contents(i).url_nodo);
                end if;
             end if;
          exception
             when others then
                  dbms_output.put_line('Directory: ' || vDirectory || ' - Error: ' || sqlerrm);
          end;
      end loop;
   end;

begin
   clean_target_directory(p_destino);
   vSourceDirectories:= get_source_directories(p_origen);
   add_source_dirs_to_target(vSourceDirectories, p_destino, p_max_contenidos);

   commit;
end;
/