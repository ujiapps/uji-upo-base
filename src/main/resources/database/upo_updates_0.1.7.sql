alter table upo_mapas add orden number default 1 not null;

--ordenar los nodos
begin
declare
  vCuenta number;
  
  cursor nodosMapa is
    select id
      from upo_mapas;
      
  cursor nodosMapaHijosDe(pId number) is
    select id
      from upo_mapas
     where mapa_id = pId;
     
begin

  for n in nodosMapa loop
    
    vCuenta := 1;
    
    for nh in nodosMapaHijosDe(n.id) loop
      
      update upo_mapas
         set orden = vCuenta
       where id = nh.id;
       
      vCuenta := vCuenta + 1;    
          
    end loop;
    
  end loop;
  
  commit;
  
end;
end;