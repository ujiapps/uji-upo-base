CREATE
OR REPLACE FORCE VIEW "UJI_PORTAL"."XPFI_VW_REGIONES" ("ID", "IDIOMA", "NOMBRE") AS
SELECT r.id,
       'ca'        idioma,
       r.nombre_ca nombre
FROM xpfi_regiones r,
     xpfi_aplicaciones a
WHERE a.id > 1000
  AND r.id = a.xir_id
UNION
SELECT r.id,
       'es'        idioma,
       r.nombre_es nombre
FROM xpfi_regiones r,
     xpfi_aplicaciones a
WHERE a.id > 1000
  AND r.id = a.xir_id
UNION
SELECT r.id,
       'en'        idioma,
       r.nombre_uk nombre
FROM xpfi_regiones r,
     xpfi_aplicaciones a
WHERE a.id > 1000
  AND r.id = a.xir_id;

CREATE
OR REPLACE FORCE VIEW UJI_PORTAL.XPFI_VW_APLICACIONES
(REGION_ID, REGION_NOMBRE, APLICACION_ID, ID, IDIOMA,
 NOMBRE, DESCRIPCION, NOMBRE_BUSQUEDA, DESCRIPCION_BUSQUEDA, COLOR, colorclass)
BEQUEATH DEFINER
AS
SELECT a.xir_id                 region_id,
       r.nombre_ca              region_nombre,
       a.id                     aplicacion_id,
       a.xir_id || '-' || a.id  id,
       'ca'                     idioma,
       a.nombre_ca              nombre,
       a.descripcion_ca         descripcion,
       limpia(a.nombre_ca)      nombre_busqueda,
       limpia(a.descripcion_ca) descripcion_busqueda,
       a.color,
       a.colorclass
FROM xpfi_aplicaciones a,
     xpfi_regiones r
WHERE a.id > 1000
  and r.id = a.xir_id
UNION
SELECT a.xir_id                 region_id,
       r.nombre_es              region_nombre,
       a.id                     aplicacion_id,
       a.xir_id || '-' || a.id  id,
       'es'                     idioma,
       a.nombre_es              nombre,
       a.descripcion_es         descripcion,
       limpia(a.nombre_es)      nombre_busqueda,
       limpia(a.descripcion_es) descripcion_busqueda,
       a.color,
       a.colorclass
FROM xpfi_aplicaciones a,
     xpfi_regiones r
WHERE a.id > 1000
  and r.id = a.xir_id
UNION
SELECT a.xir_id                 region_id,
       r.nombre_uk              region_nombre,
       a.id                     aplicacion_id,
       a.xir_id || '-' || a.id  id,
       'en'                     idioma,
       a.nombre_uk              nombre,
       a.descripcion_uk         descripcion,
       limpia(a.nombre_uk)      nombre_busqueda,
       limpia(a.descripcion_uk) descripcion_busqueda,
       a.color,
       a.colorclass
FROM xpfi_aplicaciones a,
     xpfi_regiones r
WHERE a.id > 1000
  and r.id = a.xir_id;


CREATE
OR REPLACE VIEW xpfi_vw_grupos AS
SELECT xia_xir_id                                     region_id,
       xia_id                                         aplicacion_id,
       id                                             grupo_id,
       g.id || '-' || g.xia_id || '-' || g.xia_xir_id id,
       'ca'                                           idioma,
       nombre_ca                                      nombre
FROM xpfi_grupos g
UNION
SELECT xia_xir_id                                     region_id,
       xia_id                                         aplicacion_id,
       id                                             grupo_id,
       g.id || '-' || g.xia_id || '-' || g.xia_xir_id id,
       'es'                                           idioma,
       nombre_es                                      nombre
FROM xpfi_grupos g
UNION
SELECT xia_xir_id                                     region_id,
       xia_id                                         aplicacion_id,
       id                                             grupo_id,
       g.id || '-' || g.xia_id || '-' || g.xia_xir_id id,
       'en'                                           idioma,
       nombre_uk                                      nombre
FROM xpfi_grupos g;

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.XPFI_VW_GRUPOS_ITEMS
(ID, ITEM_ID, REGION_ID, NOMBRE_REGION_CA, NOMBRE_REGION_ES,
 NOMBRE_REGION_UK, APLICACION_ID, NOMBRE_APLICACION_CA, NOMBRE_APLICACION_ES, NOMBRE_APLICACION_UK,
 GRUPO_ID, NOMBRE_ITEM_CA, NOMBRE_ITEM_ES, NOMBRE_ITEM_UK, DESCRIPCION_ITEM_CA,
 DESCRIPCION_ITEM_ES, DESCRIPCION_ITEM_UK, NOMBRE_GRUPO_CA, NOMBRE_GRUPO_ES, NOMBRE_GRUPO_UK,
 NOMBRE_GRUPO_BUSQUEDA_CA, NOMBRE_GRUPO_BUSQUEDA_ES, NOMBRE_GRUPO_BUSQUEDA_UK, URL, PER_ID,
 FAVORITO, ORDEN)
BEQUEATH DEFINER
AS
SELECT i.id || '-' || g.id || '-' || g.xia_id || '-' || g.xia_xir_id id,
       i.id item_id,
       g.xia_xir_id region_id,
       r.nombre_ca nombre_region_ca,
       r.nombre_es nombre_region_es,
       r.nombre_uk nombre_region_uk,
       g.xia_id aplicacion_id,
       a.nombre_ca nombre_aplicacion_ca,
       a.nombre_es nombre_aplicacion_es,
       a.nombre_uk nombre_aplicacion_uk,
       g.id grupo_id,
       i.nombre_ca nombre_item_ca,
       i.nombre_es nombre_item_es,
       i.nombre_uk nombre_item_uk,
       i.descripcion_ca descripcion_item_ca,
       i.descripcion_es descripcion_item_es,
       i.descripcion_uk descripcion_item_uk,
       g.nombre_ca nombre_grupo_ca,
       g.nombre_es nombre_grupo_es,
       g.nombre_uk nombre_grupo_uk,
       limpia(g.nombre_ca) nombre_grupo_busqueda_ca,
       limpia(g.nombre_es) nombre_grupo_busqueda_es,
       limpia(g.nombre_uk) nombre_grupo_busqueda_uk,
       i.url                                                         url,
       p.id
                                                                     per_id,
       (SELECT COUNT (*)
        FROM xpfi_itm_favoritos f
        WHERE f.per_id = p.id AND f.xii_id = i.id)
                                                                     favorito,
       ig.orden
FROM xpfi_items             i,
     xpfi_itm_grp           ig,
     xpfi_grupos            g,
     upo_ext_personas_todas p,
     xpfi_regiones r,
     xpfi_aplicaciones a
WHERE     i.activo = 'S'
  AND i.principal = 'S'
  AND ig.xig_xia_xir_id = g.xia_xir_id
  AND ig.xig_xia_id = g.xia_id
  AND ig.xig_id = g.id
  AND i.id = ig.xii_id
  AND gcp_xpfi.item_accesible (i.id, p.id) != 'N'
          and r.id = g.xia_xir_id
          and a.id = g.xia_id
          and a.xir_id = g.xia_xir_id;

CREATE
OR REPLACE FUNCTION url_to_windowopen(url VARCHAR2)
  RETURN VARCHAR2 IS
BEGIN
RETURN 'javascript:Eujier.doOpenWindow(''' || url || ''')';
END;

CREATE
OR REPLACE VIEW xpfi_vw_avisos AS
SELECT a.id,
       a.aviso_ca,
       a.aviso_es,
       a.aviso_uk,
       p.id persona_id
FROM xpfi_avisos a,
     upo_ext_personas_todas p
WHERE trunc(sysdate) >= trunc(nvl(f_inicio, sysdate))
  AND trunc(sysdate) <= trunc(nvl(f_fin, sysdate))
  AND NOT exists(SELECT av.aviso_id
                 FROM xpfi_avisos_vistos av
                 WHERE av.aviso_id = a.id
                   AND av.per_id = p.id);

CREATE
OR REPLACE FORCE VIEW UJI_PORTAL.XPFI_VW_SECCIONES
(ID, CODIGO, NOMBRE_CA, NOMBRE_ES, NOMBRE_EN,
 COLOR, colorclass, ORDEN, PERSONA_ID, GRUPO_PERFIL_ID, NUM_ITEMS,
 VISTA)
BEQUEATH DEFINER
AS
select s.id,
       s.codigo,
       s.nombre_ca,
       s.nombre_es,
       s.nombre_en,
       s.color,
       s.colorclass,
       sgp.orden,
       p.id persona_id,
       sgp.grupo_perfil_id,
       num_items,
       s.vista
from xpfi_secciones s,
     xpfi_seccion_grp_perfiles sgp,
     xpfi_grupos_perfiles gp,
     upo_ext_personas_todas p
where s.id = sgp.seccion_id
  and gp.id = sgp.grupo_perfil_id
  and get_max_grupo_perfil(p.id) = gp.id;


CREATE
OR REPLACE FORCE VIEW "UJI_PORTAL"."XPFI_VW_ITEMS" ("ID", "NOMBRE_CA", "NOMBRE_ES", "NOMBRE_UK", "DESCRIPCION_CA", "DESCRIPCION_ES", "DESCRIPCION_UK", "URL", "URL_PROCEDIMIENTO", "PER_ID", "FECHA", "FAVORITO") AS
SELECT i.id  id,
       i.nombre_ca,
       i.nombre_es,
       i.nombre_uk,
       i.descripcion_ca,
       i.descripcion_es,
       i.descripcion_uk,
       i.url url,
       i.url_procedimiento,
       p.id  per_id,
       i.fecha,
       (SELECT count(*)
        FROM xpfi_itm_favoritos f
        WHERE f.per_id = p.id
          AND f.xii_id = i.id)
             favorito
FROM xpfi_items i,
     upo_ext_personas_todas p
WHERE i.activo = 'S'
  AND i.principal = 'S'
  AND gcp_xpfi.item_accesible(i.id, p.id) != 'N'
        AND exists(SELECT ig.xii_id
                   FROM xpfi_itm_grp ig
                   WHERE ig.xii_id = i.id);

CREATE
OR REPLACE FORCE VIEW "UJI_PORTAL"."XPFI_VW_ITEMS_BUSQUEDA" ("ID", "NOMBRE_CA", "NOMBRE_ES", "NOMBRE_UK", "DESCRIPCION_CA", "DESCRIPCION_ES", "DESCRIPCION_UK", "NOMBRE_BUSQUEDA_CA", "NOMBRE_BUSQUEDA_ES", "NOMBRE_BUSQUEDA_UK", "DESCRIPCION_BUSQUEDA_CA", "DESCRIPCION_BUSQUEDA_ES", "DESCRIPCION_BUSQUEDA_UK", "URL", "URL_PROCEDIMIENTO", "PER_ID", "FAVORITO") AS
SELECT i.id                     id,
       i.nombre_ca,
       i.nombre_es,
       i.nombre_uk,
       i.descripcion_ca,
       i.descripcion_es,
       i.descripcion_uk,
       limpia(i.nombre_ca)      nombre_busqueda_ca,
       limpia(i.nombre_es)      nombre_busqueda_es,
       limpia(i.nombre_uk)      nombre_busqueda_uk,
       limpia(i.descripcion_ca) descripcion_busqueda_ca,
       limpia(i.descripcion_es) descripcion_busqueda_es,
       limpia(i.descripcion_uk) descripcion_busqueda_uk,
       i.url                    url,
       i.url_procedimiento,
       p.id                     per_id,
       (SELECT count(*)
        FROM xpfi_itm_favoritos f
        WHERE f.per_id = p.id
          AND f.xii_id = i.id)
                                favorito
FROM xpfi_items i,
     upo_ext_personas_todas p
WHERE i.activo = 'S'
  AND i.principal = 'S'
  AND gcp_xpfi.item_accesible(i.id, p.id) != 'N'
        AND exists(SELECT ig.xii_id
                   FROM xpfi_itm_grp ig
                   WHERE ig.xii_id = i.id);

CREATE
OR REPLACE FORCE VIEW "UJI_PORTAL"."XPFI_VW_ITEMS_DESTACADOS" ("ID", "NOMBRE_CA", "DESCRIPCION_CA", "NOMBRE_ES", "DESCRIPCION_ES", "NOMBRE_UK", "DESCRIPCION_UK", "URL", "URL_PROCEDIMIENTO", "PERSONA_ID", "FAVORITO", "GRUPO_PERFIL_ID") AS
SELECT i.id,
       i.nombre_ca,
       i.descripcion_ca,
       i.nombre_es,
       i.descripcion_es,
       i.nombre_uk,
       i.descripcion_uk,
       i.url url,
       i.url_procedimiento,
       p.id  persona_id,
       (SELECT count(*)
        FROM xpfi_itm_favoritos f
        WHERE f.per_id = p.id
          AND f.xii_id = i.id)
             favorito,
       if.grupo_perfil_id
FROM xpfi_itm_destacados if,
     xpfi_items i,
     upo_ext_personas_todas p
WHERE if.xii_id = i.id
  AND i.activo = 'S'
  AND i.principal = 'S'
  AND gcp_xpfi.item_accesible(i.id, p.id) != 'N'
        AND exists(SELECT ig.xii_id
                   FROM xpfi_itm_grp ig
                   WHERE ig.xii_id = i.id);

CREATE
OR REPLACE FORCE VIEW "UJI_PORTAL"."XPFI_VW_ITEMS_FAVORITOS" ("IDIOMA", "ID", "NOMBRE", "DESCRIPCION", "URL", "URL_PROCEDIMIENTO", "PER_ID", "FAVORITO") AS
SELECT 'ca'             idioma,
       i.id,
       i.nombre_ca      nombre,
       i.descripcion_ca descripcion,
       i.url            url,
       i.url_procedimiento,
       if.per_id,
       1                favorito
FROM xpfi_itm_favoritos if,
     xpfi_items i
WHERE if.xii_id = i.id
  AND i.activo = 'S'
  AND i.principal = 'S'
  AND gcp_xpfi.item_Accesible(i.id, if.per_id) != 'N'
        AND exists(SELECT ig.xii_id
                   FROM xpfi_itm_grp ig
                   WHERE ig.xii_id = i.id)
UNION
SELECT 'es'             idioma,
       i.id,
       i.nombre_es      nombre,
       i.descripcion_es descripcion,
       i.url            url,
       i.url_procedimiento,
       if.per_id,
       1                favorito
FROM xpfi_itm_favoritos if,
     xpfi_items i
WHERE if.xii_id = i.id
  AND i.activo = 'S'
  AND i.principal = 'S'
  AND gcp_xpfi.item_Accesible(i.id, if.per_id) != 'N'
        AND exists(SELECT ig.xii_id
                   FROM xpfi_itm_grp ig
                   WHERE ig.xii_id = i.id)
UNION
SELECT 'en'             idioma,
       i.id,
       i.nombre_uk      nombre,
       i.descripcion_uk descripcion,
       i.url            url,
       i.url_procedimiento,
       if.per_id,
       1                favorito
FROM xpfi_itm_favoritos if,
     xpfi_items i
WHERE if.xii_id = i.id
  AND i.activo = 'S'
  AND i.principal = 'S'
  AND gcp_xpfi.item_Accesible(i.id, if.per_id) != 'N'
        AND exists(SELECT ig.xii_id
                   FROM xpfi_itm_grp ig
                   WHERE ig.xii_id = i.id);

CREATE
OR REPLACE FORCE VIEW "UJI_PORTAL"."XPFI_VW_ITEMS_MAS_USADOS" ("ID", "NOMBRE_CA", "NOMBRE_ES", "NOMBRE_UK", "DESCRIPCION_CA", "DESCRIPCION_ES", "DESCRIPCION_UK", "URL", "URL_PROCEDIMIENTO", "NUM", "PER_ID", "FAVORITO") AS
SELECT i.id,
       i.NOMBRE_CA,
       i.NOMBRE_ES,
       i.NOMBRE_UK,
       i.DESCRIPCION_CA,
       i.DESCRIPCION_ES,
       i.DESCRIPCION_UK,
       i.url url,
       i.url_procedimiento,
       num,
       aa.per_id,
       (SELECT count(*)
        FROM xpfi_itm_favoritos f
        WHERE f.per_id = aa.per_id
          AND f.xii_id = i.id)
             favorito
FROM www_vmc_accesos_agrupados aa,
     xpfi_items i
WHERE i.id = aa.itm_id
  AND i.activo = 'S'
  AND i.principal = 'S'
  AND gcp_xpfi.item_accesible(i.id, aa.per_id) != 'N'
        AND exists(SELECT ig.xii_id
                   FROM xpfi_itm_grp ig
                   WHERE ig.xii_id = i.id);

CREATE
OR REPLACE FORCE VIEW "UJI_PORTAL"."XPFI_VW_ITEMS_MAS_USADOS_PRF" ("ID", "NOMBRE_CA", "NOMBRE_ES", "NOMBRE_UK", "DESCRIPCION_CA", "DESCRIPCION_ES", "DESCRIPCION_UK", "URL", "URL_PROCEDIMIENTO", "GRUPO_PERFIL_ID", "PER_ID", "FAVORITO", "NUM") AS
SELECT i.id,
       i.NOMBRE_CA,
       i.NOMBRE_ES,
       i.NOMBRE_UK,
       i.DESCRIPCION_CA,
       i.DESCRIPCION_ES,
       i.DESCRIPCION_UK,
       i.url url,
       i.url_procedimiento,
       aap.grupo_perfil_id,
       p.id  per_id,
       (SELECT count(*)
        FROM xpfi_itm_favoritos f
        WHERE f.per_id = p.id
          AND f.xii_id = i.id)
             favorito,
       num
FROM www_vmc_accesos_agrupados_prf aap,
     xpfi_items i,
     upo_ext_personas_todas p
WHERE aap.itm_id = i.id
  AND i.activo = 'S'
  AND i.principal = 'S'
  AND gcp_xpfi.item_accesible(i.id, p.id) != 'N'
        AND exists(SELECT ig.xii_id
                   FROM xpfi_itm_grp ig
                   WHERE ig.xii_id = i.id);


CREATE
OR REPLACE VIEW xpfi_vw_items_ubicacion AS
SELECT i.id || '-' || g.id                                id,
       i.id                                               item_id,
       a.aplicacion_id,
       a.region_id,
       g.grupo_id,
       a.nombre,
       a.idioma,
       r.nombre || ' > ' || a.nombre || ' > ' || g.nombre camino
FROM XPFI_VW_APLICACIONES a,
     xpfi_items i,
     xpfi_itm_grp ig,
     xpfi_vw_grupos g,
     xpfi_vw_regiones r
WHERE i.id = ig.xii_id
  AND ig.xig_id = g.grupo_id
  AND ig.xig_xia_id = a.aplicacion_id
  AND ig.xig_xia_xir_id = r.id
  AND g.aplicacion_id = a.aplicacion_id
  AND g.region_id = a.region_id
  AND a.idioma = g.idioma
  AND a.idioma = r.idioma

