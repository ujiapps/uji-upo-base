alter table upo_objetos_metadatos rename column valor to valor_ca;

alter table upo_objetos_metadatos modify valor_ca null;

alter table upo_objetos_metadatos add (valor_es varchar2(4000), valor_en varchar2(4000));

alter table upo_objetos_idiomas add (longitud number default '0' not null);

alter table upo_objetos_idiomas_log add (longitud number default '0' not null);

update upo_objetos_idiomas set longitud = nvl(length(contenido), 0);
update upo_objetos_idiomas_log set longitud = nvl(length(contenido), 0);

CREATE OR REPLACE function UJI_PORTAL.concat_metadata(v_objeto_id number, v_idioma_iso varchar2)
return clob
is
   vRdo clob; 
begin
   select decode(v_idioma_iso, 'ca', replace(replace(wm_concat(replace(clave, ',', '###') || '@@@' || replace(valor_ca, ',', '###')), ',', '~~~'), '###', ','),
                               'es', replace(replace(wm_concat(replace(clave, ',', '###') || '@@@' || replace(valor_es, ',', '###')), ',', '~~~'), '###', ','),
                               'en', replace(replace(wm_concat(replace(clave, ',', '###') || '@@@' || replace(valor_en, ',', '###')), ',', '~~~'), '###', ','))  
     into vRdo
     from upo_objetos_metadatos
    where objeto_id = v_objeto_id
      and ((v_idioma_iso = 'ca' and valor_ca is not null)
           or
           (v_idioma_iso = 'es' and valor_es is not null)
           or
           (v_idioma_iso = 'en' and valor_en is not null)
          );

   return vRdo;
end;
/

CREATE OR REPLACE FORCE VIEW UJI_PORTAL.UPO_VW_PUBLICACION
(
   CONTENIDO_ID,
   CONTENIDO_IDIOMA_ID,
   URL_NODO,
   URL_NODO_ORIGINAL,
   URL_CONTENIDO,
   URL_COMPLETA,
   URL_COMPLETA_ORIGINAL,
   URL_NUM_NIVELES,
   URL_DESTINO,
   NIVEL,
   IDIOMA,
   TITULO,
   SUBTITULO,
   TITULO_LARGO,
   RESUMEN,
   MIME_TYPE,
   ES_HTML,
   CONTENIDO,
   FECHA_MODIFICACION,
   NODO_MAPA_ORDEN,
   CONTENIDO_ORDEN,
   TIPO_CONTENIDO,
   AUTOR,
   ORIGEN_NOMBRE,
   ORIGEN_URL,
   LUGAR,
   PROXIMA_FECHA_VIGENCIA,
   PRIMERA_FECHA_VIGENCIA,
   PRIMERA_FECHA_VIGENCIA_ORDEN,
   METADATA
)
AS
   SELECT   o.id contenido_id,
            oi.id contenido_idioma_id,
            m.url_completa url_nodo,
            m.url_completa url_nodo_original,
            o.url_path url_contenido,
            m.url_completa || o.url_path url_completa,
            m.url_completa || o.url_path url_completa_original,
            times_value_in_string (m.url_completa, '/') niveles,
            oi.enlace_destino,
            NVL (mp.nivel, 1),
            i.codigo_iso idioma,
            oi.titulo,
            oi.subtitulo,
            oi.titulo_largo,
            oi.resumen,
            oi.mime_type,
            oi.html es_html,
            DECODE (oi.html, 'S', oi.contenido, NULL) contenido,
            oi.fecha_modificacion,
            m.orden nodo_mapa_orden,
            mo.orden contenido_orden,
            'NORMAL' tipo_contenido,
            (SELECT   NVL (
                         otros_autores,
                         (SELECT   UPPER(   nombre
                                         || ' '
                                         || apellido1
                                         || ' '
                                         || apellido2)
                            FROM   upo_ext_personas
                           WHERE   id = per_id_responsable)
                      )
               FROM   upo_objetos
              WHERE   id = o.id)
               autor,
            oin.nombre origen_nombre,
            oin.url origen_url,
            o.lugar,
            get_proxima_fecha_vigencia (o.id) proxima_fecha_vigencia,
            get_primera_fecha_vigencia (o.id) primera_fecha_vigencia,
            NVL (get_inicio_vigencia_contenido (o.id), SYSDATE - 10000)
               primera_fecha_vigencia_orden,
            concat_metadata (o.id, i.codigo_iso) metadata
     FROM   upo_mapas m,
            upo_mapas_plantillas mp,
            upo_mapas_objetos mo,
            upo_objetos o,
            upo_objetos_idiomas oi,
            upo_idiomas i,
            upo_origenes_informacion oin
    WHERE       m.id = mo.mapa_id
            AND m.id = mp.mapa_id(+)
            AND o.id = mo.objeto_id
            AND o.id = oi.objeto_id
            AND i.id = oi.idioma_id
            AND mo.tipo = 'NORMAL'
            AND o.origen_informacion_id = oin.id(+)
            AND o.visible = 'S'
   UNION ALL
   SELECT   o.id contenido_id,
            oi.id contenido_idioma_id,
            m.url_completa url_nodo,
            (SELECT   xm.url_completa
               FROM   upo_mapas xm, upo_mapas_objetos xmo
              WHERE       xm.id = xmo.mapa_id
                      AND o.id = xmo.objeto_id
                      AND xmo.tipo = 'NORMAL')
               url_nodo_original,
            o.url_path url_contenido,
            m.url_completa || o.url_path url_completa,
            (SELECT   xm.url_completa || o.url_path
               FROM   upo_mapas xm, upo_mapas_objetos xmo
              WHERE       xm.id = xmo.mapa_id
                      AND o.id = xmo.objeto_id
                      AND xmo.tipo = 'NORMAL')
               url_completa_original,
            times_value_in_string (m.url_completa, '/') niveles,
            oi.enlace_destino,
            NVL (mp.nivel, 1),
            i.codigo_iso idioma,
            oi.titulo,
            oi.subtitulo,
            oi.titulo_largo,
            oi.resumen,
            oi.mime_type,
            oi.html es_html,
            DECODE (oi.html, 'S', oi.contenido, NULL) contenido,
            oi.fecha_modificacion,
            m.orden nodo_mapa_orden,
            mo.orden contenido_orden,
            'LINK' tipo_contenido,
            (SELECT   NVL (
                         otros_autores,
                         (SELECT   UPPER(   nombre
                                         || ' '
                                         || apellido1
                                         || ' '
                                         || apellido2)
                            FROM   upo_ext_personas
                           WHERE   id = per_id_responsable)
                      )
               FROM   upo_objetos
              WHERE   id = o.id)
               autor,
            oin.nombre origen_nombre,
            oin.url origen_url,
            o.lugar,
            get_proxima_fecha_vigencia (o.id) proxima_fecha_vigencia,
            get_primera_fecha_vigencia (o.id) primera_fecha_vigencia,
            NVL (get_inicio_vigencia_contenido (o.id), SYSDATE - 10000)
               primera_fecha_vigencia_orden,
            concat_metadata (o.id, i.codigo_iso) metadata
     FROM   upo_mapas m,
            upo_mapas_plantillas mp,
            upo_mapas_objetos mo,
            upo_objetos o,
            upo_objetos_idiomas oi,
            upo_idiomas i,
            upo_origenes_informacion oin
    WHERE       m.id = mo.mapa_id
            AND m.id = mp.mapa_id(+)
            AND o.id = mo.objeto_id
            AND o.id = oi.objeto_id
            AND i.id = oi.idioma_id
            AND mo.tipo = 'LINK'
            AND o.origen_informacion_id = oin.id(+)
            AND o.visible = 'S'
            AND mo.estado_moderacion = 'ACEPTADO';

