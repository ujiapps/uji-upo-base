<%@page import="es.uji.commons.sso.User" %>
<%@page import="es.uji.apps.upo.model.Permiso" %>
<%@page import="es.uji.apps.upo.model.ClientConfiguration" %>
<%@page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="text/html; charset=utf-8">

    <link type="image/x-icon" rel="shortcut icon"
          href="//e-ujier.uji.es/img/portal2/favicon.ico"/>
    <link rel="stylesheet" type="text/css"
          href="//static.uji.es/js/extjs/ext-3.3.1/resources/css/ext-all.css"/>
    <link rel="stylesheet"
          href="//static.uji.es/js/extjs/ext-3.3.1/examples/ux/css/RowEditor.css"/>
    <link rel="stylesheet" type="text/css"
          href="//static.uji.es/js/extjs/ext-3.3.1/examples/ux/fileuploadfield/css/fileuploadfield.css"/>
    <link rel="stylesheet" type="text/css"
          href="//static.uji.es/js/extjs/uji-commons-extjs/css/icons.css"/>

    <script type="text/javascript"
            src="//static.uji.es/js/extjs/ext-3.3.1/adapter/ext/ext-base.js"></script>
    <script type="text/javascript"
            src="//static.uji.es/js/extjs/ext-3.3.1/ext-all-debug.js"></script>

    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>

    <script type="text/javascript"
            src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/tree/XmlTreeLoader/0.0.1/XmlTreeLoader.js"></script>
    <script type="text/javascript"
            src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/TabCloseMenu/0.0.1/TabCloseMenu.js"></script>
    <script type="text/javascript"
            src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/TabPanel/0.0.1/TabPanel.js"></script>
    <script type="text/javascript"
            src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/LanguagePanel/0.0.1/LanguagePanel.js"></script>
    <script type="text/javascript"
            src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/grid/GridPanel/0.0.1/GridPanel.js"></script>
    <script type="text/javascript"
            src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/grid/OneColumnGrid/0.0.1/OneColumnGrid.js"></script>
    <script type="text/javascript"
            src="//static.uji.es/js/extjs/ext-3.3.1/examples/ux/RowEditor.js"></script>
    <script type="text/javascript"
            src="//static.uji.es/js/extjs/ext-3.3.1/examples/ux/RowExpander.js"></script>
    <script type="text/javascript"
            src="//static.uji.es/js/extjs/ext-3.3.1/examples/ux/SearchField.js"></script>
    <script type="text/javascript"
            src="//static.uji.es/js/extjs/uji-commons-extjs/UJI/Bus/0.0.1/Bus.js"></script>

    <script type="text/javascript"
            src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/form/CKEditor/0.0.3/CKEditor.js"></script>
    <script type="text/javascript"
            src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/Util.js"></script>
    <script type="text/javascript"
            src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/FileUploader/0.0.1/FileUploader.js"></script>
    <script type="text/javascript"
            src="//static.uji.es/js/extjs/ext-3.3.1/examples/ux/fileuploadfield/FileUploadField.js"></script>
    <script type="text/javascript"
            src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/form/LookupWindow/0.0.2/LookupWindow.js"></script>

    <script src="//static.uji.es/js/jquery/jquery-1.7.2.min.js"></script>
    <script
            src="//static.uji.es/js/jquery/jquery-plugins/jquery.ui.widget.js"></script>
    <script
            src="//static.uji.es/js/jquery/jquery-plugins/jquery.iframe-transport.js"></script>
    <script
            src="//static.uji.es/js/jquery/jquery-plugins/jquery.fileupload.js"></script>

    <script type="text/javascript"
            src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/patch/XmlWriterEntityEncodingPathExtjs3-X.js"></script>

    <style type="text/css">
        .row-pendiente td {
            color: red;
        }

        .panel-url-completa div {
            background-color: #3C6888;
            color: white;
            font-weight: bold;
            font-family: 'arial';
            padding: 2px;
            border-color: transparent
        }

        .contenido-recurso-pdf {
            padding: 1px;
            padding-left: 18px;
            background-image: url(img/pdf_icon.png) !important;
            background-repeat: no-repeat
        }

        .contenido-recurso-imagen {
            padding: 1px;
            padding-left: 18px;
            background-image: url(img/imagen_icon.png) !important;
            background-repeat: no-repeat
        }

        .contenido-recurso-otro {
            padding: 1px;
            padding-left: 18px;
            background-image: url(img/binary_icon.png) !important;
            background-repeat: no-repeat
        }

        div.contenido-idioma-recurso {
            float: left;
            padding-top: 4px;
        }

        div.contenido-idioma-recurso-clear {
            float: left;
            padding-top: 4px;
            clear: both;
        }

        div.recurso-generado {
            padding: 4px;
        }

        a.recurso-generado {
            paddind: 4px;
            padding-left: 2px;
        }

        a.recurso-generado img {
            vertical-align: middle
        }

        table.cke_dialog_contents table {
            height: 100%;
        }

        div.snapshot {
            width: 100%;
            padding-left: 100px
        }

        img.snapshot {
            max-height: 250px;
        }

        .delete-min {
            background-image: url(img/delete.png) !important;
            background-repeat: no-repeat;
            width: 10px !important;
            height: 10px !important;
        }

    </style>

    <script type="text/javascript"
            src="js/Ext/ux/uji/grid/MasterGrid/0.0.1/MasterGrid.js"></script>

    <script type="text/javascript" src="js/widgets/DatePickerPanel.js"></script>
    <script type="text/javascript" src="js/dictionaries/tipoPlantilla.js"></script>
    <script type="text/javascript"
            src="js/dictionaries/tipoFranquiciaAcceso.js"></script>
    <script type="text/javascript" src="js/app/master/franquicias.js"></script>
    <script type="text/javascript" src="js/app/master/idiomas.js"></script>
    <script type="text/javascript" src="js/app/master/menu.js"></script>
    <script type="text/javascript" src="js/app/master/grupo.js"></script>
    <script type="text/javascript" src="js/app/master/item.js"></script>
    <script type="text/javascript" src="js/app/master/plantillas.js"></script>
    <script type="text/javascript" src="js/app/tree/mapa.js"></script>
    <script type="text/javascript" src="js/contenidoPanel.js"></script>
    <script type="text/javascript" src="js/contenidoForm.js"></script>
    <script type="text/javascript" src="js/contenidoGrid.js"></script>
    <script type="text/javascript" src="js/app/tree/plantillasNodoMapa.js"></script>
    <script type="text/javascript" src="js/busqueda.js"></script>
    <script type="text/javascript" src="js/app/tree/gestionNodoPanel.js"></script>
    <script type="text/javascript" src="js/app/tree/gestionNodoForm.js"></script>
    <script type="text/javascript" src="js/moderacion.js"></script>
    <script type="text/javascript" src="js/moderacionLotes.js"></script>
    <script type="text/javascript" src="js/migracionUrlGrid.js"></script>
    <script type="text/javascript" src="js/prioridadPanel.js"></script>
    <script type="text/javascript" src="js/prioridadWindow.js"></script>
    <script type="text/javascript" src="js/contenidoMetadatosUtil.js"></script>
    <script type="text/javascript" src="js/contenidoMetadatosIdiomaPanel.js"></script>
    <script type="text/javascript" src="js/contenidoMetadatosWindow.js"></script>
    <script type="text/javascript" src="js/app/tree/crearNodoWindow.js"></script>
    <script type="text/javascript"
            src="js/app/tree/crearNodoConEstructuraDeFechaWindow.js"></script>
    <script type="text/javascript"
            src="js/app/tree/crearNodoConEstructuraDeRevistaWindow.js"></script>
    <script type="text/javascript"
            src="js/app/tree/crearNodoConEstructuraDeVoxujiWindow.js"></script>
    <script type="text/javascript" src="js/visibilidadContenidoPanel.js"></script>
    <script type="text/javascript" src="js/visibilidadContenidoWindow.js"></script>
    <script type="text/javascript"
            src="js/visibilidadContenidoConCheckWindow.js"></script>
    <script type="text/javascript" src="js/dictionaries/estadoModeracion.js"></script>
    <script type="text/javascript"
            src="js/dictionaries/tipoReferenciaContenido.js"></script>
    <script type="text/javascript"
            src="js/dictionaries/tipoPrioridadContenido.js"></script>
    <script type="text/javascript"
            src="js/dictionaries/tipoFormato.js"></script>
    <script type="text/javascript" src="js/migrarContenidosWindow.js"></script>
    <script type="text/javascript" src="js/dictionaries/tipoItemMigracion.js"></script>
    <script type="text/javascript" src="js/seleccionarFranquiciaWindow.js"></script>
    <script type="text/javascript" src="js/application.js"></script>
    <script type="text/javascript" src="js/app/tree/RenombrarNodoWindow.js"></script>
    <script type="text/javascript" src="js/app/tree/DuplicarNodoWindow.js"></script>
    <script type="text/javascript" src="js/app/master/origenes.js"></script>
    <script type="text/javascript" src="js/app/master/usuariosExternos.js"></script>
    <script type="text/javascript" src="js/app/master/plantillasMetadatos.js"></script>
    <script type="text/javascript" src="js/app/master/busquedaAvanzadaRevista.js"></script>

    <script type="text/javascript"
            src="js/dictionaries/iconosTiposMimeGridContenidos.js"></script>
    <script type="text/javascript" src="js/app/tree/accionesNodoMapa.js"></script>
    <script type="text/javascript" src="js/app/tree/accionesDragAndDropNodoWindow.js"></script>
    <script type="text/javascript" src="js/RowExpander.js"></script>
    <script type="text/javascript"
            src="js/dictionaries/tipoPointsArrastrarNodoArbol.js"></script>
    <script type="text/javascript"
            src="js/dictionaries/tipoMetadato.js"></script>

    <!-- UJI specific -->
    <link rel="stylesheet" type="text/css"
          href="css/extjs3uji2011_theme.css"/>
    <link rel="stylesheet" type="text/css"
          href="css/extjs3uji2011_reset.css"/>

    <title>Portal</title>

    <%
        User user = (User) session.getAttribute(User.SESSION_USER);
        Boolean applicationAdmin = Permiso.isAdmin(user.getId());
        Boolean documentalista = Permiso.isDocumentalista(user.getId());
        Boolean hasFranquicia = Permiso.hasFranquicia(user.getId());
        String host = ClientConfiguration.getHost();
        String hostHttp = ClientConfiguration.getHostHttp();
        String heraEditoraUrl = ClientConfiguration.getHeraEditoraUrl();
        String url = request.getParameter("url");
        url = (url != null && !url.isEmpty()) ? url : "/";
    %>

    <script type="text/javascript">

        Ext.BLANK_IMAGE_URL = '//static.uji.es/js/extjs/ext-3.3.1/resources/images/default/s.gif';

        var userAuthenticatedId = <%=user.getId()%>;
        var applicationAdmin = <%=applicationAdmin%>;
        var documentalista = <%=documentalista%>;
        var hasFranquicia = <%=hasFranquicia%>;
        var url = '<%=url%>';
        var host = '<%=host%>';
        var hostHttp = '<%=hostHttp%>';
        var heraEditoraUrl = '<%=heraEditoraUrl%>';

        if (!applicationAdmin && !documentalista && !hasFranquicia)
        {
            window.location=host;
        }

        Ext.onReady(function()
        {
            Ext.QuickTips.init();

            UJI.UPO.Application.enableResponses();
            UJI.UPO.Application.init();

            var tabPanel = UJI.UPO.Application.getTabPanel();

            var menuMenu = new Ext.menu.Menu(
                    {
                        style : {
                            overflow : 'visible'
                        },
                        items : [
                            {
                                text : 'Menús - Grups',
                                handler : function(e)
                                {
                                    var panel = new UJI.UPO.Menus({});
                                    tabPanel.addTab(panel);
                                }
                            },
                            {
                                text : 'Grups - Items',
                                handler : function(e)
                                {
                                    var panel = new UJI.UPO.Grupos({});
                                    tabPanel.addTab(panel);
                                }
                            },
                            {
                                text : 'Items',
                                handler : function(e)
                                {
                                    var panel = new UJI.UPO.Items({});
                                    tabPanel.addTab(panel);
                                }
                            }
                        ]
                    });

            var menuAdministracion = new Ext.menu.Menu(
                    {
                        style : {
                            overflow : 'visible'
                        },
                        items : [
                            {
                                text : 'Franquícies',
                                handler : function(e)
                                {
                                    var panel = new UJI.UPO.Franquicias({});
                                    tabPanel.addTab(panel);
                                }
                            },
                            {
                                text : 'Idiomes',
                                handler : function(e)
                                {
                                    var panel = new UJI.UPO.Idiomas({});
                                    tabPanel.addTab(panel);
                                }
                            },
                            {
                                text : 'Plantilles',
                                handler : function(e)
                                {
                                    var panel = new UJI.UPO.Plantillas({});
                                    tabPanel.addTab(panel);
                                }
                            },
                            {
                                text : 'Orígens',
                                handler : function(e)
                                {
                                    var panel = new UJI.UPO.Origenes({});
                                    tabPanel.addTab(panel);
                                }
                            },
                            {
                                text : 'Usuaris externs',
                                handler : function(e)
                                {
                                    var panel = new UJI.UPO.UsuariosExternos({});
                                    tabPanel.addTab(panel);
                                }
                            },
                            {
                                text : 'Plantilles metadades',
                                handler : function(e)
                                {
                                    var panel = new UJI.UPO.PlantillasMetadatos({});
                                    tabPanel.addTab(panel);
                                }
                            }
                        ]
                    });

            /*var menuModeracion = new Ext.menu.Menu(
                    {
                        style : {
                            overflow : 'visible'
                        },
                        items : [
                            {
                                text : 'General',
                                handler : function(e)
                                {
                                    var panel = new UJI.UPO.Moderacion({});
                                    tabPanel.addTab(panel);
                                }
                            },
                            {
                                text : 'Lots',
                                handler : function(e)
                                {
                                    var panel = new UJI.UPO.ModeracionLotes({});
                                    tabPanel.addTab(panel);
                                }
                            }
                        ]
                    });*/

            var items = [
                {
                    text : 'Menús',
                    menu : menuMenu,
                    iconCls : 'vcard'
                }
            ];

            if (applicationAdmin)
            {
                items.push({
                    text : 'Administració',
                    iconCls : 'database-gear',
                    menu : menuAdministracion
                });

                /*items.push({
                    text : 'Moderació',
                    iconCls : 'user-comment',
                    menu : menuModeracion
                });*/
            }
            //else
            //{
            items.push({
                text : 'Moderació',
                iconCls : 'user-comment',
                handler : function(e)
                {
                    var panel = new UJI.UPO.Moderacion({});
                    tabPanel.addTab(panel);
                }
            });
            //}

            /*items.push({
                text : 'Migració',
                iconCls : 'cart',
                handler : function(e)
                {
                    var panel = new UJI.UPO.MigracionUrl({});
                    tabPanel.addTab(panel);
                }
            });*/

            if (documentalista || applicationAdmin)
            {
                items.push({
                    text : 'Revista',
                    iconCls : 'report-magnify',
                    handler : function(e)
                    {
                        var panel = new UJI.UPO.BusquedaAvanzadaRevista({});
                        tabPanel.addTab(panel);
                    }
                })
            }

            var menuToolbar = new Ext.Toolbar(
                    {
                        items : items
                    }
            );

            menuToolbar.doLayout();

            var headerPanel = new Ext.Panel(
                    {
                        region : 'north',
                        layout : {
                            type : 'vbox',
                            align : 'stretch'
                        },
                        height : 100,
                        items : [UJI.UPO.Application.getLogo(), menuToolbar]
                    });

            var treePanel = new UJI.UPO.Mapa(
                    {
                        tabPanel : tabPanel,
                        width : 200,
                        url : url
                    });

            UJI.UPO.Application.initListenersTabPanel(tabPanel, treePanel);

            var searchPanel = new UJI.UPO.Busqueda({});

            searchPanel.botonEditar.on('click', function(e)
            {
                var rec = searchPanel.grid.getSelectionModel().getSelected();

                urlNodoNormal = rec.get('urlCompleta');

                treePanel.selectPath("/root/first" + urlNodoNormal.substring(0, urlNodoNormal.length - 1), "urlPath", function(success, node)
                {
                    if (success && rec)
                    {
                        var result = UJI.UPO.Application.createNewNodeTab(treePanel, node, node.id, node.attributes.urlCompleta);

                        tabPanel.addTab(result.nodoTab);
                    }
                });
            });

            tabPanel.add(searchPanel);

            var loadIndicator =
                {
                    xtype : 'panel',
                    id : '_$loadingIndicator',
                    frame : false,
                    border : false,
                    html : '<div style="font:normal 11px tahoma,arial,helvetica,sans-serif;border:1px solid gray;padding:8px;background-color:#fff;">'
                    + '<img style="margin-right:4px;" align="left" src="/upo/img/loading2.gif" /> Carregant...</div>',
                    hidden : true,
                    style : 'position:fixed; top:5px; right:5px; left:auto;'
                };

            new Ext.Viewport(
                    {
                        layout : 'border',
                        items : [headerPanel, treePanel, tabPanel, loadIndicator]
                    });
        })
        ;
    </script>
</head>
<body>
</body>
</html>
