<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Reservori</title>

<link type="image/x-icon" rel="shortcut icon"
	href="//e-ujier.uji.es/img/portal2/favicon.ico" />
<link rel="stylesheet" type="text/css"
	href="//static.uji.es/js/extjs/ext-3.3.1/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css"
	href="//static.uji.es/js/extjs/ext-3.3.1/examples/ux/fileuploadfield/css/fileuploadfield.css" />
	<link rel="stylesheet" type="text/css"
		  href="//static.uji.es/js/extjs/uji-commons-extjs/css/icons.css"/>

<script type="text/javascript"
	src="//static.uji.es/js/extjs/ext-3.3.1/adapter/ext/ext-base.js"></script>

<script type="text/javascript"
	src="//static.uji.es/js/extjs/ext-3.3.1/ext-all-debug.js"></script>

<script type="text/javascript"
	src="//static.uji.es/js/extjs/ext-3.3.1/examples/ux/DataView-more.js"></script>
	
<script type="text/javascript"
	src="//static.uji.es/js/extjs/ext-3.3.1/examples/ux/SearchField.js"></script>
	
<script type="text/javascript"
	src="//static.uji.es/js/extjs/ext-3.3.1/examples/ux/fileuploadfield/FileUploadField.js"></script>

<script type="text/javascript"
	src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/FileUploader/0.0.1/FileUploader.js"></script>

<script type="text/javascript"
	src="/upo/js/Ext/ux/PanelCollapsedTitle.js"></script>

<script src="//static.uji.es/js/jquery/jquery-1.7.2.min.js"></script>
<script src="//static.uji.es/js/jquery/jquery-plugins/jquery.ui.widget.js"></script>
<script src="//static.uji.es/js/jquery/jquery-plugins/jquery.iframe-transport.js"></script>
<script src="//static.uji.es/js/jquery/jquery-plugins/jquery.fileupload.js"></script>

<script type="text/javascript" src="js/imatgesReservori.js"></script>

<link rel="stylesheet" type="text/css" href="css/imatgesReservori.css" />

<!-- UJI specific -->
<link rel="stylesheet" type="text/css"
	href="css/extjs3uji2011_theme.css" />
<link rel="stylesheet" type="text/css"
	href="css/extjs3uji2011_reset.css" />

<%
	String CKEditor = request.getParameter("CKEditor");
	String CKEditorFuncNum = request.getParameter("CKEditorFuncNum");
	String langCode = request.getParameter("langCode");
	String urlNode = request.getParameter("urlNode");
	String idioma = request.getParameter("idioma");
%>

<script language=JavaScript>
    Ext.BLANK_IMAGE_URL = '//static.uji.es/js/extjs/ext-3.3.1/resources/images/default/s.gif';

    Ext.onReady(function()
    {
        Ext.QuickTips.init();

        var panel = new UJI.UPO.ImagenesReservori(
        {
            funcNum : '<%=CKEditorFuncNum%>',
            urlNode : '<%=urlNode%>',
			idioma : '<%=idioma%>'
        });

        new Ext.Viewport(
        {
            layout : 'border',
            items : [ panel ]
        });
    });
</script>

</head>
<body>
</body>
</html>