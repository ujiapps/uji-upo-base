CKEDITOR.plugins.add('accessibilitydialog',
{
    icons: 'accessibilitydialog',
    init : function(editor)
    {
        var pluginName = 'accessibilitydialog';

        function muestraLoading()
        {
            try
            {
              var iframe = document.getElementById("contenidoIFrame" + editor.config.id);
              iframe.src = "/upo/loadingPage.jsp";
            } catch (e)
            {
            }
        }
        ;

        var dialog = function(editor)
        {
            var contenidoForm = Ext.getCmp(editor.config.contenidoFormId);

            return {
                title : 'Ventana de accesibilidad',
                minWidth : 700,
                minHeight : 400,
                buttons : [ CKEDITOR.dialog.okButton ],
                resizable : CKEDITOR.DIALOG_RESIZE_BOTH,
                onShow : function()
                {
                    var ref = this;
                    var idioma = editor.config.idioma;
                    var content = ref.getParentEditor().getData();

                    contenidoForm.autoSaveRequest(editor.name, idioma, function()
                    {
                        ref.setupContent(content);
                    });

                },
                onOk : function()
                {
                    muestraLoading();
                },
                onCancel : function()
                {
                    muestraLoading();
                },
                contents : [
                {
                    id : 'page1',
                    label : 'Page1',
                    elements : [
                    {
                        type : 'html',
                        id : 'content',
                        html : '<iframe id="contenidoIFrame' + editor.config.id +'" style="width: 100%; height: 380px;" src="/upo/loadingPage.jsp">Carregant...</iframe>',
                        setup : function(element)
                        {
                            var iframe = document.getElementById("contenidoIFrame" + editor.config.id);

                            var form = contenidoForm.form.getForm();

                            var idioma = editor.config.idioma;
                            var personaId = editor.config.personaId;

                            var nodoMapaId = contenidoForm.mapaId;
                            var contenidoId = form.findField("id").getValue();

                            var urlPublicacion = hostHttp + "/upo/rest/publicacion/accesibilidad?mapaId=" + nodoMapaId + '&contenidoId=' + contenidoId + '&idioma='
                                    + idioma + '&personaId=' + personaId;

                            iframe.src = heraEditoraUrl + "?url=" + escape(urlPublicacion);
                        }
                    } ]
                } ]
            };
        };

        CKEDITOR.dialog.add(pluginName, function(editor)
        {
            return dialog(editor);
        });

        editor.addCommand(pluginName, new CKEDITOR.dialogCommand(pluginName));

        editor.ui.addButton('AccessibilityDialog',
        {
            label : 'Ventana de accesibilidad',
            command : pluginName
        });
    }
});