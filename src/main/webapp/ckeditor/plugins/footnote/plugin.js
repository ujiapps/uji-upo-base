CKEDITOR.plugins.add('footnote',
{
    icons: 'footnote',
    init : function(editor)
    {
        var pluginName = 'footnote';

        editor.addCommand(pluginName,
        {
            exec : function(editor)
            {
                Ext.Ajax.request(
                {
                    url : '/upo/rest/formatocontenido',
                    method : 'POST',
                    params : {
                        content : editor.getData()
                    },
                    success : function(response)
                    {
                        var content = response.responseText;
                        editor.setData(content);
                    }
                });
            }
        });

        editor.ui.addButton('Footnote',
        {
            label : 'Limpiar HTML',
            command : pluginName
        });
    }
});