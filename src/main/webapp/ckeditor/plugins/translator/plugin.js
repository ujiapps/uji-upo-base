CKEDITOR.plugins.add('translator',
{
    requires : [ 'richcombo' ],
    init : function(editor)
    {
        var config = editor.config, lang = editor.lang.format;

        var tags = [];

        tags[0] = [ "CA", "Català", "Català" ];
        tags[1] = [ "ES", "Espanyol", "Espanyol" ];
        tags[2] = [ "EN", "Anglès", "Anglès" ];

        editor.ui.addRichCombo('Translator',
        {
            label : "Traduir desde",
            title : "Traduir desde",
            voiceLabel : "Traduir desde",
            className : 'cke_format',
            multiSelect : false,

            panel :
            {
                css : [ config.contentsCss, CKEDITOR.getUrl(CKEDITOR.skin.getPath('editor') + 'editor.css') ],
                voiceLabel : lang.panelVoiceLabel
            },

            init : function()
            {
                this.startGroup("Traduir desde...");

                for ( var this_tag in tags)
                {
                    if (tags.hasOwnProperty(this_tag) && tags[this_tag][0] != editor.config.idioma.toUpperCase())
                    {
                        this.add(tags[this_tag][0], tags[this_tag][1], tags[this_tag][2]);
                    }
                }
            },

            onClick : function(value)
            {
                var form = Ext.getCmp(config.contenidoFormId)
                var idioma = config.idioma;
                var originalText = form['htmlEditor' + value].getValue();
                var ref = this;

                if (editor.getData())
                {
                    Ext.MessageBox.confirm('Avís', 'El contingut actual será substituït, voleu continuar?', function(optional)
                    {
                        if (optional != 'no')
                        {
                            ref.traducir(originalText, value, idioma);
                        }
                    });
                }
                else
                {
                    ref.traducir(originalText, value, idioma);
                }
            },

            traducir : function(originalText, sourceLang, targetLang)
            {
                Ext.Ajax.request(
                {
                    url : '/upo/rest/translate',
                    method : 'POST',
                    params :
                    {
                        text : originalText,
                        sourceLang : sourceLang.toLowerCase(),
                        targetLang : targetLang.toLowerCase()
                    },
                    success : function(response)
                    {
                        var content = response.responseText;
                        editor.setData(content);
                    }
                });
            }
        });
    }
});