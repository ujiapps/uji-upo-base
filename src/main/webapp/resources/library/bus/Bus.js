UJIBus = {}

UJIBus.Bus = {
    subscriptors: {},

    subscribe: function(message, source) {
        if (!this.isSubscribed(message, source)) {
            var subscriptorList = this.subscriptors[message] || [];
            subscriptorList.push(source.id);
            this.subscriptors[message] = subscriptorList;
        }
    },

    unsubscribe: function(message, source) {
        if (this.isSubscribed(message, source)) {
            var subscriptorList = this.subscriptors[message] || [];

            var position = subscriptorList.indexOf(source.id);
            subscriptorList.splice(position, 1);
        }
    },

    publish: function(message, params) {
        this._unsubscribeNotExistingReferences(message);

        var subscriptorList = this.subscriptors[message] || [];
        var idsToUnsubscribe = [];

        for (var i = 0, subscriptor; subscriptor = subscriptorList[i]; i++) {
            var componentSubscriptor = Ext.getCmp(subscriptor);
            componentSubscriptor.onMessage(message, params);
        }
    },

    _unsubscribeNotExistingReferences: function(message) {
        var subscriptorList = this.subscriptors[message] || [];
        var idsToUnsubscribe = [];

        for (var i = 0, subscriptor; subscriptor = subscriptorList[i]; i++) {
            if (!Ext.getCmp(subscriptor)) {
                idsToUnsubscribe.push(subscriptor);
            }
        }

        for (var i = 0, id; id = idsToUnsubscribe[i]; i++) {
            var position = subscriptorList.indexOf(id);
            subscriptorList.splice(position, 1);
        }
    },

    isSubscribed: function(message, source) {
        var subscriptorList = this.subscriptors[message] || [];

        for (var i = 0, subscriptor; subscriptor = subscriptorList[i]; i++) {
            if (subscriptor === source.id) {
                return true;
            }
        }

        return false;
    }
}