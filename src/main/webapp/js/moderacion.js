Ext.ns('UJI.UPO');

UJI.UPO.Moderacion = Ext.extend(Ext.Panel,
{
    title : 'Moderació de continguts',
    layout : 'border',
    closable : true,
    layoutConfig :
    {
        align : 'stretch'
    },
    gridPendiente : {},
    gridHistorico : {},
    storePendiente : {},
    storeHistorico : {},
    botonAprobar : {},
    botonRechazar : {},

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.UPO.Moderacion.superclass.initComponent.call(this);

        this.initUI();

        var tabPanel = new Ext.TabPanel(
        {
            region : 'center',
            activeTab : 0,
            items : [
            {
                title : 'Pendents',
                layout : 'fit',
                items : [ this.gridPendiente ]
            },
            {
                title : 'Històric',
                layout : 'fit',
                items : [ this.gridHistorico ]
            } ],
            listeners :
            {
                tabchange : function(tabPanel, tab)
                {

                }
            }
        });

        this.add(tabPanel);
    },

    initUI : function()
    {
        this.buildStorePendiente();
        this.buildStoreHistorico();
        this.buildBotonAprobar();
        this.buildBotonRechazar();

        var urlRenderer = function(value, metadata, record, rIndex, cIndex, store)
        {
            var server = window.location.protocol + "//" + window.location.host;

            if (server.indexOf("localhost:") > -1)
            {
                server = "http://localhost"
            }

            var action = 'window.open(\'' + server + value + '&idioma=' + this.idioma + '\', \'Previsualització\', \'width=1000,height=900,location=no\')';

            return '<a href="javascript: ' + action + '" >' + '<img src="/upo/img/enlace_externo.png" alt="enllaç extern" title="enllaç extern"/>' + '</a>';
        }

        this.gridPendiente = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            viewConfig :
            {
                forceFit : true
            },
            selModel : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : this.storePendiente,
            colModel : new Ext.grid.ColumnModel(
            {
                columns : [
                {
                    header : 'Codi',
                    width : 50,
                    dataIndex : 'mapaObjetoId'
                },
                {
                    header : 'Títol',
                    width : 300,
                    dataIndex : 'objetoTitulo'
                },
                {
                    header : 'URL Path',
                    width : 100,
                    dataIndex : 'objetoUrlPath'
                },
                {
                    header : 'URL Completa',
                    width : 350,
                    dataIndex : 'urlCompleta'
                },
                {
                    header : 'CA',
                    width : 50,
                    dataIndex : 'urlNodoMapaOriginal',
                    align : 'center',
                    idioma: 'ca',
                    renderer : urlRenderer
                },
                {
                    header : 'ES',
                    width : 50,
                    dataIndex : 'urlNodoMapaOriginal',
                    align : 'center',
                    idioma: 'es',
                    renderer : urlRenderer
                },
                {
                    header : 'EN',
                    width : 50,
                    dataIndex : 'urlNodoMapaOriginal',
                    align : 'center',
                    idioma: 'en',
                    renderer : urlRenderer
                } ]
            }),
            tbar : [ this.botonAprobar, this.botonRechazar ]
        });

        this.gridHistorico = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            viewConfig :
            {
                forceFit : true
            },
            selModel : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : this.storeHistorico,
            colModel : new Ext.grid.ColumnModel(
            {
                columns : [
                {
                    header : 'Codi',
                    width : 50,
                    dataIndex : 'mapaObjetoId'
                },
                {
                    header : 'Títol',
                    width : 300,
                    dataIndex : 'objetoTitulo'
                },
                {
                    header : 'URL Path',
                    width : 100,
                    dataIndex : 'objetoUrlPath'
                },
                {
                    header : 'URL Completa',
                    width : 300,
                    dataIndex : 'urlCompleta'
                },
                {
                    header : 'Resultat',
                    width : 50,
                    dataIndex : 'estadoModeracion'
                } ]
            })
        });
    },

    buildBotonAprobar : function()
    {
        var ref = this;

        this.botonAprobar = new Ext.Button(
        {
            text : 'Aprovar',
            iconCls : 'application-add',
            handler : function()
            {
                var rec = ref.gridPendiente.getSelectionModel().getSelected();

                if (!rec)
                {
                    Ext.Msg.alert('Información', 'És necessari seleccionar una fila per poder aprovar');
                    return false;
                }
                else
                {

                    Ext.Msg.confirm('Confirmació', 'Esteu segur de voler aprovar el registre?', function(btn, text)
                    {
                        if (btn == 'yes')
                        {

                            var record = ref.storePendiente.getById(rec.data.mapaObjetoId);
                            record.set('estadoModeracion', UJI.UPO.EstadoModeracion.ACEPTADO);

                            ref.storePendiente.save();

                            UJI.Bus.publish("UJI.UPO.Moderacion.Action");
                        }
                    });
                }
            }
        });
    },

    buildBotonRechazar : function()
    {
        var ref = this;

        this.botonRechazar = new Ext.Button(
        {
            text : 'Rebutjar',
            iconCls : 'application-add',
            handler : function()
            {
                var rec = ref.gridPendiente.getSelectionModel().getSelected();

                if (!rec)
                {
                    Ext.Msg.alert('Información', 'És necessari seleccionar una fila per poder rebutjar');
                    return false;
                }
                else
                {

                    var textAreaRechazo = new Ext.form.TextArea(
                    {
                        fieldLabel : 'Introduix el motiu del rebuig',
                        width : 200,
                        height : 100
                    });

                    var formularioRegistro = new Ext.form.FormPanel(
                    {
                        frame : true,
                        items : [ textAreaRechazo ]
                    });

                    var window = new Ext.Window(
                    {
                        title : 'Motiu del rebuig',
                        layout : 'fit',
                        height : 200,
                        width : 340,
                        modal : true,
                        items : [ formularioRegistro ],
                        fbar : [
                        {
                            xtype : 'button',
                            text : 'Registrar',
                            handler : function()
                            {
                                var record = ref.storePendiente.getById(rec.data.mapaObjetoId);

                                record.set('estadoModeracion', UJI.UPO.EstadoModeracion.RECHAZADO);
                                record.set('textoRechazo', textAreaRechazo.getValue());

                                ref.storePendiente.save();

                                UJI.Bus.publish("UJI.UPO.Moderacion.Action");

                                window.close();
                            }
                        },
                        {
                            xtype : 'button',
                            text : 'Cancel·lar',
                            handler : function()
                            {
                                window.close();
                            }
                        } ]
                    }).show();
                }
            }
        });
    },

    buildStorePendiente : function()
    {
        var ref = this;

        ref.storePendiente = new Ext.data.Store(
        {
            autoLoad : true,
            autoSave : false,
            restful : true,
            proxy : new Ext.data.HttpProxy(
            {
                url : '/upo/rest/moderacion'
            }),
            reader : new Ext.data.XmlReader(
            {
                record : 'Moderacion',
                id : 'mapaObjetoId'
            }, [ 'mapaObjetoId', 'urlCompleta', 'objetoId', 'estadoModeracion', 'textoRechazo', 'objetoUrlPath', 'objetoTitulo', 'urlNodoMapaOriginal' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                'save' : function(store, record, operation)
                {
                    ref.storePendiente.reload();
                    ref.storeHistorico.reload();
                }
            }
        });
    },

    buildStoreHistorico : function()
    {
        var ref = this;

        ref.storeHistorico = new Ext.data.Store(
        {
            autoLoad : true,
            restful : true,
            proxy : new Ext.data.HttpProxy(
            {
                url : '/upo/rest/moderacion/historico'
            }),
            reader : new Ext.data.XmlReader(
            {
                record : 'Moderacion',
                id : 'mapaObjetoId'
            }, [ 'mapaObjetoId', 'urlCompleta', 'objetoId', 'estadoModeracion', 'textoRechazo', 'objetoUrlPath', 'objetoTitulo' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    }
});
