Ext.ns('UJI.UPO');

UJI.UPO.Plantillas = Ext.extend(Ext.Panel,
{
    title : 'Plantillas del portal',
    layout : 'vbox',
    closable : true,
    layoutConfig :
    {
        align : 'stretch'
    },
    grid : {},
    store : {},
    editor : {},
    botonInsertar : {},
    botonBorrar : {},
    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        Ext.util.Format.comboRenderer = function(combo)
        {
            return function(value)
            {
                var record = combo.findRecord(combo.valueField, value);
                return record ? record.get(combo.displayField) : combo.valueNotFoundText;
            };
        };

        UJI.UPO.Plantillas.superclass.initComponent.call(this);

        this.initUI();

        this.add(this.grid);
    },
    initUI : function()
    {
        this.buildStore();
        this.buildGrid();

    },
    buildGrid : function()
    {
        this.buildComboTipos();
        this.buildEditor();
        this.buildBotonInsertar();
        this.buildBotonBorrar();

        var botonInsertar = this.botonInsertar;
        var botonBorrar = this.botonBorrar;

        this.grid = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            viewConfig :
            {
                forceFit : true
            },
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : this.store,
            plugins : [ this.editor ],
            colModel : new Ext.grid.ColumnModel(
            {
                columns : [
                {
                    header : 'id',
                    dataIndex : 'id',
                    hidden : false,
                    width : 100  ,
                    sortable: true
                },
                {
                    header : 'Tipus',
                    dataIndex : 'tipo',
                    width : 300,
                    editor : this.comboTipos,
                    sortable: true,
                    renderer : Ext.util.Format.comboRenderer(this.comboTipos)
                },
                {
                    header : 'Nom',
                    dataIndex : 'nombre',
                    width : 600,
                    sortable: true,
                    editor : new Ext.form.TextField(
                    {
                        allowBlank : false,
                        blankText : 'El camp "Nom" es requerit'
                    })
                },
                {
                    header : 'Fitxer',
                    dataIndex : 'fichero',
                    width : 600,
                    sortable: true,
                    editor : new Ext.form.TextField(
                    {
                        allowBlank : false,
                        blankText : 'El camp "Fitxer" es requerit'
                    })
                } ]
            }),
            tbar :
            {
                items : [ botonInsertar, '-', botonBorrar ]
            }
        });
    },
    buildComboTipos : function()
    {
        var ref = this;

        this.comboTipos = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            valueField : 'id',
            displayField : 'nombre',
            allowBlank : false,
            mode : 'local',
            store : new Ext.data.ArrayStore(
            {
                autoLoad : true,
                fields : [ 'id', 'nombre' ],
                data : ref.comboTiposData()
            })
        });
    },

    comboTiposData : function()
    {
        var result = [];

        for ( var it in UJI.UPO.TipoPlantilla)
        {
            result.push([ it, initCap(UJI.UPO.TipoPlantilla[it]) ]);
        }

        return result;
    },

    buildStore : function()
    {
        this.store = new Ext.data.Store(
        {
            url : 'rest/plantilla',
            autoLoad : true,
            restful : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Plantilla',
                idProperty : 'id'
            }, [ 'id', 'tipo', 'nombre', 'fichero' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },
    buildEditor : function()
    {
        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancel·lar',
            errorSummary : false
        });
    },
    buildBotonInsertar : function()
    {
        var ref = this;

        this.botonInsertar = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function(btn, event)
            {
                var rec = new ref.store.recordType(
                {
                    nombre : '',
                    tipo : UJI.UPO.TipoPlantilla.WEB
                });

                ref.editor.stopEditing();
                ref.store.insert(0, rec);
                ref.editor.startEditing(0);

            }
        });
    },
    buildBotonBorrar : function()
    {
        var ref = this;
        this.botonBorrar = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(btn, event)
            {
                var rec = ref.grid.getSelectionModel().getSelected();
                if (!rec)
                {
                    Ext.Msg.alert('Información', 'És necessari seleccionar una fila per esborrar');
                    return false;
                }
                else
                {
                    Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                    {
                        if (btn == 'yes')
                        {
                            ref.store.remove(rec);
                        }
                    });
                }
            }
        });
    }
});
