Ext.ns('UJI.UPO');

UJI.UPO.Grupos = Ext.extend(Ext.Panel,
{
    title : 'Grups',
    layout : 'vbox',
    closable : true,
    layoutConfig : {
        align : 'stretch'
    },
    grid : {},
    store : {},
    editor : {},
    gridItems : {},
    storeItems : {},
    editorItems : {},
    storeComboItems : {},
    storeComboItemsNoAsignados : {},
    comboItems : {},
    comboItemsNoAsignados : {},
    botonInsertar : {},
    botonBorrar : {},
    botonInsertarItems : {},
    botonBorrarItems : {},
    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.Grupos.superclass.initComponent.call(this);

        Ext.util.Format.comboRenderer = function(combo)
        {
            return function(value)
            {
                var record = combo.findRecord(combo.valueField, value);
                return record ? record.get(combo.displayField) : combo.valueNotFoundText;
            };
        };

        this.initUI();
        this.add(this.grid);
        this.add(this.gridItems);
    },
    initUI : function()
    {
        this.buildStore();
        this.buildStoreItems();
        this.buildEditor();
        this.buildEditorItems();
        this.buildComboItems();
        this.buildComboItemsNoAsignados();
        this.buildBotonInsertar();
        this.buildBotonBorrar();
        this.buildBotonInsertarItems();
        this.buildBotonBorrarItems();

        var ref = this;

        this.grid = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            viewConfig : {
                forceFit : true
            },
            selModel : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : ref.store,
            plugins : [ref.editor],
            colModel : new Ext.grid.ColumnModel(
            {
                columns : [
                    {
                        header : "Id",
                        hidden : false,
                        dataIndex : 'id',
                        width : 50,
                        sortable: true
                    },
                    {
                        header : "Grup Català",
                        dataIndex : 'nombreCA',
                        width : 200,
                        sortable: true,
                        editor : new Ext.form.TextField(
                        {
                            allowBlank : false,
                            blankText : 'El "nom" és obligatori'
                        })
                    },
                    {
                        header : "Grup Espanyol",
                        dataIndex : 'nombreES',
                        width : 200,
                        sortable: true,
                        editor : new Ext.form.TextField(
                        {
                            allowBlank : false,
                            blankText : 'El "nom" és obligatori'
                        })
                    },
                    {
                        header : "Grup Anglès",
                        dataIndex : 'nombreEN',
                        width : 200,
                        sortable: true,
                        editor : new Ext.form.TextField(
                        {
                            allowBlank : false,
                            blankText : 'El "nom" és obligatori'
                        })
                    }]
            }),
            listeners : {
                rowclick : function()
                {

                    ref.botonInsertarItems.enable();

                    var record = ref.grid.getSelectionModel().getSelected();
                    if (record != null && !record.dirty)
                    {
                        url = '/upo/rest/grupo/' + record.id + '/item';
                        ref.storeItems.proxy.conn.url = url;
                        ref.storeItems.load();
                    }

                }
            },
            tbar : {
                items : [ref.botonInsertar, '-', ref.botonBorrar]
            }
        });

        this.gridItems = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            viewConfig : {
                forceFit : true
            },
            selModel : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : ref.storeItems,
            plugins : [ref.editorItems],
            colModel : new Ext.grid.ColumnModel(
            {
                columns : [
                    {
                        header : "Grup Id",
                        hidden : true,
                        dataIndex : 'grupoId',
                        sortable: true
                    },
                    {
                        header : "Ítem",
                        dataIndex : 'id',
                        width : 200,
                        editor : ref.comboItemsNoAsignados,
                        sortable: true,
                        renderer : Ext.util.Format.comboRenderer(ref.comboItems)
                    },
                    {
                        header : "Url",
                        dataIndex : 'url',
                        width : 300,
                        sortable: true
                    },
                    {
                        header : 'Ordre',
                        dataIndex : 'orden',
                        allowBlank : false,
                        width : 20,
                        sortable: true,
                        editor : new Ext.form.NumberField(
                        {
                            allowBlank : false,
                            allowDecimals : false,
                            blankText : 'El camp "Ordre" es requerit',
                            value : 1
                        })
                    }]
            }),
            listeners : {
                rowclick : function()
                {
                    ref.botonBorrarItems.enable();
                }
            },
            tbar : {
                items : [ref.botonInsertarItems, '-', ref.botonBorrarItems]
            }
        });

    },

    buildStore : function()
    {
        this.store = new Ext.data.Store(
        {
            url : '/upo/rest/grupo',
            restful : true,
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Grupo',
                id : 'id'
            }, ['id', 'nombreCA', 'nombreES', 'nombreEN']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildStoreItems : function()
    {
        ref = this;
        this.storeItems = new Ext.data.Store(
        {
            proxy : new Ext.data.HttpProxy(
            {
                url : '/upo/rest/menu/item',
                listeners : {
                    beforewrite : function()
                    {
                        var record = ref.grid.getSelectionModel().getSelected();
                        url = '/upo/rest/grupo/' + record.id + '/item';
                        ref.storeItems.proxy.setUrl(url);
                    }
                }
            }),
            restful : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'RecordUI',
                idProperty : 'id',
                id : 'id'
            }, ['id', 'grupoId', 'url', 'orden']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners : {
                load : function()
                {
                    if (this.totalLength > 0)
                    {
                        ref.botonBorrar.disable();
                    }
                    else
                    {
                        ref.botonBorrar.enable();
                    }
                }
            }
        });
    },

    buildStoreComboItems : function()
    {
        ref = this;

        this.storeComboItems = new Ext.data.Store(
        {
            url : '/upo/rest/item',
            restful : true,
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Item',
                id : 'id'
            }, ['id', 'nombreCA', 'url']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });

        this.storeComboItems.on("load", function()
        {
            ref.store.load();
        });

    },

    buildComboItems : function()
    {
        var ref = this;
        this.buildStoreComboItems();

        this.comboItems = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            forceSelection : true,
            editable : false,
            displayField : 'nombreCA',
            valueField : 'id',
            store : ref.storeComboItems
        });
    },

    buildStoreComboItemsNoAsignados : function()
    {
        ref = this;

        this.storeComboItemsNoAsignados = new Ext.data.Store(
        {
            proxy : new Ext.data.HttpProxy(
            {
                url : '/upo/rest/item',
                listeners : {
                    beforeload : function()
                    {
                        var record = ref.grid.getSelectionModel().getSelected();
                        if (record)
                        {
                            url = '/upo/rest/item?grupoId=' + record.id;
                            ref.storeComboItemsNoAsignados.proxy.conn.url = url;
                        }
                    }
                }
            }),
            restful : true,
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Item',
                id : 'id'
            }, ['id', 'nombreCA', 'url', {
                name : 'display',
                convert : function(v, rec)
                {
                    return Ext.DomQuery.selectValue('nombreCA', rec) + ' (' + Ext.DomQuery.selectValue('url', rec) + ')'
                }
            }]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildComboItemsNoAsignados : function()
    {
        var ref = this;
        this.buildStoreComboItemsNoAsignados();

        this.comboItemsNoAsignados = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            forceSelection : true,
            editable : false,
            displayField : 'display',
            valueField : 'id',
            store : ref.storeComboItemsNoAsignados
        });
    },

    buildEditor : function()
    {
        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancel·lar',
            errorSummary : false
        });
    },

    buildEditorItems : function()
    {
        this.editorItems = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancel·lar',
            errorSummary : false,
            listeners : {
                beforeedit : function(rowEditor, rowIndex)
                {
                    var update = (ref.gridItems.getStore().getAt(rowIndex).get('id'));

                    if (update)
                    {
                        var store = ref.storeComboItems;
                        var value = update;

                        var index = store.find('id', value);
                        var record = store.getAt(index);

                        var storeNoAsignados = ref.storeComboItemsNoAsignados;

                        var rec = new storeNoAsignados.recordType(
                        {
                            id : record.data.id,
                            nombreCA : record.data.nombreCA
                        });

                        storeNoAsignados.suspendEvents();
                        storeNoAsignados.insert(0, rec);
                        storeNoAsignados.resumeEvents();

                        ref.gridItems.getColumnModel().columns[1].editor.setDisabled(true);
                    }
                    else
                    {
                        ref.gridItems.getColumnModel().columns[1].editor.setDisabled(false);
                    }
                }
            }
        });
    },

    buildBotonInsertar : function()
    {
        var ref = this;
        this.botonInsertar = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function()
            {
                var rec = new ref.store.recordType(
                {
                    nombreCA : '',
                    nombreES : '',
                    nombreEN : ''
                });

                ref.editor.stopEditing();
                ref.store.insert(0, rec);
                ref.editor.startEditing(0);
            }
        });
    },

    buildBotonInsertarItems : function()
    {
        var ref = this;
        this.botonInsertarItems = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            disabled : true,
            handler : function()
            {
                var grupoRecord = ref.grid.getSelectionModel().getSelected();

                var rec = new ref.storeItems.recordType(
                {
                    grupoId : grupoRecord.id,
                    id : '',
                    url : '-',
                    orden : 1
                });

                var record = ref.grid.getSelectionModel().getSelected();
                if (record)
                {
                    url = '/upo/rest/item?grupoId=' + record.id;
                    ref.storeComboItemsNoAsignados.proxy.conn.url = url;
                    ref.storeComboItemsNoAsignados.load();
                }

                ref.editorItems.stopEditing();
                ref.storeItems.insert(0, rec);
                ref.editorItems.startEditing(0);
            }
        });
    },

    buildBotonBorrar : function()
    {
        var ref = this;
        this.botonBorrar = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function()
            {
                var rec = ref.grid.getSelectionModel().getSelected();
                if (!rec)
                {
                    Ext.Msg.alert('Informació', 'És necessari seleccionar una fila per esborrar');
                    return false;
                }
                else
                {
                    Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                    {
                        if (btn == 'yes')
                        {
                            var rec = ref.grid.getSelectionModel().getSelected();
                            ref.store.remove(rec);
                        }
                    });
                }
            }
        });
    },

    buildBotonBorrarItems : function()
    {
        var ref = this;
        this.botonBorrarItems = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            disabled : true,
            handler : function()
            {
                Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                {
                    if (btn == 'yes')
                    {
                        var rec = ref.gridItems.getSelectionModel().getSelected();
                        if (!rec)
                        {
                            return false;
                        }
                        ref.storeItems.remove(rec);
                    }
                });
            }
        });
    }
});