Ext.ns('UJI.UPO');

UJI.UPO.UsuariosExternos = Ext.extend(Ext.Panel,
{
    title : 'Usuaris externs',
    layout : 'vbox',
    closable : true,
    layoutConfig : {
        align : 'stretch'
    },

    grid : {},
    store : {},
    botonInsertar : {},
    botonBorrar : {},

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.UPO.UsuariosExternos.superclass.initComponent.call(this);

        this.initUI();

        this.add(this.grid);
    },

    initUI : function()
    {
        this.buildStore();
        this.buildBotonInsertar();
        this.buildBotonBorrar();
        this.buildGrid();
    },

    buildStore : function()
    {
        this.store = new Ext.data.Store(
        {
            url : '/upo/rest/persona/usuario-externo',
            autoLoad : true,
            restful : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'PersonaTodas',
                idProperty : 'id'
            }, ['id', 'nombreCompleto']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildBotonBorrar : function()
    {
        this.botonBorrar = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(btn, event)
            {
                var rec = this.grid.getSelectionModel().getSelected();
                if (!rec)
                {
                    Ext.Msg.alert('Información', 'És necessari seleccionar una fila per esborrar');
                    return false;
                }
                else
                {
                    Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre?', function(btn, text)
                    {
                        if (btn == 'yes')
                        {

                            this.store.remove(rec);
                        }
                    }, this);
                }
            },
            scope : this
        });
    },

    buildBotonInsertar : function()
    {
        var ref = this;

        this.botonInsertar = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function(btn, event)
            {
                var window = new Ext.ux.uji.form.LookupWindow(
                {
                    appPrefix : 'upo',
                    bean : 'persona'
                });

                window.on('LookoupWindowClickSeleccion', function(record)
                {
                    var rec = new ref.store.recordType(
                    {
                        id : record.data.id
                    });

                    ref.store.insert(0, rec);
                });

                window.show();
            },
            scope : this
        });
    },

    buildGrid : function()
    {
        this.grid = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            viewConfig : {
                forceFit : true
            },
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : this.store,
            colModel : new Ext.grid.ColumnModel(
            {
                columns : [
                    {
                        header : 'Id',
                        width : 30,
                        dataIndex : 'id',
                        sortable : true
                    },
                    {
                        header : 'Nombre Completo',
                        width : 500,
                        dataIndex : 'nombreCompleto',
                        sortable : true
                    }]
            }),
            tbar : {
                items : [this.botonInsertar, '-', this.botonBorrar]
            }
        });
    }
});