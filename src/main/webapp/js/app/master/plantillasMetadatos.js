Ext.ns('UJI.UPO');

UJI.UPO.PlantillasMetadatos = Ext.extend(Ext.Panel,
{
    title : 'Plantilles metadades',
    layout : 'vbox',
    closable : true,
    layoutConfig : {
        align : 'stretch'
    },

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.PlantillasMetadatos.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildStoreTiposMetadatos();
        this.buildStoreValoresMetadatos();

        this.buildBotonBorrarValor();
        this.buildBotonInsertarValor();

        this.buildGridValoresMetadatos();
        this.buildGridEsquemaMetadatos();

        this.buildSaveFormButton();
        this.buildCloseWindowButton();

        this.buildFormAtributosMetadatos();
        this.buildWindowAtributosMetadatos();

        this.buildBotonInsertarAtributo();
        this.buildBotonBorrarAtributo();
        this.buildStoreAtributosMetadatos();
        this.buildGridAtributosMetadatos();

        this.add(this.gridEsquemaMetadatos);
        this.add(this.gridAtributosMetadatos);

        this.windowAtributosMetadatos.add(this.formAtributosMetadatos);
        this.windowAtributosMetadatos.add(this.gridValoresMetadatos);
        this.windowAtributosMetadatos.getFooterToolbar().addButton(this.saveFormButton);
        this.windowAtributosMetadatos.getFooterToolbar().addButton(this.closeWindowButton);
    },

    buildStoreTiposMetadatos : function()
    {
        var ref = this;

        this.storeTiposMetadatos = new Ext.data.Store(
        {
            url : '/upo/rest/tipo-metadato',
            autoLoad : true,
            restful : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'TipoMetadato',
                id : 'id'
            }, ['id', 'nombre']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildStoreValoresMetadatos : function()
    {
        var ref = this;

        this.storeValoresMetadatos = new Ext.data.Store(
        {
            url : '/upo/rest/valor-metadato',
            autoLoad : false,
            autoSave : false,
            restful : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'ValorMetadato',
                id : 'id'
            }, ['id', 'valorCA', 'valorES', 'valorEN']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildWindowAtributosMetadatos : function()
    {
        this.windowAtributosMetadatos = new Ext.Window(
        {
            title : 'Metadades',
            layout : 'form',
            width : 700,
            height : 520,
            modal : true,
            padding : 10,
            closeAction : 'hide',
            fbar : []
        });
    },

    buildBotonBorrarAtributo : function()
    {
        this.botonBorrarAtributo = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(btn, event)
            {
                var rec = this.gridAtributosMetadatos.getSelectionModel().getSelected();
                if (!rec)
                {
                    Ext.Msg.alert('Información', 'És necessari seleccionar una fila per esborrar');
                    return false;
                }
                else
                {
                    Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre?', function(btn, text)
                    {
                        if (btn == 'yes')
                        {
                            this.storeAtributosMetadatos.remove(rec);
                        }
                    }, this);
                }
            },
            scope : this
        });
    },

    buildBotonInsertarAtributo : function()
    {
        var ref = this;

        this.botonInsertarAtributo = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function(btn, event)
            {
                var selection = ref.gridEsquemaMetadatos.grid.getSelectionModel().getSelected();

                if (!selection)
                {
                    return;
                }

                ref.resetForm();
                ref.windowAtributosMetadatos.show();
            }
        });
    },

    buildGridEsquemaMetadatos : function()
    {
        this.gridEsquemaMetadatos = new Ext.ux.uji.grid.MasterGrid(
        {
            url : '/upo/rest/esquema-metadato',
            record : 'EsquemaMetadato',
            flex : 1,
            columns : {
                'id' : {
                    header : 'Id',
                    width : 50
                },
                'nombre' : {
                    header : 'Nombre',
                    width : 600
                }
            }
        });

        this.gridEsquemaMetadatos.grid.on("click", this.loadAtributos, this);
    },

    buildStoreAtributosMetadatos : function()
    {
        this.storeAtributosMetadatos = new Ext.data.Store(
        {
            url : '/upo/rest/esquema-metadato/-1/atributo',
            restful : true,
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'AtributoMetadato',
                id : 'id'
            }, ['id', 'clave', 'orden']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildGridAtributosMetadatos : function()
    {
        var ref = this;

        this.gridAtributosMetadatos = new Ext.grid.GridPanel(
        {
            layout : 'vbox',
            flex : 1,
            viewConfig : {
                forceFit : true
            },
            layoutConfig : {
                align : 'stretch'
            },
            selModel : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : ref.storeAtributosMetadatos,
            colModel : new Ext.grid.ColumnModel(
            {
                columns : [
                    {
                        header : "Id",
                        dataIndex : 'id',
                        width : 50,
                        sortable : true
                    },
                    {
                        header : "Clau",
                        dataIndex : 'clave',
                        width : 600,
                        sortable : true
                    },
                    {
                        header : "Ordre",
                        dataIndex : 'orden',
                        width : 30,
                        sortable : true
                    }]
            }),
            tbar : {
                items : [ref.botonInsertarAtributo, ref.botonBorrarAtributo]
            },
            listeners : {
                rowdblclick : function(grid, index)
                {
                    var record = grid.getStore().getAt(index);
                    var id = record.id;

                    ref.windowAtributosMetadatos.show();
                    ref.loadForm(id);
                }
            }
        });
    },

    buildGridValoresMetadatos : function()
    {
        var ref = this;

        this.growEditor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Desar',
            cancelText : 'Cancel·lar',
            errorSummary : false
        });

        this.gridValoresMetadatos = new Ext.grid.GridPanel(
        {
            layout : 'vbox',
            flex : 1,
            viewConfig : {
                forceFit : true,
                markDirty : false
            },
            layoutConfig : {
                align : 'stretch'
            },
            selModel : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : ref.storeValoresMetadatos,
            plugins : [this.growEditor],
            colModel : new Ext.grid.ColumnModel(
            {
                columns : [
                    {
                        header : "Id",
                        dataIndex : 'id',
                        width : 100,
                        sortable : true
                    },
                    {
                        header : "Valor CA",
                        dataIndex : 'valorCA',
                        width : 200,
                        sortable : true,
                        editor : new Ext.form.TextField(
                        {
                            allowBlank : false
                        })
                    },
                    {
                        header : "Valor ES",
                        dataIndex : 'valorES',
                        width : 200,
                        sortable : true,
                        editor : new Ext.form.TextField(
                        {
                            allowBlank : false
                        })
                    },
                    {
                        header : "Valor EN",
                        dataIndex : 'valorEN',
                        width : 200,
                        sortable : true,
                        editor : new Ext.form.TextField(
                        {
                            allowBlank : false
                        })
                    }]
            }),
            tbar : {
                items : [ref.botonInsertarValor, ref.botonBorrarValor]
            }
        });
    },

    buildBotonBorrarValor : function()
    {
        this.botonBorrarValor = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(btn, event)
            {
                var rec = this.gridValoresMetadatos.getSelectionModel().getSelected();
                if (rec)
                {
                    this.storeValoresMetadatos.remove(rec);
                }
            },
            scope : this
        });
    },

    buildBotonInsertarValor : function()
    {
        var ref = this;

        this.botonInsertarValor = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function(btn, e)
            {
                var rec = new ref.storeValoresMetadatos.recordType({});

                ref.growEditor.stopEditing();
                ref.storeValoresMetadatos.insert(0, rec);
                ref.growEditor.startEditing(0);
            }
        });
    },

    loadAtributos : function()
    {
        var selection = this.gridEsquemaMetadatos.grid.getSelectionModel().getSelected();
        var store = this.storeAtributosMetadatos;

        if (selection)
        {
            store.proxy.conn.url = '/upo/rest/esquema-metadato/' + selection.id + '/atributo';
            store.load();
        }
    },

    buildCloseWindowButton : function()
    {
        var ref = this;

        this.closeWindowButton = new Ext.Button(
        {
            text : 'Cancel·lar',
            handler : function(button, event)
            {
                ref.windowAtributosMetadatos.hide();
            }
        });
    },

    buildSaveFormButton : function()
    {
        var ref = this;

        this.saveFormButton = new Ext.Button(
        {
            text : 'Desar',
            handler : function(button, event)
            {
                var selection = ref.gridEsquemaMetadatos.grid.getSelectionModel().getSelected();

                if (!selection)
                {
                    return;
                }

                var form = ref.windowAtributosMetadatos.items.items[0].getForm();
                var valores = ref.storeValoresMetadatos.data.items;

                var xml = '';

                for (var i = 0; i < valores.length; i++)
                {
                    var record = valores[i].data;

                    xml += 'ID:' + record.id + '|'
                    xml += 'CA:' + record.valorCA + '|'
                    xml += 'ES:' + record.valorES + '|'
                    xml += 'EN:' + record.valorEN + '||'
                }

                if (form.isValid())
                {
                    form.submit(
                    {
                        url : '/upo/rest/esquema-metadato/' + selection.id + "/atributo/",
                        params : {
                            valores : xml
                        },
                        method : 'post',
                        success : function(form, action)
                        {
                            ref.windowAtributosMetadatos.hide();
                            ref.loadAtributos();
                            ref.resetForm();
                        }
                    })
                }
                else
                {
                    Ext.Msg.alert('Informació', 'falten camps per reomplir');
                }
            }
        });
    },

    loadForm : function(id)
    {
        var form = this.windowAtributosMetadatos.items.items[0].getForm();
        var esquema = this.gridEsquemaMetadatos.grid.getSelectionModel().getSelected();

        form.load(
        {
            url : '/upo/rest/esquema-metadato/' + esquema.id + '/atributo/' + id,
            method : 'GET'
        });
    },

    resetForm : function()
    {
        var form = this.windowAtributosMetadatos.items.items[0].getForm();
        var storeValores = this.storeValoresMetadatos;

        form.reset();
        storeValores.removeAll();

        form.findField("publicable").setValue(1);
    },

    buildFormAtributosMetadatos : function()
    {
        var ref = this;

        this.formAtributosMetadatos = new Ext.form.FormPanel(
        {
            flex : 2,
            padding : 10,
            reader : new Ext.data.XmlReader(
            {
                record : 'AtributoMetadato'
            }, ['clave', 'id',
                {
                    name : 'tipoMetadatoId',
                    convert : function(value, record)
                    {
                        ref.onSelectCombo(value);

                        return value;
                    }
                }, 'nombreClaveCA', 'nombreClaveES', 'nombreClaveEN', 'esquemaId', 'valorDefectoCA', 'valorDefectoES', 'valorDefectoEN', 'permiteBlanco', 'publicable', 'orden',
                {
                    name : 'valores',
                    convert : function(value, record)
                    {
                        var storeValores = ref.storeValoresMetadatos;
                        storeValores.removeAll();

                        var valores = Ext.DomQuery.jsSelect('valores>value', record);

                        for (var i = 0; i < valores.length; i++)
                        {
                            var valor = valores[i];
                            var rec = new ref.storeValoresMetadatos.recordType(
                            {
                                id : Ext.DomQuery.selectValue('id', valor),
                                valorCA : Ext.DomQuery.selectValue('valorCA', valor),
                                valorES : Ext.DomQuery.selectValue('valorES', valor),
                                valorEN : Ext.DomQuery.selectValue('valorEN', valor),
                            });

                            storeValores.insert(0, rec);
                        }

                        return "";
                    }
                }]),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, ['success', 'msg']),
            items : [
                {
                    xtype : 'textfield',
                    fieldLabel : 'Id',
                    name : 'id',
                    hidden : true
                }
                , new Ext.form.CompositeField(
                {
                    items : [
                        {
                            xtype : 'textfield',
                            fieldLabel : 'Clau',
                            name : 'clave',
                            allowBlank : false
                        },
                        {
                            xtype : 'label',
                            text : 'Ordre: ',
                            style : 'padding-top: 3px'
                        }, {
                            xtype : 'numberfield',
                            fieldLabel : 'Ordre',
                            name : 'orden',
                            allowBlank : false,
                            allowDecimals : false,
                            width : 30,
                            style : 'text-align: right'
                        }]
                }), new Ext.form.ComboBox(
                {
                    valueField : 'id',
                    name : 'tipoMetadatoId',
                    displayField : 'nombre',
                    hiddenName : 'tipoMetadatoId',
                    fieldLabel : 'Tipus Metadada',
                    anchor : '90%',
                    triggerAction : 'all',
                    store : ref.storeTiposMetadatos,
                    forceSelection : true,
                    mode : 'local',
                    allowBlank : false,
                    forceSelection : true,
                    editable : false,
                    listeners : {
                        'select' : function(combo, record, index)
                        {
                            ref.onSelectCombo(record.id);
                        }
                    }
                }), new Ext.form.CompositeField(
                {
                    items : [
                        {
                            xtype : 'textfield',
                            fieldLabel : 'Nom clau CA',
                            name : 'nombreClaveCA',
                            allowBlank : false
                        },
                        {
                            xtype : 'label',
                            text : 'ES: ',
                            style : 'padding-top: 3px'
                        },
                        {
                            xtype : 'textfield',
                            name : 'nombreClaveES',
                            allowBlank : false
                        },
                        {
                            xtype : 'label',
                            text : 'EN: ',
                            style : 'padding-top: 3px'
                        },
                        {
                            xtype : 'textfield',
                            name : 'nombreClaveEN',
                            allowBlank : false
                        }]
                }), new Ext.form.CompositeField(
                {
                    items : [
                        {
                            xtype : 'textfield',
                            fieldLabel : 'Valor defecte CA',
                            name : 'valorDefectoCA'
                        },
                        {
                            xtype : 'label',
                            text : 'ES: ',
                            style : 'padding-top: 3px'
                        },
                        {
                            xtype : 'textfield',
                            name : 'valorDefectoES'
                        },
                        {
                            xtype : 'label',
                            text : 'EN: ',
                            style : 'padding-top: 3px'
                        },
                        {
                            xtype : 'textfield',
                            name : 'valorDefectoEN'
                        }]
                }), new Ext.form.Checkbox(
                {
                    fieldLabel : 'Permet blanc',
                    name : 'permiteBlanco',
                    inputValue : 1
                }), new Ext.form.Checkbox(
                {
                    fieldLabel : 'Publicable',
                    name : 'publicable',
                    checked : true,
                    inputValue : 1
                })]
        })
    },

    onSelectCombo : function(tipo)
    {
        var gridValores = this.gridValoresMetadatos;
        var form = this.formAtributosMetadatos.getForm();
        var storeValores = this.storeValoresMetadatos;
        var valorDefectoCA = form.findField("valorDefectoCA");
        var valorDefectoES = form.findField("valorDefectoES");
        var valorDefectoEN = form.findField("valorDefectoEN");

        if (UJI.UPO.TipoMetadato[tipo].tipo === 'combo')
        {
            gridValores.enable();
            valorDefectoCA.disable();
            valorDefectoES.disable();
            valorDefectoEN.disable();
            valorDefectoCA.setValue('');
            valorDefectoES.setValue('');
            valorDefectoEN.setValue('');
        }
        else
        {
            gridValores.disable();
            valorDefectoCA.enable();
            valorDefectoES.enable();
            valorDefectoEN.enable();
            storeValores.removeAll();
        }
    }
});