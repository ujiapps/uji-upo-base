Ext.ns('UJI.UPO');

UJI.UPO.Origenes = Ext.extend(Ext.ux.uji.grid.MasterGrid,
{
    title : 'Orígenes del portal',
    url : '/upo/rest/origen',
    record : 'OrigenInformacion',
    columns : {
        'id' : {
            header : 'Id',
            width : 1,
            sortable : true
        },
        'nombre' : {
            header : 'Nom Català',
            width : 2,
            sortable : true
        },
        'nombreEs' : {
            header : 'Nom Espanyol',
            width : 2,
            sortable : true
        },
        'nombreEn' : {
            header : 'Nom Anglés',
            width : 2,
            sortable : true
        },
        'url' : {
            header : 'URL',
            width : 4,
            sortable : true
        }
    }
});