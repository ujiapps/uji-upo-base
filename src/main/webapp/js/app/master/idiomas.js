Ext.ns('UJI.UPO');

UJI.UPO.Idiomas = Ext.extend(Ext.ux.uji.grid.MasterGrid,
{
    title : 'Idiomes',
    url : '/upo/rest/idioma',
    record : 'Idioma',
    columns : {
        'id' : {
            header : 'Id',
            width : 4,
            sortable : true
        },
        'nombre' : {
            header : 'Idioma',
            witdh : 4,
            sortable : true
        }
    }
});