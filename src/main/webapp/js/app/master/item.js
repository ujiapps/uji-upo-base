Ext.ns('UJI.UPO');

UJI.UPO.Items = Ext.extend(Ext.ux.uji.grid.MasterGrid,
{
    title : 'Items',
    url : '/upo/rest/item',
    record : 'Item',
    columns : {
        'id' : {
            header : 'Id',
            width : 1,
            sortable : true
        },
        'nombreCA' : {
            header : 'Nom Català',
            width : 2,
            sortable : true
        },
        'nombreES' : {
            header : 'Nom Espanyol',
            width : 2,
            sortable : true
        },
        'nombreEN' : {
            header : 'Nom Anglés',
            width : 2,
            sortable : true
        },
        'url' : {
            header : 'URL',
            width : 4,
            sortable : true
        }
    }
});