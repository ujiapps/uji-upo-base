Ext.ns('UJI.UPO');
UJI.UPO.Criterios = Ext.extend(Ext.Panel,
{
    title : 'Criteris',
    layout : 'vbox',
    region : 'center',
    closable : true,
    layoutConfig :
    {
        align : 'stretch'
    },
    editor : {},
    grid : {},
    form : {},
    panelBotones : {},
    store : {},
    botonBuscar : {},
    botonInsertar : {},
    botonBorrar : {},
    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.Criterios.superclass.initComponent.call(this);
        this.initUI();
        this.add(this.grid);
        this.add(this.form);
    },
    initUI : function()
    {
        this.buildStore();
        this.buildEditor();
        this.buildBotonBuscar();
        this.buildBotonInsertar();
        this.buildBotonBorrar();
        var ref = this;
        this.grid = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            viewConfig :
            {
                forceFit : true
            },
            selModel : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : this.store,
            plugins : [ this.editor ],
            colModel : new Ext.grid.ColumnModel(
            {
                columns : [
                {
                    header : "Id Criteris",
                    hidden : true,
                    dataIndex : 'id',
                    sortable: true
                },
                {
                    header : "Nom Criteris",
                    dataIndex : 'nombre',
                    sortable: true,
                    editor : new Ext.form.TextField(
                    {
                        allowBlank : false,
                        blankText : 'El "nom" és obligatori'
                    })
                } ]
            }),
            tbar :
            {
                items : [ ref.botonBuscar, '-', ref.botonInsertar, '-', ref.botonBorrar ]
            },
            listeners :
            {
                rowclick : function()
                {
                    var record = ref.grid.getSelectionModel().getSelected();
                    ref.form.enable();
                    ref.form.load(
                    {
                        url : '/upo/rest/criterio/' + record.id,
                        method : 'get',
                        waitMsg : 'Carregant ...'
                    });
                }
            }
        });

        this.form = new Ext.FormPanel(
        {
            title : 'Detall Criteris',
            frame : true,
            bodyStyle : 'padding:5px 5px 0',
            widthLabel : 5,
            layout : 'hbox',
            disabled : true,
            items : [
            {
                xtype : 'fieldset',
                flex : 1,
                title : 'Proximidad',
                layout : 'form',
                margins :
                {
                    top : 5,
                    right : 10,
                    bottom : 5,
                    left : 10
                },
                items : [
                {
                    fieldLabel : 'Proximidad',
                    labelStyle : 'font-weight:bold',
                    xtype : 'checkbox',
                    name : 'proximidadDiasEvento',
                    inputValue : 'S'
                },
                {
                    xtype : 'spinnerfield',
                    fieldLabel : 'Número Días',
                    name : 'numeroDias',
                    value : 0,
                    hideTrigger : false
                },
                {
                    xtype : 'spinnerfield',
                    fieldLabel : 'Peso',
                    name : 'pesoDiasEnvento',
                    value : 0
                } ]
            },
            {
                xtype : 'fieldset',
                flex : 1,
                title : 'Prioridad',
                layout : 'form',
                margins :
                {
                    top : 5,
                    right : 10,
                    bottom : 5,
                    left : 10
                },
                items : [
                {
                    fieldLabel : 'Prioridad',
                    labelStyle : 'font-weight:bold',
                    xtype : 'checkbox',
                    name : 'prioridad',
                    inputValue : 'S'
                },
                {
                    xtype : 'spinnerfield',
                    fieldLabel : 'Número Días Urgente',
                    name : 'diasUrgente',
                    hideTrigger : false,
                    value : 0
                },
                {
                    xtype : 'spinnerfield',
                    fieldLabel : 'Número Días de Alta',
                    name : 'diasAlta',
                    hideTrigger : false,
                    value : 0
                },
                {
                    xtype : 'spinnerfield',
                    fieldLabel : 'Peso',
                    name : 'pesoPrioridad',
                    value : 0
                } ]
            },
            {
                xtype : 'fieldset',
                flex : 1,
                title : 'Popularidad',
                layout : 'form',
                margins :
                {
                    top : 5,
                    right : 10,
                    bottom : 5,
                    left : 10
                },
                items : [
                {
                    fieldLabel : 'Popularidad',
                    labelStyle : 'font-weight:bold',
                    xtype : 'checkbox',
                    name : 'popularidad',
                    inputValue : 'S'
                },
                {
                    xtype : 'spinnerfield',
                    fieldLabel : 'Número Días',
                    name : 'diasVentanaPopularidad',
                    value : 0,
                    hideTrigger : false
                },
                {
                    xtype : 'spinnerfield',
                    fieldLabel : 'Peso',
                    name : 'pesoPopularidad',
                    value : 0
                } ]
            } ],

            reader : new Ext.data.XmlReader(
            {
                record : '/'
            }, [ 'id', 'nombre',
            {
                name : 'proximidadDiasEvento',
                convert : function(value, record)
                {
                    return (value == 'S') ? true : false;
                }
            }, 'numeroDias', 'pesoDiasEnvento',
            {
                name : 'popularidad',
                convert : function(value, record)
                {
                    return (value == 'S') ? true : false;
                }
            }, 'pesoPopularidad', 'diasVentanaPopularidad',
            {
                name : 'prioridad',
                convert : function(value, record)
                {
                    return (value == 'S') ? true : false;
                }
            }, 'pesoPrioridad', 'diasUrgente', 'diasAlta' ]),

            buttons : [
            {
                text : 'Guardar',
                handler : function(form, action)
                {
                    var record = ref.grid.getSelectionModel().getSelected();
                    if (record != null && !record.dirty)
                    {
                        ref.form.getForm().submit(
                        {
                            params :
                            {
                                criterioId : record.id
                            },
                            url : '/upo/rest/criterio/'
                        });
                    }
                }
            } ]
        });

    },
    buildStore : function()
    {
        this.store = new Ext.data.Store(
        {
            url : '/upo/rest/criterio',
            restful : true,
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Criterio',
                id : 'id'
            }, [ 'id', 'nombre', 'proximidadDiasEvento', 'numeroDias', 'pesoDiasEnvento', 'popularidad', 'pesoPopularidad', 'diasVentanaPopularidad', 'prioridad', 'pesoPrioridad', 'diasUrgente',
                    'diasAlta' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },
    buildEditor : function()
    {
        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancel.lar',
            errorSummary : false
        });
    },
    buildBotonBuscar : function()
    {
        var ref = this;
        this.botonBuscar = new Ext.ux.form.SearchField(
        {
            emptyText : 'Recerca...',
            store : ref.store,
            width : 180
        });
    },
    buildBotonInsertar : function()
    {
        var ref = this;
        this.botonInsertar = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function()
            {
                var rec = new ref.store.recordType(
                {
                    nombre : ''
                });

                ref.editor.stopEditing();
                ref.store.insert(0, rec);
                ref.editor.startEditing(0);
            }
        });
    },
    buildBotonBorrar : function()
    {
        var ref = this;
        this.botonBorrar = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function()
            {
                var rec = ref.grid.getSelectionModel().getSelected();
                if (!rec)
                {
                    return false;
                }
                ref.store.remove(rec);
                ref.form.disable();
            }
        });
    }
});