Ext.ns('UJI.UPO');

UJI.UPO.Franquicias = Ext.extend(Ext.Panel,
{
    title : 'Franquícies del portal',
    layout : 'vbox',
    region : 'center',
    closable : true,
    layoutConfig : {
        align : 'stretch'
    },
    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.Franquicias.superclass.initComponent.call(this);

        Ext.util.Format.comboRenderer = function(combo)
        {
            return function(value)
            {
                var record = combo.findRecord(combo.valueField, value);
                return record ? record.get(combo.displayField) : combo.valueNotFoundText;
            };
        };

        this.initUI();
        this.add(this.grid);
        this.add(this.gridAccesos);
    },
    initUI : function()
    {
        this.buildStore();
        this.buildStoreAccesos();
        this.buildStoreOrigenesInformacion();
        this.buildEditor();
        this.buildEditorAccesos();
        this.buildComboPersonas();
        this.buildComboPersonasNoAsignadas();
        this.buildComboTipoPersona();
        this.buildComboOrigenesInformacion();
        this.buildBotonInsertar();
        this.buildBotonBorrar();
        this.buildBotonInsertarAccesos();
        this.buildBotonBorrarAccesos();
        this.buildBotonInsertarDesdeFranquicia();

        var ref = this;

        this.grid = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            viewConfig : {
                forceFit : true
            },
            selModel : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : this.store,
            plugins : [this.editor],
            colModel : new Ext.grid.ColumnModel(
            {
                columns : [
                    {
                        header : "Id",
                        hidden : false,
                        dataIndex : 'id',
                        width : 100,
                        sortable : true

                    },
                    {
                        header : "Nom franquícia",
                        dataIndex : 'nombre',
                        width : 300,
                        sortable : true,
                        editor : new Ext.form.TextField(
                        {
                            allowBlank : false,
                            blankText : 'El "nom" és obligatori'
                        })
                    },
                    {
                        header : "Altres autors",
                        dataIndex : 'otrosAutores',
                        width : 300,
                        sortable : true,
                        editor : new Ext.form.TextField(
                        {
                            allowBlank : true
                        })
                    },
                    {
                        header : "Origen informació",
                        dataIndex : 'origenInformacionId',
                        width : 300,
                        sortable : true,
                        editor : ref.comboOrigenesInformacion,
                        renderer : Ext.util.Format.comboRenderer(ref.comboOrigenesInformacion)
                    }]
            }),
            listeners : {
                rowclick : function()
                {
                    ref.botonInsertarAccesos.enable();
                    ref.botonInsertarDesdeFranquicia.enable();

                    var record = ref.grid.getSelectionModel().getSelected();
                    if (record != null && !record.dirty)
                    {
                        url = '/upo/rest/franquicia/' + record.id + '/administrador';
                        ref.storeAccesos.proxy.conn.url = url;
                        ref.storeAccesos.load();
                    }
                }
            },
            tbar : {
                items : [ref.botonInsertar, '-', ref.botonBorrar]
            }
        });

        this.gridAccesos = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            viewConfig : {
                forceFit : true
            },
            selModel : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : this.storeAccesos,
            plugins : [this.editorAccesos],
            colModel : new Ext.grid.ColumnModel(
            {
                columns : [
                    {
                        header : "Id franquícia",
                        hidden : true,
                        dataIndex : 'franquiciaId',
                        sortable : true
                    },
                    {
                        header : "Persona",
                        dataIndex : 'id',
                        width : 500,
                        editor : ref.comboPersonasNoAsignadas,
                        sortable : true,
                        renderer : Ext.util.Format.comboRenderer(ref.comboPersonas)
                    },
                    {
                        header : "Tipus",
                        dataIndex : 'tipo',
                        width : 400,
                        editor : ref.comboTipoPersona,
                        sortable : true,
                        renderer : Ext.util.Format.comboRenderer(ref.comboTipoPersona)
                    }]
            }),
            listeners : {
                rowclick : function()
                {
                    ref.botonBorrarAccesos.enable();
                }
            },
            tbar : {
                items : [ref.botonInsertarAccesos, '-', ref.botonBorrarAccesos, '-', ref.botonInsertarDesdeFranquicia]
            }
        });

    },

    buildStore : function()
    {
        this.store = new Ext.data.Store(
        {
            url : '/upo/rest/franquicia',
            restful : true,
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Franquicia',
                id : 'id'
            }, ['id', 'nombre', 'otrosAutores', 'origenInformacionId']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildStoreAccesos : function()
    {
        var ref = this;

        this.storeAccesos = new Ext.data.Store(
        {
            proxy : new Ext.data.HttpProxy(
            {
                url : '/upo/rest/franquicia/administrador',
                listeners : {
                    beforewrite : function()
                    {
                        var record = ref.grid.getSelectionModel().getSelected();
                        url = '/upo/rest/franquicia/' + record.id + '/administrador';
                        ref.storeAccesos.proxy.setUrl(url);
                    }
                }
            }),
            restful : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'RecordUI',
                idProperty : 'id',
                id : 'id'
            }, ['id', 'franquiciaId', 'tipo']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildStoreOrigenesInformacion : function()
    {
        var ref = this;

        this.storeOrigenesInformacion = new Ext.data.Store(
        {
            url : '/upo/rest/origen',
            restful : true,
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'OrigenInformacion',
                id : 'id'
            }, ['id', 'nombre']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });

        this.storeOrigenesInformacion.on("load", function()
        {
            ref.store.load();
        });
    },

    buildStoreComboPersonas : function()
    {
        var ref = this;

        this.storeComboPersonas = new Ext.data.Store(
        {
            url : '/upo/rest/persona',
            restful : true,
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Persona',
                id : 'id'
            }, ['id', 'nombreCompleto']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });

        this.storeComboPersonas.on("load", function()
        {
            ref.storeOrigenesInformacion.load()
        });
    },

    buildComboTipoPersona : function()
    {
        var ref = this;

        var tipoFranquiciaAcceso = [];

        for (value in UJI.UPO.TipoFranquiciaAcceso)
        {
            tipoFranquiciaAcceso.push([value, UJI.UPO.TipoFranquiciaAcceso[value]]);
        }

        this.comboTipoPersona = new Ext.form.ComboBox(
        {
            mode : 'local',
            triggerAction : 'all',
            forceSelection : true,
            editable : false,
            displayField : 'name',
            valueField : 'id',
            store : new Ext.data.ArrayStore(
            {
                fields : ['id', 'name'],
                data : tipoFranquiciaAcceso
            })
        });
    },

    buildComboPersonas : function()
    {
        var ref = this;
        this.buildStoreComboPersonas();

        this.comboPersonas = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            forceSelection : true,
            editable : false,
            displayField : 'nombreCompleto',
            valueField : 'id',
            store : ref.storeComboPersonas
        });
    },

    buildStoreComboPersonasNoAsignadas : function()
    {
        var ref = this;

        this.storeComboPersonasNoAsignadas = new Ext.data.Store(
        {
            proxy : new Ext.data.HttpProxy(
            {
                url : '/upo/rest/persona',
                listeners : {
                    beforeload : function()
                    {
                        var record = ref.grid.getSelectionModel().getSelected();
                        if (record)
                        {
                            url = '/upo/rest/persona?franquiciaId=' + record.id;
                            ref.storeComboPersonasNoAsignadas.proxy.conn.url = url;
                        }
                    }
                }
            }),
            restful : true,
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Persona',
                id : 'id'
            }, ['id', 'nombreCompleto']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildComboPersonasNoAsignadas : function()
    {
        var ref = this;

        this.buildStoreComboPersonasNoAsignadas();

        this.comboPersonasNoAsignadas = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            forceSelection : true,
            editable : false,
            displayField : 'nombreCompleto',
            valueField : 'id',
            store : ref.storeComboPersonasNoAsignadas
        });
    },

    buildComboOrigenesInformacion : function()
    {
        var ref = this;

        this.comboOrigenesInformacion = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            forceSelection : true,
            editable : true,
            displayField : 'nombre',
            valueField : 'id',
            store : ref.storeOrigenesInformacion,
            mode : 'local'
        });
    },

    buildEditor : function()
    {
        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancel·lar',
            errorSummary : false
        });
    },

    buildEditorAccesos : function()
    {
        var ref = this;

        this.editorAccesos = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancel·lar',
            errorSummary : false,
            listeners : {
                beforeedit : function(rowEditor, rowIndex)
                {
                    var update = (rowEditor.grid.getStore().getAt(rowIndex).get('id'));

                    if (update)
                    {
                        var store = ref.storeComboPersonas;
                        var value = update;

                        var index = store.find('id', value);
                        var record = store.getAt(index);

                        var storeNoAsignados = ref.storeComboPersonasNoAsignadas;

                        var rec = new storeNoAsignados.recordType(
                        {
                            id : record.data.id,
                            nombreCompleto : record.data.nombreCompleto
                        });

                        storeNoAsignados.suspendEvents();
                        storeNoAsignados.insert(0, rec);
                        storeNoAsignados.resumeEvents();

                        rowEditor.grid.getColumnModel().columns[1].editor.setDisabled(true);
                    }
                    else
                    {
                        rowEditor.grid.getColumnModel().columns[1].editor.setDisabled(false);
                    }
                }
            }
        });
    },

    buildBotonInsertar : function()
    {
        var ref = this;
        this.botonInsertar = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function()
            {
                var rec = new ref.store.recordType(
                {
                    nombre : '',
                    otrosAutores : ''
                });

                ref.editor.stopEditing();
                ref.store.insert(0, rec);
                ref.editor.startEditing(0);
            }
        });
    },

    buildBotonInsertarAccesos : function()
    {
        var ref = this;
        this.botonInsertarAccesos = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            disabled : true,
            handler : function()
            {
                var franquiciaRecord = ref.grid.getSelectionModel().getSelected();

                var rec = new ref.storeAccesos.recordType(
                {
                    franquiciaId : franquiciaRecord.id,
                    id : '',
                    tipo : ''
                });

                var record = ref.grid.getSelectionModel().getSelected();
                if (record)
                {
                    url = '/upo/rest/persona?franquiciaId=' + record.id;
                    ref.storeComboPersonasNoAsignadas.proxy.conn.url = url;
                    ref.storeComboPersonasNoAsignadas.load();
                }

                ref.editorAccesos.stopEditing();
                ref.storeAccesos.insert(0, rec);
                ref.editorAccesos.startEditing(0);
            }
        });
    },

    buildBotonBorrar : function()
    {
        var ref = this;
        this.botonBorrar = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function()
            {
                var rec = ref.grid.getSelectionModel().getSelected();
                if (!rec)
                {
                    Ext.Msg.alert('Información', 'És necessari seleccionar una fila per esborrar');
                    return false;
                }
                else
                {
                    Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                    {
                        if (btn == 'yes')
                        {
                            ref.store.remove(rec);
                        }
                    });
                }
            }
        });
    },

    buildBotonInsertarDesdeFranquicia : function()
    {
        var ref = this;

        this.botonInsertarDesdeFranquicia = new Ext.Button(
        {
            text : 'Afegir des de franquícia',
            disabled : true,
            iconCls : 'application-add',
            handler : function()
            {
                var window = new UJI.UPO.SeleccionarFranquiciaWindow();

                window.on("close", function()
                {
                    if (this.getSelectedId())
                    {
                        ref.incorporaPersonalDesdeFranquicia(this.getSelectedId());
                    }
                });

                window.show();
            }
        });
    },

    incorporaPersonalDesdeFranquicia : function(franquiciaOrigenId)
    {
        var ref = this;
        var rec = this.grid.getSelectionModel().getSelected();

        if (!rec || !franquiciaOrigenId)
        {
            Ext.Msg.alert('Información', 'Cal seleccionar les franquícies');
            return false;
        }

        var franquiciaDestinoId = rec.id;

        Ext.Ajax.request(
        {
            url : 'rest/franquicia/' + franquiciaDestinoId + '/franquicia/' + franquiciaOrigenId,
            method : 'POST',
            success : function()
            {
                url = '/upo/rest/franquicia/' + franquiciaDestinoId + '/administrador';
                ref.storeAccesos.proxy.conn.url = url;
                ref.storeAccesos.load();
            },
            failure : function()
            {
                Ext.MessageBox.show(
                {
                    title : 'Errada',
                    msg : "Errada en inserir les persones des d'una franquícia",
                    buttons : Ext.MessageBox.OK,
                    icon : Ext.MessageBox.ERROR
                });
            }
        });

    },

    buildBotonBorrarAccesos : function()
    {
        var ref = this;
        this.botonBorrarAccesos = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            disabled : true,
            handler : function()
            {
                Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                {
                    if (btn == 'yes')
                    {
                        var rec = ref.gridAccesos.getSelectionModel().getSelected();
                        if (!rec)
                        {
                            Ext.Msg.alert('Información', 'És necessari seleccionar una fila per esborrar');
                            return false;
                        }
                        ref.storeAccesos.remove(rec);
                    }
                });
            }
        });
    }

});