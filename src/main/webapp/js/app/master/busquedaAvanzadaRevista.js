Ext.ns('UJI.UPO');

UJI.UPO.BusquedaAvanzadaRevista = Ext.extend(Ext.Panel,
{
    title : 'Cerca avançada revista',
    layout : 'vbox',
    closable : true,
    layoutConfig : {
        align : 'stretch'
    },
    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.BusquedaAvanzadaRevista.superclass.initComponent.call(this);

        this.initUI();

        this.add(this.form);
        this.add(this.panelResultados);
    },

    initUI : function()
    {
        this.buildForm();
        this.buildPanelResultados();
    },

    buildPanelResultados : function()
    {
        this.panelResultados = new Ext.Panel(
        {
            flex : 1,
            autoScroll : true,
            border: false,
            tpl : new Ext.XTemplate('<table>', '<tpl for=".">',
            '<tr><td class="x-form-item" style="border: 1px solid grey; padding: 5px; ">',
            '<span style="font-weight: bold">{titulo}</span><br/>({medio}, {fecha})<br/><br/>{resumen}<br/><br/><a href="{url}" target="_blank">{url}</a>',
            '</td></tr>',
            '</tpl>', '</table>')
        });
    },

    buildForm : function()
    {
        var ref = this;

        this.form = new Ext.FormPanel(
        {
            border : false,
            title : 'Cerca avançada revista',
            anchor : '100% 100%',
            padding : 10,
            reader : new Ext.data.XmlReader({}),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, ['success', 'msg']),
            items : [
                {
                    xtype : 'textfield',
                    fieldLabel : 'Títol',
                    name : 'titulo',
                    width : '40%'
                }, {
                    xtype : 'compositefield',
                    items : [
                        {
                            xtype : 'textfield',
                            fieldLabel : 'Resum',
                            name : 'resumen',
                            width : '40%'
                        },
                        {
                            xtype : 'radio',
                            name : 'resumenOP',
                            inputValue : 'and',
                            boxLabel : 'and',
                            checked : true
                        },
                        {
                            xtype : 'radio',
                            name : 'resumenOP',
                            boxLabel : 'or',
                            inputValue : 'or'
                        }
                    ]
                },
                {
                    xtype : 'compositefield',
                    items : [
                        {
                            xtype : 'textfield',
                            fieldLabel : 'Paraules clau',
                            name : 'palabrasClave',
                            width : '40%'
                        },
                        {
                            xtype : 'radio',
                            name : 'palabrasClaveOP',
                            inputValue : 'and',
                            boxLabel : 'and',
                            checked : true
                        },
                        {
                            xtype : 'radio',
                            name : 'palabrasClaveOP',
                            boxLabel : 'or',
                            inputValue : 'or'
                        }
                    ]
                },
                {
                    xtype : 'textfield',
                    fieldLabel : 'Secció',
                    name : 'seccion',
                    width : '40%'
                },
                {
                    xtype : 'textfield',
                    fieldLabel : 'Mitjà',
                    name : 'medio',
                    width : '40%'
                },
                {
                    xtype : 'datefield',
                    fieldLabel : 'Data inici',
                    name : 'fechaInicio',
                    format : 'd/m/Y'
                },
                {
                    xtype : 'datefield',
                    fieldLabel : 'Data fi',
                    name : 'fechaFin',
                    format : 'd/m/Y'
                },
                {
                    iconCls : 'magnifier',
                    xtype : 'button',
                    text : 'Cercar',
                    style : 'padding-left: 105px',
                    handler : function()
                    {
                        ref.doSubmit();
                    }
                }
            ]
        });
    },

    xmlToArray : function(xml)
    {
        var base = xml.childNodes;
        var values = {};
        var array = [];

        for (var j = 0; j < base.length; j++)
        {
            if (base[j].nodeType == 1)
            {
                var internValues = {};

                internValues["titulo"] = Ext.DomQuery.selectValue('titulo', base[j]);
                internValues["resumen"] = Ext.DomQuery.selectValue('resumen', base[j]);
                internValues["medio"] = Ext.DomQuery.selectValue('medio', base[j]);
                internValues["url"] = Ext.DomQuery.selectValue('url', base[j]);
                internValues["fecha"] = Ext.DomQuery.selectValue('fecha', base[j]);

                array.push(internValues);
            }
        }

        values["values"] = array;

        return values;
    },

    doSubmit : function()
    {
        var ref = this;

        this.form.getForm().submit(
        {
            url : '/upo/rest/revista/',
            method : 'post',
            timeout: '500',
            success : function(form, action)
            {
                var response = action.response.responseXML.documentElement;

                ref.panelResultados.update(ref.xmlToArray(response));
            },
            failure : function()
            {
                Ext.MessageBox.show(
                {
                    title : 'Errada',
                    msg : "Errada realitzant la recerca",
                    buttons : Ext.MessageBox.OK,
                    icon : Ext.MessageBox.ERROR
                });
            }
        });
    }
})
;
