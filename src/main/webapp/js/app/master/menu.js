Ext.ns('UJI.UPO');

UJI.UPO.Menus = Ext.extend(Ext.Panel,
{
    title : 'Menus',
    layout : 'border',
    closable : true,
    grid : {},
    store : {},
    editor : {},
    gridGrupos : {},
    storeGrupos : {},
    editorGrupos : {},
    storeComboGrupos : {},
    storeComboGruposNoAsignados : {},
    comboGrupos : {},
    comboGruposNoAsignados : {},
    botonInsertar : {},
    botonBorrar : {},
    botonInsertarGrupos : {},
    botonBorrarGrupos : {},
    previewMenu : {},
    leftPanel : {},
    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.Menus.superclass.initComponent.call(this);

        Ext.util.Format.comboRenderer = function(combo)
        {
            return function(value)
            {
                var record = combo.findRecord(combo.valueField, value);
                return record ? record.get(combo.displayField) : combo.valueNotFoundText;
            };
        };

        this.initUI();
        this.add(this.leftPanel);
        this.add(this.previewMenu);
    },
    initUI : function()
    {
        this.buildStore();
        this.buildStoreGrupos();
        this.buildEditor();
        this.buildEditorGrupos();
        this.buildComboGrupos();
        this.buildComboGruposNoAsignados();
        this.buildBotonInsertar();
        this.buildBotonBorrar();
        this.buildBotonInsertarGrupos();
        this.buildBotonBorrarGrupos();
        this.buildPreviewMenu();
        this.buildLeftPanel();

        var ref = this;

        this.grid = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            viewConfig : {
                forceFit : true
            },
            selModel : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : ref.store,
            plugins : [ref.editor],
            colModel : new Ext.grid.ColumnModel(
            {
                columns : [
                    {
                        header : "Id",
                        hidden : false,
                        dataIndex : 'id',
                        width : 100,
                        sortable : true
                    },
                    {
                        header : "Menú",
                        dataIndex : 'nombre',
                        width : 600,
                        sortable : true,
                        editor : new Ext.form.TextField(
                        {
                            allowBlank : false,
                            blankText : 'El "nom" és obligatori'
                        })
                    }]
            }),
            listeners : {
                rowclick : function()
                {

                    ref.botonInsertarGrupos.enable();

                    var record = ref.grid.getSelectionModel().getSelected();
                    if (record != null && !record.dirty)
                    {
                        url = '/upo/rest/menu/' + record.id + '/grupo';
                        ref.storeGrupos.proxy.conn.url = url;
                        ref.storeGrupos.load();
                    }

                }
            },
            tbar : {
                items : [ref.botonInsertar, '-', ref.botonBorrar]
            }
        });

        this.gridGrupos = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            viewConfig : {
                forceFit : true
            },
            selModel : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : ref.storeGrupos,
            plugins : [ref.editorGrupos],
            colModel : new Ext.grid.ColumnModel(
            {
                columns : [
                    {
                        header : "Menu Id",
                        hidden : true,
                        dataIndex : 'menuId',
                        sortable: true
                    },
                    {
                        header : "Grup",
                        dataIndex : 'id',
                        width : 500,
                        editor : ref.comboGruposNoAsignados,
                        sortable: true,
                        renderer : Ext.util.Format.comboRenderer(ref.comboGrupos)
                    },
                    {
                        header : 'Ordre',
                        dataIndex : 'orden',
                        allowBlank : false,
                        width : 20,
                        sortable: true,
                        editor : new Ext.form.NumberField(
                        {
                            allowBlank : false,
                            allowDecimals : false,
                            blankText : 'El camp "Ordre" es requerit',
                            value : 1
                        })
                    }]
            }),
            listeners : {
                rowclick : function()
                {
                    ref.botonBorrarGrupos.enable();
                }
            },
            tbar : {
                items : [ref.botonInsertarGrupos, '-', ref.botonBorrarGrupos]
            }
        });

        this.leftPanel.add(this.grid);
        this.leftPanel.add(this.gridGrupos);

    },

    buildLeftPanel : function()
    {
        this.leftPanel = new Ext.Panel(
        {
            layout : 'vbox',
            region : 'center',
            frame : true
        });
    },

    buildPreviewMenu : function()
    {
        this.previewMenu = new Ext.Panel(
        {
            title : 'Previsualización',
            region : 'east',
            frame : true,
            html : 'Previsualización del menu',
            hidden : true
        });
    },

    buildStore : function()
    {
        this.store = new Ext.data.Store(
        {
            url : '/upo/rest/menu',
            restful : true,
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Menu',
                id : 'id'
            }, ['id', 'nombre']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildStoreGrupos : function()
    {
        ref = this;
        this.storeGrupos = new Ext.data.Store(
        {
            proxy : new Ext.data.HttpProxy(
            {
                url : '/upo/rest/menu/grupo',
                listeners : {
                    beforewrite : function()
                    {
                        var record = ref.grid.getSelectionModel().getSelected();
                        url = '/upo/rest/menu/' + record.id + '/grupo';
                        ref.storeGrupos.proxy.setUrl(url);
                    }
                }
            }),
            restful : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'RecordUI',
                idProperty : 'id',
                id : 'id'
            }, ['id', 'menuId', 'orden']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners : {
                load : function()
                {
                    if (this.totalLength > 0)
                    {
                        ref.botonBorrar.disable();
                    }
                    else
                    {
                        ref.botonBorrar.enable();
                    }
                }
            }
        });
    },

    buildStoreComboGrupos : function()
    {
        ref = this;

        this.storeComboGrupos = new Ext.data.Store(
        {
            url : '/upo/rest/grupo',
            restful : true,
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Grupo',
                id : 'id'
            }, ['id', 'nombreCA']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });

        this.storeComboGrupos.on("load", function()
        {
            ref.store.load();
        });

    },

    buildComboGrupos : function()
    {
        var ref = this;
        this.buildStoreComboGrupos();

        this.comboGrupos = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            forceSelection : true,
            editable : false,
            displayField : 'nombreCA',
            valueField : 'id',
            store : ref.storeComboGrupos
        });
    },

    buildStoreComboGruposNoAsignados : function()
    {
        ref = this;

        this.storeComboGruposNoAsignados = new Ext.data.Store(
        {
            proxy : new Ext.data.HttpProxy(
            {
                url : '/upo/rest/grupo',
                listeners : {
                    beforeload : function()
                    {
                        var record = ref.grid.getSelectionModel().getSelected();
                        if (record)
                        {
                            url = '/upo/rest/grupo?menuId=' + record.id;
                            ref.storeComboGruposNoAsignados.proxy.conn.url = url;
                        }
                    }
                }
            }),
            restful : true,
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Grupo',
                id : 'id'
            }, ['id', 'nombreCA']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildComboGruposNoAsignados : function()
    {
        var ref = this;
        this.buildStoreComboGruposNoAsignados();

        this.comboGruposNoAsignados = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            forceSelection : true,
            editable : false,
            displayField : 'nombreCA',
            valueField : 'id',
            store : ref.storeComboGruposNoAsignados
        });
    },

    buildEditor : function()
    {
        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancel.lar',
            errorSummary : false
        });
    },

    buildEditorGrupos : function()
    {
        this.editorGrupos = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancel.lar',
            errorSummary : false,
            listeners : {
                beforeedit : function(rowEditor, rowIndex)
                {
                    var update = (ref.gridGrupos.getStore().getAt(rowIndex).get('id'));

                    if (update)
                    {
                        var store = ref.storeComboGrupos;
                        var value = update;

                        var index = store.find('id', value);
                        var record = store.getAt(index);

                        var storeNoAsignados = ref.storeComboGruposNoAsignados;

                        var rec = new storeNoAsignados.recordType(
                        {
                            id : record.data.id,
                            nombreCA : record.data.nombreCA
                        });

                        storeNoAsignados.suspendEvents();
                        storeNoAsignados.insert(0, rec);
                        storeNoAsignados.resumeEvents();

                        ref.gridGrupos.getColumnModel().columns[1].editor.setDisabled(true);
                    }
                    else
                    {
                        ref.gridGrupos.getColumnModel().columns[1].editor.setDisabled(false);
                    }
                }
            }
        });
    },

    buildBotonInsertar : function()
    {
        var ref = this;
        this.botonInsertar = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function()
            {
                var rec = new ref.store.recordType(
                {
                    nombre : ''
                });

                ref.editor.stopEditing();
                ref.store.insert(0, rec);
                ref.editor.startEditing(0);
            }
        });
    },

    buildBotonInsertarGrupos : function()
    {
        var ref = this;
        this.botonInsertarGrupos = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            disabled : true,
            handler : function()
            {
                var menuRecord = ref.grid.getSelectionModel().getSelected();

                var rec = new ref.storeGrupos.recordType(
                {
                    menuId : menuRecord.id,
                    id : '',
                    orden : 1
                });

                var record = ref.grid.getSelectionModel().getSelected();
                if (record)
                {
                    url = '/upo/rest/grupo?menuId=' + record.id;
                    ref.storeComboGruposNoAsignados.proxy.conn.url = url;
                    ref.storeComboGruposNoAsignados.load();
                }

                ref.editorGrupos.stopEditing();
                ref.storeGrupos.insert(0, rec);
                ref.editorGrupos.startEditing(0);
            }
        });
    },

    buildBotonBorrar : function()
    {
        var ref = this;
        this.botonBorrar = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function()
            {
                var rec = ref.grid.getSelectionModel().getSelected();
                if (!rec)
                {
                    Ext.Msg.alert('Información', 'És necessari seleccionar una fila per esborrar');
                    return false;
                }
                else
                {
                    Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                    {
                        if (btn == 'yes')
                        {
                            ref.store.remove(rec);
                        }
                    });
                }
            }
        });
    },

    buildBotonBorrarGrupos : function()
    {
        var ref = this;
        this.botonBorrarGrupos = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            disabled : true,
            handler : function()
            {
                var rec = ref.gridGrupos.getSelectionModel().getSelected();
                if (!rec)
                {
                    Ext.Msg.alert('Información', 'És necessari seleccionar una fila per esborrar');
                    return false;
                }
                else
                {
                    Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                    {
                        if (btn == 'yes')
                        {
                            ref.storeGrupos.remove(rec);
                        }
                    });
                }
            }
        });
    }
});