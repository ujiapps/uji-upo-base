Ext.ns('UJI.UPO');

UJI.UPO.DuplicarNodoWindow = Ext.extend(Ext.Window,
{
    title : '',
    layout : 'form',
    width : 500,
    height : 125,
    modal : true,
    padding : 10,
    mapa : '',
    mapaId : '',
    mapaName : '',
    mapaUrlCompleta : '',
    requestButton : {},
    cancelButton : {},
    fbar : [],
    nombreNodoField : {},
    listeners :
    {
        afterrender : function(window)
        {
            window.nombreNodoField.focus(false, 500);
        }
    },

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.CrearNodoWindow.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildDuplicarEstructuraButton();
        this.buildDuplicarEstructuraYContenidosButton();
        this.buildCancelButton();

        this.buildNombreNodoField();

        this.add(this.nombreNodoField);

        this.getFooterToolbar().addButton(this.duplicarEstructuraButton);
        this.getFooterToolbar().addButton(this.duplicarEstructuraYContenidosButton);
        this.getFooterToolbar().addButton(this.cancelButton);
    },

    buildNombreNodoField : function()
    {
        var ref = this;

        this.nombreNodoField = new Ext.form.TextField(
        {
            fieldLabel : 'Nom del node',
            allowBlank : false,
            maskRe : /[a-zA-Z0-9_.\-]/,
            regex : /^[a-zA-Z0-9_.\-]+$/i,
            width : '90%',
            value : ref.mapaName,
            selectOnFocus : true
        });
    },

    addRequest : function(nombreNodo, duplicarContenidos)
    {
        var ref = this;

        if (nombreNodo)
        {
            Ext.Ajax.request(
            {
                url : '/upo/rest/mapa/' + ref.mapaId + '/duplicar',
                params :
                {
                    nombreNodo : nombreNodo,
                    duplicarContenidos : duplicarContenidos
                },
                success : function()
                {
                    UJI.Bus.publish("UJI.UPO.Tree.DuplicateNode",
                    {
                        nodoDestino : ref.mapa.parentNode
                    });

                    ref.close();
                },
                failure : function(result, request)
                {
                    Ext.Msg.alert('Error', "S'ha produït una errada");
                    return false;
                },
                method : 'POST'
            });
        }
        else
        {
            Ext.Msg.alert('Error', "El camp Nom del Node és obligatori");
        }
    },

    buildDuplicarEstructuraButton : function()
    {
        var ref = this;

        this.duplicarEstructuraButton = new Ext.Button(
        {
            text : 'Duplicar estructura',
            handler : function()
            {
                if (ref.compruebaDuplicados())
                {
                    Ext.Msg.confirm('Confirmar', 'Esteu segur/a de voler duplicar la estuctura?', function(btn, text)
                    {
                        if (btn == 'yes')
                        {
                            ref.addRequest(ref.nombreNodoField.getValue(), false);
                        }
                    });
                }
            }
        });
    },

    buildDuplicarEstructuraYContenidosButton : function()
    {
        var ref = this;

        this.duplicarEstructuraYContenidosButton = new Ext.Button(
        {
            text : 'Duplicar estructura i continguts',
            handler : function()
            {
                if (ref.compruebaDuplicados())
                {
                    Ext.Msg.confirm('Confirmar', 'Esteu segur/a de voler duplicar la estuctura i els continguts?', function(btn, text)
                    {
                        if (btn == 'yes')
                        {
                            ref.addRequest(ref.nombreNodoField.getValue(), true);
                        }
                    });
                }
            }
        });
    },

    compruebaDuplicados : function()
    {
        var ref = this;

        if (!ref.nombreNodoField.isValid())
        {
            return;
        }

        if (!ref.nombreNodoField.getValue())
        {
            return false;
        }

        if (UJI.UPO.Application.comprobarSiNodoTieneHijoConMismoNombre(ref.mapa.parentNode, ref.nombreNodoField.getValue()))
        {
            Ext.MessageBox.show(
            {
                title : 'Error',
                msg : 'Operació no permesa, ja hi ha un node amb el mateix nom.',
                buttons : Ext.MessageBox.OK,
                icon : Ext.MessageBox.ERROR
            });

            return false;
        }

        return true;
    },

    buildCancelButton : function()
    {
        ref = this;

        this.cancelButton = new Ext.Button(
        {
            text : 'Cancel·lar',
            handler : function()
            {
                ref.close();
            }
        });
    }
});