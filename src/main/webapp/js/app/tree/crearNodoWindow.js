Ext.ns('UJI.UPO');

UJI.UPO.CrearNodoWindow = Ext.extend(Ext.Window,
{
    title : 'Nou node',
    layout : 'form',
    width : 500,
    height : 180,
    modal : true,
    padding : 10,
    mapa : '',
    mapaId : '',
    franquiciaId : '',
    plantillaId : '',
    storeFolders : {},
    addButton : {},
    cancelButton : {},
    fbar : [],
    storeFranquicias : {},
    comboFranquicias : {},
    storePlantillas : {},
    comboPlantillas : {},
    nombreNodo : {},
    listeners : {
        afterrender : function(window)
        {
            window.nombreNodo.focus(false, 500);
        }
    },

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.CrearNodoWindow.superclass.initComponent.call(this);

        this.initUI();

        syncStoreLoad([this.storeFranquicias, this.storePlantillas]);
    },

    initUI : function()
    {
        this.buildAddButton();
        this.buildCancelButton();

        this.buildStoreFranquicias();
        this.buildComboFranquicias();

        this.buildStorePlantillas();
        this.buildComboPlantillas();

        this.buildNombreNodo();

        this.buildStoreFolders();

        this.add(this.nombreNodo);
        this.add(this.comboFranquicias);
        this.add(this.comboPlantillas);

        this.getFooterToolbar().addButton(this.addButton);
        this.getFooterToolbar().addButton(this.cancelButton);
    },

    addNewFolder : function(folderName, franquiciaId, plantillaId)
    {
        if (folderName && franquiciaId)
        {
            if (UJI.UPO.Application.comprobarSiNodoTieneHijoConMismoNombre(this.mapa, folderName))
            {
                Ext.MessageBox.show(
                {
                    title : 'Error',
                    msg : 'Operació no permesa, ja hi ha un node amb el mateix nom.',
                    buttons : Ext.MessageBox.OK,
                    icon : Ext.MessageBox.ERROR
                });

                return false;
            }
            else
            {
                var NodoMapaDefinition = Ext.data.Record.create(['id', 'urlPath', 'mapaId', 'franquiciaId', 'plantillaId']);
                var nodoMapa = new NodoMapaDefinition(
                {
                    id : '',
                    urlPath : folderName,
                    mapaId : this.mapaId,
                    franquiciaId : franquiciaId,
                    plantillaId : plantillaId
                });

                this.storeFolders.add(nodoMapa);
                return true;
            }
        }
        else
        {
            Ext.MessageBox.show(
            {
                title : 'Error',
                msg : 'El Nom i la Franquícia són obligatoris.',
                buttons : Ext.MessageBox.OK,
                icon : Ext.MessageBox.ERROR
            });
            return false;
        }
    },

    buildStoreFranquicias : function()
    {
        var ref = this;

        this.storeFranquicias = new Ext.data.Store(
        {
            url : '/upo/rest/franquicia',
            restful : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Franquicia',
                id : 'id'
            }, ['id', 'nombre']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners : {
                'load' : function(store, records, option)
                {
                    ref.comboFranquicias.setValue(ref.franquiciaId);
                }
            }
        });
    },

    buildComboFranquicias : function()
    {
        var ref = this;

        this.comboFranquicias = new Ext.form.ComboBox(
        {
            valueField : 'id',
            name : 'franquiciaId',
            displayField : 'nombre',
            hiddenName : 'franquiciaId',
            fieldLabel : 'Franquícia',
            anchor : '90%',
            triggerAction : 'all',
            store : ref.storeFranquicias,
            allowBlank : false,
            forceSelection : true
        });
    },

    buildStorePlantillas : function()
    {
        var ref = this;

        this.storePlantillas = new Ext.data.Store(
        {
            url : '/upo/rest/plantilla',
            restful : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Plantilla',
                id : 'id'
            }, ['id', 'nombre']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners : {
                'load' : function(store, records, option)
                {
                    ref.comboPlantillas.setValue(ref.plantillaId);
                }
            }
        });
    },

    buildComboPlantillas : function()
    {
        var ref = this;

        this.comboPlantillas = new Ext.form.ComboBox(
        {
            valueField : 'id',
            name : 'plantillaId',
            displayField : 'nombre',
            hiddenName : 'plantillaId',
            fieldLabel : 'Plantilla Web',
            anchor : '90%',
            triggerAction : 'all',
            store : ref.storePlantillas
        });
    },

    buildNombreNodo : function()
    {
        this.nombreNodo = new Ext.form.TextField(
        {
            fieldLabel : 'Nom del Node',
            allowBlank : false,
            anchor : '90%',
            maskRe : /[a-zA-Z0-9_.\-]/,
            regex : /^[a-zA-Z0-9_.\-]+$/i
        });
    },

    buildAddButton : function()
    {
        var ref = this;
        this.addButton = new Ext.Button(
        {
            text : 'Crear',
            handler : function()
            {
                if (!ref.nombreNodo.isValid())
                {
                    return;
                }

                if (ref.addNewFolder(ref.nombreNodo.getValue(), ref.comboFranquicias.getValue(), ref.comboPlantillas.getValue()))
                {
                    ref.close();
                }
            }
        });
    },

    buildCancelButton : function()
    {
        ref = this;

        this.cancelButton = new Ext.Button(
        {
            text : 'Cancel·lar',
            handler : function()
            {
                ref.close();
            }
        });
    },

    buildStoreFolders : function()
    {
        this.storeFolders = new Ext.data.Store(
        {
            url : '/upo/rest/mapa',
            restful : true,
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'NodoMapa'
            }, ['id', 'urlPath', 'mapaId', 'franquiciaId', 'plantillaId']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners : {
                'save' : function(store, batch, data)
                {
                    store.clearModified();
                    UJI.Bus.publish("UJI.UPO.Tree.ContentNodeChanged");
                }
            }
        });
    }
});