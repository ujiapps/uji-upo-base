Ext.ns('UJI.UPO');

Ext.util.Format.comboRenderer = function(combo)
{
    return function(value)
    {
        var record = combo.findRecord(combo.valueField, value);
        return record ? record.get(combo.displayField) : combo.valueNotFoundText;
    };
};

UJI.UPO.GestionNodoForm = Ext.extend(Ext.Panel,
{
    layout : 'vbox',
    region : 'center',
    form : {},
    layoutConfig :
    {
        align : 'stretch'
    },
    saveButton : {},
    storeFranquicias : {},
    comboFranquicias : {},
    checkMenus : {},
    storeMenus : {},
    comboMenus : {},

    setAdmin : function(administrador)
    {
        if (!administrador)
        {
            this.saveButton.disable();
            this.checkMenus.disable();
            this.comboFranquicias.disable();
            this.comboMenus.disable();
            this.checkSincronizado.disable();
            this.numItemsField.disable();
            this.periodicidadSincroField.disable();
            this.desdeNodoMapaField.disable();
            this.tituloPublicacionCAField.disable();
            this.tituloPublicacionESField.disable();
            this.tituloPublicacionENField.disable();
            this.enlaceDestinoField.disable();
            this.checkPrivado.disable();
            this.checkContenidoRelacionado.disable();
            this.checkPropuestasWeb.disable();
            this.descripcionPropuestasWeb.disable();
            this.checkMostrarReloj.disable();
            this.checkMostrarMenuPerfilesPlegado.disable();
            this.checkMostrarRecursosRelacionados.disable();
            this.checkPaginado.disable();
        }

        this.administrador = administrador;
    },

    initComponent : function()
    {
        var config =
        {
            mapaId : this.mapaId
        };

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.GestionNodoForm.superclass.initComponent.call(this);

        this.initUI();
        this.add(this.form);
    },

    initUI : function()
    {
        this.buildStoreFranquicias();
        this.buildStoreMenus();

        this.buildComboFranquicias();
        this.buildCheckMenus();
        this.buildComboMenus();

        this.buildCheckSincronizado();
        this.buildNumItemsField();
        this.buildPeriodicidadSincroField();
        this.buildDesdeNodoMapaField();

        this.buildTituloPublicacionCAField();
        this.buildTituloPublicacionESField();
        this.buildTituloPublicacionENField();
        this.buildEnlaceDestinoField();
        this.buildCheckContenidoRelacionado();
        this.buildCheckMostrarReloj();
        this.buildCheckMostrarMenuPerfilesPlegado();
        this.buildCheckMostrarRecursosRelacionados();
        this.buildCheckPaginado();

        this.buildCheckPrivado();

        this.buildCheckPropuestasWeb();
        this.buildDescripcionPropuestasWeb();

        this.buildSaveButton();
        this.buildForm();

        this.storeFranquicias.load();
    },

    buildForm : function()
    {
        var ref = this;

        this.form = new Ext.form.FormPanel(
        {
            defaultType : 'textfield',
            autoScroll : true,
            flex : 1,
            frame : false,
            border : false,
            fieldDefaults :
            {
                labelAlign : 'top',
                msgTarget : 'side'
            },
            reader : new Ext.data.XmlReader(
            {
                record : 'NodoMapa'
            }, [ 'franquiciaId', 'menuId', 'menuHeredado', 'numItems', 'periodicidadSincro', 'sincroDesdeNodoMapa', 'tituloPublicacionCA', 'tituloPublicacionES', 'tituloPublicacionEN', 'privado',
                    'enlaceDestino', 'contenidoRelacionado', 'activarPropuestasWeb', 'descripcionPropuestasWeb', 'mostrarReloj', 'mostrarMenuPerfilesPlegado', 'mostrarRecursosRelacionados', 'paginado']),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, [ 'success', 'msg' ]),
            buttons : [ this.saveButton ],
            items : [
                    {
                        xtype : 'hidden',
                        name : 'formGestionNodo',
                        value : true
                    },
                    {
                        xtype : 'fieldset',
                        flex : 1,
                        title : 'Franquícies',
                        layout : 'form',
                        margins : '5 10 5 10',
                        items : [ ref.comboFranquicias ]
                    },
                    {
                        xtype : 'fieldset',
                        flex : 1,
                        title : 'Menús',
                        layout : 'form',
                        margins : '5 10 5 10',
                        items : [ ref.checkMenus, ref.comboMenus ]
                    },
                    {
                        xtype : 'fieldset',
                        flex : 1,
                        title : 'Sincronització',
                        layout : 'form',
                        margins : '5 10 5 10',
                        items : [ ref.checkSincronizado, ref.numItemsField, ref.periodicidadSincroField, ref.desdeNodoMapaField ]
                    },
                    {
                        xtype : 'fieldset',
                        flex : 1,
                        title : 'Publicació',
                        layout : 'form',
                        margins : '5 10 5 10',
                        items : [ ref.tituloPublicacionCAField, ref.tituloPublicacionESField, ref.tituloPublicacionENField, ref.enlaceDestinoField, this.checkContenidoRelacionado,
                                ref.checkMostrarReloj, ref.checkMostrarMenuPerfilesPlegado, ref.checkMostrarRecursosRelacionados, ref.checkPaginado ]
                    },
                    {
                        xtype : 'fieldset',
                        flex : 1,
                        title : 'Privacitat',
                        layout : 'form',
                        margins : '5 10 5 10',
                        items : [ ref.checkPrivado ]
                    },
                    {
                        xtype : 'fieldset',
                        flex : 1,
                        title : 'Propostes Web',
                        layout : 'form',
                        margins : '5 10 5 10',
                        items : [ ref.checkPropuestasWeb, ref.descripcionPropuestasWeb ]
                    } ]
        });
    },

    buildSaveButton : function()
    {
        var gestionNodoForm = this;
        this.saveButton = new Ext.Button(
        {
            text : 'Desar',
            handler : function(button, event)
            {
                if (gestionNodoForm.form.getForm().isValid())
                {
                    gestionNodoForm.form.getForm().submit(
                    {
                        url : '/upo/rest/mapa/' + gestionNodoForm.mapaId,
                        method : 'put',
                        success : function(form, action)
                        {
                            gestionNodoForm.fireEvent('contenidoChange');
                        }
                    });
                }
                else
                {
                    Ext.Msg.alert('Informació', 'Aquesta configuració és incorrecta.');
                }
            }
        });
    },

    buildCheckSincronizado : function()
    {
        var ref = this;

        this.checkSincronizado = new Ext.form.Checkbox(
        {
            fieldLabel : 'Sincronitzat',
            inputValue : true,
            name : 'sincronizado',
            listeners :
            {
                check : function(check, newValue, oldValue)
                {
                    if (newValue && ref.administrador)
                    {
                        ref.numItemsField.enable();
                        ref.periodicidadSincroField.enable();
                        ref.desdeNodoMapaField.enable();
                    }
                    else
                    {
                        ref.numItemsField.disable();
                        ref.periodicidadSincroField.disable();
                        ref.desdeNodoMapaField.disable();
                    }
                }
            }
        });
    },

    buildCheckPrivado : function()
    {
        this.checkPrivado = new Ext.form.Checkbox(
        {
            fieldLabel : 'Privat',
            inputValue : true,
            name : 'privado'
        });
    },

    buildCheckMostrarReloj : function()
    {
        this.checkMostrarReloj = new Ext.form.Checkbox(
        {
            fieldLabel : 'Mostrar Rellotge',
            inputValue : true,
            name : 'mostrarReloj'
        });
    },

    buildCheckMostrarMenuPerfilesPlegado : function()
    {
        this.checkMostrarMenuPerfilesPlegado = new Ext.form.Checkbox(
        {
            fieldLabel : 'Mostrar menú perfils plegat',
            inputValue : true,
            name : 'mostrarMenuPerfilesPlegado'
        });
    },

    buildCheckMostrarRecursosRelacionados : function()
    {
        this.checkMostrarRecursosRelacionados = new Ext.form.Checkbox(
        {
            fieldLabel : 'Mostrar recursos relacionats',
            inputValue : true,
            name : 'mostrarRecursosRelacionados'
        });
    },

    buildCheckPaginado : function()
    {
        this.checkPaginado = new Ext.form.Checkbox(
        {
            fieldLabel : 'Paginat',
            inputValue : true,
            name : 'paginado'
        });
    },

    buildCheckPropuestasWeb : function()
    {
        this.checkPropuestasWeb = new Ext.form.Checkbox(
        {
            fieldLabel : 'Activar',
            inputValue : true,
            name : 'activarPropuestasWeb'
        });
    },

    buildDescripcionPropuestasWeb : function()
    {
        this.descripcionPropuestasWeb = new Ext.form.TextField(
        {
            fieldLabel : 'Descripció',
            name : 'descripcionPropuestasWeb',
            anchor : '50%'
        });
    },

    buildCheckContenidoRelacionado : function()
    {
        this.checkContenidoRelacionado = new Ext.form.Checkbox(
        {
            fieldLabel : 'Mostrar com contingut relacionat',
            inputValue : true,
            name : 'contenidoRelacionado'
        });
    },

    buildNumItemsField : function()
    {
        this.numItemsField = new Ext.form.NumberField(
        {
            fieldLabel : 'Núm. Ítems',
            name : 'numItems',
            allowDecimals : false,
            allowNegative : false,
            width : 30
        });
    },

    buildPeriodicidadSincroField : function()
    {
        this.periodicidadSincroField = new Ext.form.NumberField(
        {
            fieldLabel : 'Periodicitat (min.)',
            name : 'periodicidadSincro',
            allowDecimals : false,
            allowNegative : false,
            width : 30
        });
    },

    buildDesdeNodoMapaField : function()
    {
        this.desdeNodoMapaField = new Ext.form.TextField(
        {
            fieldLabel : 'Des de node',
            name : 'sincroDesdeNodoMapa',
            anchor : '50%'
        });
    },

    buildTituloPublicacionCAField : function()
    {
        this.tituloPublicacionCAField = new Ext.form.TextField(
        {
            fieldLabel : 'Títol Català',
            name : 'tituloPublicacionCA',
            anchor : '50%'
        });
    },

    buildTituloPublicacionESField : function()
    {
        this.tituloPublicacionESField = new Ext.form.TextField(
        {
            fieldLabel : 'Títol Espanyol',
            name : 'tituloPublicacionES',
            anchor : '50%'
        });
    },

    buildTituloPublicacionENField : function()
    {
        this.tituloPublicacionENField = new Ext.form.TextField(
        {
            fieldLabel : 'Títol Anglès',
            name : 'tituloPublicacionEN',
            anchor : '50%'
        });
    },

    buildEnlaceDestinoField : function()
    {
        this.enlaceDestinoField = new Ext.form.TextField(
        {
            fieldLabel : 'URL destí',
            name : 'enlaceDestino',
            anchor : '50%'
        });
    },

    buildCheckMenus : function()
    {
        var ref = this;

        this.checkMenus = new Ext.form.Checkbox(
        {
            fieldLabel : 'Heretat',
            name : 'menuHeredado',
            inputValue : true,
            listeners :
            {
                check : function(check, newValue, oldValue)
                {
                    (!newValue && ref.administrador) ? ref.comboMenus.enable() : ref.comboMenus.disable();
                }
            }
        });
    },

    buildStoreMenus : function()
    {
        var ref = this;

        this.storeMenus = new Ext.data.Store(
        {
            url : '/upo/rest/menu/todos',
            restful : true,
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Menu',
                id : 'id'
            }, [ 'id', 'nombre' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                'load' : function(store, records, option)
                {
                    ref.loadForm();
                }
            }
        });
    },

    loadForm : function()
    {
        var ref = this;

        ref.form.getForm().load(
        {
            url : '/upo/rest/mapa/' + ref.mapaId,
            method : 'GET',
            success : function(form, action)
            {
                Ext.util.Format.comboRenderer(ref.comboMenus);
                Ext.util.Format.comboRenderer(ref.comboFranquicias);

                var desdeNodoMapa = action.result.data['sincroDesdeNodoMapa'];
                var check = ref.checkSincronizado;

                if (desdeNodoMapa && ref.administrador)
                {
                    check.setValue(true);
                    ref.numItemsField.enable();
                    ref.periodicidadSincroField.enable();
                    ref.desdeNodoMapaField.enable();
                }
                else
                {
                    check.setValue(false);
                    ref.numItemsField.disable();
                    ref.periodicidadSincroField.disable();
                    ref.desdeNodoMapaField.disable();
                }
            }
        });
    },

    buildComboMenus : function()
    {
        var ref = this;

        this.comboMenus = new Ext.form.ComboBox(
        {
            valueField : 'id',
            name : 'menuId',
            displayField : 'nombre',
            hiddenName : 'menuId',
            fieldLabel : 'Menú',
            anchor : '50%',
            triggerAction : 'all',
            store : ref.storeMenus,
            allowBlank : false,
            forceSelection : true,
            mode : 'local'
        });
    },

    buildStoreFranquicias : function()
    {
        var ref = this;

        this.storeFranquicias = new Ext.data.Store(
        {
            url : '/upo/rest/franquicia',
            restful : true,
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Franquicia',
                id : 'id'
            }, [ 'id', 'nombre' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                'load' : function(store, records, option)
                {
                    ref.storeMenus.load();
                }
            }
        });
    },

    buildComboFranquicias : function()
    {
        var ref = this;
        this.comboFranquicias = new Ext.form.ComboBox(
        {
            valueField : 'id',
            name : 'franquiciaId',
            displayField : 'nombre',
            hiddenName : 'franquiciaId',
            fieldLabel : 'Franquícia',
            anchor : '50%',
            triggerAction : 'all',
            store : ref.storeFranquicias,
            allowBlank : false,
            forceSelection : true,
            mode : 'local'
        });
    }
});