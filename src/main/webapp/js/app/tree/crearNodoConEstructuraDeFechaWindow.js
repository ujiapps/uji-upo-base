Ext.ns('UJI.UPO');

UJI.UPO.CrearNodoConEstructuraDeFechaWindow = Ext.extend(Ext.Window,
{
    title : 'Nou node amb estructura de data',
    layout : 'form',
    width : 250,
    height : 150,
    labelWidth : 60,
    modal : true,
    padding : 10,
    mapa : {},
    mapaId : '',
    addButton : {},
    cancelButton : {},
    fbar : [],
    listeners :
    {
        afterrender : function(window)
        {
            window.anyo.focus(false, 500);
        }
    },

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.CrearNodoConEstructuraDeFechaWindow.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildAddButton();
        this.buildCancelButton();

        this.buildAnyo();
        this.buildMes();

        this.add(this.anyo);
        this.add(this.mes);

        this.getFooterToolbar().addButton(this.addButton);
        this.getFooterToolbar().addButton(this.cancelButton);
    },

    buildAnyo : function()
    {
        this.anyo = new Ext.form.NumberField(
        {
            fieldLabel : 'Any',
            allowBlank : false,
            anchor : '60%',
            allorDecimals : false,
            maxLength : 4,
            minLength : 4,
            minValue : 2000,
            maxValue : 2100,
            value : new Date().getFullYear()
        });
    },

    buildMes : function()
    {
        this.mes = new Ext.form.ComboBox(
        {
            fieldLabel : 'Mes',
            anchor : '60%',
            mode : 'local',
            triggerAction : 'all',
            displayField : 'value',
            forceSelection : true,
            valueField : 'value',
            store : new Ext.data.JsonStore(
            {
                fields : [ 'name', 'value' ],
                data : [
                {
                    value : ''
                },
                {
                    value : '01'
                },
                {
                    value : '02'
                },
                {
                    value : '03'
                },
                {
                    value : '04'
                },
                {
                    value : '05'
                },
                {
                    value : '06'
                },
                {
                    value : '07'
                },
                {
                    value : '08'
                },
                {
                    value : '09'
                },
                {
                    value : '10'
                },
                {
                    value : '11'
                },
                {
                    value : '12'
                } ]
            })
        });
    },

    addNewFolder : function(anyo, mes)
    {
        var ref = this;

        if (ref.anyo.isValid() && ref.mes.isValid())
        {
            if (UJI.UPO.Application.comprobarSiNodoTieneHijoConMismoNombre(this.mapa, anyo))
            {
                Ext.MessageBox.show(
                {
                    title : 'Error',
                    msg : 'Operació no permesa, ja hi ha un node amb el mateix nom.',
                    buttons : Ext.MessageBox.OK,
                    icon : Ext.MessageBox.ERROR
                });

                return false;
            }
            else
            {
                Ext.Ajax.request(
                {
                    url : '/upo/rest/mapa/' + ref.mapaId + '/estructurafecha',
                    params :
                    {
                        anyo : anyo,
                        mes : mes
                    },
                    success : function()
                    {
                        UJI.Bus.publish("UJI.UPO.Tree.ContentNodeChanged");
                        return true;
                    },
                    failure : function(result, request)
                    {
                        Ext.Msg.alert('Error', "S'ha produït una errada");
                    },
                    method : 'POST'
                });

                return true;
            }
        }
        else
        {
            Ext.MessageBox.show(
            {
                title : 'Error',
                msg : 'Hi ha alguna errada en els camps del formulari.',
                buttons : Ext.MessageBox.OK,
                icon : Ext.MessageBox.ERROR
            });
            return false;
        }
    },

    buildAddButton : function()
    {
        var ref = this;
        this.addButton = new Ext.Button(
        {
            text : 'Crear',
            handler : function()
            {
                if (ref.addNewFolder(ref.anyo.getValue(), ref.mes.getValue()))
                {
                    ref.close();
                }
            }
        });
    },

    buildCancelButton : function()
    {
        ref = this;

        this.cancelButton = new Ext.Button(
        {
            text : 'Cancel·lar',
            handler : function()
            {
                ref.close();
            }
        });
    }
});