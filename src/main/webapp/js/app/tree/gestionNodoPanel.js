Ext.ns('UJI.UPO');

UJI.UPO.GestionNodoPanel = Ext.extend(Ext.Panel,
{
    title : 'Configuració',
    layout : 'card',
    frame : true,
    activeItem : 0,
    nodoPanelForm : {},
    setAdmin : function(administrador)
    {
        this.nodoPanelForm.setAdmin(administrador);
    },
    initComponent : function()
    {
        var config =
        {
            mapaId : this.mapaId
        };

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.GestionNodoPanel.superclass.initComponent.call(this);

        this.initUI();

        this.add(this.nodoPanelForm);

    },

    initUI : function()
    {
        this.buildForm();
    },

    buildForm : function()
    {
        this.nodoPanelForm = new UJI.UPO.GestionNodoForm(
        {
            mapaId : this.mapaId
        });
    }
});