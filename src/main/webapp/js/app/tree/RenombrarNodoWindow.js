Ext.ns('UJI.UPO');

UJI.UPO.RenombrarNodoWindow = Ext.extend(Ext.Window,
{
    title : '',
    layout : 'form',
    width : 500,
    height : 125,
    modal : true,
    padding : 10,
    mapa : '',
    mapaId : '',
    mapaName : '',
    mapaUrlCompleta : '',
    requestButton : {},
    cancelButton : {},
    fbar : [],
    nombreNodoField : {},
    listeners :
    {
        afterrender : function(window)
        {
            window.nombreNodoField.focus(false, 500);
        }
    },

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.CrearNodoWindow.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildRequestButton();
        this.buildCancelButton();

        this.buildNombreNodoField();

        this.add(this.nombreNodoField);

        this.getFooterToolbar().addButton(this.requestButton);
        this.getFooterToolbar().addButton(this.cancelButton);
    },

    buildNombreNodoField : function()
    {
        var ref = this;

        this.nombreNodoField = new Ext.form.TextField(
        {
            fieldLabel : 'Nom del node',
            allowBlank : false,
            maskRe : /[a-zA-Z0-9_.\-]/,
            regex : /^[a-zA-Z0-9_.\-]+$/i,
            width : '90%',
            value : ref.mapaName,
            selectOnFocus : true
        });
    },

    addRequest : function(nombreNodo)
    {
        var ref = this;

        if (nombreNodo)
        {
            return (Ext.Ajax.request(
            {
                url : '/upo/rest/mapa/' + ref.mapaId,
                params :
                {
                    mapaId : ref.mapaId,
                    nombreNodo : nombreNodo
                },
                success : function()
                {
                    var posReemplazar = ref.mapaUrlCompleta.lastIndexOf('/' + ref.mapaName + '/') + 1;
                    var urlBase = ref.mapaUrlCompleta.substring(0, posReemplazar);
                    var nuevaUrlCompleta = urlBase + nombreNodo + '/';

                    UJI.Bus.publish("UJI.UPO.Tree.NodeRename",
                    {
                        urlCompletaAnterior : ref.mapaUrlCompleta,
                        urlCompletaNueva : nuevaUrlCompleta,
                        nombreNodoNuevo : nombreNodo
                    });

                    UJI.UPO.Application.cambiarUrlCompletaNodeTab(ref.mapaUrlCompleta, nuevaUrlCompleta);

                    return true;
                },
                failure : function(result, request)
                {
                    Ext.Msg.alert('Error', "S'ha produït una errada");
                    return false;
                },
                method : 'PUT'
            }));
        }
        else
        {
            Ext.Msg.alert('Error', "El camp Nom del Node és obligatori");
            return false;
        }
    },

    buildRequestButton : function()
    {
        var ref = this;
        this.requestButton = new Ext.Button(
        {
            text : 'Renombrar node',
            handler : function()
            {
                if (ref.nombreNodoField.isValid() && ref.nombreNodoField.getValue())
                {
                    if (UJI.UPO.Application.comprobarSiNodoTieneHijoConMismoNombre(ref.mapa.parentNode, ref.nombreNodoField.getValue()))
                    {
                        Ext.MessageBox.show(
                        {
                            title : 'Error',
                            msg : 'Operació no permesa, ja hi ha un node amb el mateix nom.',
                            buttons : Ext.MessageBox.OK,
                            icon : Ext.MessageBox.ERROR
                        });

                        return false;
                    }
                    else
                    {
                        Ext.Msg.confirm('Confirmar', 'Esteu segur/a de voler renombrar el node <b>' + ref.mapaName + '</b> a <b>' + ref.nombreNodoField.getValue() + '</b>?', function(btn, text)
                        {
                            if (btn == 'yes')
                            {
                                if (ref.addRequest(ref.nombreNodoField.getValue()))
                                {
                                    ref.close();
                                }
                            }
                        });
                    }
                }
            }
        });
    },

    buildCancelButton : function()
    {
        ref = this;

        this.cancelButton = new Ext.Button(
        {
            text : 'Cance·lar',
            handler : function()
            {
                ref.close();
            }
        });
    }
});