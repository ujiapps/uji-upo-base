Ext.ns('UJI.UPO');

UJI.UPO.PlantillasNodoMapa = Ext.extend(Ext.Panel,
{
    title : 'Shuttle',
    layout : 'vbox',
    frame : true,
    layoutConfig :
    {
        align : 'stretch'
    },
    flex : 1,
    administrador : false,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.PlantillasNodoMapa.superclass.initComponent.call(this);

        Ext.util.Format.comboRenderer = function(combo)
        {
            return function(value)
            {
                var record = combo.findRecord(combo.valueField, value);
                return record ? record.get(combo.displayField) : combo.valueNotFoundText;
            };
        };

        this.initUI();

        this.add(this.grid);
    },

    initUI : function()
    {
        this.buildStore();
        this.buildStoreNoAsignadas();

        this.buildCombo();

        this.buildEditor();
        this.buildBotonInsertar();
        this.buildBotonBorrar();

        this.buildGrid();
    },

    buildGrid : function()
    {
        var ref = this;

        var botonInsertar = this.botonInsertar;
        var botonBorrar = this.botonBorrar;

        this.grid = new Ext.grid.GridPanel(
        {
            flex : 1,
            viewConfig :
            {
                forceFit : true
            },
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : this.store,
            plugins : [ this.editor ],
            colModel : new Ext.grid.ColumnModel(
            {
                columns : [
                {
                    header : 'Nom',
                    dataIndex : 'id',
                    allowBlank : false,
                    width : 600,
                    editor : ref.combo,
                    renderer : function(value, metaData, record, rowIndex, colIndex, store)
                    {
                        return record.data.tipo + ' - ' + record.data.nombre;
                    }
                },
                {
                    header : 'nivell',
                    dataIndex : 'nivel',
                    allowBlank : false,
                    width : 20,
                    editor : new Ext.form.NumberField(
                    {
                        allowBlank : false,
                        allowDecimals : false,
                        blankText : 'El camp "Nivell" es requerit',
                        value : 1
                    })
                } ]
            }),
            tbar :
            {
                items : [ botonInsertar, '-', botonBorrar ]
            }
        });
    },

    buildStore : function()
    {
        var ref = this;

        this.store = new Ext.data.Store(
        {
            url : 'rest/mapa/' + ref.mapaId + '/plantilla',
            autoLoad : true,
            restful : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Plantilla',
                idProperty : 'id'
            }, [ 'id', 'tipo', 'nombre', 'nivel' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                exception : function(excp, type, action, options, response, arg)
                {
                    var status = response.status;

                    if (action == 'create' && (status == '500' || status == '404'))
                    {
                        ref.store.load();
                    }
                }
            }
        });
    },

    buildStoreNoAsignadas : function()
    {
        var ref = this;

        this.storeNoAsignadas = new Ext.data.Store(
        {
            url : 'rest/mapa/' + ref.mapaId + '/plantilla?noAsignadas=1',
            autoLoad : true,
            restful : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Plantilla',
                idProperty : 'id'
            }, [ 'id', 'tipo', 'nombre', 'nivel' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildCombo : function()
    {
        var ref = this;

        this.combo = new Ext.form.ComboBox(
        {
            valueField : 'id',
            displayField : 'nombre',
            tpl : '<tpl for="."><div class="x-combo-list-item">{tipo} - {nombre}</div></tpl>',
            triggerAction : 'all',
            store : ref.storeNoAsignadas,
            forceSelection : true,
            allowBlank : false
        });
    },

    buildEditor : function()
    {
        var ref = this;

        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancel·lar',
            errorSummary : false,
            listeners :
            {
                beforeedit : function(rowEditor, rowIndex)
                {
                    var update = (ref.grid.getStore().getAt(rowIndex).get('id'));

                    if (update)
                    {
                        ref.grid.getColumnModel().columns[0].editor.setDisabled(true);
                    }
                    else
                    {
                        ref.grid.getColumnModel().columns[0].editor.setDisabled(false);
                    }
                }
            }
        });
    },

    buildBotonInsertar : function()
    {
        var ref = this;

        this.botonInsertar = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function(btn, event)
            {
                var rec = new ref.store.recordType(
                {
                    nombre : '',
                    nivel : 1
                });

                ref.editor.stopEditing();
                ref.store.insert(0, rec);
                ref.editor.startEditing(0);
            }
        });
    },

    buildBotonBorrar : function()
    {
        var ref = this;
        this.botonBorrar = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(btn, event)
            {
                var rec = ref.grid.getSelectionModel().getSelected();
                if (!rec)
                {
                    Ext.Msg.alert('Información', 'És necessari seleccionar una fila per esborrar');
                    return false;
                }
                else
                {
                    Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                    {
                        if (btn == 'yes')
                        {
                            ref.store.remove(rec);
                        }
                    });
                }
            }
        });
    },

    setAdmin : function(administrador)
    {
        this.administrador = administrador;

        if (!administrador)
        {
            this.grid.disable();
        }
    },

    isAdministrador : function()
    {
        return this.administrador;
    }
});