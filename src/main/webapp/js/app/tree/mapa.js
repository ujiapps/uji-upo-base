Ext.ns('UJI.UPO');

UJI.UPO.Mapa = Ext.extend(Ext.tree.TreePanel,
{
    title : 'Portal Universitat Jaume I',
    margins : '2 2 0 2',
    autoScroll : true,
    containerScroll : true,
    split : true,
    frame : false,
    region : 'west',
    enableDD : true,
    ddGroup : 'TreeNodeMapa',
    tabPanel : {},
    rootVisible : false,
    visibilidadOption : {},
    afegirNodoOption : {},
    afegirEstructuraDeData : {},
    afegirEstructuraRevista : {},
    renombrarNodoOption : {},
    eliminarNodoOption : {},
    duplicarNodoOption : {},
    //migracionContenidosOption : {},
    contentPreviewOption : {},
    afegirEstructura : {},
    tbar : [],
    bbar : [],
    administrador : '',
    administradorNodoPadre : '',
    mapa : '',
    mapaId : '',
    mapaName : '',
    url : '',

    disableOptions : function()
    {
        this.renombrarNodoOption.disable();
        this.eliminarNodoOption.disable();
        this.duplicarNodoOption.disable();
        this.afegirNodoOption.disable();
        this.afegirEstructura.disable();
        this.visibilidadOption.disable();
        //this.migracionContenidosOption.disable();
    },

    setAdmin : function(administrador)
    {
        this.administrador = administrador;

        this.disableOptions();

        if (this.mapaId && this.mapaId < 0)
        {
            return;
        }

        if (this.administrador)
        {
            this.afegirNodoOption.enable();
            this.afegirEstructura.enable();
            this.visibilidadOption.enable();
            //this.migracionContenidosOption.enable();
        }

        if (this.administradorNodoPadre)
        {
            this.duplicarNodoOption.enable();

            if (this.mapaId && this.mapaId > 0)
            {
                this.renombrarNodoOption.enable();
                this.eliminarNodoOption.enable();
            }
        }
    },

    setValues : function(node)
    {
        administrador = (node.attributes.administrador == 'S');
        this.administradorNodoPadre = (node.parentNode && node.parentNode.attributes.administrador == 'S')

        this.mapa = node;
        this.mapaId = node.attributes.id;
        this.mapaName = node.attributes.title;
        this.setAdmin(administrador);
        this.mapaUrlCompleta = node.attributes.urlCompleta;

        this.searchNodeField.setValue(node.attributes.urlCompleta);
    },

    listeners : {
        contextmenu : function(node, e)
        {
            node.select();

            this.setValues(node);

            var c = node.getOwnerTree().menu;
            c.contextNode = node;
            c.showAt(e.getXY());
        },

        click : function(node, e)
        {
            var result = UJI.UPO.Application.createNewNodeTab(this, node, node.id, node.attributes.urlCompleta);
            this.tabPanel.addTab(result.nodoTab);

            this.setValues(node);
        }
    },

    dropConfig : {
        ddGroup : 'TreeNodeMapa',
        expandDelay : 300,
        onNodeOver : function(target, dd, e, data)
        {
            var point = this.getDropPoint(e, target, dd);
            var elementoDrag = this.tree.getElementoDrag(dd, data);
            var nodoDestino = target.node;
            var adminDestino = (nodoDestino.attributes.administrador == 'S');

            if (elementoDrag != 'treeNodoMapa' && point != UJI.UPO.TipoPointsArrastrarNodoArbol.APPEND)
            {
                return false;
            }

            if (elementoDrag == 'treeNodoMapa')
            {
                var nodoOrigen = data.node;
                var adminPadreDestino = (nodoDestino.parentNode.attributes.administrador == 'S');
                var urlCompletaNodoDestino = nodoDestino.attributes.urlCompleta;
                var urlCompletaNodoOrigen = nodoOrigen.attributes.urlCompleta;
                var nombreNodoOrigen = nodoOrigen.attributes.title;
                var mismoNombre = UJI.UPO.Application.comprobarSiNodoTieneHijoConMismoNombre(nodoDestino, nombreNodoOrigen);
                var origenFormaParteDeDestino = (urlCompletaNodoDestino.indexOf(urlCompletaNodoOrigen) == 0);
                var mismoPadreOrigenQueDestino = (nodoDestino.parentNode.id == nodoOrigen.parentNode.id);

                if (point != 'append' && (!mismoPadreOrigenQueDestino))
                {
                    return false;
                }

                if (point != 'append' && !adminPadreDestino)
                {
                    return false;
                }

                if (mismoNombre)
                {
                    return false;
                }

                if (origenFormaParteDeDestino)
                {
                    return false;
                }
            }

            if (elementoDrag == 'migracionUrl' && !adminDestino)
            {
                return false;
            }

            if (elementoDrag == 'recursoContenidoGrid' && !adminDestino)
            {
                return false;
            }

            return Ext.tree.TreeDropZone.prototype.onNodeOver.call(this, target, dd, e, data);
        },

        onNodeDrop : function(target, dd, e, data)
        {
            var elementoDrag = this.tree.getElementoDrag(dd, data);
            var point = this.getDropPoint(e, target, dd);

            this.tree.dropFromTreeNode(target, dd, e, data, elementoDrag, point);
            this.tree.dropFromGridMigracion(target, data, elementoDrag);
            this.tree.dropFromContenidoGrid(target, data, elementoDrag);
            this.tree.dropFromRecursoContenidoGrid(target, dd, elementoDrag);
        }
    },

    dropFromTreeNode : function(target, dd, e, data, elementoDrag, point)
    {
        if (elementoDrag == 'treeNodoMapa')
        {
            if (point != UJI.UPO.TipoPointsArrastrarNodoArbol.APPEND)
            {
                return AccionesNodoMapa.reordenarNodo(target, data, point);
            }

            var nodoDestino = target.node;
            var nodoOrigen = data.node;

            var adminDestino = (nodoDestino.attributes.administrador == 'S');
            var adminOrigen = (nodoOrigen.attributes.administrador == 'S');
            var nombreNodoOrigen = nodoOrigen.attributes.title;
            var urlCompletaNodoDestino = nodoDestino.attributes.urlCompleta;
            var urlCompletaNodoOrigen = nodoOrigen.attributes.urlCompleta;
            var mismoNombre = UJI.UPO.Application.comprobarSiNodoTieneHijoConMismoNombre(nodoDestino, nombreNodoOrigen);

            if (mismoNombre)
            {
                this.showErrorMessage('Operació no permesa, ja hi ha un node amb el mateix nom.');
                return false;
            }

            if (urlCompletaNodoDestino.indexOf(urlCompletaNodoOrigen) == 0)
            {
                this.showErrorMessage('Operació no permesa, no es pot arrastrar dins del propi node');
                return false;
            }

            if (adminDestino && adminOrigen)
            {
                var window = new UJI.UPO.AccionesDragAndDropNodoWindow(
                {
                    urlCompletaNodoOrigen : urlCompletaNodoOrigen,
                    urlCompletaNodoDestino : urlCompletaNodoDestino
                });

                window.show();

                window.on("arrastrar", function()
                {
                    return AccionesNodoMapa.arrastrarNodo(target, data);
                });

                window.on("proponer", function(params)
                {
                    return AccionesNodoMapa.proponerNodo(target, data, params.incluirHijos);
                });

                window.on("duplicarEstructura", function()
                {
                    return AccionesNodoMapa.duplicarNodo(target, data, false);
                });

                window.on("duplicarTodo", function()
                {
                    return AccionesNodoMapa.duplicarNodo(target, data, true);
                });
            }
            else if (adminDestino && !adminOrigen)
            {
                var window = new UJI.UPO.AccionesDragAndDropNodoWindow(
                {
                    urlCompletaNodoOrigen : urlCompletaNodoOrigen,
                    urlCompletaNodoDestino : urlCompletaNodoDestino,
                    arrastrarOption : false
                });

                window.show();

                window.on("proponer", function(params)
                {
                    return AccionesNodoMapa.proponerNodo(target, data, params.incluirHijos);
                });

                window.on("duplicarEstructura", function()
                {
                    return AccionesNodoMapa.duplicarNodo(target, data, false);
                });

                window.on("duplicarTodo", function()
                {
                    return AccionesNodoMapa.duplicarNodo(target, data, true);
                });
            }
            else
            {
                var window = new UJI.UPO.AccionesDragAndDropNodoWindow(
                {
                    urlCompletaNodoOrigen : urlCompletaNodoOrigen,
                    urlCompletaNodoDestino : urlCompletaNodoDestino,
                    arrastrarOption : false,
                    duplicarEstructuraOption : false,
                    deuplicarEstructuraYContenidosOption : false
                });

                window.show();

                window.on("proponer", function(params)
                {
                    return AccionesNodoMapa.proponerNodo(target, data, params.incluirHijos);
                });
            }
        }
    },

    showErrorMessage : function(msgText)
    {
        Ext.MessageBox.show(
        {
            title : 'Error',
            msg : msgText,
            buttons : Ext.MessageBox.OK,
            icon : Ext.MessageBox.ERROR
        });
    },

    dropFromRecursoContenidoGrid : function(target, dd, elementoDrag)
    {
        if (elementoDrag == 'recursoContenidoGrid')
        {
            var urlCompleta = target.node.attributes.urlCompleta;
            var admin = (target.node.attributes.administrador == 'S');

            if (!admin)
            {
                this.showErrorMessage('Operació no permesa, has de ser administrador de node.');
                return false;
            }

            Ext.Msg.show(
            {
                title : 'Confirmació',
                msg : 'Voleu crear o enllaçar un contingut des del recurs <b>' + dd.recursoNombre + '</b>?',
                buttons : {
                    ok : "Enllaçar",
                    yes : "Crear",
                    cancel : "Cancel·lar"
                },
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn, text)
                {
                    if (btn == 'yes' || btn == 'ok')
                    {
                        Ext.Ajax.request(
                        {
                            url : '/upo/rest/mapa/' + target.node.id + '/recurso',
                            method : 'POST',
                            params : {
                                url : dd.recursoUrl,
                                contenidoId : dd.contenidoId,
                                idioma : dd.idioma,
                                tipo : (btn == 'ok') ? 'link' : ''
                            },
                            success : function()
                            {
                                UJI.Bus.publish('UJI.UPO.ContenidoGrid.RecursoContenido',
                                {
                                    'urlCompleta' : urlCompleta
                                });
                            }
                        });
                    }
                }
            });

            return true;
        }
    },

    dropFromContenidoGrid : function(target, data, elementoDrag)
    {
        if (elementoDrag == 'contenidoGrid')
        {
            var urlCompleta = target.node.attributes.urlCompleta;

            var nodeId = target.node.id;
            var contenidoId = data.grid.getSelectionModel().getSelected().id;
            var nodoMapaContenidoId = data.grid.getSelectionModel().getSelected().data.nodoMapaContenidoId;
            var nodoOrigenId = data.grid.getSelectionModel().getSelected().data.nodoMapaId;

            var mensaje = 'Esteu segur/a de voler proposar el contingut al node: <b> ' + urlCompleta + ' </b> ?';

            Ext.Msg.confirm('Confirmació', mensaje, function(btn, text)
            {
                if (btn == 'yes')
                {
                    Ext.Ajax.request(
                    {
                        url : '/upo/rest/mapa/' + nodeId + '/contenido',
                        method : 'POST',
                        params : {
                            contenidoId : contenidoId,
                            nodoMapaContenidoId : nodoMapaContenidoId,
                            nodoMapaOrigenId : nodoOrigenId
                        },
                        success : function()
                        {
                            UJI.Bus.publish('UJI.UPO.NodoMapa.PropuestaContenido',
                            {
                                'urlCompleta' : urlCompleta
                            });
                        }
                    });

                    return true;
                }
            });
        }
    },

    dropFromGridMigracion : function(target, data, elementoDrag)
    {
        if (elementoDrag == 'migracionUrl')
        {
            var rowOrigen = data.grid.getSelectionModel().getSelected();
            var urlOrigen = rowOrigen.get("url");
            var tipoOrigen = rowOrigen.get("tipo");
            var urlCompleta = target.node.attributes.urlCompleta;
            var admin = (target.node.attributes.administrador == 'S');
            var mensaje = 'Esteu segur/a de voler migrar el contingut <b>' + urlOrigen + '</b> al node: <b> ' + urlCompleta + ' </b> ?';

            if (!admin)
            {
                this.showErrorMessage('Operació no permesa, has de ser administrador de node.');
                return false;
            }

            Ext.Msg.confirm('Confirmació', mensaje, function(btn, text)
            {
                if (btn == 'yes')
                {
                    if (tipoOrigen == UJI.UPO.TipoItemMigracion.BINARIO)
                    {
                        Ext.Ajax.request(
                        {
                            url : '/upo/rest/migracion/url/binario',
                            method: 'GET',
                            params : {
                                url : urlOrigen,
                                urlNode : urlCompleta
                            },
                            success : function(response)
                            {
                                UJI.Bus.publish('UJI.UPO.MigracionUrlGrid.Drop.Reload',
                                {
                                    mapaId : target.node.attributes.id
                                });

                                data.grid.store.reload();

                                var result = UJI.UPO.Application.createNewNodeTab(this, target.node, target.node.attributes.id, target.node.attributes.urlCompleta);
                                var tabPanel = UJI.UPO.Application.getTabPanel();

                                tabPanel.addTab(result.nodoTab);
                            }
                        });
                    }

                    if (tipoOrigen == UJI.UPO.TipoItemMigracion.TEXTO)
                    {
                        var result = UJI.UPO.Application.createNewNodeTab(this, target.node, target.node.attributes.id, target.node.attributes.urlCompleta);
                        var tabPanel = UJI.UPO.Application.getTabPanel();

                        tabPanel.addTab(result.nodoTab);

                        UJI.Bus.publish('UJI.UPO.MigracionUrlGrid.Drop',
                        {
                            mapaId : target.node.attributes.id,
                            mapaUrl : urlCompleta,
                            url : urlOrigen,
                            migracionGrid : data.grid
                        });
                    }
                }
            });
        }
    },

    initComponent : function()
    {
        this.root = new Ext.tree.AsyncTreeNode(
        {
            id : 'root',
            urlPath : 'root',
            text : '',
            draggable : false,
            allowDrop : false,
            iconCls : 'folder',
            expanded : true
        });

        this.loader = new Ext.ux.tree.XmlTreeLoader(
        {
            dataUrl : '/upo/rest/mapa',
            listeners : {
                'load' : function(treeLoader, node, response)
                {
                    node.eachChild(function(childNode)
                    {
                        if (childNode.attributes.administrador == 'S')
                        {
                            childNode.setIconCls("folder-add");
                        }
                        else
                        {
                            childNode.setIconCls("folder");
                        }

                        childNode.elementoDrag = 'treeNodoMapa';
                    });
                }
            }
        });

        var config =
            {
                collapsible : true,
                width: 280
            };

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.Mapa.superclass.initComponent.call(this);

        UJI.Bus.subscribe("UJI.UPO.Tree.ContentNodeChanged", this);
        UJI.Bus.subscribe("UJI.UPO.Tree.NodeRename", this);
        UJI.Bus.subscribe("UJI.UPO.Tree.NodeDragAndDrop", this);
        UJI.Bus.subscribe("UJI.UPO.Tree.ProposeNode", this);
        UJI.Bus.subscribe("UJI.UPO.Tree.DuplicateNode", this);
        UJI.Bus.subscribe("UJI.UPO.Tree.ReorderNode", this);
        UJI.Bus.subscribe("UJI.UPO.Tree.NodeDelete", this);

        UJI.UPO.Application.registerTreePanel(this);

        this.buildMenu();
        this.buildEstructuraMenuButton();
        this.buildSearchNodeField();

        this.getTopToolbar().addButton(this.estructuraMenu);
        this.getTopToolbar().add(this.searchNodeField);

        this.self = this;
    },

    onMessage : function(message, params)
    {
        if (message == 'UJI.UPO.Tree.ContentNodeChanged')
        {
            var selectedNode = this.getSelectionModel().getSelectedNode();

            if (selectedNode)
            {
                selectedNode.removeAll();
                this.getLoader().load(selectedNode, function()
                {
                    selectedNode.expand();
                });
            }
        }

        if (message == 'UJI.UPO.Tree.NodeRename' || message == 'UJI.UPO.Tree.NodeDelete')
        {
            var selectedNode = this.getSelectionModel().getSelectedNode();

            if (selectedNode)
            {
                var parentNode = selectedNode.parentNode;
                selectedNode.remove();
                this.getLoader().load(parentNode, function()
                {
                    parentNode.expand();
                });
            }
        }

        if (message == 'UJI.UPO.Tree.NodeDragAndDrop')
        {
            var selectedNode = this.getSelectionModel().getSelectedNode();
            var nodoDestino = params.nodoDestino;

            if (selectedNode && nodoDestino)
            {
                selectedNode.remove();
                this.getLoader().load(nodoDestino, function()
                {
                    nodoDestino.expand();
                });
            }
        }

        if (message == 'UJI.UPO.Tree.ProposeNode' || message == 'UJI.UPO.Tree.DuplicateNode' || message == 'UJI.UPO.Tree.ReorderNode')
        {
            var selectedNode = this.getSelectionModel().getSelectedNode();
            var nodoDestino = params.nodoDestino;

            if (selectedNode && nodoDestino)
            {
                this.getLoader().load(nodoDestino, function()
                {
                    nodoDestino.expand();
                });
            }
        }
    },

    buildSearchNodeField : function()
    {
        var ref = this;

        this.searchNodeField = new Ext.form.TextField(
        {
            width : '193',
            enableKeyEvents : true,
            value : ref.url,
            listeners : {
                specialkey : function(field, e)
                {
                    if (e.getKey() == e.ENTER)
                    {
                        ref.goToPath(field);
                    }
                },

                afterrender : function(field)
                {
                    ref.goToPath(field);
                }
            }
        });

    },

    goToPath : function(field)
    {
        var ref = this;

        var path = field.getValue();

        ref.selectPath("/root/first" + path, "urlPath", ref.selectNode);

        if (path.indexOf('/', path.length - 1) !== -1)
        {
            path = path.substr(0, path.length - 1);

            ref.selectPath("/root/first" + path, "urlPath", ref.selectNode);
        }

        ref.focus();
    },

    selectNode : function(success, node)
    {
        if (success)
        {
            node.fireEvent('click', node);
        }
    },

    buildMenu : function()
    {
        var ref = this;

        ref.afegirNodoOption = new Ext.menu.Item(
        {
            text : 'Afegir node',
            disabled : true,
            handler : function(e)
            {
                Ext.Ajax.request(
                {
                    url : '/upo/rest/mapa/' + ref.mapaId,
                    success : function(response)
                    {
                        var doc = response.responseXML;
                        doc = doc.documentElement || doc;

                        var franquiciaId = Ext.DomQuery.selectValue("franquiciaId", doc);
                        var plantillaId = Ext.DomQuery.selectValue("plantillaId", doc);

                        var window = new UJI.UPO.CrearNodoWindow(
                        {
                            franquiciaId : franquiciaId,
                            plantillaId : plantillaId,
                            mapa : ref.mapa,
                            mapaId : ref.mapaId
                        });

                        window.show();
                    }
                });
            }
        });

        ref.renombrarNodoOption = new Ext.menu.Item(
        {
            text : 'Renombrar node',
            disabled : true,
            handler : function(e)
            {
                var window = new UJI.UPO.RenombrarNodoWindow(
                {
                    mapa : ref.mapa,
                    mapaId : ref.mapaId,
                    mapaName : ref.mapaName,
                    mapaUrlCompleta : ref.mapaUrlCompleta,
                    title : 'Renombrar node: ' + ref.mapaName
                });

                window.show();
            }
        });

        ref.eliminarNodoOption = new Ext.menu.Item(
        {
            text : 'Eliminar node',
            disabled : true,
            handler : function(e)
            {
                var msg = 'Els nodes de la paperera, els seus descendents i tots els seus continguts, seran completament esborrats passats uns dies. Mentre, podeu recuperar el contingut arrastrant fora de la paperera.<br/><br/>'
                + 'Segur que voleu arrastrar el node <b>' + ref.mapaUrlCompleta + '</b> a la paperera?';

                if (ref.mapaUrlCompleta.indexOf('/paperera/') === 0)
                {
                    msg = 'L\'esborrat d\'un node de la paperera comporta l\'eliminació de tots els seus nodes fills, dels objectes associats i dels enllaços a ells mateix.<br/><br/>'
                    + 'Segur que voleu esborrar el node <b>' + ref.mapaUrlCompleta + '</b>?';
                }

                Ext.Msg.confirm('Esborrat', msg, function(btn, text)
                {
                    if (btn == 'yes')
                    {
                        Ext.Ajax.request(
                        {
                            url : '/upo/rest/mapa/' + ref.mapaId,
                            params : {
                                mapaId : ref.mapaId,
                                nombreNodo : ref.nombreNodo
                            },
                            success : function()
                            {
                                UJI.Bus.publish("UJI.UPO.Tree.NodeDelete",
                                {
                                    mapaUrlCompleta : ref.mapaUrlCompleta
                                });

                                UJI.UPO.Application.deleteNodeTabsWithUrlCompletaBeginsByUrlBase(ref.mapaUrlCompleta);

                                return true;
                            },
                            failure : function(result, request)
                            {
                                Ext.Msg.alert('Error', "S'ha produït una errada");
                                return false;
                            },
                            method : 'DELETE'
                        });
                    }
                });
            }
        });

        ref.duplicarNodoOption = new Ext.menu.Item(
        {
            text : 'Duplicar node',
            disabled : true,
            handler : function(e)
            {
                var window = new UJI.UPO.DuplicarNodoWindow(
                {
                    mapa : ref.mapa,
                    mapaId : ref.mapaId,
                    mapaName : ref.mapaName,
                    mapaUrlCompleta : ref.mapaUrlCompleta,
                    title : 'Duplicar node: ' + ref.mapaName
                });

                window.show();
            }
        });

        ref.visibilidadOption = new Ext.menu.Item(
        {
            text : 'Visibilitat',
            disabled : true,
            handler : function(e)
            {
                var window = new UJI.UPO.VisibilidadContenidoConCheckWindow({});

                window.setAdmin(ref.administrador);
                window.show();

                window.on("setVisibility", function(params)
                {
                    Ext.Ajax.request(
                    {
                        url : '/upo/rest/mapa/' + ref.mapaId + '/visibilidad',
                        params : {
                            visible : params.visible,
                            textoNoVisible : params.textoNoVisible,
                            extenderAHijos : (params.extenderAHijos ? "S" : "N")
                        },
                        success : function()
                        {
                            UJI.Bus.publish("UJI.UPO.Contenido.VisibilityUpdate");
                            window.close();
                        },
                        failure : function(result, request)
                        {
                            Ext.Msg.alert('Error', "S'ha produït una errada");
                        },
                        method : 'PUT'
                    });

                });
            }
        });

        /*ref.migracionContenidosOption = new Ext.menu.Item(
        {
            text : 'Migració continguts',
            disabled : true,
            handler : function(e)
            {
                var window = new UJI.UPO.MigrarContenidosWindow(
                {
                    mapaId : ref.mapaId
                });

                window.show();
            }
        });*/

        ref.afegirEstructura = new Ext.menu.Item(
        {
            text : 'Afegir estructura',
            disabled : true,
            menu : [
                {
                    text : 'De data',
                    handler : function(e)
                    {
                        var window = new UJI.UPO.CrearNodoConEstructuraDeFechaWindow(
                        {
                            mapa : ref.mapa,
                            mapaId : ref.mapaId
                        });

                        window.show();
                    }
                },
                {
                    text : 'De revista actualitat',
                    handler : function(e)
                    {
                        var window = new UJI.UPO.CrearNodoConEstructuraDeRevistaWindow(
                        {
                            mapa : ref.mapa,
                            mapaId : ref.mapaId
                        });

                        window.show();
                    }
                },
                {
                    text : 'De Vox UJI',
                    handler : function(e)
                    {
                        var window = new UJI.UPO.CrearNodoConEstructuraDeVoxujiWindow(
                        {
                            mapa : ref.mapa,
                            mapaId : ref.mapaId
                        });

                        window.show();
                    }
                }
            ]
        });

        ref.contentPreviewOption = new Ext.menu.Item(
        {
            text : 'Previsualització',
            menu : [
                {
                    text : 'Català',
                    handler : function(e)
                    {
                        ref.openPreviewWindow(ref.mapaUrlCompleta, 'ca');
                    }
                },
                {
                    text : 'Espanyol',
                    handler : function(e)
                    {
                        ref.openPreviewWindow(ref.mapaUrlCompleta, 'es');
                    }
                },
                {
                    text : 'Anglés',
                    handler : function(e)
                    {
                        ref.openPreviewWindow(ref.mapaUrlCompleta, 'en');
                    }
                }
            ]
        });

        ref.refrescarNodoOption = new Ext.menu.Item(
        {
            text : 'Refrescar node',
            handler : function(e)
            {
                UJI.Bus.publish("UJI.UPO.Tree.ContentNodeChanged");
            }
        });

        ref.menu = new Ext.menu.Menu(
        {
            style : {
                overflow : 'visible'
            },
            items : [
                ref.afegirNodoOption, ref.afegirEstructura, ref.renombrarNodoOption, ref.eliminarNodoOption,
                ref.duplicarNodoOption, ref.refrescarNodoOption, '-', ref.visibilidadOption, '-',
                /*ref.migracionContenidosOption, '-',*/
                ref.contentPreviewOption
            ]
        });

    },

    openPreviewWindow : function(url, idioma)
    {
        var server = window.location.protocol + "//" + window.location.host;

        if (server.indexOf("localhost:") > -1)
        {
            server = "http://localhost"
        }

        window.open(server + url + "&idioma=" + idioma, 'Previsualització', 'width=1000,height=900,location=no');
    },

    buildEstructuraMenuButton : function()
    {
        var ref = this;

        this.estructuraMenu = new Ext.Button(
        {
            text : 'Operacions',
            menu : ref.menu,
            handler : function(e)
            {
                var selectedNode = ref.getSelectionModel().getSelectedNode();

                if (!selectedNode)
                {
                    ref.disableOptions();
                }
            }
        });
    },

    getElementoDrag : function(dd, data)
    {
        if (dd.elementoDrag)
        {
            return dd.elementoDrag;
        }
        else
        {
            return (data.grid ? data.grid.elementoDrag : data.node.elementoDrag);
        }
    }
});