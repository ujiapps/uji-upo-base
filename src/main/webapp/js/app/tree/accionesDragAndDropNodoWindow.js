Ext.ns('UJI.UPO');

UJI.UPO.AccionesDragAndDropNodoWindow = Ext.extend(Ext.Window,
{
    title : 'Confirmació',
    layout : 'form',
    width : 600,
    height : 185,
    modal : true,
    padding : 10,
    fbar : [],
    urlCompletaNodoOrigen : '',
    urlCompletaNodoDestino : '',
    html : '',
    arrastrarOption : true,
    proponerOption : true,
    duplicarEstructuraOption : true,
    deuplicarEstructuraYContenidosOption : true,

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.AccionesDragAndDropNodoWindow.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.render(document.body);

        this.buildArrastrarButton();
        this.buildProponerButton();
        this.buildDuplicarEstructuraButton();
        this.buildDuplicarEstructuraYContenidosButton();

        var msg = 'Podeu fer les següents accions des del node origen <b>' + this.urlCompletaNodoOrigen + '</b> al node destí <b> ' + this.urlCompletaNodoDestino + '</b>:<br/>';
        if (this.arrastrarOption)
        {
            msg += ' - Arrastrar el node <br/>';
            this.getFooterToolbar().addButton(this.arrastrarButton);
        }

        if (this.duplicarEstructuraOption)
        {
            msg += ' - Duplicar l\'estructura d\'arbre<br/>';
            this.getFooterToolbar().addButton(this.duplicarEstructuraButton);
        }

        if (this.deuplicarEstructuraYContenidosOption)
        {
            msg += ' - Duplicar l\'estructura i tots els seus continguts<br/>';
            this.getFooterToolbar().addButton(this.duplicarEstructuraYContenidosButton);
        }

        if (this.proponerOption)
        {
            msg += ' - Proposar el node i tots els seus continguts<br/>';
            msg += '<input id="check-propuesta" style="margin-left: 8px" type="checkbox" /> Incloure tots els nodes descendents';
            msg += '<br/>'
            this.getFooterToolbar().addButton(this.proponerButton);
        }

        this.update(
        '<div class="x-window-dlg">' +
        '  <div class="x-dlg-icon">' +
        '    <div class="ext-mb-icon ext-mb-question">' +
        '    </div>' +
        '  <div class="ext-mb-text ext-mb-content">' +
        msg +
        '  </div>' +
        '</div>'
        );
    },

    buildArrastrarButton : function()
    {
        var ref = this;

        this.arrastrarButton = new Ext.Button(
        {
            text : 'Arrastrar',
            handler : function()
            {
                ref.fireEvent('arrastrar');
                ref.close();
            }
        });
    },

    buildProponerButton : function()
    {
        var ref = this;

        this.proponerButton = new Ext.Button(
        {
            text : 'Proposar',
            handler : function()
            {
                var check = Ext.DomQuery.selectNode('#check-propuesta');
                var incluirHijos = (check && check.checked) || false;

                ref.fireEvent('proponer',
                {
                    incluirHijos : incluirHijos
                });
                ref.close();
            }
        });
    },

    buildDuplicarEstructuraButton : function()
    {
        var ref = this;

        this.duplicarEstructuraButton = new Ext.Button(
        {
            text : 'Duplicar estructura',
            handler : function()
            {
                ref.fireEvent('duplicarEstructura');
                ref.close();
            }
        });
    },

    buildDuplicarEstructuraYContenidosButton : function()
    {
        var ref = this;

        this.duplicarEstructuraYContenidosButton = new Ext.Button(
        {
            text : 'Duplicar estructura i continguts',
            handler : function()
            {
                ref.fireEvent('duplicarTodo');
                ref.close();
            }
        });
    }
});