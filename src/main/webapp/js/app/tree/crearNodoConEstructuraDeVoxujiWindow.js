Ext.ns('UJI.UPO');

UJI.UPO.CrearNodoConEstructuraDeVoxujiWindow = Ext.extend(Ext.Window,
{
    title : "Nou node amb estructura de Vox UJI",
    layout : 'form',
    width : 300,
    height : 120,
    labelWidth : 60,
    modal : true,
    padding : 10,
    mapa : {},
    mapaId : '',
    addButton : {},
    cancelButton : {},
    fbar : [],
    listeners :
    {
        afterrender : function(window)
        {
            window.dia.focus(false, 500);
        }
    },

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.CrearNodoConEstructuraDeVoxujiWindow.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildAddButton();
        this.buildCancelButton();

        this.buildDia();

        this.add(this.dia);

        this.getFooterToolbar().addButton(this.addButton);
        this.getFooterToolbar().addButton(this.cancelButton);
    },

    buildDia : function()
    {
        this.dia = new Ext.form.NumberField(
        {
            fieldLabel : 'Número',
            allowBlank : false,
            anchor : '45%',
            allorDecimals : false,
            minLength : 1
        });
    },

    addNewFolder : function(dia)
    {
        var ref = this;

        if (ref.dia.isValid())
        {
            if (UJI.UPO.Application.comprobarSiNodoTieneHijoConMismoNombre(this.mapa, dia))
            {
                Ext.MessageBox.show(
                {
                    title : 'Error',
                    msg : 'Operació no permesa, ja hi ha un node amb el mateix nom.',
                    buttons : Ext.MessageBox.OK,
                    icon : Ext.MessageBox.ERROR
                });

                return false;
            }
            else
            {
                Ext.Ajax.request(
                {
                    url : '/upo/rest/mapa/' + ref.mapaId + '/estructuravoxuji',
                    params :
                    {
                        dia : dia
                    },
                    success : function()
                    {
                        UJI.Bus.publish("UJI.UPO.Tree.ContentNodeChanged");
                        return true;
                    },
                    failure : function(result, request)
                    {
                        Ext.Msg.alert('Error', "S'ha produït una errada");
                    },
                    method : 'POST'
                });

                return true;
            }
        }
        else
        {
            Ext.MessageBox.show(
            {
                title : 'Error',
                msg : 'Hi ha alguna errada en els camps del formulari.',
                buttons : Ext.MessageBox.OK,
                icon : Ext.MessageBox.ERROR
            });
            return false;
        }
    },

    buildAddButton : function()
    {
        var ref = this;
        this.addButton = new Ext.Button(
        {
            text : 'Crear',
            handler : function()
            {
                if (ref.addNewFolder(ref.dia.getValue()))
                {
                    ref.close();
                }
            }
        });
    },

    buildCancelButton : function()
    {
        ref = this;

        this.cancelButton = new Ext.Button(
        {
            text : 'Cancel·lar',
            handler : function()
            {
                ref.close();
            }
        });
    }
});