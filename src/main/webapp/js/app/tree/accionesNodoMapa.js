var AccionesNodoMapa =
{
    arrastrarNodo : function(target, data)
    {
        var nodoOrigen = data.node;
        var nodoDestino = target.node;
        var idNodoOrigen = nodoOrigen.attributes.id;
        var idNodoDestino = nodoDestino.attributes.id;
        var urlCompletaNodoOrigen = nodoOrigen.attributes.urlCompleta;
        var urlCompletaNodoDestino = nodoDestino.attributes.urlCompleta;
        var nombreNodoOrigen = nodoOrigen.attributes.title;
        var posReemplazar = urlCompletaNodoOrigen.lastIndexOf('/' + nombreNodoOrigen + '/') + 1;
        var urlBaseNodoOrigen = urlCompletaNodoOrigen.substring(0, posReemplazar);

        Ext.Ajax.request(
        {
            url : '/upo/rest/mapa/' + idNodoOrigen,
            method : 'PUT',
            params :
            {
                nodoMapaDestinoId : idNodoDestino
            },
            success : function()
            {
                UJI.UPO.Application.agregarUrlCompletaNodeTab(urlBaseNodoOrigen, urlCompletaNodoDestino, urlCompletaNodoOrigen);

                UJI.Bus.publish("UJI.UPO.Tree.NodeDragAndDrop",
                {
                    urlBaseNodoOrigen : urlBaseNodoOrigen,
                    urlCompletaNodoDestino : urlCompletaNodoDestino,
                    urlCompletaNodoOrigen : urlCompletaNodoOrigen,
                    nodoDestino : nodoDestino
                });
            },
            failure : function(result, request)
            {
                Ext.Msg.alert('Error', "S'ha produït una errada");
            }
        });
    },

    proponerNodo : function(target, data, incluirHijos)
    {
        var nodoOrigen = data.node;
        var nodoDestino = target.node;
        var idNodoOrigen = nodoOrigen.attributes.id;
        var idNodoDestino = nodoDestino.attributes.id;

        Ext.Ajax.request(
        {
            url : '/upo/rest/mapa/' + idNodoDestino + '/proponer',
            method : 'POST',
            params :
            {
                nodoMapaOrigenId : idNodoOrigen,
                incluirHijos: incluirHijos
            },
            success : function()
            {
                UJI.Bus.publish("UJI.UPO.Tree.ProposeNode",
                {
                    nodoDestino : nodoDestino
                });
            },
            failure : function(result, request)
            {
                Ext.Msg.alert('Error', "S'ha produït una errada");
            }
        });
    },

    duplicarNodo : function(target, data, duplicarContenidos)
    {
        var nodoOrigen = data.node;
        var nodoDestino = target.node;
        var idNodoOrigen = nodoOrigen.attributes.id;
        var idNodoDestino = nodoDestino.attributes.id;

        Ext.Ajax.request(
        {
            url : '/upo/rest/mapa/' + idNodoDestino + '/duplicar',
            method : 'POST',
            params :
            {
                nodoMapaOrigenId : idNodoOrigen,
                duplicarContenidos: duplicarContenidos
            },
            success : function()
            {
                UJI.Bus.publish("UJI.UPO.Tree.DuplicateNode",
                {
                    nodoDestino : nodoDestino
                });
            },
            failure : function(result, request)
            {
                Ext.Msg.alert('Error', "S'ha produït una errada");
            }
        });
    },

    reordenarNodo : function(target, data, point)
    {
        var nodoOrigen = data.node;
        var nodoDestino = target.node;
        var idNodoOrigen = nodoOrigen.attributes.id;
        var idNodoDestino = nodoDestino.attributes.id;
        var idNodoPadreDestino = nodoDestino.parentNode.attributes.id;

        Ext.Ajax.request(
        {
            url : '/upo/rest/mapa/' + idNodoPadreDestino + '/reordenar-nodos',
            method : 'PUT',
            params :
            {
                nodoMapaOrigenId : idNodoOrigen,
                nodoMapaJuntoId : idNodoDestino,
                point : point
            },
            success : function()
            {
                UJI.Bus.publish("UJI.UPO.Tree.ReorderNode",
                {
                    nodoDestino : nodoDestino.parentNode
                });
            },
            failure : function(result, request)
            {
                Ext.Msg.alert('Error', "S'ha produït una errada");
            }
        });
    }
};