Ext.ns('UJI.UPO.ux');

Ext.util.Format.timefieldRenderer = function(format)
{
    return function(v)
    {
        if (v instanceof Date)
        {
            return v.format(format);
        }
        else
        {
            return v;
        }
    };
};

UJI.UPO.ux.DatePickerPanel = Ext
        .extend(
                Ext.Panel,
                {
                    dateList : {},
                    datePicker : {},
                    layout : 'border',
                    width : 468,
                    height : 240,
                    getDateList : function()
                    {
                        return this.dateList;
                    },
                    getDatePicker : function()
                    {
                        return this.datePicker;
                    },
                    initComponent : function()
                    {
                        var config = {};
                        Ext.apply(this, Ext.apply(this.initialConfig, config));
                        UJI.UPO.ux.DatePickerPanel.superclass.initComponent.call(this);

                        var rowEditor = new Ext.ux.grid.RowEditor(
                        {
                            saveText : 'Actualitzar',
                            cancelText : 'Cancel·lar',
                            errorSummary : false,
                            listeners :
                            {
                                'afteredit' : function(e)
                                {
                                    e.record.commit();
                                }
                            }
                        });

                        this.dateList = new Ext.grid.GridPanel(
                        {
                            autoScroll : true,
                            width : 290,
                            height : 165,
                            style : 'padding-top: 10px; padding-left: 10px;',
                            plugins : [ rowEditor ],
                            store : new Ext.data.ArrayStore(
                            {
                                fields : [ 'data',
                                {
                                    name : 'horaInicio',
                                    type : 'date',
                                    dateFormat : 'd/m/Y H:i:s'
                                },
                                {
                                    name : 'horaFin',
                                    type : 'date',
                                    dateFormat : 'd/m/Y H:i:s'
                                } ]
                            }),
                            columns : [
                            {
                                header : 'Data seleccionada',
                                name : 'data',
                                dataIndex : 'data'
                            },
                            {
                                header : 'Hora inici',
                                dataIndex : 'horaInicio',
                                renderer : Ext.util.Format.timefieldRenderer('H:i'),
                                width : 65,
                                format : 'H:i',
                                editor : new Ext.form.TimeField(
                                {
                                    format : 'H:i',
                                    mode : 'local'
                                })
                            },
                            {
                                header : 'Hora fi',
                                dataIndex : 'horaFin',
                                renderer : Ext.util.Format.timefieldRenderer('H:i'),
                                width : 65,
                                editor : new Ext.form.TimeField(
                                {
                                    format : 'H:i',
                                    mode : 'local'
                                })
                            } ]
                        });

                        var dateListComponent = this.dateList;

                        this.datePicker = new Ext.DatePicker(
                        {
                            region : 'center',
                            width : 180,
                            height : 190,
                            startDay : 1,
                            cancelText: 'Cance·lar',
                            monthNames : ['Gener','Febrer','Març','Abril','Maig','Juny','Juliol','Agost','Setembre','Octubre','Novembre','Decembre'],
                            dayNames : ["Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte", "Diumenge"],
                            monthYearText: 'Elegeix un mes',
                            nextText: 'Pròxim mes',
                            prevText: 'Mes anterior',
                            todayText: 'Avui',
                            listeners :
                            {
                                'select' : function(element, date)
                                {
                                    ref.addVigencia(date.format('d/m/Y'), '', '');
                                }
                            }
                        });

                        this.add(this.datePicker);

                        this.add(
                        {
                            xtype : 'panel',
                            layout : 'vbox',
                            region : 'east',
                            width : 290,
                            height : 190,
                            items : [
                                    {
                                        xtype : 'panel',
                                        layout : 'hbox',
                                        width : 290,
                                        height : 25,
                                        layoutConfig :
                                        {
                                            pack : 'end',
                                            align : 'right'
                                        },
                                        items : [
                                                {
                                                    xtype : 'button',
                                                    text : 'Eliminar',
                                                    handler : function(item, event)
                                                    {
                                                        var lista = dateListComponent
                                                                .getSelectionModel()
                                                                .getSelections();

                                                        for ( var i = 0; i < lista.length; i++)
                                                        {
                                                            dateListComponent.store
                                                                    .remove(lista[i]);
                                                        }
                                                    }
                                                },
                                                {
                                                    xtype : 'button',
                                                    text : 'Netejar',
                                                    handler : function(item, event)
                                                    {
                                                        dateListComponent.store.removeAll();
                                                    }
                                                } ]
                                    }, dateListComponent ]
                        });

                        var rangoInicio = new Ext.form.TextField(
                        {
                            width : 82,
                            style : 'padding-right:5px',
                            value : new Date().format('d/m/Y')
                        });

                        var rangoFin = new Ext.form.TextField(
                        {
                            width : 82,
                            style : 'padding-right:5px'
                        });

                        var ref = this;

                        this
                                .add(
                                {
                                    xtype : 'panel',
                                    layout : 'hbox',
                                    region : 'south',
                                    width : 350,
                                    height : 50,
                                    style : 'padding-top: 15px;',
                                    items : [
                                            {
                                                xtype : 'label',
                                                text : 'Des de:',
                                                style : 'padding-right:5px'
                                            },
                                            rangoInicio,
                                            {
                                                xtype : 'label',
                                                text : ' a: ',
                                                style : 'padding-right:5px; padding-left:5px'
                                            },
                                            rangoFin,
                                            {
                                                xtype : 'button',
                                                text : 'Afegir rang',
                                                style : 'padding-left:10px',
                                                handler : function()
                                                {
                                                    if (rangoInicio.getValue() == "")
                                                    {
                                                        Ext.Msg
                                                                .alert('Error',
                                                                        'La data d\'inici és obligatòria');
                                                        return;
                                                    }

                                                    if (rangoFin.getValue() == "")
                                                    {
                                                        Ext.Msg.alert('Error',
                                                                'La data de fi és obligatòria');
                                                        return;
                                                    }

                                                    var ini = ref.fromStringToDate(rangoInicio
                                                            .getValue());
                                                    var fin = ref.fromStringToDate(rangoFin
                                                            .getValue());

                                                    if (ini >= fin)
                                                    {
                                                        Ext.Msg
                                                                .alert('Error',
                                                                        'Les dates introduïdes han de ser diferents i la data d\'inici ha de ser major que la de fi');
                                                        return;
                                                    }

                                                    while (ini <= fin)
                                                    {
                                                        ref
                                                                .addVigencia(ini.format('d/m/Y'),
                                                                        '', '');
                                                        ini.setDate(ini.getDate() + 1);
                                                    }
                                                }
                                            } ]
                                });
                    },

                    fromStringToDate : function(dateString)
                    {
                        var dateTokenList = dateString.split('/');
                        var englishDate = dateTokenList[1] + '/' + dateTokenList[0] + '/'
                                + dateTokenList[2];

                        return new Date(Date.parse(englishDate));
                    },

                    addVigencia : function(data, horaInicio, horaFin)
                    {
                        if (this.dateList.store.find("data", data) != -1)
                        {
                            return;
                        }

                        var VigenciaTemplate = Ext.data.Record.create([
                        {
                            name : 'data'
                        },
                        {
                            name : 'horaInicio'
                        },
                        {
                            name : 'horaFin'
                        } ]);

                        var newVigencia = new VigenciaTemplate(
                        {
                            data : data,
                            horaInicio : horaInicio,
                            horaFin : horaFin
                        });

                        this.dateList.stopEditing();
                        this.dateList.store.add(newVigencia);
                    }
                });