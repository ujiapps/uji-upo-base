Ext.ns('UJI.UPO');

UJI.UPO.VisibilidadContenidoWindow = Ext.extend(Ext.Window,
{
    title : 'Visibilitat',
    layout : 'fit',
    height : 240,
    width : 680,
    modal : true,
    padding : 10,
    formRegistro : {},
    formRegistroRequest : {},
    addButton : {},
    cancelButton : {},
    textoNoVisible : '',
    fbar : [],

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.VisibilidadContenidoWindow.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildAddButton();
        this.buildCancelButton();
        this.buildFormRegistro();

        this.add(this.formRegistro);

        this.getFooterToolbar().addButton(this.addButton);
        this.getFooterToolbar().addButton(this.cancelButton);
    },

    setAdmin : function(administrador)
    {
        this.administrador = administrador;

        if (this.administrador)
        {
            this.addButton.enable();
            this.formRegistro.enable();
        }
    },

    buildFormRegistroRequest : function()
    {
        var ref = this;

        ref.fireEvent("setVisibility",
        {
            visible : ref.formRegistro.radioButtonVisibilidad.getValue().inputValue,
            textoNoVisible : ref.formRegistro.textAreaAlternativo.getValue()
        });
    },

    buildFormRegistro : function()
    {
        this.formRegistro = new UJI.UPO.VisibilidadContenidoPanel({
            padding : 5,
            visibleActivo : this.visibleActivo,
            textoNoVisible : this.textoNoVisible,
            disabled : true
        });
    },

    buildAddButton : function()
    {
        var ref = this;
        this.addButton = new Ext.Button(
        {
            text : 'Desar',
            disabled : true,
            handler : function()
            {
                ref.buildFormRegistroRequest();
                ref.close();
            }
        });
    },

    buildCancelButton : function()
    {
        ref = this;

        this.cancelButton = new Ext.Button(
        {
            text : 'Cancel·lar',
            handler : function()
            {
                ref.close();
            }
        });
    }
});