Ext.MessageBox.buttonText.yes = "Sí";
Ext.MessageBox.buttonText.ok = "Acceptar";
Ext.MessageBox.buttonText.cancel = "Cancel·lar";

UJI.UPO.Application =
{
    getTreeSelectedNode : function()
    {
        if (this.treePanel)
        {
            return this.treePanel.getSelectionModel().getSelectedNode();
        }
    },

    registerTreePanel : function(treePanel)
    {
        this.treePanel = treePanel;
    },

    createNewNodeTab : function(treePanel, selectedNode, nodeId, nodeText)
    {
        administrador = (selectedNode.attributes.administrador == 'S');
        nombreNodo = selectedNode.attributes.title;
        urlCompleta = selectedNode.attributes.urlCompleta;

        var contenidoPanel = new UJI.UPO.ContenidoPanel(
        {
            mapaId : nodeId,
            mapaName : nombreNodo,
            mapaUrlCompleta : urlCompleta,
            treePanel : treePanel
        });

        var plantillaPanel = new UJI.UPO.PlantillasNodoMapa(
        {
            title : 'Plantilles',
            mapaId : nodeId
        });

        var gestionNodoPanel = new UJI.UPO.GestionNodoPanel(
        {
            mapaId : nodeId
        });

        var nodoTabPanel = new Ext.TabPanel(
        {
            activeTab : 0,
            autoScroll : true,
            padding : 10,
            deferredRender : false,
            flex : 1,
            items : [ contenidoPanel, plantillaPanel, gestionNodoPanel ]
        });

        var tab = new Ext.Panel(
        {
            title : nodeText,
            urlCompleta : urlCompleta,
            layout : 'vbox',
            closable : true,
            activeTab : 0,
            autoScroll : true,
            deferredRender : false,
            treeNodePath : selectedNode.getPath("urlPath"),
            layoutConfig :
            {
                align : 'stretch'
            },
            items : [
            {
                html : urlCompleta,
                cls : 'panel-url-completa'
            }, nodoTabPanel ],
            setUrlCompleta : function(urlCompleta)
            {
                this.urlCompleta = urlCompleta;
                this.items.items[0].update(urlCompleta);
            }
        });

        plantillaPanel.setAdmin(administrador);
        contenidoPanel.setAdmin(administrador);
        gestionNodoPanel.setAdmin(administrador);

        return {
            nodoTab : tab,
            contenidoPanel : contenidoPanel
        };
    },

    deleteNodeTabsWithUrlCompletaBeginsByUrlBase : function(urlBase)
    {
        var listaTabs = this.tabPanel.items.items;
        var longitudLista = listaTabs.length;

        for ( var it = 0; it < longitudLista; it++)
        {
            if (listaTabs[it] && listaTabs[it].urlCompleta && listaTabs[it].urlCompleta.indexOf(urlBase) == 0)
            {
                this.tabPanel.remove(listaTabs[it]);
                it--;
            }
        }
    },

    cambiarUrlCompletaNodeTab : function(urlAnterior, urlNueva)
    {
        var listaTabs = this.tabPanel.items.items;
        var longitudLista = listaTabs.length;

        for ( var it = 0; it < longitudLista; it++)
        {
            if (listaTabs[it] && listaTabs[it].urlCompleta && listaTabs[it].urlCompleta.indexOf(urlAnterior) == 0)
            {
                var posACortar = urlAnterior.length;
                var cadenaAGuardar = listaTabs[it].urlCompleta.substring(posACortar);
                var nuevaUrlCompleta = urlNueva + cadenaAGuardar;

                listaTabs[it].setTitle(nuevaUrlCompleta);
                listaTabs[it].setUrlCompleta(nuevaUrlCompleta);
            }
        }
    },

    agregarUrlCompletaNodeTab : function(urlBase, urlNueva, urlAnterior)
    {
        var listaTabs = this.tabPanel.items.items;
        var longitudLista = listaTabs.length;

        for ( var it = 0; it < longitudLista; it++)
        {
            if (listaTabs[it] && listaTabs[it].urlCompleta && listaTabs[it].urlCompleta.indexOf(urlAnterior) == 0)
            {
                var posACortar = urlBase.length;
                var cadenaAGuardar = listaTabs[it].urlCompleta.substring(posACortar);
                var nuevaUrlCompleta = urlNueva + cadenaAGuardar;

                listaTabs[it].setTitle(nuevaUrlCompleta);
                listaTabs[it].setUrlCompleta(nuevaUrlCompleta);
            }
        }
    },

    comprobarSiNodoTieneHijoConMismoNombre : function(nodo, nombre)
    {
        var respuesta = false;

        nodo.eachChild(function(childNode)
        {
            if (childNode.attributes.title == nombre)
            {
                respuesta = true;
            }
        });

        return respuesta;
    },

    enableResponses : function()
    {
        Ext.Ajax.on('beforerequest', function()
        {
            Ext.getCmp('_$loadingIndicator').show();
        });

        Ext.Ajax.on('requestcomplete', function(conn, response, options)
        {
            Ext.getCmp('_$loadingIndicator').hide();

            if (options.isUpload)
            {
                var msgList = response.responseXML.getElementsByTagName("msg");

                if (msgList && msgList[0] && msgList[0].firstChild)
                {
                    Ext.MessageBox.show(
                    {
                        title : 'Error',
                        msg : msgList[0].firstChild.nodeValue,
                        buttons : Ext.MessageBox.OK,
                        icon : Ext.MessageBox.ERROR
                    });
                }
            }
        });

        Ext.Ajax.on('requestexception', function(conn, response, options)
        {
            Ext.getCmp('_$loadingIndicator').hide();

            if (response.responseXML)
            {
                var msgList = response.responseXML.getElementsByTagName("msg");

                if (msgList && msgList[0] && msgList[0].firstChild)
                {
                    alert(msgList[0].firstChild.nodeValue);
                }
            }
        });
    },

    getLogo : function()
    {
        return new Ext.Panel(
        {
            style : 'border:0px; border:0px;',
            html : '<div style="background: url(//e-ujier.uji.es/img/portal2/imagenes/cabecera_1px.png) repeat-x scroll left top transparent; height: 70px;">'
                    + '  <img src="//static.uji.es/img/commons/m-uji.png" style="float: left;margin: 10px 16px;">' + '  <div style="float:left; margin-top:11px;">'
                    + '    <span style="color: rgb(255, 255, 255); font-family: Helvetica,Arial,sans-serif;font-size:1.2em;">E-UJIER@</span><br/>'
                    + '    <span style="color: #CDCCE5; font-family: Helvetica,Arial,sans-serif;">Portal</span>' + '  </div>' + '</div>'
        });
    },

    init : function()
    {
        this.tabPanel = new Ext.ux.uji.TabPanel(
        {
            region : 'center'
        });
    },

    initListenersTabPanel : function(tabPanel, treePanel)
    {
        this.tabPanel.on("tabchange", function(tabPanel, tab)
        {
            treePanel.selectPath(tab.treeNodePath, "urlPath");
        });
    },

    getTabPanel : function()
    {
        return this.tabPanel;
    },

    closeCurrentTabPanel : function()
    {
        this.tabPanel.remove(this.tabPanel.activeTab);
    },

    showMessageError : function(message)
    {
        Ext.MessageBox.show(
        {
            title : 'Errada',
            msg : message,
            buttons : Ext.MessageBox.OK,
            icon : Ext.MessageBox.ERROR
        });
    }
};