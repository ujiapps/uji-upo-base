Ext.ns('UJI.UPO');

UJI.UPO.VisibilidadContenidoConCheckWindow = Ext.extend(UJI.UPO.VisibilidadContenidoWindow,
{
    height : 280,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.VisibilidadContenidoConCheckWindow.superclass.initComponent.call(this);

        this.buildCheckBoxExtenderAHijos();
        this.formRegistro.add(this.checkBoxExtenderAHijos);
    },

    buildCheckBoxExtenderAHijos : function()
    {
        this.checkBoxExtenderAHijos = new Ext.form.Checkbox(
        {
            fieldLabel : 'Aplicar a totes les subcarpetes'
        });
    },

    buildFormRegistroRequest : function()
    {
        var ref = this;
        var visibleRadioValue = ref.formRegistro.radioButtonVisibilidad.getValue();

        if (!visibleRadioValue)
        {
            return;
        }

        ref.fireEvent("setVisibility",
        {
            visible : visibleRadioValue.inputValue,
            textoNoVisible : ref.formRegistro.textAreaAlternativo.getValue(),
            extenderAHijos : ref.checkBoxExtenderAHijos.getValue()
        });
    }
});