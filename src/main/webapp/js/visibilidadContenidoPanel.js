Ext.ns('UJI.UPO');

UJI.UPO.VisibilidadContenidoPanel = Ext.extend(Ext.Panel,
{
    layout : 'form',
    formRegistro : {},
    textAreaAlternativo : {},
    radioButtonVisibilidad : {},
    textoNoVisible : '',

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.VisibilidadContenidoPanel.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildTextAreaAlternativo();
        this.buildRadioButtonVisibilidad();

        this.add(this.radioButtonVisibilidad);
        this.add(this.textAreaAlternativo);
    },

    setValues : function(visible, textoNovisible)
    {
        this.radioButtonVisibilidad.setValue(visible);
        this.fireActionsWhenSetVisibilidadField(visible);

        this.textAreaAlternativo.setValue(textoNovisible);
    },

    buildTextAreaAlternativo : function()
    {
        var ref = this;

        this.textAreaAlternativo = new Ext.form.TextArea(
        {
            fieldLabel : 'Introdueix el text alternatiu',
            name : 'textoNoVisible',
            width : '95%',
            height : 100,
            html : ref.textoNoVisible,
            disabled : ref.visibleActivo
        });
    },

    buildRadioButtonVisibilidad : function()
    {
        var ref = this;
        var visibilidadEstablecida = (typeof ref.visibleActivo != 'undefined');

        this.radioButtonVisibilidad = new Ext.form.RadioGroup(
        {
            fieldLabel : 'Visible',
            name : 'visible',
            columns : 2,
            width : 100,
            items : [
                {
                    boxLabel : 'Si',
                    name : 'visible',
                    inputValue : 'S',
                    checked : visibilidadEstablecida && ref.visibleActivo,
                    width: '40px'
                },
                {
                    boxLabel : 'No',
                    name : 'visible',
                    inputValue : 'N',
                    checked : visibilidadEstablecida && !ref.visibleActivo
                }
            ],
            listeners : {
                'change' : function(radioGroup, checked)
                {
                    if (checked)
                    {
                        ref.fireActionsWhenSetVisibilidadField(checked.inputValue)
                    }
                }
            }
        });
    },

    fireActionsWhenSetVisibilidadField : function(visibilidad)
    {
        if (visibilidad == 'S')
        {
            this.textAreaAlternativo.disable();
        }

        if (visibilidad == 'N')
        {
            this.textAreaAlternativo.enable();
        }
    },

    buildFormRegistro : function()
    {
        this.formRegistro = new Ext.form.FormPanel(
        {
            disabled : true,
            frame : true,
            items : [ref.radioButtonVisibilidad, ref.textAreaAlternativo]
        });
    }
});