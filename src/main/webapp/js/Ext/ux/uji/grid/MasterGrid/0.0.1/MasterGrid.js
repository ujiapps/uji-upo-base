Ext.ns('Ext.ux.uji.grid');

Ext.ux.uji.grid.MasterGrid = Ext.extend(Ext.Panel,
{
    layout : 'vbox',
    closable : true,
    layoutConfig :
    {
        align : 'stretch'
    },

    grid : {},
    store : {},
    editor : {},
    botonInsertar : {},
    botonBorrar : {},

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        Ext.ux.uji.grid.MasterGrid.superclass.initComponent.call(this);

        this.initUI();

        this.add(this.grid);
    },

    initUI : function()
    {
        this.buildStore();
        this.buildEditor();
        this.buildBotonInsertar();
        this.buildBotonBorrar();
        this.buildGrid();
    },

    getColumnNames : function()
    {
        var keys = [];

        for ( var column in this.columns)
        {
            if (this.columns.hasOwnProperty(column))
            {
                keys.push(column);
            }
        }

        return keys;
    },

    generateEmptyRecord : function()
    {
        var emptyRecord = {};

        for ( var column in this.columns)
        {
            if (this.columns.hasOwnProperty(column))
            {
                emptyRecord[column] = '';
            }
        }

        return emptyRecord;
    },

    buildColumnList : function()
    {
        var columnList = [];

        for ( var column in this.columns)
        {
            if (this.columns.hasOwnProperty(column))
            {
                var baseColumn = this.columns[column];
                baseColumn.dataIndex = column;

                if (!this.columns[column].editor && column != 'id')
                {
                    baseColumn.editor = new Ext.form.TextField(
                    {
                        allowBlank : false,
                        blankText : 'El camp "' + this.columns[column].header + '" es requerit'
                    });
                }

                columnList.push(baseColumn);
            }
        }

        return columnList;
    },

    buildGrid : function()
    {
        this.grid = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            viewConfig :
            {
                forceFit : true
            },
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : this.store,
            plugins : [ this.editor ],
            colModel : new Ext.grid.ColumnModel(
            {
                columns : this.buildColumnList()
            }),
            tbar :
            {
                items : [ this.botonInsertar, '-', this.botonBorrar ]
            }
        });
    },

    buildStore : function()
    {
        this.store = new Ext.data.Store(
        {
            url : this.url,
            autoLoad : true,
            restful : true,
            reader : new Ext.data.XmlReader(
            {
                record : this.record,
                idProperty : 'id'
            }, this.getColumnNames()),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildEditor : function()
    {
        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancel·lar',
            errorSummary : false
        });
    },

    buildBotonInsertar : function()
    {
        this.botonInsertar = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function(btn, event)
            {
                var rec = new this.store.recordType(this.generateEmptyRecord());

                this.editor.stopEditing();
                this.store.insert(0, rec);
                this.editor.startEditing(0);
            },
            scope : this
        });
    },

    buildBotonBorrar : function()
    {
        this.botonBorrar = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(btn, event)
            {
                var rec = this.grid.getSelectionModel().getSelected();
                if (!rec)
                {
                    Ext.Msg.alert('Información', 'És necessari seleccionar una fila per esborrar');
                    return false;
                }
                else
                {
                    Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre?',
                            function(btn, text)
                            {
                                if (btn == 'yes')
                                {

                                    this.store.remove(rec);
                                }
                            }, this);
                }
            },
            scope : this
        });
    }
});