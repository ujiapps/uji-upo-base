Ext.ns('UJI.UPO');

UJI.UPO.Busqueda = Ext.extend(Ext.Panel,
{
    title : 'Dashboard',
    layout : 'vbox',
    closable : false,
    layoutConfig :
    {
        align : 'stretch'
    },
    grid : {},
    store : {},
    botonEditar : {},
    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.UPO.Busqueda.superclass.initComponent.call(this);

        this.initUI();

        var searchField = new Ext.form.TextField(
        {
            name : 'query'
        });

        var ref = this;

        var panelBusqueda = new Ext.form.FormPanel(
        {
            layout : 'hbox',
            frame : true,
            items : [
            {
                xtype : 'label',
                text : 'Consulta:',
                cls : 'x-btn button'
            }, searchField,
            {
                xtype : 'button',
                text : 'Cercar',
                handler : function(e)
                {
                    ref.store.load(
                    {
                        params :
                        {
                            query : searchField.getValue()
                        }
                    });
                }
            } ]
        });

        this.add(panelBusqueda);
        this.add(this.grid);
    },
    initUI : function()
    {
        this.buildStore();

        var ref = this;

        this.botonEditar = new Ext.Button(
        {
            text : 'Editar',
            iconCls : 'application-edit'
        });

        this.grid = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            viewConfig :
            {
                forceFit : true
            },
            selModel : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : this.store,
            tbar : [ ref.botonEditar ],
            colModel : new Ext.grid.ColumnModel(
            {
                columns : [
                {
                    header : 'id',
                    dataIndex : 'id'
                },
                {
                    header : 'URL',
                    dataIndex : 'url'
                },
                {
                    header : 'Título',
                    dataIndex : 'titulo'
                } ]
            })
        });
    },
    buildStore : function()
    {
        this.store = new Ext.data.Store(
        {
            restful : true,
            proxy : new Ext.data.HttpProxy(
            {
                url : '/upo/rest/busqueda'
            }),
            reader : new Ext.data.XmlReader(
            {
                record : 'contenidoSolr',
                id : 'id'
            }, [ 'id', 'titulo', 'url', 'urlCompleta' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    }
});
