Ext.ns('UJI.UPO');

UJI.UPO.ContenidoMetadatosWindow = Ext.extend(Ext.Window,
{
    title : 'Metadades',
    layout : 'form',
    width : 700,
    height : 600,
    modal : true,
    padding : 10,
    mapaId : '',
    contenidoId : '',
    addButton : {},
    cancelButton : {},
    fbar : [],
    storeEsquemas : {},
    comboEsquemas : {},
    tabPanel : {},

    initComponent : function()
    {
        Ext.layout.FormLayout.prototype.trackLabels = true;

        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.ContenidoMetadatosWindow.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildAddButton();
        this.buildCancelButton();

        this.buildForm();

        this.buildStoreEsquemas();
        this.buildComboEsquemas();

        this.buildNuevoMetadato();

        this.buildTabsIdiomas();

        this.add(this.form);

        this.form.add(this.comboEsquemas);
        this.form.add(this.nuevoMetadato);
        this.form.add(this.tabPanel);

        if (this.administrador && this.tipoContenido == UJI.UPO.TipoReferenciaContenido.NORMAL)
        {
            this.addButton.enable();
        }

        this.getFooterToolbar().addButton(this.addButton);
        this.getFooterToolbar().addButton(this.cancelButton);
    },

    loadForm : function()
    {
        var ref = this;

        Ext.Ajax.request(
        {
            url : '/upo/rest/contenido/' + ref.contenidoId + '/metadato/',
            method : 'GET',
            success : function(r)
            {
                ref.buildFormFromLoadRequest(r);
            },
            failure : function()
            {
                UJI.UPO.Application.showMessageError("Errada carregant el formulari");
            }
        });
    },

    buildFormFromLoadRequest : function(r, esquemaId)
    {
        var ref = this;
        var esquemaId = Ext.DomQuery.selectValue('esquemaId', r.responseXML);

        if (esquemaId)
        {
            this.comboEsquemas.setValue(esquemaId);
        }
        else
        {
            esquemaId = -1;
        }

        UJI.UPO.ContenidoMetadatosUtil.requestAtributosFromEsquema(esquemaId, ref.panels, function()
        {
            var metadatos = Ext.DomQuery.jsSelect('ContenidoMetadato', r.responseXML);

            ref.doLayout();

            for (var i = 0; i < metadatos.length; i++)
            {
                var tipo = Ext.DomQuery.selectValue('tipoMetadatoId', metadatos[i]);
                var atributo = Ext.DomQuery.selectValue('atributoMetadatoId', metadatos[i]);
                var key = Ext.DomQuery.selectValue('clave', metadatos[i]);
                var valorCA = Ext.DomQuery.selectValue('valorCA', metadatos[i]);
                var valorES = Ext.DomQuery.selectValue('valorES', metadatos[i]);
                var valorEN = Ext.DomQuery.selectValue('valorEN', metadatos[i]);

                if (atributo)
                {
                    ref.panelCA.setValue(key, valorCA);
                    ref.panelES.setValue(key, valorES);
                    ref.panelEN.setValue(key, valorEN);
                }

                if (tipo)
                {
                    UJI.UPO.ContenidoMetadatosUtil.addField(
                    {
                        id : tipo,
                        key : key,
                        tipo : tipo,
                        permiteBlanco : true,
                        valores : null,
                        nodo : null,
                        nuevo : true,
                        publicable : true,
                        values : {
                            CA : valorCA,
                            ES : valorES,
                            EN : valorEN
                        },
                        nombreClave : {
                            CA : Ext.DomQuery.selectValue('nombreClaveCA', metadatos[i]),
                            ES : Ext.DomQuery.selectValue('nombreClaveES', metadatos[i]),
                            EN : Ext.DomQuery.selectValue('nombreClaveEN', metadatos[i])
                        }
                    }, ref.panels);
                }
            }
            ref.doLayout();
        });
    },

    buildForm : function()
    {
        this.form = new Ext.FormPanel(
        {
            border : false,
            anchor : '100% 100%',
            padding : 5,
            reader : new Ext.data.XmlReader({}),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, ['success', 'msg'])
        });
    },

    doLayout : function()
    {
        UJI.UPO.ContenidoMetadatosUtil.doLayout(this.panels);
        ref.tabPanel.setActiveTab(1);
        ref.tabPanel.setActiveTab(2);
        ref.tabPanel.setActiveTab(0);
    },

    buildNuevoMetadato : function()
    {
        var ref = this;
        var arrayValores = [];

        for (tipo in UJI.UPO.TipoMetadato)
        {
            if (UJI.UPO.TipoMetadato[tipo].select)
            {
                arrayValores.push([tipo, UJI.UPO.TipoMetadato[tipo].nombre]);
            }
        }

        this.nuevoMetadatoKey = new Ext.form.TextField(
        {
            fieldLabel : 'Clau',
            width : '20%',
            maskRe : /[a-zA-Z0-9_\-]/,
            regex : /^[a-zA-Z0-9_.\-]+$/i
        })

        this.nuevoMetadatoTipo = new Ext.form.ComboBox(
        {
            valueField : 'id',
            displayField : 'nombre',
            triggerAction : 'all',
            forceSelection : true,
            mode : 'local',
            editable : false,
            store : new Ext.data.ArrayStore(
            {
                id : 0,
                fields : ['id', 'nombre'],
                data : arrayValores
            })
        }),

        this.nombreMetadatoCA = new Ext.form.TextField(
        {
            fieldLabel : 'Títol',
            width : '50%'
        });

        this.nombreMetadatoES = new Ext.form.TextField(
        {
            width : '50%'
        });

        this.nombreMetadatoEN = new Ext.form.TextField(
        {
            width : '50%'
        });

        this.nuevoMetadato = new Ext.form.FieldSet(
        {
            title : 'Nou atribut',
            layout : 'form',
            items : [
                {
                    xtype : 'compositefield',
                    items : [
                        this.nuevoMetadatoKey,
                        {
                            xtype : 'label',
                            text : 'Tipus: ',
                            style : 'line-height: 20px'
                        }, this.nuevoMetadatoTipo
                    ]
                },
                {
                    xtype : 'compositefield',
                    items : [
                        this.nombreMetadatoCA,
                        {
                            xtype : 'label',
                            text : 'Català',
                            style : 'line-height: 20px'
                        }
                    ]
                },
                {
                    xtype : 'compositefield',
                    items : [
                        this.nombreMetadatoES,
                        {
                            xtype : 'label',
                            text : 'Espanyol',
                            style : 'line-height: 20px'
                        }
                    ]
                },
                {
                    xtype : 'compositefield',
                    items : [
                        this.nombreMetadatoEN,
                        {
                            xtype : 'label',
                            text : 'Anglès',
                            style : 'line-height: 20px'
                        }
                    ]
                },
                {
                    iconCls : 'accept',
                    xtype : 'button',
                    text : 'afegir',
                    style : 'padding-left: 105px',
                    handler : function()
                    {
                        var key = ref.nuevoMetadatoKey.getValue();
                        var tipo = ref.nuevoMetadatoTipo.getValue();
                        var nombreClaveCA = ref.nombreMetadatoCA.getValue();
                        var nombreClaveES = ref.nombreMetadatoES.getValue();
                        var nombreClaveEN = ref.nombreMetadatoEN.getValue();

                        if (!ref.nuevoMetadatoKey.isValid())
                        {
                            UJI.UPO.Application.showMessageError("La clau no és vàl·lida");
                            return;
                        }

                        if (!key || !tipo || !nombreClaveCA || !nombreClaveEN || !nombreClaveES)
                        {
                            UJI.UPO.Application.showMessageError("Tots els camps són obligatoris");
                            return;
                        }

                        UJI.UPO.ContenidoMetadatosUtil.addField(
                        {
                            id : tipo,
                            key : key,
                            tipo : tipo,
                            permiteBlanco : true,
                            valores : null,
                            nodo : null,
                            nuevo : true,
                            publicable : true,
                            width: '90%',
                            nombreClave : {
                                CA : nombreClaveCA,
                                ES : nombreClaveES,
                                EN : nombreClaveEN
                            }
                        }, ref.panels);

                        ref.doLayout();
                    }
                }
            ]
        });
    },

    deleteField : function(key)
    {
        ref.searchFieldAndDoAction(key, function(panel, item)
        {
            panel.remove(item);
            panel.doLayout();
        });
    },

    buildTabsIdiomas : function()
    {
        this.panelCA = new UJI.UPO.ContenidoMetadatosIdiomaPanel(
        {
            title : 'Català',
            autoScroll : true,
            layout : 'form',
            idioma : 'CA',
            width : '90%'
        });

        this.panelES = new UJI.UPO.ContenidoMetadatosIdiomaPanel(
        {
            title : 'Espanyol',
            autoScroll : true,
            layout : 'form',
            idioma : 'ES',
            width : '90%'
        });

        this.panelEN = new UJI.UPO.ContenidoMetadatosIdiomaPanel(
        {
            title : 'Anglès',
            autoScroll : true,
            layout : 'form',
            idioma : 'EN',
            width : '90%'
        });

        this.panels = [ref.panelCA, ref.panelES, ref.panelEN];

        this.panelCA.on("deleteField", function(params)
        {
            ref.deleteField(params.key);
        });

        this.panelES.on("deleteField", function(params)
        {
            ref.deleteField(params.key);
        });

        this.panelEN.on("deleteField", function(params)
        {
            ref.deleteField(params.key);
        });

        this.tabPanel = new Ext.TabPanel(
        {
            anchor : '100% 60%',
            padding : 10,
            activeTab : 0,
            deferredRender : false,
            items : [this.panelCA, this.panelES, this.panelEN],
            listeners : {
                tabchange : function(ref, tab)
                {
                    tab.doLayout();
                }
            }
        })
    },

    searchFieldAndDoAction : function(key, action)
    {
        var idiomas = ['CA', 'ES', 'EN'];

        for (var i = 0; i < idiomas.length; i++)
        {
            var items = ref['panel' + idiomas[i]].items.items;

            for (var j = 0; j < items.length; j++)
            {
                if (items[j].items)
                {
                    var keyItem = items[j].items.items[0].name;

                    if (keyItem && keyItem.toUpperCase() === key.toUpperCase() + idiomas[i])
                    {
                        action(ref['panel' + idiomas[i]], items[j]);
                    }
                }
            }
        }
    },

    buildStoreEsquemas : function()
    {
        var ref = this;

        this.storeEsquemas = new Ext.data.Store(
        {
            url : '/upo/rest/esquema-metadato',
            autoLoad : true,
            restful : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'EsquemaMetadato',
                id : 'id'
            }, ['id', 'nombre']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners : {
                load : function()
                {
                    ref.loadForm();
                }
            }
        });
    },

    buildComboEsquemas : function()
    {
        var ref = this;

        this.comboEsquemas = new Ext.form.ComboBox(
        {
            valueField : 'id',
            name : 'esquemaId',
            displayField : 'nombre',
            hiddenName : 'esquemaId',
            fieldLabel : 'Esquema',
            anchor : '90%',
            triggerAction : 'all',
            store : ref.storeEsquemas,
            forceSelection : true,
            mode : 'local',
            listeners : {
                select : function(combo, record, index)
                {
                    UJI.UPO.ContenidoMetadatosUtil.requestAtributosFromEsquema(record.id, ref.panels, function()
                    {
                        ref.doLayout();
                    });
                }
            }
        });
    },

    isValid : function()
    {
        //el form da un error cuando se consulta isValid() después de borrar un campo
        var valid = true;
        var idiomas = ['CA', 'ES', 'EN'];

        for (var i = 0; i < idiomas.length; i++)
        {
            var items = ref['panel' + idiomas[i]].items.items;

            for (var j = 0; j < items.length; j++)
            {
                if (items[j].items)
                {
                    valid = valid && items[j].items.items[0].isValid();
                }
            }
        }

        return valid;
    },

    buildAddButton : function()
    {
        var ref = this;

        this.addButton = new Ext.Button(
        {
            text : 'Desar',
            disabled : true,
            handler : function()
            {
                if (ref.isValid())
                {
                    ref.form.getForm().submit(
                    {
                        url : '/upo/rest/contenido/' + ref.contenidoId + '/metadato/',
                        method : 'POST',
                        clientValidation : false,
                        success : function(form, action)
                        {
                            ref.close();
                        }
                    });
                }
                else
                {
                    UJI.UPO.Application.showMessageError("Hi ha errades o queden camps per reomplir en el formulari");
                }
            }
        });
    },

    buildCancelButton : function()
    {
        ref = this;

        this.cancelButton = new Ext.Button(
        {
            text : 'Cancel·lar',
            handler : function()
            {
                ref.close();
            }
        });
    },
})
;