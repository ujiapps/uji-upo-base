Ext.ns('UJI.UPO');

UJI.UPO.ContenidoGrid = Ext
.extend(
Ext.grid.GridPanel,
{
    autoScroll : true,
    estructuraMenu : {},
    addButton : {},
    addFolderButton : {},
    editItem : {},
    deleteItem : {},
    linkToNormalItem : {},
    duplicateContentItem : {},
    goToNormalButton : {},
    enableDragDrop : true,
    ddGroup : 'TreeNodeMapa',
    elementoDrag : 'contenidoGrid',
    tbar : [],
    administrador : false,
    editRow : function()
    {
        var grid = this;
        var rec = grid.getSelectionModel().getSelected();

        if (!rec)
        {
            Ext.Msg.alert('Información', 'És necessari seleccionar una fila per poder editar');
            return false;
        }

        UJI.Bus.publish('UJI.UPO.ContenidoGrid.Button.Edit',
        {
            contenidoId : rec.data.id,
            nodoMapaContenidoId : rec.data.nodoMapaContenidoId,
            mapaId : grid.mapaId,
            tipoContenido : rec.data.tipoContenido
        });
    },

    setMapaUrlCompleta : function(urlNueva)
    {
        this.mapaUrlCompleta = urlNueva;
    },

    setMapaNombreNodo : function(nombreNuevo)
    {
        this.mapaName = nombreNuevo;
    },

    listeners : {
        'rowdblclick' : function(grid, rowIndex, event)
        {
            this.editRow();
        },

        'rowclick' : function(grid, rowIndex, event)
        {
            if (grid.getSelectionModel().getSelections().length > 1)
            {
                this.editMenuButton.disable();
                this.deleteItem.disable();
                this.editItem.disable();
                this.linkToNormalItem.disable();
                this.duplicateContentItem.disable();
                this.goToNormalButton.disable();
                this.prioridadItem.disable();
                this.metadatosItem.disable();
                this.editMetadatosMenu.disable();

                if (!this.administrador)
                {
                    this.propiedadesMenuButton.disable();
                }
            }

            if (grid.getSelectionModel().getSelections().length == 1)
            {
                var record = grid.getSelectionModel().getSelected();

                if (this.administrador)
                {
                    this.deleteItem.enable();
                }

                this.editMenuButton.enable();
                this.editItem.enable();
                this.propiedadesMenuButton.enable();
                this.prioridadItem.enable();
                this.metadatosItem.enable();

                if (record.data.tipoContenido != UJI.UPO.TipoReferenciaContenido.NORMAL && this.administrador)
                {
                    this.linkToNormalItem.enable();
                }
                else
                {
                    this.linkToNormalItem.disable();
                }

                if (record.data.tipoContenido == UJI.UPO.TipoReferenciaContenido.NORMAL && this.administrador)
                {
                    this.duplicateContentItem.enable();
                    this.editMetadatosMenu.enable();
                }
                else
                {
                    this.duplicateContentItem.disable();
                    this.editMetadatosMenu.disable();
                }

                if (record.data.tipoContenido != UJI.UPO.TipoReferenciaContenido.NORMAL)
                {
                    this.goToNormalButton.enable();
                }
                else
                {
                    this.goToNormalButton.disable();
                }

            }
            if (grid.getSelectionModel().getSelections().length == 0)
            {
                this.editMenuButton.disable();
                this.deleteItem.disable();
                this.editItem.disable();
                this.linkToNormalItem.disable();
                this.duplicateContentItem.disable();
                this.goToNormalButton.disable();
                this.prioridadItem.disable();
                this.metadatosItem.disable();
                this.visibilidadItem.disable();
                this.propiedadesMenuButton.disable();
                this.editMetadatosMenu.disable();
            }
        },

        'render' : function(comp)
        {
            var dragZone = comp.getView().dragZone;

            dragZone.onBeforeDrag = function(data, e)
            {
                if (data.selections.length > 1)
                {
                    return false;
                }

                if (data.selections[0])
                {
                    return true;
                }
            };
        }
    },

    setAdmin : function(administrador)
    {
        this.administrador = administrador;

        if (this.administrador)
        {
            this.addButton.enable();
        }
    },

    viewConfig : {
        forceFit : true,
        getRowClass : function(record, rowIndex, rowParams, store)
        {
            if (record.data.estadoModeracion == UJI.UPO.EstadoModeracion.PENDIENTE)
            {
                return 'row-pendiente';
            }
        }
    },

    initComponent : function()
    {
        var config =
            {
                mapaId : this.mapaId,
                mapaName : this.mapaName,
                mapaUrlCompleta : this.mapaUrlCompleta
            };

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.ContenidoGrid.superclass.initComponent.call(this);

        this.expander = new UJI.UPO.RowExpander({});

        this.plugins = [this.expander];

        this.initUI();

        UJI.Bus.subscribe('UJI.UPO.ContenidoForm.Change', this);
        UJI.Bus.subscribe('UJI.UPO.NodoMapa.PropuestaContenido', this);
        UJI.Bus.subscribe('UJI.UPO.Tree.NodeDelete', this);
        UJI.Bus.subscribe('UJI.UPO.ContenidoGrid.Button.Delete', this);
        UJI.Bus.subscribe('UJI.UPO.Moderacion.Action', this);
        UJI.Bus.subscribe('UJI.UPO.Contenido.VisibilityUpdate', this);
        UJI.Bus.subscribe('UJI.UPO.ContenidoGrid.RecursoContenido', this);
        UJI.Bus.subscribe('UJI.UPO.Tree.ContentNodeChanged', this);
        UJI.Bus.subscribe('UJI.UPO.MigracionUrlGrid.Drop.Reload', this);
    },

    initUI : function()
    {
        this.buildStore();
        this.buildColumns();

        this.buildAddMenu();
        this.buildAddButton();

        this.buildEditItem();
        this.buildDeleteItem();
        this.buildLinkToNormalItem();
        this.buildDuplicateContentItem();
        this.buildGoToNormalButton();

        this.buildEditMenu();
        this.buildEditMenuButton();

        this.buildMetadatosSubmenu();

        this.buildPrioridadItem();
        this.buildVisibilidadItem();
        this.buildMetadatosItem();
        this.buildPropiedadesMenu();
        this.buildPropiedadesMenuButton();

        this.getTopToolbar().addButton(this.addButton);
        this.getTopToolbar().addButton(this.editMenuButton);
        this.getTopToolbar().add('-');
        this.getTopToolbar().addButton(this.goToNormalButton);
    },

    onMessage : function(message, params)
    {
        if (message == 'UJI.UPO.ContenidoForm.Change')
        {
            this.expander.collapseAll();
        }

        if (message == 'UJI.UPO.ContenidoForm.Change' || message == 'UJI.UPO.Tree.NodeDelete' || message == 'UJI.UPO.Moderacion.Action'
        || message == 'UJI.UPO.Contenido.VisibilityUpdate' || message == 'UJI.UPO.Tree.ContentNodeChanged')
        {
            this.store.load();
        }

        if (message == 'UJI.UPO.MigracionUrlGrid.Drop.Reload' && this.mapaId == params.mapaId)
        {
            this.store.load();
        }

        if (message == 'UJI.UPO.ContenidoGrid.Button.Delete' && this.mapaId != params.mapaId)
        {
            this.store.load();
        }

        if (message == 'UJI.UPO.NodoMapa.PropuestaContenido' || message == 'UJI.UPO.ContenidoGrid.RecursoContenido')
        {
            if (this.mapaUrlCompleta == params.urlCompleta)
            {
                this.store.load();
            }
        }
    },

    xmlToHtml : function(xml, id)
    {
        var base = xml.childNodes;
        var values = {};
        var array = [];

        for (var j = 0; j < base.length; j++)
        {
            if (base[j].nodeType == 1)
            {
                var internValues = {};
                var mimeType = Ext.DomQuery.selectValue('mimeType', base[j]);

                internValues["nombre"] = Ext.DomQuery.selectValue('nombre', base[j]);
                internValues["url"] = Ext.DomQuery.selectValue('url', base[j]);
                internValues["idioma"] = Ext.DomQuery.selectValue('idioma', base[j]);
                internValues["id"] = id;

                var clase = "contenido-recurso-otro";

                if (mimeType)
                {
                    internValues["mimeType"] = mimeType;

                    if (mimeType == "application/pdf")
                    {
                        clase = "contenido-recurso-pdf";
                    }
                    else if (mimeType.indexOf("image") > -1)
                    {
                        clase = "contenido-recurso-imagen";
                    }
                }

                internValues["clase"] = clase;

                array.push(internValues);
            }
        }

        values["values"] = array;

        var tpl = new Ext.XTemplate(
        '<tpl for="values">',
        '<div style="float: left"><div identifier="{id}" idioma="{idioma}" url="{url}" nombre="{nombre}" class="recurso-generado"><span class="{clase}">{nombre}</span><a class="recurso-generado" target="_blank" href="{url}"><img src="/upo/img/enlace_externo.png" alt="enlace externo"></img></a></div></div>',
        '</tpl>');

        return tpl.apply(values);
    },

    buildStore : function()
    {
        var baseParams = {};
        var ref = this;

        if (this.mapaId)
        {
            baseParams.mapaId = this.mapaId;
        }

        this.store = new Ext.data.Store(
        {
            url : '/upo/rest/contenido',
            baseParams : baseParams,
            restful : true,
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Contenido',
                id : 'id'
            }, [
                'id', 'nodoMapaContenidoId', 'nodoMapaId', 'adminNodoMapaContenidoNormal', 'urlNodoMapaContenidoNormal',
                'urlPath', 'estadoModeracion', 'publicable', 'visible',
                'textoNoVisible',
                {
                    name : 'htmlCA',
                    convert : function(value, record)
                    {
                        return value == 'S' ? true : false;
                    }
                },
                {
                    name : 'htmlES',
                    convert : function(value, record)
                    {
                        return value == 'S' ? true : false;
                    }
                },
                {
                    name : 'htmlEN',
                    convert : function(value, record)
                    {
                        return value == 'S' ? true : false;
                    }
                },
                {
                    name : 'contenidoIdiomaRecursosCA',
                    convert : function(value, record)
                    {
                        valor = Ext.DomQuery.selectNode('contenidoIdiomaRecursosCA', record);
                        id = Ext.DomQuery.selectValue('id', record);

                        if (valor)
                        {
                            return ref.xmlToHtml(valor, id);
                        }

                        return "";
                    }
                },
                {
                    name : 'contenidoIdiomaRecursosES',
                    convert : function(value, record)
                    {
                        valor = Ext.DomQuery.selectNode('contenidoIdiomaRecursosES', record);
                        id = Ext.DomQuery.selectValue('id', record);

                        if (valor)
                        {
                            return ref.xmlToHtml(valor, id);
                        }

                        return "";
                    }
                },
                {
                    name : 'contenidoIdiomaRecursosEN',
                    convert : function(value, record)
                    {
                        valor = Ext.DomQuery.selectNode('contenidoIdiomaRecursosEN', record);
                        id = Ext.DomQuery.selectValue('id', record);

                        if (valor)
                        {
                            return ref.xmlToHtml(valor, id);
                        }

                        return "";
                    }
                }, 'mimeTypeES', 'mimeTypeCA', 'mimeTypeEN', 'longitudCA', 'longitudES', 'longitudEN', 'tipoContenido',
                'tipoFormatoCA', 'tipoFormatoES', 'tipoFormatoEN',
                {
                    name : 'titulo',
                    convert : function(value, record)
                    {
                        var nombreCA = Ext.DomQuery.selectValue('tituloCA', record);
                        var nombreES = Ext.DomQuery.selectValue('tituloES', record);
                        var nombreEN = Ext.DomQuery.selectValue('tituloEN', record);

                        if (nombreCA)
                        {
                            return nombreCA;
                        }
                        if (nombreES)
                        {
                            return nombreES;
                        }
                        if (nombreEN)
                        {
                            return nombreEN;
                        }
                    }
                }
            ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners : {
                write : function(store, action, result, transaction, rs)
                {
                    if (action == 'destroy')
                    {
                        UJI.Bus.publish('UJI.UPO.ContenidoGrid.Button.Delete',
                        {
                            mapaId : ref.mapaId
                        });
                    }
                },

                load : function()
                {
                    ref.combierteRecursosEnDD();
                }
            }
        });
    },

    combierteRecursosEnDD : function()
    {
        var recursos = Ext.select('div.recurso-generado');

        Ext.each(recursos.elements, function(el)
        {
            new Ext.dd.DragSource(el,
            {
                ddGroup : 'TreeNodeMapa',
                elementoDrag : 'recursoContenidoGrid',
                recursoUrl : el.getAttribute("url"),
                recursoNombre : el.getAttribute("nombre"),
                contenidoId : el.getAttribute("identifier"),
                idioma : el.getAttribute("idioma")
            });
        });

    },

    buildColumns : function()
    {
        var ref = this;

        this.colModel = new Ext.grid.ColumnModel([
            this.plugins[0],
            {
                header : 'Tipus',
                dataIndex : 'tipoContenido',
                width : 15,
                align : 'center',
                renderer : function(value, p, record)
                {
                    var mimeCA = ref.getMimeType(record.data.mimeTypeCA, 'video', 'image', record.data.tipoFormatoCA, record.data.longitudCA);
                    var mimeES = ref.getMimeType(record.data.mimeTypeES, 'video', 'image', record.data.tipoFormatoES, record.data.longitudES);
                    var mimeEN = ref.getMimeType(record.data.mimeTypeEN, 'video', 'image', record.data.tipoFormatoEN, record.data.longitudEN);

                    var tipoContenido = record.data.tipoContenido;

                    var contenidosIguales = ref.tipoMimeIguales(mimeCA, mimeES, mimeEN);

                    var nombreIcono;
                    var textoIcono;

                    var mime = mimeCA || mimeES || mimeEN;

                    if (contenidosIguales)
                    {
                        if (!mime)
                        {
                            nombreIcono = UJI.UPO.IconosTiposMimeGridContenidos.empty.icono;
                            textoIcono = UJI.UPO.IconosTiposMimeGridContenidos.empty.texto;
                        }
                        else
                        {
                            if (UJI.UPO.IconosTiposMimeGridContenidos[mime])
                            {
                                nombreIcono = UJI.UPO.IconosTiposMimeGridContenidos[mime].icono;
                                textoIcono = UJI.UPO.IconosTiposMimeGridContenidos[mime].texto;
                            }
                            else
                            {
                                nombreIcono = UJI.UPO.IconosTiposMimeGridContenidos.binary.icono;
                                textoIcono = UJI.UPO.IconosTiposMimeGridContenidos.binary.texto;
                            }
                        }
                    }
                    else
                    {
                        nombreIcono = UJI.UPO.IconosTiposMimeGridContenidos.mixed.icono;
                        textoIcono = UJI.UPO.IconosTiposMimeGridContenidos.mixed.texto;
                    }

                    if (tipoContenido == UJI.UPO.TipoReferenciaContenido.LINK)
                    {
                        nombreIcono = "link_" + nombreIcono;
                        textoIcono = "link a " + textoIcono;
                    }

                    return '<img src="/upo/img/' + nombreIcono + '" alt="' + textoIcono + '" title="' + textoIcono + '" />';

                    return null;
                }
            },
            {
                header : 'Id',
                hidden : true,
                width : 20,
                align : 'left',
                dataIndex : 'nodoMapaContenidoId',
                sortable : true
            },
            {
                header : 'contenidoId',
                hidden : true,
                dataIndex : 'id',
                readOnly : true,
                sortable : true
            },
            {
                header : 'Títol',
                dataIndex : 'titulo',
                sortable : true
            },
            {
                header : 'Url',
                dataIndex : 'urlPath',
                sortable : true
            },
            {
                header : 'Publicable',
                dataIndex : 'publicable',
                width : 15,
                align : 'center',
                sortable : true,
                renderer : function(value, p, record)
                {
                    if (record.data.publicable == 'S')
                    {
                        return '<img src="/upo/img/ledlightgreen.png" alt="publicable" title="publicable"/>';
                    }
                    else
                    {
                        return '<img src="/upo/img/ledred.png" alt="no publicable" title="no publicable" />';
                    }
                }
            },
            {
                header : 'Visible',
                dataIndex : 'visible',
                width : 15,
                sortable : true,
                align : 'center',
                renderer : function(value, p, record)
                {
                    if (record.data.visible == 'S')
                    {
                        return '<img src="/upo/img/ledlightgreen.png" alt="visible" title="visible" />';
                    }
                    else
                    {
                        return '<img src="/upo/img/ledred.png" alt="no visible" title="no visible" />';
                    }
                }
            }
        ]);
    },

    getMimeType : function(mime, videoWord, imageWord, tipoFormato, longitud)
    {
        if (mime.indexOf(videoWord) != -1 && tipoFormato == UJI.UPO.TipoFormato.VIDEO_YOUTUBE.value)
        {
            return "youtube";
        }
        else if (mime.indexOf(videoWord) != -1 && tipoFormato == UJI.UPO.TipoFormato.VIDEO_VIMEO.value)
        {
            mime = "vimeo";
        }
        else if (mime.indexOf(videoWord) != -1)
        {
            mime = videoWord;
        }
        else if (longitud == 0)
        {
            mime = null;
        }
        else if (mime.indexOf(imageWord) != -1)
        {
            mime = imageWord;
        }

        return mime;
    },

    tipoMimeIguales : function(mCA, mES, mEN)
    {
        if (mCA && mES && mEN && mCA == mES && mES == mEN)
            return true;
        if (!mCA && mES && mEN && mES == mEN)
            return true;
        if (mCA && !mES && mEN && mCA == mEN)
            return true;
        if (mCA && mES && !mEN && mCA == mES)
            return true;
        if (!mCA && !mES && mEN)
            return true;
        if (!mCA && mES && !mEN)
            return true;
        if (mCA && !mES && !mEN)
            return true;
        if (!mCA && !mES && !mEN)
            return true;

        return false;
    },

    buildLinkToNormalItem : function()
    {
        var contenidoGrid = this;

        this.linkToNormalItem = new Ext.menu.Item(
        {
            text : 'Fer propi',
            iconCls : 'folder-user',
            disabled : true,
            handler : function()
            {
                var rec = contenidoGrid.getSelectionModel().getSelected();
                if (!rec)
                {
                    Ext.Msg.alert('Información', 'És necessari seleccionar una fila per poder fer propi');
                    return false;
                }

                if (rec.get("tipoContenido") == UJI.UPO.TipoReferenciaContenido.NORMAL)
                {
                    return false;
                }

                if (!contenidoGrid.administrador)
                {
                    return false;
                }

                Ext.Ajax.request(
                {
                    url : '/upo/rest/nodomapa-contenido/' + rec.data.nodoMapaContenidoId + '/actualizar-tipo',
                    method : 'PUT',
                    success : function(response)
                    {
                        UJI.Bus.publish('UJI.UPO.ContenidoForm.Change',
                        {
                            mapaId : contenidoGrid.mapaId
                        });
                    }
                });
            }
        });
    },

    buildDuplicateContentItem : function()
    {
        var contenidoGrid = this;

        this.duplicateContentItem = new Ext.menu.Item(
        {
            text : 'Duplicar',
            iconCls : 'application-double',
            disabled : true,
            handler : function()
            {
                var rec = contenidoGrid.getSelectionModel().getSelected();
                if (!rec)
                {
                    Ext.Msg.alert('Información', 'És necessari seleccionar una fila per poder fer propi');
                    return false;
                }

                if (rec.get("tipoContenido") != UJI.UPO.TipoReferenciaContenido.NORMAL)
                {
                    return false;
                }

                if (!contenidoGrid.administrador)
                {
                    return false;
                }

                Ext.Ajax.request(
                {
                    url : '/upo/rest/nodomapa-contenido/' + rec.data.nodoMapaContenidoId + '/duplicar',
                    method : 'PUT',
                    success : function(response)
                    {
                        UJI.Bus.publish('UJI.UPO.ContenidoForm.Change',
                        {
                            mapaId : contenidoGrid.mapaId
                        });
                    }
                });

            }
        });
    },

    buildGoToNormalButton : function()
    {
        var contenidoGrid = this;

        this.goToNormalButton = new Ext.Button(
        {
            text : "Anar a l'original",
            iconCls : 'application-go',
            disabled : true,
            handler : function()
            {
                var rec = contenidoGrid.getSelectionModel().getSelected();
                if (!rec)
                {
                    Ext.Msg.alert('Información', 'És necessari seleccionar una fila per poder fer propi');
                    return false;
                }

                if (rec.get("tipoContenido") == UJI.UPO.TipoReferenciaContenido.NORMAL)
                {
                    return false;
                }

                var urlNodoNormal = rec.get("urlNodoMapaContenidoNormal");

                contenidoGrid.treePanel.selectPath("/root/first" + urlNodoNormal.substring(0, urlNodoNormal.length - 1), "urlPath", function(success, node)
                {
                    if (success)
                    {
                        var result = UJI.UPO.Application.createNewNodeTab(contenidoGrid.treePanel, node, node.id, node.attributes.urlCompleta);
                        var tabPanel = UJI.UPO.Application.getTabPanel();

                        tabPanel.addTab(result.nodoTab);
                    }
                });
            }
        });
    },

    buildPrioridadItem : function()
    {
        var contenidoGrid = this;
        this.prioridadItem = new Ext.menu.Item(
        {
            text : 'Prioritats',
            handler : function()
            {
                var rec = contenidoGrid.getSelectionModel().getSelected();
                if (!rec)
                {
                    Ext.Msg.alert('Información', 'És necessari seleccionar una fila per poder prioritzar');
                    return false;
                }

                prioridadWindow = new UJI.UPO.PrioridadWindow(
                {
                    mapaId : contenidoGrid.mapaId,
                    contenidoId : rec.data.id,
                    tipoContenido : rec.get("tipoContenido"),
                    administrador : contenidoGrid.administrador
                });

                prioridadWindow.show();
            }
        });
    },

    buildVisibilidadItem : function()
    {
        var ref = this;
        this.visibilidadItem = new Ext.menu.Item(
        {
            text : 'Visibilidad',
            handler : function()
            {
                var window = null;
                var contenidoIdList = [];
                var numRegSelected = ref.getSelectionModel().getSelections().length;

                if (numRegSelected == 0)
                {
                    Ext.Msg.alert('Información', 'És necessari seleccionar una fila per poder visualitzar');
                    return false;
                }

                if (numRegSelected == 1)
                {
                    var rec = ref.getSelectionModel().getSelected();

                    window = new UJI.UPO.VisibilidadContenidoWindow(
                    {
                        visibleActivo : (rec.get("visible") == 'S') ? true : false,
                        textoNoVisible : rec.get("textoNoVisible")
                    });

                    window.setAdmin(ref.administrador && rec.get("tipoContenido") == UJI.UPO.TipoReferenciaContenido.NORMAL);
                }

                if (numRegSelected > 1)
                {
                    window = new UJI.UPO.VisibilidadContenidoWindow(
                    {
                        visibleActivo : true
                    });

                    window.setAdmin(ref.administrador);
                }

                for (var it = 0; it < numRegSelected; it++)
                {
                    var rec = ref.getSelectionModel().getSelections()[it];
                    contenidoIdList.push(rec.get("id"));
                }

                window.show();

                window.on("setVisibility", function(params)
                {
                    Ext.Ajax.request(
                    {
                        url : '/upo/rest/contenido/visibilidad',
                        params : {
                            visible : params.visible,
                            textoNoVisible : params.textoNoVisible,
                            contenidoId : contenidoIdList,
                            mapaId : ref.mapaId
                        },
                        success : function()
                        {
                            UJI.Bus.publish("UJI.UPO.Contenido.VisibilityUpdate");
                            window.close();
                        },
                        failure : function(result, request)
                        {
                            Ext.Msg.alert('Error', "S'ha produït una errada");
                        },
                        method : 'PUT'
                    });
                });
            }
        });
    },

    buildMetadatosItem : function()
    {
        var ref = this;
        this.metadatosItem = new Ext.menu.Item(
        {
            text : 'Metadades',
            handler : function()
            {
                var rec = ref.getSelectionModel().getSelected();
                var numRegSelected = ref.getSelectionModel().getSelections().length;

                if (numRegSelected == 0)
                {
                    Ext.Msg.alert('Información', 'És necessari seleccionar una fila per poder gestionar les metadades');
                    return false;
                }

                var metadatosWindow = new UJI.UPO.ContenidoMetadatosWindow(
                {
                    mapaId : ref.mapaId,
                    contenidoId : rec.data.id,
                    tipoContenido : rec.get("tipoContenido"),
                    administrador : ref.administrador
                });

                metadatosWindow.show();
            }
        });
    },

    buildMetadatosSubmenu : function(addMenu)
    {
        var contenidoGrid = this;

        Ext.Ajax.request(
        {
            url : '/upo/rest/esquema-metadato/',
            success : function(response)
            {
                var addItems = [];
                var editItems = [];

                Ext.each(Ext.DomQuery.jsSelect('EsquemaMetadato', response.responseXML), function(xmlItem)
                {
                    var id = Ext.DomQuery.selectValue("id", xmlItem);
                    var nombre = Ext.DomQuery.selectValue("nombre", xmlItem);

                    addItems.push(
                    {
                        text : nombre,
                        handler : function()
                        {
                            UJI.Bus.publish('UJI.UPO.ContenidoGrid.Button.Add',
                            {
                                mapaId : contenidoGrid.mapaId,
                                tipo : UJI.UPO.TipoFormato.PAGINA.value,
                                esquemaId : id
                            });
                        }
                    })

                    editItems.push(
                    {
                        text : nombre,
                        handler : function()
                        {
                            var contenidoId = contenidoGrid.getSelectionModel().getSelected().data.id;

                            Ext.Msg.confirm('Canvi tipus', 'Esteu segur/a de voler canviar el tipus d\'objecte ? (podeu perdre les dades si ja tenia un tipus assignat)', function(btn, text)
                            {
                                if (btn == 'yes')
                                {
                                    Ext.Ajax.request(
                                    {
                                        url : '/upo/rest/contenido/' + contenidoId + '/metadato/' + id,
                                        method : 'PUT',
                                        success : function()
                                        {
                                            UJI.Bus.publish('UJI.UPO.ContenidoForm.Change',
                                            {
                                                mapaId : contenidoGrid.mapaId
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    })
                });

                contenidoGrid.addMenu.add(new Ext.menu.Item(
                {
                    text : 'Altres tipus',
                    iconCls : 'page-white-gear',
                    menu : addItems
                }));

                editItems.push('-');

                editItems.push(
                {
                    text : 'Sense tipus',
                    handler : function()
                    {
                        var contenidoId = contenidoGrid.getSelectionModel().getSelected().data.id;

                        Ext.Msg.confirm('Sense tipus', 'Esteu segur/a de eliminar el tipus d\'objecte ? (podeu perdre les dades si ja tenia un tipus assignat)', function(btn, text)
                        {
                            if (btn == 'yes')
                            {
                                Ext.Ajax.request(
                                {
                                    url : '/upo/rest/contenido/' + contenidoId + '/metadato/',
                                    method : 'DELETE',
                                    success : function()
                                    {
                                        UJI.Bus.publish('UJI.UPO.ContenidoForm.Change',
                                        {
                                            mapaId : contenidoGrid.mapaId
                                        });
                                    }
                                });
                            }
                        });
                    }
                })

                contenidoGrid.editMetadatosMenu = new Ext.menu.Item(
                {
                    text : 'Tipus',
                    iconCls : 'page-white-gear',
                    menu : editItems
                })

                contenidoGrid.editMenu.add(contenidoGrid.editMetadatosMenu);
            }
        });
    },

    buildAddMenu : function()
    {
        var contenidoGrid = this;

        var items = [];
        var formatos = UJI.UPO.TipoFormato.getData();

        Ext.each(formatos, function(formato)
        {
            items.push(
            {
                text : formato.name,
                icon : '/upo/img/' + formato.icon,
                handler : function()
                {
                    if (formato.value === 'BINARIO')
                    {
                        var window = new Ext.ux.uji.FileUploader(
                        {
                            url : '/upo/rest/mapa/' + contenidoGrid.mapaId + '/fichero'
                        });

                        window.show();

                        window.on("close", function()
                        {
                            contenidoGrid.store.load();
                        });
                    } else
                    {
                        UJI.Bus.publish('UJI.UPO.ContenidoGrid.Button.Add',
                        {
                            mapaId : contenidoGrid.mapaId,
                            tipo : formato.value
                        });
                    }
                }
            });
        });

        this.addMenu = new Ext.menu.Menu(
        {
            style : {
                overflow : 'visible'
            },
            items : items
        });
    },

    buildEditMenu : function()
    {
        var contenidoGrid = this;

        this.editMenu = new Ext.menu.Menu(
        {
            style : {
                overflow : 'visible'
            }
        });

        this.editMenu.add(this.editItem);
        this.editMenu.add(this.deleteItem);
        this.editMenu.add(this.duplicateContentItem);
        this.editMenu.add(this.linkToNormalItem);
    },

    buildPropiedadesMenu : function()
    {
        var contenidoGrid = this;

        this.propiedadesMenu = new Ext.menu.Menu(
        {
            style : {
                overflow : 'visible'
            }
        });

        this.propiedadesMenu.add(this.prioridadItem);
        this.propiedadesMenu.add(this.visibilidadItem);
        this.propiedadesMenu.add(this.metadatosItem);
    },

    buildPropiedadesMenuButton : function()
    {
        var contenidoGrid = this;

        this.propiedadesMenuButton = new Ext.Button(
        {
            text : 'Propietats',
            iconCls : 'application-key',
            disabled : true,
            menu : contenidoGrid.propiedadesMenu
        });
    },

    buildAddButton : function()
    {
        var contenidoGrid = this;
        this.addButton = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            disabled : true,
            menu : contenidoGrid.addMenu
        });
    },

    buildEditMenuButton : function()
    {
        var contenidoGrid = this;

        this.editMenuButton = new Ext.Button(
        {
            text : 'Editar',
            iconCls : 'application-edit',
            disabled : true,
            menu : contenidoGrid.editMenu
        });
    },

    buildEditItem : function()
    {
        var ref = this;

        this.editItem = new Ext.menu.Item({
            text : 'Editar',
            iconCls : 'application-edit',
            disabled : true,
            handler : function()
            {
                ref.editRow();
            }
        });
    },

    buildDeleteItem : function()
    {
        var ref = this;

        this.deleteItem = new Ext.menu.Item(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            disabled : true,
            handler : function()
            {
                var rec = ref.getSelectionModel().getSelected();

                if (!rec)
                {
                    Ext.Msg.alert('Informació', 'És necessari seleccionar una fila per poder esborrar');
                    return false;
                }

                Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                {
                    if (btn == 'yes')
                    {
                        ref.store.remove(rec);
                    }
                });
            }
        });
    }
});