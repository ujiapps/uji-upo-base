Ext.ns('UJI.UPO');

UJI.UPO.MigracionUrl = Ext.extend(Ext.grid.GridPanel,
{
    title : 'Migració per Url',
    frame : true,
    flex : 1,
    closable : true,
    viewConfig : {
        forceFit : true
    },
    enableDragDrop : true,
    ddGroup : 'TreeNodeMapa',
    elementoDrag : 'migracionUrl',
    bbar : [],
    tbar : [],
    initComponent : function()
    {
        var config = {};

        this.selModel = new Ext.grid.RowSelectionModel(
        {
            singleSelect : true
        });

        this.colModel = new Ext.grid.ColumnModel(
        {
            columns : [
                {
                    header : 'URL',
                    width : 100,
                    dataIndex : 'url'
                },
                {
                    header : 'tipo',
                    dataIndex : 'tipo',
                    hidden: true
                }
            ]
        }),

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.MigracionUrl.superclass.initComponent.call(this);

        this.initUI();

    },
    initUI : function()
    {
        this.buildStore();

        var ref = this;

        var bt = this.getBottomToolbar();
        tb = this.getTopToolbar();

        bt.add([
            new Ext.PagingToolbar(
            {
                pageSize : 25,
                store : ref.store,
                displayInfo : true,
                displayMsg : 'Visualizando {0} - {1} de {2}',
                emptyMsg : 'No hay registros'
            })
        ]);

        tb.add([
            new Ext.ux.form.SearchField(
            {
                emptyText : 'Recerca...',
                store : ref.store,
                width : 180
            })
        ]);
    },
    buildStore : function()
    {
        this.store = new Ext.data.Store(
        {
            autoLoad : true,
            autoLoad : {
                params : {
                    start : 0,
                    limit : 25
                }
            },
            restful : true,
            url : '/upo/rest/migracion/listaurl',
            reader : new Ext.data.XmlReader(
            {
                record : 'Migracion',
                id : 'url',
                totalProperty : 'pagination>numResults'
            }, ['url', 'tipo']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    }
});