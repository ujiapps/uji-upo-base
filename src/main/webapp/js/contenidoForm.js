Ext.ns('UJI.UPO');

UJI.UPO.ContenidoForm = Ext.extend(Ext.Panel,
{
    layout : 'vbox',
    region : 'center',
    layoutConfig : {
        align : 'stretch'
    },
    panelTabs : {},
    comboOrigenesInformacion : {},
    binaryFile : {},
    storeOrigenesInformacion : {},
    editor : {},
    form : {},
    saveButton : {},
    saveAndContinueButton : {},
    cancelButton : {},
    datePanel : {},
    imgFlag : {},
    imgGreen : '/upo/img/ledlightgreen.png',
    imgRed : '/upo/img/ledred.png',
    urlForm : '',
    paramsForm : {},
    administrador : false,
    tipoContenidoNormal : true,
    avisoCambiosSinGuardar : false,

    setMapaUrlCompleta : function(urlNueva)
    {
        this.mapaUrlCompleta = urlNueva;
    },

    initComponent : function()
    {
        var config =
            {
                mapaId : this.mapaId,
                mapaUrlCompleta : this.mapaUrlCompleta,
                mapaName : this.mapaName
            };

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.ContenidoForm.superclass.initComponent.call(this);

        this.initUI();
        this.add(this.form);

        this.refresherTask =
        {
            run : function()
            {
                this.autoSaveRequest(this.htmlEditorCA.id, "CA");
                this.autoSaveRequest(this.htmlEditorES.id, "ES");
                this.autoSaveRequest(this.htmlEditorEN.id, "EN");
            },
            scope : this,
            interval : 15000
        };

        UJI.Bus.subscribe('UJI.UPO.ContenidoGrid.Button.Add', this);
        UJI.Bus.subscribe('UJI.UPO.ContenidoGrid.Button.Edit', this);
        UJI.Bus.subscribe('UJI.UPO.MigracionUrlGrid.Drop', this);
        UJI.Bus.subscribe('UJI.UPO.Tree.NodeDelete', this);
    },

    startAutoguardado : function()
    {
        this.buildPeriodinalRequestForAutoSave();
    },

    onMessage : function(message, params)
    {
        if (params.mapaId == this.mapaId)
        {
            var ref = this;
            ref.prioridadPanel.reset();

            if (message == 'UJI.UPO.ContenidoGrid.Button.Add')
            {
                ref.storeOrigenesInformacion.un('load', this.loadForm, this);
                ref.deleteFields();
                ref.enableFormFields(true);
                ref.loadEsquemaMetadatos(params.esquemaId);

                setTimeout(function()
                {
                    ref.startAutoguardado();
                }, 5000);

                ref.requestAutoguardados();

                Ext.Ajax.request(
                {
                    url : '/upo/rest/mapa/' + this.mapaId + "/default-values-new-content",
                    method : 'GET',
                    success : function(r)
                    {
                        var otrosAutores = Ext.DomQuery.selectValue('otrosAutores', r.responseXML);
                        var origenInformacion = Ext.DomQuery.selectValue('origenId', r.responseXML);
                        var personaResponsable = Ext.DomQuery.selectValue('personaResponsable', r.responseXML);
                        var url = ref.mapaName + (params.tipo === "PAGINA" ? '.html' : '.');

                        ref.form.getForm().findField("otrosAutores").setValue(otrosAutores);
                        ref.form.getForm().findField("personaResponsable").setValue(personaResponsable);
                        ref.form.getForm().findField("urlPath").setValue(url);

                        ref.storeOrigenesInformacion.on("load", function()
                        {
                            ref.comboOrigenesInformacion.setValue(origenInformacion);
                        });

                        ref.storeOrigenesInformacion.load();

                        ref.tipoFormatoChangeActions("CA", params.tipo);
                        ref.setImage("CA", '');
                        ref.tipoFormatoChangeActions("ES", params.tipo);
                        ref.setImage("ES", '');
                        ref.tipoFormatoChangeActions("EN", params.tipo);
                        ref.setImage("EN", '');

                        ref.form.body.scrollTo('top', 0);
                    }
                });
            }

            if (message == 'UJI.UPO.MigracionUrlGrid.Drop')
            {
                this.storeOrigenesInformacion.on('load', this.loadForm, this);

                this.urlForm = '/upo/rest/migracion/url/texto/';
                this.paramsForm =
                {
                    url : params.url,
                    migracionGrid : params.migracionGrid,
                    urlNode : params.mapaUrl
                };
            }

            if (message == 'UJI.UPO.ContenidoGrid.Button.Edit')
            {
                this.storeOrigenesInformacion.on('load', this.loadForm, this);

                this.urlForm = '/upo/rest/contenido/' + params.contenidoId;

                this.paramsForm =
                {
                    nodoMapaContenidoId : params.nodoMapaContenidoId
                };

                if (ref.administrador && params.tipoContenido == UJI.UPO.TipoReferenciaContenido.NORMAL)
                {
                    this.tipoContenidoNormal = true;
                    this.enableFormFields(true);
                }
                else
                {
                    this.tipoContenidoNormal = false;
                    this.enableFormFields(false);

                    if (ref.administrador)
                    {
                        ref.enableElement(ref.form.getForm().findField("orden"), 'enable');
                    }
                }
            }

            ref.storeOrigenesInformacion.load()
        }

        if (message == 'UJI.UPO.Tree.NodeDelete')
        {
            var form = this.form.getForm();

            if (form)
            {
                var urlCompletaNodoMapa = form.findField("urlCompletaNodoMapa").getValue();

                if (urlCompletaNodoMapa && urlCompletaNodoMapa.indexOf(params.mapaUrlCompleta) == 0)
                {
                    form.reset();

                    UJI.Bus.publish('UJI.UPO.ContenidoForm.Button.Cancel',
                    {
                        mapaId : this.mapaId
                    });
                }
            }
        }
    },

    initUI : function()
    {
        var ref = this;

        this.buildStoreOrigenesInformacion();
        this.buildComboOrigenesInformacion();
        this.buildSaveButton();
        this.buildSaveAndContinueButton();
        this.buildCancelButton();

        this.fieldCodigo = new Ext.form.TextField(
        {
            name : 'id',
            fieldLabel : 'Código',
            hidden : true
        });

        var fieldCodigo = this.fieldCodigo;

        var fieldBooleanContenidoConAutoguardado = new Ext.form.TextField(
        {
            name : 'contenidoSustituidoPorAutoguardado',
            fieldLabel : 'Té Autoguardat',
            hidden : true
        });

        var fieldOtrasPersonasEditandoContenido = new Ext.form.TextField(
        {
            name : 'otrasPersonasEditandoContenido',
            fieldLabel : 'Altres persones editant el contingut',
            hidden : true
        });

        this.nombreFicheroCA = new Ext.form.TextField(
        {
            name : 'nombreFicheroCA',
            readOnly : true,
            width : 300,
            fieldClass : 'x-item-disabled',
            style : {
                position : 'relative'
            }
        });

        this.nombreFicheroES = new Ext.form.TextField(
        {
            name : 'nombreFicheroES',
            readOnly : true,
            width : 300,
            fieldClass : 'x-item-disabled',
            style : {
                position : 'relative'
            }

        });

        this.nombreFicheroEN = new Ext.form.TextField(
        {
            name : 'nombreFicheroEN',
            readOnly : true,
            width : 300,
            fieldClass : 'x-item-disabled',
            style : {
                position : 'relative'
            }

        });

        this.htmlEditorCA = this.buildHtmlEditor('CA');
        this.htmlEditorEN = this.buildHtmlEditor('EN');
        this.htmlEditorES = this.buildHtmlEditor('ES');

        this.imageCA = new Ext.Panel(
        {
            layout : 'form',
            flex : 1,
            html : '',
            padding : 5
        });

        this.imageES = new Ext.Panel(
        {
            layout : 'form',
            flex : 1,
            html : '',
            padding : 5
        });

        this.imageEN = new Ext.Panel(
        {
            layout : 'form',
            flex : 1,
            html : '',
            padding : 5
        });

        this.binaryFileDeleteCA = this.buildBinaryFileDelete('CA');
        this.binaryFileDeleteES = this.buildBinaryFileDelete('ES');
        this.binaryFileDeleteEN = this.buildBinaryFileDelete('EN');
        this.binaryFileDownloadCA = this.buildBinaryFileDownload('CA');
        this.binaryFileDownloadES = this.buildBinaryFileDownload('ES');
        this.binaryFileDownloadEN = this.buildBinaryFileDownload('EN');

        this.tipoFormatoPanelButtonsCA = this.buildTipoFormatoPanelButtons('CA');
        this.tipoFormatoPanelButtonsES = this.buildTipoFormatoPanelButtons('ES');
        this.tipoFormatoPanelButtonsEN = this.buildTipoFormatoPanelButtons('EN');

        var configMetadatosPanel = {
            width : '620',
            allBlanks : true,
            showKeyHowLabel : true,
            keyPrefix : '$'
        }

        configMetadatosPanel.idioma = 'CA';
        configMetadatosPanel.name = 'metadatosPanelCA';
        this.metadatosPanelCA = new UJI.UPO.ContenidoMetadatosIdiomaPanel(configMetadatosPanel);

        configMetadatosPanel.idioma = 'ES';
        configMetadatosPanel.name = 'metadatosPanelES';
        this.metadatosPanelES = new UJI.UPO.ContenidoMetadatosIdiomaPanel(configMetadatosPanel);

        configMetadatosPanel.idioma = 'EN';
        configMetadatosPanel.name = 'metadatosPanelEN';
        this.metadatosPanelEN = new UJI.UPO.ContenidoMetadatosIdiomaPanel(configMetadatosPanel);

        this.metadatosPanels = [this.metadatosPanelCA, this.metadatosPanelES, this.metadatosPanelEN];

        this.botonCopiarCA = this.buildCopyValuesButton("CA");
        this.botonCopiarES = this.buildCopyValuesButton("ES");
        this.botonCopiarEN = this.buildCopyValuesButton("EN");

        this.traducirDesdeMenuCA = this.buildTranslateFromMenu("CA");
        this.traducirDesdeMenuES = this.buildTranslateFromMenu("ES");
        this.traducirDesdeMenuEN = this.buildTranslateFromMenu("EN");

        this.resumenPanelCA = this.buildResumenPanel("CA");
        this.resumenPanelES = this.buildResumenPanel("ES");
        this.resumenPanelEN = this.buildResumenPanel("EN");

        this.panelTabs = new Ext.TabPanel(
        {
            border : false,
            activeTab : 0,
            autoHeight : true,
            deferredRender : false,
            defaults : {
                autoHeight : true
            },
            items : [
                {
                    xtype : 'panel',
                    title : 'Català',
                    frame : true,
                    items : [
                        {
                            xtype : 'textfield',
                            name : 'idDescargaCA',
                            hidden : true
                        },
                        {
                            xtype : 'panel',
                            layout : 'hbox',
                            items : [
                                {
                                    xtype : 'panel',
                                    layout : 'form',
                                    flex : 3,
                                    items : [
                                        {
                                            xtype : 'textfield',
                                            name : 'tituloLargoCA',
                                            fieldLabel : 'Títol llarg',
                                            width : '620px'
                                        }, new Ext.form.CompositeField(
                                        {
                                            items : [
                                                {
                                                    xtype : 'textfield',
                                                    name : 'tituloCA',
                                                    fieldLabel : 'Títol',
                                                    width : '620px'
                                                }, this.botonCopiarCA
                                            ]
                                        }),
                                        {
                                            xtype : 'textfield',
                                            name : 'subtituloCA',
                                            fieldLabel : 'Subtítol',
                                            width : '620px'
                                        }, this.resumenPanelCA,
                                        {
                                            xtype : 'textfield',
                                            name : 'enlaceDestinoCA',
                                            fieldLabel : 'Enllaç de destí',
                                            width : '620px'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype : 'panel',
                            layout : 'form',
                            items : [
                                this.tipoFormatoPanelButtonsCA,
                                {
                                    xtype : 'textfield',
                                    name : 'tipoFormatoCA',
                                    hidden : true
                                }, this.htmlEditorCA,
                                {
                                    xtype : 'panel',
                                    layout : 'form',
                                    name : 'binarioPanelCA',
                                    border : false,
                                    items : [
                                        this.buildBinaryFile('CA'), new Ext.form.CompositeField(
                                        {
                                            name : 'compositeCA',
                                            anchor : '100%',
                                            items : [
                                                this.nombreFicheroCA, this.binaryFileDownloadCA, this.binaryFileDeleteCA
                                            ]
                                        }),
                                        {
                                            xtype : 'checkbox',
                                            name : 'repetirFicheroCA',
                                            boxLabel : 'substituir aquest fitxer en els altres idiomes',
                                            inputValue : true
                                        }, this.imageCA
                                    ]
                                },
                                {
                                    xtype : 'panel',
                                    layout : 'form',
                                    name : 'videoUJIPanelCA',
                                    border : false,
                                    items : [
                                        {
                                            xtype : 'textfield',
                                            name : '_videoMP4CA',
                                            fieldLabel : 'Fitxer MP4',
                                            width : '620px'
                                        },
                                        {
                                            xtype : 'textfield',
                                            name : '_videoOGVCA',
                                            fieldLabel : 'Fitxer OGV',
                                            width : '620px'
                                        },
                                        {
                                            xtype : 'textfield',
                                            name : '_videoPosterTextoAlternativoCA',
                                            fieldLabel : 'Text alt. Poster',
                                            width : '620px'
                                        },
                                        {
                                            xtype : 'textfield',
                                            name : '_videoPosterImagenCA',
                                            fieldLabel : 'Imatge Poster',
                                            width : '620px'
                                        },
                                        {
                                            xtype : 'textfield',
                                            name : '_videoSubtitulosCA',
                                            fieldLabel : 'Subtítols',
                                            width : '620px'
                                        }
                                    ]
                                },
                                {
                                    xtype : 'panel',
                                    layout : 'form',
                                    name : 'videoOTROSPanelCA',
                                    border : false,
                                    items : [
                                        {
                                            xtype : 'textfield',
                                            name : '_videoCA',
                                            fieldLabel : 'URL vídeo',
                                            width : '620px'
                                        }
                                    ]
                                }, this.metadatosPanelCA
                            ]
                        }
                    ]
                },
                {
                    xtype : 'panel',
                    title : 'Espanyol',
                    frame : true,
                    items : [
                        {
                            xtype : 'textfield',
                            name : 'idDescargaES',
                            hidden : true
                        },
                        {
                            xtype : 'panel',
                            layout : 'hbox',
                            items : [
                                {
                                    xtype : 'panel',
                                    layout : 'form',
                                    flex : 3,
                                    items : [
                                        {
                                            xtype : 'textfield',
                                            name : 'tituloLargoES',
                                            fieldLabel : 'Títol llarg',
                                            width : '620px'
                                        }, new Ext.form.CompositeField(
                                        {
                                            items : [
                                                {
                                                    xtype : 'textfield',
                                                    name : 'tituloES',
                                                    fieldLabel : 'Títol',
                                                    width : '620px'
                                                }, this.botonCopiarES
                                            ]
                                        }),
                                        {
                                            xtype : 'textfield',
                                            name : 'subtituloES',
                                            fieldLabel : 'Subtítol',
                                            width : '620px'
                                        }, this.resumenPanelES,
                                        {
                                            xtype : 'textfield',
                                            name : 'enlaceDestinoES',
                                            fieldLabel : 'Enllaç de destí',
                                            width : '620px'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype : 'panel',
                            layout : 'form',
                            items : [
                                this.tipoFormatoPanelButtonsES,
                                {
                                    xtype : 'textfield',
                                    name : 'tipoFormatoES',
                                    hidden : true
                                }, this.htmlEditorES,
                                {
                                    xtype : 'panel',
                                    layout : 'form',
                                    name : 'binarioPanelES',
                                    border : false,
                                    items : [
                                        this.buildBinaryFile('ES'), new Ext.form.CompositeField(
                                        {
                                            name : 'compositeES',
                                            anchor : '100%',
                                            items : [
                                                this.nombreFicheroES, this.binaryFileDownloadES, this.binaryFileDeleteES
                                            ]
                                        }),
                                        {
                                            xtype : 'checkbox',
                                            name : 'repetirFicheroES',
                                            boxLabel : 'substituir aquest fitxer en els altres idiomes',
                                            inputValue : true
                                        }, this.imageES
                                    ]
                                },
                                {
                                    xtype : 'panel',
                                    layout : 'form',
                                    name : 'videoUJIPanelES',
                                    border : false,
                                    items : [
                                        {
                                            xtype : 'textfield',
                                            name : '_videoMP4ES',
                                            fieldLabel : 'Fitxer MP4',
                                            width : '620px'
                                        },
                                        {
                                            xtype : 'textfield',
                                            name : '_videoOGVES',
                                            fieldLabel : 'Fitxer OGV',
                                            width : '620px'
                                        },
                                        {
                                            xtype : 'textfield',
                                            name : '_videoPosterTextoAlternativoES',
                                            fieldLabel : 'Text alt. Poster',
                                            width : '620px'
                                        },
                                        {
                                            xtype : 'textfield',
                                            name : '_videoPosterImagenES',
                                            fieldLabel : 'Poster',
                                            width : '620px'
                                        },
                                        {
                                            xtype : 'textfield',
                                            name : '_videoSubtitulosES',
                                            fieldLabel : 'Subtítols',
                                            width : '620px'
                                        }
                                    ]
                                },
                                {
                                    xtype : 'panel',
                                    layout : 'form',
                                    name : 'videoOTROSPanelES',
                                    border : false,
                                    items : [
                                        {
                                            xtype : 'textfield',
                                            name : '_videoES',
                                            fieldLabel : 'URL vídeo',
                                            width : '620px'
                                        }
                                    ]
                                }, this.metadatosPanelES
                            ]
                        }
                    ]
                },
                {
                    xtype : 'panel',
                    title : 'Anglès',
                    frame : true,
                    items : [
                        {
                            xtype : 'textfield',
                            name : 'idDescargaEN',
                            hidden : true
                        },
                        {
                            xtype : 'panel',
                            layout : 'hbox',
                            items : [
                                {
                                    xtype : 'panel',
                                    layout : 'form',
                                    flex : 3,
                                    items : [
                                        {
                                            xtype : 'textfield',
                                            name : 'tituloLargoEN',
                                            fieldLabel : 'Títol llarg',
                                            width : '620px'
                                        }, new Ext.form.CompositeField(
                                        {
                                            items : [
                                                {
                                                    xtype : 'textfield',
                                                    name : 'tituloEN',
                                                    fieldLabel : 'Títol',
                                                    width : '620px'
                                                }, this.botonCopiarEN
                                            ]
                                        }),
                                        {
                                            xtype : 'textfield',
                                            name : 'subtituloEN',
                                            fieldLabel : 'Subtítol',
                                            width : '620px'
                                        }, this.resumenPanelEN,
                                        {
                                            xtype : 'textfield',
                                            name : 'enlaceDestinoEN',
                                            fieldLabel : 'Enllaç de destí',
                                            width : '620px'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype : 'panel',
                            layout : 'form',
                            items : [
                                this.tipoFormatoPanelButtonsEN,
                                {
                                    xtype : 'textfield',
                                    name : 'tipoFormatoEN',
                                    hidden : true
                                }, this.htmlEditorEN,
                                {
                                    xtype : 'panel',
                                    layout : 'form',
                                    name : 'binarioPanelEN',
                                    border : false,
                                    items : [
                                        this.buildBinaryFile('EN'), new Ext.form.CompositeField(
                                        {
                                            name : 'compositeEN',
                                            anchor : '100%',
                                            items : [
                                                this.nombreFicheroEN, this.binaryFileDownloadEN, this.binaryFileDeleteEN
                                            ]
                                        }),
                                        {
                                            xtype : 'checkbox',
                                            name : 'repetirFicheroEN',
                                            boxLabel : 'substituir aquest fitxer en els altres idiomes',
                                            inputValue : true
                                        }, this.imageEN
                                    ]
                                },
                                {
                                    xtype : 'panel',
                                    layout : 'form',
                                    name : 'videoUJIPanelEN',
                                    border : false,
                                    items : [
                                        {
                                            xtype : 'textfield',
                                            name : '_videoMP4EN',
                                            fieldLabel : 'Fitxer MP4',
                                            width : '620px'
                                        },
                                        {
                                            xtype : 'textfield',
                                            name : '_videoOGVEN',
                                            fieldLabel : 'Fitxer OGV',
                                            width : '620px'
                                        },
                                        {
                                            xtype : 'textfield',
                                            name : '_videoPosterTextoAlternativoEN',
                                            fieldLabel : 'Text alt. Poster',
                                            width : '620px'
                                        },
                                        {
                                            xtype : 'textfield',
                                            name : '_videoPosterImagenEN',
                                            fieldLabel : 'Poster',
                                            width : '620px'
                                        },
                                        {
                                            xtype : 'textfield',
                                            name : '_videoSubtitulosEN',
                                            fieldLabel : 'Subtítols',
                                            width : '620px'
                                        }
                                    ]
                                },
                                {
                                    xtype : 'panel',
                                    layout : 'form',
                                    name : 'videoOTROSPanelEN',
                                    border : false,
                                    items : [
                                        {
                                            xtype : 'textfield',
                                            name : '_videoEN',
                                            fieldLabel : 'URL vídeo',
                                            width : '620px'
                                        }
                                    ]
                                }, this.metadatosPanelEN
                            ]
                        }
                    ]
                }
            ]
        });

        this.datePanel = new UJI.UPO.ux.DatePickerPanel(
        {
            fieldLabel : 'Vigència'
        });

        this.visibilidadPanel = new UJI.UPO.VisibilidadContenidoPanel({});

        this.prioridadPanel = new UJI.UPO.PrioridadPanel({
            height : 150,
            administrador : true,
            tipoContenido : UJI.UPO.TipoReferenciaContenido.NORMAL,
            autoSave : false
        });

        this.imgFlag = new Ext.Component(
        {
            xtype : 'box',
            fieldLabel : 'Publicable',
            autoEl : {
                tag : 'img',
                src : ref.imgRed,
                alt : 'No publicable',
                title : 'No publicable'
            }
        });

        this.form = new Ext.form.FormPanel(
        {
            defaultType : 'textfield',
            fileUpload : true,
            autoScroll : true,
            flex : 1,
            frame : false,
            border : false,
            fieldDefaults : {
                labelAlign : 'top',
                msgTarget : 'side'
            },
            listeners : {
                actioncomplete : function(form, action)
                {
                    if (action.type == 'load')
                    {
                        ref.form.body.scrollTo('top', 0);

                        setTimeout(function()
                        {
                            ref.startAutoguardado();
                        }, 5000);

                        if (fieldOtrasPersonasEditandoContenido.getValue())
                        {
                            Ext.MessageBox.show(
                            {
                                title : 'Informació',
                                msg : 'És possible que les següents persones estiguen editant aquest contingut: <b>' + fieldOtrasPersonasEditandoContenido.getValue() + '</b>',
                                buttons : Ext.MessageBox.OK,
                                icon : Ext.MessageBox.INFO
                            });
                        }

                        if (fieldBooleanContenidoConAutoguardado.getValue() == "true")
                        {
                            ref.mostrarAvisoDeAutoguardados();
                        }
                    }
                }
            },
            enctype : 'multipart/form-data',
            reader : new Ext.data.XmlReader(
            {
                record : 'Contenido'
            }, [
                'id', 'urlPath', 'urlOriginal', 'origenInformacion', 'urlCompletaNodoMapa', 'personaResponsable',
                'orden', 'publicable', 'latitud', 'longitud', 'lugar', 'tituloCA', 'subtituloCA',
                'tituloLargoCA', 'resumenCA', 'contenidoCA', 'dataBinaryCA', 'enlaceDestinoCA', 'mimeTypeCA',
                'tituloES', 'subtituloES', 'tituloLargoES', 'resumenES', 'contenidoES',
                'dataBinaryES', 'mimeTypeES', 'enlaceDestinoES', 'tituloEN', 'subtituloEN', 'tituloLargoEN',
                'resumenEN', 'contenidoEN', 'dataBinaryEN', 'mimeTypeEN', 'enlaceDestinoEN',
                'vigencia', 'tags', 'htmlCA', 'htmlES', 'htmlEN', 'tipoFormatoCA', 'tipoFormatoES', 'tipoFormatoEN',
                'nombreFicheroCA', 'nombreFicheroES', 'nombreFicheroEN',
                'contenidoSustituidoPorAutoguardado', 'otrasPersonasEditandoContenido', 'otrosAutores', 'tipoFormatoCA',
                'tipoFormatoES', 'tipoFormatoEN', '_videoMP4CA', '_videoOGVCA',
                '_videoPosterTextoAlternativoCA', '_videoPosterImagenCA', '_videoSubtitulosCA', '_videoMP4ES',
                '_videoOGVES', '_videoPosterTextoAlternativoES', '_videoPosterImagenES',
                '_videoSubtitulosES', '_videoMP4EN', '_videoOGVEN', '_videoPosterTextoAlternativoEN',
                '_videoPosterImagenEN', '_videoSubtitulosEN', '_videoCA', '_videoES', '_videoEN', 'visible',
                'textoNoVisible', 'esquemaId', 'contenidoMetadatos', 'ajustarAFechasVigencia', 'idDescargaCA',
                'idDescargaES', 'idDescargaEN',
                {
                    name : 'fechaCreacion',
                    type : 'date',
                    dateFormat : 'd/m/Y H:i:s'
                }
            ]),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, ['success', 'msg']),
            buttons : [this.saveAndContinueButton, this.saveButton, this.cancelButton],
            items : [
                fieldCodigo, new Ext.form.TextField(
                {
                    name : 'nodoMapaId',
                    fieldLabel : 'Nodo Mapa',
                    hidden : true,
                    value : ref.mapaId
                }), fieldBooleanContenidoConAutoguardado, fieldOtrasPersonasEditandoContenido, ref.imgFlag,
                new Ext.form.TextField(
                {
                    name : 'urlPath',
                    fieldLabel : 'URL',
                    width : '630',
                    allowBlank : false,
                    blankText : 'Aquest camp és obligatori',
                    maskRe : /[a-zA-Z0-9_.\-]/,
                    regex : /^[a-zA-Z0-9_.\-]+$/i
                }), ref.comboOrigenesInformacion, new Ext.form.TextField(
                {
                    name : 'urlOriginal',
                    fieldLabel : 'Url Original',
                    width : '630',
                    readOnly : true,
                    hidden : true,
                    fieldClass : "x-item-disabled"
                }), new Ext.form.DateField(
                {
                    name : 'fechaCreacion',
                    fieldLabel : 'Data creació',
                    allowBlank : false,
                    format : 'd/m/Y',
                    value : new Date()
                }), new Ext.form.TextField(
                {
                    name : 'urlCompletaNodoMapa',
                    fieldLabel : 'urlCompletaNodoMapa',
                    readOnly : true,
                    hidden : true
                }), this.panelTabs,
                {
                    xtype : 'fieldset',
                    flex : 1,
                    title : 'Publicació',
                    collapsible : true,
                    layout : 'form',
                    margins : {
                        top : 5,
                        right : 10,
                        bottom : 0,
                        left : 10
                    },
                    items : [
                        this.datePanel, new Ext.form.Checkbox(
                        {
                            fieldLabel : 'Ajustar a dates de vigència',
                            inputValue : true,
                            name : 'ajustarAFechasVigencia'
                        })
                    ]
                },
                {
                    xtype : 'fieldset',
                    flex : 1,
                    title : 'Visibilitat',
                    collapsible : true,
                    collapsed : true,
                    forceLayout : true,
                    layout : 'form',
                    margins : {
                        top : 5,
                        right : 10,
                        bottom : 5,
                        left : 10
                    },
                    items : [this.visibilidadPanel]
                },
                {
                    xtype : 'fieldset',
                    flex : 1,
                    title : 'Prioritat',
                    collapsible : true,
                    collapsed : true,
                    forceLayout : true,
                    layout : 'form',
                    margins : {
                        top : 5,
                        right : 10,
                        bottom : 5,
                        left : 10
                    },
                    items : [this.prioridadPanel]
                },
                {
                    xtype : 'fieldset',
                    flex : 1,
                    title : 'Etiquetes',
                    collapsed : true,
                    forceLayout : true,
                    collapsible : true,
                    layout : 'form',
                    margins : '5 10 5 10',
                    items : [
                        new Ext.form.TextField(
                        {
                            name : 'tags',
                            fieldLabel : 'Tags',
                            width : 630
                        }), new Ext.form.Label(
                        {
                            text : '* etiquetes separades per comes',
                            style : 'font-style: italic; color: #BBB; padding-left: 105px'
                        })
                    ]
                },
                {
                    xtype : 'fieldset',
                    flex : 1,
                    title : 'Persones',
                    collapsed : true,
                    forceLayout : true,
                    collapsible : true,
                    layout : 'form',
                    margins : '5 10 5 10',
                    items : [
                        new Ext.form.TextField(
                        {
                            name : 'personaResponsable',
                            fieldLabel : 'Responsable',
                            disabled : true,
                            width : 630
                        }), new Ext.form.TextField(
                        {
                            name : 'otrosAutores',
                            fieldLabel : 'Altres Autors',
                            width : 630
                        })
                    ]
                },
                {
                    xtype : 'fieldset',
                    flex : 1,
                    title : 'Paràmetres',
                    collapsed : true,
                    forceLayout : true,
                    collapsible : true,
                    layout : 'form',
                    margins : {
                        top : 5,
                        right : 10,
                        bottom : 5,
                        left : 10
                    },
                    items : [
                        new Ext.form.TextField(
                        {
                            name : 'orden',
                            fieldLabel : 'Ordre',
                            width : 50
                        })
                    ]
                },
                {
                    xtype : 'fieldset',
                    flex : 1,
                    title : 'Localització',
                    collapsed : true,
                    forceLayout : true,
                    collapsible : true,
                    layout : 'form',
                    margins : {
                        top : 5,
                        right : 10,
                        bottom : 5,
                        left : 10
                    },
                    items : [
                        new Ext.form.TextField(
                        {
                            name : 'lugar',
                            fieldLabel : 'Lloc',
                            width : 630
                        }), new Ext.form.TextField(
                        {
                            name : 'longitud',
                            fieldLabel : 'Longitud',
                            width : 50
                        }), new Ext.form.TextField(
                        {
                            name : 'latitud',
                            fieldLabel : 'Latitud',
                            width : 50
                        })
                    ]
                }
            ]
        });
    },

    buildCopyValuesButton : function(idioma)
    {
        var ref = this;

        return new Ext.Button(
        {
            text : 'Copiar',
            handler : function(button, event)
            {
                var form = ref.form.getForm();
                var originalValue = form.findField("tituloLargo" + idioma).getValue();
                form.findField("titulo" + idioma).setValue(originalValue);
            }
        });
    },

    buildResumenPanel : function(idioma)
    {
        return new Ext.Panel(
        {
            fieldLabel : 'Resum',
            name : 'resumenPanel' + idioma,
            tbar : [
                {
                    text : 'Traduir des de...',
                    menu : this.buildTranslateFromMenu(idioma)
                }
            ],
            items : [
                {
                    xtype : 'textarea',
                    name : 'resumen' + idioma,
                    fieldLabel : 'Resum',
                    width : '620px',
                    height : '80'
                }
            ]
        })
    },

    buildTranslateFromMenu : function(idioma)
    {
        var ref = this;

        var tags = [];

        tags[0] = ["CA", "Català"];
        tags[1] = ["ES", "Espanyol"];
        tags[2] = ["EN", "Anglès"];

        var items = [];

        for (var this_tag in tags)
        {
            if (tags.hasOwnProperty(this_tag) && tags[this_tag][0] != idioma)
            {
                items.push({
                    text : tags[this_tag][1],
                    ref : tags[this_tag][0],
                    handler : function(item, e)
                    {
                        var originalText = ref.form.getForm().findField("resumen" + item.ref).getValue();

                        Ext.MessageBox.confirm('Avís', 'El contingut actual será substituït, voleu continuar?', function(optional)
                        {
                            if (optional != 'no')
                            {
                                ref.translateResumen(originalText, item.ref, idioma);
                            }
                        });
                    }
                });
            }
        }

        return new Ext.menu.Menu(
        {
            style : {
                overflow : 'visible'
            },
            items : items
        });
    },

    translateResumen : function(originalText, sourceLang, targetLang)
    {
        var input = this.form.getForm().findField("resumen" + targetLang);

        Ext.Ajax.request(
        {
            url : '/upo/rest/translate',
            method : 'POST',
            params : {
                text : originalText,
                sourceLang : sourceLang.toLowerCase(),
                targetLang : targetLang.toLowerCase(),
                format : 'txt'
            },
            success : function(response)
            {
                var content = response.responseText;
                input.setValue(content);
            }
        });
    },

    enableFormFields : function(option)
    {
        var form = this.form.getForm();

        this.enableElement(form.findField("urlPath"), option);
        this.enableElement(form.findField("fechaCreacion"), option);
        this.enableElement(this.comboOrigenesInformacion, option);
        this.enableElement(form.findField("tags"), option);
        this.enableElement(form.findField("longitud"), option);
        this.enableElement(form.findField("latitud"), option);
        this.enableElement(form.findField("lugar"), option);
        this.enableElement(form.findField("otrosAutores"), option);
        this.enableElement(form.findField("orden"), option);
        this.enableElement(this.datePanel, option);
        this.enableElement(this.visibilidadPanel, option);
        this.enableElement(this.prioridadPanel, option);
        this.enableElement(form.findField("ajustarAFechasVigencia"), option);

        var listaIdiomas = ['CA', 'ES', 'EN'];

        for (var i = 0; i < listaIdiomas.length; i++)
        {
            this.enableElement(form.findField("titulo" + listaIdiomas[i]), option);
            this.enableElement(form.findField("subtitulo" + listaIdiomas[i]), option);
            this.enableElement(form.findField("tituloLargo" + listaIdiomas[i]), option);
            this.enableElement(form.findField("resumen" + listaIdiomas[i]), option);
            this.enableElement(form.findField("enlaceDestino" + listaIdiomas[i]), option);
            this.enableElement(form.findField("dataBinary" + listaIdiomas[i]), option);
            this.enableElement(eval("this.binaryFileDelete" + listaIdiomas[i]), option);
            this.enableElement(eval("this.binaryFileDownload" + listaIdiomas[i]), option);
            this.enableElement(form.findField("repetirFichero" + listaIdiomas[i]), option);
            this.enableElement(form.findField("_videoMP4" + listaIdiomas[i]), option);
            this.enableElement(form.findField("_videoOGV" + listaIdiomas[i]), option);
            this.enableElement(form.findField("_videoPosterTextoAlternativo" + listaIdiomas[i]), option);
            this.enableElement(form.findField("_videoPosterImagen" + listaIdiomas[i]), option);
            this.enableElement(form.findField("_videoSubtitulos" + listaIdiomas[i]), option);
            this.enableElement(form.findField("_video" + listaIdiomas[i]), option);
            this.enableElement(this.panelTabs.find("name", "tipoFormatoPanelButtons" + listaIdiomas[i])[0], option);
            this.enableElement(this.panelTabs.find("name", "metadatosPanel" + listaIdiomas[i])[0], option);
            this.enableElement(eval("this.botonCopiar" + listaIdiomas[i]), option);
            this.enableElement(eval("this.traducirDesdeMenu" + listaIdiomas[i]), option);
            this.enableElement(this.panelTabs.find("name", "resumenPanel" + listaIdiomas[i])[0], option);

            eval("this.htmlEditor" + listaIdiomas[i]).setReadOnly(!option);
        }
    },

    enableElement : function(element, option)
    {
        if (element)
        {
            (option ? element.enable() : element.disable());
        }
    },

    setAdmin : function(administrador)
    {
        this.administrador = administrador;

        if (this.administrador)
        {
            this.saveButton.enable();
            this.saveAndContinueButton.enable();
        }
    },

    setIdiomasObligatorios : function(idiomasObligatorios)
    {
        var ref = this;

        if (idiomasObligatorios != "" && idiomasObligatorios != null)
        {
            var listaIdiomas = idiomasObligatorios.split(",");

            for (var i = 0; i < listaIdiomas.length; i++)
            {
                var lang = listaIdiomas[i];
                var titulo = ref.panelTabs.find("name", "titulo" + lang)[0];

                titulo.allowBlank = false;
                titulo.blankText = 'Aquest camp és obligatori';
            }
        }
    },

    executeFormSubmit : function(contenidoId, continueEditingForm)
    {
        var contenidoForm = this;
        var vigencia = [], prioridad = [];
        var itemsVigencia = contenidoForm.datePanel.dateList.store.data.items;
        var itemsPrioridad = contenidoForm.prioridadPanel.store.data.items;

        for (var i = 0; i < itemsVigencia.length; i++)
        {
            var record = itemsVigencia[i].data;
            vigencia.push(record.data + "|" + record.horaInicio + "|" + record.horaFin);
        }

        for (var i = 0; i < itemsPrioridad.length; i++)
        {
            var record = itemsPrioridad[i].data;
            prioridad.push(contenidoForm.getFormattedValue(record.fechaInicio, 'd/m/Y') + "|" + contenidoForm.getFormattedValue(record.horaInicio, 'H:i') + "|" + contenidoForm.getFormattedValue(record.fechaFin, 'd/m/Y') + "|" + contenidoForm.getFormattedValue(record.horaFin, 'H:i') + "|" + record.prioridad);
        }

        contenidoForm.form.getForm().submit(
        {
            url : '/upo/rest/contenido/',
            method : 'post',
            success : function(form, action)
            {
                var response = action.response.responseXML;

                contenidoForm.form.getForm().findField("id").setValue(Ext.DomQuery.selectValue('id', response));

                var publicable = (Ext.DomQuery.selectValue('publicable', response) == 'S');

                if (publicable)
                {
                    contenidoForm.setIconPublicable();
                }

                if (!publicable)
                {
                    contenidoForm.setIconNoPublicable();
                }

                UJI.Bus.publish('UJI.UPO.ContenidoForm.Change',
                {
                    mapaId : contenidoForm.mapaId
                });

                if (!continueEditingForm)
                {
                    UJI.Bus.publish('UJI.UPO.ContenidoForm.Button.Cancel',
                    {
                        mapaId : contenidoForm.mapaId
                    });

                    contenidoForm.deleteAutoGuardados(contenidoId, false);
                }

                if (continueEditingForm)
                {
                    contenidoForm.resetImageDataForNewBinary('CA');
                    contenidoForm.resetImageDataForNewBinary('ES');
                    contenidoForm.resetImageDataForNewBinary('EN');

                    contenidoForm.deleteAutoGuardados(contenidoId, true);
                    contenidoForm.resetDirtyDataEditors();
                }

                if (contenidoForm.paramsForm.migracionGrid)
                {
                    contenidoForm.paramsForm.migracionGrid.store.reload();
                }
            },
            params : {
                vigencia : vigencia,
                prioridad : prioridad
            }
        });
    },

    resetImageDataForNewBinary : function(idioma)
    {
        var form = this.form.getForm();

        if (form.findField("dataBinary" + idioma).getValue())
        {
            this.resetImageFields(idioma);

            if (form.findField("repetirFicheroCA").getValue() || form.findField("repetirFicheroES").getValue() || form.findField("repetirFicheroEN").getValue())
            {
                this.resetImageFields('CA');
                this.resetImageFields('ES');
                this.resetImageFields('EN');
            }
        }
    },

    resetImageFields : function(idioma)
    {
        var form = this.form.getForm();

        form.findField("nombreFichero" + idioma).setValue('file');
        form.findField("idDescarga" + idioma).setValue('');
        this.setImage(idioma, '');
    },

    getFormattedValue : function(value, format)
    {
        if (!value || value == null)
        {
            return '';
        }

        if (!format || !(value instanceof Date))
        {
            return value
        }

        return value.format(format);
    },

    buildSaveAndContinueButton : function()
    {
        var contenidoForm = this;
        this.saveAndContinueButton = new Ext.Button(
        {
            text : 'Desar i continuar',
            iconCls : 'btn-verde',
            disabled : true,
            handler : function(button, event)
            {
                contenidoForm.executeSaveHandler(true);
            }
        });
    },

    getContenidoId : function()
    {
        return (this.form.getForm().findField("id") ? this.form.getForm().findField("id").getValue() : null);
    },

    executeSaveHandler : function(continueEditing)
    {
        var contenidoForm = this;
        var contenidoId = contenidoForm.getContenidoId();

        this.prioridadPanel.editor.stopEditing(true);

        if (contenidoForm.form.getForm().isValid())
        {
            if (contenidoForm.tipoContenidoNormal)
            {
                Ext.Ajax.request(
                {
                    url : '/upo/rest/contenido/texto-accesible',
                    method : 'POST',
                    params : {
                        contenidoES : contenidoForm.htmlEditorES.getValue(),
                        contenidoCA : contenidoForm.htmlEditorCA.getValue(),
                        contenidoEN : contenidoForm.htmlEditorEN.getValue()
                    },
                    success : function(r)
                    {
                        var respuesta = Ext.DomQuery.selectValue('success', r.responseXML);

                        if (respuesta == "false" && continueEditing)
                        {
                            Ext.MessageBox.show(
                            {
                                title : 'Aviso',
                                msg : "El contenido no es accesible",
                                buttons : Ext.MessageBox.OK,
                                icon : Ext.MessageBox.WARNING
                            });

                            contenidoForm.executeFormSubmit(contenidoId, true);
                        }

                        if (respuesta == "false" && !continueEditing)
                        {
                            Ext.MessageBox.confirm('Avís', 'El contingut no és accesible. Voleu quedar-vos i arreglar-ho?', function(optional)
                            {
                                if (optional == 'no')
                                {
                                    UJI.Bus.publish('UJI.UPO.ContenidoForm.Button.Cancel',
                                    {
                                        mapaId : contenidoForm.mapaId
                                    });

                                    contenidoForm.executeFormSubmit(contenidoId, false);
                                }
                            });
                        }

                        if (respuesta == "true")
                        {
                            contenidoForm.executeFormSubmit(contenidoId, continueEditing);
                        }
                    },
                    failure : function()
                    {
                        Ext.MessageBox.show(
                        {
                            title : 'Error',
                            msg : "Timeout al comprovar l'accessibilitat. Torneu a intentar desar els canvis.",
                            buttons : Ext.MessageBox.OK,
                            icon : Ext.MessageBox.ERROR
                        });
                    }
                });
            }
            else
            {
                contenidoForm.executeFormSubmit(contenidoId, continueEditing);
            }
        }
        else
        {
            Ext.MessageBox.show(
            {
                title : 'Error',
                msg : "Revisa que tots els camps obligatoris estiguen plens i siguen vàlids",
                buttons : Ext.MessageBox.OK,
                icon : Ext.MessageBox.ERROR
            });
        }
    },

    buildSaveButton : function()
    {
        var contenidoForm = this;
        this.saveButton = new Ext.Button(
        {
            text : 'Desar',
            iconCls : 'btn-verde',
            disabled : true,
            handler : function(button, event)
            {
                contenidoForm.executeSaveHandler(false);
            }
        });
    },

    buildCancelButton : function()
    {
        var ref = this;

        this.cancelButton = new Ext.Button(
        {
            text : 'Cancel·lar',
            iconCls : 'btn-rojo',
            handler : function()
            {
                var contenidoId = ref.getContenidoId();

                if (ref.hayCambiosSinGuardar() || ref.avisoCambiosSinGuardar)
                {
                    if (confirm("Hi ha canvis sense desar. Segur que voleu eixir?"))
                    {
                        ref.deleteAutoGuardados(contenidoId, false);
                    }
                }
                else
                {
                    ref.deleteAutoGuardados(contenidoId, false);
                }
            }
        });
    },

    hayCambiosSinGuardar : function()
    {
        return (this.htmlEditorCA.isDirty() || this.htmlEditorES.isDirty() || this.htmlEditorEN.isDirty());
    },

    executeCancelButton : function()
    {
        UJI.Bus.publish('UJI.UPO.ContenidoForm.Button.Cancel',
        {
            mapaId : this.mapaId
        });
    },

    deleteAutoGuardados : function(contenidoId, continueEditing)
    {
        var ref = this;

        Ext.Ajax.request(
        {
            url : '/upo/rest/autoguardado/',
            method : 'DELETE',
            params : {
                mapaId : ref.mapaId,
                contenidoId : contenidoId
            },
            success : function()
            {
                ref.avisoCambiosSinGuardar = false;

                if (!continueEditing)
                {
                    Ext.TaskMgr.stop(ref.refresherTask);
                    ref.executeCancelButton();
                }
            }
        });
    },

    buildBinaryFile : function(idioma)
    {
        return new Ext.form.TextField(
        {
            allowBlank : true,
            inputType : 'file',
            name : 'dataBinary' + idioma,
            fieldLabel : 'Fitxer',
            anchor : '70%'
        });
    },

    buildBinaryFileDownload : function(idioma)
    {
        var ref = this;

        return new Ext.Button(
        {
            text : 'Descarregar fitxer',
            iconCls : 'arrow-down',
            handler : function()
            {
                var nombreFichero = eval("this.nombreFichero" + idioma);

                if (nombreFichero.getValue())
                {
                    window.open(ref.getUrlImageDownload(idioma), '');
                }
            },
            scope : this
        });
    },

    setImage : function(idioma, mimeType)
    {
        var image = eval("this.image" + idioma);

        if (mimeType && mimeType.indexOf('image') != -1)
        {
            var src = this.getUrlImageDownload(idioma)

            image.update('<div class="snapshot"><a target="_blank" href="' + src + '"><img alt="img" class="snapshot" src="' + src + '" /></a></div>');
        }
        else
        {
            image.update('');
        }
    },

    getUrlImageDownload : function(idioma)
    {
        var idDescarga = this.form.getForm().findField("idDescarga" + idioma).getValue();

        if (idDescarga) return 'http://ujiapps.uji.es/ade/rest/storage/' + idDescarga;

        var id = this.fieldCodigo.getValue();

        if (id) return '/upo/rest/contenido/' + id + '/raw?idioma=' + idioma;
    },

    buildBinaryFileDelete : function(idioma)
    {
        var ref = this;

        return new Ext.Button(
        {
            text : 'Esborrar fitxer',
            iconCls : 'cancel',
            disabled : false,
            handler : function()
            {
                var nombreFichero = eval("this.nombreFichero" + idioma);

                if (nombreFichero.getValue())
                {
                    Ext.Ajax.request(
                    {
                        url : '/upo/rest/contenido/' + ref.fieldCodigo.getValue() + '/adjunto/' + idioma,
                        method : 'DELETE',
                        success : function()
                        {
                            if (idioma == 'CA')
                                ref.nombreFicheroCA.setValue('');
                            if (idioma == 'ES')
                                ref.nombreFicheroES.setValue('');
                            if (idioma == 'EN')
                                ref.nombreFicheroEN.setValue('');

                            ref.setImage(idioma, '');

                            Ext.MessageBox.show(
                            {
                                title : 'Borrado',
                                msg : 'Adjunt esborrat correctament',
                                buttons : Ext.MessageBox.OK,
                                icon : Ext.MessageBox.INFO
                            });
                        },
                        failure : function()
                        {
                            Ext.MessageBox.show(
                            {
                                title : 'Error',
                                msg : 'Error esborrant l\'adjunt',
                                buttons : Ext.MessageBox.OK,
                                icon : Ext.MessageBox.ERROR
                            });
                        }
                    });
                }
            },
            scope : this
        });
    },

    buildHtmlEditor : function(idioma)
    {
        var ref = this;

        var ckEditor = new Ext.form.CKEditor(
        {
            name : 'contenido' + idioma,
            fieldLabel : 'Contingut',
            hideLabel : false,
            height : 300,
            id : 'contenido' + idioma + '_' + this.id,
            CKConfig : {
                extraAllowedContent : 'script[*]; style[*]; link[*]; meta[*]; html[*]; base[*]; head[*]; title[*];' +
                'aside(*)[*]{*}; address(*)[*]{*}; article(*)[*]{*}; body(*)[*]{*}; footer(*)[*]{*}; header(*)[*]{*}; hgroup(*)[*]{*}; nav(*)[*]{*}; section(*)[*]{*}; h1(*)[*]{*}; h2(*)[*]{*}; h3(*)[*]{*}; h4(*)[*]{*}; h5(*)[*]{*}; h6(*)[*]{*};' +
                'blockquote(*)[*]{*}; dd(*)[*]{*}; div(*)[*]{*}; dl(*)[*]{*}; dt(*)[*]{*}; figcaption(*)[*]{*}; figure(*)[*]{*}; hr(*)[*]{*}; li(*)[*]{*}; main(*)[*]{*}; ol(*)[*]{*}; p(*)[*]{*}; pre(*)[*]{*}; ul(*)[*]{*};' +
                'a(*)[*]{*}; abbr(*)[*]{*}; b(*)[*]{*}; bdi(*)[*]{*}; bdo(*)[*]{*}; br(*)[*]{*}; cite(*)[*]{*}; code(*)[*]{*}; data(*)[*]{*}; dfn(*)[*]{*}; em(*)[*]{*}; i(*)[*]{*}; kbd(*)[*]{*}; ' +
                'mark(*)[*]{*}; q(*)[*]{*}; rp(*)[*]{*}; rt(*)[*]{*}; rtc(*)[*]{*}; ruby(*)[*]{*}; s(*)[*]{*}; samp(*)[*]{*}; small(*)[*]{*}; span(*)[*]{*}; strong(*)[*]{*}; sub(*)[*]{*}; sup(*)[*]{*}; time(*)[*]{*}; u(*)[*]{*}; var(*)[*]{*}; wbr(*)[*]{*};' +
                'area(*)[*]{*}; audio(*)[*]{*}; map(*)[*]{*}; track(*)[*]{*}; video(*)[*]{*};' +
                'embed(*)[*]{*}; iframe(*)[*]{*}; img(*)[*]{*}; object(*)[*]{*}; param(*)[*]{*}; source(*)[*]{*};' +
                'del(*)[*]{*}; ins(*)[*]{*};' +
                'details(*)[*]{*}; dialog(*)[*]{*}; menu(*)[*]{*}; menuitem(*)[*]{*}; summary(*)[*]{*};' +
                'button(*)[*]{*}; datalist(*)[*]{*}; fieldset(*)[*]{*}; form(*)[*]{*}; input(*)[*]{*}; keygen(*)[*]{*}; label(*)[*]{*}; legend(*)[*]{*}; meter(*)[*]{*}; optgroup(*)[*]{*}; option(*)[*]{*}; output(*)[*]{*}; progress(*)[*]{*}; select(*)[*]{*}; textarea(*)[*]{*};' +
                'caption(*)[*]{*}; col(*)[*]{*}; colgroup(*)[*]{*}; table(*)[*]{*}; tbody(*)[*]{*}; td(*)[*]{*}; tfoot(*)[*]{*}; th(*)[*]{*}; thead(*)[*]{*}; tr(*)[*]{*};',
                disallowedContent : '*[dir]',
                pasteFilter : 'strong[*]; b[*]; span[*]; cite[*]; h1[*]; h2[*]; h3[*]; h4[*]; h5[*]; h6[*]; p[*]; br[*]; a[*]; dl[*]; dt[*]; dd[*]; div[*]; sub[*]; sup[*]; caption[*]; col[*]; colgroup[*]; table[*]; tbody[*]; td[*]; tfoot[*]; th[*]; thead[*]; tr[*]; li[*]; ol[*]; ul[*];',
                extraPlugins : 'footnote,accessibilitydialog,translator',
                toolbar : 'MyToolbarSet',
                filebrowserBrowseUrl : 'documentsReservori.jsp?urlNode=' + ref.mapaUrlCompleta + "&idioma=" + idioma,
                filebrowserImageBrowseUrl : 'imatgesReservori.jsp?urlNode=' + ref.mapaUrlCompleta + "&idioma=" + idioma,
                filebrowserWindowWidth : '640',
                filebrowserWindowHeight : '540',
                contenidoFormId : ref.id,
                idioma : idioma,
                id : 'contenido' + idioma + '_' + this.id,
                personaId : userAuthenticatedId,
                toolbar_MyToolbarSet : [
                    {
                        name : 'document',
                        items : ['Source', '-', 'Preview', '-', 'Templates']
                    },
                    {
                        name : 'clipboard',
                        items : ['Cut', 'Copy', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']
                    },
                    {
                        name : 'editing',
                        items : ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt']
                    },
                    {
                        name : 'insert',
                        items : ['Image', 'Flash', 'Table', 'HorizontalRule', 'SpecialChar', 'Iframe']
                    },
                    {
                        name : 'tools',
                        items : ['Maximize', 'ShowBlocks', '-', 'About']
                    },
                    '/',
                    {
                        name : 'styles',
                        items : ['Styles', 'Format']
                    },
                    {
                        name : 'basicstyles',
                        items : [
                            'Bold', 'Italic', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                        ]
                    },
                    {
                        name : 'paragraph',
                        items : [
                            'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv',
                            '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock',
                            '-', 'BidiLtr', 'BidiRtl'
                        ]
                    },
                    {
                        name : 'links',
                        items : ['Link', 'Unlink', 'Anchor']
                    }, '/',
                    {
                        name : 'UJI',
                        items : ['Footnote', 'AccessibilityDialog', 'Translator']
                    }
                ],
                height : '300'
            },
            enableColors : false
        });

        return ckEditor;
    }
    ,
    buildTipoFormatoPanelButtons : function(lang)
    {
        var ref = this;
        var buttons = [];
        var items = UJI.UPO.TipoFormato.getData();

        Ext.each(items, function(item)
        {
            buttons.push(
            {
                xtype : 'button',
                icon : '/upo/img/' + item.icon,
                tooltip : item.name,
                handler : function()
                {
                    ref.tipoFormatoChangeActions(lang, item.value);
                }
            });
        });

        return new Ext.Panel(
        {
            name : 'tipoFormatoPanelButtons' + lang,
            layout : 'hbox',
            border : false,
            items : buttons,
            padding : "20px 0px 3px 105px"
        });
    },

    tipoFormatoChangeActions : function(lang, value)
    {
        var ref = this;
        var contenido = ref.panelTabs.find("name", "contenido" + lang)[0];
        var videoUJIPanel = ref.panelTabs.find("name", "videoUJIPanel" + lang)[0];
        var videoOTROSPanel = ref.panelTabs.find("name", "videoOTROSPanel" + lang)[0];
        var binarioPanel = ref.panelTabs.find("name", "binarioPanel" + lang)[0];
        ref.panelTabs.find("name", "tipoFormato" + lang)[0].setValue(value);

        if (value === "PAGINA")
        {
            contenido.show();
            binarioPanel.hide();
            videoUJIPanel.hide();
            videoOTROSPanel.hide();
        }
        else if (value === "BINARIO")
        {
            binarioPanel.show();
            contenido.hide();
            videoUJIPanel.hide();
            videoOTROSPanel.hide();
        }
        else if (value === 'VIDEO_UJI')
        {
            videoUJIPanel.show();
            contenido.hide();
            binarioPanel.hide();
            videoOTROSPanel.hide();
        }
        else
        {
            videoOTROSPanel.show();
            videoUJIPanel.hide();
            contenido.hide();
            binarioPanel.hide();
        }
    },

    buildComboOrigenesInformacion : function()
    {
        var ref = this;
        this.comboOrigenesInformacion = new Ext.form.ComboBox(
        {
            valueField : 'id',
            width : 640,
            name : 'origenInformacion',
            displayField : 'nombre',
            hiddenName : 'origenInformacion',
            fieldLabel : 'Origen',
            triggerAction : 'all',
            store : ref.storeOrigenesInformacion,
            mode : 'local',
            forceSelection : true
        });
    },

    removeFields : function()
    {
        if (this.datePanel && this.datePanel.dateList && this.datePanel.dateList.store)
        {
            this.datePanel.dateList.store.removeAll();
        }

        if (this.prioridadPanel && this.prioridadPanel.store)
        {
            this.prioridadPanel.store.removeAll();
        }

        this.metadatosPanelCA.removeAll();
        this.metadatosPanelES.removeAll();
        this.metadatosPanelEN.removeAll();
    },

    deleteFields : function()
    {
        if (this.imgFlag.el)
        {
            this.setIconNoPublicable();
        }

        this.removeFields();
    },

    loadForm : function(store, record, options)
    {
        var ref = this;

        ref.removeFields();

        ref.avisoCambiosSinGuardar = false;
        ref.prepareFirstSetDataEditors();
        ref.requestLoadForm(ref);
    },

    requestLoadForm : function(ref)
    {
        ref.form.getForm().load(
        {
            url : this.urlForm,
            waitMsg : 'Per favor, espereu...',
            waitTitle : 'Cargando formulario',
            method : 'GET',
            timeout : 120000,
            params : ref.paramsForm,
            failure : function(f, action)
            {
                Ext.MessageBox.show(
                {
                    title : 'Error',
                    msg : "Errada carregant el formulari",
                    buttons : Ext.MessageBox.OK,
                    icon : Ext.MessageBox.ERROR
                });
            },
            success : function(f, action)
            {
                var listaIdiomas = ['CA', 'ES', 'EN'];
                var formData = action.result.data;

                for (var i = 0; i < listaIdiomas.length; i++)
                {
                    var lang = listaIdiomas[i];
                    ref.tipoFormatoChangeActions(lang, formData["tipoFormato" + lang]);

                    ref.setImage(lang, formData["mimeType" + lang]);
                }

                if (formData && formData.vigencia)
                {
                    var fechas = formData.vigencia.split(",");

                    for (var i = 0; i < fechas.length; i++)
                    {
                        var fechasYHoras = fechas[i].split("|");

                        var fecha = new Date(fechasYHoras[0]).format('d/m/Y');
                        var horaInicio = "";
                        var horaFin = "";

                        if (fechasYHoras[1])
                        {
                            horaInicio = new Date(fechasYHoras[0] + " " + fechasYHoras[1]).format('H:i');
                        }

                        if (fechasYHoras[2])
                        {
                            horaFin = new Date(fechasYHoras[0] + " " + fechasYHoras[2]).format('H:i');
                        }

                        ref.datePanel.addVigencia(fecha, horaInicio, horaFin);
                    }
                }

                if (formData && formData.publicable)
                {
                    if (formData.publicable == 'S')
                    {
                        ref.setIconPublicable();
                    }

                    if (formData.publicable == 'N')
                    {
                        ref.setIconNoPublicable();
                    }
                }

                if (formData && formData.visible)
                {
                    ref.visibilidadPanel.setValues(formData.visible, formData.textoNoVisible);
                }

                ref.prioridadPanel.reload(formData.id);

                var metadatos = Ext.DomQuery.jsSelect('contenidoMetadatos>value', action.response.responseXML);
                ref.loadEsquemaMetadatos(formData["esquemaId"], metadatos);
            }
        });
    },

    setIconNoPublicable : function()
    {
        this.imgFlag.el.dom.src = this.imgRed;
        this.imgFlag.el.dom.title = "publicable";
        this.imgFlag.el.dom.alt = "publicable";
    },

    setIconPublicable : function()
    {
        this.imgFlag.el.dom.src = this.imgGreen;
        this.imgFlag.el.dom.title = "publicable";
        this.imgFlag.el.dom.alt = "publicable";
    },

    fillEsquemaMetadatos : function(metadatos)
    {
        if (!metadatos)
        {
            return;
        }

        for (var i = 0; i < metadatos.length; i++)
        {
            var key = Ext.DomQuery.selectValue('clave', metadatos[i]);
            var valorCA = Ext.DomQuery.selectValue('valorCA', metadatos[i]);
            var valorES = Ext.DomQuery.selectValue('valorES', metadatos[i]);
            var valorEN = Ext.DomQuery.selectValue('valorEN', metadatos[i]);

            this.metadatosPanelCA.setValue(key, valorCA);
            this.metadatosPanelES.setValue(key, valorES);
            this.metadatosPanelEN.setValue(key, valorEN);
        }
    },

    loadEsquemaMetadatos : function(esquemaId, metadatos)
    {
        var ref = this;

        if (!esquemaId || esquemaId === -1)
        {
            return;
        }

        UJI.UPO.ContenidoMetadatosUtil.requestAtributosFromEsquema(esquemaId, this.metadatosPanels, function()
        {
            UJI.UPO.ContenidoMetadatosUtil.doLayout(ref.metadatosPanels);
            ref.fillEsquemaMetadatos(metadatos);
            UJI.UPO.ContenidoMetadatosUtil.doLayout(ref.metadatosPanels);
        });
    },

    prepareFirstSetDataEditors : function()
    {
        this.htmlEditorCA.prepareFirstSetData(true);
        this.htmlEditorES.prepareFirstSetData(true);
        this.htmlEditorEN.prepareFirstSetData(true);
    },

    resetDirtyDataEditors : function()
    {
        this.htmlEditorCA.resetDirty();
        this.htmlEditorES.resetDirty()
        this.htmlEditorEN.resetDirty()
    },

    buildStoreOrigenesInformacion : function()
    {
        this.storeOrigenesInformacion = new Ext.data.Store(
        {
            url : '/upo/rest/origen',
            restful : true,
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'OrigenInformacion',
                id : 'id'
            }, ['id', 'nombre']),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    autoSaveRequest : function(id, idioma, functionSuccess)
    {
        var ref = this;
        var instancia = eval("this.htmlEditor" + idioma);

        if (ref.administrador && ref.tipoContenidoNormal && instancia && instancia.isDirty())
        {
            var contenidoId = ref.getContenidoId();

            Ext.Ajax.request(
            {
                url : '/upo/rest/autoguardado/',
                method : 'POST',
                params : {
                    mapaId : ref.mapaId,
                    contenido : instancia.getValue(),
                    idioma : idioma,
                    contenidoId : contenidoId
                },
                success : function()
                {
                    instancia.resetDirty();
                    ref.avisoCambiosSinGuardar = true;

                    if (functionSuccess)
                    {
                        functionSuccess();
                    }
                }
            });
        }
        else
        {
            if (functionSuccess)
            {
                functionSuccess();
            }
        }
    }
    ,

    buildPeriodinalRequestForAutoSave : function()
    {
        Ext.TaskMgr.start(this.refresherTask);
    }
    ,

    requestAutoguardados : function()
    {
        var ref = this;

        Ext.Ajax.request(
        {
            url : '/upo/rest/autoguardado/',
            method : 'GET',
            params : {
                mapaId : ref.mapaId
            },
            success : function(response)
            {
                autoguardadoContent = response.responseXML;

                contenidoES = Ext.DomQuery.selectValue('autoguardadoES', autoguardadoContent);
                contenidoCA = Ext.DomQuery.selectValue('autoguardadoCA', autoguardadoContent);
                contenidoEN = Ext.DomQuery.selectValue('autoguardadoEN', autoguardadoContent);

                ref.prepareFirstSetDataEditors();

                if (contenidoCA)
                {
                    ref.htmlEditorCA.setValue(contenidoCA, true);
                }

                if (contenidoES)
                {
                    ref.htmlEditorES.setValue(contenidoES, true);
                }

                if (contenidoEN)
                {
                    ref.htmlEditorEN.setValue(contenidoEN, true);
                }

                if (contenidoCA || contenidoES || contenidoEN)
                {
                    ref.mostrarAvisoDeAutoguardados();
                }
            }
        });
    }
    ,

    listeners : {
        beforedestroy : function(object)
        {
            Ext.TaskMgr.stop(this.refresherTask);
        }
    },

    mostrarAvisoDeAutoguardados : function()
    {
        Ext.MessageBox.show(
        {
            title : 'Información',
            msg : 'S\'ha detectat contingut procedent d\'un autoguardat. Recordeu que cal prèmer el botó de Desar per fer efectius els canvis',
            buttons : Ext.MessageBox.OK,
            icon : Ext.MessageBox.INFO
        });
    }
});
