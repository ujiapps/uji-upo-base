Ext.ns('UJI.UPO');

UJI.UPO.ContenidoMetadatosIdiomaPanel = Ext.extend(Ext.Panel,
{
    initComponent : function()
    {
        Ext.layout.FormLayout.prototype.trackLabels = true;

        var config = {
            width : this.width,
            allBlanks : this.allBlanks || false,
            showKeyHowLabel : this.showKeyHowLabel || false,
            keyPrefix : this.keyPrefix || '',
            idioma : this.idioma,
            name : this.name
        };

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.ContenidoMetadatosIdiomaPanel.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
    },

    addField : function(params)
    {
        var ref = this;
        var insertar = true;

        this.searchFieldAndDoAction(this.keyPrefix + params.key, function(panel, item)
        {
            UJI.UPO.Application.showMessageError("Aquest camp ja existeix");

            insertar = false
        });

        if (insertar)
        {
            var valorDefecto;

            if (params.nodo)
            {
                valorDefecto = Ext.DomQuery.selectValue('valorDefecto' + ref.idioma, params.nodo);
            }

            var parametrosPorDefecto =
                {
                    id : params.id,
                    idioma : ref.idioma,
                    key : this.keyPrefix + params.key,
                    valorDefecto : valorDefecto,
                    permiteBlanco : this.allBlanks || params.permiteBlanco,
                    nuevo : params.nuevo,
                    values : params.values,
                    nombreClave : params.nombreClave,
                    publicable : params.publicable,
                    width : params.width || this.width
                }

            this.addTypedField(parametrosPorDefecto, UJI.UPO.TipoMetadato[params.tipo].tipo, params.valoresCombo);
        }
    },

    addTypedField : function(parametrosPorDefecto, tipo, valoresCombo)
    {
        if (tipo === 'textfield')
        {
            this.addTextField(parametrosPorDefecto);
        }

        if (tipo === 'datefield')
        {
            this.addDateField(parametrosPorDefecto);
        }

        if (tipo === 'textarea')
        {
            this.addTextArea(parametrosPorDefecto);
        }

        if (tipo === 'editora')
        {
            this.addEditora(parametrosPorDefecto);
        }

        if (tipo === 'combo')
        {
            parametrosPorDefecto.valoresCombo = valoresCombo;
            parametrosPorDefecto.nuevo = false;
            parametrosPorDefecto.valorDefecto = null;

            this.addCombo(parametrosPorDefecto);
        }
    },

    addTextField : function(params)
    {
        var field = this.getDefaultStructureField(params);

        field.xtype = 'textfield';
        field.width = params.width;

        this.addStructuredField(field, params);
    },

    addDateField : function(params)
    {
        var field = this.getDefaultStructureField(params);

        field.xtype = 'datefield';
        field.format = 'd/m/Y';
        field.width = '120';

        this.addStructuredField(field, params);
    },

    addTextArea : function(params)
    {
        var field = this.getDefaultStructureField(params);

        field.xtype = 'textarea';
        field.width = params.width;

        this.addStructuredField(field, params);
    },

    addEditora : function(params)
    {
        var field = this.getDefaultStructureField(params);

        field.hideLabel = false;
        field.height = 150;
        field.enableColors = false;
        field.CKConfig = {
            toolbar : 'Basic',
            height : '70',
            width : params.width,
            border : false,
            readOnly : field.readOnly,
            resize_enabled : false,
            toolbar_Basic : [
                {
                    name : 'basic',
                    items : ['Bold', 'Italic', 'NumberedList', 'BulletedList', 'Link', 'Unlink']
                }
            ]
        }

        this.addStructuredField(new Ext.form.CKEditor(field), params);
    },

    addCombo : function(params)
    {
        var defaultField = this.getDefaultStructureField(params);

        var arrayValores = [];

        for (var j = 0; j < params.valoresCombo.length; j++)
        {
            arrayValores.push([
                Ext.DomQuery.selectValue('id', params.valoresCombo[j]),
                Ext.DomQuery.selectValue('valor' + params.idioma, params.valoresCombo[j])
            ]);
        }

        this.addStructuredField(
        {
            xtype : 'combo',
            valueField : 'id',
            width : this.getNumberFromWidth(params.width),
            displayField : 'nombre',
            fieldLabel : defaultField.fieldLabel,
            name : defaultField.name,
            hiddenName : defaultField.name,
            anchor : defaultField.width,
            triggerAction : 'all',
            forceSelection : true,
            mode : 'local',
            allowBlank : defaultField.allowBlank,
            store : new Ext.data.ArrayStore(
            {
                id : 0,
                fields : ['id', 'nombre'],
                data : arrayValores
            })
        }, params);
    },

    getNumberFromWidth : function(width)
    {
        if (width.indexOf("px") > 0)
        {
            return +width.substr(0, width.indexOf('px'));
        }

        return +width || width;
    },

    getDefaultStructureField : function(params)
    {
        var value = params.values ? params.values[params.idioma] : null;

        var field =
            {
                fieldLabel : params.key,
                key : params.key,
                name : params.key + params.idioma,
                value : value ? value : params.valorDefecto,
                readOnly : params.valorDefecto ? true : false,
                allowBlank : params.permiteBlanco,
            }

        if (params.width && params.width.indexOf('%') > 0)
        {
            field.width = params.width;
        }

        return field;
    },

    addStructuredField : function(field, params)
    {
        var deleteButton = this.getDeleteKeyButton(!params.nuevo, params.key);
        var hiddenFieldTipo = this.getHiddenFieldTipo(params.id, params.key + params.idioma, params.nuevo);
        var hiddenFieldNombre = this.getHiddenFieldNombreClave(params.key + params.idioma, params.nombreClave[params.idioma]);

        var label =
            {
                xtype : 'label',
                text : params.nombreClave[params.idioma],
                style : 'color: grey; line-height: 14px;'
            };

        if (this.showKeyHowLabel)
        {
            field.fieldLabel = params.nombreClave[params.idioma];
        }

        if (!params.publicable)
        {
            label =
            {
                xtype : 'label',
                text : '[ no publicable ]',
                style : 'color: grey; line-height: 14px;'
            };
        }

        var composite =
            {
                xtype : 'compositefield',
                padding : 0,
                items : []
            };

        composite.items.push(deleteButton);

        if (!this.showKeyHowLabel || !params.publicable)
        {
            composite.items.push(label);
        }

        composite.items.push(hiddenFieldTipo);
        composite.items.push(hiddenFieldNombre);

        this.add(
        {
            xtype : 'panel',
            layout : 'form',
            border : false,
            items : [field, composite],
            style : 'margin-bottom:' + ((!this.showKeyHowLabel || !params.publicable) ? '8px;' : '-10px;')
        });

        deleteButton.setVisible(params.nuevo);
    },

    getDeleteKeyButton : function(disabled, key)
    {
        var ref = this;

        return new Ext.Button(
        {
            iconCls : 'delete-min',
            disabled : disabled,
            handler : function()
            {
                ref.fireEvent("deleteField",
                {
                    key : key
                });
            }
        })
    },

    getHiddenFieldTipo : function(id, name, nuevo)
    {
        return new Ext.form.TextField(
        {
            hidden : true,
            name : name,
            value : nuevo ? 'tipo::' + id : 'atributo::' + id
        });
    },

    getHiddenFieldNombreClave : function(name, value)
    {
        return new Ext.form.TextField(
        {
            hidden : true,
            name : name,
            value : 'nombreClave::' + value
        });
    },

    setValue : function(key, value)
    {
        this.searchFieldAndDoAction(this.keyPrefix + key, function(panel, item)
        {
            item.items.items[0].setValue(value);
        });
    },

    searchFieldAndDoAction : function(key, action)
    {
        var items = this.items.items;

        for (var j = 0; j < items.length; j++)
        {
            if (items[j].items)
            {
                var keyItem = items[j].items.items[0].name;

                if (keyItem && keyItem.toUpperCase() === key.toUpperCase() + this.idioma)
                {
                    action(this, items[j]);
                }
            }
        }
    }
});