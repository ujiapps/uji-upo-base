Ext.ns('UJI.UPO');

UJI.UPO.MigrarContenidosWindow = Ext.extend(Ext.Window,
{
    title : 'Migració de continguts',
    layout : 'form',
    width : 500,
    height : 125,
    modal : true,
    padding : 10,
    mapaId : '',
    requestButton : {},
    cancelButton : {},
    fbar : [],
    urlField : {},
    listeners :
    {
        afterrender : function(window)
        {
            window.urlField.focus(false, 50);
        }
    },

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.CrearNodoWindow.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildRequestButton();
        this.buildCancelButton();

        this.buildUrlField();

        this.add(this.urlField);

        this.getFooterToolbar().addButton(this.requestButton);
        this.getFooterToolbar().addButton(this.cancelButton);
    },

    buildUrlField : function()
    {
        this.urlField = new Ext.form.TextField(
        {
            fieldLabel : 'Url',
            allowBlank : false,
            width : '90%'
        });
    },

    addRequest : function(url)
    {
        if (url)
        {
            return (Ext.Ajax.request(
            {
                url : '/upo/rest/migracionlotes/',
                params :
                {
                    mapaId : ref.mapaId,
                    url : url
                },
                success : function()
                {
                    return true;
                },
                failure : function(result, request)
                {
                    Ext.Msg.alert('Error', "S'ha produït una errada");
                    return false;
                },
                method : 'POST'
            }));
        }
        else
        {
            Ext.Msg.alert('Error', "El camp URL és obligatori");
            return false;
        }
    },

    buildRequestButton : function()
    {
        var ref = this;
        this.requestButton = new Ext.Button(
        {
            text : 'Solicitar Migración',
            handler : function()
            {
                Ext.Msg.confirm('Confirmar', 'Esteu segur/a de voler sol·licitar la migració ?', function(btn, text)
                {
                    if (btn == 'yes')
                    {
                        if (ref.addRequest(ref.urlField.getValue()))
                        {
                            ref.close();
                        }
                    }
                });
            }
        });
    },

    buildCancelButton : function()
    {
        ref = this;

        this.cancelButton = new Ext.Button(
        {
            text : 'Cancel·lar',
            handler : function()
            {
                ref.close();
            }
        });
    }
});