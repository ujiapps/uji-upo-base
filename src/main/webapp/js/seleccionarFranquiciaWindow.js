Ext.ns('UJI.UPO');

UJI.UPO.SeleccionarFranquiciaWindow = Ext.extend(Ext.Window,
{
    title : 'Selecció Franquícia',
    layout : 'vbox',
    width : 500,
    height : 350,
    modal : true,
    padding : 10,
    mapaId : '',
    requestButton : {},
    cancelButton : {},
    fbar : [],

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.SeleccionarFranquiciaWindow.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildRequestButton();
        this.buildCancelButton();

        this.buildStoreFranquicias();
        this.buildGridFranquicias();

        this.add(this.gridFranquicias);

        this.getFooterToolbar().addButton(this.requestButton);
        this.getFooterToolbar().addButton(this.cancelButton);
    },

    buildStoreFranquicias : function()
    {
        this.storeFranquicias = new Ext.data.Store(
        {
            url : '/upo/rest/franquicia',
            restful : true,
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Franquicia',
                id : 'id'
            }, [ 'id', 'nombre' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildGridFranquicias : function()
    {
        var ref = this;

        this.gridFranquicias = new Ext.grid.GridPanel(
        {
            flex : 1,
            viewConfig :
            {
                forceFit : true
            },
            selModel : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : this.storeFranquicias,
            colModel : new Ext.grid.ColumnModel(
            {
                columns : [
                {
                    header : "Id",
                    dataIndex : 'id',
                    width : 50

                },
                {
                    header : "Nom Franquícia",
                    dataIndex : 'nombre',
                    width : 100
                } ]
            }),
            listeners :
            {
                dblclick : function()
                {
                    ref.setSelectedId();
                }
            }
        });
    },

    buildRequestButton : function()
    {
        var ref = this;

        this.requestButton = new Ext.Button(
        {
            text : 'Seleccionar',
            handler : function()
            {
                ref.setSelectedId();
            }
        });
    },

    setSelectedId : function()
    {
        var selectedRow = this.gridFranquicias.getSelectionModel().getSelected();

        if (selectedRow)
        {
            this.selectedId = selectedRow.id;
            this.close();
        }
    },

    getSelectedId : function()
    {
        return this.selectedId;
    },

    buildCancelButton : function()
    {
        var ref = this;

        this.cancelButton = new Ext.Button(
        {
            text : 'Cancel·lar',
            handler : function()
            {
                ref.close();
            }
        });
    }
});