Ext.ns('UJI.UPO');

UJI.UPO.PrioridadWindow = Ext.extend(Ext.Window,
{
    title : 'Priotitats',
    layout: 'fit',
    width : 500,
    height : 400,
    modal : true,
    cancelButton : {},
    fbar : [],

    initComponent : function()
    {
        var config =
            {
                mapaId : this.mapaId,
                contenidoId : this.contenidoId,
                administrador : this.administrador,
                tipoContenido : this.tipoContenido
            };

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.PrioridadWindow.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildPrioridadPanel();
        this.buildCancelButton();

        this.getFooterToolbar().addButton(this.cancelButton);

        this.add(this.prioridadPanel);
    },

    buildPrioridadPanel : function()
    {
        this.prioridadPanel = new UJI.UPO.PrioridadPanel({
            mapaId : this.mapaId,
            contenidoId : this.contenidoId,
            administrador : this.administrador,
            tipoContenido : this.tipoContenido,
            autoSave: true
        });
    },

    buildCancelButton : function()
    {
        ref = this;

        this.cancelButton = new Ext.Button(
        {
            text : 'Cancel·lar',
            handler : function()
            {
                ref.close();
            }
        });
    }
});
