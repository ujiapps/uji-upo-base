Ext.ns('UJI.UPO');

UJI.UPO.ContenidoMetadatosUtil =
{
    requestAtributosFromEsquema : function(esquemaId, panels, action)
    {
        var ref = this;
        Ext.Ajax.request(
        {
            url : '/upo/rest/esquema-metadato/' + esquemaId + '/atributo',
            method : 'GET',
            success : function(r)
            {
                fields = ref.buildFieldsFromXml(r);

                ref.doActiontoPanels(panels, function(panel)
                {
                    panel.removeAll();
                });

                for (var i = 0; i < fields.length; i++)
                {
                    ref.addField(fields[i], panels);
                }

                if (action)
                {
                    action();
                }
            },
            failure : function()
            {
                UJI.UPO.Application.showMessageError("Errada recuperant les dades");
            }
        });
    },

    doActiontoPanels : function(panels, action)
    {
        for (var i = 0; i < panels.length; i++)
        {
            action(panels[i]);
        }
    },

    addField : function(params, panels)
    {
        this.doActiontoPanels(panels, function(panel)
        {
            panel.addField(params);
        })
    },

    buildFieldsFromXml : function(r)
    {
        var nodos = Ext.DomQuery.jsSelect('AtributoMetadato', r.responseXML);
        var fields = [];

        for (var i = 0; i < nodos.length; i++)
        {
            fields.push(this.addFieldFromXml(nodos[i]));
        }

        return fields;
    },

    addFieldFromXml : function(nodo)
    {
        var id = Ext.DomQuery.selectValue('id', nodo);
        var key = Ext.DomQuery.selectValue('clave', nodo);
        var tipo = Ext.DomQuery.selectValue('tipoMetadatoId', nodo);
        var permiteBlanco = Ext.DomQuery.selectNumber('permiteBlanco', nodo);
        var publicable = Ext.DomQuery.selectNumber('publicable', nodo);
        var valoresCombo = Ext.DomQuery.jsSelect('valores>value', nodo);

        var nombreClave =
            {
                CA : Ext.DomQuery.selectValue('nombreClaveCA', nodo),
                ES : Ext.DomQuery.selectValue('nombreClaveES', nodo),
                EN : Ext.DomQuery.selectValue('nombreClaveEN', nodo)
            }

        return {
            id : id,
            key : key,
            tipo : tipo,
            permiteBlanco : permiteBlanco,
            valoresCombo : valoresCombo,
            nodo : nodo,
            nuevo : false,
            nombreClave : nombreClave,
            publicable : publicable
        };
    },

    doLayout : function(panels)
    {
        this.doActiontoPanels(panels, function(panel)
        {
            panel.doLayout();
        })
    }
};