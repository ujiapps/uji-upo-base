Ext.ns('UJI.UPO');

UJI.UPO.DocumentosReservori = Ext.extend(Ext.Panel,
{
    id : 'images-view',
    layout : 'vbox',
    region : 'center',
    title : 'Reservori de documents',
    layoutConfig : {
        align : 'stretch'
    },
    store : {},
    grid : {},
    saveButton : {},
    cancelButton : {},
    fbar : [],
    tbar : [],
    searchField : {},
    showAttachedFiles : true,

    selectRow : function(ref)
    {
        var rec = ref.grid.getSelectionModel().getSelected();

        if (rec)
        {
            var url = rec.get("url");

            window.opener.CKEDITOR.tools.callFunction(ref.funcNum, url);
            window.close();
        }
    },

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.DocumentosReservori.superclass.initComponent.call(this);

        this.initUI();
        this.add(this.grid);

        this.getFooterToolbar().addButton(this.saveButton);
        this.getFooterToolbar().addButton(this.cancelButton);

        this.getTopToolbar().add(this.searchField);
        this.getTopToolbar().add('-');
        this.getTopToolbar().add(
        {
            text : 'Configuració recerca',
            iconCls : 'cog',
            menu : this.configMenu
        });
        this.getTopToolbar().add('->');
        this.getTopToolbar().add(this.addButton);
        this.getTopToolbar().add(this.updateForm);
        this.getTopToolbar().add(this.deleteButton);
    },

    initUI : function()
    {
        this.buildStore();
        this.buildSaveButton();
        this.buildCancelButton();

        this.buildSearchField();
        this.buildSearchCheck();
        this.buildSearchAttachedFilesCheck();
        this.buildConfigMenu();

        this.buildGrid();
        this.buildUploadButton();
        this.buildUpdateForm();
        this.buildDeleteButton();
    },

    buildStore : function()
    {
        var ref = this;

        ref.store = new Ext.data.Store(
        {
            url : 'rest/reservori/recursos/documentos',
            baseParams : {
                query : '*',
                urlNode : ref.urlNode,
                urlNodeSearch : ref.urlNode,
                showAttachedFiles : ref.showAttachedFiles,
                idioma : ref.idioma
            },
            restful : true,
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'RecursoReservori',
                totalProperty : 'pagination>numDocumentos'
            }, ['url', 'nombre', 'typeMime', 'id', 'gestionable']),
            listeners : {
                exception : function(self, type, action, request, response)
                {
                    Ext.MessageBox.show(
                    {
                        title : 'Error',
                        msg : 'Errada cercant en el repositori. Potser es deu a una cerca massa general',
                        buttons : Ext.MessageBox.OK,
                        icon : Ext.MessageBox.ERROR
                    });
                }
            }
        });

        ref.mask = new Ext.LoadMask(Ext.getBody(),
        {
            msg : 'Carregant els fitxers',
            store : ref.store
        });
    },

    buildCancelButton : function()
    {
        var ref = this;

        ref.cancelButton = new Ext.Button(
        {
            text : 'Cancel·lar',
            iconCls : 'btn-rojo',
            handler : function()
            {
                window.close();
            }
        });
    },

    buildSaveButton : function()
    {
        var ref = this;

        ref.saveButton = new Ext.Button(
        {
            text : 'Inserir enllaç',
            iconCls : 'btn-verde',
            handler : function()
            {
                ref.selectRow(ref);
            }
        });
    },

    buildConfigMenu : function()
    {
        this.configMenu = new Ext.menu.Menu(
        {
            style : {
                overflow : 'visible'
            },
            items : [this.searchCheck, this.searchAttachedFilesCheck]
        });
    },

    buildSearchCheck : function()
    {
        var ref = this;

        this.searchCheck = new Ext.form.Checkbox(
        {
            boxLabel : "Recerca només en la carpeta i descendents",
            checked : true,
            listeners : {
                check : function(check, checked)
                {
                    var urlNode = '/';

                    if (checked)
                    {
                        urlNode = ref.urlNode;
                    }

                    ref.store.setBaseParam('urlNodeSearch', urlNode);
                    ref.store.setBaseParam('urlNode', ref.urlNode);
                }
            }
        });
    },

    buildSearchAttachedFilesCheck : function()
    {
        var ref = this;

        this.searchAttachedFilesCheck = new Ext.form.Checkbox(
        {
            boxLabel : "Mostra documents adjunts",
            checked : true,
            listeners : {
                check : function(check, checked)
                {
                    ref.store.setBaseParam('showAttachedFiles', checked);
                }
            }
        });
    },

    buildSearchField : function()
    {
        var ref = this;

        ref.searchField = new Ext.ux.form.SearchField(
        {
            emptyText : 'Recerca...',
            store : ref.store,
            width : 180
        });
    },

    buildDeleteButton : function()
    {
        var ref = this;

        ref.deleteButton = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'document-delete-button',
            disabled : true,
            handler : function()
            {
                ref.deleteSelectRow();
            }
        });
    },

    deleteSelectRow : function()
    {
        var ref = this;

        var rec = ref.grid.getSelectionModel().getSelected();

        if (rec)
        {
            var id = rec.get("id");

            Ext.Ajax.request(
            {
                url : 'rest/reservori/recursos',
                method : 'DELETE',
                params : {
                    id : id
                },
                success : function()
                {
                    ref.searchField.setValue("*");
                    ref.store.setBaseParam('query', '*');
                    ref.store.load();
                },
                failure : function()
                {
                    Ext.MessageBox.show(
                    {
                        title : 'Errada',
                        msg : "Errada al esborrar el document del repositori",
                        buttons : Ext.MessageBox.OK,
                        icon : Ext.MessageBox.ERROR
                    });
                }
            });
        }
    },

    buildUploadButton : function()
    {
        var ref = this;

        ref.addButton = new Ext.Button(
        {
            text : 'Pujar',
            iconCls : 'document-add-button',
            handler : function()
            {
                var window = new Ext.ux.uji.FileUploader(
                {
                    url : 'rest/reservori/recursos',
                    extraData : {
                        urlNode : ref.urlNode
                    }
                });

                window.show();

                window.on("close", function()
                {
                    var fileNames = window.getFileNames();
                    var fileName;

                    if (fileNames.length > 0)
                    {
                        fileName = fileNames[0];
                    }

                    if (fileName)
                    {
                        ref.searchField.setValue(fileName);
                        ref.searchField.onTrigger2Click();
                    }
                });
            }
        });
    },

    buildUpdateForm : function()
    {
        var ref = this;

        ref.uploadfile = new Ext.ux.form.FileUploadField(
        {
            buttonOnly : true,
            disabled : true,
            buttonCfg : {
                text : '<div class="document-edit-button">Actualitzar</div>',
                style : {
                    paddingTop : '2px'
                }
            },
            name : 'file',
            listeners : {
                'fileselected' : function()
                {
                    var rec = ref.grid.getSelectionModel().getSelected();

                    if (rec)
                    {
                        ref.el.mask('Substituint el document', 'x-mask-loading');
                        var id = rec.get("id");

                        ref.updateForm.getForm().submit(
                        {
                            method : 'POST',
                            params : {
                                urlNode : ref.urlNode,
                                id : id
                            },
                            url : 'rest/reservori/recursos/replace',
                            success : function(form, action)
                            {
                                ref.el.unmask();
                                ref.searchField.setValue("*");
                                ref.store.setBaseParam('query', '*');
                                ref.store.load();

                            },
                            failure : function()
                            {
                                ref.el.unmask();

                                Ext.MessageBox.show(
                                {
                                    title : 'Errada',
                                    msg : "Errada al desar el document al repositori",
                                    buttons : Ext.MessageBox.OK,
                                    icon : Ext.MessageBox.ERROR
                                });
                            }
                        });
                    }
                }
            }
        });

        ref.updateForm = new Ext.FormPanel(
        {
            fileUpload : true,
            labelWidth : 1,
            items : [ref.uploadfile],
            border : false,
            enctype : 'multipart/form-data',
            bodyStyle : 'background-color: transparent;',
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, ['success', 'msg'])
        });
    },

    buildGrid : function()
    {
        var ref = this;

        ref.grid = new Ext.grid.GridPanel(
        {
            store : ref.store,
            flex : 1,
            singleSelect : true,
            viewConfig : {
                forceFit : true
            },
            selModel : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            emptyText : 'No hi ha documents per a mostrar',
            columns : [
                {
                    header : '',
                    dataIndex : 'typeMime',
                    width : 10,
                    align : 'center',
                    menuDisabled : true,
                    renderer : function(value, p, record)
                    {
                        var mime = record.data.typeMime;
                        reservoriIconos = UJI.UPO.IconosDocumentosReservori;

                        if (reservoriIconos[mime])
                        {
                            return '<img src="/upo/img/' + reservoriIconos[mime] + '" alt="icon" title="icon" />';
                        }

                        return '<img src="/upo/img/document_icon.png" alt="icon" title="icon" />';
                    }
                },
                {
                    header : 'Documento',
                    dataIndex : 'nombre',
                    width : 70,
                    sortable : true,
                    renderer : function(value, p, record)
                    {
                        if (record.data.gestionable === 'true')
                        {
                            return value;
                        }

                        return '<span style="color: red">' + value + '</span>';
                    }
                },
                {
                    header : 'URL',
                    dataIndex : 'url',
                    sortable : true,
                    renderer : function(value, p, record)
                    {
                        var url = record.data.url;

                        return '<a href="' + url + '" target="_blank">' + url + '</a>';
                    }
                },
                {
                    header : 'id',
                    dataIndex : 'id',
                    hidden : true
                }],

            listeners : {
                rowdblclick : function(grid, index, event)
                {
                    ref.selectRow(ref);
                },
                rowclick : function(grid, index, event)
                {
                    var rec = ref.grid.getSelectionModel().getSelected();

                    ref.uploadfile.setDisabled(rec.data.gestionable === 'false');
                    ref.deleteButton.setDisabled(rec.data.gestionable === 'false');
                }
            },
            bbar : [new Ext.PagingToolbar(
            {
                pageSize : 25,
                store : ref.store,
                displayInfo : true,
                displayMsg : 'Visualizando {0} - {1} de {2}',
                emptyMsg : 'No hay registros'
            })]
        });
    }
});
