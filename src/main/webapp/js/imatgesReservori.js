Ext.ns('UJI.UPO');

UJI.UPO.ImagenesReservori = Ext.extend(Ext.Panel,
{
    id : 'images-view',
    layout : 'border',
    region : 'center',
    title : 'Reservori d\'imatges',
    layoutConfig : {
        align : 'stretch'
    },
    store : {},
    tpl : {},
    dataView : {},
    panelDetalles : {},
    saveButton : {},
    cancelButton : {},
    fbar : [],
    tbar : [],
    bbar : [],
    searchField : {},
    showAttachedFiles : true,

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.ImagenesReservori.superclass.initComponent.call(this);

        this.initUI();

        this.add(this.dataView);
        this.add(this.panelDetalles);

        this.getBottomToolbar().add(this.paging);

        this.getFooterToolbar().addButton(this.saveButton);
        this.getFooterToolbar().addButton(this.cancelButton);

        this.getTopToolbar().add(this.searchField);
        this.getTopToolbar().add('-');
        this.getTopToolbar().add(
        {
            text : 'Configuració recerca',
            iconCls : 'cog',
            menu : this.configMenu
        });

        this.getTopToolbar().add('->');
        this.getTopToolbar().add(this.addButton);
        this.getTopToolbar().add(this.updateForm);
        this.getTopToolbar().add(this.deleteButton);
    },

    initUI : function()
    {
        this.buildStore();
        this.buildTpls();
        this.buildSaveButton();
        this.buildCancelButton();

        this.buildSearchField();
        this.buildSearchCheck();
        this.buildSearchAttachedFilesCheck();
        this.buildConfigMenu();

        this.buildDataView();
        this.buildPanelDetalles();
        this.buildAddButton();
        this.buildUpdateForm();
        this.buildDeleteButton();
        this.buildPaging();
    },

    buildStore : function()
    {
        var ref = this;

        ref.store = new Ext.data.Store(
        {
            url : 'rest/reservori/recursos/imagenes',
            baseParams : {
                query : '*',
                urlNode : ref.urlNode,
                urlNodeSearch : ref.urlNode,
                showAttachedFiles : ref.showAttachedFiles,
                idioma : ref.idioma
            },
            restful : true,
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'RecursoReservori',
                totalProperty : 'pagination>numDocumentos'
            }, ['url', 'urlThumbnail', 'nombre', 'id', 'gestionable']),
            listeners : {
                exception : function(self, type, action, request, response)
                {
                    Ext.MessageBox.show(
                    {
                        title : 'Error',
                        msg : 'Errada cercant en el repositori. Potser es deu a una cerca massa general',
                        buttons : Ext.MessageBox.OK,
                        icon : Ext.MessageBox.ERROR
                    });
                }
            }
        });

        ref.mask = new Ext.LoadMask(Ext.getBody(),
        {
            msg : 'Carregant les imatges',
            store : ref.store
        });
    },

    buildTpls : function()
    {
        var ref = this;

        ref.tpl = new Ext.XTemplate('<tpl for=".">', '<div class="thumb-wrap" title="{nombre}" id="{nombre}">', '<div class="thumb"><img src="{urlThumbnail}" title="{nombre}"></div>',
        '<tpl if="gestionable == \'true\'"><span>{shortName}</span></tpl><tpl if="gestionable != \'true\'"><span class="color-red">{shortName}</span></tpl></div>', '</tpl>', '<div class="x-clear"></div>');

        ref.tplDetails = new Ext.XTemplate('<tpl for="."><span style="font-weight: bold">[{type}] {key}</span><br/><span style="padding-left: 10px">{value}</span><br/></tpl>');

        ref.tpl.compile();
        ref.tplDetails.compile();
    },

    buildPanelDetalles : function()
    {
        this.panelDetalles = new Ext.Panel(
        {
            title : 'Metadades',
            plugins : [Ext.ux.PanelCollapsedTitle],
            region : 'east',
            width : 175,
            border : false,
            autoScroll : true,
            collapsible : true,
            collapsed : true
        });
    },

    buildCancelButton : function()
    {
        var ref = this;

        ref.cancelButton = new Ext.Button(
        {
            text : 'Cancel·lar',
            iconCls : 'btn-rojo',
            handler : function()
            {
                window.close();
            }
        });
    },

    buildSaveButton : function()
    {
        var ref = this;

        ref.saveButton = new Ext.Button(
        {
            text : 'Inserir imatge',
            iconCls : 'btn-verde',
            handler : function()
            {
                var index = ref.dataView.getSelectedIndexes();

                if (index.length > 0)
                {
                    var url = ref.store.data.items[index].get("url");

                    window.opener.CKEDITOR.tools.callFunction(ref.funcNum, url);
                    window.close();
                }
            }
        });
    },

    buildConfigMenu : function()
    {
        this.configMenu = new Ext.menu.Menu(
        {
            style : {
                overflow : 'visible'
            },
            items : [this.searchCheck, this.searchAttachedFilesCheck]
        });
    },

    buildSearchCheck : function()
    {
        var ref = this;

        this.searchCheck = new Ext.form.Checkbox(
        {
            boxLabel : "Recerca només en la carpeta i descendents",
            checked : true,
            listeners : {
                check : function(check, checked)
                {
                    var urlNode = '/';

                    if (checked)
                    {
                        urlNode = ref.urlNode;
                    }

                    ref.store.setBaseParam('urlNodeSearch', urlNode);
                    ref.store.setBaseParam('urlNode', ref.urlNode);
                }
            }
        });
    },

    buildSearchAttachedFilesCheck : function()
    {
        var ref = this;

        this.searchAttachedFilesCheck = new Ext.form.Checkbox(
        {
            boxLabel : "Mostra documents adjunts",
            checked : true,
            listeners : {
                check : function(check, checked)
                {
                    ref.store.setBaseParam('showAttachedFiles', checked);
                }
            }
        });
    },

    buildSearchField : function()
    {
        var ref = this;

        ref.searchField = new Ext.ux.form.SearchField(
        {
            emptyText : 'Recerca...',
            store : ref.store,
            width : 180
        });
    },

    buildDeleteButton : function()
    {
        var ref = this;

        ref.deleteButton = new Ext.Button(
        {
            text : 'Esborrar',
            disabled : true,
            iconCls : 'image-delete-button',
            handler : function()
            {
                ref.deleteSelectRow();
            }
        });
    },

    deleteSelectRow : function()
    {
        var ref = this;

        var index = ref.dataView.getSelectedIndexes();

        if (index.length > 0)
        {
            var id = ref.store.data.items[index].get("id");

            Ext.Ajax.request(
            {
                url : 'rest/reservori/recursos',
                method : 'DELETE',
                params : {
                    id : id
                },
                success : function()
                {
                    ref.searchField.setValue("*");
                    ref.store.setBaseParam('query', '*');
                    ref.store.load();
                },
                failure : function()
                {
                    Ext.MessageBox.show(
                    {
                        title : 'Errada',
                        msg : "Errada al esborrar la imatge del repositori",
                        buttons : Ext.MessageBox.OK,
                        icon : Ext.MessageBox.ERROR
                    });
                }
            });
        }
    },

    buildAddButton : function()
    {
        var ref = this;

        ref.addButton = new Ext.Button(
        {
            text : 'Pujar',
            iconCls : 'image-add-button',
            handler : function()
            {
                var window = new Ext.ux.uji.FileUploader(
                {
                    url : 'rest/reservori/imagenes',
                    accept : 'image/*',
                    extraData : {
                        urlNode : ref.urlNode
                    }
                });

                window.show();

                window.on("close", function()
                {
                    var fileNames = window.getFileNames();
                    var fileName;

                    if (fileNames.length > 0)
                    {
                        fileName = fileNames[0];
                    }

                    if (fileName)
                    {
                        ref.searchField.setValue(fileName);
                        ref.searchField.onTrigger2Click();
                    }
                });
            }
        });
    },

    buildUpdateForm : function()
    {
        var ref = this;

        ref.uploadfile = new Ext.ux.form.FileUploadField(
        {
            buttonOnly : true,
            disabled : true,
            buttonCfg : {
                text : '<div class="image-edit-button">Actualitzar</div>',
                style : {
                    paddingTop : '2px'
                }
            },
            name : 'file',
            listeners : {
                'fileselected' : function()
                {
                    var index = ref.dataView.getSelectedIndexes();

                    if (index.length > 0)
                    {
                        ref.el.mask('Substituint la imatge', 'x-mask-loading');
                        var id = ref.store.data.items[index].get("id");

                        ref.updateForm.getForm().submit(
                        {
                            method : 'POST',
                            params : {
                                urlNode : ref.urlNode,
                                id : id
                            },
                            url : 'rest/reservori/imagenes/replace',
                            success : function(form, action)
                            {
                                ref.el.unmask();
                                ref.searchField.setValue("*");
                                ref.store.setBaseParam('query', '*');
                                ref.store.load();

                            },
                            failure : function()
                            {
                                ref.el.unmask();

                                Ext.MessageBox.show(
                                {
                                    title : 'Errada',
                                    msg : "Errada al desar la imatge al repositori",
                                    buttons : Ext.MessageBox.OK,
                                    icon : Ext.MessageBox.ERROR
                                });
                            }
                        });
                    }
                }
            }
        });

        ref.updateForm = new Ext.FormPanel(
        {
            fileUpload : true,
            labelWidth : 1,
            items : [ref.uploadfile],
            border : false,
            enctype : 'multipart/form-data',
            bodyStyle : 'background-color: transparent;',
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, ['success', 'msg'])
        });
    },

    buildDataView : function()
    {
        var ref = this;

        ref.dataView = new Ext.DataView(
        {
            store : ref.store,
            tpl : ref.tpl,
            flex : 1,
            region : 'center',
            singleSelect : true,
            autoScroll : true,
            overClass : 'x-view-over',
            itemSelector : 'div.thumb-wrap',
            emptyText : 'No hi ha imatges per a mostrar',

            plugins : [new Ext.DataView.DragSelector(), new Ext.DataView.LabelEditor(
            {
                dataIndex : 'name'
            })],

            prepareData : function(data)
            {
                data.shortName = Ext.util.Format.ellipsis(data.nombre, 15);
                return data;
            },

            listeners : {
                dblclick : function(dataView, index, node, event)
                {
                    var url = ref.store.data.items[index].get("url");

                    window.opener.CKEDITOR.tools.callFunction(ref.funcNum, url);
                    window.close();
                },
                click : function(grid, index, event)
                {
                    var gest = ref.store.data.items[index].get("gestionable");
                    var id = ref.store.data.items[index].get("id");
                    var gestionable = ref.store.data.items[index].get("gestionable");

                    ref.uploadfile.setDisabled(gest === 'false');
                    ref.deleteButton.setDisabled(gest === 'false');

                    Ext.Ajax.request(
                    {
                        url : '/upo/rest/reservori/recursos/imagenes/metadatos',
                        params : {
                            id : id,
                            gestionable : gestionable
                        },
                        method : 'PUT',
                        success : function(r)
                        {
                            ref.panelDetalles.update(ref.xmlToHtml(r.responseXML.documentElement));
                        },
                        failure : function()
                        {
                            ref.showMessageError("Errada recuperant les metadades");
                        }
                    });
                }
            }
        });
    },

    xmlToHtml : function(xml)
    {
        var base = xml.childNodes;
        array = [];

        for (var j = 0; j < base.length; j++)
        {
            if (base[j].nodeType == 1)
            {
                var value = {};

                value["key"] = Ext.DomQuery.selectValue('key', base[j]);
                value["type"] = Ext.DomQuery.selectValue('type', base[j]);
                value["value"] = Ext.DomQuery.selectValue('value', base[j]);

                array.push(value);
            }
        }

        return this.tplDetails.apply(array);
    },

    buildPaging : function()
    {
        var ref = this;

        this.paging = new Ext.PagingToolbar(
        {
            pageSize : 20,
            store : ref.store,
            displayInfo : true,
            displayMsg : 'Visualizando {0} - {1} de {2}',
            emptyMsg : 'No hay registros'
        });
    }
});
