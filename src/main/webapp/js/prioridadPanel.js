Ext.ns('UJI.UPO');

UJI.UPO.PrioridadPanel = Ext.extend(Ext.Panel,
{
    layout : 'fit',
    viewConfig : {
        forceFit : true
    },
    padding : 5,
    prioridadGrid : {},
    addButton : {},
    deleteButton : {},
    editor : {},
    colModel : [],
    store : {},
    comboPrioridades : {},
    pluginList : [],

    initComponent : function()
    {
        var config =
            {
                mapaId : this.mapaId,
                contenidoId : this.contenidoId,
                administrador : this.administrador,
                tipoContenido : this.tipoContenido,
                autoSave : this.autoSave
            };

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.PrioridadPanel.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildComboPrioridades();
        this.buildAddButton();
        this.buildDeleteButton();
        this.buildEditor();
        this.buildColModel();
        this.buildStore();

        if (this.administrador && this.tipoContenido == UJI.UPO.TipoReferenciaContenido.NORMAL)
        {
            this.enableEdit();
        }

        this.buildPrioridadGrid();

        this.prioridadGrid.getTopToolbar().addButton(this.addButton);
        this.prioridadGrid.getTopToolbar().addButton(this.deleteButton);

        if (this.load && this.contenidoId)
        {
            this.reload(this.contenidoId);
        }

        this.add(this.prioridadGrid);
    },

    enableEdit : function()
    {
        this.addButton.enable();
        this.deleteButton.enable();
        this.pluginList = [this.editor];
    },

    reload : function(contenidoId)
    {
        if (!contenidoId)
        {
            return;
        }

        this.store.proxy.setUrl('/upo/rest/contenido/' + contenidoId + '/prioridad');
        this.store.load();
    },

    comboRenderer : function(combo)
    {
        return function(value)
        {
            var record = combo.findRecord(combo.valueField, value);
            return record ? initCap(record.get(combo.displayField)) : combo.valueNotFoundText;
        };
    },

    timefieldRenderer : function(format)
    {
        return function(v)
        {
            if (v instanceof Date)
            {
                return v.format(format);
            }
            else
            {
                return v;
            }
        };
    },

    comboPrioridadesData : function()
    {
        var result = [];

        for (var it in UJI.UPO.TipoPrioridadContenido)
        {
            result.push([it, initCap(UJI.UPO.TipoPrioridadContenido[it])]);
        }

        return result;
    },

    buildColModel : function()
    {
        var ref = this;

        this.colModel = new Ext.grid.ColumnModel([
            {
                header : 'Id',
                hidden : true,
                dataIndex : 'id'
            },
            {
                header : 'Contingut',
                hidden : true,
                dataIndex : 'contenidoId'
            },
            {
                xtype : 'datecolumn',
                header : 'Data inici',
                dataIndex : 'fechaInicio',
                format : 'd/m/Y',
                editor : new Ext.form.DateField(
                {
                    format : 'd/m/Y'
                })
            },
            {
                header : 'Hora inici',
                dataIndex : 'horaInicio',
                renderer : ref.timefieldRenderer('H:i'),
                editor : {
                    xtype : 'timefield',
                    format : 'H:i'
                }
            },
            {
                xtype : 'datecolumn',
                header : 'Fecha Fi',
                dataIndex : 'fechaFin',
                format : 'd/m/Y',
                editor : new Ext.form.DateField(
                {
                    format : 'd/m/Y'
                })
            },
            {
                header : 'Hora fi',
                dataIndex : 'horaFin',
                renderer : ref.timefieldRenderer('H:i'),
                editor : {
                    xtype : 'timefield',
                    format : 'H:i'
                }
            },
            {
                header : 'Prioritat',
                dataIndex : 'prioridad',
                editor : ref.comboPrioridades,
                renderer : ref.comboRenderer(ref.comboPrioridades)
            }
        ]);
    },

    buildStore : function()
    {
        var prioridadGrid = this;
        this.store = new Ext.data.Store(
        {
            proxy : new Ext.data.HttpProxy(
            {
                url : '/upo/rest/contenido/' + prioridadGrid.contenidoId + '/prioridad',
                listeners : {
                    beforewrite : function()
                    {
                        var url = '/upo/rest/contenido/' + prioridadGrid.contenidoId + '/prioridad';
                        prioridadGrid.store.proxy.setUrl(url);
                    }
                }
            }),
            restful : true,
            autoSave : prioridadGrid.autoSave,
            reader : new Ext.data.XmlReader(
            {
                record : 'PrioridadContenido',
                id : 'id'
            }, [
                'id', 'contenidoId',
                {
                    name : 'fechaInicio',
                    type : 'date',
                    dateFormat : 'd/m/Y H:i:s'
                },
                {
                    name : 'fechaFin',
                    type : 'date',
                    dateFormat : 'd/m/Y H:i:s'
                },
                {
                    name : 'horaInicio',
                    type : 'date',
                    dateFormat : 'd/m/Y H:i:s'
                },
                {
                    name : 'horaFin',
                    type : 'date',
                    dateFormat : 'd/m/Y H:i:s'
                },
                {
                    name : 'prioridad',
                    allowBlank : false
                }
            ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildPrioridadGrid : function()
    {
        var ref = this;

        this.prioridadGrid = new Ext.grid.GridPanel(
        {
            title : this.titleGrid,
            flex : 1,
            viewConfig : {
                forceFit : true,
                markDirty : false
            },
            selModel : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            plugins : ref.pluginList,
            store : ref.store,
            colModel : ref.colModel,
            tbar : {}
        });
    },

    buildAddButton : function()
    {
        var ref = this;
        this.addButton = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            disabled : true,
            handler : function()
            {
                ref.enableMandatoryFields();

                var rec = new ref.store.recordType(
                {
                    id : '',
                    contenidoId : ref.contenidoId,
                    fechaInicio : (new Date()).clearTime(),
                    fechaFin : '',
                    prioridad : ''
                });

                ref.editor.stopEditing();
                ref.store.insert(0, rec);
                ref.editor.startEditing(0);
            }
        });
    },

    buildDeleteButton : function()
    {
        var ref = this;
        this.deleteButton = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            disabled : true,
            handler : function()
            {
                var rec = ref.prioridadGrid.getSelectionModel().getSelected();
                if (!rec)
                {
                    return false;
                }
                ref.store.remove(rec);
            }
        });
    },

    buildEditor : function()
    {
        var ref = this;

        this.editor = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancel·lar',
            errorSummary : false,
            monitorValid : false,
            listeners : {
                canceledit : function(rowEditor, state)
                {
                    var record = rowEditor.record;

                    if (record.phantom && !record.data.prioridad)
                    {
                        ref.store.remove(rowEditor.record);
                        ref.reset();
                    }
                },
                beforeedit : function(rowEditor)
                {
                    ref.enableMandatoryFields();
                }
            }
        });
    },

    enableMandatoryFields : function()
    {
        this.comboPrioridades.allowBlank = false;
    },

    reset : function()
    {
        this.comboPrioridades.allowBlank = true;
    },

    buildComboPrioridades : function()
    {
        var ref = this;

        this.comboPrioridades = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            valueField : 'id',
            displayField : 'nombre',
            allowBlank : false,
            mode : 'local',
            forceSelection : true,
            editable : false,
            store : new Ext.data.ArrayStore(
            {
                autoLoad : true,
                fields : ['id', 'nombre'],
                data : ref.comboPrioridadesData()
            })
        });
    }
});
