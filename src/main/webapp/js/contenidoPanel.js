Ext.ns('UJI.UPO');

UJI.UPO.ContenidoPanel = Ext.extend(Ext.Panel,
{
    title : 'Continguts',
    layout : 'card',
    frame : true,
    activeItem : 0,
    contenidoForm : {},
    contenidoGrid : {},
    prioridadGrid : {},
    prioridadWindow : {},

    setAdmin : function(administrador)
    {
        this.contenidoGrid.setAdmin(administrador);
        this.contenidoForm.setAdmin(administrador);
    },

    setIdiomasObligatorios : function(idiomasObligatorios)
    {
        this.contenidoForm.setIdiomasObligatorios(idiomasObligatorios);
    },

    initComponent : function()
    {
        var config =
        {
            mapaId : this.mapaId,
            mapaName : this.mapaName,
            mapaUrlCompleta : this.mapaUrlCompleta
        };

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.ContenidoPanel.superclass.initComponent.call(this);

        this.initUI();
        this.add(this.contenidoGrid);
        this.add(this.contenidoForm);

        UJI.Bus.subscribe('UJI.UPO.ContenidoForm.Button.Cancel', this);
        UJI.Bus.subscribe('UJI.UPO.ContenidoGrid.Button.Edit', this);
        UJI.Bus.subscribe('UJI.UPO.ContenidoGrid.Button.Add', this);
        UJI.Bus.subscribe('UJI.UPO.MigracionUrlGrid.Drop', this);
        UJI.Bus.subscribe('UJI.UPO.Tree.NodeRename', this);
        UJI.Bus.subscribe('UJI.UPO.Tree.NodeDragAndDrop', this);
    },

    initUI : function()
    {
        this.buildContenidoGrid();
        this.buildContenidoForm();
    },

    onMessage : function(message, params)
    {
        if (params.mapaId == this.mapaId)
        {
            if (message == 'UJI.UPO.ContenidoForm.Button.Cancel')
            {
                this.layout.setActiveItem(0);
            }

            if (message == 'UJI.UPO.ContenidoGrid.Button.Edit')
            {
                this.layout.setActiveItem(1);
            }

            if (message == 'UJI.UPO.ContenidoGrid.Button.Add' || message == 'UJI.UPO.MigracionUrlGrid.Drop')
            {
                this.layout.setActiveItem(1);

                if (this.contenidoForm.form && this.contenidoForm.form.getForm())
                {
                    this.contenidoForm.form.getForm().reset();
                    var ref = this;

                    setTimeout(function()
                    {
                        ref.contenidoForm.form.body.scrollTo('top', 0);
                    }, 100);
                }
            }
        }

        if (message == 'UJI.UPO.Tree.NodeRename')
        {
            if (this.mapaUrlCompleta.indexOf(params.urlCompletaAnterior) == 0)
            {
                var posACortar = params.urlCompletaAnterior.length;
                var cadenaAGuardar = this.mapaUrlCompleta.substring(posACortar);
                var nuevaUrlCompleta = params.urlCompletaNueva + cadenaAGuardar;

                this.mapaUrlCompleta = nuevaUrlCompleta;
                this.mapaName = params.nombreNodoNuevo;

                this.contenidoGrid.setMapaUrlCompleta(nuevaUrlCompleta);
                this.contenidoForm.setMapaUrlCompleta(nuevaUrlCompleta);

                this.contenidoGrid.setMapaNombreNodo(this.mapaName);
            }
        }

        if (message == 'UJI.UPO.Tree.NodeDragAndDrop')
        {
            if (this.mapaUrlCompleta.indexOf(params.urlCompletaNodoOrigen) == 0)
            {
                var posACortar = params.urlBaseNodoOrigen.length;
                var cadenaAGuardar = this.mapaUrlCompleta.substring(posACortar);
                var nuevaUrlCompleta = params.urlCompletaNodoDestino + cadenaAGuardar;

                this.mapaUrlCompleta = nuevaUrlCompleta;

                this.contenidoGrid.setMapaUrlCompleta(nuevaUrlCompleta);
                this.contenidoForm.setMapaUrlCompleta(nuevaUrlCompleta);
            }
        }
    },

    buildContenidoGrid : function()
    {
        this.contenidoGrid = new UJI.UPO.ContenidoGrid(
        {
            mapaId : this.mapaId,
            mapaName : this.mapaName,
            mapaUrlCompleta : this.mapaUrlCompleta,
            treePanel : this.treePanel
        });
    },

    buildContenidoForm : function()
    {
        this.contenidoForm = new UJI.UPO.ContenidoForm(
        {
            mapaId : this.mapaId,
            mapaUrlCompleta : this.mapaUrlCompleta,
            mapaName : this.mapaName,
            treePanel : this.treePanel
        });
    }
});