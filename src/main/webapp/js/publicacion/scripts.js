var formatNumber = function(number)
{
    if (number < 10)
    {
        number = '0' + number;
    }

    return number;
}

var formatDate = function(timestamp)
{
    var time = new Date(+timestamp);

    return formatNumber(time.getDate()) + "/" + formatNumber((time.getMonth() + 1)) + "/" + time.getFullYear();
}

var formatTime = function(timestamp)
{
    var time = new Date(+timestamp);

    return formatNumber(time.getHours()) + ":" + formatNumber(time.getMinutes()) + ":" + formatNumber(time.getSeconds());
}

var setTimeFromServer = function(tags)
{
    $.ajax(
    {
        url : "/upo/rest/time"
    }).done(function(data)
    {
        var serverTime = data;
        var localTime = new Date().getTime();
        var timeDiff = serverTime - localTime;

        setInterval(function()
        {
            var time = new Date(new Date().getTime() + timeDiff);

            tags.tagServerDay.html(formatNumber(time.getDate()));
            tags.tagServerMonth.html(formatNumber((time.getMonth() + 1)));
            tags.tagServerYear.html(time.getFullYear());
            tags.tagServerHour.html(formatNumber(time.getHours()));
            tags.tagServerMinut.html(formatNumber(time.getMinutes()));
            tags.tagServerSecond.html(formatNumber(time.getSeconds()))

            tags.tagServerTime.show();
        }, 1000);
    });
}

var syncTimeFromServer = function()
{
    if (document.getElementById("server-datetime") === null)
    {
        return;
    }

    var tags =
        {
            tagServerDay : $('#server-day'),
            tagServerMonth : $('#server-month'),
            tagServerYear : $('#server-year'),
            tagServerHour : $('#server-hour'),
            tagServerMinut : $('#server-minut'),
            tagServerSecond : $('#server-second'),
            tagServerTime : $('#server-datetime')
        }

    setTimeFromServer(tags);

    setInterval(function()
    {
        setTimeFromServer(tags)
    }, 1000 * 60 * 15);
}

var setAuthenticatedLinks = function()
{
    $.ajax(
    {
        url : "/upo/rest/authenticated",
        data : {
            url : window.location.pathname
        },
        dataType: "json",
        success : function(json)
        {
            if (json.data.edicion === 'true')
            {
                var url = '/upo?url=' + window.location.pathname;
                $('body').prepend('<a target="_blank" href="' + url + '">Editar</a>');
            }

            if (json.data.autenticado === 'true')
            {
                $('#disconnect-button').show();
                $('#share-header-bar').toggleClass('tc','tl')
            }
        }
    });
}

var slideShow = function()
{
    $('.slideshow').each(function()
    {
        $(this).css('display', 'block');
    });

    $('.slideshow').each(function(num, slide)
    {
        var links = $(slide).find('a');

        var max = -1;
        links.each(function(i, link)
        {
            var h = $(link).find('img').height();
            var resumen = $(link).next('div');

            if (resumen.height())
            {
                h += resumen.height() + 10;
            }

            max = h > max ? h : max;
        });

        $(slide).css('min-height', max || 'px');
    });

    $('.slideshow').cycle(
    {
        fx : 'fade'
    });
}

var togglePlaceHolderSelect = function(select)
{
    if (!select.val())
    {
        $('a.current', select.next()).addClass("select-placeholder")
    } else
    {
        $('a.current', select.next()).removeClass("select-placeholder")
    }
}

var initTogglePlaceHolderSelect = function()
{
    setTimeout(function()
    {
        $('form.custom select.select-placeholder').each(function()
        {
            togglePlaceHolderSelect($(this));
        })
    }, 50);

    $('form.custom select.select-placeholder').change(function()
    {
        togglePlaceHolderSelect($(this));
    });
}

var trimOptGroupSelect = function(select)
{
    var current = $('a.current', select.next());
    setTimeout(function()
    {
        current.text(current.text().trim());
    }, 50);
}

var initToggleOptGroupSelect = function()
{
    $('form.custom select.select-optgroup').each(function()
    {
        trimOptGroupSelect($(this));
    });

    $('form.custom select.select-optgroup').change(function()
    {
        trimOptGroupSelect($(this));
    });
}

var initCustomSelects = function()
{
    initTogglePlaceHolderSelect();
    initToggleOptGroupSelect();
}

$(document).ready(function()
{
    setAuthenticatedLinks();
    syncTimeFromServer();
    initCustomSelects();
});

$(window).load(function()
{
    slideShow();
});