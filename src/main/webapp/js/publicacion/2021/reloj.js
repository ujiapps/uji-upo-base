var secondInterval;
var formatNumber = function(number)
{
    if (number < 10)
    {
        number = '0' + number;
    }

    return number;
}

var setTimeFromServer = function()
{
    var dateTag = $('#date');
    var timeTag = $('#clock');
    var digit_to_name = 'zero one two three four five six seven eight nine'.split(' ');
    var digits = {};
    var positions = [
        'h1', 'h2', ':', 'm1', 'm2', ':', 's1', 's2'
    ];
    var digit_holder = timeTag.find('.digits');

    digit_holder.html('');

    $.each(positions, function()
    {
        if (this == ':')
        {
            digit_holder.append('<div class="dots">');
        } else
        {
            var pos = $('<div>');

            for (var i = 1; i < 8; i++)
            {
                pos.append('<span class="d' + i + '">');
            }

            digits[this] = pos;

            digit_holder.append(pos);
        }
    });

    $.ajax(
    {
        url : "//www.uji.es/upo/rest/time"
    }).done(function(data)
    {
        var serverTime = data;
        var localTime = new Date().getTime();
        var timeDiff = serverTime - localTime;

        secondInterval = setInterval(function()
        {
            var time = new Date(new Date().getTime() + timeDiff);

            digits.h1.attr('class', digit_to_name[formatNumber(time.getHours()).toString()[0]]);
            digits.h2.attr('class', digit_to_name[formatNumber(time.getHours()).toString()[1]]);
            digits.m1.attr('class', digit_to_name[formatNumber(time.getMinutes()).toString()[0]]);
            digits.m2.attr('class', digit_to_name[formatNumber(time.getMinutes()).toString()[1]]);
            digits.s1.attr('class', digit_to_name[formatNumber(time.getSeconds()).toString()[0]]);
            digits.s2.attr('class', digit_to_name[formatNumber(time.getSeconds()).toString()[1]]);

            dateTag.html(formatNumber(time.getDate()) + '/' + formatNumber(time.getMonth() + 1) + '/' + time.getFullYear());

        }, 1000);
    });
}

var syncTimeFromServer = function()
{
    if (document.getElementById("clock") === null)
    {
        return;
    }

    setTimeFromServer();

    setInterval(function()
    {
        clearInterval(secondInterval);
        setTimeFromServer()
    }, 1000 * 60 * 5);
}

$(document).ready(function()
{
    syncTimeFromServer();
});