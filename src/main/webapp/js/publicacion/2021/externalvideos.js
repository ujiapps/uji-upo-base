var replaceVideosFromImages = function()
{
    $(".gdpr-video").each(function() {
        var url = $(this).data("url")
            parent = $(this).parent();

        parent.children().remove();

        parent.prepend('<iframe width="100%" class="news-video" title="Youtube video" src="' + url +'" frameborder="0" allowfullscreen="allowfullscreen">');
    })
}

$(document).ready(function()
{
    replaceVideosFromImages();
});