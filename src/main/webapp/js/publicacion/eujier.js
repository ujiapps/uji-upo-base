var Eujier = (function()
{
    var acciones = {
        ADD : 'add',
        DEL : 'del'
    };

    var tipoVista = {
        LISTA : 'lista',
        CUADRICULA : 'cuadricula'
    };

    var reloadMethod = {
        AJAX : 'ajax',
        SUBMIT : 'submit'
    };

    var ID_SECCION_FAVORITOS = '#seccionfavoritos',
        ACCION_FAVORITO      = 'a.eujier-action-favorito',
        PREFIJO_ID_BOTON     = '#button',
        PREFIJO_ID_SECCION   = '#seccion',
        PREFIJO_ITEM         = 'item',
        SECCION              = 'div.seccion',
        ACCION_AVISO         = 'a.eujier-action-aviso',
        PREFIJO_BUTTON       = 'button',
        PREFIJO_AVISO        = 'aviso';

    function toggleIconFavoritos(itemId)
    {
        var boxes = $(itemId);

        $.each(boxes, function(index, box)
        {
            $(box).find(ACCION_FAVORITO).children().first().toggleClass('fa-star fa-star-o')
        });
    }

    function delItemFromFavoritos(itemId)
    {
        $(ID_SECCION_FAVORITOS).find(itemId).fadeOut(function()
        {
            $(this).remove();
        });
    }

    function reloadSection(codigoSeccion)
    {
        var seccion = $(PREFIJO_ID_SECCION + codigoSeccion);

        if (seccion.length === 0)
        {
            return;
        }

        $.ajax(
        {
            url : '/upo/rest/iglu/section?codigoSeccion=' + codigoSeccion + '&vista=' + $(PREFIJO_ID_BOTON + codigoSeccion).next().val(),
            type : 'GET',
            success : function(r)
            {
                seccion.html(r);
                equalizeSectionBoxes(codigoSeccion)
            }
        });
    }

    function getActionFavoritos(classString)
    {
        return (classString.indexOf('fa-star-o') >= 0) ? acciones.ADD : acciones.DEL;
    }

    function getIdFavoritos(classString)
    {
        var classList = classString.split(/\s+/), id = "";

        $.each(classList, function(index, item)
        {
            if (item.indexOf(PREFIJO_ITEM) === 0)
            {
                id = item.substr(PREFIJO_ITEM.length);
            }
        });

        return id;
    }

    function getValueButtonVista(value)
    {
        if (value.toLowerCase() === tipoVista.CUADRICULA)
        {
            return tipoVista.LISTA;
        }

        return tipoVista.CUADRICULA;
    }

    function onClickFavoritos()
    {
        $(SECCION).on('click', ACCION_FAVORITO, function()
        {
            var id     = getIdFavoritos($(this).closest("li").attr('class')),
                icon   = $(this).children().first(),
                action = getActionFavoritos(icon.attr('class'));

            $.ajax(
            {
                url : "/upo/rest/iglu/favourite",
                type : 'PUT',
                data : {
                    id : id,
                    action : action
                },
                success : function(r)
                {
                    var itemId = '.item' + id;

                    toggleIconFavoritos(itemId);

                    if (action === acciones.DEL)
                    {
                        delItemFromFavoritos(itemId)
                    }

                    if (action === acciones.ADD)
                    {
                        reloadSection('favoritos');
                    }
                }
            });

            return false;
        });
    }

    function onClickAvisos()
    {
        $(ACCION_AVISO).click(function()
        {
            var id     = $(this).attr("id").substr(PREFIJO_AVISO.length),
                parent = $(this).parent();

            $.ajax(
            {
                url : "/upo/rest/iglu/notice",
                type : 'PUT',
                data : {
                    id : id
                },
                success : function(r)
                {
                    parent.fadeOut();
                }
            });

            return false;
        });
    }

    function toggleValueButtonVista(button)
    {
        var hidden = $(button).next();
        hidden.val(getValueButtonVista(hidden.val()));
    }

    function toggleViewSection(button, method)
    {
        var codigoSeccion = button.id.substr(PREFIJO_BUTTON.length);
        toggleValueButtonVista(button);

        if (method === reloadMethod.AJAX)
        {
            reloadSection(codigoSeccion);
        }

        if (method === reloadMethod.SUBMIT)
        {
            document.base.submit();
        }
    }

    function equalizeAllBoxes()
    {
        Boxes.equalizeAllBoxes($("div.eujier-aplicacion-top"));
        Boxes.equalizeAllBoxes($("div.eujier-aplicacion-bottom"));
    }

    function equalizeSectionBoxes(codigoSeccion)
    {
        Boxes.equalizeAllBoxes($("div" + PREFIJO_ID_SECCION + codigoSeccion + " div.eujier-aplicacion-top"));
        Boxes.equalizeAllBoxes($("div" + PREFIJO_ID_SECCION + codigoSeccion + " div.eujier-aplicacion-bottom"));
    }

    function initListeners()
    {
        onClickAvisos();
        onClickFavoritos();
    }

    function doOpenWindow(url, params)
    {
        if (params)
        {
            window.open(url, '', params);
        }
        else
        {
            window.open(url, '', 'width=965,height=560,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=yes');
        }
    }

    function setDisconnectLink()
    {
        $.ajax(
        {
            url : "/upo/rest/authenticated",
            data : {
                url : window.location.pathname
            },
            dataType : "json",
            success : function(json)
            {
                if (json.data.autenticado === 'true')
                {
                    $('#disconnect-button').show();
                    $('#share-header-bar').toggleClass('tc', 'tl')
                }
            }
        });
    }

    return {
        initListeners : initListeners,
        toggleViewSection : toggleViewSection,
        equalizeAllBoxes : equalizeAllBoxes,
        doOpenWindow : doOpenWindow,
        setDisconnectLink : setDisconnectLink
    }
})
();

$(document).ready(function()
{
    Eujier.initListeners();
    Eujier.setDisconnectLink();
});

$(window).load(function()
{
    Eujier.equalizeAllBoxes();
});


