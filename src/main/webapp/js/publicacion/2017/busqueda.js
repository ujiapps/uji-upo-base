$(document).ready(function()
{
    $('#finicio').datepicker({
        firstDay: 1,
        dateFormat: 'dd/mm/yy',
        onClose: function( selectedDate ) {
            $( "#ffin" ).datepicker( "option", "minDate", selectedDate );
        }
    });

    $('#ffin').datepicker({
        firstDay: 1,
        dateFormat: 'dd/mm/yy',
        onClose: function( selectedDate ) {
            $( "#finicio" ).datepicker( "option", "maxDate", selectedDate );
        }
    });
});
