var setAuthenticatedLinks = function()
{
    $.ajax(
    {
        url : "/upo/rest/authenticated",
        data : {
            url : window.location.pathname
        },
        dataType: "json",
        success : function(json)
        {
            if (json.data.edicion === 'true')
            {
                var url = '/upo?url=' + window.location.pathname,
                    link = $('.uji-edit-link');

                link.each(function() {
                    $(this).show();
                    $(this).find('a').attr('href', url);
                });
            }

            if (json.data.autenticado === 'true')
            {
               $('.uji-logout-link').each(function()
               {
                   $(this).show();
               });
            }
        }
    });
}

$(document).ready(function()
{
    setAuthenticatedLinks();
});