var replaceVideosFromImages = function()
{
    $(".gdpr-video").each(function() {
        var url = $(this).data("url")
            parent = $(this).parent();

        parent.children().remove();

        parent.prepend('<iframe style="display: block; width: 100%;" title="Video Youtube" src="' + url +'" frameborder="0" allowfullscreen="allowfullscreen">');
    })
}

$(document).ready(function()
{
    replaceVideosFromImages();
});