var Boxes = (function()
{
    function buildGroups(group)
    {
        if (group.length === 0)
        {
            return;
        }

        var rowcount = 0,
        rows = [],
        topOffset = group.first().offset().top;

        group.each(function()
        {
            var el = $(this);
            var elTopOffset = el.offset().top;

            if (elTopOffset !== topOffset)
            {
                topOffset = elTopOffset;
                rowcount++;
            }

            if (!rows[rowcount])
            {
                rows[rowcount] = [];
            }
            rows[rowcount].push(el[0]);
        });

        return rows;
    }

    function equalizeHeight(rows)
    {
        if (!rows)
        {
            return;
        }

        rows.map(function(row)
        {
            var heights = row.map(function(tile)
            {
                return $(tile).outerHeight();
            });

            var max = Math.max.apply(null, heights);

            row.map(function(tile)
            {
                $(tile).height(max);
            });
        });
    }

    function equalizeAllBoxes(id)
    {
        equalizeHeight(buildGroups(id));
    }

    return {
        equalizeAllBoxes : equalizeAllBoxes
    }
})
();

