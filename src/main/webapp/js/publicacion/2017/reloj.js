var formatNumber = function(number)
{
    if (number < 10)
    {
        number = '0' + number;
    }

    return number;
}

var setTimeFromServer = function(tags)
{
    $.ajax(
    {
        url : "//www.uji.es/upo/rest/time"
    }).done(function(data)
    {
        var serverTime = data;
        var localTime = new Date().getTime();
        var timeDiff = serverTime - localTime;

        setInterval(function()
        {
            var time = new Date(new Date().getTime() + timeDiff);

            tags.tagServerDay.html(formatNumber(time.getDate()));
            tags.tagServerMonth.html(formatNumber((time.getMonth() + 1)));
            tags.tagServerYear.html(time.getFullYear());
            tags.tagServerHour.html(formatNumber(time.getHours()));
            tags.tagServerMinut.html(formatNumber(time.getMinutes()));
            tags.tagServerSecond.html(formatNumber(time.getSeconds()))

            tags.tagServerTime.show();
        }, 1000);
    });
}

var syncTimeFromServer = function()
{
    if (document.getElementById("server-datetime") === null)
    {
        return;
    }

    var tags =
        {
            tagServerDay : $('#server-day'),
            tagServerMonth : $('#server-month'),
            tagServerYear : $('#server-year'),
            tagServerHour : $('#server-hour'),
            tagServerMinut : $('#server-minut'),
            tagServerSecond : $('#server-second'),
            tagServerTime : $('#server-datetime')
        }

    setTimeFromServer(tags);

    setInterval(function()
    {
        setTimeFromServer(tags)
    }, 1000 * 60 * 15);
}

$(document).ready(function()
{
    syncTimeFromServer();
});