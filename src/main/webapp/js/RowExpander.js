Ext.ns('UJI.UPO');

UJI.UPO.RowExpander = Ext.extend(Ext.ux.grid.RowExpander,
{
    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.UPO.RowExpander.superclass.initComponent.call(this);
    },

    tpl : new Ext.Template('<div><div class="contenido-idioma-recurso" ><b>Catalàn: </b></div> {contenidoIdiomaRecursosCA}</div>'
            + '<div><div class="contenido-idioma-recurso-clear"><b>Español: </b></div> {contenidoIdiomaRecursosES}</div>'
            + '<div><div class="contenido-idioma-recurso-clear"><b>Inglés: </b></div> {contenidoIdiomaRecursosEN}</div>'),

    expandOnDblClick : false,
    enableCaching : true,

    getRowClass : function(record, rowIndex, p, ds)
    {
        var retorno = Ext.ux.grid.RowExpander.prototype.getRowClass.apply(this, arguments);

        var rowPendiente = '';

        if (record.data.estadoModeracion == UJI.UPO.EstadoModeracion.PENDIENTE)
        {
            rowPendiente = 'row-pendiente';
        }

        return retorno + ' ' + rowPendiente;
    },

    renderer : function(v, p, record)
    {
        var mostrar = record.data.contenidoIdiomaRecursosCA || record.data.contenidoIdiomaRecursosEN || record.data.contenidoIdiomaRecursosES;

        var retorno = Ext.ux.grid.RowExpander.prototype.renderer.apply(this, arguments);

        return mostrar ? retorno : '&#160;';
    },

    getBodyContent : function(record, index)
    {
        return this.tpl.apply(record.data);
    },

    collapseAll : function()
    {
        for ( var i = 0; i < this.grid.getStore().getCount(); i++)
        {
            this.collapseRow(this.grid.view.getRow(i));
        }
    },

    listeners :
    {
        expand : function(rexpander, v, body, rindex)
        {
            rexpander.grid.combierteRecursosEnDD();
        }
    }
});