UJI.UPO.TipoMetadato =
{
    1 : {
        tipo : 'textfield',
        nombre : 'Campo de texto',
        select : true
    },
    2 : {
        tipo : 'datefield',
        nombre : 'Campo de fecha',
        select : true
    },
    3 : {
        tipo : 'textarea',
        nombre : 'Área de texto',
        select : true
    },
    4 : {
        tipo : 'combo',
        nombre : 'Lista desplegable',
        select : false
    },
    5 : {
        tipo : 'editora',
        nombre : 'Área de texto con editora',
        select : true
    }
};