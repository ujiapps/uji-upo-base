UJI.UPO.IconosTiposMimeGridContenidos =
{
    'application/pdf' :
    {
        icono : 'pdf_icon.png',
        texto : 'contenido de tipo pdf'
    },
    'text/html' :
    {
        icono : 'html.png',
        texto : 'contenido de tipo html'
    },
    'image' :
    {
        icono : 'imagen_icon.png',
        texto : 'contenido de tipo imagen'
    },
    'video' :
    {
        icono : 'film.png',
        texto : 'contenido de tipo vídeo'
    },
    'youtube' :
    {
        icono : 'youtube.png',
        texto : 'contenido de tipo vídeo Youtube'
    },
    'vimeo' :
    {
        icono : 'vimeo.png',
        texto : 'contenido de tipo vídeo Vimeo'
    },
   'empty' :
    {
        icono : 'nada.png',
        texto : 'contenido vacío'
    },
    'binary' :
    {
        icono : 'binary_icon.png',
        texto : 'contenido de tipo binario o texto'
    },
    'mixed' :
    {
        icono : 'mixed_icon.png',
        texto : 'contenido de diferentes tipos'
    }
};