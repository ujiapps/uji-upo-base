UJI.UPO.TipoFormato =
{
    PAGINA :
    {
        name : 'Pàgina',
        value : 'PAGINA',
        icon: 'html.png'
    },
    BINARIO :
    {
        name : 'Binari',
        value : 'BINARIO',
        icon: 'binary_icon.png'
    },
    VIDEO_UJI :
    {
        name : 'Vídeo UJI',
        value : 'VIDEO_UJI',
        icon: 'film.png'
    },
    VIDEO_YOUTUBE :
    {
        name : 'Vídeo Youtube',
        value : 'VIDEO_YOUTUBE',
        icon: 'youtube.png'
    },
    VIDEO_VIMEO :
    {
        name : 'Vídeo Vimeo',
        value : 'VIDEO_VIMEO',
        icon: 'vimeo.png'
    },

    getData : function()
    {
        var result = [];

        Ext.each(Object.keys(this), function(elemento)
        {
            if (!Ext.isFunction(this[elemento]))
            {
                result.push(this[elemento]);
            }
        }, this);

        return result;
    }
};