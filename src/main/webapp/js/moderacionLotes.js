Ext.ns('UJI.UPO');

UJI.UPO.ModeracionLotes = Ext.extend(Ext.Panel,
{
    title : 'Moderació Migració per lots',
    layout : 'border',
    closable : true,
    layoutConfig :
    {
        align : 'stretch'
    },
    gridGestionSolicitudes : {},
    storeGestionSolicitudes : {},
    gridPermisosPersonas : {},
    storePermisosPersonas : {},
    storeComboPersonas : {},
    comboPersonas : {},
    botonAprobar : {},
    botonRechazar : {},

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.UPO.ModeracionLotes.superclass.initComponent.call(this);

        this.initUI();

        syncStoreLoad([ this.storeComboPersonas, this.storePermisosPersonas, this.storeGestionSolicitudes ]);

        var tabPanel = new Ext.TabPanel(
        {
            region : 'center',
            activeTab : 0,
            items : [
            {
                title : 'Sol·licituds',
                layout : 'fit',
                items : [ this.gridGestionSolicitudes ]
            } ]
        });

        this.add(tabPanel);
    },

    initUI : function()
    {
        this.buildBotonInsertarPersona();
        this.buildBotonBorrarPersona();
        this.buildEditorPermisosPersonas();

        this.buildStoreComboPersonas();
        this.buildComboPersonas();

        this.buildStoreGestionSolicitudes();
        this.buildStorePermisosPersonas();

        this.buildBotonAprobar();
        this.buildBotonRechazar();

        this.buildGridGestionSolicitudes();
        this.buildGridPermisosPersonas();
    },

    buildBotonAprobar : function()
    {
        var ref = this;

        this.botonAprobar = new Ext.Button(
        {
            text : 'Aprovar',
            iconCls : 'application-add',
            handler : function()
            {
                var rec = ref.gridGestionSolicitudes.getSelectionModel().getSelected();

                if (!rec)
                {
                    Ext.Msg.alert('Información', 'És necessari seleccionar una fila per poder aprovar');
                    return false;
                }
                else
                {
                    Ext.Msg.confirm('Confirmació', 'Esteu segur de voler aprovar el registre?', function(btn, text)
                    {
                        if (btn == 'yes')
                        {
                            var record = ref.storeGestionSolicitudes.getById(rec.data.id);
                            record.set('estado', UJI.UPO.EstadoModeracion.ACEPTADO);

                            ref.storeGestionSolicitudes.save();
                        }
                    });
                }
            }
        });
    },

    buildBotonRechazar : function()
    {
        var ref = this;

        this.botonRechazar = new Ext.Button(
        {
            text : 'Rebutjar',
            iconCls : 'application-add',
            handler : function()
            {
                var rec = ref.gridGestionSolicitudes.getSelectionModel().getSelected();

                if (!rec)
                {
                    Ext.Msg.alert('Información', 'És necessari seleccionar una fila per poder rebutjar');
                    return false;
                }
                else
                {
                    var textAreaRechazo = new Ext.form.TextArea(
                    {
                        fieldLabel : 'Introdueix el motiu de rebuig',
                        width : 200,
                        height : 100
                    });

                    var formularioRegistro = new Ext.form.FormPanel(
                    {
                        frame : true,
                        items : [ textAreaRechazo ]
                    });

                    var window = new Ext.Window(
                    {
                        title : 'Motiu de rebuig',
                        layout : 'fit',
                        height : 200,
                        width : 340,
                        modal : true,
                        items : [ formularioRegistro ],
                        fbar : [
                        {
                            xtype : 'button',
                            text : 'Registrar',
                            handler : function()
                            {
                                var record = ref.storeGestionSolicitudes.getById(rec.data.id);

                                record.set('estado', UJI.UPO.EstadoModeracion.RECHAZADO);
                                record.set('motivo', textAreaRechazo.getValue());

                                ref.storeGestionSolicitudes.save();

                                window.close();
                            }
                        },
                        {
                            xtype : 'button',
                            text : 'Cancel·lar',
                            handler : function()
                            {
                                window.close();
                            }
                        } ]
                    }).show();
                }
            }
        });
    },

    buildStoreGestionSolicitudes : function()
    {
        var ref = this;

        ref.storeGestionSolicitudes = new Ext.data.Store(
        {
            autoSave : false,
            restful : true,
            proxy : new Ext.data.HttpProxy(
            {
                url : '/upo/rest/migracionlotes'
            }),
            reader : new Ext.data.XmlReader(
            {
                record : 'ModeracionLote',
                id : 'id'
            }, [ 'id', 'estado', 'fechaRechazo', 'fechaSolicitud', 'fechaFinMigracion', 'url', 'motivo' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }, [ 'id', 'estado', 'fechaRechazo', 'fechaSolicitud', 'fechaFinMigracion', 'url', 'motivo' ]),
            listeners :
            {
                'save' : function(store, record, operation)
                {
                    ref.storeGestionSolicitudes.reload();
                }
            }
        });
    },

    buildGridGestionSolicitudes : function()
    {
        this.gridGestionSolicitudes = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            viewConfig :
            {
                forceFit : true
            },
            selModel : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : this.storeGestionSolicitudes,
            colModel : new Ext.grid.ColumnModel(
            {
                columns : [
                {
                    header : 'Codi',
                    width : 100,
                    dataIndex : 'id'
                },
                {
                    header : 'URL',
                    width : 500,
                    dataIndex : 'url'
                } ]
            }),
            tbar : [ this.botonAprobar, this.botonRechazar ]
        });
    },

    buildStorePermisosPersonas : function()
    {
        var ref = this;

        ref.storePermisosPersonas = new Ext.data.Store(
        {
            restful : true,
            url : '/upo/rest/permisos-migracion',
            reader : new Ext.data.XmlReader(
            {
                record : 'MigracionUrlsPer',
                id : 'id'
            }, [ 'id', 'personaId', 'urlBase' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildBotonInsertarPersona : function()
    {
        var ref = this;

        this.botonInsertarPersona = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function()
            {
                var rec = new ref.storePermisosPersonas.recordType(
                {
                    personaId : '',
                    urlBase : ''
                });

                ref.editorPermisosPersonas.stopEditing();
                ref.storePermisosPersonas.insert(0, rec);
                ref.editorPermisosPersonas.startEditing(0);
            }
        });
    },

    buildBotonBorrarPersona : function()
    {
        var ref = this;
        this.botonBorrarPersona = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function()
            {
                var rec = ref.gridPermisosPersonas.getSelectionModel().getSelected();
                if (!rec)
                {
                    Ext.Msg.alert('Información', 'És necessari seleccionar una fila per esborrar');
                    return false;
                }
                else
                {
                    Ext.Msg.confirm('Esborrat', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                    {
                        if (btn == 'yes')
                        {
                            ref.storePermisosPersonas.remove(rec);
                        }
                    });
                }
            }
        });
    },

    buildEditorPermisosPersonas : function()
    {
        this.editorPermisosPersonas = new Ext.ux.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancel.lar',
            errorSummary : false
        });
    },

    buildGridPermisosPersonas : function()
    {
        this.gridPermisosPersonas = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            viewConfig :
            {
                forceFit : true
            },
            selModel : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            store : this.storePermisosPersonas,
            plugins : [ this.editorPermisosPersonas ],
            colModel : new Ext.grid.ColumnModel(
            {
                columns : [
                {
                    header : "Id",
                    hidden : true,
                    dataIndex : 'id'
                },
                {
                    header : "Persona",
                    dataIndex : 'personaId',
                    width : 500,
                    editor : ref.comboPersonas,
                    renderer : Ext.util.Format.comboRenderer(ref.comboPersonas)
                },
                {
                    header : 'URL',
                    width : 500,
                    dataIndex : 'urlBase',
                    editor : new Ext.form.TextField(
                    {
                        allowBlank : false,
                        blankText : 'El "nom" és obligatori'
                    })
                } ]
            }),
            tbar : [ ref.botonInsertarPersona, '-', ref.botonBorrarPersona ]
        });
    },

    buildStoreComboPersonas : function()
    {
        ref = this;

        this.storeComboPersonas = new Ext.data.Store(
        {
            url : '/upo/rest/persona',
            restful : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Persona',
                id : 'id'
            }, [ 'id', 'nombreCompleto' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildComboPersonas : function()
    {
        var ref = this;

        this.comboPersonas = new Ext.form.ComboBox(
        {
            triggerAction : 'all',
            forceSelection : true,
            allowBlank : false,
            editable : false,
            displayField : 'nombreCompleto',
            valueField : 'id',
            store : ref.storeComboPersonas
        });
    }
});